<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemOperationsRepository")
 */
class Itemoperations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $itemid;

    /**
     * @ORM\Column(type="integer")
     */
    private $containerid;

    /**
     * @ORM\Column(type="integer")
     */
    private $containertypeid;

    /**
     * @ORM\Column(type="integer")
     */
    private $stockid;

    /**
     * @ORM\Column(type="integer")
     */
    private $workstationid;

    /**
     * @ORM\Column(type="integer")
     */
    private $parentid;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $description;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getItemid(): ?int
    {
        return $this->itemid;
    }

    public function setItemid(int $itemid): self
    {
        $this->itemid = $itemid;

        return $this;
    }

    public function getContainerid(): ?int
    {
        return $this->containerid;
    }

    public function setContainerid(int $containerid): self
    {
        $this->containerid = $containerid;

        return $this;
    }

    public function getContainertypeid(): ?int
    {
        return $this->containertypeid;
    }

    public function setContainertypeid(int $containertypeid): self
    {
        $this->containertypeid = $containertypeid;

        return $this;
    }

    public function getStockid(): ?int
    {
        return $this->stockid;
    }

    public function setStockid(int $stockid): self
    {
        $this->stockid = $stockid;

        return $this;
    }

    public function getWorkstationid(): ?int
    {
        return $this->workstationid;
    }

    public function setWorkstationid(int $workstationid): self
    {
        $this->workstationid = $workstationid;

        return $this;
    }

    public function getParentid(): ?int
    {
        return $this->parentid;
    }

    public function setParentid(int $parentid): self
    {
        $this->parentid = $parentid;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
