<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\StatisticsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController extends ApiController {

    /**
    * @Route("/statistics/gtin/position/exact", methods="GET")
    */
    public function downloadPositionExactFile(
        EntityManagerInterface $em,
        StatisticsRepository $statisticsRepo
    ) {

        // Provide a name for your file with extension
        $filename = 'ExactFlow.csv';
        
        // The dinamically created content of the file
        $fileContent = $statisticsRepo->getExactGtinPosition();
        
        // Return a response with a specific content
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');

        // Dispatch request
        return $response;

    }

    /**
    * @Route("/statistics/gtin/position/image", methods="GET")
    */
    public function downloadPositionImageFile(
        EntityManagerInterface $em,
        StatisticsRepository $statisticsRepo
    ) {


        // Provide a name for your file with extension
        $filename = 'ImageFlow.csv';
        
        // The dinamically created content of the file
        $fileContent = $statisticsRepo->getImageGtinPosition();
        
        // Return a response with a specific content
        $response = new Response($fileContent);

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'text/csv; charset=windows-1252');

        // Dispatch request
        return $response;

    }

    /**
    * @Route("/statistics/gtin/position/all", methods="GET")
    */
    public function downloadPositionAllFile(
        EntityManagerInterface $em,
        StatisticsRepository $statisticsRepo
    ) {
        return $this->respond(
            array(
                'error'         => false,
                'errormessage'  => '', 
                'data'          => 'asdasdasd',
            )
        );
    }

    /**
    * @Route("/statistics/gtin", methods="POST")
    */
    public function statisticsGtin(
        EntityManagerInterface $em,
        StatisticsRepository $statisticsRepo,
        Request $request
    ) {

        $request = file_get_contents('php://input');
        if (!$request && $request !== '') {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object', 
                    'data'          => $request,
                )
            );
        }
        $req = $request;
        $request = json_decode($request, true);
        
        if($request == '' && $req !== ''){
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object', 
                    'data'          => [],
                )
            );  
        }

        $postCheck = $statisticsRepo->checkPostedData($request);
        if($postCheck !== true){
            return $this->respondError(
                array(
                    'error'         => true,
                    'errormessage'  => $postCheck,
                    'data'          => [],
                )
            );
        }

        return $this->respond(
            array(
                'error'         => false,
                'errormessage'  => '', 
                'data'          => $statisticsRepo->getItemStatistics($request),
            )
        );

    }

    /**
    * @Route("/statistics/gtin", methods="GET")
    */
    public function statisticsGtinGet(
        EntityManagerInterface $em,
        StatisticsRepository $statisticsRepo,
        Request $request
    ) {

        $params = array();

        if($request->query->get('active') !== null){
            $params['active'] = $request->query->get('active');
        }
        if($request->query->get('size') !== null){
            $params['size'] = (int)$request->query->get('size');
        }
        if($request->query->get('page') !== null){
            $params['page'] = (int)$request->query->get('page');
        }
        if($request->query->get('fromdate') !== null){
            $params['fromdate'] = $request->query->get('fromdate');
        }
        if($request->query->get('todate') !== null){
            $params['todate'] = $request->query->get('todate');
        }

        $postCheck = $statisticsRepo->checkPostedData($params);
        if($postCheck !== true){
    		return $this->respondError(
	            array(
	                'error'        	=> true,
	                'errormessage' 	=> $postCheck,
	                'data' 			=> [],
	            )
	        );
    	}

        return $this->respond(
            array(
                'error'        	=> false,
                'errormessage' 	=> '', 
                'data' 			=> $statisticsRepo->getItemStatistics($params),
            )
        );

    }
   
}