<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
// use App\Repository\CompanyLogRepository;
// use App\Repository\OrdercontactlogRepository;
// use App\Repository\AuthRepository;
// use App\Repository\UserLoginRepository;
// use App\Repository\UserGuidRepository;
// use App\Repository\CompanyRepository;
// use App\Repository\SalesOrderRepository;
// use App\Repository\MailRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
// use SendGrid\Mail\Mail;

class AdminController extends ApiController {

//     /**
//     * @Route("/admin/companylogtest", methods="GET")
//     */
//     public function companylogtest(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         CompanyRepository $companyRepository,
//         UserGuidRepository $userGuidRepository,
//         CompanyLogRepository $companyLogRepository,
//         OrdercontactlogRepository $ordercontactlogRepository,
//         SalesOrderRepository $salesOrderRepository,
//         Request $request
//     ) {

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 //'orders'         => $salesOrderRepository->convertOrdersToCsv($salesOrderRepository->findOrdersWithNewItems($salesOrderRepository->getOrdersByProduct('exact'))),
//             )
//         );
        
//         $filename = 'TextFile.csv';

//         $fileContent = $salesOrderRepository->convertOrdersToCsv($salesOrderRepository->findOrdersWithNewItems($salesOrderRepository->getOrdersByProduct('exact')));//"Hello, this is the content of my File";
        
//         // Return a response with a specific content
//         $response = new Response($fileContent);

//         // Create the disposition of the file
//         $disposition = $response->headers->makeDisposition(
//             ResponseHeaderBag::DISPOSITION_ATTACHMENT,
//             $filename
//         );

//         // Set the content disposition
//         $response->headers->set('Content-Disposition', $disposition);

//         // Dispatch request
//         return $response;
        


//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 'orders'         => $salesOrderRepository->convertOrdersToCsv($salesOrderRepository->findOrdersWithNewItems($salesOrderRepository->getOrdersByProduct('exact'))),
//             )
//         );

        
//         $users = $userGuidRepository->getDistinctCompanies();
//         $companies = array();
//         $ordercontacts = array();
//         foreach ($users as $key => $value) {
//             // if($ordercontactlogRepository->IsInDb($value['bccompanyid']) === false){
//             //     $companyRepository->createAllOrderContacts($value['bccompanyid']);
//             // }
//             // if($companyLogRepository->checkIfInDb($value['bccompanyid']) === false){
//             //     // $companies[] = array($companyRepository->getBcCompanyInfo($value['bccompanyid']), $value['bccompanyid']);
//             //     $company = '';
//             //     $company = array($companyRepository->getBcCompanyInfo($value['bccompanyid']), $value['bccompanyid']);
//             //     if(isset($company[0]->iBranch_Noa46) === true){
//             //         $company[0]->iBranch_Noa46 = $companyRepository->getIndustriesSpecific($company[0]->iBranch_Noa46);
//             //     }
//             //     $temp = json_decode(json_encode($company[0]), true);
//             //     $companyLogRepository->createCompanyLogEntryFirstAdmin($temp, $company[1]);
//             //     // die();
//             // }
//         }

// /*
// companyid: 102352
// [2020-10-12 20:20:09] app.ERROR: ** Sunland ** getOrderContact() Returned error: {"faultstring":"The length of the string is 59, but it must be less than or equal to 50 characters. Value: AKTIESELSKABET THISTED BRYGHUS & THISTED MINERALVANDSFABRIK","faultcode":"a:Microsoft.Dynamics.Nav.Types.Exceptions.NavNCLStringLengthExceededException","detail":{"string":"The length of the string is 59, but it must be less than or equal to 50 characters. Value: AKTIESELSKABET THISTED BRYGHUS & THISTED MINERALVANDSFABRIK"}} 
// */

//         // $array = array();
//         // foreach ($companies as $key => $value) {
//         //     if(isset($value[0]->iBranch_Noa46) === true){
//         //         $value[0]->iBranch_Noa46 = $companyRepository->getIndustriesSpecific($value[0]->iBranch_Noa46);
//         //     }
//         //     $temp = json_decode(json_encode($value[0]), true);
//         //     $companyLogRepository->createCompanyLogEntryFirstAdmin($temp, $value[1]);
//         //     die();
//         // }

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 // 'users'         => $companies
//             )
//         );

//     }

//     /**
//     * @Route("/admin/existingusers/create", methods="POST")
//     */
//     public function createExistingUsersList(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         if($header['Authorization'] !== 'Alexersej'){
//             return $this->respondLogout('Please login again');
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $users = array();
//         foreach ($request['contacts'] as $key => $contact) {
//             if($userLoginRepository->findUserInApiByEmailStr($contact['email']) === false){
//                 $created = $userGuidRepository->createBcAndCrmUsersAlreadyInBc($contact['bccompanyid'], $contact);
//                 array_push($users, array('email' => $contact['email'], 'created' => $created) );
//             }
//             else{
//                 array_push($users, array('email' => $contact['email'], 'created' => 'Email already in system') );   
//             }
//         }


//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 // 'request'       => $request,
//                 'users'         => $users
//             )
//         );


//     }

//     /**
//     * @Route("/admin/send/subscription/mails", methods="POST")
//     */
//     public function sendSubscriptionMails(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         MailRepository $MailRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         if($header['Authorization'] !== 'Alexersej'){
//             return $this->respondLogout('Please login again');
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $users = array();
//         foreach ($request['contacts'] as $key => $contact) {
//             $MailRepository->sendSubscriptionMail($contact['email']);
//         }


//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 // 'request'       => $request,
//                 //'users'         => $users
//             )
//         );


//     }

//     /**
//     * @Route("/admin/download/pdf/order", methods="POST")
//     */
//     public function getSingleOrderPdf(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         SalesOrderRepository $salesOrderRepository,
//         CompanyRepository $companyRepository,
//         MailRepository $MailRepository,
//         Request $request
//     ) {

//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 
//                 )
//             );
//         }

//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);
        
//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $order = $salesOrderRepository->getOrder($request['orderid']);
//         if(getType($order) !== 'array'){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => 'No order found with id'. $request['orderid'], 
//                 )
//             );
//         }

//         $pdf = $MailRepository->adminEmailOrderPdf($order['content'], $request['orderid'], $request['email'], 'da');

//         if($pdf !== true){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => $pdf, 
//                 )
//             );
//         }

//         return $this->respond(
//             array(
//                 'status'    => 'success',
//                 'response'  => 'Order', 
//                 'pdf'       =>  $pdf
//             )
//         );
//     }

//     /**
//     * @Route("/admin/get/order/{orderid}", methods="GET")
//     */
//     public function getSingleOrder(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         SalesOrderRepository $salesOrderRepository,
//         CompanyRepository $companyRepository,
//         $orderid,
//         Request $request
//     ) {

//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 
//                 )
//             );
//         }

//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);
        
//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//         }

//         $order = $salesOrderRepository->getOrder($orderid);
//         if(getType($order) !== 'array'){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => 'No order found with id'. $orderid, 
//                 )
//             );
//         }

//         $company = $companyRepository->getBcCompanyInfo($order['company']);
//         if(getType($company) == 'string'){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => $company, 
//                 )
//             );
//         }

//         return $this->respond(
//             array(
//                 'status'    => 'success',
//                 'response'  => 'Order', 
//                 'order'    => $order,
//                 'company'   => $company,
//             )
//         );
//     }

//     /**
//     * @Route("/admin/get/all/orders/{offset}/{companyid}", methods="GET")
//     */
//     public function getAllOrdersWithOffset(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         SalesOrderRepository $salesOrderRepository,
//         CompanyRepository $companyRepository,
//         $offset,
//         $companyid,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }

//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//         }

//         if($companyid == 0){
//             $company = array('iName' => 'All companies');
//         }
//         else{    
//             $company = $companyRepository->getBcCompanyInfo($companyid);
//             if(getType($company) == 'string'){
//                 return $this->respond(
//                     array(
//                         'status'    => 'error',
//                         'response'  => $company, 
//                     )
//                 );
//             }
//         }

//         $orders = $salesOrderRepository->getAllOrders($offset, $companyid);

//         return $this->respond(
//             array(
//                 'status'    => 'success',
//                 'response'  => 'Orders with offset', 
//                 'orders'    => $orders,
//                 'company'   => $company,
//             )
//         );


//     }
    
//     /**
//     * @Route("/admin/company/get/users", methods="POST")
//     */
//     public function companygetusers(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         CompanyRepository $companyRepository,
//         CompanyLogRepository $companylogRepository,
//         OrdercontactlogRepository $ordercontactlog,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         if($request['companyid'] == '' && $request['companycvr'] == ''){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => 'Either Debitor number or CVR must be filled.', 
//                 )
//             );
//         }

//         if($request['companyid'] !== ''){
//             $company = $companyRepository->getBcCompanyInfo($request['companyid']);
//             $companyid = $request['companyid'];
//         }
//         else{
//             $company = $companyRepository->getCompanyByCvrBc($request['companycvr'], '');
//             $companyid = $company->iCustNo;
//         }

//         if(getType($company) == 'string'){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => $company, 
//                 )
//             );
//         }

//         $users = $companyRepository->getUsersByCompany($companyid, false);

//         if(getType($users) == 'string'){
//             return $this->respond(
//                 array(
//                     'status'    => 'error',
//                     'response'  => $company, 
//                     'users'  => $users, 
//                 )
//             );
//         }

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users info success', 
//                 'users'         => $users,
//                 'company'       => $company,
//                 'id'            => $companyid,
//                 'companylog'    => $companylogRepository->getCompanySpecificLog($companyid),
//                 'ordercontacts' => $ordercontactlog->getOrdercontactLog($companyid)
//             )
//         );


//     }

//     // /**
//     // * @Route("/admin/user/reset/password", methods="POST")
//     // */
//     // public function resetPassword(
//     //     EntityManagerInterface $em,
//     //     AuthRepository $authRepository,
//     //     UserLoginRepository $userLoginRepository,
//     //     UserGuidRepository $userGuidRepository,
//     //     Request $request
//     // ) {

//     //     $request = file_get_contents('php://input');

//     //     if (!$request) {
//     //         return $this->respond(array(
//     //                 'status' => 'error',
//     //                 'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//     //             )
//     //         );
//     //     }

//     //     $request = json_decode($request, true);

//     //     $user = $userLoginRepository->findUserInApiByEmailStr($request['email']);
//     //     $userbc = $userGuidRepository->getBcContactInfo($user);
//     //     $name = '';
//     //     if(isset($userbc->Name) === true){
//     //         $name = $userbc->Name;
//     //     }
//     //     $token = $userLoginRepository->resetUserByEmail($request['email'], $name, $request['lang']);
        
//     //     if($token === false){
//     //         return $this->respond(
//     //             array(
//     //                 'status' => 'error',
//     //                 'response' => 'If the email exists in our system you will get a reset password mail.'
//     //             )
//     //         );
//     //     }
//     //     else{
//     //         return $this->respond(array(
//     //                 'status'    => 'success',
//     //                 'response' => 'If the email exists in our system you will get a reset password mail.'
//     //                 // 'token'     => $token
//     //             )
//     //         );
//     //     }
//     // }



//     // /**
//     // * @Route("/admin/user/update/password", methods="POST")
//     // */
//     // public function updatePassword(
//     //     EntityManagerInterface $em,
//     //     AuthRepository $authRepository,
//     //     UserLoginRepository $userLoginRepository,
//     //     Request $request
//     // ) {
//     //     $header = getallheaders();
//     //     if(isset($header['Authorization']) === false){
//     //         return $this->respondLogout('Please login again');
//     //         return $this->respond(array(
//     //                 'status' => 'error',
//     //                 'response' => 'Please provide a valid token.', 

//     //             )
//     //         );
//     //     }

//     //     $request = file_get_contents('php://input');

//     //     if (!$request) {
//     //         return $this->respond(array(
//     //                 'status' => 'error',
//     //                 'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//     //             )
//     //         );
//     //     }

//     //     $request = json_decode($request);
        
//     //     $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);
//     //     if(getType($user) == 'object'){
//     //         $return = $userLoginRepository->updatePassword($user, $request->password);
//     //         if($return === true){
//     //             return $this->respond(
//     //                 array(
//     //                     'status' => 'success',
//     //                     'response' => 'Password updated, please try to login again.', 
//     //                 )
//     //             );
//     //         }

//     //         return $this->respond(array(
//     //                 'status' => 'error',
//     //                 'response' => 'error in password setting.', 

//     //             )
//     //         );
//     //     }
//     //     else{
//     //         return $this->respondLogout('Please login again');
//     //         return $this->respond(array(
//     //                 'status' => 'error',
//     //                 'response' => 'token not in system, try to reset the password again.', 

//     //             )
//     //         );
//     //     }

//     // }

//     /**
//     * @Route("/admin/user/deactivate", methods="POST")
//     */
//     public function deactivateUser(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $userGuidRepository->deactivateUser($request['bcuserid']);

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users deactivated success', 
//             )
//         );
//     }

//     /**
//     * @Route("/admin/user/activate", methods="POST")
//     */
//     public function activateUser(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $userGuidRepository->activateUser($request['bcuserid']);

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users deactivated success', 
//             )
//         );
//     }

//     /**
//     * @Route("/admin/user/admin", methods="POST")
//     */
//     public function adminUser(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request, true);

//         $userGuidRepository->setUserAdmin($request['bcuserid'], $request['admin']);

//         return $this->respond(
//             array(
//                 'status'        => 'success',
//                 'response'      => 'users deactivated success', 
//             )
//         );
//     }

//     /**
//     * @Route("/admin/user/create", methods="POST")
//     */
//     public function create(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }
//         $request = json_decode($request, true);

//         if(isset($request['contacts']) === true && isset($request['bccompanyid'])){
//             // $user = $userLoginRepository->createUserInApi($request);
//             $user = $userGuidRepository->createBcAndCrmUsers($request['bccompanyid'], $request['contacts'], 0);
//             if($user === false){
//                 return $this->respond(array(
//                         'status' => 'error',
//                         'response' => 'Email already exists.', 
//                         'data'  => $user,
//                     )
//                 );
//             }
            
//             return $this->respond(array(
//                     'status'    => 'success',
//                     'response'  => 'logincomplete', 
//                     // 'token'     => $user['token'],
//                     'user'      => $user,
//                 )
//             );            
//         }

//         return $this->respond(array(
//                 'status' => 'error',
//                 'response' => 'loginfail', 
//             )
//         );


//     }

//     /**
//     * @Route("/admin/user/create/alreadyinsystem", methods="POST")
//     */
//     public function createAlreadyInSystem(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {
//         $header = getallheaders();
//         if(isset($header['Authorization']) === false){
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Please provide a valid token.', 

//                 )
//             );
//         }
        
//         $user = $this->validateToken($authRepository, $userLoginRepository, $header['Authorization']);

//         if(getType($user) == 'object'){
//         }
//         else{
//             return $this->respondLogout('Please login again');
//             return $this->respond(array(
//                     'status' => 'tokeninvalid',
//                     'response' => 'loginfail', 
//                     'user'  => $header

//                 )
//             );
//         }

//         $request = file_get_contents('php://input');
//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }
//         $request = json_decode($request, true);


//         $user = $userGuidRepository->createBcAndCrmUsersAlreadyInBc($request['bccompanyid'], $request['contact']);
        
//         return $this->respond(array(
//                 'status'    => 'success',
//                 'response'  => 'logincomplete', 
//                 // 'token'     => $user['token'],
//                 'user'      => $user,
//             )
//         );            


//     }

//     /**
//     * @Route("/admin/user/login", methods="POST")
//     */
//     public function login(
//         EntityManagerInterface $em,
//         AuthRepository $authRepository,
//         UserLoginRepository $userLoginRepository,
//         UserGuidRepository $userGuidRepository,
//         Request $request
//     ) {


//         $request = file_get_contents('php://input');

//         if (!$request) {
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => 'Serveren modtog ikke en gyldig anmodning.', 
//                 )
//             );
//         }

//         $request = json_decode($request);

//         if(isset($request->password) && isset($request->email)){
//             $user = $userLoginRepository->findUserInApi($request, 0);
            
//             if(getType($user) == 'array'){
//                 // $userBc = $userLoginRepository->findUserByBcUser($bcUser, $em);
                
//                 if($user['admin'] === 0){
//                     return $this->respond(array(
//                             'status' => 'error',
//                             'response' => 'loginfail', 
//                         )
//                     );
//                 }

//                 return $this->respond(
//                     array(
//                         'status'        => 'success',
//                         'response'      => 'logincomplete', 
//                         'token'         => $user['token'],
//                         'user'          => $user,
//                     )
//                 );
//             }
            
//             return $this->respond(array(
//                     'status' => 'error',
//                     'response' => $user, 
//                     'request'   => $request,
//                 )
//             );            
//         }

//         return $this->respond(array(
//                 'status' => 'error',
//                 'response' => 'loginfail', 
//             )
//         );

//     }


    
}