<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
// use App\Repository\CompanyLogRepository;
// use App\Repository\OrdercontactlogRepository;
// use App\Repository\AuthRepository;
// use App\Repository\UserLoginRepository;
// use App\Repository\UserGuidRepository;
// use App\Repository\CompanyRepository;
// use App\Repository\SalesOrderRepository;
use App\Repository\ItemOperationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
// use SendGrid\Mail\Mail;

class TestController extends ApiController {

    /**
    * @Route("/test", methods="GET")
    */
    public function companylogtest(
        EntityManagerInterface $em,
        ItemOperationsRepository $itemOperations,
        Request $request
    ) {

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $itemOperations->getAll(), 
            )
        );
    }
   
}