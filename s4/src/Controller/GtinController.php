<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\GtinRepository;
use App\Repository\ContainerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class GtinController extends ApiController {

    /**
    * @Route("/inventory/get/gtin/{gtin}", methods="GET")
    */
    public function getGtin(
        EntityManagerInterface $em,
        GtinRepository $gtinRepository,
        ContainerRepository $containerRepository,
        Request $request,
        $gtin
    ) {

    	$gtinInfo = $gtinRepository->getGtinInfo($gtin);
    	if($gtinInfo === true){
    		return $this->respondError(
	            array(
	                'error'        	=> true,
	                'errormessage' 	=> 'Try scanning '.$gtin.' again.', 
	                'data' 			=> array(
	                	'gtin' => $gtin,
	                ),
	            )
	        );
    	}
    	else{
    		return $this->respond(
	            array(
	                'error'        	=> false,
	                'errormessage' 	=> '', 
	                'data' 			=> $gtinRepository->getGtinInfo($gtin),
	            )
	        );
    	}
    }
   
}