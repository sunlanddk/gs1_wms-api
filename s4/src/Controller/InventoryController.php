<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InventoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class InventoryController extends ApiController {

    /**
    * @Route("/inventory/place/gtin", methods="POST")
    */
    public function placeGtin(
        EntityManagerInterface $em,
        InventoryRepository $inventoryRepository,
        Request $request
    ) {

    	$request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
	                'error'        	=> true,
	                'errormessage' 	=> 'Malformed post object', 
	                'data' 			=> '',
            	)
            );
        }
        $request = json_decode($request, true);

        $placedItems = $inventoryRepository->placeItemsAtPosition($request['items'], $request['position']);

        if($placedItems === true){
	        return $this->respond(
	            array(
	                'error'        	=> false,
	                'errormessage' 	=> '', 
	                'data' 			=> $placedItems,
	                'position' 		=> $inventoryRepository->getPositionName($request['position']),
	            )
	        );
	    }
	    else{
	    	return $this->respondError(
	            array(
	                'error'        	=> true,
	                'errormessage' 	=> $placedItems, 
	                'data' 			=> '',
	                'position' 			=> '',
	            )
	        );
	    }
    }
   
}