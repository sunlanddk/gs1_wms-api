<?php

namespace App\Repository;

use App\Entity\Itemoperations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Itemoperations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Itemoperations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Itemoperations[]    findAll()
 * @method Itemoperations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemOperationsRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Itemoperations::class);
        $this->_em = $em;
    }

    public function transform(Itemoperations $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'itemid'      		=> (int) $item->getItemid(),
            'parentid'      	=> (int) $item->getParentid(),
            'containerid'      	=> (int) $item->getContainerid(),
            'containertypeid'   => (int) $item->getContainertypeid(),
            'stockid'      		=> (int) $item->getStockid(),
            'workstationid'     => (int) $item->getWorkstationid(),
            'description'      	=> (string) $item->getDescription(),
        ];
    }

    public function createItemOperations($post){

    	$itemOp = new Itemoperations;
        $itemOp->setCreatedate(new \DateTime());
        $itemOp->setCreateuserid(1);
		$itemOp->setModifydate(new \DateTime());
        $itemOp->setModifyuserid(1);
        $itemOp->setActive(1);
        $itemOp->setItemid($post['itemid']);
        $itemOp->setParentid($post['parentid']);
        $itemOp->setContainerid($post['containerid']);
        $itemOp->setContainertypeid($post['containertypeid']);
        $itemOp->setStockid($post['stockid']);
        $itemOp->setWorkstationid($post['workstationid']);
        $itemOp->setDescription($post['description']);

        $this->_em->persist($itemOp);
        $this->_em->flush();

        return $itemOp->getId();
    }

    public function getDistinctCompanies($active = 1){
        $usersArray = array();
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.Active = :val')
            // ->andWhere('u.bc_company_id > 105461')
            ->setParameter('val', $active)
            ->groupBy('u.bc_company_id')
            ->getQuery()
            ->getResult();

        foreach ($guiduser as $user) {
            $usersArray[] = $this->transform($user);
        }

        return $usersArray;
    }

    public function isCompanyInDb($bccompanyid){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_company_id = :val')
            ->setParameter('val', $bccompanyid)
            ->getQuery()
            ->getResult();

        $inDB = false;
        foreach ($guiduser as $user) {
            $inDB = true;
        }

        return $inDB;
    }

    public function getRawSql(){
    	$conn = $this->_em->getConnection();
    	$sql = "SELECT * FROM article WHERE id > ?";
		$stmt = $conn->prepare($sql);
		$stmt->bindValue(1, 1);
		$stmt->execute();
		return $stmt->fetchAll();
    }

    public function getAll(){

        $records = array();
        $recordlogs =  $this->createQueryBuilder('i')
            ->orderBy('i.id', 'ASC')
            // ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;

        foreach ($recordlogs as $log) {
            $records[] = $this->transform($log);
        }


        return $records;
    }
 
}
