<?php

namespace App\Repository;

use App\Entity\Gtin;

// use App\Repository\ItemOperationsRepository;
use App\Repository\VariantcodeRepository;
use App\Repository\ArticleRepository;
use App\Repository\ItemRepository;
use App\Repository\ContainerRepository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;


class GtinRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;
    private $_itemOperationsRepo;
    private $_variantCodeRepo;
    private $_articleRepo;
    private $_itemRepo;
    private $_containerRepo;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em,
        ItemOperationsRepository $itemOperations,
        VariantcodeRepository $variantcodeRepository,
        ArticleRepository $articleRepository,
        ItemRepository $itemRepository,
        ContainerRepository $containerRepository
    )
    {
        parent::__construct($registry, Gtin::class);
        $this->_em = $em;
        $this->_itemOperationsRepo = $itemOperations;
        $this->_variantCodeRepo = $variantcodeRepository;
        $this->_articleRepo = $articleRepository;
        $this->_itemRepo = $itemRepository;
        $this->_containerRepo = $containerRepository;
    }



    public function test($gtin){
    	return $this->getGtinInfo($gtin);
    	// return $this->_variantCodeRepo->createVariantcode(1, '570923748');
    	return $this->_variantCodeRepo->findGtinByString('570923748');
    	return $this->_articleRepo->createArticle('570923748');
    	return $this->_itemOperationsRepo->getAll();
    	return 'asdadasdasdad asdasdad '.$gtin;
    }

    public function getGtinInfo($gtin){
    	$foundGtinData = $this->_variantCodeRepo->findGtinByString($gtin);
    	if($foundGtinData === false){
    		$articleId = $this->_articleRepo->createArticle($gtin);
    		$variantCodeId = $this->_variantCodeRepo->createVariantcode($articleId, $gtin);
    		$itemId = $this->_itemRepo->createItem($articleId, $variantCodeId);

    		return array(
    			'gtin' 			=> $gtin,
    			'id' 			=> $itemId,
    			'new' 			=> 1,
    			'position' 		=> '',
    			'attributes' 	=> [],
    		);
    	}
    	else{
    		$articleId = $foundGtinData['articleid'];
    		$variantCodeId = $foundGtinData['id'];
    		$new = $this->_itemRepo->newItem($articleId, $variantCodeId);
    		$item = $this->_itemRepo->getItemByArticleVariant($articleId, $variantCodeId);
    		$container = $this->_containerRepo->getContainerById($item['containerid']);

    		return array(
    			'gtin' 			=> $gtin,
    			'id' 			=> $item['id'],
    			'new' 			=> $new,
    			'position' 		=> $container['position'],
    			'attributes' 	=> [],
    		);
    	}

    	return true;
    }

    // public function transform(Itemoperations $item) {
    //     return [
    //         'id'                => (int) $item->getId(),
    //         'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
    //         'createuserid'      => (int) $item->getCreateuserid(),
    //         'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
    //         'modifyuserid'      => (int) $item->getCreateuserid(),
    //         'active'      		=> (int) $item->getActive(),
    //         'itemid'      		=> (int) $item->getItemid(),
    //         'parentid'      	=> (int) $item->getParentid(),
    //         'containerid'      	=> (int) $item->getContainerid(),
    //         'containertypeid'   => (int) $item->getContainertypeid(),
    //         'stockid'      		=> (int) $item->getStockid(),
    //         'workstationid'     => (int) $item->getWorkstationid(),
    //         'description'      	=> (string) $item->getDescription(),
    //     ];
    // }

  //   public function createItemOperations($post){

  //   	$itemOp = new Itemoperations;
  //       $itemOp->setCreatedate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
		// $itemOp->setModifydate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
  //       $itemOp->setActive(1);
  //       $itemOp->setItemid($post['itemid']);
  //       $itemOp->setParentid($post['parentid']);
  //       $itemOp->setContainerid($post['containerid']);
  //       $itemOp->setContainertypeid($post['containertypeid']);
  //       $itemOp->setStockid($post['stockid']);
  //       $itemOp->setWorkstationid($post['workstationid']);
  //       $itemOp->setDescription($post['description']);

  //       $this->_em->persist($itemOp);
  //       $this->_em->flush();

  //       return $itemOp->getId();
  //   }

  //   public function getDistinctCompanies($active = 1){
  //       $usersArray = array();
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.Active = :val')
  //           // ->andWhere('u.bc_company_id > 105461')
  //           ->setParameter('val', $active)
  //           ->groupBy('u.bc_company_id')
  //           ->getQuery()
  //           ->getResult();

  //       foreach ($guiduser as $user) {
  //           $usersArray[] = $this->transform($user);
  //       }

  //       return $usersArray;
  //   }

  //   public function isCompanyInDb($bccompanyid){
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.bc_company_id = :val')
  //           ->setParameter('val', $bccompanyid)
  //           ->getQuery()
  //           ->getResult();

  //       $inDB = false;
  //       foreach ($guiduser as $user) {
  //           $inDB = true;
  //       }

  //       return $inDB;
  //   }

  //   public function getRawSql(){
  //   	$conn = $this->_em->getConnection();
  //   	$sql = "SELECT * FROM article WHERE id > ?";
		// $stmt = $conn->prepare($sql);
		// $stmt->bindValue(1, 1);
		// $stmt->execute();
		// return $stmt->fetchAll();
  //   }

  //   public function getAll(){

  //       $records = array();
  //       $recordlogs =  $this->createQueryBuilder('i')
  //           ->orderBy('i.id', 'ASC')
  //           // ->setMaxResults(10)
  //           ->getQuery()
  //           ->getResult()
  //       ;

  //       foreach ($recordlogs as $log) {
  //           $records[] = $this->transform($log);
  //       }


  //       return $records;
  //   }
 
}
