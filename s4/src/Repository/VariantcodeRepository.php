<?php

namespace App\Repository;

use App\Entity\Variantcode;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Variantcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Variantcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Variantcode[]    findAll()
 * @method Variantcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariantcodeRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Variantcode::class);
        $this->_em = $em;
    }

    public function transform(Variantcode $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'type'      		=> (string) $item->getType(),
            'articleid'      	=> (int) $item->getArticleid(),
            'variantcode'      	=> (string) $item->getVariantcode(),
            'ownercompanyid'    => (string) $item->getOwnercompanyid(),
        ];
    }

    public function transformSimple(Variantcode $item) {
        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'variantcode'       => (string) $item->getVariantcode(),
        ];
    }

    public function findGtinByString($gtin){
        $variant = $this->findOneBy(array('variantcode' => $gtin));
        $variant = $this->findOneBy(array('variantcode' => (string)sprintf("%014d",$gtin), 'active' => 1));
        if($variant !== null){
            return $this->transformSimple($variant);
        }

        return false;

    }

    public function createVariantcode($articleId, $gtin){

    	$variant = new Variantcode;
        $variant->setCreatedate(new \DateTime());
        $variant->setCreateuserid(1);
		$variant->setModifydate(new \DateTime());
        $variant->setModifyuserid(1);
        $variant->setOwnercompanyid(787);
        $variant->setActive(1);
        $variant->setType('case');
        $variant->setArticleid((int)$articleId);
        $variant->setVariantcode((string)$gtin);
        $variant->setVariantcode((string)sprintf("%014d",$gtin));

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

}
