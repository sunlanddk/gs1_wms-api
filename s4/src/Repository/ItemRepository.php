<?php

namespace App\Repository;

use App\Entity\Item;

use App\Repository\ItemOperationsRepository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;
    private $_itemOperationsRepo;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em,
        ItemOperationsRepository $itemOperationsRepository
    )
    {
        parent::__construct($registry, Item::class);
        $this->_em = $em;
        $this->_itemOperationsRepo = $itemOperationsRepository;
    }

    public function transform(Item $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'parentid'      	=> (int) $item->getParentid(),
            'articleid'      	=> (int) $item->getArticleid(),
            'variantcodeid'     => (int) $item->getVariantcodeid(),
            'containerid'       => (int) $item->getContainerid(),
            'quantity'          => (int) $item->getQuantity(),
        ];
    }

    public function newItem($articleId, $variantCodeId){
        $item = $this->findOneBy(array('articleid' => $articleId, 'variantcodeid' => $variantCodeId, 'active' => 1));
        if($item !== null){
            if((int)$item->getContainerid() === 0){
                return 1;   
            }
            return 0;
        }
        return 1;
    }

    public function getItemByArticleVariant($articleId, $variantCodeId){
        $item = $this->findOneBy(array('articleid' => $articleId, 'variantcodeid' => $variantCodeId, 'active' => 1));
        if($item !== null){
            return $this->transformSimple($item);
        }
        
        $itemId = $this->createItem($articleId, $variantCodeId);
        $item = $this->find($itemId);
        return $this->transformSimple($item);
    }

    public function transformSimple(Item $item) {
        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'variantcodeid'     => (int) $item->getVariantcodeid(),
            'containerid'       => (int) $item->getContainerid(),
            'parentid'          => (int) $item->getParentid(),
        ];
    }

    public function placeItemAtPosition($jsItem, $position){
        $item = $this->find($jsItem['id']);
        $post = array(
            'itemid' => $item->getId(),
            'stockid' => $position['stockid'],
            'parentid' => 0,
            'description' => 'Place item from '. $jsItem['position'] .' to '.$position['position'],
            'containerid' => $position['id'],
            'workstationid' => 0,
            'containertypeid' => $position['containertypeid'],
        );

        $item->setContainerid($position['id']);
        $item->setModifydate(new \DateTime());
        if((int)$position['containertypeid'] === 44){
            $item->setActive(0);
        }

        $this->_em->persist($item);
        $this->_em->flush();

        $this->_itemOperationsRepo->createItemOperations($post);

        return true;
    }

    public function placeItemsAtPosition($items, $position){
        
        foreach ($items as $key => $jsItem) {
            $this->placeItemAtPosition($jsItem, $position);
        }

        return true;
    }

    public function createItem($articleId, $variantCodeId){

    	$item = new Item;
        $item->setCreatedate(new \DateTime());
        $item->setCreateuserid(1);
		$item->setModifydate(new \DateTime());
        $item->setModifyuserid(1);
        $item->setActive(1);
        $item->setParentid(0);
        $item->setArticleid((int)$articleId);
        $item->setVariantcodeid((int)$variantCodeId);
        $item->setComment((int)$variantCodeId);
        $item->setContainerid(0);
        $item->setQuantity(1);

        $this->_em->persist($item);
        $this->_em->flush();

        $post = array(
            'itemid' => $item->getId(),
            'stockid' => 0,
            'parentid' => 0,
            'description' => 'Initial scan',
            'containerid' => 0,
            'workstationid' => 0,
            'containertypeid' => 0,
        );

        $this->_itemOperationsRepo->createItemOperations($post);

        return $item->getId();
    }

}
