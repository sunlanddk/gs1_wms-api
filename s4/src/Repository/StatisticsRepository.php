<?php

namespace App\Repository;

use App\Entity\Statistics;
use App\Entity\Item;
use App\Entity\Container;

use App\Repository\ItemOperationsRepository;
use App\Repository\VariantcodeRepository;
use App\Repository\ArticleRepository;
use App\Repository\ItemRepository;
use App\Repository\ContainerRepository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;


class StatisticsRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;
    private $_itemOperationsRepo;
    private $_variantCodeRepo;
    private $_articleRepo;
    private $_itemRepo;
    private $_containerRepo;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em,
        ItemOperationsRepository $itemOperations,
        VariantcodeRepository $variantcodeRepository,
        ArticleRepository $articleRepository,
        ItemRepository $itemRepository,
        ContainerRepository $containerRepository
    )
    {
        parent::__construct($registry, Statistics::class, Item::class, Container::class);
        $this->_em = $em;
        $this->_itemOperationsRepo = $itemOperations;
        $this->_variantCodeRepo = $variantcodeRepository;
        $this->_articleRepo = $articleRepository;
        $this->_itemRepo = $itemRepository;
        $this->_containerRepo = $containerRepository;
    }
    public function transformSimpleItem($item) {
    // public function transformSimpleItem(Item $item) {
    	return $className = $this->_em->getClassMetadata(get_class($item))->getName();
    	return $item->getData();
        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'variantcodeid'     => (int) $item->getVariantcodeid(),
            'containerid'       => (int) $item->getContainerid(),
            'parentid'          => (int) $item->getParentid(),
        ];
    }

    public function deleteFalseScans(){
      /*
        SELECT count(i.Id), io.CreateDate
FROM (itemoperations io, item i)
WHERE io.Active=1 AND i.Id=io.ItemId AND i.Active=1
GROUP BY i.Id
HAVING count(i.Id) = 1 AND io.CreateDate < '2021-08-09 14:32:34'
      */
    }

    public function getExactGtinPosition(){
      // modtaget 2
      // køl exact 4,36,37,38,39,40
      // frys exact 5,41,42,43,44, 45
      // Exact 6,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35
      // Gentjek 14, 75, 76

      $ids = '2';
      $received = $this->getGtinsAtContainers($ids);

      $ids = '4,36,37,38,39,40,73';
      $fridge = $this->getGtinsAtContainers($ids);

      $ids = '5,41,42,43,44,45,74';
      $freezer = $this->getGtinsAtContainers($ids);

      $ids = '6,7,8,13,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,72,77,78,79,80,81,82,83,84,85,86';
      $exact = $this->getGtinsAtContainers($ids);

      $ids = '14,75,76';
      $checkAgain = $this->getGtinsAtContainers($ids);

      $totalFile = 'Modtaget;Koel exact;Frys exact;Exact;Gentjek'."\n\r";
      $keepGoing = true;
      $count = 0;
      while ($keepGoing === true) {
        $check = false;        

        $str = '';
        if(isset($received[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$received[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($fridge[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$fridge[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($freezer[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$freezer[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($exact[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$exact[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($checkAgain[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$checkAgain[$count]['gtin']).'';
        }
        else{
          $str .= '';
        }


        $count ++;
        if($check === false){
          $keepGoing = false;
        }
        $totalFile .= $str."\n\r";
      }

      return $totalFile;
    }

    private function getGtinsAtContainers($containerids){
      $conn = $this->_em->getConnection();
        $sql = sprintf("
            SELECT v.VariantCode as gtin
            FROM (item i, variantcode v)
            WHERE  i.ContainerId IN(%s) AND i.Active=1 AND v.ArticleId=i.ArticleId
          ",
          $containerids
        );
    
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

    public function getImageGtinPosition(){
      // image 9, 62,63, 64, 65, 66, 67, 68, 69, 70, 71
      // image køl 10
      // image frys 11
      // hos fotografen 12
      // ImageUpload 3

      $ids = '9,62,63,64,65,66,67,68,69,70,71';
      $image = $this->getGtinsAtContainers($ids);

      $ids = '10';
      $fridge = $this->getGtinsAtContainers($ids);

      $ids = '11';
      $freezer = $this->getGtinsAtContainers($ids);

      $ids = '12';
      $foto = $this->getGtinsAtContainers($ids);

      $ids = '3';
      $upload = $this->getGtinsAtContainers($ids);

      $totalFile = 'Image;Koel Image;Frys Image;Hos fotografen;ImageUpload'."\n\r";
      $keepGoing = true;
      $count = 0;
      while ($keepGoing === true) {
        $check = false;        

        $str = '';
        if(isset($image[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$image[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($fridge[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$fridge[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($freezer[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$freezer[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($foto[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$foto[$count]['gtin']).';';
        }
        else{
          $str .= ';';
        }

        if(isset($upload[$count]) === true){
          $check = true;
          $str .= (string)sprintf("%014d",$upload[$count]['gtin']).'';
        }
        else{
          $str .= '';
        }


        $count ++;
        if($check === false){
          $keepGoing = false;
        }
        $totalFile .= $str."\n\r";
      }

      return $totalFile;
    }

    public function checkPostedData($post){
    	if(isset($post['active']) === true){
    		if($post['active'] !== true && $post['active'] !== false && $post['active'] !== 'all' ){
    			return 'Error: posted param [active] can only be (bool)true/(bool)false/(string)all, type "'.getType($post['active']).'" with value "'.$post['active'].'" was posted';
    		}
    	}

    	if(isset($post['size']) === true){
    		if(getType($post['size']) !== 'integer'){
    			return 'Error: posted param [size] can only be (int)1 to (int)5000, type "'.getType($post['size']).'" with value "'.$post['size'].'" was posted';
    		}
    	}

    	if(isset($post['page']) === true){
    		if(getType($post['page']) !== 'integer'){
    			return 'Error: posted param [page] can only be (int), type "'.getType($post['page']).'" with value "'.$post['page'].'" was posted';
    		}
    	}

    	if(isset($post['fromdate']) === true){
    		if(isset($post['todate']) === false){
    			return 'Error: Both fromdate and todate needs to be set';
    		}
    		$dt = \DateTime::createFromFormat("Y-m-d H:i:s", $post['fromdate']);
			$dt !== false && !array_sum($dt::getLastErrors());

    		if($dt === false){
    			return 'Error: posted param [fromdate] should look like "2021-01-01 00:00:00", type "'.getType($post['fromdate']).'" with value "'.$post['fromdate'].'" was posted';
    		}
    	}

    	if(isset($post['todate']) === true){
    		if(isset($post['fromdate']) === false){
    			return 'Error: Both fromdate and todate needs to be set';
    		}
    		
    		$dt = \DateTime::createFromFormat("Y-m-d H:i:s", $post['todate']);
			$dt !== false && !array_sum($dt::getLastErrors());

    		if($dt === false){
    			return 'Error: posted param [todate] should look like "2021-01-01 00:00:00", type "'.getType($post['todate']).'" with value "'.$post['todate'].'" was posted';
    		}
    	}

    	return true;
    }

    public function getItemStatistics($post){
    	$active = (isset($post['active']) === true ? $post['active'] : 'all');
    	$fromdate = (isset($post['fromdate']) === true ? $post['fromdate'] : '');
    	$todate = (isset($post['todate']) === true ? $post['todate'] : '');
    	$page = (isset($post['page']) === true ? (int)$post['page'] : 1);
    	$size = (isset($post['size']) === true ? (int)$post['size'] : 1000);
    	if(isset($post['todate']) === true ){
    		return $this->getItemsWithDate($active, $fromdate, $todate, $page, $size);
    	}
    	else{
    		return $this->getItemsWithNoDate($active, $page, $size);
    	}
    }

    private function processItems($fetchAll){
    	$items = [];
    	// foreach ($fetchAll as $key => $row) {
    	// 	if(isset($items[$row['id']]) == true){
    	// 		$items[$row['id']]['operations'][] = array(
    	// 			'stock'		=> (int)$row['stock'],
    	// 			'position' 	=> (string)$row['position'],
    	// 			'name'		=> (string)$row['name'],
    	// 			'operation'	=> (string)$row['operation'],
    	// 			'date'		=> (string)$row['date'],
    	// 		);
    	// 	}
    	// 	else{
    	// 		$items[$row['id']] = array(
    	// 			'gtin'			=> (string)$row['gtin'],
    	// 			'active'		=> (bool)$row['active'],
    	// 			'receivedate' 	=> (string)$row['receivedate'],
    	// 			'lastchange' 	=> (string)$row['lastchange'],
    	// 			'operations'	=> []
    	// 		);
    	// 		$items[$row['id']]['operations'][] = array(
    	// 			'stock'		=> (int)$row['stock'],
    	// 			'position' 	=> (string)$row['position'],
    	// 			'name'		=> (string)$row['name'],
    	// 			'operation'	=> (string)$row['operation'],
    	// 			'date'		=> (string)$row['date'],
    	// 		);
    	// 	}
    	// }
    	
    	$tempId = 0;
    	$tempItem = [];
    	foreach ($fetchAll as $key => $row) {

        $tempPosition = substr_replace($row['position'], '(', 0, 0);
        $tempPosition = substr_replace($tempPosition, ')', 3, 0);

        if($tempPosition === '()'){
            $tempPosition = 'No position yet.';
        }

    		if($tempId !== $row['id']){
    			if($tempId !== 0){
    				array_push($items, $tempItem);
    			}

    			$tempItem = [];
    			$tempId = $row['id'];
    			$tempItem = array(
    				'gtin'			=> (string)sprintf("%014d", $row['gtin']),
    				'active'		=> (bool)$row['active'],
    				'receivedate' 	=> (string)$row['receivedate'],
    				'lastchange' 	=> (string)$row['lastchange'],
    				'operations'	=> [],
    			);
    			$tempItem['operations'][] = array(
    				'stock'		=> (int)$row['stock'],
    				'position' 	=> (string)utf8_encode($tempPosition),
    				'name'		=> (string)utf8_encode($row['name']),
    				'operation'	=> (string)utf8_encode($row['operation']),
    				'date'		=> (string)$row['date'],
            'catid' => (int)$row['categoryid'],
            'timespent' => 0,
    			);

    		}else{
    			$tempItem['operations'][] = array(
    				'stock'		=> (int)$row['stock'],
    				'position' 	=> (string)$tempPosition,
    				'name'		=> (string)utf8_encode($row['name']),
    				'operation'	=> (string)utf8_encode($row['operation']),
    				'date'		=> (string)$row['date'],
            'catid' => (int)$row['categoryid'],
            'timespent' => 0,
    			);
    		}
    	}
    	array_push($items, $tempItem);

      foreach ($items as $ikey => $item) {
        foreach ($item['operations'] as $key => $operation) {
          if(isset($items[$ikey]['operations'][$key+1]) === true){
            $items[$ikey]['operations'][$key]['timespent'] = (strtotime($items[$ikey]['operations'][$key+1]['date']) - strtotime($items[$ikey]['operations'][$key]['date']));
          }
        }
      }
    	return $items;
    }


    private function getItemsWithNoDate($active, $page, $size){
    	$offset = (($page - 1) * $size);
		  $conn = $this->_em->getConnection();
		  if($active === 'all'){
  			$sql = sprintf("
  					SELECT 
              i.Id as id, 
              i.Active as active,
              v.VariantCode as gtin, 
              i.CreateDate as receivedate, 
              i.ModifyDate as lastchange, 
              op.Id as opId, 
              op.StockId as stock,
              op.CreateDate as date, 
              op.Description as operation,
              c.Description as name, 
              c.Position as position,
              c.CategoryId as categoryid

  					FROM ((select * from item LIMIT %d OFFSET %d) as i, variantcode v, itemoperations op)
            LEFT JOIN container c ON c.Id=op.ContainerId
  					WHERE v.Id=i.VariantCodeId AND v.Active=1 AND op.ItemId=i.Id AND op.Active=1
            ORDER BY i.Id, op.Id ASC
  				",
  				$size,
  				$offset
  			);
        $totalPagesSql = sprintf("
					SELECT COUNT(*) as totalpages
					FROM (item i)
				"); 
		  }
		  else{
        $sql = sprintf("
  					SELECT 
              i.Id as id, 
              i.Active as active,
              v.VariantCode as gtin, 
              i.CreateDate as receivedate, 
              i.ModifyDate as lastchange, 
              op.Id as opId, 
              op.StockId as stock,
              op.CreateDate as date, 
              op.Description as operation,
              c.Description as name, 
              c.Position as position,
              c.CategoryId as categoryid

  					FROM ((select * from item WHERE Active=%d LIMIT %d OFFSET %d) as i, variantcode v, itemoperations op)
            LEFT JOIN container c ON c.Id=op.ContainerId
  					WHERE v.Id=i.VariantCodeId AND v.Active=1 AND op.ItemId=i.Id AND op.Active=1
            ORDER BY i.Id, op.Id ASC
  				",
  				$active,
  				$size,
  				$offset
        );
        $totalPagesSql = sprintf("
					SELECT COUNT(*) as totalpages
					FROM (item i)
					WHERE i.Active=%d
				  ",
				  $active
        ); 
		  }
		
		  $stmt = $conn->prepare($sql);
		  $stmt->execute();

		  $Totalstmt = $conn->prepare($totalPagesSql);
		  $Totalstmt->execute();
		  $totalPagesSql = $Totalstmt->fetch();
		
		  return array(
        'page' 			=> $page,
        'size' 			=> $size,
        'totalpages' 	=> ceil($totalPagesSql['totalpages'] / $size),
        'items' 		=> $this->processItems($stmt->fetchAll()),
		  );
    }

    private function getItemsWithDate($active, $fromDate, $toDate, $page, $size){
    	$offset = (($page - 1) * $size);
		$conn = $this->_em->getConnection();
		if($active === 'all'){
			$sql = sprintf("
					SELECT 
						i.Id as id, 
						i.Active as active,
						v.VariantCode as gtin, 
						i.CreateDate as receivedate, 
						i.ModifyDate as lastchange, 
						op.Id as opId, 
						op.StockId as stock,
						op.CreateDate as date, 
						op.Description as operation,
						c.Description as name, 
						c.Position as position,
            c.CategoryId as categoryid

					FROM ((select * from item LIMIT %d OFFSET %d) as i, variantcode v, itemoperations op)
					LEFT JOIN container c ON c.Id=op.ContainerId
					WHERE v.Id=i.VariantCodeId AND v.Active=1 AND op.ItemId=i.Id AND op.Active=1 AND i.CreateDate > '%s' AND op.CreateDate < '%s'
					ORDER BY i.Id, op.Id ASC
				",
				$size,
				$offset,
				$fromDate, 
				$toDate
			);
			$totalPagesSql = sprintf("
				SELECT COUNT(*) as totalpages
				FROM (item i)
			"); 
		}
		else{
			$sql = sprintf("
					SELECT 
						i.Id as id, 
						i.Active as active,
						v.VariantCode as gtin, 
						i.CreateDate as receivedate, 
						i.ModifyDate as lastchange, 
						op.Id as opId, 
						op.StockId as stock,
						op.CreateDate as date, 
						op.Description as operation,
						c.Description as name, 
						c.Position as position,
            c.CategoryId as categoryid

					FROM ((select * from item WHERE item.Active=%d LIMIT %d OFFSET %d) as i, variantcode v, itemoperations op)
					LEFT JOIN container c ON c.Id=op.ContainerId
					WHERE v.Id=i.VariantCodeId AND v.Active=1 AND op.ItemId=i.Id AND op.Active=1 AND i.CreateDate > '%s' AND op.CreateDate < '%s'
					ORDER BY i.Id, op.Id ASC
				",
				$active,
				$size,
				$offset,
				$fromDate, 
				$toDate
			);
			$totalPagesSql = sprintf("
					SELECT COUNT(*) as totalpages
					FROM (item i)
					WHERE i.Active=%d AND i.CreateDate > '%s'
				",
				$active,
				$fromDate
			); 
		}
		$stmt = $conn->prepare($sql);
		$stmt->execute();

		$Totalstmt = $conn->prepare($totalPagesSql);
		$Totalstmt->execute();
		$totalPagesSql = $Totalstmt->fetch();

		return array(
			'page' 			=> $page,
			'size' 			=> $size,
			'totalpages' 	=> ceil($totalPagesSql['totalpages'] / $size),
			'items' 		=> $this->processItems($stmt->fetchAll()),
		);
	}

    // public function getDistinctCompanies(){
    //      $queryBuilder = $this->_em->createQueryBuilder();       
	   //  $queryBuilder->addSelect("item,container")
	   //      ->from("App\Entity\Item", 'item')
	   //      ->from("App\Entity\Container", 'container')
	   //      ->andWhere("item.active = :active")
	   //      ->andWhere("container.id = item.containerid")
	   //      ->setParameter("active",1);

	   //  return $queryBuilder->getQuery()->getResult();
    // }

 //    public function getRawSql(){
	// 	$conn = $this->_em->getConnection();
	// 	$sql = "SELECT * FROM article WHERE id > ?";
	// 	$stmt = $conn->prepare($sql);
	// 	$stmt->bindValue(1, 1);
	// 	$stmt->execute();
	// 	return $stmt->fetchAll();
	// }

    // public function placeItemsAtPosition($items, $position){
    // 	$position = $this->_containerRepo->getContainerByPosition($position);
    // 	if($position === false){
    // 		return 'Position not found.';
    // 	}

    // 	$readyToPlace = true;
    // 	$notPlacedBefore = true;
    // 	$alreadyPlaced = false;
    // 	foreach ($items as $key => $value) {
    // 		if((int)$value['id'] == 0){
    // 			$readyToPlace = $value['gtin'];
    // 		}
    // 		if($value['position'] == ''){
    // 			$notPlacedBefore = $value['gtin'];
    // 		}
    // 		if($value['position'] !== ''){
    // 			$alreadyPlaced = $value['gtin'];
    // 		}
    // 	}

    // 	if($readyToPlace !== true){
    // 		return $readyToPlace.' cannot be placed. Please try again.';
    // 	}

    // 	if($notPlacedBefore !== true && (int)$position['containertypeid'] !== 43){
    // 		return $notPlacedBefore.' needs to be placed at a start position before continuing.';
    // 	}

    // 	if($alreadyPlaced !== false && (int)$position['containertypeid'] === 43){
    // 		return $alreadyPlaced.' cannot be placed at start position again, please place at another position.';	
    // 	}

    // 	return $this->_itemRepo->placeItemsAtPosition($items, $position);
    // }

    // public function transform(Itemoperations $item) {
    //     return [
    //         'id'                => (int) $item->getId(),
    //         'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
    //         'createuserid'      => (int) $item->getCreateuserid(),
    //         'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
    //         'modifyuserid'      => (int) $item->getCreateuserid(),
    //         'active'      		=> (int) $item->getActive(),
    //         'itemid'      		=> (int) $item->getItemid(),
    //         'parentid'      	=> (int) $item->getParentid(),
    //         'containerid'      	=> (int) $item->getContainerid(),
    //         'containertypeid'   => (int) $item->getContainertypeid(),
    //         'stockid'      		=> (int) $item->getStockid(),
    //         'workstationid'     => (int) $item->getWorkstationid(),
    //         'description'      	=> (string) $item->getDescription(),
    //     ];
    // }

  //   public function createItemOperations($post){

  //   	$itemOp = new Itemoperations;
  //       $itemOp->setCreatedate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
		// $itemOp->setModifydate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
  //       $itemOp->setActive(1);
  //       $itemOp->setItemid($post['itemid']);
  //       $itemOp->setParentid($post['parentid']);
  //       $itemOp->setContainerid($post['containerid']);
  //       $itemOp->setContainertypeid($post['containertypeid']);
  //       $itemOp->setStockid($post['stockid']);
  //       $itemOp->setWorkstationid($post['workstationid']);
  //       $itemOp->setDescription($post['description']);

  //       $this->_em->persist($itemOp);
  //       $this->_em->flush();

  //       return $itemOp->getId();
  //   }

  //   public function getDistinctCompanies($active = 1){
  //       $usersArray = array();
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.Active = :val')
  //           // ->andWhere('u.bc_company_id > 105461')
  //           ->setParameter('val', $active)
  //           ->groupBy('u.bc_company_id')
  //           ->getQuery()
  //           ->getResult();

  //       foreach ($guiduser as $user) {
  //           $usersArray[] = $this->transform($user);
  //       }

  //       return $usersArray;
  //   }

  //   public function isCompanyInDb($bccompanyid){
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.bc_company_id = :val')
  //           ->setParameter('val', $bccompanyid)
  //           ->getQuery()
  //           ->getResult();

  //       $inDB = false;
  //       foreach ($guiduser as $user) {
  //           $inDB = true;
  //       }

  //       return $inDB;
  //   }

  //   public function getRawSql(){
  //   	$conn = $this->_em->getConnection();
  //   	$sql = "SELECT * FROM article WHERE id > ?";
		// $stmt = $conn->prepare($sql);
		// $stmt->bindValue(1, 1);
		// $stmt->execute();
		// return $stmt->fetchAll();
  //   }

  //   public function getAll(){

  //       $records = array();
  //       $recordlogs =  $this->createQueryBuilder('i')
  //           ->orderBy('i.id', 'ASC')
  //           // ->setMaxResults(10)
  //           ->getQuery()
  //           ->getResult()
  //       ;

  //       foreach ($recordlogs as $log) {
  //           $records[] = $this->transform($log);
  //       }


  //       return $records;
  //   }
 
}
