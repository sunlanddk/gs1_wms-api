<?php


namespace App\Repository;


class AuthRepository {

    public function checkToken($token, $continue = 1) {
        // $tokenArray = explode(" ", $token);
        // $token = $tokenArray[1];
        $cipher = "AES256";
        $key = '7849793793';
        $iv = '6786387682736253';
        $decryptedToken = openssl_decrypt($token, $cipher, $key, $options=0, $iv);

        // $decryptedToken = str_replace(array('/','?'), '',$decryptedToken);
        
        if(strlen($decryptedToken) === 28) {
            $decryptedTokenArray = explode("_", $decryptedToken);
            return $decryptedTokenArray;
        } else {
            if($continue === 1){
                return $this->checkToken(base64_decode($token), 0);
            }
            return "error";
        }
    }
}