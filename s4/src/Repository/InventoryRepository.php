<?php

namespace App\Repository;

use App\Entity\Inventory;

// use App\Repository\ItemOperationsRepository;
use App\Repository\VariantcodeRepository;
use App\Repository\ArticleRepository;
use App\Repository\ItemRepository;
use App\Repository\ContainerRepository;
use App\Repository\GtinRepository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManagerInterface;


class InventoryRepository extends ServiceEntityRepository
{   
    protected $_em;
    private $_logger;
    private $_itemOperationsRepo;
    private $_variantCodeRepo;
    private $_articleRepo;
    private $_itemRepo;
    private $_containerRepo;
    private $_gtinRepository;

    public function __construct(
        RegistryInterface $registry, 
        EntityManagerInterface $em,
        ItemOperationsRepository $itemOperations,
        VariantcodeRepository $variantcodeRepository,
        ArticleRepository $articleRepository,
        ItemRepository $itemRepository,
        ContainerRepository $containerRepository,
        GtinRepository $gtinRepository
    )
    {
        parent::__construct($registry, Inventory::class);
        $this->_em = $em;
        $this->_itemOperationsRepo = $itemOperations;
        $this->_variantCodeRepo = $variantcodeRepository;
        $this->_articleRepo = $articleRepository;
        $this->_itemRepo = $itemRepository;
        $this->_containerRepo = $containerRepository;
        $this->_gtinRepository = $gtinRepository;
    }

    public function getPositionName($positionstr){
      $position = $this->_containerRepo->getContainerByPosition($positionstr);
      $newpos = substr_replace($positionstr, '(', 0, 0);
      $newpos = substr_replace($newpos, ')', 3, 0);
      return '<br/> '.utf8_encode($position['name']) .' <br/> '. $newpos;
    }

    public function placeItemsAtPosition($items, $position){
      // UPDATE `stocklayout` SET `Position` = concat('91000',`Position`);
      // UPDATE `container` SET `Position` = concat('91000',`Position`);

    	$position = $this->_containerRepo->getContainerByPosition($position);
    	if($position === false){
    		return 'Position not found.';
    	}

    	$readyToPlace = true;
    	$notPlacedBefore = true;
    	$alreadyPlaced = false;
    	foreach ($items as $key => $value) {
    		if((int)$value['id'] == 0){
    			$readyToPlace = $value['gtin'];
    		}
    		if($value['position'] == ''){
          $tempItem = $this->_gtinRepository->getGtinInfo($value['gtin']);
          if($tempItem['position'] == ''){
            $notPlacedBefore = $value['gtin'];
          }
    		}
    		if($value['position'] !== ''){
    			$alreadyPlaced = $value['gtin'];
    		}
    	}

    	if($readyToPlace !== true){
    		return $readyToPlace.' cannot be placed. Please try again.';
    	}

    	if($notPlacedBefore !== true && (int)$position['containertypeid'] !== 43){
    		return $notPlacedBefore.' needs to be placed at a start position before continuing.';
    	}

    	if($alreadyPlaced !== false && (int)$position['containertypeid'] === 43){
    		return $alreadyPlaced.' cannot be placed at start position again, please place at another position.';	
    	}
      // $position = substr_replace($position, '(', 0, 0);
      // $position = substr_replace($position, ')', 3, 0);
    	return $this->_itemRepo->placeItemsAtPosition($items, $position);
    }

    // public function transform(Itemoperations $item) {
    //     return [
    //         'id'                => (int) $item->getId(),
    //         'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
    //         'createuserid'      => (int) $item->getCreateuserid(),
    //         'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
    //         'modifyuserid'      => (int) $item->getCreateuserid(),
    //         'active'      		=> (int) $item->getActive(),
    //         'itemid'      		=> (int) $item->getItemid(),
    //         'parentid'      	=> (int) $item->getParentid(),
    //         'containerid'      	=> (int) $item->getContainerid(),
    //         'containertypeid'   => (int) $item->getContainertypeid(),
    //         'stockid'      		=> (int) $item->getStockid(),
    //         'workstationid'     => (int) $item->getWorkstationid(),
    //         'description'      	=> (string) $item->getDescription(),
    //     ];
    // }

  //   public function createItemOperations($post){

  //   	$itemOp = new Itemoperations;
  //       $itemOp->setCreatedate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
		// $itemOp->setModifydate(new \DateTime());
  //       $itemOp->setCreateuserid(1);
  //       $itemOp->setActive(1);
  //       $itemOp->setItemid($post['itemid']);
  //       $itemOp->setParentid($post['parentid']);
  //       $itemOp->setContainerid($post['containerid']);
  //       $itemOp->setContainertypeid($post['containertypeid']);
  //       $itemOp->setStockid($post['stockid']);
  //       $itemOp->setWorkstationid($post['workstationid']);
  //       $itemOp->setDescription($post['description']);

  //       $this->_em->persist($itemOp);
  //       $this->_em->flush();

  //       return $itemOp->getId();
  //   }

  //   public function getDistinctCompanies($active = 1){
  //       $usersArray = array();
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.Active = :val')
  //           // ->andWhere('u.bc_company_id > 105461')
  //           ->setParameter('val', $active)
  //           ->groupBy('u.bc_company_id')
  //           ->getQuery()
  //           ->getResult();

  //       foreach ($guiduser as $user) {
  //           $usersArray[] = $this->transform($user);
  //       }

  //       return $usersArray;
  //   }

  //   public function isCompanyInDb($bccompanyid){
  //       $guiduser = $this->createQueryBuilder('u')
  //           ->andWhere('u.bc_company_id = :val')
  //           ->setParameter('val', $bccompanyid)
  //           ->getQuery()
  //           ->getResult();

  //       $inDB = false;
  //       foreach ($guiduser as $user) {
  //           $inDB = true;
  //       }

  //       return $inDB;
  //   }

  //   public function getRawSql(){
  //   	$conn = $this->_em->getConnection();
  //   	$sql = "SELECT * FROM article WHERE id > ?";
		// $stmt = $conn->prepare($sql);
		// $stmt->bindValue(1, 1);
		// $stmt->execute();
		// return $stmt->fetchAll();
  //   }

  //   public function getAll(){

  //       $records = array();
  //       $recordlogs =  $this->createQueryBuilder('i')
  //           ->orderBy('i.id', 'ASC')
  //           // ->setMaxResults(10)
  //           ->getQuery()
  //           ->getResult()
  //       ;

  //       foreach ($recordlogs as $log) {
  //           $records[] = $this->transform($log);
  //       }


  //       return $records;
  //   }
 
}
