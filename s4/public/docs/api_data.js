define({ "api": [
  {
    "type": "post",
    "url": "/statistics/gtin",
    "title": "GTIN Statistics POST",
    "name": "GtinStatisticsPOST",
    "description": "Get GTIN operation statistics.",
    "group": "GTIN",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String / Boolean",
            "optional": true,
            "field": "active",
            "allowedValues": [
              "all",
              true,
              false
            ],
            "description": "<p></p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "size",
            "size": "1-5000",  
            "description": "<p>Max number of GTIN returned</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Page to show</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fromdate",
            "description": "<p class='type-size'>Allowed format: <code>2020-01-01 00:00:00</code></p><p class='type-size'>Only allowed with: <code>todate</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "todate",
            "description": "<p class='type-size'>Allowed format: <code>2020-01-01 00:00:00</code></p><p class='type-size'>Only allowed with: <code>fromdate</code></p>"
          },
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "error",
            "description": "<p>Did an error occur</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errormessage",
            "description": "<p>Error message</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.size",
            "description": "<p>Posted size</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.page",
            "description": "<p>Current page</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.totalpages",
            "description": "<p>Total number of pages</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.items",
            "description": "<p>List of items</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.gtin",
            "description": "<p>GTIN</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.items.active",
            "allowedValues": [
              true,
              false
            ],
            "description": "<p>Is GTIN active</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.receivedate",
            "description": "<p>First interaction date</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.lastchange",
            "description": "<p>Last interaction date</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.items.operations",
            "description": "<p>List of operations</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.stock",
            "description": "<p>Warehouse location id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.name",
            "description": "<p>Position name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.operation",
            "description": "<p>Operation description</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.catid",
            "description": "<p>Category Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.timespent",
            "description": "<p>Time spent at position in seconds</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.date",
            "description": "<p>Date for operation</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          //"content": "\tHTTP/1.1 200 OK\n\t{\n      \"error\": false,\n      \"errormessage\": \"\"\n\t}",
          "content": "\tHTTP/1.1 200 OK\n\t{\n\t\t\"error\":false,\n\t\t\"errormessage\":\"\",\n\t\t\"data\":{\n\t\t\t\"page\":1,\n\t\t\t\"size\":1000,\n\t\t\t\"totalpages\":1,\n\t\t\t\"items\":[\n\t\t\t\t{\n\t\t\t\t\t\"gtin\":\"1234567890123\",\n\t\t\t\t\t\"active\":false,\n\t\t\t\t\t\"receivedate\":\"2020-01-01 00:00:00\",\n\t\t\t\t\t\"lastchange\":\"2020-01-01 00:00:00\",\n\t\t\t\t\t\"operations\":[\n\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\"stock\":0,\n\t\t\t\t\t\t\t\"position\":\"\",\n\t\t\t\t\t\t\t\"name\":\"\",\n\t\t\t\t\t\t\t\"operation\":\"Initial scan\",\n\t\t\t\t\t\t\t\"catid\": 10,\n\t\t\t\t\t\t\t\"timespent\": 60,\n\t\t\t\t\t\t\t\"date\":\"2020-01-01 00:00:00\"\n\t\t\t\t\t\t}\n\t\t\t\t\t]\n\t\t\t\t}\n\t\t\t]\n\t\t}\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx / 5xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "type": "Boolean",
            "field": "error",
            "description": "<p>Did an error occur.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "type": "String",
            "field": "errormessage",
            "description": "<p>Error Message</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p> </p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\tHTTP/1.1 4xx / 5xx \n\t{\n      \"error\": false,\n      \"errormessage\": \"Error message here.\"\n      \"data\": []   \n\t}",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "filename": "wmsapp/GetAvailablePosition.js",
    "groupTitle": "Gtin"
  },
  {
    "type": "get",
    "url": "/statistics/gtin?active=all&size=1&page=1&fromdate=2021-01-01%2000%3A00%3A00&todate=2021-10-01%2023%3A59%3A59",
    "title": "GTIN Statistics GET",
    "name": "GtinStatisticsGET",
    "description": "Get GTIN operation statistics.",
    "group": "GTIN",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String / Boolean",
            "optional": true,
            "field": "active",
            "allowedValues": [
              "all",
              true,
              false
            ],
            "description": "<p></p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "size",
            "size": "1-5000",  
            "description": "<p>Max number of GTIN returned</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Page to show</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "fromdate",
            "description": "<p class='type-size'>Allowed format: <code>2020-01-01 00:00:00</code></p><p class='type-size'>Only allowed with: <code>todate</code></p><p class='type-size'><code>utf8_encoded</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "todate",
            "description": "<p class='type-size'>Allowed format: <code>2020-01-01 00:00:00</code></p><p class='type-size'>Only allowed with: <code>fromdate</code></p><p class='type-size'><code>utf8_encoded</code></p>"
          },
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "error",
            "description": "<p>Did an error occur</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errormessage",
            "description": "<p>Error message</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p></p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.size",
            "description": "<p>Posted size</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.page",
            "description": "<p>Current page</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.totalpages",
            "description": "<p>Total number of pages</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.items",
            "description": "<p>List of items</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.gtin",
            "description": "<p>GTIN</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.items.active",
            "allowedValues": [
              true,
              false
            ],
            "description": "<p>Is GTIN active</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.receivedate",
            "description": "<p>First interaction date</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.lastchange",
            "description": "<p>Last interaction date</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.items.operations",
            "description": "<p>List of operations</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.stock",
            "description": "<p>Warehouse location id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.name",
            "description": "<p>Position name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.operation",
            "description": "<p>Operation description</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.catid",
            "description": "<p>Category Id</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data.items.operations.timespent",
            "description": "<p>Time spent at position in seconds</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.items.operations.date",
            "description": "<p>Date for operation</p><p class='type-size'>Format: <code>2020-01-01 00:00:00</code></p>"
          },
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          //"content": "\tHTTP/1.1 200 OK\n\t{\n      \"error\": false,\n      \"errormessage\": \"\"\n\t}",
          "content": "\tHTTP/1.1 200 OK\n\t{\n\t\t\"error\":false,\n\t\t\"errormessage\":\"\",\n\t\t\"data\":{\n\t\t\t\"page\":1,\n\t\t\t\"size\":1000,\n\t\t\t\"totalpages\":1,\n\t\t\t\"items\":[\n\t\t\t\t{\n\t\t\t\t\t\"gtin\":\"1234567890123\",\n\t\t\t\t\t\"active\":false,\n\t\t\t\t\t\"receivedate\":\"2020-01-01 00:00:00\",\n\t\t\t\t\t\"lastchange\":\"2020-01-01 00:00:00\",\n\t\t\t\t\t\"operations\":[\n\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\"stock\":0,\n\t\t\t\t\t\t\t\"position\":\"\",\n\t\t\t\t\t\t\t\"name\":\"\",\n\t\t\t\t\t\t\t\"operation\":\"Initial scan\",\n\t\t\t\t\t\t\t\"catid\": 10,\n\t\t\t\t\t\t\t\"timespent\": 60,\n\t\t\t\t\t\t\t\"date\":\"2020-01-01 00:00:00\"\n\t\t\t\t\t\t}\n\t\t\t\t\t]\n\t\t\t\t}\n\t\t\t]\n\t\t}\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx / 5xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "type": "Boolean",
            "field": "error",
            "description": "<p>Did an error occur.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "type": "String",
            "field": "errormessage",
            "description": "<p>Error Message</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p> </p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "\tHTTP/1.1 4xx / 5xx \n\t{\n      \"error\": false,\n      \"errormessage\": \"Error message here.\"\n      \"data\": []   \n\t}",
          "type": "json"
        }
      ]
    },
    "version": "1.0.0",
    "filename": "wmsapp/GetAvailablePosition.js",
    "groupTitle": "Gtin"
  }  
] });
