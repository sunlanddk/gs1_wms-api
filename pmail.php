<?php
   ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . 'PEAR');

   require_once "lib/config.inc";
   require_once "PEAR/Mail.php";
   require_once "PEAR/Mail/mime.php";

   if (isset($_POST['subject']) && isset($_POST['to'])) {

      // MIME encode
      $headers = array();

      $mime = new Mail_mime();
      $mime->setFrom($Config['SMTPFrom']);
      $mime->setSubject($_POST['subject']);
      $mime->setTXTBody("\r\n"."Use HTML-mode to view this E-mail");
      $mime->setHTMLBody("\r\n".$_POST['body']);

      $body    = $mime->get();
      $headers = $mime->headers($headers);

      // Send E-mail
      $params = array();
      switch ($Config["mailDriver"]) {
         case "smtp":
            $params['host']     = $Config["SMTPHost"];
            $params["port"]     = $Config['SMTPPort'];
            $params["auth"]     = $Config['SMTPAuth'];
            $params["username"] = $Config['SMTPFrom'];
            $params["password"] = $Config['SMTPPasswd'];
            $params["localhost"]= $Config["SMTPHost"];
            break;
      }

      $mail = & Mail::factory($Config["mailDriver"], $params);
      if (PEAR::isError($mail)) {
         print_r("Error: " . $mail);
      }
      $res = $mail->send($_POST['to'], $headers, $body);
      if (PEAR::isError($res)) {
         print_r("Error: " . $res);
      } else {
         print_r("Mail sent.");
         print_r($res);
      }

   }
?>


<form method="post" action="pmail.php">
   <table>
      <tr>
         <td>To:</td>
         <td><input type="text" name="to"></td>
      </tr>
      <tr>
         <td>Subject:</td>
         <td><input type="text" name="subject"></td>
      </tr>
      <tr>
         <td>Body:</td>
         <td><textarea name="body" cols="100" rows="20"></textarea></td>
      </tr>
      <tr>
         <td colspan="2"><input type="submit" name="Send" value="Send"></td>
      </tr>
   </table>
</form>
