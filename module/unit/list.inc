<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 60) ;
    listHead ('Description') ;
    listHead ('Decimals', 80, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ((int)$row['Decimals'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Units', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
