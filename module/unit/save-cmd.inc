<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'Decimals'		=> array ('type' => 'integer',	'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM Unit WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Unit allready existing' ;
		return true ;

	    case 'Decimals' :
		if (!$changed) return false ;
		if ($value < 0 or $value > 3) return 'invalid number of digits' ;
		return true ;
	}
	return false ;
    }
     
    return saveFields ('Unit', $Id, $fields) ;
?>
