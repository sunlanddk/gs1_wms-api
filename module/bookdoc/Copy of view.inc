<?php


    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    
    // Document header
    printf ("<table class=item>\n") ;
    printf ("<tr><td height=9 width=80></td><td></td><td width=80></td><td></td><td width=80></td><td></td></tr>") ;
    printf ("<tr><td class=itemlabel>Book</td><td class=itemfield colspan=3>%s, %s</td><td class=itemfield>Document</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BookName']), htmlentities($Record['BookDescription']), htmlentities($Record['BookLevelName'].$Record['BookLevelDelimiter'].$Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Level</td><td class=itemfield colspan=3>%d: %s</td><td class=itemfield>Date</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Level']), htmlentities($Record['BookLevelDescription']), date ("Y-m-d H:i:s", dbDateDecode($Record["CreateDate"]))) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield colspan=3>%s</td><td class=itemfield>Version</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description']), htmlentities($Record['Version'])) ;
    printf ("<tr><td class=itemlabel>%s</td><td class=itemfield>%s</td><td class=itemfield>%s</td><td class=itemfield>%s</td><td class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", $Approve[1]['TaskTypeName'], $Approve[1]['FullName'], $Approve[2]['TaskTypeName'], $Approve[2]['FullName'], $Approve[3]['TaskTypeName'], $Approve[3]['FullName']) ;
    printf ("<tr><td height=3></td></tr>\n") ;
    if ($Record['BookFooter']) {
	printf ("<tr><td height=3></td></tr>\n") ;
	printf ("<tr><td class=itemfield colspan=6 align=center>%s</td></tr>\n", $Record['BookFooter']) ;
    }

    // Document seperator
    printf ("<td colspan=6 height=9 style='border-bottom: 1px solid #cdcabb;'></td>") ;
    printf ("</table>\n") ;

    // Comment
    if ($Navigation['Parameters'] == 'comment') {
	if ($Record['Comment'] == '') {
	    printf ("<br><p>No comments avaliable</p>\n") ;
	} else{
	    printf ("<br><p style='padding-right:8px;'>%s</p>\n", str_replace('  ', '&nbsp;&nbsp;', nl2br(htmlentities($Record['Comment'])))) ;
	}
	return 0 ;
    }
    
    // Check for type of file
    if ($Record['Type'] != 'message/rfc822') return sprintf ('%s(%d) not a Mime file, id %d, type %s', __FILE__, __LINE__, $Id, $Record['Type']) ;
    
    // Make filename
    require_once 'lib/file.inc' ;
    $file = fileName ('doc', $Id) ;
    $mime = file_get_contents($file) ;
    $filesize = strlen ($mime) ;
    if ($filesize != $Record['Size']) return sprintf ('%s(%d) invalid size of document, real %d, expected %d', __FILE__, __LINE__, $filesize, $Record['Size']) ;

    // Decode mime structure
    require_once 'Mail/mimeDecode.php' ;
    $params['include_bodies'] = true;
    $params['decode_bodies']  = true;
    $params['decode_headers'] = true;
    $decoder = new Mail_mimeDecode($mime) ;
    $structure = $decoder->decode($params) ;
    unset ($mime) ;

    // Debug
    if (((int)$_GET['debug']) > 0) {
        printf ("<table class=item>\n") ;
        printf ("<tr><td height=9 width=80></td><td></td></tr>") ;
	printf ("<tr><td class=itemlabel>File</td><td class=itemfield>%s (%s)</td></tr>\n", htmlentities($Record['Filename']), htmlentities($file)) ;
	printf ("<tr><td class=itemlabel>Size</td><td class=itemfield>%d</td></tr>\n", $filesize) ;

	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Mime parts</td><td class=itemfield>") ;
	if (isset($structure->parts)) {
	    foreach ($structure->parts as $no => $part) {
		printf ("%d: file %s, size %d, type %s<br>", $no, htmlentities($part->headers['content-location']), strlen($part->body), htmlentities($part->headers['content-type'])) ;
	    }
	} else {
	    printf ('None') ;
	}
	printf ("</td></tr>\n") ;

	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Structure</td><td class=itemfield>") ;
	require_once 'lib/log.inc' ;
	logPrintVar ($structure, 'Mime structure') ;
	printf ("</td></tr>\n") ;
    	printf ("<td colspan=2 height=9 style='border-bottom: 1px solid #cdcabb;'></td>") ;
    	printf ("</table>\n") ;
    }

    // Type of MIME document
    switch ($structure->ctype_primary) {
	case 'text' :
	    // Validate secondary type of body
	    if ($structure->ctype_secondary != 'html') return sprintf ('%s(%d) invalid secondary type, id %d, type %s', __FILE__, __LINE__, $Id, $structure->ctype_secondary) ;
	  
	    // Display document
	    print ('<div>') ;
	    print ($structure->body) ;
	    print ('</div>') ;
	    break ;

	case 'multipart' :
	    // Always display part 0
	    $partno = 0 ;

	    // Validate that part exists
	    if (!isset($structure->parts[$partno])) return sprintf ('%s(%d) part does not exist, id %d, part %d', __FILE__, __LINE__, $Id, $partno) ;

	    // Validate type of part
	    if ($structure->parts[$partno]->ctype_primary != 'text' or $structure->parts[$partno]->ctype_secondary != 'html') return sprintf ('%s(%d) invalid type of part, id %d, part %d, type %s', __FILE__, __LINE__, $Id, $partno, $structure->parts[$partno]->headers['content-type']) ;
	  
	    // Get body exploded by 'scr='
	    $bodyexplode = explode ('src=', $structure->parts[$partno]->body) ;

	    // Modify referances contained in the MIME structure
	    foreach ($bodyexplode as $i => $chunk) {
		// Do not process first chunk of body (should not start with 'src=')
		if ($i == 0) continue ;

		// Locate first delimiter
		$delimiter = substr($chunk, 0, 1) ;
		if ($delimiter == '"' or $delimiter == "'") {
		    $startpos = 1 ;   
		} else {
		    $startpos = 0 ;
		    $delimiter = ' ' ;
		}
		    
		// Locate next delimiter
		if (($endpos = strpos ($chunk, $delimiter, $startpos)) === false) continue ;

		// Get name of referance
		$name = substr ($chunk, $startpos, $endpos-$startpos) ;

		// Locate the filename among the 
		for ($n = 1 ; $n < count($structure->parts) ; $n++) {
		    // Compare name to trailer of location
		    if (strcmp (substr($structure->parts[$n]->headers['content-location'], -strlen($name)), $name) == 0) {
			// Replace referance
			$bodyexplode[$i] = sprintf ('"index.php?nid=%d&id=%d&part=%d" ', navigationMark ('mimepart'), $Id, $n) . substr($chunk, $endpos+1) ;
			break ;
		    }
		}
	    }
	 
//	    printf ("%s", nl2br(htmlentities($structure->parts[0]->body))) ;
//	    return 0 ;
	    
	    // Display document
	    print ('<div>') ;
	    print (implode ('src=', $bodyexplode)) ;
	    print ('</div>') ;
	    break ;

	default :
	    return sprintf ('invalid type of MIME-documment, primary type "%s"', $structure->ctype_primary) ;
    }
    
    return 0 ;
?>
