<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Book</td><td class=itemfield>%s, %s</td></tr>\n", htmlentities($Record['BookName']), htmlentities($Record['BookDescription'])) ;
    printf ("<tr><td class=itemlabel>Level</td><td class=itemfield>%d: %s</td></tr>\n", htmlentities($Record['Level']), htmlentities($Record['BookLevelDescription'])) ;
    printf ("<tr><td class=itemlabel>Document</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BookLevelName'].$Record['BookLevelDelimiter'].$Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Version', 90) ;
    listHead ('Released', 80) ;
    listHead ('User', 80) ;
    listHead ('Created', 120) ;
    listHead ('') ;
    while ($row = dbFetch($Result)) {
        listRow () ;
	listFieldIcon ('doc.gif', 'view', $row["Id"]) ;
	listField ($row['Version']) ;
	listField (($row['Released']) ? 'yes' : 'no') ;
	listField ($row["Loginname"]) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row["CreateDate"]))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No versions', 'colspan=3') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
