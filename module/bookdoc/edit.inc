<?php
   
    require_once "lib/html.inc" ;
    require_once "lib/table.inc" ;
   
    switch ($Navigation["Parameters"]) {
	case 'newdoc' :
	    unset ($Record) ;
	    break ;
    }

    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;

    switch ($Navigation["Parameters"]) {
	case "newversion" :
	    printf ("<tr><td><p>Name</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Name"])) ;
	    printf ("<tr><td><p>Description</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Description"])) ;
	    break ;
	    
	default:
	    printf ("<tr><td><p>Name</p></td><td><input type=text name=Name size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
	    printf ("<tr><td><p>Description</p></td><td><input type=text name=Description size=200 value=\"%s\" style='width=100%%'></td></tr>\n", htmlentities($Record["Description"])) ;
	    print htmlItemSpace() ;
	    printf ("<tr><td><p>KeyWords</p></td><td><input type=text name=KeyWords size=200 value=\"%s\" style='width=100%%'></td></tr>\n", htmlentities($Record["KeyWords"])) ;
	    break ;
    }
    
    switch ($Navigation["Parameters"]) {
	case "newdoc" :
	case "newversion" :
	    print htmlItemSpace() ;
	    printf ("<input type=hidden name=MAX_FILE_SIZE value=2000000>\n") ;
	    printf ("<tr><td><p>File</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    
	    print htmlItemSpace() ;
	    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:100px;'>%s</textarea></td></tr>\n", $Record["Comment"]) ;
	    break ;

	default:
	    print (htmlItemInfo ($Record)) ;
	    break ;
    }

    printf ("</table>\n") ;
    printf ("</form>\n") ;
    
    return 0 ;
?>
