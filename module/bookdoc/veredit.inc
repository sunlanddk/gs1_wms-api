<?php
   
    require_once "lib/html.inc" ;
    require_once "lib/table.inc" ;
   
    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:100px;'>%s</textarea></td></tr>\n", $Record["Comment"]) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;
    
    return 0 ;
?>
