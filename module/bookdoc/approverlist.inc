<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Book</td><td class=itemfield>%s, %s</td></tr>\n", htmlentities($Record['BookName']), htmlentities($Record['BookDescription'])) ;
    printf ("<tr><td class=itemlabel>Level</td><td class=itemfield>%d: %s</td></tr>\n", htmlentities($Record['Level']), htmlentities($Record['BookLevelDescription'])) ;
    printf ("<tr><td class=itemlabel>Document</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BookLevelName'].$Record['BookLevelDelimiter'].$Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Type', 90) ;
    listHead ('User', 80) ;
    listHead ('Name') ;
    listHead ('Done', 120) ;
    listHead ('') ;
    while ($row = dbFetch($Result)) {
        listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row["Id"]) ;
	listField ($row['TaskTypeName']) ;
	listField ($row["Loginname"]) ;
	listField ($row["Fullname"]) ;
	listField (($row['Done']) ? date ("Y-m-d H:i:s", dbDateDecode($row["DoneDate"])) : '-') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No approvers', 'colspan=3') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
