<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
	'Comment'	=> array (),
	'UserId'	=> array ('type' => 'integer'),
	'EndDate'	=> array ('type' => 'date')	
    ) ;

    // Verify that task not has been done
    if ($Record['Done']) return 'modifications not allowed to a task allready done' ; 
  
    // Do saving
    return saveFields ('Task', $Id, $fields) 
?>
