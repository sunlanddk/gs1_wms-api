<?php

    require_once 'lib/file.inc' ;
    
    // Make filename
    $file = fileName ('doc', $Id) ;

    switch ($_SERVER["SERVER_PROTOCOL"]) {
	case "HTTP/1.1":
	    header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	    break ;
	
	case "HTTP/1.0":
	    header ("Pragma: no-cache") ; 
	    break ;

	default:
	    return sprintf ("%s(%d) invalid server protocol %s", __FILE__, __LINE__, $_SERVER["SERVER_PROTOCOL"]) ;
    }

//  $size = filesize($file) ;
//  header ('Content-Length: '.$size) ;		// Can't be used when compressing
    header ('Content-Type: '.$Record['Type']) ;
//  header ('Content-Type: application/force-download');
//  header ('Content-Type: application/octet-stream');
    header ('Content-Disposition: attachment; filename='.$Record['Filename']);
    header ('Last-Modified: '.date("D, j M Y G:i:s T", dbDateDecode($Record['CreateDate'])));

    readfile ($file) ;

    return 0 ;

?>
