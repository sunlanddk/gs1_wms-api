<?php
   
    require_once "lib/html.inc" ;
    require_once "lib/table.inc" ;
    require_once "lib/form.inc" ;
   
    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td td class=itemlabel>Type</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["TaskTypeName"])) ;
    print htmlItemSpace() ;

    if ($Record['Done']) {
	printf ("<tr><td class=itemlabel>Approved</td><td class=itemfield>%s, %s</td></tr>", date("Y-m-d H:i:s", dbDateDecode($Record['DoneDate'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['DoneUserId'])) ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemfield>Comment</td><td class=itemfield>%s</td></tr>\n", nl2br(htmlentities($Record["Comment"]))) ;
    } else {
	printf ("<tr><td class=itemlabel>User</td><td>%s</td></tr>\n", htmlDBSelect ('UserId style="width:250px;"', $Record['UserId'], 'SELECT User.Id AS Id, CONCAT(User.Loginname," (",User.FirstName," ",User.Lastname,")") AS Value FROM User WHERE User.Active=1 AND User.Login=1 ORDER BY User.Loginname')) ;
//	printf ("<tr><td class=itemlabel>Enddate</td><td><input type=text id=IdEndDate name=EndDate size=10 value=\"%s\" style='width:68px' readonly=1><img src='image/date.gif' id=IdEndDateButton style='vertical-align:bottom;margin-left:0px;cursor: pointer;'></td></tr>\n", date("Y-m-d",dbDateDecode($Record["EndDate"]))) ;
	printf ("<tr><td class=itemlabel>Enddate</td><td>%s</td></tr>\n", formDate ('EndDate', dbDateDecode($Record["EndDate"]))) ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:100px;'>%s</textarea></td></tr>\n", $Record["Comment"]) ;
    }

    print (htmlItemInfo ($Record)) ;

    printf ("</table>\n") ;
    printf ("</form>\n") ;
    
    return 0 ;
?>
