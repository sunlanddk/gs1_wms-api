<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'stock' :
	    itemStart () ;
	    itemSpace () ;
	    switch ($Record['Type']) {
		case 'fixed' :
		    itemFieldIcon ('Stock', $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
		    break ;

		case 'transport' :
		    itemFieldIcon ('Transport', $Record['Name'], 'transport.gif', 'transportview', $Record['Id']) ;
		    break ;

		case 'shipment' :
		    itemFieldIcon ('Shipment', $Record['Name'], 'shipment.gif', 'shipmentview', $Record['Id']) ;
		    break ;
	    }
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Type', 90) ;
    listHead ('Description') ;
    if (!$HideStock) {
	listHead ('Stock', 100) ;
    }
    listHead ('Batch', 90) ;
    listHead ('Items', 90, 'align=right') ;
    listHead ('Article', 80, 'align=right') ;
    listHead ('Color', 80, 'align=right') ;
    listHead ('Size', 40, 'align=right') ;
    listHead ('Pick?', 40) ;
    listHead ('Qty', 60, 'align=right') ;
    listHead ('Volume (m3)', 90, 'align=right') ;
    listHead ('Tara (kg)', 90, 'align=right') ;
    listHead ('Gross (kg)', 90, 'align=right') ;
    listHead ('SKU assigned', 100, 'align=right') ;
    listHead ('Items ok?',50) ;
    listHead ('Position', 50) ;
    listHead ('Container', 80, 'align=right') ;
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'containerview', $row['Id']) ;
	listField ($row['ContainerTypeName']) ;
	listField ($row['Description']) ;
	if (!$HideStock) {
	    listField ($row['StockName']) ;
	}
	listField ($row['BatchNumber']) ;
	listField ($row['ItemCount'], 'align=right') ;
	listField ($row['ItemArticleNumber'], 'align=right') ;
	listField ($row['ItemColorNumber'], 'align=right') ;
	listField ($row['ItemSize'], 'align=right') ;
	listField ($row['ItemPickAssigned']?'Yes':'', 'align=right') ;
	listField ($row['ItemQty'], 'align=right') ;
	listField ($row['Volume'], 'align=right') ;
	listField ($row['TaraWeight'], 'align=right') ;
	listField ($row['GrossWeight'], 'align=right') ;
	listField ($row['VariantCode'], 'align=right') ;
	if ($row['VariantCode']<>'') {
		if ($row['ItemQty']>0) {
			if($row['ItemPickAssigned']) 
				$ItemsOk = 'Yes' ;
			else
				$ItemsOk = 'No' ;
		} else {
			$ItemsOk = 'Yes' ;
		}
	} else {
		$ItemsOk = '' ;
	}
	listField ($ItemsOk) ; 
	listField ($row['Position']) ;
	listField ((int)$row['Id'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Containers', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
