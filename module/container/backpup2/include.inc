<?php

    require_once 'lib/table.inc' ;

    function containerDeleteEmpty ($id) {
	// Check if the container has become empty
	$query = sprintf ('SELECT COUNT(*) AS Count FROM Item WHERE Item.ContainerId=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$row['Count'] == 0) {
	    // The container is empty
	    // Delete it
	    tableDelete ('Container', $id) ;
	}	
	return 0 ;
    }
?>
