<?php

    require_once 'lib/table.inc' ;

    // Ensure no Items in Container
    $query = sprintf ("SELECT Id FROM Item WHERE ContainerId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "can not be deleted when it contains Items" ;
    
    tableDelete ('Container', $Id) ;

    // Free Picking position
	$query = sprintf ("UPDATE `VariantCode` SET `PickContainerId`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE PickContainerId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
	dbQuery ($query) ;
    
    return 0
?>
