<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    ?>

    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>

    <div id="appinbound">
        <p class="hiddenkeyboard">{{str}}</p>
        <input type="hidden" class="sscc" value="<?php echo '00'.$Record['Sscc']; ?>">
        <h2>SSCC: {{sscc}}</h2>
        
        <div class="width50 pr">
            <h2>Current sscc information:</h2>
            <table id="alextest" class="list">
                <tr bgcolor="#FFFFFF">
                    <td class="listhead" width="10"><p class="listhead">AI</p></td>
                    <td class="listhead" width="35"><p class="listhead">Description</p></td>
                    <td class="listhead" width="35"><p class="listhead">Value</p></td>
                </tr>
                <tr v-for="(item, index) in ais" :key="index">
                    <td class="list"><p class="listtall">{{item.ai}}</p></td>
                    <td class="list"><p class="listtall">{{item.description}}</p></td>
                    <td class="list"><p class="listtall">{{item.value}}</p></td>
                    <!-- <td class="list"><p class="listtall delete" v-on:click="removetempitem(index)">Delete</p></td> -->
                </tr>
            </table>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            <div class="button" v-on:click="resetAll()">Reset</div>
        </div>
        <div class="width50 pl">
            <h2>New sscc information:</h2>
            <table id="alextest" class="list">
                <tr bgcolor="#FFFFFF">
                    <td class="listhead" width="10"><p class="listhead">AI</p></td>
                    <td class="listhead" width="35"><p class="listhead">Description</p></td>
                    <td class="listhead" width="35"><p class="listhead">Value</p></td>
                    <td class="listhead" width="35"><p class="listhead">Action</p></td>
                </tr>
                <tr v-for="(item, index) in newais" :key="index">
                    <td class="list"><p class="listtall">{{item.ai}}</p></td>
                    <td class="list"><p class="listtall">{{item.description}}</p></td>
                    <td class="list"><p class="listtall">{{item.value}}</p></td>
                    <td class="list"><p class="listtall delete" v-on:click="removetempitem(index)">Delete</p></td>
                </tr>
            </table>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            <div class="button" v-on:click="getAiInfo()">Save</div>
        </div>

    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/addaitosscc.js"></script>
<style>
    #appinbound{
        position: relative;
    }
    #appinbound table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .width50{
        box-sizing: border-box;
        width: 50%;
        float: left;
    }
    .pr{
        padding-right: 20px;
    }
    .pl{
        padding-left: 20px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        float: left;
        cursor: pointer;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    .delete{
        cursor: pointer;
        text-decoration: underline;
    }
</style>

<?php
    return 0 ;

?>
