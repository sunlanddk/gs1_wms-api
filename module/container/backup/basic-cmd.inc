<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'ContainerTypeId'	=> array ('type' => 'integer',	'check' => true,	'mandatory' => true),
	'Description'		=> array (),
	'Position'		=> array (),
	'TaraWeight'		=> array ('type' => 'decimal',	'format' => '12.3'),
	'GrossWeight'		=> array ('type' => 'decimal',	'format' => '12.3',	'check' => true),
	'Volume'		=> array ('type' => 'decimal',	'format' => '12.3')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $fields, $Record, $User ;
	switch ($fieldname) {
	    case 'ContainerTypeId' :
		if (!$changed or $Id > 0) return false ;

		// Any Type Selected
		if ($value == 0) return 'please select Container type' ;
		
		// Get ContainerType
		$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] <= 0) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $value) ;
	    
		// Set defaults for new Containers
		if ($Id <= 0) {
		    $fields['TaraWeight'] = array ('type' => 'set', 'value' => $row['TaraWeight']) ;
		    $fields['Volume'] = array ('type' => 'set', 'value' => $row['Volume']) ;
		}

		return true ;

	    case 'GrossWeight' :
		if (!$changed and $Id > 0) return false ;

		if ($value == 0) {
		    // No value supplied
		    // Use Tara weight as default
		    $fields['GrossWeight']['value'] = $fields['TaraWeight']['value'] ;
		} else {
		    // Validate Gross Weight
		    if ((float)$value < (float)$fields['TaraWeight']['value']) return 'Gross Weight can not be below Tara Weight' ;
		}
		return true ;
    	}
	return false ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Only new Containers to fixed Storage locations
	    if ($Record['Type'] != 'fixed') return sprintf ('%s(%d) can not create Container at this Sotrage Location, id %d', __FILE__, __LINE__, $Record['Id']) ;

	    $fields['StockId'] = array ('type' => 'set', 'value' => $Record['Id']) ;
	    $Id = -1 ;
	    unset ($Record) ;
	    break ;

	default :
	    // Only modify Containers on fixed Storage locations
	    if ($Record['StockType'] != 'fixed') return sprintf ('%s(%d) can only modify Container on fixed Sotrage Location, id %d', __FILE__, __LINE__, $Record['Id']) ;
	    break ;
    }
     
    // Save record
    $res = saveFields ('Container', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
    	    // View new Item
	    return navigationCommandMark ('containerview', (int)$Record['Id']) ;
    }

    return 0 ;
?>
