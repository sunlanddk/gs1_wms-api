<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Stock', $Record['StockName']) ;
    if ($Navigation['Parameters'] != 'new') {
	itemField ('Type', $Record['ContainerTypeName']) ;
    }
    itemSpace () ;
    itemEnd () ;
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    break ;
    }
 
    formStart () ;

    itemStart () ;    
    itemHeader() ;
    if ($Navigation['Parameters'] == 'new') {
	itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 0, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:150px;')) ;
	itemSpace () ;
    }
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%')) ;
    itemFieldRaw ('Position', formText ('Position', $Record['Position'], 20)) ;
    itemSpace () ;
    if ($Navigation['Parameters'] != 'new') {
	itemFieldRaw ('Volume', formText ('Volume', $Record['Volume'], 10, 'text-align:right;') . ' m3') ;
	itemSpace () ;
 	itemFieldRaw ('Tara Weight', formText ('TaraWeight', $Record['TaraWeight'], 10, 'text-align:right;') . ' kg') ;
    }    
    itemFieldRaw ('Gross Weight', formText ('GrossWeight', $Record['GrossWeight'], 10, 'text-align:right;') . ' kg') ;
    itemInfo ($Record) ;
    itemEnd () ;   

    formEnd () ;

    return 0 ;
?>
