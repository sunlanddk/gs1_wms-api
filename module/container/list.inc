<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

    // Filter Bar
	if ($Navigation['Parameters'] <> 'layout') 
	    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'layout' :
	case 'stock' :
	    itemStart () ;
	    itemSpace () ;
	    switch ($Record['Type']) {
		case 'fixed' :
		    itemFieldIcon ('Stock', $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
		    break ;

		case 'transport' :
		    itemFieldIcon ('Transport', $Record['Name'], 'transport.gif', 'transportview', $Record['Id']) ;
		    break ;

		case 'shipment' :
		    itemFieldIcon ('Shipment', $Record['Name'], 'shipment.gif', 'shipmentview', $Record['Id']) ;
		    break ;
			
		case 'inbound' :
		    itemFieldIcon ('Shipment', $Record['Name'], 'shipment.gif', 'shipmentview', $Record['Id']) ;
		    break ;
	    }
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }

	if ($Navigation['Parameters'] <> 'layout') {
		// List
		listStart () ;
		listRow () ;
		listHeadIcon () ;
		listHead ('Type', 130) ;
		//listHead ('SSCC', 130) ;
		listHead ('Description') ;
		if ($HidePosition) {
			listHead ('Position', 1) ;
			//listHead ('PosType', 1) ;
		} else {
			listHead ('Position', 60) ;
			//listHead ('PosType', 60) ;
		}
		
		if (!$HideStock) {
			listHead ('Stock', 300) ;
			listHead ('Owner', 150) ;
		}
		//listHead ('VariantCode', 100) ;
		//listHead ('Description') ;

//		listHead ('Order') ;

		//listHead ('Reserved', 80, 'align=left') ;
		//listHead ('Article', 100, 'align=left') ;
		//listHead ('Batch', 70) ;
		//listHead ('BestBefore', 63) ;
/*
		listHead ('Items', 90, 'align=right') ;
		listHead ('Article', 80, 'align=right') ;
		listHead ('Color', 80, 'align=right') ;
		listHead ('Size', 40, 'align=right') ;
		listHead ('Pick?', 40) ;
*/
		//listHead ('Qty', 40, 'align=right') ;
		//listHead ('Volume (m3)', 2, 'align=right') ;
		//listHead ('Tara (kg)', 2, 'align=right') ;
		//listHead ('Net (kg)', 60, 'align=right') ;
		//listHead ('Gross (kg)', 60, 'align=right') ;
		listHead ('ContId', 50, 'align=right') ;
		listHead ('', 8) ;
		$n = 0 ;
		while ($n++ < $listLines and ($row = dbFetch($Result))) {
			listRow () ;
			listFieldIcon ($Navigation['Icon'], 'containerview', $row['Id']) ;
			listField ($row['ContainerTypeName']) ;
			//listField ($row['Sscc']) ;
			listField ($row['Description']) ;
			listField ($row['Position']) ;
			//listField ($row['LayoutType']) ;
			
			if (!$HideStock) {
				listField ($row['StockNumber'] . ' - ' . $row['StockName']) ;
				listField ($row['Owner']) ;
			}
			//listField ($row['VariantCode']) ;
			//listField ($row['VariantDescription']) ;
			$_subquery = 'Select PickOrder.Reference 
						from ContainerReserved 
						LEFT JOIN PickOrderLine ON PickOrderLine.Id=ContainerReserved.PickOrderLineId and PickOrderLine.active=1 
						LEFT JOIN PickOrder On PickOrder.Id=PickOrderLine.PickOrderId and PickOrder.Packed=0 and PickOrder.active=1
						Where ContainerReserved.ContainerId=' . (int)$row['Id'] . ' AND PickOrder.Id>0 AND ContainerReserved.active=1';
			$_subres = dbQuery($_subquery) ;
			$_subrow = dbFetch ($_subres) ;
			dbQueryFree($_subres) ;
			
			
			//listField ($_subrow['Reference']) ;
			//listField ($row['ItemArticleNumber']) ;
			//listField ($row['BatchNumber']) ;
			//listField ($row['BestBefore']) ;
			//listField (number_format($row['ItemQty'],0), 'align=right') ;
			//listField ($row['Volume'], 'align=right') ;
			//listField (number_format($row['TaraWeight'],0), 'align=right') ;
			//listField (number_format($row['ItemWeight'],0), 'align=right') ;
			//if ($row['GrossWeight']==$row['TaraWeight']) 
				//listField (number_format($row['TaraWeight']+$row['ItemWeight'],0), 'align=right') ;
			//else
				//listField (number_format($row['GrossWeight'],0), 'align=right') ;
			
			listField ((int)$row['Id'], 'align=right') ;
		}
		if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Containers', 'colspan=4') ;
		}
		listEnd () ;
		dbQueryFree ($Result) ;

	// Layout list.
	} else {
		// List
		listStart () ;
		listRow () ;
		listHead ('', 10) ;
		listHead ('Position', 50) ;
		listHead ('SKU assigned', 80, 'align=left') ;
		listHead ('Items ok?',55) ;
		listHead ('Article', 80, 'align=left') ;
		listHead ('Color', 80, 'align=left') ;
		listHead ('Size', 40, 'align=left') ;
		listHead ('Qty', 60, 'align=left') ;
//		listHead ('Container', 80, 'align=right') ;
		listHead ('', 200) ;

		while (($row = dbFetch($Result))) {
			$row['Position'] = strtoupper($row['Position']) ;
			$Lines[$row['Position']] = $row ;
			$Addr = substr($row['Position'], 0, 5) ;
			if (strlen($row['Position'])==6 or strlen($row['Position'])==7) {
				$Addr = strtoupper(substr($row['Position'], 0, 5)) ;
				$Room = (int)substr($row['Position'], 5, 2) ;
				if ($Room>0 and $Room<25) $Rooms[$Addr][$Room] = 1 ;
				$Rooms[$Addr][0] = 1 ;
			} else
				$Rooms[$Addr][0] = 0 ;
		}

		foreach ($PickLayout[$Record['Name']] as $StreetName => $Street) {
			if (isset($_GET['StreetName']))
				if ($_GET['StreetName'] <> $StreetName) continue ;
 			for ($StreetNo=1; $StreetNo<=$Street['NoStreetNo']; $StreetNo++) {
				$NoFloors = ($StreetNo/2 > (int)($StreetNo/2)) ? $Street['OddNoFloors'] : $Street['EvenNoFloors'] ;
				for ($i=0; $i<$NoFloors; $i++) {
					$Floor = chr(ord('a') + $i) ;
					$Pos = strtoupper(sprintf ('%s%03d%s', $StreetName,  $StreetNo, $Floor)) ;
					$Pos_Floor = $Pos ;

//					foreach ($Rooms[$Pos] as $RoomNo => $value) {
					for ($j=0; $j<25; $j++) {
						 if ($j>0) {
							if ($Rooms[$Pos_Floor][$j]>0) 
								$Pos = strtoupper(sprintf ('%s%03d%s%d', $StreetName,  $StreetNo, $Floor, $j)) ;
							else continue;
						} else {
							if ($Rooms[$Pos][0]>0) continue;
						}
						
						listRow () ;
						if ($Lines[$Pos]['VariantCode'] <> '') {
							if ((int)$Lines[$Pos]['ItemQty'] == 0)
								listFieldIcon ('delete.gif', 'freeposition', $Lines[$Pos]['Id'], 'StreetName=' . $_GET['StreetName']) ;
							else
								listFieldIcon ('move.gif', 'movefreeposition', $Lines[$Pos]['Id'], 'StreetName=' . $_GET['StreetName']) ;
						} else {
							listField ('') ;
						}
						listField ($Pos) ;
						listField ($Lines[$Pos]['VariantCode'], 'align=left') ;
						if ($Lines[$Pos]['VariantCode']<>'') {
							if ($Lines[$Pos]['ItemQty']>0) {
								if($Lines[$Pos]['ItemPickAssigned']) 
									$ItemsOk = 'Yes' ;
								else
									$ItemsOk = 'No' ;
							} else {
								$ItemsOk = 'Yes' ;
							}
						} else {
							$ItemsOk = '' ;
						}
						listField ($ItemsOk) ; 
						listField ($Lines[$Pos]['ItemArticleNumber'], 'align=left') ;
						listField ($Lines[$Pos]['ItemColorNumber'], 'align=left') ;
						listField ($Lines[$Pos]['ItemSize'], 'align=left') ;
						listField (number_format($Lines[$Pos]['ItemQty'],0), 'align=rigth') ;
	//					listField ((int)$Lines[$Pos]['Id'], 'align=right') ;
						listField ('') ;
					} 
				} 
			} 
		}
		
		if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Containers', 'colspan=4') ;
		}
		listEnd () ;
		dbQueryFree ($Result) ;
	}   
    return 0 ;
?>
