<?php

    require_once 'lib/navigation.inc' ;

    $queryFields = 'Container.*, 
					Stock.Name AS StockName, Stock.StockNo AS StockNumber, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Arrived AS StockArrived, Stock.Verified AS StockVerified, 
					ContainerType.Name AS ContainerTypeName, Company.Name as Owner' ;
//					PickOrder.Id as PickOrderId, 
    $queryTables = 'Container LEFT JOIN ContainerType ON ContainerType.Id=Container.ContainerTypeId 
						LEFT JOIN Stock ON Stock.Id=Container.StockId 
						LEFT JOIN Stock Istock ON IStock.Id=Container.InboundStockId 
						LEFT JOIN Company ON Company.Id=IStock.FromCompanyId' ;
//						LEFT JOIN ContainerReserved ON ContainerReserved.ContainerId=Container.Id 
//						LEFT JOIN PickOrderLine ON PickOrderLine.Id=ContainerReserved.PickOrderLineId and PickOrderLine.active=1 
//						LEFT JOIN PickOrder On PickOrder.Id=PickOrderLine.PickOrderId and PickOrder.Packed=0 and PickOrder.active=1' ;
    $queryGroup = 'Container.Id' ;

    switch ($Navigation['Function']) {
	case 'movelist' :
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

	    // Fall through
	     
	case 'streetlist' :
	case 'list' :
	case 'containerlist' :
	    // Expand query
	    $queryTables .= ' LEFT JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
					LEFT JOIN StockLayout ON StockLayout.Position=Container.Position and StockLayout.StockId=Container.StockId
					LEFT JOIN Stock AltStock ON AltStock.Id = Item.AltStockId
					LEFT JOIN VariantCode ON VariantCode.Id=Item.VariantCodeId AND VariantCode.Active=1
				    LEFT JOIN Article ON VariantCode.ArticleId=Article.Id 
				    LEFT JOIN OrderLine ol ON Item.OrderLineId>0 and ol.id=Item.OrderLineId
				    LEFT JOIN ArticleColor ON Item.ArticleColorId=ArticleColor.Id 
				    LEFT JOIN Color ON ArticleColor.ColorId=Color.Id 
					LEFT JOIN ArticleSize ON Item.ArticleSizeId=ArticleSize.Id 
					Left join variant_ai ON variant_ai.itemid=item.id and variant_ai.aiid=4' ;
//					LEFT JOIN VariantCode ON VariantCode.PickContainerId=Container.Id AND VariantCode.Active=1';
//				    LEFT JOIN VariantCode on VariantCode.ArticleColorId=Item.ArticleColorId AND VariantCode.ArticleSizeId=Item.ArticleSizeId AND VariantCode.Active=1' ;

	    $queryFields .= ', COUNT(Item.Id) AS ItemCount, 
					(select sum(quantity) from item where item.containerid=container.id and item.variantcodeid=variantcode.id and item.active=1) as ItemQty, SUM(Item.Quantity*VariantCode.Weight/1000) as ItemWeight, Item.BatchNumber, MIN(Item.AltStockId) as MinAltStockId, 
					StockLayout.Type as LayoutType,
					MAX(Item.AltStockId) as MaxAltStockId, Max(AltStock.Name) as AltStockName,
					Article.Number as ItemArticleNumber, Color.Number as ItemColorNumber, ArticleSize.Name as ItemSize, 
					VariantCode.VariantCode, VariantCode.PickContainerId, ol.orderid as OrderId, VariantCode.VariantDescription, VariantCode.Weight as VariantWeight, variant_ai.value as BestBefore,
					IF (VariantCode.ArticleColorId=Item.ArticleColorId AND VariantCode.ArticleSizeId=Item.ArticleSizeId, 1, 0) AS ItemPickAssigned' ;
//					Article.Number as ItemArticleNumber, Color.Number as ItemColorNumber, ArticleSize.Name as ItemSize, VariantCode.VariantCode, VariantCode.PickContainerId' ;

	    // List field specification
	    $fields = array (
			NULL, // index 0 reserved
	//		array ('name' => 'StockNumber',		'db' => 'Stock.StockNo'),
	//		array ('name' => 'Order',			'db' => 'ol.orderid'),
	//		array ('name' => 'Type',			'db' => 'ContainerType.Name'),
			array ('name' => 'Stock',			'db' => 'concat(Stock.StockNo, " - ", Stock.Name)'),
			array ('name' => 'Position',		'db' => 'Container.Position'),
			array ('name' => 'ContId',		'db' => 'Container.Id'),
			//array ('name' => 'SSCC',			'db' => 'Container.Sscc'),
	//		array ('name' => 'Items',			'db' => 'COUNT(Item.Id)'),
			//array ('name' => 'Owner',			'db' => 'Company.Name'),
			//array ('name' => 'Article',			'db' => 'Article.Number'),
	//		array ('name' => 'Color',			'db' => 'Color.Number'),
	//		array ('name' => 'Size',			'db' => 'ArticleSize.Name'),
			//array ('name' => 'VariantCode',	'db' => 'VariantCode.VariantCode'),
			//array ('name' => 'Batch',			'db' => 'Item.BatchNumber'),
			//array ('name' => 'BestBefore',		'db' => 'variant_ai.value'),
	//		array ('name' => 'Container',		'db' => 'Container.Id', 			'direct' => 'Container.Active=1'),
	//		array ('name' => 'Qty',				'db' => 'ItemQty',				'nofilter' => true),
	//		array ('name' => 'Volume (m3)',		'db' => 'Container.Volume'),
	//		array ('name' => 'Tara (kg)',		'db' => 'Container.TaraWeight'),
	//		array ('name' => 'MoveTo',			'db' => 'AltStock.Name'),
	//		array ('name' => 'Gross (kg)',		'db' => 'Container.GrossWeight')
	    ) ;

	    switch ($Navigation['Parameters']) {
	        case 'layout' :
	        case 'stock' :
				$query = sprintf ('SELECT * FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
				$res = dbQuery ($query) ;
				$Record = dbFetch ($res) ;
				dbQueryFree ($res) ;

				// Navigation
				if ($Record['Type'] == 'fixed') {
					navigationEnable ('new') ;
				}

				// Display
				if ($Record['Type']=="inbound")
					$HideStock = false ;
				else
					$HideStock = true ;

				$queryClause = sprintf ('(Container.StockId=%d or container.InboundStockId=%d) AND Container.Active=1', $Id, $Id) ;
				break ;

	        case 'inbound' :
				$HidePosition = true ;
				$queryClause = sprintf ('Stock.Type="inbound" AND Container.Active=1') ;
				break ;
				
	        case 'fixed' :
				$queryClause = sprintf ('Stock.Type="fixed" AND Container.Active=1') ;
				break ;
				
			default :
				$queryClause = 'Container.Active=1' ;
				break ;
	    }
	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, '', $queryGroup) ;
	    if (is_string($Result)) return $Result ;

	    if ($Navigation['Function'] == 'list') {
			// Navigation
			// Pass all parameters for the list function to the move function
			$param = 'param=' . $Navigation['Parameters'] ;
			if ($listParam != '') $param .= '&' . $listParam ;
			navigationSetParameter ('movelist', $param) ;
			
			if (dbNumRows($Result) == 0) {
				navigationPasive ('movelist') ;
			}
			
			return listView ($Result, 'containerview') ;
	    }
	    return 0 ;

	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ('SELECT Stock.*, Stock.Name AS StockName FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    return 0 ;
	    }

	    // Fall through
	case 'printsscclabel' :
	case 'view' :
	case 'move' :
	case 'move-cmd' :
	case 'delete-cmd' :
	    // Get Container
	    $query = sprintf ("SELECT %s FROM %s WHERE Container.Id=%d AND Container.Active=1", $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Navigation
//	    if ($Record['StockType'] != 'fixed') {
//		navigationPasive ('edit') ;
//	    }

	    if ($Record['StockDone'] or ($Record['StockReady'] and !$Record['StockVerified'])) {
		navigationPasive ('move') ;
	    }		
		
	    break ;

	case 'move-cmd' :
	    // Everything handled in the command module
	    break ;    
    }
 
    return 0 ;
?>
