<?php
    
    // Check for user associations
    if ($Record['ProjectId'] == 0) {
	// General role direct related to user
	$query = sprintf ("SELECT User.Id AS Id FROM User WHERE User.Active=1 AND User.RoleId=%d", $Record['Id']) ;
    } else {
	// Project Role
	$query = sprintf ("SELECT UserRole.Id AS Id FROM UserRole, User WHERE UserRole.UserId=User.Id AND UserRole.Active=1 AND User.Active=1 AND UserRole.RoleId=%d", $Record['Id']) ;
    }
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("Due to User associations, the role can't be deleted") ;
    
    // do delete
    require_once "lib/table.inc" ;
    tableDelete ("Role", $Id) ;

    return 0
?>
