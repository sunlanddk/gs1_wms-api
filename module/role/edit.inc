<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/html.inc' ;

    switch ($Navigation["Parameters"]) {
	case "globalnew":
	case "projectnew":
	case "selectednew":
	    unset ($Record) ;
	    $Record["ProjectId"] = $Id ;
	    break ;
    }

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
//    printf ("<input type=hidden name=ProjectId value=%d>", $Record["ProjectId"]) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    if ($Record['ProjectId'] > 0) {
	printf ("<tr><td class=itemlabel>Project</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('Project', 'Name', $Record['ProjectId']))) ;
	print htmlItemSpace() ;
    }
    printf ("<tr><td><p>Role</p></td><td><input type=text name=Name size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
    printf ("<tr><td><p>Description</p></td><td><input type=text name=Description size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["Description"])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;   

    printf ("<br><H3>Permissions</H3>\n") ;
    printf ("<table width=100%% style='table-layout:fixed;'>\n") ;
    printf ("<tr><td class=listhead width=120><p class=listhead>Function</p></td><td class=listhead width=45 align=center><p class=listhead>R</p></td><td class=listhead width=45 align=center><p class=listhead>W</p></td><td class=listhead width=45 align=center><p class=listhead>C</p></td><td class=listhead width=45 align=center><p class=listhead>D</p></td><td class=listhead width=45 align=center><p class=listhead>R-o</p></td><td class=listhead width=45 align=center><p class=listhead>W-o</p></td><td class=listhead width=45 align=center><p class=listhead>D-o</p></td><td class=listhead></td></tr>\n") ;

    $query = sprintf ("SELECT Function.Name, Function.Id") ;
    foreach ($PermFields AS $perm) $query .= ", Permission.".$perm ;
    $query .= sprintf (" FROM Function LEFT JOIN Permission ON Permission.FunctionId=Function.Id AND Permission.RoleId=%d AND Permission.Active=1 WHERE Function.Active=1 AND Function.FlagNoPermission=0", $Id) ;
    $query .= ($Record['ProjectId'] == 0) ? ' AND Function.PermProjectRole=0' : ' AND Function.PermProjectRole<>0' ;
    $query .= " ORDER BY Function.Name" ;
    $result = dbQuery ($query) ;

    while ($func = dbFetch($result)) {
	printf ("<tr>") ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($func["Name"])) ;
	printf ("<td align=center><input type=checkbox name=PermRead[%d]%s></td>\n", $func["Id"], ($func["PermRead"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermWrite[%d]%s></td>\n", $func["Id"], ($func["PermWrite"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermCreate[%d]%s></td>\n", $func["Id"], ($func["PermCreate"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermDelete[%d]%s></td>\n", $func["Id"], ($func["PermDelete"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermReadOther[%d]%s></td>\n", $func["Id"], ($func["PermReadOther"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermWriteOther[%d]%s></td>\n", $func["Id"], ($func["PermWriteOther"])?" checked":"") ;
	printf ("<td align=center><input type=checkbox name=PermDeleteOther[%d]%s></td>\n", $func["Id"], ($func["PermDeleteOther"])?" checked":"") ;
	printf ("</tr>\n") ;
    }
    dbQueryFree ($result) ;

    printf ("</table>\n") ;
    printf ("</form>\n") ;
    return 0 ;
?>
