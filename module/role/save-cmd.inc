<?php

    require "lib/save.inc" ;
   
    $fields = array (
	"Name"		=> array ("mandatory" => true),
	"Description"	=> array ()
    ) ;

    switch ($Navigation["Parameters"]) {
	case "globalnew":
	case "projectnew":
	case "selectednew":
	    $fields['ProjectId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Id = -1 ;
	    break ;
    }
    
    $r = saveFields ("Role", $Id, $fields) ;

    if ($Id == -1) $Id = dbInsertedId() ;

    if ($r === 0) {
	$query = sprintf ("SELECT Function.Name AS FunctionName, Function.Id AS FunctionId, Permission.Id AS PermissionId, Permission.PermRead AS PermRead, Permission.PermWrite AS PermWrite, Permission.PermCreate AS PermCreate, Permission.PermDelete AS PermDelete, Permission.PermReadOther AS PermReadOther, Permission.PermWriteOther AS PermWriteOther, Permission.PermDeleteOther AS PermDeleteOther FROM Function LEFT JOIN Permission ON Permission.FunctionId=Function.Id AND Permission.RoleId=%d AND Permission.Active=1 WHERE Function.Active=1 ORDER BY Function.Name", $Id) ;
	$result = dbQuery ($query) ;
	$n = 0 ;
	while ($func = dbFetch($result)) {
	    $sqlfields = '' ;
	    $funcid = $func['FunctionId'] ;	    
	    foreach ($PermFields AS $perm) {
		if ($_POST[$perm][$funcid] == "on") {
		    if ($func[$perm]) continue ;
		    $value = 1 ;
		} else {
		    if (!$func[$perm]) continue ;
		    $value = 0 ;
		}
		$sqlfields .= sprintf ("%s=%d, ", $perm, $value) ;
		$n++ ;
	    }
	    
	    if ($sqlfields == '') continue ;
	
	    if ($func["PermissionId"] > 0) {
		$query = sprintf ("UPDATE Permission SET %s ModifyDate='%s' WHERE Id=%d", $sqlfields, dbDateEncode(time()), $func["PermissionId"]) ;
	    } else {
		$query = sprintf ("INSERT INTO Permission SET %s FunctionId=%d, RoleId=%d, Active=1, CreateDate='%s', ModifyDate='%s'", $sqlfields, $funcid, $Id, dbDateEncode(time()), dbDateEncode(time())) ;
	    }
	    dbQuery ($query) ;
	}
	dbQueryFree ($result) ;

	if ($n > 0) tableTouch ('Role', $Id) ;
    }
    
    return $r ;
?>
