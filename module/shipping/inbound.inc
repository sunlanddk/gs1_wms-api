<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    ?>

    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appinbound">
        <p class="hiddenkeyboard">{{str}}</p>
        <h2>Inbound Shipment: New</h2>
        <h3>Number of registered collies: {{items.length}}</h3>

        
        <h3>Register colli:</h3>
        <table id="alextest" class="list">
            <tr bgcolor="#FFFFFF">
                <td class="listhead" width="35"><p class="listhead">Scan Barcode(s)</p></td>
                <td class="listhead" width="10"><p class="listhead">Action</p></td>
            </tr>
            <tr v-for="(item, index) in tempitems" :key="index">
                <td class="list"><p class="listtall">{{item}}</p></td>
                <td class="list"><p class="listtall delete" v-on:click="removetempitem(index)">Delete</p></td>
            </tr>
        </table>

        </br>
        </br>
        </br>

        <h3>List of collies with SSCC</h3>
        <table id="alextest" class="list">
            <tr bgcolor="#FFFFFF">
                <td class="listhead" width="10"><p class="listhead">#</p></td>
                <td class="listhead" width="35"><p class="listhead">SSCC Barcode</p></td>
                <td class="listhead" width="35"><p class="listhead">Additional data barcodes</p></td>
            </tr>
            <template v-for="(item, index) in items.slice().reverse()">
                <tr :key="index">
                    <td class="list"><p class="listtall">{{index + 1}}</p></td>
                    <td class="list"><p class="listtall">{{item.sscc}}</p></td>
                    <td class="list"><p class="listtall">{{item.items[0]}}</p></td>
                </tr>
                <tr v-for="(inneritem, index) in item.items" v-if="index > 0">
                    <td class="list"><p class="listtall"></p></td>
                    <td class="list"><p class="listtall"></p></td>
                    <td class="list"><p class="listtall">{{inneritem}}</p></td>
                </tr>
            </template>
        </table>
        </br>
        </br>
        </br>
        </br>
        </br>
        </br>
        <div class="form">
            <div class="button" v-on:click="createInbound()" >Receive inbound shipment</div>
            <div class="button" v-on:click="resetAll()">Reset</div>
        </div>
        <input type="hidden" class="ownercompanyid" name="ownercompanyid" value="<?php echo $User['CustomerCompanyId'] ?>">
    </div>
    

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/shipping.js?v=4"></script>
<style>
    #appinbound{
        position: relative;
    }
    #appinbound table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        float: left;
        cursor: pointer;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    .delete{
        cursor: pointer;
        text-decoration: underline;
    }
</style>

<?php
    return 0 ;

?>
