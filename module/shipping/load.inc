<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
	case 'ziplist' :
		$query = "SELECT * FROM PickOrder WHERE Active=1 AND Id=1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id=1 ;
		$Record['Id']=1 ;
		
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

	case 'list' :
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Order',		'db' => 'po.Reference'),
			array ('name' => 'Owner',		'db' => 'oc.Name'),
			array ('name' => 'Customer',		'db' => 'c.Name'),
			array ('name' => 'CustomerNumber',		'db' => 'c.Number'),
			array ('name' => 'Country',		'db' => 'ct.Name'),
			array ('name' => 'Delivery',		'db' => 'po.DeliveryDate'),
			array ('name' => 'ImportState',			'db' => 'ImportError')
	    ) ;

//if(po.Packed>0,"Done",if(po.ToId>0,if(s.ready>0,"Part shipped","Packing"),if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")))) as State,   
	    require_once 'lib/list.inc' ;
		$queryFields =  'po.Id as Id,CONCAT(u.FirstName," ",u.LastName," (",u.Loginname,")") as PickUser, 
					if(po.Packed>0,"Done", if (po.updated, "Updated", if(po.printed>0, if(po.ToId>0,	
					if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId=0,"","Assigned"))))) as State, 
					po.Reference as Reference, po.ImportError as ImportError,
					c.Name as CompanyName, c.Number as CompanyNumber, ct.Name as CountryName,
					oc.Name as OwnerCompanyName, po.DeliveryDate as DeliveryDate, count(pol.id) as NoLines, 
					if (po.ToId>0,DepartedDate,"2001-01-01") as DepartedDate, 
					sum(pol.PackedQuantity) as PackedQty, o.reference as OrderReference,
					sum(pol.OrderedQuantity) as Qty' ;
	    
		$queryTables = '(PickOrder po, PickOrderLine pol, Company c, Company oc )
					LEFT JOIN Stock s ON po.toid=s.id 
					LEFT JOIN Country ct ON c.countryid=ct.id
					LEFT JOIN User u ON po.PickUserId=u.Id
					 LEFT JOIN `Order` o On o.id=po.referenceid' ;


	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }

		    $queryTables = $queryTables . 
					 ' LEFT JOIN `Order` ON `Order`.Id=po.ReferenceId' ;
				
		    // Clause
		    $queryClause = sprintf ('`Order`.SeasonId=%d', $Id) ;
				$queryClause = sprintf ('po.Active=1 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and pol.Active=1 and po.Active=1 and `Order`.SeasonId=%d
						GROUP BY po.Id', $Id) ;
		    break ;

		default :
			if ($Navigation['Function'] == 'list') {
				$queryClause = sprintf ('po.Active=1 And po.Packed=0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1
						GROUP BY po.Id', $User['CompanyId']) ;
				// Navigation
				// Pass all parameters for the list function to the zip function
				$param = 'param=' . $Navigation['Parameters'] ;
				if ($listParam != '') $param .= '&' . $listParam ;
				navigationSetParameter ('ziplist', $param) ;
			} else {
				$queryClause = sprintf ('po.Active=1 And po.ToId>0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1
						GROUP BY po.Id', $User['CompanyId']) ;
			}
		    break ;
	    }

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $query = 
	     sprintf ("SELECT Container.`Position` AS `Position`, PickOrderLine.Id AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
				Article.Number as ArticleNumber, PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, PickOrderLine.VariantSize as VariantSize,
				PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, PickOrderLine.PickedQuantity as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity ORDER BY Container.`Position`", $Id) ;
	    $Result2 = dbQuery ($query) ;
    
	    $query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return listView ($Result2, 'packlist') ;

	case 'varview' :
		$query = sprintf ("SELECT * FROM VariantCode WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;

	case 'varlist' :
	    $DescriptionField = 'if (VariantCode.VariantDescription="" or isnull(VariantCode.VariantDescription), Article.Description, VariantCode.VariantDescription)' ;
	    $ColorField = 'if (VariantCode.VariantColorDesc="" or isnull(VariantCode.VariantColorDesc), Color.Description,VariantCode.VariantColorDesc)' ;
	    $ColorCodeField = 'if (VariantCode.VariantColorCode="" or isnull(VariantCode.VariantColorCode), Color.Number,VariantCode.VariantColorCode)' ;
	    $SizeField = 'if (VariantCode.VariantSize="" or isnull(VariantCode.VariantSize), ArticleSize.Name,VariantCode.VariantSize)' ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Variant',		'db' => 'VariantCode'),
		array ('name' => 'Position',		'db' => 'Position'),
		array ('name' => 'Description',	'db' => $DescriptionField),
		array ('name' => 'Color',		'db' => $ColorCodeField),
		array ('name' => 'ColorDesc',		'db' => $ColorField),
		array ('name' => 'Size',		'db' => $SizeField),
		array ('name' => 'Stock',		'db' => 'Stock.Name'),
		array ('name' => 'Container',		'db' => 'Container.Id')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
					VariantCode.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
					$DescriptionField . ' as VariantDescription,' .
					$ColorField . ' as VariantColor,' .
					$ColorCodeField . ' as VariantNumber,' .
					$SizeField . ' as VariantSize,
					Article.Number as ArticleNumber';
		$queryTables = 'VariantCode
			 LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
			 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
			 LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
			 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
			 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
			 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
		$queryOrderBy = '' ;

	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }
			if ($Record['ExplicitMembers']==1) {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							CollectionMember.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(Collection, CollectionMember)
					 LEFT JOIN VariantCode ON CollectionMember.articleid=VariantCode.articleid AND CollectionMember.articleColorid=VariantCode.articleColorid AND VariantCode.Active=1
					 LEFT JOIN Article ON CollectionMember.ArticleId=Article.Id and Article.Active=1 
					 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
					 LEFT JOIN ArticleColor ON CollectionMember.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;

					// Clause
					$queryClause = sprintf ('Collection.SeasonId=%d AND CollectionMember.CollectionId=Collection.Id AND CollectionMember.Active=1', $Id) ;
			} else {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							Article.Id as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(`order` o, orderline ol, article)
					 LEFT JOIN ArticleSize ON article.Id=ArticleSize.ArticleId and ArticleSize.Active=1 
					 LEFT JOIN VariantCode ON ol.articleid=VariantCode.articleid AND ol.articleColorid=VariantCode.articleColorid AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.Active=1
					 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
				$queryClause = sprintf ('o.seasonid=%d and ol.orderid=o.id and ol.articleid=article.id
								and o.active=1 and ol.active=1 and article.active=1
								', $Id) ;
			}  
			$queryOrderBy = 'ArticleNumber, VariantColor, ArticleSize.DisplayOrder' ;
			break ;

		default :
		    // Display
		    $HideSeason = true ;

		    // Clause
		    $queryClause = "VariantCode.Active=1" ;
		    break ;
	    }
//return 'select ' . $queryFields . ' from ' . $queryTables . ' where ' . $queryClause ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrderBy) ;
		
	    if (is_string($Result)) return $Result ;
		$query='Select * from parameter' ;
	    $Result2 = dbQuery ($query) ;
	    return listView ($Result2, 'view') ;

	case 'store' :
	case 'inbound' :
	    $query = sprintf ("SELECT * FROM container Limit 1", 10) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    
	    return 0;
    }

    if ($Id <= 0) return 0 ;
    

    return 0 ;
?>
