<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    ?>
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appStore">
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
            <h2>Next available position: {{nextposition}}</h2>
            <h3>(scan SCCC and postion to store in stock)</h3>
            <h2>SSCC Scanned: {{sscc}} {{oldposition}}</h2>
            <h2>Position Scanned: {{position}}</h2>
        </center>
        </br>
        </br>
        </br>
        <center>
            <div class="form">
                <div class="button" v-on:click="positionContainer()" >Store item</div>
            </div>
        </center>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/shipping-store.js"></script>
<style>
    #appStore{
        position: relative;
    }
    #appStore table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        width: 100px;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        /*float: left;*/
        cursor: pointer;
        margin: auto;

    }
    h3{
        font-size: 15px;
    }
    h2{
        font-size: 18px;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
</style>
<?php

    return 0 ;

?>
