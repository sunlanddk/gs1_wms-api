<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    ?>
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appStoreSku">
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
            <h3>(scan GTIN or SSCC and position to store in stock)</h3>
            <h2>Scanned: {{gtin}}{{sscc}}</h2>
            <h2 v-if="gtin != ''">GTIN Quantity: {{quantity}}</h2>
            <div v-if="gtin != ''">
                <div class="addToQty" v-on:click="add(1)">+1</div>
                <div class="addToQty" v-on:click="add(10)">+10</div>
                <div class="addToQty" v-on:click="add(100)">+100</div>
            </div>
            <h2>Position Scanned: {{position}}</h2>
        </center>
        </br>
        </br>
        </br>
        <center>
            <div class="form">
                <div class="button" v-on:click="positionSku()" >Store item</div>
                <div class="button" v-on:click="reset()" >Reset</div>
            </div>
        </center>
        <div v-if="preloader" class="preloader"><div class="rotator"></div></div>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/module/shipping/shipping-store-sku.js?v=6"></script>
<style>
    #appStore{
        position: relative;
    }
    #appStore table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        /*float: left;*/
        display: inline-block;
        cursor: pointer;
    }
    .form{
    	text-align: center;
    }
    .addToQty{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin: 0 12px;
        /* float: left; */
        display: inline-block;
        cursor: pointer;
    }
    h3{
        font-size: 15px;
    }
    h2{
        font-size: 18px;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .preloader{
        position: fixed;
        z-index: 99999999;
        left: 0;
        top: 0;
        background: rgba(255, 255, 255, 0.7);
        width: 100%;
        height: 100%;
    }
    .preloader .rotator{
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border: 5px solid #3b9ac4;
        border-radius: 50%;
        border-top: 5px solid #01205a;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
</style>
<?php

    return 0 ;

?>
