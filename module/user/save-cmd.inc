<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/perm.inc' ;
    
    function checkfield ($fieldname, $value) {
	global $User, $Id, $my ;
	switch ($fieldname) {
/*	    case 'Login' :
			if ($value == 0 and $Id == $User['Id']) return "Login can not be reset for your own account" ;
			return true ;
*/
	    case "Loginname":
			if ($Navigation["Parameters"] == "new") {
				$query = sprintf ("SELECT * FROM User WHERE Active=1 AND Loginname=\"%s\" AND Id<>%d", $value, $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Loginname is already in use" ;
			}
			return true ;

	    case "Password":
			if ($value == "") return false ;
			return true ;
	}
	return false ;
    }

    $fields = array (
		"Loginname"	=> array ("mandatory" => true,		'permission' => 'function',	'mark' => 'admin',	"check" => true),
		"Password"	=> array (								"check" => true),
		"Title"		=> array (),
		"FirstName"	=> array ("mandatory" => true),
		"LastName"	=> array ("mandatory" => true),
//		"CompanyId"	=> array ("mandatory" => true,		'permission' => 'function',	'mark' => 'admin'),	
		"CompanyId"	=> array ("type" => "set"),	
		"CustomerCompanyId"	=> array ("type" => "set"),	
		"AgentCompanyId"	=> array ("type" => "integer"),	
		"AvatarId"	=> array ("type" => "integer"),	
		"Email"		=> array (),
		"PhoneBusiness"	=> array (),
		"PhoneDirect"	=> array (),
		"PhoneMobile"	=> array (),
		"PhonePrivate"	=> array (),
		"Address1"	=> array (),
		"Address2"	=> array (),
		"ZIP"		=> array (),
		"City"		=> array (),
		"Country"	=> array (),
		"TimeZoneId"	=> array ("type" => "integer"),
		"RoleId"	=> array ("type" => "integer",	 	"mandatory" => true),
		"ProjectId"	=> array ("type" => "integer",		'permission' => 'function',	'mark' => 'admin'),
		"ProjectSelect"	=> array ("type" => "checkbox",		'permission' => 'function',	'mark' => 'admin'),
		"Login"		=> array ("type" => "checkbox",		'permission' => 'function',	'mark' => 'admin'),
		"AdminSystem"	=> array ("type" => "checkbox",		"permission" => "admin"),
		"SalesRef" => array ("type" => "checkbox"),
		"AgentManager" => array ("type" => "checkbox")
//		"Extern" => array ("type" => "checkbox",		"permission" => "function", 'mark' => 'admin')
    ) ;

    if (!permAdminSystem()) {
	unset ($fields['AdminSystem']) ;
    }
    
    switch ($Navigation["Parameters"]) {
	case "new":
	    $Id = -1 ;
		$fields['CompanyId']['value'] = 787 ;
		$fields['CustomerCompanyId']['value'] = 787 ;
	    break ;

	case "my":
	    $Id = $User["Id"] ; 
	    unset ($fields['RoleId']) ;
	    unset ($fields['AdminSystem']) ;
	    unset ($fields['ProjectSelect']) ;
	    unset ($fields['Login']) ;
	    unset ($fields['Loginname']) ;
	    unset ($fields['ProjectId']) ;
	    unset ($fields['ProjectSelect']) ;
	    break ;
    }    
   
    return saveFields ("User", $Id, $fields) ;
?>
