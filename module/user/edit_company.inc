<?php
   
    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/perm.inc' ;
    
    $my = true ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
	printf ("<tr><td><p>Company</p></td><td>%s</td></tr>\n", htmlDBSelect ("CustomerCompanyId style='width:250px'", $Record["CustomerCompanyId"], 
	              sprintf("SELECT Company.Id, Company.Name AS Value 
							FROM (company) 
							WHERE company.TypeCustomer=1 and Company.Active=1 ORDER BY Name", $User['CustomerCompanyId']))) ;  
    print htmlItemSpace() ;

    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
