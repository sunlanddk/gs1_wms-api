<?php

    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    // Get dates
    $FromDate = utsDateString($_GET['fromdate']) ;
    $ToDate = utsDateString($_GET['todate']) ;

    // Header
    printf ("<br>\n") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>From</td><td class=itemfield>%s</td></tr>\n", date('Y-m-d', $FromDate)) ;
    printf ("<tr><td class=itemlabel>To</td><td class=itemfield>%s</td></tr>\n", date('Y-m-d', $ToDate)) ;
    printf ("<tr><td class=itemlabel>Operator</td><td class=itemfield>%s</td></tr>\n", tableGetField ('User', "CONCAT(FirstName,' ',LastName,' (',Loginname,')')", $Id)) ;
    printf ("</table>\n") ;

    // Any interval
    if ($FromDate == 0 or $ToDate == 0 or $Id <= 0) return 0 ;
    
    // Query
    $query = sprintf ("SELECT BundleWork.BundleId, BundleWork.StartTime, CONCAT(MachineGroup.Number,Operation.Number) AS Operation, BundleWork.Quantity, BundleWork.UsedMinutes, currentstyleoperation.ProductionMinutes FROM (WorkSheet, BundleWork) LEFT JOIN currentstyleoperation ON currentstyleoperation.Id=BundleWork.StyleOperationId LEFT JOIN Operation ON Operation.Id=currentstyleoperation.OperationId LEFT JOIN MachineGroup ON MachineGroup.Id=currentstyleoperation.MachineGroupId WHERE WorkSheet.UserId=%d AND BundleWork.WorkSheetId=WorkSheet.Id AND BundleWork.StartTime>='%s' AND BundleWork.StartTime<'%s' AND BundleWork.Active=1 ORDER BY BundleWork.Id", $Id, dbDateEncode($FromDate), dbDateEncode($ToDate)) ;
    $Result = dbQuery ($query) ;
 
    // List of bundles
    printf ("<br><table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=57><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead width=60><p class=listhead>Operation</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Used</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Calculated</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    $sum = array() ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' style='cursor:pointer;' %s></td>", './image/toolbar/bundle.gif', navigationOnClickMark ('bundle', $row["BundleId"])) ;
	printf ("<td><p class=list>%08d</p></td>", $row['BundleId']) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Operation'])) ;
	printf ("<td><p class=list>%s</p></td>", date('Y-m-d H:i', dbDateDecode($row['StartTime']))) ;
	$sum['Quantity'] += $row['Quantity'] ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['Quantity']) ;
	$v = $row['UsedMinutes'] ;
	$sum['UsedMinutes'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	$v = $row['Quantity']*$row['ProductionMinutes'] ;
	$sum['ProductionMinutes'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	printf ("</tr>\n") ;
	$sum['Bundles'] += 1 ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No registrations</p></td></tr>\n") ;
    } else {
	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $sum['Bundles']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $sum['Quantity']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['UsedMinutes']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['ProductionMinutes']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
