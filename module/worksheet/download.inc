<?php

    require_once 'lib/uts.inc' ;
    require_once 'lib/log.inc' ;
    
    // Get dates
    $FromDate = utsDateString($_GET['FromDate']) ;
    $ToDate = utsDateString($_GET['ToDate']) ;

    // header
    // header('Content-Length: '.$size); 
    header ('Content-Type: text/plain') ;
    header ('Content-Disposition: attachment; filename=downlaod.txt') ;
    header ('Last-Modified: '.date('D, j M Y G:i:s T')) ;

    // Any interval
    if ($FromDate == 0 or $ToDate == 0) return 0 ;
    
    // Query
    $query = sprintf ("SELECT BundleWork.Id as BId, BundleWork.StartTime, BundleWork.Quantity, BundleWork.UsedMinutes, User.Loginname, CONCAT(User.FirstName,' ',User.LastName) AS Name, CONCAT(Article.Number,'-',Style.Version) AS StyleNumber, CONCAT(Production.CaseId,'/',Production.Number) AS ProductionNumber, currentstyleoperation.ProductionMinutes, MachineGroup.Number AS MachineNumber, CONCAT(MachineGroup.Number,Operation.Number) AS Operation
    FROM (WorkSheet, BundleWork, currentstyleoperation)
    LEFT JOIN Production ON Production.Id=BundleWork.ProductionId
    LEFT JOIN `Case` ON Case.Id=Production.CaseId 
    LEFT JOIN Article ON Article.Id=Case.ArticleId
    LEFT JOIN Style ON Style.Id=Production.StyleId
    LEFT JOIN Operation ON Operation.Id=currentstyleoperation.OperationId 
    LEFT JOIN MachineGroup ON MachineGroup.Id=currentstyleoperation.MachineGroupId 
    LEFT JOIN User ON User.Id=WorkSheet.UserId 
    WHERE BundleWork.StartTime>='%s' AND BundleWork.StartTime<'%s' AND BundleWork.Active=1 AND BundleWork.WorkSheetId=WorkSheet.Id AND currentstyleoperation.Id=BundleWork.StyleOperationId", dbDateEncode($FromDate), dbDateEncode($ToDate)) ;
    $Result = dbQuery ($query) ;

    // List of work
    while ($row = dbFetch($Result)) {
	$t = dbDateDecode($row['StartTime']) ;
	$s = 
	    date('Y-m-d', $t) .						// Date
	    ',' .
	    $row['Loginname'] .						// Sewer's code as it is in production
	    ',' .
	    $row['Name'] .						// Name, Surname
	    ',' .
	    date('H:i', $t) .						// Time for starting working on the Operation
	    ',' .
	    date('H:i', $t + $row['UsedMinutes']*60) .			// Time for ending working on the Operation
	    ',' .
	    $row['ProductionNumber'] .					// Order Number
	    ',' .
	    $row['StyleNumber'] .					// Style Number
	    ',' .
	    $row['Quantity'] .						// Quantity of pcs
	    ',' .
	    $row['Operation'] .						// Operation Number
	    ',' .
	    $row['MachineNumber'] .					// Code/type of sewing machine
	    ',' .
	    sprintf ("%01.2f", $row['ProductionMinutes']) .		// Time consumption for 1 operation
	    "\r\n" ;
	print $s ;
   }

    dbQueryFree ($Result) ;

    return 0 ;
?>
