<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
    
    // Get date of work for last registration
    $query = sprintf ("SELECT StartTime FROM WorkSheet ORDER BY Id DESC LIMIT 1") ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $WorkDate = ($row) ? dbDateDecode($row['StartTime']) : time() ;
    
    // Operator information
    if (isset($_GET['operator'])) {
	$Operator = $_GET['operator'] ;
	
    	// Locate entry
	$query = sprintf ("SELECT Id, CONCAT(FirstName,' ',LastName) AS FullName FROM User WHERE Active=1 AND Login=0 AND Loginname=\"%s\"", $Operator) ;  
	$result = dbQuery ($query) ;
	$Record = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Update record
	if ($Record) {
	    $Navigation['Focus'] = 'Time[1]' ;
	} else {
	    $Record['FullName'] = ' - Not Found - ' ;
	}	
    }

//    $Init = array (
//	'Operator'	=> 361,
//	'EndTime'	=> '1100',
//	1		=> array ('Time' => '1000') 
//    ) ;
       	
    // Navigation script
    printf ("<script type='text/javascript'>\n") ;
    printf ("function operatorselect() {location.replace(\"index.php?nid=%d&operator=\"+document.appform.Operator.value)}\n", $Navigation["Id"]) ;
    printf ("</script>\n") ;
    
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;
    printf ("<input type=hidden name=UserId value=%d>\n", $Record['Id']) ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;
    
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Operator</td><td width=80><input type=text name=Operator size=5 maxlength=5 tabindex=1 onChange='operatorselect()' value=\"%s\" style='width:42px;'></td><td class=itemfield>%s</td></tr>\n", $Operator, $Record['FullName']) ;  
    printf ("</table>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Date</td><td>%s</td></tr>\n", formDate ('WorkDate', $WorkDate)) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td class=listhead width=40></td>") ;
    printf ("<td class=listhead width=47><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=68><p class=listhead>Bundle</p></td>") ;
    printf ("<td class=listhead width=32><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=32><p class=listhead>Qty</p></td>") ;
    printf ("<td class=listhead width=50></td>") ;
    printf ("<td class=listhead width=47><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=68><p class=listhead>Bundle</p></td>") ;
    printf ("<td class=listhead width=32><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=32><p class=listhead>Qty</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    
    function mkfield ($n) {
	global $Init ;
	printf ("<td align=right><p class=list>%d</p></td>", $n) ;
	printf ("<td><input type=text style='width:37px;' name=Time[%d] maxlength=12 size=5 tabindex=%d value=\"%s\" onchange='chklabel(this, %d)'></td>", $n, $n*4+1, $Init[$n]['Time'], $n) ;
	printf ("<td><input type=text style='width:58px;' name=Bundle[%d] maxlength=12 size=8 tabindex=%d value=\"%s\" onchange='chklabel(this, %d)'></td>", $n, $n*4+2, $Init[$n]['Bundle'], $n) ;
	printf ("<td><input type=text style='width:22px;' name=No[%d] maxlength=12 size=12 tabindex=%d value=\"%s\" onchange='chklabel(this, %d)'></td>", $n, $n*4+3, $Init[$n]['No'], $n) ;
	printf ("<td><input type=text style='width:22px;' name=Quantity[%d] maxlength=12 size=2 tabindex=%d value=\"%s\" onchange='chklabel(this, %d)'></td>\n", $n, $n*4+4, $Init[$n]['Quantity'], $n) ;
    }	
    
    define("LINES", 15);
    
    for ($n = 1 ; $n <= LINES ; $n++) {
	printf ("<tr>\n") ;
	mkfield ($n) ;
	mkfield ($n+LINES) ;
	printf ("</tr>\n") ;
    }
    
    printf ("<tr>") ;
    printf ("<td colspan=5></td>") ;	
    printf ("<td align=right><p class=list>END</p></td>") ;
    printf ("<td><input type=text style='width:37px;' name=EndTime maxlength=5 size=5 tabindex=200 value=\"%s\"></td>", $Init['EndTime']) ;
    printf ("</tr>\n") ;
 
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

?>
<script type="text/javascript">
function chklabel(field, n) {
    // Validate label
    var label=field.value ;
    if (label.length != 12) return 0 ;
    if (label.substr(11,1) != '0') return 0 ;

    // Type of label
    switch (label.substr(0,1)) {
        case '3':
	    // Clear field
	    field.value = '' ;
	    
	    // Set bundlenumber
	    var e = document.getElementsByName('Bundle['+n+']') ; 
	    e[0].value = label.substr(1,8) ;

	    // Set operatione linenumber
	    e = document.getElementsByName('No['+n+']') ; 
	    e[0].value = label.substr(9,2) ;   
	    break ;
	    
        case '2':
	    // Clear field
	    field.value = '' ;
	    
	    // Set bundlenumber
	    var e = document.getElementsByName('Bundle['+n+']') ; 
	    e[0].value = label.substr(1,8) ;

	    // Clear operatione linenumber
	    e = document.getElementsByName('No['+n+']') ; 
	    e[0].value = '' ;   
	    break ;
	    
	 default:
	     return 0 ;
    }

    // Clear quantity
    e = document.getElementsByName('Quantity['+n+']') ; 
    e[0].value = '' ;   
    
    // Focus
    e = document.getElementsByName('Time['+(n+1)+']') ; 
    if (e[0]) {
       	e[0].focus() ;
	return 0 ;
    }
    e = document.getElementsByName('EndTime') ; 
    if (e[0]) e[0].focus() ;
}   
</script>
<?php
    
    return 0 ;
?>
