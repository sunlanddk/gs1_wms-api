<?php

    require_once 'lib/uts.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/html.inc' ;

    // Get dates
    $FromDate = utsDateString($_GET['FromDate']) ;
    $ToDate = utsDateString($_GET['ToDate']) ;
    $CompanyId = $_GET['CompanyId'] ;

    // Header
    printf ("<br>\n") ;
    printf ("<form method=GET name=appform>\n") ;
    printf ("<input type=hidden name=nid value=%s>\n", $Navigation['Id']) ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>From</td><td>%s</td></tr>\n", formDate ('FromDate', $FromDate, null, 'tR')) ;
    printf ("<tr><td class=itemlabel>To</td><td>%s</td></tr>\n", formDate ('ToDate', $ToDate, null, 'tR')) ;
	printf ("<tr><td><p>Company</p></td><td>%s</td></tr>\n", htmlDBSelect ("CompanyId style='width:250px'", $CompanyId,
			 "Select 0 as Id, '-Select-' as Value Union
			  Select Id, `Value`
			  From (SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value
					FROM Company
					WHERE Active=1 AND Id in (18, 4606)
					ORDER BY Name) comp
			  ")
			) ;  
    printf ("</table>\n") ;
    printf ("</form>\n") ;
    // Any interval
    if ($FromDate == 0 or $ToDate == 0) return 0 ;
    
    if ($CompanyId>0) {
		$CompanyLimit = ' AND User.CompanyId=' . $CompanyId ;
    } else {
		$CompanyLimit = '' ;
	}
    // Query
    $query = sprintf ("SELECT WorkSheet.UserId, COUNT(BundleWork.Id) AS Bundles, Company.Name as CompanyName,
						SUM(BundleWork.Quantity) AS Quantity, SUM(BundleWork.UsedMinutes) AS UsedMinutes, 
						SUM(BundleWork.Quantity*currentstyleoperation.ProductionMinutes) AS ProductionMinutes, 
						User.Loginname, CONCAT(User.FirstName,' ',User.LastName) AS Name, User.Title 
						FROM (WorkSheet, BundleWork, currentstyleoperation) 
						LEFT JOIN User ON User.Id=WorkSheet.UserId 
						LEFT JOIN Company ON User.CompanyId=Company.Id
						WHERE BundleWork.StartTime>='%s' AND BundleWork.StartTime<'%s' AND BundleWork.Active=1 
							AND BundleWork.WorkSheetId=WorkSheet.Id AND currentstyleoperation.Id=BundleWork.StyleOperationId ", 
						dbDateEncode($FromDate), dbDateEncode($ToDate)) 
						. $CompanyLimit 
						. " GROUP BY WorkSheet.UserId ORDER BY User.LoginName" ;
//return $query ;
    $Result = dbQuery ($query) ;
 
    // List of workgroups
    printf ("<br><table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=57><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Title</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Company</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Bundles</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Used</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Calculated</p></td>") ;
    printf ("<td class=listhead width=95 align=right><p class=listhead>Efficiency</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    $dateinterval = sprintf ("fromdate=%s&todate=%s", date('Y-m-d', $FromDate), date('Y-m-d', $ToDate)) ;
    $total = array () ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/users.gif', navigationOnClickLink ('detail', $row['UserId'], $dateinterval)) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Loginname'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Name'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Title'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['CompanyName'])) ;
	$b = $row['Bundles'] ;
	$total['Bundles'] += $b ;
	printf ("<td align=right><p class=list>%d</p></td>", $b) ;
	$q = $row['Quantity'] ;
	$total['Quantity'] += $q ;
	printf ("<td align=right><p class=list>%d</p></td>", $q) ;
	$u = $row['UsedMinutes'] ;
	$total['UsedMinutes'] += $u ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($u)) ;
	$p = $row['ProductionMinutes'] ;
	$total['ProductionMinutes'] += $p ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($p)) ;
	if ($u != 0) printf ("<td align=right><p class=list>%d%%</p></td>", 100*$p/$u) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No registrations</p></td></tr>\n") ;
    } else {

	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", dbNumRows($Result)) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $total['Bundles']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $total['Quantity']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $total['UsedMinutes']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $total['ProductionMinutes']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%s</p></td>", ($total['UsedMinutes'] == 0) ? "" : sprintf ("%d%%", 100*$total['ProductionMinutes']/$total['UsedMinutes'])) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;


    }
    printf ("</table>\n") ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
