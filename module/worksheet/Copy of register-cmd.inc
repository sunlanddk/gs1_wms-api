<?php

    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
//  require_once 'lib/log.inc' ;

    $ErrorText = 'while processing the worksheet, the following errors has been found:<br><br>' ;
    $ErrorNote = '' ;

    // Get maximum number of lines
    $LineMax = count($_POST['Bundle']) ;
    if ($LineMax <= 0 or $LineMax > 100) return sprintf ("%s(%d) invalid linecount, value %d", __FILE__, __LINE__, $LineMax) ;
    
    // Get and validate UserId
    $UserId = (int)$_POST['UserId'] ;
    if ($UserId == 0) {
	$ErrorNote .= sprintf ("No Operator selected<br><br>") ;
    } else {
	$query = sprintf ("SELECT Id FROM User WHERE Id=%d AND Active=1", $UserId) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ($row['Id'] != $UserId) return sprintf ("%s(%d) operator not found, id %d", __FILE__, __LINE__, $UserId) ;
    }
    
    // Get and validate date
    if (is_null($WorkDate = utsDateString($_POST['WorkDate']))) return sprintf ("%s(%d) invalid date", __FILE__, __LINE__) ;
    
    // Walk through label lines to get information
    $Line = array() ;
    for ($n = 1 ; $n <= $LineMax ; $n++) {
	// Get and validate time
	$s = trim ($_POST['Time'][$n]) ;
	if ($s != '') {	    
	    if (is_null($Line[$n]['Time'] = utsTimeString($s))) $ErrorNote .= sprintf ("Line %d: invalid time<br><br>", $n) ;
	    if ($Line[$n]['Time'] < $Line[1]['Time']) $Line[$n]['Time'] += 60*60*24 ;
	}

	// Get and validate BundleId
	$s = $_POST['Bundle'][$n] ;
	$v = (int)$s ;
	$s = ltrim(trim($s), '0') ;
	if ($s != '') {
	    if ((string)$v !== $s) $ErrorNote .= sprintf ("Line %d: invalid bundlenumber<br><br>", $n) ;
	    $Line[$n]['BundleId'] = $v ;	
	}
	
	// Get and validate Operation No
	$s = $_POST['No'][$n] ;
	$v = (int)$s ;
	$s = ltrim(trim($s), '0') ;
	if ($s != '') {
	    if ((string)$v !== $s or $v > 99) $ErrorNote .= sprintf ("Line %d: invalid operationnumber<br><br>", $n) ;
	    $Line[$n]['No'] = $v ;
	}	    
	
	// Get and validate Quantity
	$s = $_POST['Quantity'][$n] ;
	$v = (int)$s ;
	$s = ltrim(trim($s), '0') ;
	if ($s != '') {
	    if ((string)$v !== $s or $v > 99) $ErrorNote .= sprintf ("Line %d: invalid quantity<br><br>", $n) ;
	    $Line[$n]['Quantity'] = $v ;
	}	    
    }

    // Get and validate EndTime
    if ($_POST['EndTime'] == '') $ErrorNote .= sprintf ("No End-time<br><br>") ;
    else if (is_null($EndTime = utsTimeString($_POST['EndTime']))) $ErrorNote .= sprintf ("Invalid END-time<br><br>") ;
    if ($EndTime < ($Line[1]['Time'])) $EndTime += 60*60*24 ;
    
    // Calculate real number of lines and ensure continious registration
    $LineCount = 0 ;
    foreach ($Line as $n => $l)  {
	if ($n != $LineCount+1) $ErrorNote .= sprintf ("Operations must be registrated in a continious line<br><br>") ;
	if ($l['BundleId'] == 0) $ErrorNote .= sprintf ("Line %d: no bundle number<br><br>", $n) ;
	if ($l['No'] == 0) $ErrorNote .= sprintf ("Line %d: no Operation No<br><br>", $n) ;
	$LineCount = $n ;
    }
    if ($LineCount == 0) $ErrorNote .= sprintf ("No operatons registrated") ;
    else if (is_null($Line[1]['Time'])) $ErrorNote .= sprintf ("Line 1: time for starting must be pressent<br><br>") ;

    // Check length of workday
    if (($EndTime - $Line[1]['Time']) > 16*60*60) $ErrorNote .= sprintf ("Workday above 16 hours<br><br>") ;
	
    // Errors ?
    if ($ErrorNote != '') return $ErrorText . $ErrorNote ;

    // Walk through lines to gather Information 
    foreach ($Line as $n => $l)  {
	$query = sprintf ("SELECT Bundle.ProductionId, Production.SourceLocationId, Production.SourceFromNo, Production.SourceToNo, currentstyleoperation.Id AS StyleOperationId, currentstyleoperation.ProductionMinutes, SUM(BundleWork.Quantity) AS BundleWorkQuantity, IFNULL(BundleQuantity.Quantity,Bundle.Quantity) AS BundleQuantity FROM (Bundle, Production, currentstyleoperation) LEFT JOIN BundleWork ON BundleWork.BundleId=Bundle.Id AND BundleWork.StyleOperationId=currentstyleoperation.Id AND BundleWork.Active=1 LEFT JOIN BundleQuantity ON BundleQuantity.BundleId=Bundle.Id AND BundleQuantity.StyleOperationId=currentstyleoperation.Id AND BundleQuantity.Active=1 WHERE Bundle.Id=%d AND Bundle.Active=1 AND Production.Id=Bundle.ProductionId AND currentstyleoperation.StyleId=Production.StyleId AND currentstyleoperation.Active=1 AND currentstyleoperation.No=%d GROUP BY Bundle.Id", $l['BundleId'], $l['No']) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if (!$row) $ErrorNote .= sprintf ("Line %d: operation not found, bundle %d, no %d<br><br>", $n, $l['BundleId'], $l['No']) ;
	$Line[$n] = array_merge ($l, $row) ;
    }

    // Check for double registrations
    foreach ($Line as $n => $l)  {
	for ($i = 1 ; $i < $n ; $i++) {
	    if ($l['BundleId'] == $Line[$i]['BundleId'] and $l['No'] == $Line[$i]['No']) {
		$ErrorNote .= sprintf ("Line %d: operation (bundle %08d, operation No %d) also appears in line %d<br><br>", $n, $l['BundleId'], $l['No'], $i) ;
	    }
	}
    }

    if ($ErrorNote != '') return $ErrorText . $ErrorNote ;

    // Compute and validate quantity where not entered
    foreach ($Line as $n => $l)  {
	if ($l['SourceLocationId'] > 0 and $l['SourceFromNo'] <= $l['No'] and $l['SourceToNo'] >= $l['No']) {
	    // Outsourced operation
	    $ErrorNote .= sprintf ("Line %d: operation done externaly, no registration possible<br><br>", $n) ;
	    continue ;
	}
	
	$QuantityLeft = $l['BundleQuantity'] - $l['BundleWorkQuantity'] ;
	if ($l['Quantity'] == 0) {
	    // No quantity specified
	    if ($QuantityLeft == 0) $ErrorNote .= sprintf ("Line %d: all work (%d) has allready been done<br><br>", $n, $l['BundleQuantity']) ;
	    $Line[$n]['Quantity'] = $QuantityLeft ;
	    continue ;
	}
	
	if ($l['Quantity'] > $QuantityLeft) $ErrorNote .= sprintf ("Line %d: specified quantity (%d) exceded quantity left (%d)<br><br>", $n, $l['Quantity'], $QuantityLeft) ;
    }

    // Calculate missing timestamps
    $Line[$LineCount+1]['Time'] = $EndTime ;
    $lasttimeindex = 1 ;
    $ProductionMinutes = 0 ;
    foreach ($Line as $n => $l)  {
	if ($n > 1) {
	    if (isset($l['Time'])) {
		if ($n <= $lasttimeindex) continue ;
		$delta = $l['Time']-$Line[$lasttimeindex]['Time'] ;
		if ($delta <= 0) $ErrorNote .= sprintf ("Line %d: time must increase as operations are executed<br><br>", $n) ;
		for ($i = $lasttimeindex+1 ; $i < $n ; $i++) $Line[$i]['Time'] = $Line[$i-1]['Time'] + $delta * $Line[$i-1]['Quantity'] * $Line[$i-1]['ProductionMinutes'] / $ProductionMinutes ;
		$lasttimeindex = $n ;
		$ProductionMinutes = 0 ;
	    }
	}
	$ProductionMinutes += $l['Quantity'] * $l['ProductionMinutes'] ;
    }

//require_once 'lib/log.inc' ;
//logPrintVar ($Line, 'Line') ;
//printf ("Error: %s", $ErrorNote) ;

    if ($ErrorNote != '') return $ErrorText . $ErrorNote ;

    // Create worksheed
    $row = array (
        'UserId'	=> $UserId,
        'StartTime'	=> dbDateEncode($WorkDate+$Line[1]['Time']),
	'EndTime'	=> dbDateEncode($WorkDate+$EndTime),
	'Active'	=> 1
    ) ;
//logPrintVar ($row, "WorkSheet") ;
    $WorkSheedId = tableWrite ('WorkSheet', $row) ;
    
    // Opdate bundle operations
    foreach ($Line as $n => $l)  {
	if ($l['BundleId'] > 0) {
	    $row = array (
		'BundleId'		=> $l['BundleId'],
		'ProductionId'		=> $l['ProductionId'],
		'StyleOperationId'	=> $l['StyleOperationId'],
		'WorkSheetId'		=> $WorkSheedId,
		'Quantity'		=> $l['Quantity'],
		'StartTime'		=> dbDateEncode($WorkDate+$l['Time']),
		'UsedMinutes'		=> sprintf ("%01.2f",  ($Line[$n+1]['Time']-$l['Time'])/60),
		'Active'		=> 1
	    ) ;
//logPrintVar ($row, 'BundleWork') ;
	    tableWrite ('BundleWork', $row) ;
	}
    }

    return 0 ;
?>
