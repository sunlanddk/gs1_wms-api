<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Order', $Record['Id']) ;
    flag ($Record, 'Ready') ;
    itemSpace () ;
    itemHeader () ;

	itemFieldRaw ('Supplier', formDBSelect ('SupplierCompanyId', 0, 
				"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company 
				 WHERE (TypeSupplier=1 or Internal=1) AND Active=1 ORDER BY Name", 'width:250px;')) ;  
	itemSpace () ;
    itemSpace () ;
//    itemFieldRaw ('Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:100px;')) ;
    itemSpace () ;
    itemSpace () ;
    itemEnd () ;

    formEnd () ;

    return 0 ;
?>
