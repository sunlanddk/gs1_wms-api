<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    $MyCompanyId = parameterGet ('CompanyMy') ;
    
		
    itemStart () ;
    itemSpace () ;
    if (($Navigation['Parameters'] != 'new') and ($Navigation['Parameters'] != 'new-purch')) 
		itemField ('Order', (int)$Record['Id']) ;     
		
    itemField ('Customer', sprintf ('%s (%d)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     

    if ($Navigation['Parameters'] == 'new-purch') {
		$query = sprintf('SELECT * from requisition where id=%d', (int)$Record['PurchaseOrderId']) ;
		$res = dbQuery ($query) ;
		$PurchaseOrder = dbFetch ($res) ;
	    $ToCompanyId = (int)$PurchaseOrder['CompanyId'] ;
	    itemField ('To Company', tableGetField('Company', 'Name', $ToCompanyId)) ;
	    $PurchaseOrderId = $Record['PurchaseOrderId'] ;     
	}
    itemSpace () ;
    itemEnd () ;

    
    switch ($Navigation['Parameters']) {
	case 'new-purch' :
	    $Company = $Record ;

	    // Default Values
	    unset ($Record) ;
	    $Record['SalesUserId'] = $User['Id'] ;
		$Record['Description'] = 'Generated from purchase Order ' .  $PurchaseOrderId ;
		$Record['Reference'] = 'O' . $PurchaseOrderId . ' - ' . tableGetField ('User','FirstName',$PurchaseOrder['PurchaseUserId']) . ' ' . tableGetField ('User','LastName',$PurchaseOrder['PurchaseUserId'] );
		$Record['PurchaseOrderId'] = $PurchaseOrderId ;
		$Record['ToCompanyId'] = $ToCompanyId ;
	    $Record['CompanyId'] = $Company['Id'] ;
	    $Record['CurrencyId'] = $PurchaseOrder['CurrencyId']>0 ? $PurchaseOrder['CurrencyId'] : $Company['CurrencyId'] ;
	    $Record['PaymentTermId'] = $PurchaseOrder['PaymentTermId']>0 ? $PurchaseOrder['PaymentTermId'] : $Company['PaymentTermId'] ;
	    $Record['DeliveryTermId'] = $PurchaseOrder['DeliveryTermId']>0 ? $PurchaseOrder['DeliveryTermId'] : $Company['DeliveryTermId'] ;
	    $Record['CarrierId'] = $PurchaseOrder['CarrierId']>0 ? $PurchaseOrder['CarrierId'] : $Company['CarrierId'] ;
	    $Record['InvoiceHeader'] = $PurchaseOrder['RequisitionHeader'] ;
	    $Record['OrderTypeId'] = 1 ;
	    $Record['ReceivedDate'] = dbDateEncode(time()) ;
	    
	    if ((int)$Company['DeliveryCountryId'] > 0) {
			$Record['Address1'] = $Company['DeliveryAddress1'] ;
			$Record['Address2'] = $Company['DeliveryAddress2'] ;
			$Record['ZIP'] = $Company['DeliveryZIP'] ;
			$Record['City'] = $Company['DeliveryCity'] ;
			$Record['CountryId'] = $Company['DeliveryCountryId'] ;
	    } else {
			$Record['Address1'] = $Company['Address1'] ;
			$Record['Address2'] = $Company['Address2'] ;
			$Record['ZIP'] = $Company['ZIP'] ;
			$Record['City'] = $Company['City'] ;
			$Record['CountryId'] = $Company['CountryId'] ;
	    }	    
	    
		break ;
	case 'new' :
	    $Company = $Record ;

	    // Default Values
	    unset ($Record) ;
	    $Record['SalesUserId'] = $Company['SalesUserId'] ;
	    $Record['ToCompanyId'] =  $User['CompanyId'] ;
	    $Record['CompanyId'] = $Company['Id'] ;
	    $Record['CurrencyId'] = $Company['CurrencyId'] ;
	    $Record['PaymentTermId'] = $Company['PaymentTermId'] ;
	    $Record['DeliveryTermId'] = $Company['DeliveryTermId'] ;
	    $Record['CarrierId'] = $Company['CarrierId'] ;
	    $Record['OrderTypeId'] = 1 ;
	    $Record['InvoiceDraft'] =  $Company['InvoiceDraft'] ;
	    $Record['PartDelivery'] =  $Company['PartDelivery'] ;
	    $Record['PartDeliveryLine'] =  $Company['PartDeliveryLine'] ;
	    $Record['ReceivedDate'] = dbDateEncode(time()) ;
	    
	    if ((int)$Company['DeliveryCountryId'] > 0) {
			$Record['Address1'] = $Company['DeliveryAddress1'] ;
			$Record['Address2'] = $Company['DeliveryAddress2'] ;
			$Record['ZIP'] = $Company['DeliveryZIP'] ;
			$Record['City'] = $Company['DeliveryCity'] ;
			$Record['CountryId'] = $Company['DeliveryCountryId'] ;
	    } else {
			$Record['Address1'] = $Company['Name'] ;
			$Record['Address2'] = $Company['Address1'] ;
			$Record['ZIP'] = $Company['ZIP'] ;
			$Record['City'] = $Company['City'] ;
			$Record['CountryId'] = $Company['CountryId'] ;
	    }	    
	    
	    break ;
    }
     
    // Form
    //formStart () ;
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;


    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    if (strstr($Record['Description'], '@')) {
        $WholesaleProjectMember = 1 ;
    } else {
        $WholesaleProjectMember = 0 ;
    }

    itemFieldRaw ('', formHidden ('PurchaseOrderId', $Record['PurchaseOrderId'],40)) ;
    itemFieldRaw ('', formHidden ('CompanyId', $Record['CompanyId'],40)) ;
    itemFieldRaw ('Reference', formText ('Reference', $Record['Reference'], 40)) ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Received', formDate ('ReceivedDate',dbDateDecode($Record['ReceivedDate']))) ;
	else
		itemFieldRaw ('', formHidden ('ReceivedDate', '2001-01-01',40)) ;

//    itemFieldRaw ('Proposal', formCheckbox ('Proposal', $Record['Proposal'])) ;
    itemFieldRaw ('Invoice draft', formCheckbox ('InvoiceDraft', $Record['InvoiceDraft'])) ;
 
    //itemFieldRaw ('Consolidated Order', formDBSelect ('ConsolidatedId', (int)$Record['ConsolidatedId'], sprintf("SELECT co.Id, CONCAT(co.Id,' - ',GROUP_CONCAT(o.Id SEPARATOR ', ')) as Value FROM ConsolidatedOrder co LEFT JOIN `Order` o ON o.ConsolidatedId=co.Id AND o.Active=1 WHERE o.CompanyId=".$Record['CompanyId']." AND o.Done=0 AND co.Active=1 AND o.Id IS NOT NULL GROUP BY Id"), 'width:250px;', array(0 => 'New Consolidated Order'))) ;  
 	if ($Navigation['Parameters']=='new') {
	    itemFieldRaw ('PartDelivery', formCheckbox ('PartDelivery', $Record['PartDelivery'])) ;
	    itemFieldRaw ('PartDelivery Line', formCheckbox ('PartDeliveryLine', $Record['PartDeliveryLine'])) ;
	} else {
	    itemFieldRaw ('Part Delivery', '<input hidden name="PartDelivery" value="'.$Record['PartDelivery'].'" > '. ($Record['PartDelivery'] == 1 ? 'Yes' : 'No') . ' (can only be changed through consolidated order view)' );
    	itemFieldRaw ('Part Delivery Line', '<input hidden name="PartDeliveryLine" value="'.$Record['PartDeliveryLine'].'" > '. ($Record['PartDeliveryLine'] == 1 ? 'Yes' : 'No') . ' (can only be changed through consolidated order view)' )  ;
	}
    if (!$User['SalesRef']) 
		itemFieldRaw ('Consolidate with', formDBSelect ('ConsolidatedId', (int)$Record['ConsolidatedId'], sprintf("SELECT 
																													co.Id, 
																													CONCAT('SO ',GROUP_CONCAT(o.Id SEPARATOR ', '), ' (CO ' , co.Id,') - ', (SELECT MIN(ool.DeliveryDate)
																													FROM ConsolidatedOrder coo 
																													LEFT JOIN `Order` oo ON oo.ConsolidatedId=coo.Id AND oo.Active=1 
																													LEFT JOIN orderline ool ON ool.OrderId=oo.Id AND ool.Active=1
																													WHERE co.Id=coo.Id
																													GROUP BY coo.Id) ) as Value 
																													FROM ConsolidatedOrder co 
																													LEFT JOIN `Order` o ON o.ConsolidatedId=co.Id AND o.Active=1 
																													WHERE o.CompanyId=".$Record['CompanyId']." AND o.Done=0 AND co.Active=1 AND o.Id IS NOT NULL 
																													GROUP BY co.Id"), 'width:250px;', array(0 => 'New Consolidated Order'))) ;  
    if (!$User['SalesRef']) 
		flag ($Record, 'Ready') ;
    if (!$User['SalesRef']) 
		itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Type', formDBSelect ('OrderTypeId', (int)$Record['OrderTypeId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM OrderType WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Sales Ref', formDBSelect ('SalesUserId', (int)$Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=787 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND (User.Login=1 OR User.CompanyId=1) ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
  //  if (!$User['SalesRef']) 
//		itemFieldRaw ('Owner Company', formDBSelect ('ToCompanyId', (int)$Record['ToCompanyId'], sprintf('SELECT Company.Id, Compan&y.Name AS Value FROM Company WHERE Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    if (!$User['SalesRef']) 
		itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Currency', formDBSelect ('CurrencyId', (int)$Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    if (!$User['SalesRef']) 
		itemFieldRaw ('Payment Term', formDBSelect ('PaymentTermId', (int)$Record['PaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Delivery Terms', formDBSelect ('DeliveryTermId', (int)$Record['DeliveryTermId'], 'SELECT Id, CONCAT(Name," (",Description,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
    if (!$User['SalesRef']) 
		itemFieldRaw ('Carrier', formDBSelect ('CarrierId', (int)$Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
    itemSpace () ;
	echo '<tr><td colspan=2>  Alternative delivery address</td></tr>' ;
    itemFieldRaw ('Name', formText ('Address1', $Record['Address1'], 50)) ;
    itemFieldRaw ('Street', formText ('Address2', $Record['Address2'], 50)) ;
    itemFieldRaw ('ZIP', formText ('ZIP', $Record['ZIP'], 20)) ;
    itemFieldRaw ('City', formText ('City', $Record['City'], 50)) ;
    itemFieldRaw ('Country',  formDBSelect ('CountryId', (int)$Record['CountryId'], 'SELECT Id, CONCAT(Name," - ",Description) AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px; ')) ;
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Var/ean/SKU', formCheckbox ('VariantCodes', $Record['VariantCodes'])) ;
    itemSpace () ;
    if (!$User['SalesRef'])  {
		printf ("<input type=hidden name=MAX_FILE_SIZE value=20000000>\n") ;
		printf ("<tr><td><p>Import orderlines</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;  
	}
    if (!$User['SalesRef']) {
		if ((($Record['ToCompanyId'] == 2) or ($Record['ToCompanyId'] == 18)) and ($WholesaleProjectMember==0))
			itemFieldRaw ('Season', formDBSelect ('SeasonId', (int)$Record['SeasonId'], 
							sprintf('SELECT Id, Name AS Value FROM Season WHERE Active=1 and Done=0 and CompanyId=%d and OwnerId=%d ORDER BY Name', 
									$Record['CompanyId'], (int)$Record['ToCompanyId']), 'width:150px;', array (0 => 'none'))) ;  
		else
			itemFieldRaw ('Season', formDBSelect ('SeasonId', (int)$Record['SeasonId'], 
							sprintf('SELECT Id, Name AS Value FROM Season WHERE Active=1 and Done=0 and CompanyId=%d  and OwnerId=%d ORDER BY Name', 
									$Record['ToCompanyId'],$Record['ToCompanyId']), 'width:150px;', array (0 => 'none'))) ;  
    }
    if (!$User['SalesRef']) 
		itemFieldRaw ('Discount', formText ('Discount', 0, 3, 'text-align:right;') . ' %') ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Delivery', formDate ('DeliveryDate','')) ;
    itemFieldRaw ('Pick Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:50px;')) ;
     itemSpace () ;

    itemFieldRaw ('InvoiceHeader', formTextArea ('InvoiceHeader', $Record['InvoiceHeader'], 'width:100%;height:100px;')) ;
    itemFieldRaw ('InvoiceFooter', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
    if (($Navigation['Parameters'] != 'new')  and ($Navigation['Parameters'] != 'new-purch')) {
	itemSpace () ;
	if (!$User['SalesRef']) 
		itemFieldRaw ('Done condition', formDBSelect ('OrderDoneId', (int)$Record['OrderDoneId'], 'SELECT Id, Name AS Value FROM OrderDone WHERE Active=1 ORDER BY Name', 'width:150px;')) ;
	itemSpace () ;
	if (!$User['SalesRef']) 
		flag ($Record, 'Done') ; 
	itemInfo ($Record) ;
    }
    itemEnd () ;
    //formEnd () ;
    printf ("</form>\n") ;

    return 0 ;
?>
