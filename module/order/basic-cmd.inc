<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc';
    

    $fields = array (
  	'CompanyId'		=> array ('type' => 'set'),
//  	'ToCompanyId'		=> array ('type' => 'set'),
//  	'ToCompanyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'Description'		=> array (						'check' => true),
	'Reference'		=> array (						'check' => true),
	'ReceivedDate' 		=> array ('type' => 'date',	'mandatory' => false),
	'DesiredDate' 		=> array ('type' => 'date',	'mandatory' => false),
	'OrderTypeId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'SalesUserId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CurrencyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'PaymentTermId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'Address1'		=> array (						'check' => true),
	'Address2'		=> array (						'check' => true),
	'ZIP'			=> array (						'check' => true),
	'City'			=> array (						'check' => true),
	'CountryId'		=> array ('type' => 'integer',				'check' => true),
	'SeasonId'		=> array ('type' => 'integer',				'check' => true),
	
	'InvoiceHeader'		=> array (						'check' => true),
	'InvoiceFooter'		=> array (						'check' => true),
	'Instructions'		=> array (						'check' => true),

	'OrderDoneId'		=> array ('type' => 'integer'),

	'Ready'			=> array ('type' => 'checkbox',				'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),
	'Done'			=> array ('type' => 'checkbox',				'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set'),

	'VariantCodes'			=> array ('type' => 'checkbox',				'check' => false),

//	'Proposal'			=> array ('type' => 'checkbox',				'check' => false),
	'InvoiceDraft'			=> array ('type' => 'checkbox',				'check' => false),
	
	'PartDelivery'			=> array ('type' => 'checkbox',				'check' => false),
	'PartDeliveryLine'			=> array ('type' => 'checkbox',				'check' => false),

	'ConsolidatedId'			=> array ('type' => 'integer',				'check' => false),

	'ExchangeRate'		=> array ('type' => 'set'),	

	'PurchaseOrderId'		=> array ('type' => 'set')	

    ) ;

    $query = 'SELECT * FROM company WHERE Id='.$Record['CompanyId'];
    $res = dbQuery($query);
    $comp = dbFetch($res);
    //$_POST['ConsolidatedId'] = 0;
	$_co = array('Active' => 1, 
				'ConsolidatedInvoice' 	=> $comp['InvoiceSplit'], 
				'PartDelivery' 			=> ($_POST['PartDelivery']=='on'?1:0), 
				'PartDeliveryLine' 		=> ($_POST['PartDeliveryLine']=='on'?1:0), 
				'InvoiceDraft' 			=> ($_POST['InvoiceDraft']=='on'?1:0)) ;
    if($_POST['ConsolidatedId'] == 0){
    	$ConId = tableWriteNew('ConsolidatedOrder', $_co);
    	if($ConId == 0){
    		return 'Consolidated Order not created';
    	}
    	$_POST['ConsolidatedId'] = $ConId;
    } else {
//		return 'her ' . $_POST['ConsolidatedId'] . ' - ' . $_POST['InvoiceDraft'] . '!';
    	$ConId = tableWriteNew('ConsolidatedOrder', $_co, (int)$_POST['ConsolidatedId']);
	}

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
// return 'Ken demo' ;
	switch ($fieldname) {
	    case 'Ready':
			// Ignore if not setting flag
			if (!$changed or !$value) return false ;
			
			if ($_POST['Proposal']=='on') return 'Setting Ready is not allowed for propoasal' ;
			
			// Any Order Positions
			$query = sprintf ("SELECT OrderLine.*, Article.Number AS ArticleNumber, Article.VariantColor, Article.CustomsPositionId, Article.MaterialCountryId, Article.KnitCountryId, Article.WorkCountryId FROM OrderLine LEFT JOIN Article ON Article.Id=OrderLine.ArticleId WHERE OrderLine.OrderId=%d AND OrderLine.Active=1", $Id) ;
			$result = dbQuery ($query) ;
			$n = 0 ;
			while ($row = dbFetch ($result)) {
				// Validate OrderLine
				if ($row['VariantColor'] and (int)$row['ArticleColorId'] == 0) return sprintf ('line %d, article %s: no color assigned', (int)$row['No'], $row['ArticleNumber']) ;
				if ((int)$row['Quantity'] == 0) return sprintf ('line %d, article %s: zero Quantity', (int)$row['No'], $row['ArticleNumber']) ;
//				return 'Dates ' . $row['DeliveryDate'] . ' ' . date('Y-m-d') ;
//				if ($row['DeliveryDate'] < date('Y-m-d')) return 'Delivery date cant be before today. Please update '. $row['DeliveryDate'] . ' starting by order line ' . (int)$row['No'];
				//		    if ($row['PriceSale'] == 0) return sprintf ('line %d, article %s: zero Price', (int)$row['No'], $row['ArticleNumber']) ;
	
			    
				// Validate Article
	//			if ((int)$row['CustomsPositionId'] <= 0) return sprintf ('line %d, article %s: no Customs Position', (int)$row['No'], $row['ArticleNumber']) ;
	//		    if ((int)$row['MaterialCountryId'] <= 0) return sprintf ('line %d, article %s: no Material Country', (int)$row['No'], $row['ArticleNumber']) ;
	//			if ((int)$row['KnitCountryId'] <= 0) return sprintf ('line %d, article %s: no Knit Country', (int)$row['No'], $row['ArticleNumber']) ;
	//			if ((int)$row['WorkCountryId'] <= 0) return sprintf ('line %d, article %s: no Work Country', (int)$row['No'], $row['ArticleNumber']) ;
				$n++ ;    
			}
			dbQueryFree ($result) ;

			if ($n == 0) return 'the Order can not be set Ready when it has no OrderLines' ;

			// Get and save exchangerate
			$fields['ExchangeRate']['value'] = tableGetField ('Currency', 'Rate', (int)$Record['CurrencyId']) ;
			
			// Set tracking information
			$fields['ReadyUserId']['value'] = $User['Id'] ;
			$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
			
			return true ;

	    case 'Done':
			// Ignore if not setting flag
			if (!$changed or !$value) return false ;

			// Check for Allocation
			if (!$Record['Ready']) return 'the Order can not be Done before it has been set Ready' ;	

			// Check that OrderDone condition exists
			if ((int)$Record['OrderDoneId'] <= 0) return 'Done condition has not been set' ;

			// Set tracking information
			$fields['DoneUserId']['value'] = $User['Id'] ;
			$fields['DoneDate']['value'] = dbDateEncode(time()) ;
			
			// Get orderlines
			$query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0', $Record['Id']) ;
			$res = dbQuery ($query) ;

			// Set Orderlines done
			$OrderLine = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time())
			) ;
			while ($row = dbFetch ($res)) {
				tableWrite ('OrderLine', $OrderLine, $row['id']) ;
			}		

			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $Record['Id'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

			// Set state on WEbshop order if there is one.
			$OrderTypeId=tableGetField('`Order`', 'OrderTypeId', $Record['Id']) ;
			if ($OrderTypeId == 10) {
				// Set state in weborder.
				$DoneState = $Record['OrderDoneId'] == 5 ? 5 : 7 ;
				// Transfer to Webshop.
			}		

			return true ;
	}

	if (!$changed) return false ;


// Ken 2006.04.27	if ($Record['Ready']) return sprintf ('the Order can not be modified when Ready, field %s', $fieldname) ;
	return true ;	
    }


	// Newest way of importing order sheet.
    function makeorderlines ($data, $SheetConfig) {
	global $User, $Navigation, $Record, $Id, $fields ;
/*		
	for ($k = 0; $k <= 5; $k++) {
	for ($i = 0; $i <= $data->sheets[$k]['numRows']; $i++) {
		$errormsg .= ' k' . $k . ': ' . $data->sheets[$k]['cells'][1][1] . ','  . $data->sheets[$k]['cells'][2][1] . ',' . $data->sheets[$k]['cells'][4][1] . ',' ;
      }}
	return $errormsg ;
*/
	//	Excel read help!
	//	 $data->sheets[0]['numRows'] - count rows
	//	 $data->sheets[0]['numCols'] - count columns
	//	 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column
	//	 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
	//	    
	//	$data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
	//	if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
	//	$data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
	//	$data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
	//	$data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 

	if ($SheetConfig['DeliveryDateColomn'] == 0 and $_POST['DeliveryDate'] <= '2001-01-01' ) {
		return 'No delivery Date specified (Currency: ' . $data->sheets[$DefSheet]['cells'][4][1] . ' Project: ' . $data->sheets[$DefSheet]['cells'][1][1] .  ' Collection: ' . $data->sheets[$DefSheet]['cells'][2][1] . ')' ;
	}

	$OrderLineNo=0;		
	if ($SheetConfig['DeliveryDateColomn'] != 0) {
	for ($k = 0; $k <= 15; $k++) {
		//Sorting and setting data.
		foreach ($data->sheets[$k]['cells'] as $key => $row) {
			$DeliveryDate[$key]  = $row[$SheetConfig['DeliveryDateColomn']] ;
			$data->sheets[$k]['cells'][$key][$SheetConfig['DeliveryDateColomn']] = $data->sheets[$k]['cellsInfo'][$key][$SheetConfig['DeliveryDateColomn']]['raw'] ;
		}
		array_multisort ($DeliveryDate, SORT_ASC, $data->sheets[$k]['cells']);
	}	
	}	
	for ($k = 0; $k <= 15; $k++) {
	for ($i = 0; $i <= $data->sheets[$k]['numRows']; $i++) {
		// Only proces lines with "active" in colomn A
		if ($data->sheets[$k]['cells'][$i][1] != 'active') continue ;

		unset ($SizeQty) ;
		$OrderQty=0 ;
		$sizeoffset=1 ;
		$NoSizeOffset=0 ;

		// read ordered qty's
		for ($j = $SheetConfig['SizeStart']; $j <= $SheetConfig['SizeEnd']; $j++) {
			if (($j==$SheetConfig['DirtyFlag']) and $i<205) {
				$sizeoffset=0 ;
				continue ; 
			}

			if ($data->sheets[$k]['cells'][$i][$j] == "-") {
				$NoSizeOffset++ ;
				continue ;
			}

			if ((int)$data->sheets[$k]['cells'][$i][$j] > 0) {
//				$SizeQty[($j-($SheetConfig['SizeStart']-$sizeoffset))] = (int)$data->sheets[$k]['cells'][$i][$j];
				$SizeQty[($j-($SheetConfig['SizeStart']-$sizeoffset))-$NoSizeOffset] = (int)$data->sheets[$k]['cells'][$i][$j];
				$OrderQty += (int)$data->sheets[$k]['cells'][$i][$j] ;
			}
		}
		if ($OrderQty==0) continue ; // Only create orderline if something ordered!
		
		$OrderLineNo++;
		// Find article, color and price info
		unset($OrderLineNew) ;
		$OrderLineNew = array (
			'No' => $OrderLineNo,
			'Description' => '',
			'ArticleId' => 0,
			'ArticleColorId' => 0,
			'OrderId' => (int)$Record['Id'],
			'PriceCost' => 0,
			'PriceSale' => (float)str_replace (',', '.', $data->sheets[$k]['cells'][$i][$SheetConfig['PriceColomn']]) ,
			'Discount' => (int)$_POST['Discount'],
			'DeliveryDate' => $_POST['DeliveryDate'] > '2001-01-01' ? $_POST['DeliveryDate'] : date("Y-m-d", $data->sheets[$k]['cells'][$i][$SheetConfig['DeliveryDateColomn']]) ,
			'Quantity' => $OrderQty,
			'Surplus'	=> 0,
			'CustArtRef' => '',
			'InvoiceFooter' => ''
		) ;

		$OrderLineNew['PriceSale'] = (float)str_replace (',', '.', $data->sheets[$k]['cells'][$i][$SheetConfig['PriceColomn']]) ;
		if ($SheetConfig['ArticleInfoFromVariant']) {
			$whereclausemodelref = "VariantModelRef like '%" . $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']] . "%' and Active=1" ;
			$OrderLineNew['ArticleId']=(int)tableGetFieldWhere ('VariantCode', 'ArticleId', $whereclausemodelref ) ;
			if ($OrderLineNew['ArticleId']==0) {
				$OrderLineNew['Description']=sprintf('Article not found in Excel sheet %d line %d orderline %d', ($k+1), $i, $OrderLineNo ) ;
				$OrderLineNew['No']=0 ;
			}  else {
				$whereclause = sprintf ("Id=%d and Active=1", $OrderLineNew['ArticleId']) ;
				$OrderLineNew['Description']=tableGetFieldWhere ('Article', 'Description', $whereclause ) ;
				$OrderLineNew['ArticleColorId']=(int)tableGetFieldWhere ('VariantCode', 'ArticleColorId', $whereclausemodelref ) ;
				
				if (((int)tableGetFieldWhere ('Article', 'VariantColor', $whereclause )==1) and $OrderLineNew['ArticleColorId']==0) {
					$OrderLineNew['Description']=$OrderLineNew['Description'] . sprintf('ArticleColor1 not found in Excel sheet %d line %d, orderline %d', ($k+1), $i, $OrderLineNo) ;
					$OrderLineNew['No']=0 ;
				}
			}
		} else {
			$query = sprintf ("SELECT * from Article Where Number='%s' and Active=1", $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']]) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
		    	dbQueryFree ($result) ;
			$OrderLineNew['ArticleId']=(int)$row['Id'] ;
			if ($OrderLineNew['ArticleId']==0) {
				$OrderLineNew['Description']=sprintf('Article not found in Excel sheet %d line %d orderline %d', ($k+1), $i, $OrderLineNo ) ;
				$OrderLineNew['No']=0 ;
			}  else {
				$OrderLineNew['Description']=$row['Description'] ;
				
				$VariantColor = $row['VariantColor'] ;
				$query = sprintf ("SELECT ac.id as Id from ArticleColor ac, Color c 
							Where ac.colorid=c.id and ac.articleid=%d and c.number='%s' and ac.Active=1 and c.Active=1", 
							$OrderLineNew['ArticleId'], $data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']]) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
			    	dbQueryFree ($result) ;
				$OrderLineNew['ArticleColorId'] = $row['Id'] ;

				if ($VariantColor and $OrderLineNew['ArticleColorId']==0) {
					$OrderLineNew['Description']=sprintf('ArticleColor2 not found in Excel sheet %d line %d, orderline %d: ', ($k+1), $i, $OrderLineNo) . $OrderLineNew['Description'] ;
					$OrderLineNew['No']=0 ;
					$OrderLineNew['ArticleColorId']=0 ;
				}
			}
		}
		$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;

		// Get and save size quantities
		$query = sprintf ('SELECT * FROM articlesize WHERE ArticleId=%d AND Active=1 ORDER BY DisplayOrder', (int)$OrderLineNew['ArticleId']) ;
		$result = dbQuery ($query) ;
		$displayno = 0 ;
		while ($row = dbFetch ($result)) {
		    if ((int)$SizeQty[(int)$row['DisplayOrder']] > 0) {
			    // Create new record
				$OrderQuantity = array (
					'OrderLineId'		=> (int)$OrderLineId,
					'ArticleSizeId'		=> (int)$row['Id'],
					'Quantity'		=> (int)$SizeQty[(int)$row['DisplayOrder']]
				) ;
				$OrderQuantityId = tableWrite ('OrderQuantity', $OrderQuantity) ;
			}
		}
	    	dbQueryFree ($result) ;
	} // end for rows
	} // end for sheets
	return 0 ;
    } // end function


    // Main start
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Done']) ;
	    $fields['CompanyId']['value'] = (int)$Record['Id'] ;
	    $Id = -1 ;
	    break ;

	case 'new-purch' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Done']) ;
	    $fields['CompanyId']['value'] = (int)$_POST['CompanyId'] ;
	    $fields['PurchaseOrderId']['value'] = (int)$_POST['PurchaseOrderId'] ;
//		tableGetFieldWhere ('`Order`', 'Id', sprintf('PurchaseOrderId=%d And Active=1', (int)$_POST['PurchaseOrderId']))
	    $Id = -1 ;
	    break ;

	default :
	    if ($Record['Done']) return 'the Order can not be modified when Done' ;
	    break ;
    }
    
	$FileUpload = false ;
	if (!$User['SalesRef'])  {
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	} 
	
     
    $res = saveFields ('Order', $Id, $fields, true) ;
    if ($res) return $res ;

	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);

		if ((int)$Record['ToCompanyId'] == 1430 and strlen($data->sheets[0]['cells'][1][1]) == 0) {
		return 'test';
				// Legacy Drappadot
				$SheetConfig = array (
					'ArticleInfoFromVariant' => 1,
					'ArticleColomn' => 5, 	// VariantModelRef 5
					'SizeStart' => 12,	// 12
					'SizeEnd' => 19,		// 18
					'DirtyFlag' => 0,		// Loose size 5 years colomn 15
					'PriceColomn' => 0
				) ;
				switch ((int)$Record['CurrencyId']) {
					case 1: // DKK
						$SheetConfig['PriceColomn']=6 ; //7
						break ;
					case 2: // EUR
						$SheetConfig['PriceColomn']=8 ; 
						break ;
					case 10: // USD
						$SheetConfig['PriceColomn']=10 ; 
						break ;
					default:
						return sprintf ("No support for CurrencyId %d",(int)$Record['CurrencyId']) ; 
						break ;
				}
		} else {
				$DefSheet = 0 ;
/*				
				$err_msg= 'test ' . $data->sheets[$DefSheet]['cells'][1][1] . ' sheets: ';
				for ($i = 0; $i <= 50; $i++) {
					$err_msg = $err_msg . $i . ':' . $data->sheets[$i]['cells'][1][1] . ' raw: ' . $data->sheets[$i]['cellsInfo'][1][1]['raw'] . ' ';
				}
				return $err_msg ;
*/				
/*				for ($i = 0; $i <= 15; $i++) {
					if ($data->sheets[$i]['cells'][1][1] == '1' or $data->sheets[$i]['cells'][1][1] == '2') {
						$DefSheet = $i ;
					    break ;
					}
				}
				return 'test ' . $DefSheet ;
*/
				if ($data->sheets[$DefSheet]['cells'][1][1] == '') { // Format until 2010.06.09
					$SheetConfig = array (
						'ArticleInfoFromVariant' => 0,
						'ArticleColomn' => 3,
						'ColorColomn' => 4,
						'DeliveryDateColomn' => 0,
						'SizeStart' => 6,
						'SizeEnd' => 20,
//						'PriceStart' => 24,
//						'PriceEnd' => 24,
						'DirtyFlag' => 0,		
						'PriceColomn' => 22
					) ;
				} else {
					$SheetConfig = array (					// Current format 1/2
						'ArticleInfoFromVariant' => 0,
						'ArticleColomn' => 3,
						'ColorColomn' => 4,
						'DeliveryDateColomn' => 7,
						'SizeStart' => 8,
						'SizeEnd' => 22,
						'PriceStart' => 24,
						'PriceEnd' => 32,
						'DirtyFlag' => 0,		
						'PriceColomn' => 24
					) ;

					// Get price groups etc.
					if ($data->sheets[$DefSheet]['cells'][1][1] == '2') {
						$SheetConfig['PriceEnd'] = 35 ;
						
						// Get Group name
						$whereclause = sprintf ("name='%s'", $data->sheets[$DefSheet]['cells'][3][1]) ;
						$SheetCollectionPriceGroupId = tableGetFieldWhere ('CollectionPriceGroup', 'Id', $whereclause );

						if ($SheetCollectionPriceGroupId > 0) {
		 					// Get price colomns
							for ($x = $SheetConfig['PriceStart']; $x <= $SheetConfig['PriceEnd']; $x++) {
								if ($data->sheets[$DefSheet]['cells'][3][1] == $data->sheets[$DefSheet]['cells'][9][$x]) {
									if ($data->sheets[$DefSheet]['cells'][5][1] == 'Retail') 
										$SheetConfig['PriceColomn'] = $x + 1 ;
									else
										$SheetConfig['PriceColomn'] = $x ;
								}
							}
						} else
								return 'Price group not found: ' . $data->sheets[$DefSheet]['cells'][3][1] ;
					} else {
						// Check record currency against sheet currency.
						$whereclause = sprintf ("name='%s' and active=1", $data->sheets[$DefSheet]['cells'][4][1]) ;
						$SheetCurrencyId = tableGetFieldWhere ('Currency', 'Id', $whereclause );

						if ($SheetCurrencyId != (int)$Record['CurrencyId']) {
							return sprintf ("Currency (%s) for order different from Currency in sheet (%s)", tableGetField ('Currency', 'Name', (int)$Record['CurrencyId']), $data->sheets[$DefSheet]['cells'][4][1]) ; 
						}
						
		 				// Get price colomns
						for ($x = $SheetConfig['PriceStart']; $x <= $SheetConfig['PriceEnd']; $x++) {
							if ($data->sheets[$DefSheet]['cells'][4][1] == $data->sheets[$DefSheet]['cells'][10][$x]) {
								$SheetConfig['PriceColomn'] = $x;
								//$SheetConfig['RetailColomn'] = $x + 1 ;
							}
						}
					}
				}
		}		

		$ret = makeorderlines ($data, $SheetConfig) ;
		if ($ret) return $ret ;
	 } 

    switch ($Navigation['Parameters']) {
	case 'new-purch' :
		$OrderLineNo = 0 ;
		$PurchaseOrderLineId = 0 ;
		$OrderQty = 0 ;
		if ($_POST['GroupLines'] == 'on')
			$query = sprintf ('SELECT min(rl.Id) as PurchaseOrderLineId, rl.Description as Description, rl.articleid,
									rl.articlecolorid as articlecolorid, rl.PurchasePriceConsumption as Price, 
									rl.RequestedDate, rl.ConfirmedDate,rl.Surplus,rl.LineFooter,
									a.VariantSize, a.VariantDimension, rq.articlesizeid, rq.dimension, sum(rq.quantity) as quantity
							FROM requisitionline rl, requisitionlinequantity rq, article a 
							WHERE rq.requesitionlineid=rl.id and rl.articleid=a.id and rl.requisitionid=%d
									and rl.active=1 and rq.active=1
							GROUP BY rl.articleid, rl.articlecolorid, rq.articlesizeid,rl.LineFooter, rl.RequestedDate, Price',
						   $Record['PurchaseOrderId']);
		else
			$query = sprintf ('SELECT rl.Id as PurchaseOrderLineId, rl.Description as Description, rl.articleid,
									rl.articlecolorid as articlecolorid, rl.PurchasePriceConsumption as Price, 
									rl.RequestedDate, rl.ConfirmedDate,rl.Surplus,rl.LineFooter,
									a.VariantSize, a.VariantDimension, rq.articlesizeid, rq.dimension, rq.quantity
							FROM requisitionline rl, requisitionlinequantity rq, article a 
							WHERE rq.requesitionlineid=rl.id and rl.articleid=a.id and rl.requisitionid=%d
									and rl.active=1 and rq.active=1', 
						   $Record['PurchaseOrderId']);

		$res = dbQuery($query) ;

		// Generate lines
		while ($row = dbFetch ($res)) {
			if ($PurchaseOrderLineId != $row['PurchaseOrderLineId'])  {
				if ($PurchaseOrderLineId != 0) {
					$OrderLineUpd['Quantity'] = $OrderQty ;
				    tableWrite ('orderline', $OrderLineUpd, $OrderLineId) ;
					$OrderQty = 0 ;
				}
				$OrderLineNo++;
				$PurchaseOrderLineId = $row['PurchaseOrderLineId'] ;

				// Find article, color and price info
				unset($OrderLineNew) ;
				$OrderLineNew = array (
					'No' => $OrderLineNo,
					'Description' => $row['Description'],
					'ArticleId' => $row['articleid'],
					'ArticleColorId' => (int)$row['articlecolorid'],
					'OrderId' => (int)$Record['Id'],
					'PriceCost' => 0,
					'PriceSale' => $row['Price'] ,
					'Discount' => 0,
					'DeliveryDate' => $row['ConfirmedDate'] > '2001-01-01' ? $row['ConfirmedDate'] : $row['RequestedDate'] ,
					'Quantity' => $OrderQty,
					'Surplus'	=> $row['Surplus'],
					'CustArtRef' => '',
					'InvoiceFooter' => $row['LineFooter'],
					'LineFooter' => $row['LineFooter']
				) ;
				$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;
			}
			// Create new qty record
			unset($OrderQuantity) ;
			if ($row['VariantSize']==1) {
				$OrderQuantity = array (
					'OrderLineId'	=> (int)$OrderLineId,
					'ArticleSizeId'	=> (int)$row['articlesizeid'],
					'Quantity'		=> (int)$row['quantity']
				) ;
				$OrderQuantityId = tableWrite ('OrderQuantity', $OrderQuantity) ;
			}
			if ($row['VariantDimension']==1) {
				$OrderQuantity = array (
					'OrderLineId'	=> (int)$OrderLineId,
					'Dimension'	=> (int)$row['dimension'],
					'Quantity'		=> (int)$row['quantity']
				) ;
				$OrderQuantityId = tableWrite ('OrderQuantity', $OrderQuantity) ;
			}
			$OrderQty += (int)$row['quantity'] ;
		}
		$OrderLineUpd['Quantity'] = $OrderQty ;
	    tableWrite ('orderline', $OrderLineUpd, $OrderLineId) ;
	    if (tableGetFieldWhere ('UserCompanies', 'Id', sprintf('`UserId`=%d and CompanyId=%d', $User['Id'], $Record['ToCompanyId']))>0)
		    return navigationCommandMark ('orderview', (int)$Record['Id']) ;
		break ;
	case 'new' :
	    // View
	    return navigationCommandMark ('orderview', (int)$Record['Id']) ;
	case 'consorder'  :
	    return navigationCommandMark ('conorderview', (int)$_POST['ConsolidatedId']) ;
    }

    return 0 ;    
?>
