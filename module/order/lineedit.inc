<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }
    function flagRevert (&$Record, $field) {
	if ($Record[$field]) {
	    itemFieldRaw ($field, 
					   formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . 
					                       tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', 
					                       $Record[$field.'UserId'])) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }
   
    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['OrderId']) ;     
    itemField ('Customer', sprintf ('%s (%s)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;     
	if ((int)$Record['CaseId'] > 0) itemField ('Case', (int)$Record['CaseId']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Pos', formText ('No', (int)$Record['No'], 3)) ;
    itemSpace () ;

  
  itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
/*    
    $tp = dbDateDecode($Record['PrefDeliveryDate']) ;
    $tc = dbDateDecode($Record['ConfDeliveryDate']) ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Requested', formDate ('PrefDeliveryDate', ($Record['PrefDeliveryDate'] > '2000-01-01') ? $tp : '')) ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Confirmed', formDate ('ConfDeliveryDate', ($Record['ConfDeliveryDate'] > '2000-01-01') ? $tc : '')) ;
*/
		itemFieldRaw ('Delivery', formDate ('DeliveryDate', dbDateDecode($Record['DeliveryDate']))) ;
    if ($Record['VariantColor']) {
		itemSpace () ;
/*		
		itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], 
									sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value 
											FROM (ArticleColor, Color)
											LEFT JOIN orderline ol ON ol.orderid=%d and ol.articlecolorid=ArticleColor.Id and ol.active=1
											WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId AND (isnull(ol.id) or ol.id=0 or ArticleColor.Id=%d)
											GROUP BY ArticleColor.Id
											ORDER BY Value', $Record['OrderId'], $Record['ArticleId'],(int)$Record['ArticleColorId'] ), 'width:200px')) ; 
*/
		itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], 
									sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value 
											FROM (ArticleColor, Color)
											LEFT JOIN orderline ol ON ol.orderid=%d and ol.articlecolorid=ArticleColor.Id and ol.active=1
											WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId 
											GROUP BY ArticleColor.Id
											ORDER BY Value', $Record['OrderId'], $Record['ArticleId']), 'width:200px')) ; 
    }
    itemSpace () ;
    if ($Record['VariantSize']) {
		$field = '<table><tr>' ;
		foreach ($Size as $s) $field .= '<td width=60 align=right><p style="margin-right=6px;">' . htmlentities($s['Name']) . '</p></td>' ;
		$field .= '<td width=60></td>' ;
		$field .= '</tr>' ;
		$field .= '<tr>' ;
		foreach ($Size as $s) $field .= '<td>' . formText (sprintf('Quantity[%d]', (int)$s['Id']), number_format((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 6, 'width:50px;text-align:right;margin-right:0;') . '</td>' ;
		$field .= '<td style="padding-top:6px;padding-left:4px;">' . htmlentities ($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Quantity', $field) ; 
    } else {
		itemFieldRaw ('Quantity', formText ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 6, 'text-align:right;') . ' ' . htmlentities ($Record['UnitName'])) ;
    }
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Surplus', formText ('Surplus', (int)$Record['Surplus'], 2, 'text-align:right;') . ' %') ;
    if (!$User['SalesRef']) 
		itemSpace () ;
	if ($Record['ToCompanyId']==5060) {
		itemFieldRaw ('Cost Price', formText ('PriceCost', $Record['PriceCost'], 12, 'text-align:right;') . ' DKK') ;
		itemSpace () ;
	}
    itemFieldRaw ('Sales Price', formText ('PriceSale', number_format((float)$Record['PriceSale'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
    itemFieldRaw ('Discount', formText ('Discount', (int)$Record['Discount'], 3, 'text-align:right;') . ' %') ;
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Customer ref.', formtext ('CustArtRef', $Record['CustArtRef'], 12, 'text-align:right;')) ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Line Footer', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
    if (!$User['SalesRef']) 
		flagRevert ($Record, 'Done') ; 
    itemInfo ($Record) ;
/*
	if (!$User['SalesRef']) 
		itemFieldRaw('Case- not active', formDBSelect('CaseId', (int)$Record['CaseId'], sprintf("select Id, Id AS Value from `Case` where ArticleId=%d and CompanyId=%d and Active=1",$Record['ArticleId'],$Record['CompanyId']), '', array(0 => '--none--'),  $disable));
    itemSpace () ;
*/
    if (!$User['SalesRef']) 
		itemSpace () ;
 
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
