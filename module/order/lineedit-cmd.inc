<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

    $fields = array (
		'No'			=> array ('type' => 'integer',				'check' => true),

		'CaseId'	=> array ('type' => 'integer'),

		'Description'		=> array (),
	 
		'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

		'PriceCost'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'PriceSale'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'Discount'		=> array ('type' => 'integer',	'mandatory' => true),
		
		'PrefDeliveryDate'		=> array ('type' => 'date',				'check' => true),
		'ConfDeliveryDate'		=> array ('type' => 'date',				'check' => true),
		'Quantity'		=> array ('type' => 'decimal',				'check' => true),
		'Surplus'		=> array ('type' => 'integer'),

		'CustArtRef'		=> array (),
		'InvoiceFooter'		=> array ()
		
	) ;
	$specialfields = array (
		'ProductionId'         => array( 'type' => 'integer'),

		'DeliveryDate'		=> array ('type' => 'date',	'check' => true),

		'Done'			=> array ('type' => 'checkbox'),
		'DoneUserId'	=> array ('type' => 'set'),
		'DoneDate' 		=> array ('type' => 'set'),
	
   );

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Size, $Id, $fields ;
	switch ($fieldname) {
	    case 'Done':
		// Ignore if not setting flag
		if ((!$changed)) return false ;

		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;

		return true ;
		
	    case 'No' :
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT Id FROM OrderLine WHERE OrderId=%d AND Active=1 AND Id<>%d', (int)$Record['OrderId'], (int)$Record['Id']) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($value <= 0 or $value > ($count+1)) return sprintf ('invalid OrderLine Position number, max %d', $count+1) ;

		return true ;

	    case 'ArticleColorId' :
		if ($value <= 0) return 'Not allowed without Color: Please select a color before saving' ;
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT * FROM ArticleColor WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] <= 0) return sprintf ('%s(%d) not found, id %d', __FILE__, __LINE__, $value) ;
		if ((int)$row['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) ArticleColor mismatch, id %d', __FILE__, __LINE__) ;

		return true ;
		
	    case 'DeliveryDate' :
			if (!$changed) return false ;

			// Validate
			$t = dbDateDecode ($value) ;
//			if ($t < utsDateOnly(time())) return 'Delivery Date can not be in the past' ;
			return true ;
		
	    case 'PrefDeliveryDate' :
			if ((!$changed) or ($value=='')) return false ;
			// Validate
			$t = dbDateDecode ($value) ;
			if ($t <= 0) return 'Requested Date cant be deleted' ;
			return true ;
			
	    case 'ConfDeliveryDate' :
			if ((!$changed) or ($value=='')) return false ;
			// Validate
			$t = dbDateDecode ($value) ;
			if ($t <= 0) return 'Confirmed Date cant be deleted' ;
			return true ;

	    case 'Quantity' :
		if (!$changed) return false ;
		
		// Validate
		if ($value <= 0) return 'invalid Quantity' ;

		return true ;
	}
	
	return false ;	
    }
    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
	if(!$Record['OrderReady']) {
		if ($_POST['ConfDeliveryDate']=='') {
			$_POST['ConfDeliveryDate'] = $_POST['DeliveryDate'] ;
		}
	}

//return 'hallo: ' . $Record['ConfDeliveryDate'] . ' ' .  $_POST['ConfDeliveryDate'];
//	if ($Record['ConfDeliveryDate'] != $_POST['ConfDeliveryDate']) return 'hallo' ;

	$OrderLineDone=false;
	// Update production even when order is ready or done
	$Post_Done = ($_POST['Done'] == 'on') ? 1 : 0 ;
	if (	((int)$Record['ProductionId'] != (int)$_POST['ProductionId']) 
	     or ($Record['Done'] != $Post_Done)
	     or ($Record['DeliveryDate'] != $_POST['DeliveryDate'])
	    ) {
		// Update orderlinedone regardless of orderstate, 
		if ($Record['Done'] != $Post_Done) {		
			if($Record['OrderReady']) {
				$specialfields['Done']['value'] = ($_POST['Done'] == 'on') ? 1 : 0  ;
				$specialfields['DoneUserId']['value'] = $User['Id'] ;
				$specialfields['DoneDate']['value'] = dbDateEncode(time()) ;	
			} else return 'Order not ready' ;
			$OrderLineDone=true;
		}
		$res = saveFields ('OrderLine', $Id, $specialfields, true) ;
		if ($res) return $res ;

		if ($OrderLineDone) {
			// last line? set order done
			$query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0', $Record['OrderId']) ;
			$res = dbQuery ($query) ;

			if (!($row = dbFetch ($res))) {
				$Order = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
					'OrderDoneID'	=>  5
				) ;
				tableWrite ('Order', $Order, $Record['OrderId']) ;
			}
			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $Record['OrderId'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

		}    
    
		if ($Record['OrderDone'] || $Record['OrderReady']) {
		  return 0;
		}
	}

    // Validate state of Order
    if ($Record['OrderDone']) return 'the OrderLine can not be modified when Order is Done' ;
    if ($Record['OrderReady']) return 'the OrderLine can not be modified when Order is Ready' ;

    if (!$Record['VariantColor']) {
		unset ($fields['ArticleColorId']) ;
	}	
    
    if ($Record['VariantSize']) {
	// Generat verify format
	$format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", 9-(int)$Record['UnitDecimals'], 9-(int)$Record['UnitDecimals'], (int)$Record['UnitDecimals']) ;

	$Quantity = 0 ;
	foreach ($Size as $i => $s) {
	    $value_string = trim($_POST['Quantity'][(int)$s['Id']]) ;
	    if ($value_string == '') $value_string = '0' ;

	    // verify format
	    if (!ereg($format,$value_string)) return sprintf ("invalid quantity for %s", $s['Name']) ;
	    
	    // Substitute , -> .
	    $value_string = str_replace (',', '.', $value_string) ;
	    
	    $Size[$i]['Value'] = $value_string ;
	    $Quantity += (float)$value_string ;
	}
	if ($Quantity == 0) return 'Quantity must be bigger than zero - otherwise delete line' ; 
	// Set total quantity for orderline
	$fields['Quantity'] = array ('type' => 'set', 'value' => number_format ($Quantity, (int)$Record['UnitDecimals'], '.', '')) ;
    }
        
    // Process and save OrderLine
    $res = saveFields ('OrderLine', $Id, $fields, true) ;
    if ($res) return $res ;

    if ($Record['VariantSize']) {
	// Update Quantities by Sizes
	foreach ($Size as $i => $s) {
	    if ((float)$s['Quantity'] != (float)$s['Value']) {
		if ((int)$s['OrderQuantityId'] > 0) {
		    if ((float)$s['Value'] > 0) {
			// Update existing record
			$fields = array (
			    'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
			) ;
			$res = saveFields ('OrderQuantity', (int)$s['OrderQuantityId'], $fields) ;
		    } else {
			// Delete record
			tableDelete ('OrderQuantity', (int)$s['OrderQuantityId']) ;
		    }
		} else {
		    // Create new record
		    $fields = array (
			'OrderLineId'		=> array ('type' => 'set',	'value' => (int)$Record['Id']),
			'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$s['Id']),
			'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
		    ) ;
		    $res = saveFields ('OrderQuantity', -1, $fields) ;
		}
		if ($res) return $res ;
	    }			    
	}
    }
    
    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM OrderLine WHERE OrderId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['OrderId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == (int)$Record['No']) $i += 1 ;       
	if ($i != (int)$row['No']) {
	    $query = sprintf ("UPDATE OrderLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;
    
    return 0 ;    
?>
