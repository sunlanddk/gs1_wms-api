<?php

    require_once 'lib/table.inc' ;

    // Ready ?
    if ($Record['Ready']) return 'An Order that is Ready cant be deleted' ;

    // Verify references
//    $query = sprintf ("SELECT Id FROM OrderLine WHERE OrderId=%d AND Active=1", $Id) ;
//    $result = dbQuery ($query) ;
//    $count = dbNumRows ($result) ;
//    while ($row = dbFetch ($result)) {
//    	tableDelete ('OrderLine', $row['Id']) ;
//    }		
//    dbQueryFree ($result) ;
//    if ($count > 0) return ("the Order has associated OrderLine(s)") ;

    if ((int)tableGetFieldWhere('PickOrder', 'Id', sprintf('ReferenceId=%d AND Active=1', $Id))>0) 
		return 'Cant delete Order with Active PickOrder' ;

    $query = sprintf ("SELECT Id FROM OrderLine WHERE OrderId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    while ($row = dbFetch ($result)) {
//    	tableDelete ('OrderLine', $row['Id']) ;
		$query = sprintf ("UPDATE OrderQuantity SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d where OrderLineId=%d", dbDateEncode(time()), $User["Id"], $row['Id']) ;
		dbQuery ($query) ;
    }		
    dbQueryFree ($result) ;

    $query = sprintf ("UPDATE OrderLine SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE OrderId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
    dbQuery ($query) ;

    // Do delete  
    tableDelete ('Order', $Id) ;

    return navigationCommandMark ('sales.order', (int)$Id) ;
?>
