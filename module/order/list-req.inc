<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHeadIcon () ;
	listHead ('From Company', 200) ;
	listHead ('From PO Ref', 120) ;
    listHead ('Created', 120) ;
    listHead ('Description') ;
	listHead ('To PO Ref', 120) ;

    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldIcon ('move.gif', 'gen-purch', (int)$row['Id']) ;
	    listField (sprintf ('%s (%s)', $row['CompanyName'], $row['CompanyNumber'])) ;
		listField ((int)$row['Id']) ;
		listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
		listField ($row['Description']) ;
		listField ((int)$row['ToId']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Requested orders', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
