<?php
function getProductOrderQuantity($articleId, $articleColorId, $articleSizeId) {
	$query = sprintf ("select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 left join collection c on c.seasonid=o.seasonid
                 left join collectionmember cm on cm.collectionid=c.id and cm.articleid=ol.articleid and cm.articlecolorid=ol.articlecolorid
                 left join collectionlot cl ON cl.Active=1 and cl.id=cm.CollectionLOTId
                 where o.active=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid=%d and ol.articlecolorid=%d
                       and oq.articlesizeid=%d
					   and cm.notonwebshop = 0
                       and (isnull(cl.id) or cl.id=0 or (cl.id>0 and cl.presale=0 and (select count(id) from requisitionline rl where rl.Active=1 and rl.Done=0 and rl.LotId=cl.id)=0) )
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid	
					 ", $articleId,  $articleColorId, $articleSizeId);
	$res=dbQuery($query) ;
	$row=dbFetch($res) ;
	return $row['quantity'];
}

function getProductInventoryQuantity($articleId, $articleColorId, $articleSizeId) {
	$query = sprintf ("SELECT  cm.articleid, cm.articlecolorid, i.articlesizeid, sum(quantity) as quantity
					FROM (Season se, Collection co, collectionmember cm)
					INNER JOIN item i ON i.articleid=cm.articleid and  i.articlecolorid=cm.articlecolorid and i.active=1
					INNER JOIN container c ON i.containerid=c.id and (c.stockid=se.PickStockId or c.stockid=se.FromStockId) and c.active=1
					INNER JOIN articlesize az ON az.id=i.articlesizeid
					WHERE se.WebshopId=3 and se.done=0 and se.active=1
						and cm.articleid=%d and cm.articlecolorid=%d
						and i.articlesizeid=%d
						and co.seasonid=se.id and co.active=1
						and cm.collectionid=co.id and cm.notonwebshop=0 and cm.active=1
						GROUP BY cm.articleid, cm.articlecolorid, i.articlesizeid
					 ", $articleId,  $articleColorId, $articleSizeId);
//print_r($query) ; die() ;
	$res=dbQuery($query) ;
	$row=dbFetch($res) ;
	return $row['quantity'];
}

function sendStockDataToWebshop($_orderId, $_processTimeStart) {
	
	if ($Config['Instance'] == 'Test') return ; // Dont update if test site.

	$_startown = time() ;
	$_processTimeStart = $_startown - $_processTimeStart ;
	// Find order data
	$query = sprintf ('SELECT  
							Article.Number AS ArticleNumber, 
							variantcode.VariantCode AS sku,
							orderquantity.Quantity as quantity,
							Article.Id AS ArticleId, 
							ArticleColor.Id AS ArticleColorId, 
							orderquantity.articlesizeId AS ArticleSizeId 
						FROM `Order`
						LEFT JOIN OrderLine ON OrderLine.OrderId=`Order`.Id and orderline.active=1
						LEFT JOIN orderquantity ON orderquantity.OrderLineId=OrderLine.Id and orderquantity.active=1
						LEFT JOIN Country ON Country.Id=Order.CountryId
						LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
						LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId
						LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
						LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
						LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
						LEFT JOIN variantcode ON variantcode.ArticleId = OrderLine.ArticleId AND (Article.variantcolor=0 or (Article.variantcolor=1 and variantcode.ArticleColorId=OrderLine.ArticleColorId)) AND (Article.variantsize=0 or (Article.variantsize=1 and variantcode.ArticleSizeId = orderquantity.ArticleSizeId))
						WHERE `Order`.Id=%d and orderquantity.Quantity>0 and not article.articletypeid=6
                        ORDER BY OrderLine.No', $_orderId) ;

	$productres = dbQuery ($query);
	 
	// process order data on build data
	while ($product = dbFetch($productres)){
		$availableQuantity = (int)getProductInventoryQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);
		if ($availableQuantity>0) {
			$orderQuantity = (int)getProductOrderQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);
			if ($availableQuantity>$orderQuantity) {
				$availableQuantity -= $orderQuantity ;
			} else {
				$availableQuantity = 0 ;
			}
		}
		$articles[] = array("productNumber" => $product['ArticleNumber'], "sku" => $product['sku'], "quantity" => $availableQuantity);
	}

	// post json data
	$potdata = json_encode(array('fromPot' => array('request' => 'stock_update','data'    => $articles)));
//print_r($potdata) ; die() ;
//$_start = time() ;
//$_startown = $_start - $_startown ;
	$ch = curl_init(WEBSHOP_API_STOCK_URL);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_TIMEOUT_MS, 600);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
/*
$_start = time() - $_start;

print_r($potdata.'<br>') ;
print_r($result.'<br>'); 
print_r($_processTimeStart.'<br>') ; 
print_r($_startown.'<br>') ; 
print_r($_start.'<br>') ;
*/
//die() ;
	$result = json_decode($result);


}

?>