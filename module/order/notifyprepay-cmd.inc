<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/mail.inc';

    function generatePdfFile() {
         global $User, $Id, $Config;

        $Navigation['Function'] = 'printorder';
		include(dirname(__FILE__) . '/../order/load.inc');

        $Config['savePdfToFile'] = true;
        include(dirname(__FILE__) . '/../order/printorder.inc');
        $Config['savePdfToFile'] = false;

         return fileName('order', (int)$Id);
     }

	switch ($Navigation['Parameters']) {
		case 'followup' :
			$_mailmark = "notifyprepayrepeat" ;
			break;
		default:
			$_mailmark = "notifyprepay" ;
			break;
	}

	set_time_limit(800);
	foreach($Lines as $orderid => $line) {
		$Id = $orderid ;
		
		// Generate and email
		$_pdffile = generatePdfFile() ; //fileName('order', (int)$Id); 

		//preparing pdf attachment for the mail
	   $_attachmentArray = array(
			array(
				'path'     => $_pdffile,
				'mimeType' => 'application/pdf',
				'name'     => sprintf('Order #%d.pdf', $orderid)
			)
		);

		$_param['body'] = nl2br(htmlentities($_POST['BodyTxt'])) . '<br>' ;
		$txt .= 'Send to: order ' . $Id . ' ' . $line['Email']. ' ' . $line['CompanyName'] ;
		if ($Config['Instance']=='Test')
			$_res = sendmail($_mailmark, $line['CompanyId'], NULL, $_param, NULL, false, "kc@passon-solutions.com", false, $_attachmentArray, sprintf('#%d from FUB to %s', $Id, $line['CompanyName']));
		else
			$_res = sendmail($_mailmark, $line['CompanyId'], NULL, $_param, NULL, false, $line['Email']  . ', sales@fub.dk', false, $_attachmentArray, sprintf('#%d from FUB to %s', $Id, $line['CompanyName']));
		if ($_res) {
			$txt .= ': emailing failed with error: ' . $_res . '<br>' ;
		} else {
			$txt .= ': emailing succeed<br>' ;
			$query = sprintf ('UPDATE `order` SET `PrepayNotified`=1 WHERE `Id`=%d', $Id) ;
			dbQuery ($query) ;
		}
//		sendmail("notifyprepay",$Record['CompanyId'], NULL, $_param, NULL, false, $line['Email']  . ', sales@fub.dk', false, $_attachmentArray, sprintf('#%d from FUB to %s', $Id, $line['CompanyName']));
//		if (is_file($_pdffile) AND !$Record['Ready']=='on') unlink ($_pdffile) ;
	}
	dbQueryFree ($res) ;
	
    return 'Completed:<br>' . $txt ;	
?>
