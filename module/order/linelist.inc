<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;

    switch ($Navigation['Parameters']) {
	case 'case' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Case', $Record['Id'], 'case.gif', 'caseview', $Record['Id']) ;
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }

    listStart () ;
    listHeader ('Lines') ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Order', 50) ;
    listHead ('Pos', 30) ;
    listHead ('State', 70) ;
    listHead ('Colour', 90) ;
    listHead ('Delivery', 70) ;
    listHead ('Quantity', 100, 'align=right') ;
    listHead ('Surplus', 70, 'align=right') ;
    listHead ('') ;

    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ('orderline.gif', 'view', (int)$row['Id']) ;
	listField ((int)$row['OrderId']) ;
	listField ((int)$row['No']) ;
	listField ($row['State']) ;

	$field = '' ;
	if ($row['VariantColor']) {
	    $field .= '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	}
	listFieldRaw ($field) ;

	$t = dbDateDecode($row['DeliveryDate']) ;
	listField (($t > 0) ? date ('Y-m-d', $t) : '') ;
	
	listField (number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
	listField ((int)$row['Surplus'] . ' %', 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No OrderLines', 'colspan=5') ;
    }

    listEnd () ;

    return 0 ;
?>
