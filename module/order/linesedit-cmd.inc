<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
 	require_once 'lib/csvftp.inc' ;
   require_once 'module/container/include.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

    // Get Stock to move from
    if ($_POST['ToStockId'] == 0) return 'please select Stock to receive items ' . $_POST['ToStockId'] ;
    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $_POST['ToStockId']) ;
    $result = dbQuery ($query) ;
    $ToStock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ToStock['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $_POST['ToStockId']) ;
    if ($ToStock['Type'] != 'fixed') return sprintf ('%s(%d) invalid Stock, type "%s"', __FILE__, __LINE__, $_POST['ToStockId']) ;

	// Proces each line.    
	foreach ($Lines as $j => $line) {
		//return ' Line: ' . (float)$line['ActualPriceConsumption'] . ' R ' . $line['ActualPriceCurrencyRate'] . ' L '  . (float)$line['LogisticCostConsumption'] . ' R '  . $line['LogisticCurrencyRate'] ; // Calculate stockvalue
		unset ($_orderlineqtyadj) ;
		
		// adjust orderquantitities
		foreach ($_POST['Quantity'][$j] as $artsizeid => $qty) {
			if ($qty=='' or is_null($qty)) $qty=0; // continue ; // no changes in quantity
			
			// Adjust qty in SO
			$query = sprintf ('select * from orderquantity rq where rq.orderlineid in (%s) and rq.articlesizeid=%d and rq.Active=1 and rq.quantity>0', $line['OrderLineId'], $artsizeid) ;
			$result = dbQuery ($query) ;
			$reqcount = dbNumRows ($result) ;
			if ($reqcount>1) return 'invalid number of order quantities - query ' . $query ; // only one item per articlesize is handled.
			$order_rq = dbFetch ($result) ;
			dbQueryFree($result) ;
	
			$_orderlineqtyadj[$order_rq['OrderLineId']] += $qty ;
			if ((float)$qty==(float)$order_rq['Quantity']) continue ; // no changes in quantity

			$unpackqty = ($order_rq['QuantityReceived']>$qty) ? $order_rq['QuantityReceived'] - $qty : $order_rq['QuantityReceived'];
			
			$_oq = array (
				'Quantity' 			=> $qty,
				'QuantityReceived' 	=> $order_rq['QuantityReceived']-$unpackqty,
				'QuantityUnpack' 	=> $unpackqty 
			) ;
//			if ($_oq['QuantityReceived']==0) $_oq['BoxNumber']=0 ; // s<Commented out to ave boxnumber for debugging for now.
			tableWrite('orderquantity', $_oq, $order_rq['Id']) ;
			
			// POst Process quantity?
			if ($_POST['Done'][$j]=='on') {
				// Only update prepacked after confirmation from stock?
				$_some = 1;
			}
		}
		
		// Adjust orderlines
		foreach ($_orderlineqtyadj as $orderlineid => $val) {
			$_qty = tableGetField('OrderLine', 'Quantity', $orderlineid) ;
			if (!($_qty==$val)) {
				$_ol = array (
					'Quantity' => $val 
				) ;
				tableWrite('orderline', $_ol, $orderlineid) ;
			} 
		}
	}
		
	// Create unpack request from SO and send data to Alpi as incomming (unpack to stock).
	$_qtyvalue = 'oq.quantityunpack' ;
	$_whereclause = 'and oq.quantityunpack>0' ;
	$query = sprintf ("Select 
								oq.id as LinjeID,
								ol.OrderId as ordrenummer,
								Variantcode.id as Varenummer,
								format(%s,0) as Antal
							FROM
								(orderline ol, orderquantity oq)
							LEFT JOIN VariantCode ON VariantCode.articleid=ol.articleid and VariantCode.articleColorid=ol.articleColorid and VariantCode.articlesizeid=oq.articlesizeid AND VariantCode.Active=1
							WHERE
								ol.OrderId=%d AND ol.Active=1 AND oq.orderlineid=ol.id %s and oq.active=1
					  ", $_qtyvalue, $Id, $_whereclause) ;
	// Target filename
	$_datetime = date('Ymd_His'); 
	$ftp_file = 'PURCHASE_' . $_datetime . '_SO' . $Id .  'Unpack.txt';
	if ($_res = CreateCSV($query, $ftp_file)) {
		echo 'Information transfer failed, Close and try to retransmit again later.';
	} else {
		$_record = array (
			'LogText' => 'Unpack request send at ' . ' ' . dbDateEncode(time()) 
		);
		tableWrite('order', $_record, $Id) ;
		// Delete empty orderlines.
		$query = "update orderline set active=0 where quantity=0 and orderid=" . $Id ;
		dbQuery ($query) ;
/* Only reset data on confirmation back from stock
		$result = dbQuery ($query) ;
		while ($row = dbFetch ($result)) {                              
			$_record = array (
				'QuantityUnpack' => 0,
			);
			tableWrite('orderquantity', $_record, $row['LinjeID']) ;
		}
		dbQueryFree ($result) ;
*/		
		return 'Information transfer to Alpi succeeded and prepacked data adjusted. You can now close the window.<br><br>' .  $csv_data_list ;
	}
    return navigationCommandMark ('orderview', $Id) ;
?>
