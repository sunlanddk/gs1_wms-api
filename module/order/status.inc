<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
require_once 'lib/table.inc';

    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['Id']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemField ('State', $Record['State']) ;
    itemSpace () ;
    itemEnd () ;


 
    listStart () ;
    listRow () ;
//    listHeadIcon () ;
    listHead ('Pos', 30) ;
    listHead ('Article', 75) ;
    listHead ('Colour', 70) ;
    listHead ('Description') ;
    listHead ('Done', 70) ;
    listHead ('Delivery', 70) ;
listHead ('', 23) ;
listHead('', 59);
    if ($SizeCount > 0) {
	listHead ('Quantity', 50) ;
	for ($n = 1 ; $n < $SizeCount ; $n++) listHead ('', 60) ; 
    }
    listHead ('Total', 100, 'align=right') ;
    listHead ('', 8) ;

    foreach ($Line as $i => $row1) {
	$SortedLines[(int)$row1['No']] = $row1 ;
    }
//    asort($Line) ;

    foreach ($Line as $i => $row) {
	// Spacing
	listRow () ;
	listField () ;
	//print_r($row['Size']);
	// Size Header
	if ($row['VariantSize']) {
	    listRow () ;
	    listField ('', 'colspan=8') ;
	    foreach ($row['Size'] as $s) listField ($s['Name'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	}

	// Items Ordered
	listRow () ;
//	listFieldIcon ('orderline.gif', 'orderlineview', (int)$row['Id']) ;
	listField ((int)$row['No']) ;
	listField ($row['ArticleNumber']) ;

	if ($row['VariantColor']) {
	    listField ($row['ColorNumber']) ;	    
	} else {
	    listField () ;
	}

	listField ($row['Description']) ;

	$s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
	listfield ($s);
	
	$t = dbDateDecode($row['DeliveryDate']) ;
	listField (($t > 0) ? date ('Y-m-d', $t) : '') ;

	listField ('Ordered', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
	
	if ($row['VariantSize']) {
//	    foreach ($row['Size'] as $s) listField ((int)$s['Quantity'], 'align=right') ;
	    foreach ($row['Size'] as $s) listField (number_format((float)$s['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	}
	if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d', $SizeCount-count($row['Size']))) ;

	listField (number_format((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	//link to production order
	listRow();
	listField ('', 'colspan=2') ;
	if ($row['VariantColor'] and (int)$row['ColorGroupId'] > 0) {
	    listFieldRaw (sprintf ('<div style="width:57px;height:15px;margin-left:6px;margin-top:1px;background:#%02x%02x%02x;"></div>', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	} else {
	    listField () ;
	}
	if($row['ProductionId']){
	  listField ('', 'colspan=2') ;
	  listField(tableGetField('Production','SubDeliveryDate',$row['ProductionId']));
	  listFieldIcon('production.gif', 'ord_production', $row['ProductionId']);
	  listField($row['CaseId'] . "/" . $row['ProductionNo']);
	} else {
	  listField ('', 'colspan=4') ;
	  listField('');
	}
	if($row['VariantSize']){
	  foreach($row['Size'] as $s=>$dmp) {
	    //listField($sizes[$s]);
	    listField (number_format((float)$row['Production'][$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
	  }
	}
	   if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d', $SizeCount-count($row['Size']))) ;

	   //listField (number_format((float)$row['Production']['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;

	// Items Shipped
	listRow () ;
	listField ('', 'colspan=6') ;
	listField ('Shipped', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
	if ($row['VariantSize']) {
	    foreach ($row['Size'] as $s) {
	      listField (number_format((float)$s['QuantityShipped'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	    }
	}

	if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;"', $SizeCount-count($row['Size']))) ;

	listField (number_format((float)$row['QuantityShipped'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;

	// shipments
	foreach($row['Shipments'] as $shp =>$sizes) {
	  listRow();
	  listField('','colspan=4');
	  $t = date('Y-m-d', dbDateDecode(tableGetField('Stock','DepartedDate',$shp))) ;
	  $t = ($t > '0001-01-01') ? $t : '' ;
	  listField($t);
	  listField(tableGetField('Stock','DepartureDate',$shp));
	  listFieldIcon('shipment.gif','ord_shipment',$shp);
	  listField(tableGetField('Stock','Name',$shp));
	  if($row['VariantSize']) {
	    foreach($row['Size'] as $s=>$dmp) {
	      listField (number_format((float)$sizes[$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
	    }
	  }
	     if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style=""', $SizeCount-count($row['Size']))) ;


	}
	// Items Invoiced
	listRow () ;
	listField ('', 'colspan=6') ;
	listField ('Invoiced', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
	if ($row['VariantSize']) {

	  foreach ($row['Size'] as $s) listField (number_format((float)$s['QuantityInvoiced'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	}
	if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;"', $SizeCount-count($row['Size']))) ;
	
	listField (number_format((float)$row['QuantityInvoiced'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	listField ('') ;
    
	foreach($row['Invoices'] as $inv =>$sizes) {
	  listRow();
	  listField('','colspan=4');
  	  $t = date('Y-m-d', dbDateDecode(tableGetField('Invoice','ReadyDate',$inv))) ;
	  $t = ($t > '2001-01-01') ? $t : '' ;
	  listField($t);

//	  listField(tableGetField('Invoice','ReadyDate',$inv));
	  listField('','colspan=1');
	  listFieldIcon('invoice.gif','ord_invoice',$inv);
	  $_invNo = (int)tableGetField('Invoice','Number',$inv) ;
	  listField($_invNo>0?$_invNo:'draft');
	  if($row['VariantSize']) {
	    foreach($row['Size'] as $s=>$dmp) {
	      //listField($sizes[$s]);
	      listField (number_format((float)$sizes[$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
	    }
	  }

	}
	listRow();
	listField('',sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;height:5px"', $SizeCount + 9));
    }

    if (count($Line) == 0) {
	listRow () ;
	listField () ;
	listField ('No OrderLines', 'colspan=2') ;
    }
    
    listEnd () ;

    return 0 ;
?>
