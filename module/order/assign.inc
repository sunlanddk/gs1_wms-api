<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;
    
    itemStart () ;
    itemHeader () ;
//    itemFieldRaw ('Shipment', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 AND type="shipment" ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    $query = sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="shipment" AND Active=1 AND CompanyId=%d ORDER BY Value', (int)$Record['CompanyId']) ;
    itemFieldRaw ('To Shipment', formDBSelect ('StockId', 0, $query, 'width:250px')) ;

    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Orderline', 90) ;
    listHead ('Description')  ;
    listHead ('Delivery', 80, 'align=right') ;
    listHead ('', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
    	listRow () ;
     	listFieldIcon ('orderline.gif', 'orderlineview', 0) ;
    	listField ($row['Id']) ;
    	listField ($row['Description']) ;
    	listField ($row['DeliveryDate']) ;
    	listFieldRaw (formCheckbox (sprintf('Orderlineid[%d]', (int)$row['Id']), 1)) ;
    }
    if (dbNumRows($Result) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Orderlines', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;


    formEnd () ;

    return 0 ;
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
