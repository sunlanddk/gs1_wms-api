<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
	$s = ($Record[$field]) ? sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) : 'No' ;
	if ($field == 'Done' and $Record['Done'] and (int)$Record['OrderDoneId'] > 0) {
	    $s .= ', ' . htmlentities($Record['OrderDoneName']) ;
	}
	itemField ($field, $s) ;
    }
    itemStart () ;
	if ((int)$Record['Proposal']>0) {
		itemField ('Proposal', '(change to order by resetting proposal flag using EDIT)') ;
		itemSpace () ;
	}
    itemHeader () ;
    itemField ('Order', (int)$Record['Id']) ;
    if($Record['ConsolidatedId']) {
        itemFieldIcon ('Consolidated', $Record['ConsolidatedId'], 'order.png', 'conorderview', $Record['ConsolidatedId']) ;
	}
	if (dbNumRows($cosolidatedorders)==1) {
		if ($Record['PickOrderId']>0) {
			if (!$User['Extern'] AND !$User['SalesRef'])
				itemFieldIcon ('PickOrder', $Record['PickOrderState'], 'cart.gif', 'picklist', $Record['PickOrderId'], '', 'pack.png', 'packlist') ;
			} else {
			if (!$User['Extern'] AND !$User['SalesRef'])
				$PickOrderId = tableGetFieldWhere('pickorder', 'Id', sprintf("referenceid=%d and Type='SalesOrder' and Active=1 and packed=1", (int)$Record['Id'])) ;
	//return 'pickid ' . $PickOrderId . 'query ' . sprintf("referenceid=%d and Type='SalesOrder' and Active=1 and packed=1", (int)$Record['Id']) ;
			if ($PickOrderId>0) {
				itemFieldIcon ('PickOrder', 'Done', 'cart.gif', 'picklist', $PickOrderId, '', 'pack.png', 'packlist') ;
			}
		}
	} else {
		ItemField('PickOrder', 'Handle from Consolidated order') ;
	}
	if (!$User['Extern']) {
		itemField ('Description', $Record['Description']) ;
		if (!(is_null($Record['LogText'])) or !$Record['LogText']=='') {
			itemField ('LogText', $Record['LogText']) ;
		}
	}
    if ($Record['SeasonId']>0) {
		if ($User['Extern'] OR $User['SalesRef']) 
			itemField ('Season', tableGetField('season','Name',$Record['SeasonId'])) ;
		else
			itemFieldIcon ('Season', tableGetField('season','Name',$Record['SeasonId']), 'season.png', 'seasonview', $Record['SeasonId']) ;
    }
	itemSpace () ;
    itemField ('State', $Record['State']) ;
	if (!$User['Extern'] AND !$User['SalesRef'])
		itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['ToCompanyId']), 'company.gif', 'companyview', $Record['ToCompanyId']) ;
	if ($Record['PurchaseOrderId']>0)
		itemFieldIcon ('From PO',$Record['PurchaseOrderId'] , 'requisition.png', 'requisitionview', $Record['PurchaseOrderId']) ;
//	if ($Record['ToPurchaseOrderId']>0)
//		itemFieldIcon ('To PO',$Record['ToPurchaseOrderId'] , 'requisition.gif', 'requisitionview', $Record['ToPurchaseOrderId']) ;
    itemSpace () ;
    if (!$User['Extern']) 
		flag ($Record, 'Ready') ;
    if (!$User['Extern']) 
		flag ($Record, 'Done') ; 
 	itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    printf ("<table class=item><tr><td>\n") ;

    itemStart () ;
    itemHeader ('Sales') ;
	if ($User['Extern'])  {
		itemField('Customer', $Record['CompanyNumber']) ;
	} else if ($User['SalesRef']) { 
		if ((int)$Record['SalesUserId']==(int)$Record['CompanySalesUserId']) {
			itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
		} else {
			itemField($Record['SalesUserId'], $Record['CompanyNumber']) ; 
		}
	} else {
		itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
	}
    itemField ('Name', empty($Record['AltCompanyName'])?$Record['CompanyName']:$Record['AltCompanyName'].' ('.$Record['CompanyName'].')') ;    
    itemSpace () ;
    itemField ('Reference', $Record['Reference']) ;
    itemSpace () ;
    itemField ('Sales Ref', tableGetField ('User', 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', (int)$Record['SalesUserId'])) ;  
    itemSpace () ;
    itemEnd () ;

    printf ("</td>") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader ('Confirmation/Invoice') ;
    itemField ('Currency', tableGetField ('Currency', 'CONCAT(Description," (",Name,")")', (int)$Record['CurrencyId'])) ;  
    itemSpace () ;
    itemField ('Payment Term', tableGetField ('PaymentTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['PaymentTermId'])) ;  
    itemSpace () ;
	if (!$User['Extern'] AND !$User['SalesRef'])
		itemField ('VariantCodes', $Record['VariantCodes']?'Yes':'No') ;
    itemFieldText ('Header', $Record['InvoiceHeader']) ;
    itemFieldText ('Footer', $Record['InvoiceFooter']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    
    itemStart () ;
    itemHeader ('Delivery') ;
    itemField ('Delivery Terms', tableGetField ('DeliveryTerm', 'CONCAT(Name," (",Description,")")', (int)$Record['DeliveryTermId'])) ;  
    itemSpace () ;
    itemField ('Carrier', tableGetField ('Carrier', 'Name', (int)$Record['CarrierId'])) ;  
    itemSpace () ;
    itemField ('Street', $Record['Address1']) ;
    itemField ('', $Record['Address2']) ;
    itemField ('ZIP', $Record['ZIP']) ;
    itemField ('City', $Record['City']) ;
    itemField ('Country',  tableGetField ('Country', 'CONCAT(Name," - ",Description)', (int)$Record['CountryId'])) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    if($Record['ConsolidatedId'] > 0){
        itemStart () ;
        itemHeader ('Consolidated Orders') ;
        while ($row = dbFetch($cosolidatedorders)) {
            listRow () ;
            listFieldIcon ($Navigation['Icon'], 'orderviewcon', (int)$row['Id']);
            listfield ($row['Id']);
        }
        itemEnd () ;
    }

    printf ("</td>\n") ;
    printf ("</tr></table>\n") ;

    listStart () ;
    listHeader ('Lines') ;
    listRow () ;
    listHead ('', 50) ;
    listHead ('Pos', 30) ;
    listHead ('Case', 50) ;
    listHead ('Article', 75) ;
    listHead ('Colour', 90) ;
    listHead ('Description') ;
    listHead ('Sizes', 200) ;
    listHead ('Delivery', 70) ;
    listHead ('Done', 70) ;
    listHead ('Quantity', 100, 'align=right') ;
    listHead ('Boxnumbers', 100, 'align=right') ;
    listHead ('Surplus', 70, 'align=right') ;
    listHead ('Discount', 70, 'align=right') ;
    listHead ('Price', 90, 'align=right') ;
    listHead ('Total', 90, 'align=right') ;
    listHead ('', 8) ;

    while ($row = dbFetch($Result)) {
		listRow () ;
		listStyleIcon ('orderlineview', $row) ;
		listField ((int)$row['No']) ;
		listField (((int)$row['CaseId'] > 0) ? (int)$row['CaseId'] : '') ;
		listField ($row['ArticleNumber']) ;

		$field = '' ;
		if ($row['VariantColor']) {
			$field .= '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
			if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
			$field .= 'display:inline;"></div>' ;
			$field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorDescription']) ;	    
		}
		listFieldRaw ($field) ;

		listField ($row['Description']) ;

		$query = 'SELECT group_concat(az.name) AS Sizes FROM orderquantity oq, articlesize az where oq.quantity>0 and oq.active=1 and oq.orderlineid=' . $row['Id'] . ' and az.id=oq.articlesizeid group by oq.orderlineid';
	    $res = dbQuery ($query) ;
	    $_row = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		listField ($_row['Sizes']) ;
	
		
		$t = dbDateDecode($row['DeliveryDate']) ;
		listField (($t > 0) ? date ('Y-m-d', $t) : '') ;
		$s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
		listfield ($s);
		listField (number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
		$query = 'SELECT group_concat(cast(boxnumber as char)) as box from (select boxnumber FROM orderquantity oq where oq.quantity>0 and oq.active=1 and orderlineid=' . $row['Id'] . ' group by boxnumber) Tab';
	    $res = dbQuery ($query) ;
	    $_row = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		
//		$_boxnumber=$tableGetfieldwhere('max(BoxNumber)', 'orderquantity', sprintf('orderlineid=%d',$row['Id'])) ;
		listField ($_row['box'], 'align=right') ;
		listField ((int)$row['Surplus'] . ' %', 'align=right') ;
		listField ((int)$row['Discount'] . ' %', 'align=right') ;
		listField (number_format((float)$row['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
		$v = (float)$row['PriceSale'] * (float)$row['Quantity'] ;
		if ((int)$row['Discount'] > 0) $v -= $v * (int)$row['Discount'] / 100 ;
		$total += $v ;
		  $qtytotal += (float)$row['Quantity'] ;
		listField (number_format($v, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No OrderLines', 'colspan=2') ;
    } else {
		// Total
		listRow () ;
		listField ('', 'colspan=9 style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ((float)$qtytotal, 2, ',', '.'), 'align=right style="border-top: 1px solid #cdcabb;"') ;
		listField ('', 'colspan=4 style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ($total, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right style="border-top: 1px solid #cdcabb;"') ;
    }
    listEnd () ;

    return 0 ;
?>
