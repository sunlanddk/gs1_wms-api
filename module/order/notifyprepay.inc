<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/list.inc' ;
    
    function flag (&$Record, $field, $name='') {
		if ($name=='') $name=$field ;
		if ($Record[$field]) {
			itemField ($name, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($name, formCheckbox ($field, $Record[$field])) ;
		}
    }
	
	switch ($Navigation['Parameters']) {
		case 'followup' :
			$_mailmark = "notifyprepayreminder" ;
			break;
		default:
			$_mailmark = "notifyprepay" ;
			break;
	}
    $BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="'.$_mailmark.'"') ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Season', $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;
	
//    itemSpace () ;
//    itemFieldRaw ('Email', formText ('Mail', $Record['Mail'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Email Txt', formtextArea ('BodyTxt', $BodyTxt, 'width:100%;height:200px;')) ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;

    listStart () ;
    listHeader ('Orders') ;
    listRow () ;
    listHead ('Order #', 30) ;
    listHead ('Customer', 90) ;
    listHead ('Email', 90) ;
    listHead ('', 90) ;

	
	foreach($Lines as $orderid => $line) {
		listRow () ;
		listField($line['Id']) ;
		listField($line['CompanyName']) ;
		listField($line['Email']) ;
		listField('') ;
//		echo 'Order #' . $line['Id'] . ' to Customer: ' . $line['CompanyName'] . ' Email: ' . $line['Email'] . '<br>';
	}
	ListEnd() ;
	
    return 0 ;
?>
