<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    // Validate
    if ($Record['Done']) return 'the OrderLine can not be added when Order is Done' ;
    if ($Record['Ready']) return 'the OrderLine can not be added when Order is Ready' ;
 
    // Find next position
    $query = sprintf ('SELECT MAX(No) AS No FROM OrderLine WHERE OrderId=%d AND Active=1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $No = (int)$row['No'] ;

    // Default DeliveryDate
    if ($No == 0) {
		// No existing OrderLine to default to
		unset ($row) ;
		$row['DeliveryDate'] = dbDateEncode (time() + 60*60*24*60) ;
		$row['Surplus'] = $Record['CompanySurplus'] ;
    } else {
		// Default to DeliveryDate in last OrderLine
		$query = sprintf ('SELECT * FROM OrderLine WHERE OrderId=%d AND No=%d AND Active=1', (int)$Record['Id'], $No) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
    }

    $fields = array (
		'ArticleNumber'	=> array ('type' => 'set'),
		'ArticleId'		=> array ('check' => true),
		'Description'	=> array ('type' => 'set'),
		'DeliveryDate'	=> array ('type' => 'set',		'value' => $row['DeliveryDate']),
		'Surplus'		=> array ('type' => 'set',		'value' => $row['Surplus']),
		'OrderId'		=> array ('type' => 'set',		'value' => (int)$Record['Id']),
		'No'			=> array ('type' => 'set',		'value' => $No + 1)
    ) ;
 
    function checkfield ($fieldname, $value, $changed) {
		global $Id, $fields, $Record, $ArticleTypeFields ;
		switch ($fieldname) {
			case 'ArticleId':
				// Validate
				// Get Article
				$query = sprintf ('SELECT Id, Number, Description FROM Article WHERE Id="%s" AND Active=1', (int)$value) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
				dbQueryFree ($result) ;
				if ((int)$row['Id'] == 0) return 'Article not found - value '. $value ;

				// Save Article number
				$fields['ArticleNumber']['value'] = (int)$row['Number'] ;
				$fields['Description']['value'] = $row['Description'] ;

				// ArticleNumber are not to be used
				return true ;
		}
		return false ;
    } 

    // Read fields
    $res = saveFields ('OrderLine', -1, $fields) ;
    if ($res) return $res ;

    return navigationCommandMark ('orderlineedit', dbInsertedId ()) ;

?>
