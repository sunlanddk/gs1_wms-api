<?php

    require_once 'lib/table.inc' ;

    // Order Ready ?
    if ($Record['OrderDone']) return 'the Order is Done' ;
    if ($Record['OrderReady']) return 'the Order is Ready' ;
 
    // Do delete  
    tableDelete ('OrderLine', $Id) ;
    tableDeleteWhere ('OrderQuantity', sprintf ('OrderLineId=%d', $Id)) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM OrderLine WHERE OrderId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['OrderId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['No']) {
	    $query = sprintf ("UPDATE OrderLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
