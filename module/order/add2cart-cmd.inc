<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once  "module/sales/cart.inc" ;
//return 'scr ' . $_POST['WholeSalePrice']; 

    global $User;

	// Get active Currency
	$_currencyId = isset($_COOKIE['currencyId']) ? $_COOKIE['currencyId'] : NULL ;
	 if ($_currencyId == NULL) {
		$_currency 	= Cart::getCartCurrency('ordercart');
		$_currencyId= isset($_currency['Id']) ? $_currency['Id'] : NULL;
	 }
	$query = sprintf("select * from `currency` c where c.Id = %d", $_currencyId);
	$result = mysql_query($query);
	$_currencyname = mysql_fetch_assoc($result);
	mysql_free_result($result);

	 // Get cart
	 $query = sprintf("SELECT * FROM usercart WHERE CartName='ordercart' and UserId=%d", $User['Id']);
	 $res = dbQuery($query);
	 $_usercart = dbFetch($res);
	 dbQueryFree($res);
	 if ($_usercart == NULL) {
		$_usercart = array(
		   "CartName" => $cartName,
		   "UserId"   => $User['Id'],
		   "Cart"     => ""
		);
	 }
//print_r($_usercart) ;
	// Get cart
	$_cart = json_decode(str_replace('\\', '', $_usercart['Cart']), true);
	if (is_null($_cart) || count($_cart) == 0) {
		$_cart = array();
	}
	
	$art["ArticleId"] 		  = $Record[ArticleId];
	$art["ArticleTypeId"] 	  = $Record[ArticleTypeId];
	$art["Price"] 			  = 0 ;
	$art["Desc"] 			  = $Record[ArticleDescription];
	$art["Number"] 			  = $Record[ArticleNumber];
	$art["TopBottom"] 		  = 0 ;
	$art["CurrencyId"] 	      = 	$_currencyId ; // ??? first insert ???
	$art["CategoryGroupId"] 	  = 0 ;

	foreach($_POST['Quantity'] as $cid => $color) {
		$query = sprintf ("SELECT ac.id as ArticleColorId, c.Description as ColorDesc, c.Number as ColorNumber, cm.Id as CollectionMemberId, 
								  cl.Id as lotId, cl.Name as lotName, cl.Date as lotDate, cl.presale as Unlimited, cl.ShowOnly as ShowOnly,
								  s.id as SeasonId, s.Name as SeasonName, cmp.Id as CollectionMemberPriceId
						FROM (collectionmember cm, collectionlot cl, collectionmemberprice cmp, articlecolor ac, color c, collection col, season s)
						where cm.id=%d and col.id=cm.collectionid and s.id=col.seasonid
						and ac.id=cm.articlecolorid and c.id=ac.colorid
						and cl.id=cm.collectionLOTId
						and cmp.collectionmemberid=cm.id and cmp.currencyid=%d and cmp.active=1
						and cm.active=1 and ac.active=1", $cid, $_currencyId) ;
//return $query ;
	    $res = dbQuery ($query) ;
	    $row = dbFetch ($res) ;
  	    dbQueryFree ($res) ;
		$art["ArticleColorId"] 	  	= $row['ArticleColorId'];
		$art["ColorNumber"] 		= $row['ColorNumber'];
		$art["ColorDesc"] 		  	= $row['ColorDesc'];
		$art["CollectionMemberId"] 	= $row['CollectionMemberId'];
		$art["LOTId"] 	          	= $row['lotId'];
		$art["LOTName"] 	        = $row['lotName'];
		$art["LotDate"] 	        = $row['lotDate'];
		$art["SeasonId"] 		  	= $row['SeasonId'];
		$art["SeasonName"] 		  	= $row['SeasonName'];
		$art["qty"] 		  		= 0;
		$art["subTotal"] 		  	= 0;

		$_subtotal = 0 ;
		$art["Sizes"] = array() ;
		foreach($color as $sid => $value) {
			if ($value>0) {
				$art["Sizes"][$sid]["qty"] = (int)$value ;
				$_subtotal += (int)$value ;
				$art["Sizes"][$sid]["name"] = tableGetField('articlesize','Name',$sid) ;
				$art["Sizes"][$sid]["sizeprice"] = tableGetFieldWhere('collectionmemberpricesize','WholeSalePrice', 'articlesizeid='.$sid.' and collectionmemberpriceid='.(int)$row['CollectionMemberPriceId']) ;
				$art["Price"] = $art["Sizes"][$sid]["sizeprice"] ;
			}
		}
		// Add to cart?
		if ($_subtotal > 0) {
			
			 $_articleId        = (int)$art['ArticleId'];
			 $_articleColorId   = (int)$art['ArticleColorId'];
			 $_seasonId         = (int)$art['SeasonId'];

			 $_exists = false;
			 if (array_key_exists($_seasonId, $_cart)) {
				foreach ($_cart[$_seasonId]['items'] as &$item) {
				   if (((int)$item['ArticleId'] == $_articleId) && ((int)$item['ArticleColorId'] == $_articleColorId)) {
					  $item['Sizes'] = $art['Sizes'];
					  $_exists       = true;
					  break;
				   }
				}
			 } else {
				$_cart[$_seasonId] = array(
				   'seasonName' => $art['SeasonName'],
				   'currency'   => $_currencyname,
				   'subTotal'   => 0,
				   'qty'        => 0,
				   'items'      => array()
				);
			 }
			 if (!$_exists) {
				array_push($_cart[$_seasonId]['items'], ($art));
			 }
			 $_cartInfo = Cart::get_cart_info($_cart);
			 
			 $_usercart['Cart'] = json_encode($_cart);
//print_r($_cart) ;
			 tableWrite('usercart', $_usercart, (int)$_usercart['Id']);

		} 
	}
//	$_artjson = json_encode($art) ;
 // print_r($art) ;  
 	return navigationCommandPopupClose() ;
//	return navigationCommandMark ('backtolist') ;

    return 0 ;    
    return 'test ' ;    
	return navigationCommandMark ('collmemberview', (int)$Record['CollectionMemberId']) ;
?>
