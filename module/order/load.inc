<?php

    require_once _LEGACY_LIB.'/lib/navigation.inc' ;
    require_once _LEGACY_LIB.'/lib/fieldtype.inc' ;
    require_once _LEGACY_LIB.'/lib/lookups.inc';

	if ($User['Restricted']) {
		$LimitClause = sprintf(' AND User.Loginname IN ( %s ) ', $User['UserList']);
	} else {
		$LimitClause = '';
	}

//    $StateField = 'IF(Order.Done,"Done",IF(NOT Order.Ready,"Defined","Ready"))' ;
    $StateField    = 'IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0, "Draft",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1, "Proposal",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=1, "Confirmed",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Shipped",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Undefined" )))))';

    $queryFields = '`Order`.*,
		    Currency.Symbol AS CurrencySymbol,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, if(isnull(`ORDER`.Email),Company.Mail,`ORDER`.Email)  AS Mail,
		    Company.Surplus AS CompanySurplus, Company.SalesUserId as CompanySalesUserId,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName,
		    OrderDone.Name AS OrderDoneName, PickOrder.Id as PickOrderId,
		    Season.Name as SeasonName,
		    ' . $StateField . ' AS State' ;

    $queryTables = '`Order`
		    LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
		    LEFT JOIN PickOrder ON PickOrder.ConsolidatedId=Order.ConsolidatedId and PickOrder.Active=1 and PickOrder.Packed=0
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    LEFT JOIN User ON User.Id=Order.SalesUserId
		    LEFT JOIN Season ON Season.Id=Order.SeasonId
		    LEFT JOIN OrderDone ON OrderDone.Id=Order.OrderDoneId' ;

	$companylimit = sprintf('`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) AND `Order`.Draft = 0 ', $User['Id']);
	$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);

    switch ($Navigation['Function']) {
	case 'list' :
		$companylimit = sprintf('`Order`.ToCompanyId = %d AND `Order`.Draft = 0 ', $User['CompanyId']);


		$state_options = array('Ready' => 'Ready', 'Defined' => 'Defined', 'Done' => 'Done');

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Order.Id',			'direct' => 'Order.Active=1', 	'type' => FieldType::INTEGER),
		array ('name' => 'Consolidated No',	'db' => 'Order.ConsolidatedId',								'type' => FieldType::INTEGER),
		array ('name' => 'Company',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")', 	'type' => FieldType::STRING),
//		array ('name' => 'State',		'db' => $StateField, 										'type' => FieldType::SELECT, 	'options' => $state_options),
		array ('name' => 'State',		'db' => $StateField, 										'type' => FieldType::STRING),
		array ('name' => 'Season',		'db' => 'Season.Id', 										'type' => FieldType::SELECT, 	'options' => getOwnerSeasons($User['CompanyId'])),
//		array ('name' => 'Season',		'db' => 'Season.Name', 										'type' => FieldType::STRING),
		array ('name' => 'Reference',	'db' => 'Order.Reference', 									'type' => FieldType::STRING),
		array ('name' => 'Description',	'db' => 'Order.Description', 								'type' => FieldType::STRING),
		array ('name' => 'Sales person','db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', 'type' => FieldType::STRING),
		array ('name' => 'Created',		'db' => 'Order.CreateDate', 								'type' => FieldType::DATE)
	    ) ;

	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Clause
		    $queryClause = sprintf ('Order.SeasonId=%d AND Order.Active=1 AND %s', $Id, $companylimit) ;
		    $HideCompany = false ;
		    break ;

		case 'company' :
		case 'company-new' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Display
		    $HideCompany = true ;

		    // Navigation
		    if ($Record['TypeCustomer']) {
			navigationEnable ('new') ;
		    }
			navigationEnable ('orderlinesrep') ;

		    // Clause
		    $queryClause = sprintf ('Order.CompanyId=%d AND Order.Active=1 AND %s', $Id, $companylimit) ;
		    break ;

		default :
		    // Display
		    $HideDone = true ;

		    // Clause
		    $queryClause = 'Order.Done=0 AND Order.Active=1 AND ' . $companylimit ;
		    break ;
	    }

		$queryClause .= $LimitClause;

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'orderview') ;
	    return $res ;

	case 'list-req' :
		$queryFields = '`requisition`.*,
				Company.Number AS CompanyNumber, Company.Name AS CompanyName, `order`.id as ToId' ;

		$queryTables = '`requisition`
				LEFT JOIN Company ON Company.Id=requisition.FromCompanyId
				LEFT JOIN `order` ON requisition.id=order.purchaseorderid and `order`.active=1' ;

//		$queryClause = sprintf('`requisition`.CompanyId = %d AND `Order`.Id is null ', $User['CompanyId']);
		$queryClause = sprintf('`requisition`.CompanyId = %d and `requisition`.done=0 and `requisition`.ready=1
		and `order`.id is null', $User['CompanyId']);

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'requisition.Id'),
		array ('name' => 'Company',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")'),
		array ('name' => 'Description',	'db' => 'requisition.Description'),
		array ('name' => 'Created',		'db' => 'requisition.CreateDate')
	    ) ;

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'orderview') ;
	    return $res ;



	case 'basic' :
	case 'basic-cmd' :
	case 'upd-delivery-cmd' :
	case 'upd-discount-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'Seasonnew' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName, Season.Is as SeasonId FROM Company, Season
						WHERE Season.Id=%d AND Company.Id=Season.CompanyId AND AND Season.Active=1 AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    return 0 ;
		case 'new' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName, Company.Id as CompanyId FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    return 0 ;
		case 'new-purch' :
			$PurchaseOrderId = $Id ;

		    // Specific company
		    $Id = tableGetField('requisition', 'FromCompanyId', $Id) ;

		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName
		                       FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
			$Record['PurchaseOrderId'] = $PurchaseOrderId ;
		    return 0 ;
		case 'consorder' :
	    }

	    // Fall through

	case 'gen-purchase-cmd' :
	case 'gen-purch-line-cmd' :
	case 'gen-purchase' :
	case 'gen-purchase-line' :
	case 'linenew' :
	case 'linenew-cmd' :
	case 'revert' :
	case 'revert-cmd' :
	case 'delete-cmd' :
	case 'process' :
	    // Query Order
	  $query = sprintf ('SELECT %s FROM %s WHERE Order.Id=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;

	case 'view' :
	    global $Result, $DifIdLoad ;				// For direct view only
//	dbQuery("SET CHARACTER SET 'utf8'");
//	dbQuery("SET collation_connection = 'utf8_danish_ci'");
		$companylimit = sprintf('`Order`.ToCompanyId = %d AND `Order`.Draft = 0 ', $User['CompanyId']);
		if ($User['Extern']) {
			$companylimit = sprintf('(`Order`.CompanyId = %d) AND `Order`.Draft = 0 ', $User['CompanyId']);
//			$companylimit = ' 1=1' ;
		} else {
			$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);
		}
	    // Query Order
	    $query = sprintf ('SELECT %s, po.Id as PickOrderId,
					if(po.Packed>0,"Done", if (po.updated, "Updated", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId=0,"Ready","Assigned"))))) as PickOrderState
					FROM %s
					LEFT JOIN PickOrder po ON (po.ReferenceId=Order.Id or po.ConsolidatedId=Order.ConsolidatedId) and po.Type="SalesOrder" and po.Active=1 and po.packed=0
					LEFT JOIN Stock s ON po.toid=s.id
					WHERE Order.Id=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Query OrderLines
	    if($Record['OrderTypeId'] == 10 AND $Record['ConsolidatedId'] > 0){
	    	 // Query OrderLines
		    $query = sprintf ('SELECT OrderLine.*,
		    Article.Number AS ArticleNumber, Article.VariantColor,
		    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
		    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
		    FROM `order`
		    LEFT JOIN orderline ON orderline.OrderId=`order`.Id AND orderline.Active=1
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    WHERE `order`.ConsolidatedId=%d AND OrderLine.Active=1 ORDER BY OrderLine.No', $Record['ConsolidatedId']) ;
	    } else {
		    $query = sprintf ('SELECT OrderLine.*,
		    Article.Number AS ArticleNumber, Article.VariantColor,
		    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
		    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
		    FROM OrderLine
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 ORDER BY OrderLine.No', $Id) ;
		}
	    $Result = dbQuery ($query) ;

	    $query = sprintf ('SELECT *
		    FROM `order`
		    WHERE `order`.ConsolidatedId=%d', $Record['ConsolidatedId']) ;
	    $cosolidatedorders = dbQuery ($query) ;
	    if(dbNumRows($cosolidatedorders) > 1){
//	    	navigationPasive ('generate') ;
//	    	navigationPasive ('unpackbyline') ;
//	    	navigationPasive ('invoice') ;
	    	navigationPasive ('generatepickorder') ;
//	    	navigationPasive ('confandemail') ;
	    	//navigationPasive ('print') ;
	    }
	    $DifIdLoad = (int)$Record['ConsolidatedId'];
//		if ($User['SalesRef']) 
//			navigationPasive ('new') ;
	    // Navigation
	    if ($Record['Done']) {
			navigationPasive ('new') ;
//			navigationPasive ('unpackbyline') ;
			navigationPasive ('shipment') ;
			navigationPasive ('process') ;
			navigationPasive ('production') ;
	    }

	    if ($Record['Ready']) {
			navigationPasive ('new') ;
//			navigationPasive ('unpackbyline') ;
	    } else {
			navigationPasive ('shipment') ;
			navigationPasive ('process') ;
		// navigationPasive ('production') ;
	    }

	    return 0 ;

	case 'notifyprepay' :
	case 'notifyprepay-cmd' :
		switch ($Navigation['Parameters']) {
			case 'followup' :
				$_notified = 1 ;
				break;
			default:
				$_notified = 0 ;
				break;
		}
		$query = sprintf("SELECT o.Id as Id, c.name as CompanyName, c.Id as CompanyId, c.mail as Email,
					min(ol.deliverydate) as DeliveryDate, 
					sum(if(oq.quantity>0,oq.quantity,ol.quantity)*ol.pricesale*(100-ol.discount)/100) AS Price, cu.Name as Currency,
					sum(if(oq.quantity>0,oq.quantity,ol.quantity)*ol.pricesale*(cu.rate/100)*(100-ol.discount)/100) AS DKKPrice
				FROM (`order` o, orderline ol, company c, paymentterm p)
				LEFT JOIN Currency cu ON cu.id=o.currencyid
				LEFT JOIN orderquantity oq ON oq.orderlineid=ol.id and oq.active=1
				LEFT JOIN pickorder po ON po.ReferenceId=o.Id and po.active=1 and po.packed=0
				WHERE o.SeasonId=%d and o.done=0 and o.active=1 and o.ready=1 and po.id is null
					and c.id=o.companyid and ol.orderid=o.id and ol.active=1 and ol.done=0
					and p.id=o.paymenttermid and p.active=1 and p.prepay=1 and o.PrepayNotified=%d
				GROUP BY o.Id", $Id, $_notified) ;
		$res = dbQuery ($query) ;
		
		while ($row = dbFetch ($res)) {
			if ($row['CompanyName']=='Privat') continue ;
			$Lines[$row['Id']] = $row ;
		}
	
	    $query = sprintf ('SELECT * FROM Season WHERE Id=%d',$Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    return 0 ;

	case 'status' :
		if ($User['Extern']) 
			$companylimit = sprintf('(`Order`.CompanyId = %d) AND `Order`.Draft = 0 ', $User['CompanyId']);
	    $linequery = sprintf ('SELECT 
								OrderLine.ArticleId as GroupArticleId,
								OrderLine.Id, OrderLine.No as No, 
								 OrderLine.No as NoGroup,
								 1 as NoGroupLines,
								`orderline`.`Description`,
								`orderline`.`InvoiceFooter`,
								`orderline`.`OrderId`,
								`orderline`.`CaseId`,
								`orderline`.`ArticleId`,
								`orderline`.`ArticleColorId`,
								 `orderline`.`Quantity` as Quantity,
								`orderline`.`Surplus`,
								 `orderline`.`PriceSale`*`orderline`.`Quantity` as PriceSaleSubTotal,
								`orderline`.`PriceCost`,
								`orderline`.`Discount`,
								`orderline`.`DeliveryDate`,
								`orderline`.`ArticleCertificateId`,
								`orderline`.`PrefDeliveryDate`,
								`orderline`.`RequestComment`,
								`orderline`.`CustArtRef`,
								`orderline`.`ProductionId`,
								`orderline`.`Done`,
								`orderline`.`DoneUserId`,
								`orderline`.`DoneDate`,
								`orderline`.`ConfDeliveryDate`,
								`orderline`.`LineFooter`,
								`orderline`.`AltColorName`,
									Case.CustomerReference, Case.ArticleCertificateId, ArticleColor.Id as ArticleColorId,
									Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantDimension, VariantSortation,
									Color.Description AS ColorDescription, Color.Number AS ColorNumber,
									ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
									Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, (SELECT Number FROM Production WHERE Id = OrderLine.ProductionId) AS ProductionNo,
									CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Description AS CustomsPositionDesc, CustomsPosition.Name as CustomsPos, Country.Name as WorkCountryName, Country.Name as WorkCountryId
		    FROM OrderLine
		    LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
			LEFT JOIN CustomsPosition ON Article.CustomsPositionId=CustomsPosition.Id and CustomsPosition.active=1
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    LEFT JOIN Country ON Article.WorkCountryId=Country.Id
		    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 
			ORDER BY  No', $Id) ;

	case 'printorder' :
	case 'confirm' :
	case 'confirm-cmd' :
		$ProformaInvoice=0;
		$ProformaCustoms=0;
	    switch ($Navigation['Parameters']) {
	      case 'proformainvoice':
			$ProformaInvoice=1;
			break;
	      case 'proformacustoms':
			$ProformaCustoms=1;
			break;
		  default:
			break;
		}
	case 'process-cmd' :
		global $Record, $Line;
	    // Query Order
	    $query = sprintf ('SELECT %s FROM %s WHERE Order.Id=%d AND Order.Active=1', $queryFields, $queryTables, $Id) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Variables
	    $SizeCount = 0 ;

	    // Get OrderLines
	    $Line = array () ;
			$linequery = sprintf ('SELECT 
								if(Article.ArticleTypeId=6,OrderLine.Id+10000000,OrderLine.ArticleId) as GroupArticleId,
								OrderLine.Id, min(OrderLine.No) as No, 
								 group_concat(OrderLine.No) as NoGroup,
								 count(OrderLine.Id) as NoGroupLines,
								`orderline`.`Description`,
								`orderline`.`InvoiceFooter`,
								`orderline`.`OrderId`,
								`orderline`.`CaseId`,
								`orderline`.`ArticleId`,
								`orderline`.`ArticleColorId`,
								 sum(`orderline`.`Quantity`) as Quantity,
								`orderline`.`Surplus`,
								 sum(`orderline`.`PriceSale`*`orderline`.`Quantity`) as PriceSaleSubTotal,
								`orderline`.`PriceCost`,
								`orderline`.`Discount`,
								`orderline`.`DeliveryDate`,
								`orderline`.`ArticleCertificateId`,
								`orderline`.`PrefDeliveryDate`,
								`orderline`.`RequestComment`,
								`orderline`.`CustArtRef`,
								`orderline`.`ProductionId`,
								`orderline`.`Done`,
								`orderline`.`DoneUserId`,
								`orderline`.`DoneDate`,
								`orderline`.`ConfDeliveryDate`,
								`orderline`.`LineFooter`,
								`orderline`.`AltColorName`,
									Case.CustomerReference, Case.ArticleCertificateId, ArticleColor.Id as ArticleColorId,
									Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantDimension, VariantSortation,
									Color.Description AS ColorDescription, Color.Number AS ColorNumber,
									ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
									Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, (SELECT Number FROM Production WHERE Id = OrderLine.ProductionId) AS ProductionNo,
									CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Description AS CustomsPositionDesc, CustomsPosition.Name as CustomsPos, Country.Name as WorkCountryName, Country.Name as WorkCountryId
		    FROM OrderLine
		    LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
			LEFT JOIN CustomsPosition ON Article.CustomsPositionId=CustomsPosition.Id and CustomsPosition.active=1
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    LEFT JOIN Country ON Article.WorkCountryId=Country.Id
		    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 
			GROUP BY GroupArticleId, OrderLine.ArticleColorId
			ORDER BY  No', $Id) ;
//			ORDER BY OrderLine.DeliveryDate, No', $Id) ;
	    $result = dbQuery ($linequery) ;
	    while ($row = dbFetch ($result)) {
//			$Line[(int)$row['Id']] = $row ;
			$Line[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($result) ;

	    // Get size specific quantities for each Line with VariantSize/Dimension
	    foreach ($Line as $i => $l) {
//			if (!$l['VariantSize'] or !$l['VariantDimension']) continue ;

			$Line[$i]['Size'] = array () ;
			if ($l['VariantSize']) {
/*				$query = sprintf ("SELECT ArticleSize.*, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity, vc.VariantCode as VariantCode
									FROM ArticleSize
									LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1 AND OrderQuantity.ArticleSizeId=ArticleSize.Id
									LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
									WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $i,(int)$l['ArticleId'],(int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
*/
									$query = sprintf ("SELECT ArticleSize.*, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity, vc.VariantCode as VariantCode, OrderLine.PriceSale, OrderLine.No as OrderLineNo
														FROM ArticleSize
														INNER JOIN OrderQuantity ON OrderQuantity.Active=1 AND OrderQuantity.ArticleSizeId=ArticleSize.Id
														INNER JOIN OrderLine On orderline.orderid=%d and OrderLine.Id=OrderQuantity.OrderLineId and OrderLine.articlecolorid=%d and orderline.active=1
														LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
														WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name
									", $Id, (int)$l['ArticleColorId'],(int)$l['ArticleId'],(int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
			} else if ($l['VariantDimension']) {
				$query = sprintf ("SELECT OrderQuantity.Dimension as Name, OrderQuantity.Dimension as Id, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity
									FROM OrderQuantity
									WHERE OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1", $i) ;
			} else continue ;

			$result = dbQuery ($query) ;
			$n = 0 ;
			while ($row = dbFetch ($result)) {
				$Line[$i]['Size'][$row['Id']] = $row ;
				$n++ ;
			}
			dbQueryFree ($result) ;

			// Update max size count
			if ($n > $SizeCount) $SizeCount = $n ;
	    }

	    //get quantities from production orders
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Production'] = array () ;
	      $query = sprintf ("SELECT Quantity, ArticleSizeId as Id FROM ProductionQuantity
				WHERE ProductionId= %d AND ArticleColorId = %d", $l['ProductionId'], $l['ArticleColorId']) ;
	      $result = dbQuery ($query) ;
	      while ($row = dbFetch ($result)) {
	          $Line[$i]['Production'][(int)$row['Id']] = $row['Quantity'] ;
	      }
	      dbQueryFree ($result) ;

	    }

	    // Get Items Shipped for each Line
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Shipments']= array();
	      $quantity = 0 ;
	      if ($l['VariantSize']) {
			// Get Shipped Quantity by Size
//			$query = sprintf ('SELECT Item.ArticleSizeId AS Id, SUM(Item.Quantity) AS Quantity FROM Item INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Item.OrderLineId=%d', $i) ;
			$query = sprintf ('SELECT Item.ArticleSizeId AS Id, 
								SUM(Item.Quantity) AS Quantity 
								FROM orderline, Item 
								INNER JOIN Container ON Container.Id=Item.ContainerId 
								INNER JOIN Stock ON Stock.Id=Container.StockId 
								WHERE OrderLine.OrderId=%d and Item.OrderLineId=Orderline.Id', $Id) ;
			if ($l['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $l['ArticleColorId']) ;
			$query .= ' AND Item.Active=1 AND Stock.Type="shipment" GROUP BY Item.ArticleSizeId' ;
//die($query);
			$result = dbQuery ($query) ;
			while ($row = dbFetch ($result)) {
				if (!isset($l['Size'][(int)$row['Id']])) {
					logPrintf (logWARNING, '%s(%d) invalid ArticleSizeId (%d) for OrderLine (%d)', __FILE__, __LINE__, (int)$row['Id'], $i) ;
					continue ;
				}
				$Line[$i]['Size'][(int)$row['Id']]['QuantityShipped'] = $row['Quantity'] ;
				$quantity += $row['Quantity'] ;
				dbQueryFree ($result) ;
			}

			//get shipments
			foreach($Line[$i]['Size'] as $size =>$quant) {
				if ($l['VariantColor']) $color_clause = sprintf (' AND i.ArticleColorId=%d ', $l['ArticleColorId']) ;
				else  $color_clause = '' ;
				$query = sprintf('SELECT s.Id AS ShipmentId, s.Name AS ShipmentName,
					SUM(i.Quantity) AS ShipmentQuantity
					FROM orderline ol, Item i INNER JOIN Container c ON i.ContainerId = c.Id
					INNER JOIN Stock s ON s.Id = c.StockId
					WHERE s.Type = \'shipment\' AND i.Active=1
					and ol.orderid=%d AND i.OrderLineId = ol.id AND i.ArticleSizeId = %d %s
					GROUP BY s.Id, s.Name', $Id, $size, $color_clause);

				$res = dbQuery($query);
				while ($row = dbFetch($res)){
					$Line[$i]['Shipments'][$row['ShipmentId']][$size]=$row['ShipmentQuantity'];
				}
				dbQueryFree($res);
			}
	      } else {
		    // Get Shipped Quantity
		    $query = sprintf ('SELECT SUM(Item.Quantity) AS Quantity FROM Item INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Item.OrderLineId=%d', $i) ;
		    if ($l['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $l['ArticleColorId']) ;
		    $query .= ' AND Item.Active=1 AND Stock.Type="shipment"' ;
		    $result = dbQuery ($query) ;
		    $row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    $quantity = $row['Quantity'] ;
	      }
	      $Line[$i]['QuantityShipped'] = $quantity ;

	      $query = sprintf('SELECT s.Id AS ShipmentId, s.Name AS ShipmentName,
								SUM(i.Quantity) AS ShipmentQuantity
								FROM orderline ol, Item i INNER JOIN Container c ON i.ContainerId = c.Id
								INNER JOIN Stock s ON s.Id = c.StockId
								WHERE s.Type = \'shipment\' AND i.Active=1
								and ol.orderid=%d AND i.OrderLineId = ol.id
								GROUP BY s.Id, s.Name', $Id);

	      $res = dbQuery($query);
	      while ($row = dbFetch($res)){
			$Line[$i]['Shipments'][$row['ShipmentId']][0]=$row['ShipmentQuantity'];
	      }
	      dbQueryFree($res);
	    }


	    // Get Items Invoices for each Line
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Invoices'] = array();
	      $quantity = 0 ;
	      if ($l['VariantSize']) {
			if ($l['VariantColor']) $color_clause = sprintf (' AND InvoiceLine.ArticleColorId=%d ', $l['ArticleColorId']) ;
			else  $color_clause = '' ;
			// Get Invoiced Quantity by Size
			$query = sprintf ('SELECT InvoiceQuantity.ArticleSizeId AS Id, SUM(InvoiceQuantity.Quantity *IF(Invoice.Credit=1,-1,1)) AS Quantity 
							FROM InvoiceLine INNER JOIN OrderLine ON OrderLine.OrderId=%d and OrderLine.Active=1 INNER JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId INNER JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId=InvoiceLine.Id AND InvoiceQuantity.Active=1 
							WHERE InvoiceLine.OrderLineId=OrderLine.Id AND InvoiceLine.Active=1 %s
							GROUP BY InvoiceQuantity.ArticleSizeId', $Id, $color_clause) ;
							// AND Invoice.Ready=1 - also show draft.
		  $result = dbQuery ($query) ;
		  while ($row = dbFetch ($result)) {
		  if (!isset($l['Size'][(int)$row['Id']])) {
		    logPrintf (logWARNING, '%s(%d) invalid ArticleSizeId (%d) for OrderLine (%d)', __FILE__, __LINE__, (int)$row['Id'], $i) ;
		    continue ;
		  }
		  $Line[$i]['Size'][(int)$row['Id']]['QuantityInvoiced'] = $row['Quantity'] ;
		  $quantity += $row['Quantity'] ;
		}
		dbQueryFree ($result) ;

		//get Invoices
		foreach($Line[$i]['Size'] as $size =>$quant) {
		  $query = sprintf('SELECT c.InvoiceId AS InvoiceId,
						SUM(i.Quantity)*IF(Invoice.Credit=1,-1,1) AS InvoiceQuantity
						FROM InvoiceQuantity i INNER JOIN InvoiceLine c ON c.Id = i.InvoiceLineId INNER JOIN OrderLine ON OrderLine.OrderId=%d and OrderLine.Active=1 
						INNER JOIN Invoice ON c.InvoiceId = Invoice.Id
						WHERE i.Active=1 
						AND c.OrderLineId = OrderLine.Id AND i.ArticleSizeId = %d
						GROUP BY c.Id', $Id, $size);
						// AND Invoice.Ready=1 - also show draft.

		  $res = dbQuery($query);
		  while ($row = dbFetch($res)){
		    $Line[$i]['Invoices'][$row['InvoiceId']][$size]=$row['InvoiceQuantity'];
		  }
		  dbQueryFree($res);

		}
	      } else {
		// Get Invoiced Quantity
		$query = sprintf ('SELECT SUM(InvoiceLine.Quantity)*IF(Invoice.Credit=1,-1,1) AS Quantity FROM InvoiceLine INNER JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId WHERE InvoiceLine.OrderLineId=%d AND InvoiceLine.Active=1 ', $i) ; // also show draft
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$quantity = $row['Quantity'] ;

		$query = sprintf('SELECT c.InvoiceId AS InvoiceId,
							SUM(c.Quantity)*IF(Invoice.Credit =1,-1,1) AS InvoiceQuantity
							FROM InvoiceLine c INNER JOIN Invoice ON c.InvoiceId = Invoice.Id
							WHERE c.Active=1 AND Invoice.Active=1 AND c.OrderLineId = %d
							GROUP BY c.InvoiceId', $i);
// AND Invoice.Ready=1  - also show draft

		$res = dbQuery($query);
		while ($row = dbFetch($res)){
		  $Line[$i]['Invoices'][$row['InvoiceId']][0]=$row['InvoiceQuantity'];
		}
		dbQueryFree($res);
	      }

	      $Line[$i]['QuantityInvoiced'] = $quantity ;
	    }
	    return 0 ;

	case 'editdelay' :
	case 'editdelay-cmd' :
	    // Query Delay
	    $query = sprintf ('SELECT OrderDelay.*
	    FROM (OrderDelay, OrderLine)
	    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId
	    LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
	    LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
	    LEFT JOIN Company ON Company.Id=Order.CompanyId
	    WHERE OrderDelay.Id=%d AND OrderLine.Id=OrderDelay.OrderLineId AND OrderLine.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;

	case 'adddelay' :
	case 'adddelay-cmd' :
	case 'lineview' :
	case 'lineedit' :
	case 'lineedit-cmd' :
	case 'linedelete-cmd' :
	    global $Color, $Size, $Dim, $Quantity ;		// For direct view only

	    // Query Order
	    $query = sprintf ('SELECT OrderLine.*,
								Order.Id AS OrderId, Order.Ready AS OrderReady, Order.Done AS OrderDone, Order.ToCompanyId as ToCompanyId,
								Company.Id AS CompanyId, Company.Number AS CompanyNumber, Company.Name AS CompanyName,
								Currency.Symbol AS CurrencySymbol,
								Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize,Article.VariantDimension, Article.HasStyle,
								Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, Production.Number AS ProductionNo
						FROM OrderLine
						LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
						LEFT JOIN Unit ON Unit.Id=Article.UnitId
						LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
						LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
						LEFT JOIN Company ON Company.Id=Order.CompanyId
						LEFT JOIN Production ON OrderLine.ProductionId=Production.Id
						WHERE OrderLine.Id=%d AND OrderLine.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    if ($Record['VariantSize']) {
			// Get sizes
            $query = sprintf ("SELECT ArticleSize.*, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity, OrderQuantity.QuantityReceived, OrderQuantity.QuantityUnpack, OrderQuantity.BoxNumber FROM ArticleSize LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1 AND OrderQuantity.ArticleSizeId=ArticleSize.Id WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
			$result = dbQuery ($query) ;
			$Size = array() ;
			while ($row = dbFetch ($result)) {
				$Size[] = $row ;
			}
			dbQueryFree ($result) ;
			if (count($Size) == 0) return 'no Sizes specified for Article' ;
	    }

	    if ($Record['VariantDimension']) {
			// Get sizes
			$query = sprintf ("SELECT OrderQuantity.Dimension as Name, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity FROM OrderQuantity WHERE OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1 ORDER BY OrderQuantity.Dimension", $Id) ;
			$result = dbQuery ($query) ;
			$Size = array() ;
			while ($row = dbFetch ($result)) {
				$Size[] = $row ;
			}
			dbQueryFree ($result) ;
//			if (count($Dim) == 0) return 'no Sizes specified for Article' ;
	    }

	    if ($Record['VariantColor']) {
			// Get Color information
			$query = sprintf ('SELECT ArticleColor.Id,
			Color.Description AS ColorDescription, Color.Number AS ColorNumber,
			ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue
			FROM ArticleColor
			LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
			LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
			WHERE ArticleColor.Id=%d', (int)$Record['ArticleColorId']) ;
			$res = dbQuery ($query) ;
			$Color = dbFetch ($res) ;
			dbQueryFree ($res) ;
	    }

	    return 0 ;

	case 'linelist' :
	    switch ($Navigation['Parameters']) {
		case 'case' :
		    // Get Case
		    $query = sprintf ("SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1", $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Clause
		    $queryClause = sprintf (' OrderLine.CaseId=%d AND OrderLine.Active=1 AND Order.Id=OrderLine.OrderId', $Id) ;
		    break ;

		default :
		    return sprintf ('%s(%d) invalid parameter "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
	    }
	    // Query OrderLines
	    $query = 'SELECT OrderLine.*,
	    Article.Number AS ArticleNumber, Article.VariantColor,
	    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals' ;
	    $query .= ', ' . $StateField . ' AS State' ;
	    $query .= ' FROM (OrderLine, `Order`)
	    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId' ;
	    $query .= ' WHERE ' . $queryClause . ' ORDER BY OrderLine.OrderId, OrderLine.No' ;
	    $Result = dbQuery ($query) ;
	    break ;

	case 'assign-cmd' :
	case 'assign' :
	    // Query Order
		$query = sprintf ('SELECT %s FROM %s WHERE Order.Id=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Overload the Navigation Parameter with the one supplied in the URL
//	    $Navigation['Parameters'] = $_GET['param'] ;

		 $queryClause = sprintf (' Order.Id=%d AND OrderLine.Active=1 AND Order.Id=OrderLine.OrderId', $Id) ;

	    // Query OrderLines
	    $query = 'SELECT OrderLine.*,
	    Article.Number AS ArticleNumber, Article.VariantColor,
	    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals' ;
	    $query .= ', ' . $StateField . ' AS State' ;
	    $query .= ' FROM (OrderLine, `Order`)
	    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId' ;
	    $query .= ' WHERE ' . $queryClause . ' ORDER BY OrderLine.OrderId, OrderLine.No' ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	case 'add2cart-cmd' :
	case 'add2cart' :
		$query = sprintf ("SELECT cm.articleid as ArticleId, s.name as SeasonName, s.Id as SeasonId, 
								  a.description as ArticleName, a.number as ArticleNumber, a.articletypeid as ArticleTypeId, a.description as ArticleDescription,
								  co.number as ColorNumber, cm.articleColorid as ArticleColorId   
							FROM (collectionmember cm, article a, collection c, season s, articlecolor ac, color co) 
							WHERE cm.Id=%d and a.id=cm.articleid and c.id=cm.collectionid and s.id=c.seasonid and ac.id=cm.articlecolorid and co.id=ac.colorid", $Id) ;
	    $res = dbQuery ($query) ;

	    $Record = dbFetch ($res) ;
  	    dbQueryFree ($res) ;

	    break ;

    }

    return 0 ;
?>
