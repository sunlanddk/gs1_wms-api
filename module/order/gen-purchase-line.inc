<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }
	    // Query OrderLines
    $query = sprintf ('SELECT OrderLine.*, 
	    Article.Number AS ArticleNumber, Article.VariantColor,
	    if (ArticlePrice.SupplierId is not null, ArticlePrice.SupplierId, Article.SupplierCompanyId) as SupplierId,
	    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
	    FROM OrderLine 
	    LEFT JOIN `order`o ON o.id=OrderLine.orderid
	    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		LEFT JOIN ArticlePrice ON ArticlePrice.ArticleId=Article.Id and ArticlePrice.OwnerId=o.ToCompanyId and ArticlePrice.Active=1
	    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId
	    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 
	    ORDER BY ArticlePrice.SupplierId, OrderLine.No', $Id) ;
    $res = dbQuery ($query) ;


    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Sales Order', $Record['Id']) ;
    flag ($Record, 'Ready') ;
    itemSpace () ;
    itemHeader () ;

	$SupplierId = 0 ;
	while ($row = dbFetch ($res)) {
		if ($row['SupplierId'] != $SupplierId) {
			if ($SupplierId>0) itemSpace () ;
			$SupplierId = $row['SupplierId'] ;
			itemFieldRaw ('From Supplier', formDBSelect (sprintf('SupplierCompanyId[%d]',$row['SupplierId']), $row['SupplierId'], 
				"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company 
				 WHERE (TypeSupplier=1 or Internal=1) AND Active=1 ORDER BY Name", 'width:250px;')) ;  
		}
		ItemFieldRaw('Line: ', $row['No'] . ',   Article:  ' . $row['ArticleNumber'] . ' ' . $row['Description'] . ', Color: ' . $row['ColorDescription']) ;
	}
    itemSpace () ;
//    itemFieldRaw ('Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:100px;')) ;
    itemSpace () ;
    itemSpace () ;
    itemEnd () ;

    formEnd () ;

    return 0 ;
?>
