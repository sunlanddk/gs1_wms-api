<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
 	require_once 'lib/csvftp.inc' ;
   require_once 'module/container/include.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

	// Proces each line.    
	foreach ($Lines as $j => $line) {
		//return ' Line: ' . (float)$line['ActualPriceConsumption'] . ' R ' . $line['ActualPriceCurrencyRate'] . ' L '  . (float)$line['LogisticCostConsumption'] . ' R '  . $line['LogisticCurrencyRate'] ; // Calculate stockvalue
		unset ($_orderlineqtyadj) ;
		
		// adjust orderquantitities
		foreach ($_POST['QuantityReceived'][$j] as $artsizeid => $qty) {
			if ($qty=='' or is_null($qty) or $qty==0) continue ; // no changes in quantity
			
			// Adjust qty in SO
			$query = sprintf ('select * from orderquantity rq where rq.orderlineid in (%s) and rq.articlesizeid=%d and rq.Active=1 and rq.quantity>0', $line['OrderLineId'], $artsizeid) ;
			$result = dbQuery ($query) ;
			$reqcount = dbNumRows ($result) ;
			if ($reqcount>1) return 'invalid number of order quantities - query ' . $query ; // only one item per articlesize is handled.
			$order_rq = dbFetch ($result) ;
			dbQueryFree($result) ;
	
			$_oq = array (
				'QuantityUnpack' 	=> $qty 
			) ;
//			if ($_oq['QuantityReceived']==0) $_oq['BoxNumber']=0 ; // s<Commented out to ave boxnumber for debugging for now.
			tableWrite('orderquantity', $_oq, $order_rq['Id']) ;
		}
	}
		
	// Create unpack request from SO and send data to Alpi as incomming (unpack to stock).
	$_qtyvalue = 'oq.quantityunpack' ;
	$_whereclause = 'and oq.quantityunpack>0' ;
	$query = sprintf ("Select 
								oq.id as LinjeID,
								ol.OrderId as ordrenummer,
								Variantcode.id as Varenummer,
								format(%s,0) as Antal
							FROM
								(orderline ol, orderquantity oq)
							LEFT JOIN VariantCode ON VariantCode.articleid=ol.articleid and VariantCode.articleColorid=ol.articleColorid and VariantCode.articlesizeid=oq.articlesizeid AND VariantCode.Active=1
							WHERE
								ol.OrderId=%d AND ol.Active=1 AND oq.orderlineid=ol.id %s and oq.active=1
					  ", $_qtyvalue, $Id, $_whereclause) ;
	// Target filename
	$_datetime = date('Ymd_His'); 
	$ftp_file = 'PURCHASE_' . $_datetime . '_SO' . $Id .  'Unpack.txt';
	if ($_res = CreateCSV($query, $ftp_file)) {
		echo 'Information transfer failed, Close and try to retransmit again later.';
	} else {
		$_record = array (
			'LogText' => 'Unpack request send at ' . ' ' . dbDateEncode(time()) 
		);
		tableWrite('order', $_record, $Id) ;
/* Only reset data on confirmation back from stock
		$result = dbQuery ($query) ;
		while ($row = dbFetch ($result)) {                              
			$_record = array (
				'QuantityUnpack' => 0,
			);
			tableWrite('orderquantity', $_record, $row['LinjeID']) ;
		}
		dbQueryFree ($result) ;
*/		
		return 'Information transfer to Alpi succeeded and prepacked data adjusted. You can now close the window.<br><br>' .  $csv_data_list ;
	}
    return navigationCommandMark ('orderview', $Id) ;
?>
