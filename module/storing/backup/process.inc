<?php
	function process_file($_handle) {
	    Global $PO_Data ;	

		$_boxno =0 ;
		$debug = 0; 
		$row = 1 ; 
		while (($data = fgetcsv($_handle, 1000, ";")) !== FALSE) {
			$_cols = (int)count($data) ;
			switch ($_cols) {
			  case 0: 	
			  case 1: 	
				continue ;
			  case 8:
			  case 9:
				if ($debug) {
					$error_txt .= $_cols . ' colomns in line ' . $row  . "<br />\n";
					$error_txt .= 'Variant ' . $data[VariantCodeId] . "<br />\n";
					$error_txt .= 'Qty     ' . $data[VariantQuantity] . "<br />\n";
				}
				// dont process if nothing to receive
				if ((float)$data[VariantQuantity]==0) continue ;
				
				// Chaeck varinatcodeid
				$query = sprintf ("SELECT *
									FROM (variantcode) 
									WHERE Id=%d and active=1", (int)$data[VariantCodeId]) ;
									$res = dbQuery ($query) ;
				$variant = dbFetch ($res) ;
				dbQueryFree ($res) ;
				if ((int)$variant['Id']==0) {
					$error_txt .= "Line " . $row . ": Variant not found in system - " . $query .  "<br />\n";
				}

				// Find corresponding requistionlinequantity
				$query = sprintf ("SELECT rq.id as reqqtylineid, rl.articlecolorid
				FROM (Requisitionlinequantity rq, Requisitionline rl) 
									WHERE rq.ArticleSizeId=%d and rq.quantity>0 and rq.Active=1 and rl.id=rq.requesitionlineid and rl.articlecolorid=%d and rl.active=1", $variant['ArticleSizeId'], $variant['ArticleColorId']) ;
				$res = dbQuery ($query) ;
				$reqqty = dbFetch ($res) ;
				dbQueryFree ($res) ;
				
				if ((int)$reqqty['reqqtylineid']==0) {
					$error_txt .= "Line " . $row . ": Variant not found in purchase order "  . $query  .  "<br />\n";
				}

				$PO_Data[$variant['ArticleColorId']][$variant['ArticleSizeId']] += (float)$data[VariantQuantity] + (float)$data[VariantSurplusQuantity] ;

				break ;
			  case 13:
				if ($debug) {
					$error_txt .= $_cols . ' colomns in line ' . $row  . "<br />\n";
					$error_txt .= $data[OrderNumber] . "<br />\n";
					$error_txt .= $data[OrderQuantityId] . "<br />\n";
					$error_txt .= $data[PackedQuantity] . "<br />\n";
					$error_txt .= $data[BoxNumber] . "<br />\n";
				}				
				$query = sprintf ("SELECT oq.articlesizeid as articlesizeid, ol.articlecolorid
									FROM (orderquantity oq, orderline ol, `order` o) 
									WHERE oq.Id=%d and oq.quantity>0 and oq.Active=1 and ol.id=oq.orderlineid and ol.active=1 and o.id=ol.orderid and o.active=1 and o.ready=1", (int)$data[OrderQuantityId]) ;
									$res = dbQuery ($query) ;
				$orderqty = dbFetch ($res) ;
				dbQueryFree ($res) ;
				if ((int)$orderqty['articlesizeid']==0) {
					$error_txt .= "Line " . $row . ": Variant not found in sales order " . $query . "<br />\n";
				}
				$query = sprintf ("SELECT rq.id as reqqtylineid, rl.articlecolorid
									FROM (Requisitionlinequantity rq, Requisitionline rl) 
									WHERE rq.ArticleSizeId=%d and rq.quantity>0 and rq.Active=1 and rl.id=rq.requesitionlineid and rl.articlecolorid=%d and rl.active=1", $orderqty['articlesizeid'], $orderqty['articlecolorid']) ;
				$res = dbQuery ($query) ;
				$reqqty = dbFetch ($res) ;
				dbQueryFree ($res) ;
				
				if ((int)$reqqty['reqqtylineid']==0) {
					$error_txt .= "Line " . $row . ": Variant not found in purchase order "  . $query . "<br />\n";
				}
				
				if ((int)$data[BoxNumber]==0 And $_boxno==0)
					$error_txt .= "Line " . $row . ": Box Number required as minimum for first line<br />\n";
					
				if ((int)$data[BoxNumber] > 0)
					$_boxno = (int)$data[BoxNumber] ;
					
				$PO_Data[$orderqty['articlecolorid']][$orderqty['articlesizeid']] += (float)$data[PackedQuantity];

				printf(formText('PackQty['.$data[OrderQuantityId].']', $data[PackedQuantity] , 5, 'text-align:right;', ' ' . 'hidden')) ;
				printf(formText('PackBox['.$data[OrderQuantityId].']', $_boxno , 5, 'text-align:right;', ' ' . 'hidden')) ;
				$OrderData[$data[OrderQuantityId]]['OrderNumber'] 		= $data[OrderNumber] ;
				$OrderData[$data[OrderQuantityId]]['OrderQuantityId'] 	= $data[OrderQuantityId] ;
				$OrderData[$data[OrderQuantityId]]['PackedQuantity'] 	= $data[PackedQuantity] ;
				$OrderData[$data[OrderQuantityId]]['BoxNumber'] 		= $_boxno ;

				break ;
			  default: 	
				return $_cols . ' colomns in line ' . $row . ' - only 8 or 13 coloms is allowed - Maybe check if ; is used as seperator.' ;
			}
			$row++;
		}
		return $error_txt ;
	}
?>