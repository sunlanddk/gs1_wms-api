<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

	$reqcontainer_id = ParameterGet('ReqContainerId');
	$reccontainer_id = ParameterGet('RecContainerId');
	$_receivecontainertypeid = 37 ; // ParameterGet('receivecontainertypeid');
	$_containerid = $reccontainer_id ;

    // Get Stock to move from
    if ($_POST['ToStockId'] == 0) return 'please select Stock to receive items' ;
    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $_POST['ToStockId']) ;
    $result = dbQuery ($query) ;
    $ToStock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ToStock['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $_POST['ToStockId']) ;
    if ($ToStock['Type'] != 'fixed') return sprintf ('%s(%d) invalid Stock, type "%s"', __FILE__, __LINE__, $_POST['ToStockId']) ;

	$_containerid = $reccontainer_id ;
	//Create container
/*
	$Container = array (
	    'StockId' => (int)$ToStock['Id'],
//	    'Position' => $fields['Position']['value'],
//	    'TaraWeight' => $ContainerType['TaraWeight'],
//	    'GrossWeight' => $fields['GrossWeight']['value'],
//	    'Volume' => $ContainerType['Volume'],
	    'ContainerTypeId' => (int)$_receivecontainertypeid
	) ;
	$_containerid = tableWrite ('Container', $Container) ;
*/
	$transaction = array (
		'Type'				=> 'Receive',
		'ObjectId'			=> $Id ,
		'FromStockId'		=> tableGetField('Container', 'StockId', $reqcontainer_id),
		'ToStockId'			=> tableGetField('Container', 'StockId', $reccontainer_id),
		'Value'				=> 0 
	) ;
	// Proces each line.    
	foreach ($Lines as $j => $line) {
//return ' Line: ' . (float)$line['ActualPriceConsumption'] . ' R ' . $line['ActualPriceCurrencyRate'] . ' L '  . (float)$line['LogisticCostConsumption'] . ' R '  . $line['LogisticCurrencyRate'] ; // Calculate stockvalue
		foreach ($_POST['Quantity'][$j] as $artsizeid => $qty) {
//if ($line['RequisitionLineId']==499) return 'test ' . $qty . ' ' . $artsizeid;
//			if ((int)$qty>0) {
				// Add received qty to PO
				$query = sprintf ('select * from requisitionlinequantity rq where rq.requesitionlineid in (%s) and rq.articlesizeid=%d and rq.Active=1 and rq.quantity>0', $line['RequisitionLineId'], $artsizeid) ;
				$result = dbQuery ($query) ;
				$reqcount = dbNumRows ($result) ;
				if ($reqcount>1) return 'invalid number of req quantities - query ' . $query ; // only one item per articlesize is handled.
				$purchase_rq = dbFetch ($result) ;
				dbQueryFree($result) ;
				$rlq['QuantityReceived']  = $qty ; // Multiple receiving: $purchase_rq['QuantityReceived'] + $qty ;
				$rlq['QuantityPrepacked'] = $_POST['QuantityPrepacked'][$j][$artsizeid] ;
				tableWrite('requisitionlinequantity', $rlq, $purchase_rq['Id']) ;

				// Process and receive items 
				if ($_POST['Done'][$j]=='on') {
					// Get purchase item
					$query = sprintf ('select * from item i where i.requisitionlineid in (%s) and i.articlesizeid=%d and i.Active=1 and i.ContainerId=%d', $line['RequisitionLineId'], $artsizeid, $reqcontainer_id) ;
					$result = dbQuery ($query) ;
					$itemcount = dbNumRows ($result) ;
					if ($itemcount>1) return 'invalid item count - query ' . $query ; // only one item per articlesize is handled.
					$purchase_item = dbFetch ($result) ;
					dbQueryFree($result) ;

					// Get requistionline
					$query = sprintf ('select * from requisitionline where id=%d', $purchase_item['RequisitionLineId']) ;
					$result = dbQuery ($query) ;
					$req_line = dbFetch ($result) ;
					dbQueryFree($result) ;
					
					// Create received item
					unset($received_item) ; 
					$received_item = $purchase_item ;
					unset ($received_item['Id']) ;
					$received_item['Quantity'] 				= $qty ;
					$received_item['Price'] 				= (float)($req_line['ActualPriceConsumption']*$line['ActualPriceCurrencyRate'])/100+(float)($req_line['LogisticCostConsumption']*$line['LogisticCurrencyRate'])/100 ; // Calculate stockvalue
					$received_item['PreviousContainerId'] 	= $received_item['ContainerId'] ;
					$received_item['ContainerId'] 			= $_containerid ;
					$received_item['ParentId']              =  $purchase_item['Id'] ;

					$_itemid = tableWrite('item', $received_item) ;
					
					// Create received item transaction 
					$transaction_item = array (
						'ItemId' 			=> $_itemid ,
						'RequisitionLineId'	=> $purchase_item['RequisitionLineId'] ,
						'ArticleSiseId'		=> $purchase_item['ArticleSiseId'] ,
						'Quantity'			=> $qty ,
						'Value'				=> $received_item['Price'] ,
						'FromStockId'		=> tableGetField('Container', 'StockId', $reqcontainer_id),
						'ToStockId'			=> tableGetField('Container', 'StockId', $reccontainer_id)
					) ;
	//				tableWrite('transactionitems', $transaction_item) ;
					$transaction['value'] += $qty * $received_item['Price'] ;				
					
					// Update purchase item
					$upd_purchase_item['Quantity'] = ($purchase_item['Quantity'] > $qty) ? ($purchase_item['Quantity'] - $qty) : 0 ;
					tableWrite('Item', $upd_purchase_item, $purchase_item['Id'] ) ;
				}
//			}
		}
		// Cloase orderline and purchase items?
		if ($_POST['Done'][$j]=='on') { 	// Always close line if no quantity left.
			$query = sprintf('Update Item set Active=0 where RequisitionLineId in (%s) and Active=1 and ContainerId=%d', $line['RequisitionLineId'], $reqcontainer_id) ;
			dbQuery($query) ; // $row['Active'] = 0 ; 
			
			// Set Orderline done
			$OrderLine = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time())
			) ;
			$reqlines = explode(',' , $line['RequisitionLineId']) ;
			foreach($reqlines as $idx => $reqlineid) {
				tableWrite('requisitionline',$OrderLine,$reqlineid) ;
			}
		}	
	}

//	tableWrite('transaction', $transaction) ;

	// prepacked quantities
	foreach ($_POST['PackQty'] as $orderquantityid => $_packqty) {
		$OrderQuantity = array (	
			'QuantityReceived'		=>  (float)$_packqty,
			'BoxNumber'				=>  $_POST['PackBox'][$orderquantityid] 
		) ;
//		return 'test ' . $_POST['PackData[3773]["PackedQuantity"]'] . ' - ' . $_row["BoxQuantity"] . ' - ' . $orderquantityid;
		tableWrite('orderquantity',$OrderQuantity,$orderquantityid) ;
	}


    return navigationCommandMark ('purchaseview', $Id) ;
?>
