<?php

	$PickLayout = array (
		"Picking" => array (
				"A" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"B" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"C" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"D" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"E" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"F" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"G" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"H" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"I" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"J" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"K" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"L" => array ("NoStreetNo" => 104,  "OddNoFloors" => 4, "EvenNoFloors" => 4)
		),
		"Some other Picking" => array (
				"A" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"B" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"C" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"D" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"E" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"F" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"G" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"H" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"I" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"J" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"K" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"L" => array ("NoStreetNo" => 22,  "OddNoFloors" => 4, "EvenNoFloors" => 4)
		),
		"Some OLD" => array (
				"A" => array ("NoStreetNo" => 90,  "OddNoFloors" => 5, "EvenNoFloors" => 5),
				"B" => array ("NoStreetNo" => 90,  "OddNoFloors" => 5, "EvenNoFloors" => 5),
				"C" => array ("NoStreetNo" => 90,  "OddNoFloors" => 5, "EvenNoFloors" => 4),
				"D" => array ("NoStreetNo" => 102, "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"E" => array ("NoStreetNo" => 102, "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"F" => array ("NoStreetNo" => 102, "OddNoFloors" => 4, "EvenNoFloors" => 4),
				"G" => array ("NoStreetNo" => 51,  "OddNoFloors" => 4, "EvenNoFloors" => 4)
		)
    ); 

?>
