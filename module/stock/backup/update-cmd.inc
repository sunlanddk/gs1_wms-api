<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc' ;

/*
	Adjustment is calculated as diffence between stock Qty and Actual Qty in Excel sheet.
	
	Hence if real Stock Qty is different from Excel only difference is updated.

	This means Stock only has to be blocked from generating excel untill counted 
	(dosnt have to wait untill system is updated).

	Updating stock balance is:
		Adding qty by creating a new item as a clone of an existing and marking it with "StatusId" in batchnumber
		Consume loss qty by splitting item and mark with "StatusId" in batchnumber

	Qty that is not represented on the stock before status (not in the generated Excel Sheet) is created manuel
	with article->new item with batchnumber="StatusId"
	
	Updating position is:
		Container is named the description entered in "New position" colomn.
		
		If "delete" is entered container is set active=0 and pick pos is released (containerid in variantcode table is set to 0)
		
		If more lines is tagged with same position name a running number is appended to description according to display order.
*/
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	set_time_limit (120) ;

	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);

		//	Excel read help!
		//	 $data->sheets[0]['numRows'] - count rows
		//	 $data->sheets[0]['numCols'] - count columns
		//	 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column
		//	 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
		//	    
		//	$data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
		//	if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
		//	$data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 

		$Error_txt=0 ;
		for ($k = 0; $k <= 0; $k++) {
			for ($i = 6; $i <= $data->sheets[$k]['numRows']; $i++) {
				// Initializing data
				$ConsumedQty = 0;
				
				$ArticleNumber = $data->sheets[0]['cells'][$i][1] ;
				$ArticleColorNumber = $data->sheets[0]['cells'][$i][3] ;
				$ArticleSize = $data->sheets[0]['cells'][$i][5] ;
				$QtyDiff=round((float)$data->sheets[0]['cells'][$i][7] - (float)$data->sheets[0]['cells'][$i][9], 3) ;		// C7: Stock Qty
//return "1: " . (float)$data->sheets[0]['cells'][$i][7] . " 2: ". (float)$data->sheets[0]['cells'][$i][9] . " Q:" . (float)$QtyDiff ;
																												// C9: Counted Qty.
				$Position = $data->sheets[0]['cells'][$i][10] ;								
				$ContainerId = (int)$data->sheets[0]['cells'][$i][11] ;								

				// Any change? Any Container number?
				if ($QtyDiff != 0) {
				if ($ContainerId == 0) continue ;

				// Get items in container in question.
				if ($ArticleColorNumber == '') {
					$QueryTables = '' ;
					$QueryWhere = '' ;
				} else {
					$QueryTables = ", articlecolor ac, color co" ;
					$QueryWhere = sprintf (" and i.articlecolorid=ac.id and ac.active=1 and ac.colorid=co.id and co.number='%s'", $ArticleColorNumber) ;
				}
				if ($ArticleSize== '') {
					$x = 1 ;
				} else {
					$VariantDimension=tableGetFieldWhere ('Article','VariantDimension', sprintf('Number=%s', $ArticleNumber)) ;
					if ($VariantDimension) {
						$QueryWhere = $QueryWhere . sprintf (" and i.dimension='%s'", $ArticleSize) ;
					} else {
						$QueryTables = $QueryTables . ", articlesize az" ;
						$QueryWhere = $QueryWhere . sprintf (" and i.articlesizeid=az.id and az.active=1 and az.name='%s'", $ArticleSize) ;
					}
				}
				$query = sprintf ("SELECT i.* FROM item i, container c, article a %s
									WHERE i.containerid=c.id and i.active=1 and c.id=%d
									and i.articleid=a.id and a.active=1 and a.number='%s' %s",
								  $QueryTables, $ContainerId, $ArticleNumber, $QueryWhere) ;			 
	
/*				$query = sprintf ("SELECT i.* FROM item i, container c, article a, articlecolor ac, color co, articlesize az
									WHERE i.containerid=c.id and i.active=1 and c.id=%d
									and i.articleid=a.id and a.active=1 and a.number='%s'
									and i.articlecolorid=ac.id and ac.active=1 and ac.colorid=co.id and co.number='%s'
									and i.articlesizeid=az.id and az.active=1 and az.name='%s'", 
								  $ContainerId, $ArticleNumber, $ArticleColorNumber, $ArticleSize) ;			 
*/
				$res = dbQuery ($query) ;
				// Losses: Consume Qty!
				if ($QtyDiff > 0 ) {
					While ($Item = dbFetch ($res)) {
						if ($Item['Id'] > 0) {
							if (($Item['Quantity'] > $QtyDiff)) {		// More than missing Qty in this item
								// Split Item and create new item with complete qty.

								// Update Item still on stock.
								$Item['Quantity'] -= $QtyDiff ;
								tableWrite ('Item', $Item, $Item['Id']) ;
	
								// Create new consumed item.
								$Item['ParentId'] = $Item['Id'] ;
								$Item['Quantity'] = $QtyDiff ;
								$Item['BatchNumber'] = $_POST['StatusId'] ;
								$Item['ContainerId'] = 0 ;
								unset ($Item['Id']) ;
//return 'her 1' ;
								tableWrite ('Item', $Item) ;
								$QtyDiff = 0 ;
								break ;
							} else {										// Not enough qty in one item!
								$QtyDiff -= $Item['Quantity'] ;

								// Consume Item.
								$Item['ContainerId'] = 0 ;
								$Item['BatchNumber'] = $_POST['StatusId'] ;
//return 'her 2' ;
								tableWrite ('Item', $Item, $Item['Id']) ;
								
								if ($QtyDiff == 0) break;
							}
						} else
							return 'Invalid loss: No item at position:' . $Position ;
					}
					dbQueryFree ($res) ;

					// Check if enough items (qty) for loss
					if ($QtyDiff>0)
						$Error_txt=$Error_txt . sprintf('<br>Line %d: %f to few items on stock at position %s (cont %d) for losses', $i, $QtyDiff, $Position, $ContainerId) ;
			
				// Surplus: 
				} else {
					$QtyDiff = $QtyDiff * (-1) ;
					$FoundOneToClone=0;
					// Create Item with surplus qty
					While ($Item = dbFetch ($res)) {
						// Create new cloned Item.
						$FoundOneToClone=1;
						$Item['ParentId'] = $Item['Id'] ;
						$Item['BatchNumber'] = $_POST['StatusId'] ;
						$Item['Quantity'] = $QtyDiff ;
						unset ($Item['Id']) ;
//return 'her 3' ;

						tableWrite ('Item', $Item) ;
						break ;
					}
					dbQueryFree ($res) ;
					
					// Create Item from scratch if no item in advance!
					if ($FoundOneToClone==0) {
						$query = sprintf ("SELECT * FROM variantcode WHERE PickContainerId=%d", $ContainerId) ;
						$res = dbQuery ($query) ;
						if (dbNumRows($res) > 0) {
							$Variant = dbFetch ($res) ;
						    $Item = array (
								'ArticleId' => (int)$Variant['ArticleId'],
								'ArticleColorId' => (int)$Variant['ArticleColorId'],
								'ArticleSizeId' => (int)$Variant['ArticleSizeId'],
								'ContainerId' => (int)$ContainerId,
								'BatchNumber' => $_POST['StatusId'] ,
								'Sortation' => 1,
	// 'ArticleCertificateId',
	// 'Dimension',
								'Quantity' => $QtyDiff 
							) ;
							tableWrite ('Item', $Item) ;
						} else {
							$Error_txt=$Error_txt . sprintf('<br>Not enough information in line %d to make surplus qty', $i) ;						
						}
						dbQueryFree ($res) ;					
					}
				}
				}
				// Process position info.
				$NewPosition = $data->sheets[0]['cells'][$i][12] ;
//return "NewPos " . $NewPosition ;			
				if (($NewPosition == 'delete') or ($NewPosition == 'Delete')) {
					// delete container
					tableDelete ('Container', $ContainerId) ;
					
					// Free Picking position
				    $query = sprintf ("UPDATE `VariantCode` SET `PickContainerId`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE PickContainerId=%d", dbDateEncode(time()), $User["Id"], $ContainerId) ;
				    dbQuery ($query) ;

				} else if ($NewPosition != '') {
					if ($Positions[substr($NewPosition,0,5)]['NoRooms'] < 1) {
						$Positions[substr($NewPosition,0,5)]['ContainerId'] =  $ContainerId ;
						$Positions[substr($NewPosition,0,5)]['NoRooms'] = 1 ;
					} else {
						if ($Positions[substr($NewPosition,0,5)]['NoRooms'] == 1) {
							$Container['Position'] = substr($NewPosition,0,5) . '1' ;
							tableWrite ('Container', $Container, $Positions[substr($NewPosition,0,5)]['ContainerId']) ;
						}
						$Positions[substr($NewPosition,0,5)]['ContainerId'] =  0 ;
						$Positions[substr($NewPosition,0,5)]['NoRooms'] += 1 ;
						$NewPosition = substr($NewPosition,0,5) . $Positions[substr($NewPosition,0,5)]['NoRooms'] ;
					}

/*
					$query = "SELECT max(Position) as MaxPos FROM container WHERE Position LIKE " . "'" . substr($NewPosition,0,5) . "%'" ;

					$res = dbQuery ($query) ;
					$row = dbFetch ($res) ;
					
					if (dbNumRows($res) > 0) {
						$NewPosition = substr($NewPosition,0,5) . ((int)substr($row['MaxPos'],5,1)+1) ;
					}														
*/					
					$Container['Position'] = $NewPosition ;
					tableWrite ('Container', $Container, $ContainerId) ;
					dbQueryFree ($res) ;					
				}														
				
				
			} // End >Row loop
		} // End Sheet loop
	} // End File uploaded

	return $Error_txt ;
?>
