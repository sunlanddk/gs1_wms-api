<?php

    require_once 'lib/item.inc' ;

    itemStart () ;
    itemHeader () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemFieldIcon ('Location', $Record['CompanyName'], 'company.gif', 'companyview', $Record['CompanyId']) ;
    if ($Record['CompanyAddress1']) itemField ('', $Record['CompanyAddress1']) ;
    if ($Record['CompanyAddress2']) itemField ('', $Record['CompanyAddress2']) ;
    itemField ('', $Record['CompanyCity']) ;
    itemField ('', $Record['CompanyCountry']) ;
    itemInfo ($Record) ;
    itemEnd () ;

    return 0 ;
?>
