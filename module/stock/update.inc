<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;

    printf ("<table>\n") ;
    // Form start
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    itemSpace () ;
    itemFieldRaw ('Status ID', formText ('StatusId', '', 10, 'text-align:left;') . 'used to mark surplus and loss items') ;
    itemSpace () ;
    itemSpace () ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=20000000>\n") ;
	printf ("<tr><td><p>File</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;  
  
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
