<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc' ;

/*
	Adjustment is calculated as diffence between stock Qty and Actual Qty in Excel sheet.
	
	Hence if real Stock Qty is different from Excel only difference is updated.

	This means Stock only has to be blocked from generating excel untill counted 
	(dosnt have to wait untill system is updated).

	Updating stock balance is:
		Adding qty by creating a new item as a clone of an existing and marking it with "StatusId" in batchnumber
		Consume loss qty by splitting item and mark with "StatusId" in batchnumber

	Qty that is not represented on the stock before status (not in the generated Excel Sheet) is created manuel
	with article->new item with batchnumber="StatusId"
	
	Updating position is:
		Container is named the description entered in "New position" colomn.
		
		If "delete" is entered container is set active=0 and pick pos is released (containerid in variantcode table is set to 0)
		
		If more lines is tagged with same position name a running number is appended to description according to display order.
*/
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 

	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);

		//	Excel read help!
		//	 $data->sheets[0]['numRows'] - count rows
		//	 $data->sheets[0]['numCols'] - count columns
		//	 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column
		//	 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
		//	    
		//	$data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
		//	if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
		//	$data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 

		$Error_txt=0 ;
		for ($k = 0; $k <= 0; $k++) {
			for ($i = 6; $i <= $data->sheets[$k]['numRows']; $i++) {
				// Initializing data
				$ArticleNumber = $data->sheets[0]['cells'][$i][1] ;
				$ArticleColorNumber = $data->sheets[0]['cells'][$i][3] ;
				$ArticleSize = $data->sheets[0]['cells'][$i][5] ;
				$QtyDiff=(float)$data->sheets[0]['cells'][$i][7] - (float)$data->sheets[0]['cells'][$i][9] ;		// C7: Stock Qty
																												// C9: Counted Qty.
				$Position = $data->sheets[0]['cells'][$i][10] ;								
				$ContainerId = (int)$data->sheets[0]['cells'][$i][11] ;								

				$PurchasedForId = (int)$data->sheets[0]['cells'][$i][14] ;
				$ReservationId  = (int)$data->sheets[0]['cells'][$i][21] ;
				$ActualPrice    = (float)$data->sheets[0]['cells'][$i][24] ;

				// Any change? Any Container number?
				if ($ContainerId > 0) {
					// Get items in container in question.
					if ($ArticleColorNumber == '') {
						$QueryTables = '' ;
						$QueryWhere = '' ;
					} else {
						$QueryTables = ", articlecolor ac, color co" ;
						$QueryWhere = sprintf (" and i.articlecolorid=ac.id and ac.active=1 and ac.colorid=co.id and co.number='%s'", $ArticleColorNumber) ;
					}
					if ($ArticleSize== '') {
						$x = 1 ;
					} else {
						$VariantDimension=tableGetFieldWhere ('Article','VariantDimension', sprintf('Number=%s', $ArticleNumber)) ;
						if ($VariantDimension) {
							$QueryWhere = $QueryWhere . sprintf (" and i.dimension='%s'", $ArticleSize) ;
						} else {
							$QueryTables = $QueryTables . ", articlesize az" ;
							$QueryWhere = $QueryWhere . sprintf (" and i.articlesizeid=az.id and az.active=1 and az.name='%s'", $ArticleSize) ;
						}
					}
					$query = sprintf ("SELECT i.* FROM item i, container c, article a %s
										WHERE i.containerid=c.id and i.active=1 and c.id=%d
										and i.articleid=a.id and a.active=1 and a.number='%s' %s",
									  $QueryTables, $ContainerId, $ArticleNumber, $QueryWhere) ;			 
					$res = dbQuery ($query) ;

					// Set new values for items
					While ($Item = dbFetch ($res)) {
						$Item['OwnerId']			= 2 ;
						$Item['PurchasedForId']		= $PurchasedForId ;
						$CaseCompany = (int)tableGetField('`Case`','CompanyId',(int)$Item['CaseId']) ;
						if (($ReservationId==$CaseCompany) or ($CaseCompany==0))
							$Item['ReservationId'] = $ReservationId ;
						else						
							$Item['ReservationId'] = $CaseCompany ;
						$Item['ReservationUserId']	= $User['Id'] ;
						$Item['ReservationDate']	= dbDateEncode(time()) ;
						$Item['Price']				= $ActualPrice ;
						tableWrite ('Item', $Item, $Item['Id']) ;
					}
					dbQueryFree ($res) ;					
				}
				
				// Move containers to new stock?
				
				
			} // End >Row loop
		} // End Sheet loop
	} // End File uploaded

	return $Error_txt ;
?>
