    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appStore">
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
            <h2>Add new position: {{position}}</h2>
            <h3>Max Containers<input type="number" min="1" v-model="max"></h3>
            <h3>Type 
                <select name="type" id="type" v-model="type">
                    <option value="sscc">SSCC</option>
                    <option value="gtin">GTIN</option>
                </select>
            </h3>
        </center>
        </br>
        </br>
        </br>
        <center>
            <div class="form">
                <div class="button" v-on:click="addPosition()" >Add position</div>
            </div>
        </center>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/addStock.js"></script>
<style>
    #appStore{
        position: relative;
    }
    #appStore table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        width: 100px;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        /*float: left;*/
        cursor: pointer;
        margin: auto;

    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    select{
        border: solid 1px black;
        width: 100px;
    }
</style>
<?php

    return 0 ;

?>
