<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

	itemStart () ;
	itemSpace () ;
	itemFieldIcon ('Stock', $Record['StockNo'] . ' - ' . $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
	itemField ('Description', $Record["Description"]) ;
	itemSpace () ;
	itemEnd () ;

	if ($Navigation['Parameters']=='occupation') {
		$_showContent = 1 ;
	} 
    listFilterBar () ;
	listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Position', 100) ;
    listHead ('Type', 60) ;
    listHead ('Capacity', 60) ;
	if ($_showContent) {
	    listHead ('# on Position', 80) ;
	    listHead ('Content') ;
	} else {
		listHeadIcon () ;
		listHead () ;
	}
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldIcon ('edit.gif', 'layoutedit', $row['Id']) ;
		listField ($row['Position']) ;
		listField ($row['Type']) ;
		listField ((int)$row['Max']) ;
		if ($_showContent) {
			switch ($row['Type']) {
				case 'sscc':
					$query = sprintf("select count(id) as NoContainers, group_concat(cast(sscc as char)) as SSCCs from container where active=1 and stockid=%d and position='%s'", $Id, $row['Position']) ;
					$result = dbQuery($query) ;
					$_content = dbFetch($result) ;
					listField ($_content['NoContainers']) ;
					$_many = $_content['NoContainers']>3?' ...':'' ;
					listField (substr($_content['SSCCs'],0,57).$_many) ;
					break ; 
				case 'gtin':
					$query = sprintf("select *, group_concat(cast(variantcode as char),': ',cast(Qty as unsigned), '  ') as VariantCodes, sum(Qty) as Qty, count(id) as NoVars
					from (
						select vc.id, vc.variantcode, sum(i.quantity) as Qty, c.position
						from (container c, item i, variantcode vc)
						where c.active=1 and i.active=1 and i.containerid=c.id and c.stockid=%d and c.position='%s' and vc.id=i.variantcodeid 
						group by i.variantcodeid) tab group by tab.Position", $Id, $row['Position']) ;
//		die($query) ;
					$result = dbQuery($query) ;
					$_noVariants = dbNumRows($result) ;
					$_content = dbFetch($result) ;
					// listField ($_noVariants) ;
					$_noVariants = $_content['NoVars']>1 ? ($_content['NoVars'] . ' Gtin ') : '' ;
					listField ((int)$_content['Qty']) ;
					$_many = strlen($_content['VariantCodes'])>67 ? ' ...' : '' ;
					listField ($_noVariants . substr($_content['VariantCodes'],0,55).$_many) ;
					break ; 
				default:
					listField('') ;
					break ;
			}
		} else {
			listFieldIcon ('delete.gif', 'layoutdelete', $row['Id']) ;
			listField() ;
		}
	}
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Positions configured', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
