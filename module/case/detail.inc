<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    printf ("<br>\n") ;
    itemStart () ;
    itemField ('Case', (int)$Record['Id']) ;
    itemEnd () ;
    printf ("<br>\n") ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Certificate', formDBSelect ('ArticleCertificateId', (int)$Record['ArticleCertificateId'], sprintf ('SELECT ArticleCertificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Value', $Record['ArticleId']), 'width:250px')) ; 
    itemSpace() ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
