<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'article' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Article', sprintf ('%s (%s)', $Record['Number'], $Record['Description']), 'article.gif', 'articleview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', sprintf ('%s (%s)', $Record['Name'], $Record['Number']), 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 80) ;
    listHead ('Description') ;
    listHead ('Article', 80) ;
    listHead ('Article Description') ;
    listHead ('Reference',120) ;
    listHead ('Customer', 80) ;
    listHead ('Name') ;
    listHead ('Cost price') ;
    if (!$HideDone) {
	listHead ('Done', 75) ;
    }
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'casepo', $row['Id']) ;
	listField ($row['Id']) ;
	listField ($row['Description']) ;
	listField ($row['ArticleNumber']) ;
	listField ($row['ArticleDescription']) ;
	listField ($row['CustomerReference']) ;
	listField ($row['CustomerNumber']) ;
	listField ($row['CustomerName']) ;
	listField ($row['CaseCostPriceId']>0?'Yes':'No') ;
	if (!$HideDone) {
	    listField (($row['Done']) ? date ("Y-m-d", dbDateDecode($row['DoneDate'])) : 'No') ;
	}
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Cases', 'colspan=2') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
