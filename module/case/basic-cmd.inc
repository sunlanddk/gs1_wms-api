<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'Description'		=> array (),
	'ArticleNumber'		=> array ('mandatory' => true,	'check' => true),
	'ArticleId'		=> array ('type' => 'set'),
	'CompanyId'		=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
	'CustomerReference'	=> array (),
	'Comment'		=> array (),
	'Done'			=> array ('type' => 'checkbox',	'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $fields, $Record, $User ;
	switch ($fieldname) {
	    case 'ArticleNumber':
		// Get Article
		$query = sprintf ("SELECT Article.Id, ArticleType.Product, Unit.Decimals AS UnitDecimals FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Number=\"%s\" AND Article.Active=1", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] <= 0) return sprintf ('article not found, number "%s"', $value) ;

		// Changed
		if ($Record['ArticleId'] == $row['Id']) return false ;

		// Check that the Article is for production
		if ($row['Product'] == 0) return 'specified Article is not a Product for Production' ;

		// No decimals on Units
		if ((int)$row['UnitDecimals']) return 'no decimals on Unit for Article allowed' ;

		// Ensure no ProductionOrders attached
		$query = sprintf ("SELECT Id FROM Production WHERE CaseId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return "Article can not be changed when Case has attached ProductionOrders" ;

		// Ensure no Order
		$query = sprintf ("SELECT Id FROM OrderLine WHERE CaseId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return "Article can not be changed when Case has attached Orders" ;

		// Ensure no Certifications specified
		if ($Record['ArticleCertificateId'] > 0) return 'Article can not be changed when a Certification has been specified for the Case' ;

		// Update ArticleId
		$fields['ArticleId']['value'] = $row['Id'] ;
		return false ;

	    case 'CompanyId' :
		if (!$changed) return false ;

		// Ensure no Order
		$query = sprintf ("SELECT Id FROM OrderLine WHERE CaseId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return "Customer can not be changed when Case has attached Orders" ;
		
		return true ;
		
	    case 'Done':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check Precondition for setting Case Done
		// Any ProdoctionOrders not done
		$query = sprintf ("SELECT Id FROM Production WHERE CaseId=%d AND Done=0 AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return "the Case can not be Done when unfinished ProductionsOrders exists" ;

		// Any Orders not done ?


		
		
		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    unset ($Record) ;
	    break ;
    }
     
    // Save record
    $res = saveFields ('Case', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Case
	    return navigationCommandMark ('caseview', $Record['Id']) ;
    }
    
    return 0 ;    
?>
