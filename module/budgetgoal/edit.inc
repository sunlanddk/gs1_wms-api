<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;


    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    unset ($Record) ;

	    // Default values
	    $Record['BudgetGoal'] = '0' ;
	    break ;
    }
     
    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('FromDate', formDate ('FromDate', dbDateDecode($Record['FromDate']))) ;
    itemSpace () ;
    itemFieldRaw ('Goal Internal', formText ('BudgetGoal', $Record['BudgetGoal'], 20) . "minutes pr. day") ;
    itemFieldRaw ('Goal External', formText ('BudgetGoalExt', $Record['BudgetGoalExt'], 20). "minutes pr. day") ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
