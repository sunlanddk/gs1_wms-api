<?php
   require_once  "module/sales/cart.inc" ;
   require_once  "lib/file.inc" ;
   require_once  "lib/parameter.inc" ;

   class OrderCart extends Cart {
      protected $_cookieName = "ordercart";
      protected $_deliveryDate = NULL;

      public function add_to_draft() {
         $fields  = array();
         $headers = array(
            'Number', 'Company', 'State', 'Season', 'Reference', 'Description', 'Discount', 'Total Price', 'Sales Person', 'Created'
         );
         $styles = array('Total Price' => "right", 'Discount' => "right");

         $queryFields = "`Order`.Id AS Number, Company.Number AS \"Company Number\", Company.Name AS Company, `Order`.CurrencyId,
      					IF(Order.Done,\"Done\",IF(NOT Order.Ready,\"Defined\",\"Ready\")) AS State,
      					Season.Name AS \"Season\", `Order`.Reference AS Reference, `Order`.Description AS Description,
      					ol.Quantity,
						SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'PriceSale1',
						ol.Discount, Currency.Name as CurrencyName,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", `Order`.CreateDate AS Created";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
                        LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
            $companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d) ', $this->_user['Id'], $this->_user['Id']);
         }
         if ($this->_user['AvatarId']>0) {
            $companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d) ', $this->_user['AvatarId'], $this->_user['AvatarId']);
         }
         if($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

//         $queryClause = "`ORDER`.Active=1 AND `ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0 AND `ORDER`.SeasonId=%s ".$companyClause;
         $queryClause = "`ORDER`.Active=1 AND `ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.SeasonId=%s ".$companyClause;
         parent::add_to_draft($fields, $headers, $styles, $queryFields, $queryTables, $queryClause, '`Order`.Id', '`Order`.Id');
      }

      public function edit_defaults() {
         global $nid;

         $_companyId =  ($this->_user['Extern']) ? $this->_user['CompanyId'] : (isset($_GET['CompanyId']) ? (int)$_GET['CompanyId'] : (int)$_COOKIE['customerId']);
		   $_companyId =  ($_companyId) ? $_companyId : (int)$_GET['id'];
         $_currency = $this->getCartCurrency($this->_cookieName);

         if ($_companyId > 0) {
            // Specific company
            $query = sprintf (
               'SELECT Company.*, c.Name as CurrencyName, c.Description as CurrencyDesc, co.FreightPrice as FreightPrice,  co.CurrencyId as FreightCurrencyId
                FROM Company
                    LEFT JOIN Currency c on c.Id = Company.CurrencyId
					LEFT JOIN Country co on co.id = Company.CountryId
                WHERE Company.Id=%d AND Company.Active=1', $_companyId
            ) ;
            $res        = dbQuery ($query) ;
            $_record    = dbFetch ($res) ;
            dbQueryFree ($res) ;

            $_company = $_record ;
            // Default Values
            unset ($_record) ;
            $_record['Name']            = $_company['Name'] ;
            $_record['Mail']            = $_company['Mail'] ;
            $_record['Number']          = $_company['Number'] ;
            $_record['SalesUserId']     = $_company['SalesUserId'] ;
            $_record['ToCompanyId']     = $this->_user['Extern'] ? $_company['ToCompanyId'] : $this->_user['CompanyId'];
            $_record['CompanyId']       = $_company['Id'] ;
            $_record['CurrencyId']      = $_company['CurrencyId'] ;
            $_record['FreightPrice']    = ($_company['FreightCurrencyId']==$_company['CurrencyId'])?(float)$_company['FreightPrice']:0 ;
            $_record['CurrencyName']    = sprintf('%s (%s)', $_company['CurrencyDesc'], $_company['CurrencyName']) ;
            $_record['PaymentTermId']   = $_company['PaymentTermId'] ;
            $_record['DeliveryTermId']  = $_company['DeliveryTermId'] ;
            $_record['CarrierId']       = $_company['CarrierId'] ;
            $_record['OrderTypeId']     = 1 ;
            $_record['ReceivedDate']    = dbDateOnlyEncode(time()) ;

            if ((int)$_company['DeliveryCountryId'] > 0) {
               $_record['Address1']     = $_company['DeliveryAddress1'] ;
               $_record['Address2']     = $_company['DeliveryAddress2'] ;
               $_record['ZIP']          = $_company['DeliveryZIP'] ;
               $_record['City']         = $_company['DeliveryCity'] ;
               $_record['CountryId']    = $_company['DeliveryCountryId'] ;
            } else {
               $_record['Address1']     = $_company['Address1'] ;
               $_record['Address2']     = $_company['Address2'] ;
               $_record['ZIP']          = $_company['ZIP'] ;
               $_record['City']         = $_company['City'] ;
               $_record['CountryId']    = $_company['CountryId'] ;
            }
			$_record['Body']    = 
'Please confirm the attached order confirmation from FUB to your sales agent.

Thank you.

Best regards,

FUB ApS
Postvej 101, Foldby
DK-8382 Hinnerup
P: +45 22 93 18 90
www.fub.dk'; 
         } else {
            $_record = array();
         }
         if (count($this->_cart) == 0) {
            navigationPasive ('sales.cart.save') ;
         }

		$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
         $this->_smarty->assign(array(
               'header'        => sprintf ('%s (%d)', $_company['Name'], $_company['Number']),
               'record'        => $_record,
               'user'          => $this->_user,
               'types'         => getOrderTypes(),
               'sales_ref'     => getSalesResponsible($this->_user['CompanyId']),
               'companies'     => getOwnerCompanies(), 																			//Owner
               'userCompanies' => getUserCompanies($_userid, $this->_user['CompanyId'],  $this->_user['SalesRef']), // Customers
               'currencies'    => $this->get_collection_currency(),
               'payment_terms' => getPaymentTerms(),
               'delivery_terms'=> getDeliveryTerms(),
               'carriers'      => getCarriers(),
               'countries'     => getCountries($this->_user['Extern']),
               'nid'           => $nid,
               'currencyId'    => $_currency['Id'],
               'currencyName'  => sprintf('%s (%s)', $_currency['Description'], $_currency['Name'])
         ));

         $this->_smarty->display('modules/sales/cart/edit_defaults.tpl');
         return 0;
      }

      public function  edit_proposal() {
         $this->_smarty->assign('Proposal', 1);
         return $this->edit_defaults();
      }

      public function update_lots() {
         $_lots          = $_GET['LaterDelivery'];
         $_deliveryDate  = $_GET['DeliveryDate'];
         $_seasonId      = $_GET['SeasonId'];

         if (isset($_deliveryDate)) {
            foreach ($this->_cart[$_seasonId]['items'] as &$item) {
               $item['DeliveryDate'] = $_deliveryDate;
            }
         } else if ($_lots != NULL && is_array($_lots)) {
            foreach ($_lots as $seasonId => $lots) {
               foreach ($lots as $lotId => $date) {
                  foreach ($this->_cart[$seasonId]['items'] as &$item) {
                     if ($item['LOTId'] == $lotId) {
                        $item['DeliveryDate'] = $date;
                     }
                  }
               }
            }
         }
         $this->saveCart($this->_cart);
         return 0;
      }

      public function save_new() {
         global $Config;

         $currencyId = (int)$_POST['CurrencyId'];
         $fields = array (
               'CompanyId'		=> array ('type' => 'integer',  'mandatory' => true),
               'ToCompanyId'	=> array ('type' => 'integer',	'mandatory' => true),

               'Description'	=> array (),
               'Reference'		=> array (),
               'ReceivedDate' 	=> array ('type' => 'date',	    'mandatory' => false),
               'OrderTypeId'	=> array ('type' => 'integer',	'mandatory' => true),

               'SalesUserId'	=> array ('type' => 'integer'),
               'CurrencyId'	    => array ('type' => 'integer',	'mandatory' => true),
               'PaymentTermId'	=> array ('type' => 'integer',	'mandatory' => true),
               'DeliveryTermId' => array ('type' => 'integer',	'mandatory' => true),
               'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true),

               'Address1'		=> array (),
               'Address2'		=> array (),
               'ZIP'			=> array (),
               'City'			=> array (),
               'CountryId'		=> array ('type' => 'integer'),
               'SeasonId'		=> array ('type' => 'set'),

               'InvoiceHeader'	=> array (),
               'InvoiceFooter'	=> array (),
               'Instructions'	=> array (),

               'OrderDoneId'	=> array ('type' => 'integer'),

               'Ready'			=> array ('type' => 'checkbox'),
               'ReadyUserId'	=> array ('type' => 'set'),
               'ReadyDate'		=> array ('type' => 'set'),

               'VariantCodes'	=> array ('type' => 'checkbox'),

               'Proposal'	    => array ('type' => 'integer'),

               'ExchangeRate'	=> array ('type' => 'set', 'value' => tableGetField ('Currency', 'Rate', $currencyId))
         ) ;

         $_saved   = false;
         $_seasons = array();

         try {
            mysql_query("START TRANSACTION");

            foreach ($this->_cart as $seasonId => $season) {
               $fields['SeasonId']['value']     = $seasonId;
               $_ready = ($_POST['Ready'] == 'on') ? 1 : 0 ;
               if ($_ready) {
                  // Set tracking information
                  $fields['ReadyUserId']['value']  = $this->_user['Id'] ;
                  $fields['ReadyDate']['value']    = dbDateEncode(time()) ;
               }
               $res = saveFields2 ('Order', NULL, $fields, true) ;

               if ( $res ) {
                  throw new Exception($res);
               } else {
                  $this->saveLine($this->Id, $seasonId, $currencyId);
                  $_seasons[] = $seasonId;
               }
               $_saved  = true;
            }

            mysql_query("COMMIT");
         } catch (Exception $e) {
            $_saved  = false;
            mysql_query("ROLLBACK");

            $this->_smarty->assign(array(
                  'error' => $e->getMessage()
            ));
            $this->_smarty->display('layout/error.tpl');
            return 0;
         }

         if ($_saved) {
            /*
             * send mail
             *
             */
            if ((int)$_POST['sendMail'] == 1) {
				$_pdffile=$this->generatePdfFile() ;
            	//preparing pdf attachment for the mail
               $_attachmentArray = array(
					array(
						'path'     => $_pdffile,
						'mimeType' => 'application/pdf',
						'name'     => sprintf('Order #%d.pdf', $this->Id)
					)
				);
            	
               if (isLocalPath($Config['imageStore'])) {
                  $_imagesPath = $Config['base_url'];
               } elseif (isURL($Config['imageStore'])) {
                  $_imagesPath = $Config['imageStore'];
               } else {
                  //imageStore is relative path
                  $_imagesPath = joinPaths($Config['base_url'], $Config['imageStore']);
               }

               $this->_smarty->assign(array(
                   'imagesPath'  => $_imagesPath,
                   'imageStore'   => $Config['imageStore'],
                   'base_url'    => $Config['base_url'],
                   'cart'        => $this->_cart
               ));
               $_param['body'] = nl2br($_POST['Body']) . '<br>' ; //. $this->_smarty->fetch('modules/sales/cart/mail.tpl');
//               $_param['body'] = $this->_smarty->fetch('modules/sales/cart/mail.tpl');
               $this->saveCustomerMail();
               $res = sendmail("ordercreated", $this->_user['Id'], NULL, $_param, NULL, false, $_POST['Email'], false, $_attachmentArray);
				if (is_file($_pdffile)) unlink ($_pdffile) ;

            }

            //remove from cart
            $_temp = array();
            foreach ($this->_cart as $seasonId => $season) {
               if (!in_array($seasonId, $_seasons)) {
                  $_temp[$seasonId] = $season;
               }
            }
            $this->saveCart($_temp);
            if (!($this->_user['SalesRef'] and $this->_user['AgentCompanyId']>0)) {
            	setcookie('customerId', NULL);
            	setcookie('customerId', NULL);
            }
			
            setcookie("listSettings", json_encode(array(
               "filter"     =>array(
                  "Created" => '' //array('value' => date('Y-m-d'))
               ),
               'sortDir'    => "DESC",
               'sortField'  => "Created"
            )));

            $this->_smarty->assign(array(
               'nid'        => navigationMark('sales.order'),
               'defaultNID' => navigationMark('sales.order.all')
            ));
         } else {
            $this->_smarty->assign(array(
               'mark' => 'sales.ordercart.view'
            ));
         }

         $this->_smarty->display('lib/redirect.tpl');
         return 0;
      }

      public function delete_from_cart() {
         return parent::delete_from_cart();
      }

      public function view_cart($cart = NULL) {
         $this->_smarty->assign("detailsMark", "sales.shop.details");
//         $this->_smarty->assign("showLots", 1);
		$_cartCustomerid = (int)$_COOKIE['customerId'];
		$_cartCustomer = tableGetField("Company", "Name", $_cartCustomerid);
        $this->_smarty->assign("cartCustomer", $_cartCustomer);

         parent::view_cart($cart);
      }

      protected function switchCurrency($seasonId, $currencyId) {
         parent::switchCurrency($seasonId, $currencyId); // TODO: Change the autogenerated stub
      }

      protected function redirectDraft() {
         $this->_smarty->assign(array(
            'nid'        => navigationMark('sales.order'),
            'defaultNID' => navigationMark('sales.order.draft')
         ));
      }

      protected function saveLineOld($orderId, $seasonId, $currencyId) {
         $_no = 0;  //select max(No) from orderline where orderid = 10739

         // Find next position
         $query  = sprintf ('SELECT MAX(No) AS No FROM OrderLine WHERE OrderId=%d AND Active=1', (int)$orderId) ;
         $result = dbQuery ($query) ;
         $row    = dbFetch ($result) ;
         $_no    = (int)$row['No'] ;
         dbQueryFree ($result) ;

         if ($_no == 0) {
            // No existing OrderLine to default to
            unset ($row) ;
            $row['DeliveryDate'] = dbDateEncode (time()) ; // + 60*60*24*60) ;
            $row['Surplus']      = 0 ; $this->_user['CompanySurplus'] ;
         } else {
            // Default to DeliveryDate in last OrderLine
            $query  = sprintf ('SELECT * FROM OrderLine WHERE OrderId=%d AND No=%d AND Active=1', (int)$orderId, $_no) ;
            $result = dbQuery ($query) ;
            $row    = dbFetch ($result) ;
            dbQueryFree ($result) ;
         }
         /*
          * check currency
          */
         $this->switchCurrency($seasonId, $currencyId);

         $_deliveryDate = NULL;
         foreach ($this->_cart[$seasonId]['items'] as &$article) {
            //check if collection memeber exists
            $query  = sprintf ('SELECT * FROM OrderLine WHERE OrderId=%d AND Active=1 AND ArticleId=%d AND ArticleColorId=%d', (int)$orderId, (int)$article['ArticleId'], (int)$article['ArticleColorId']) ;
            $result = dbQuery ($query) ;
            $row    = dbFetch ($result) ;
            dbQueryFree ($result) ;
            $_orderlineId = (int)$row['Id'];
            if ($_orderlineId > 0) {
               //update existing
               $_orderLine = array (
                  'Quantity'  => (int)$row['Quantity'] + (int)$article['qty']
               );
               $_orderlineId = tableWrite ('OrderLine', $_orderLine, $_orderlineId);
            } else {
               //create new OrderLine
               $_no++;
               $_deliveryDate = $article['DeliveryDate'] != NULL ? $article['DeliveryDate'] : $article['LotDate'];
               $_discount = 0;
               if (isset($_POST['Discount'])) {
                  //Discount shouldn't be set on articletype=9 articles
                  if (!isset($article['ArticleTypeId']) || (int)$article['ArticleTypeId'] <> 9) {
                     $_discount = (int)$_POST['Discount'];
                  }
               }
               $_orderLine = array (
                     'Active' 			  => 1,
                     'No'                => $_no,
                     'ArticleId'         => (int)$article['ArticleId'],
                     'Description' 	     => html_entity_decode($article['Desc'], ENT_QUOTES | ENT_IGNORE),
                     'ArticleColorId'    => (int)$article['ArticleColorId'],
                     'OrderId'           => (int)$orderId,
                     'PriceCost'         => 0,
                     'PriceSale'         => is_string($article['Price']) ? price_to_float($article['Price']) : $article['Price'],
                     'Discount'          => $_discount,
                     'DeliveryDate'      => $_deliveryDate,
                     'Quantity'          => (int)$article['qty']
 //                    'Surplus'	          => (int)$row['Surplus']
               ) ;

               $_orderlineId = tableWrite ('OrderLine', $_orderLine);
            }


            foreach ($article['Sizes'] as $sizeId => $size) {
               //check if size exists
               $query  = sprintf ('SELECT * FROM OrderQuantity WHERE OrderLineId=%d AND Active=1 AND ArticleSizeId=%d', (int)$_orderlineId, (int)$sizeId) ;
               $result = dbQuery ($query) ;
               $row    = dbFetch ($result) ;
               dbQueryFree ($result) ;
               $_quantityId = (int)$row['Id'];
               if ($_quantityId > 0) {
                  //update existing
                  $_quantity = array (
                        'Quantity'  => (int)$row['Quantity'] + (int)$size['qty']
                  );
                  $_quantityId = tableWrite ('OrderQuantity', $_quantity, $_quantityId);
               } else {
                  $_quantity = array (
                        'Active' 			=> 1,
                        'OrderLineId'		=> (int)$_orderlineId,
                        'ArticleSizeId'	    => (int)$sizeId,
                        'Quantity'		    => (int)$size['qty']
                  ) ;
                  $_quantityId = tableWrite ('OrderQuantity', $_quantity);
               }
            }
         }

         //save Freight
         if ((!isset($_POST['Freight']) or ($_POST['Freight'] == '')) or ($this->_deliveryDate == $_deliveryDate)) {
            return;
         }
//         $_freight       = (int)$_POST['Freight'];
         $this->_deliveryDate = $_deliveryDate;
         $_freightPrice  = price_to_float($_POST['Freight']);
         $_orderLine = array (
               'Active' 		 => 1,
               'No'              => $_no + 1,
               'Description'     => 'Freight Cost',
               'ArticleId'       => parameterGet('DefFreightArticle'),
               'ArticleColorId'  => 0,
               'OrderId'         => (int)$orderId,
               'PriceCost'       => 0,
               'PriceSale'       => $_freightPrice,
               'DeliveryDate'    => $_deliveryDate,
               'Quantity'        => 1,
               'Surplus'	     => 0,
               'CustArtRef'      => '',
               'InvoiceFooter'   => ''
         ) ;
         $_orderlineId = tableWrite ('OrderLine', $_orderLine);
      }

	protected function saveLine($orderId, $seasonId, $currencyId) {
         $_no = 0;  //select max(No) from orderline where orderid = 10739

         // Find next position
         $query  = sprintf ('SELECT MAX(No) AS No FROM OrderLine WHERE OrderId=%d AND Active=1', (int)$orderId) ;
         $result = dbQuery ($query) ;
         $row    = dbFetch ($result) ;
         $_no    = (int)$row['No'] ;
         dbQueryFree ($result) ;

         if ($_no == 0) {
            // No existing OrderLine to default to
            unset ($row) ;
            $row['DeliveryDate'] = dbDateEncode (time()) ; // + 60*60*24*60) ;
            $row['Surplus']      = 0 ; $this->_user['CompanySurplus'] ;
         } else {
            // Default to DeliveryDate in last OrderLine
            $query  = sprintf ('SELECT * FROM OrderLine WHERE OrderId=%d AND No=%d AND Active=1', (int)$orderId, $_no) ;
            $result = dbQuery ($query) ;
            $row    = dbFetch ($result) ;
            dbQueryFree ($result) ;
         }
         /*
          * check currency
          */
         $this->switchCurrency($seasonId, $currencyId);

         $_deliveryDate = NULL;

         foreach ($this->_cart[$seasonId]['items'] as &$article) {
			$SizePrice = (float)-1 ;
            foreach ($article['Sizes'] as $sizeId => $size) {
				$_pricesale = is_string($size['sizeprice']) ? price_to_float($size['sizeprice']) : $size['sizeprice'] ;
				if (!((float)$SizePrice == (float)$_pricesale)) {
					if (!($SizePrice==-1)) {
					   $_orderLine = array (
							 'Quantity'          => (int)$_lineqty
					   ) ;
					   $_orderlineId = tableWrite ('OrderLine', $_orderLine,$_orderlineId);
					}
					$SizePrice = (float)$size['sizeprice'] ;
					$_lineqty = 0 ;
				   //create new OrderLine
				   $_no++;
				   $_deliveryDate = $article['DeliveryDate'] != NULL ? $article['DeliveryDate'] : $article['LotDate'];
				   $_discount = 0;
				   if (isset($_POST['Discount'])) {
					  //Discount shouldn't be set on articletype=9 articles
					  if (!isset($article['ArticleTypeId']) || (int)$article['ArticleTypeId'] <> 9) {
						 $_discount = (int)$_POST['Discount'];
					  }
				   }
				   $_orderLine = array (
						 'Active' 			  => 1,
						 'No'                => $_no,
						 'ArticleId'         => (int)$article['ArticleId'],
						 'Description' 	     => html_entity_decode($article['Desc'], ENT_QUOTES | ENT_IGNORE),
						 'ArticleColorId'    => (int)$article['ArticleColorId'],
						 'OrderId'           => (int)$orderId,
						 'PriceCost'         => 0,
						 'PriceSale'         => $_pricesale, // is_string($article['Price']) ? price_to_float($article['Price']) : $article['Price'],
						 'Discount'          => $_discount,
						 'DeliveryDate'      => $_deliveryDate,
						 'Quantity'          => (int)$article['qty']
	 //                    'Surplus'	          => (int)$row['Surplus']
				   ) ;

				   $_orderlineId = tableWrite ('OrderLine', $_orderLine);
				
				}
               //check if size exists
               $query  = sprintf ('SELECT * FROM OrderQuantity WHERE OrderLineId=%d AND Active=1 AND ArticleSizeId=%d', (int)$_orderlineId, (int)$sizeId) ;
               $result = dbQuery ($query) ;
               $row    = dbFetch ($result) ;
               dbQueryFree ($result) ;
               $_quantityId = (int)$row['Id'];
               if ($_quantityId > 0) {
                  //update existing
                  $_quantity = array (
                        'Quantity'  => (int)$row['Quantity'] + (int)$size['qty']
                  );
                  $_quantityId = tableWrite ('OrderQuantity', $_quantity, $_quantityId);
               } else {
                  $_quantity = array (
                        'Active' 			=> 1,
                        'OrderLineId'		=> (int)$_orderlineId,
                        'ArticleSizeId'	    => (int)$sizeId,
                        'Quantity'		    => (int)$size['qty']
                  ) ;
                  $_quantityId = tableWrite ('OrderQuantity', $_quantity);
               }
			   $_lineqty += (int)$_quantity['Quantity'] ;
            }
		   $_orderLine = array (
				 'Quantity'          => (int)$_lineqty
		   ) ;
		   $_orderlineId = tableWrite ('OrderLine', $_orderLine,$_orderlineId);
         }

         //save Freight
         if ((!isset($_POST['Freight']) or ($_POST['Freight'] == '')) or ($this->_deliveryDate == $_deliveryDate)) {
            return;
         }
//         $_freight       = (int)$_POST['Freight'];
         $this->_deliveryDate = $_deliveryDate;
         $_freightPrice  = price_to_float($_POST['Freight']);
         $_orderLine = array (
               'Active' 		 => 1,
               'No'              => $_no + 1,
               'Description'     => 'Freight Cost',
               'ArticleId'       => parameterGet('DefFreightArticle'),
               'ArticleColorId'  => 0,
               'OrderId'         => (int)$orderId,
               'PriceCost'       => 0,
               'PriceSale'       => $_freightPrice,
               'DeliveryDate'    => $_deliveryDate,
               'Quantity'        => 1,
               'Surplus'	     => 0,
               'CustArtRef'      => '',
               'InvoiceFooter'   => ''
         ) ;
         $_orderlineId = tableWrite ('OrderLine', $_orderLine);
      }

	  
      public function change_currency(){
         if(isset($_POST['currencyId'])){

            if(isset($_POST['companyId'])){
            	setcookie('customerId', $_POST['companyId']);
            }

         	$currencyId = $_POST['currencyId'];
         	setcookie('currencyId', $currencyId);
         	foreach ($this->_cart as $seasonId => $season) {
         		$this->switchCurrency($seasonId, $currencyId);
         	}
         	parent::saveCart($this->_cart);

            $this->view_cart($this->_cart);
         } else {
            $this->_smarty->assign(array(
               'currencies'       => getSeasonCollectionCurrencies( $this->_user['Extern'] ? tableGetField('Company', 'ToCompanyId', $this->_user['CompanyId']) : $this->_user['CompanyId']),
               'userCompanies'    => getCompaniesWithCurrency($this->_user['Id'], $this->_user['CompanyId'],  $this->_user['SalesRef']),
               'selectedCurrency' => $_COOKIE['currencyId'],
               'selectedCompany'  => $_COOKIE['customerId']
            ));
            $this->_smarty->display('modules/sales/cart/currency.tpl');
         }
      	 return 0;
      }

      private function generatePdfFile() {
         global $User, $Id, $Config;

         $User = $this->_user;
         $Id   = $this->Id;

         $Navigation['Function'] = 'printorder';
         include(dirname(__FILE__) . '/../order/load.inc');

         $Config['savePdfToFile'] = true;
         include(dirname(__FILE__) . '/../order/printorder.inc');
         $Config['savePdfToFile'] = false;

         return fileName('order', (int)$this->Id);
      }
   }


   return 0;
?>