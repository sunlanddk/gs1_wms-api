<?php
   require_once  "module/sales/cart.inc" ;

   class ClaimCart extends Cart {
      protected $_cookieName = "claimcart";

      public function add_to_draft() {
         $fields  = array();
         $headers = array(
            'Number', 'Company', 'State', 'Season', 'Reference', 'Description', 'Total Price', 'Sales Person', 'Created'
         );
         $styles = array('Total Price' => "right");
      	 $queryFields = "Claim.Id AS Number, Company.Number AS \"Company Number\", Company.Name AS Company, Claim.CurrencyId,
			            ClaimState.Name AS State, Season.Name AS \"Season\", Claim.Reference AS Reference, Claim.Description AS Description,
			            CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", Claim.CreateDate AS Created,
      	                cl.Quantity, cl.PriceSale, cl.Discount, Currency.Name as CurrencyName";

	     $queryTables = "Claim
      					LEFT JOIN Company ON Company.Id=Claim.CompanyId
      					LEFT JOIN User ON User.Id=Claim.SalesUserId
      					LEFT JOIN Season ON Season.Id=Claim.SeasonId
      					LEFT JOIN ClaimState ON ClaimState.Id=Claim.ClaimStateId
      					LEFT JOIN Currency ON Currency.Id=Claim.CurrencyId
      					RIGHT JOIN Claimline cl ON cl.ClaimId = Claim.Id";


	     $companyClause = sprintf('AND Claim.ToCompanyId = %d', $this->_user['CompanyId']);
	     if($this->_user['Extern']) {
	        $companyClause = sprintf('AND Claim.CompanyId=%d', $this->_user['CompanyId']);
	     }

         $queryClause = "Claim.ClaimStateId = 1 AND Claim.SeasonId=%s ".$companyClause;
         parent::add_to_draft($fields, $headers, $styles, $queryFields, $queryTables, $queryClause, 'Claim.Id DESC', 'Claim.Id');
      }

      public function edit_defaults() {
         global $nid;

         $_companyId =  ($this->_user['Extern']) ? $this->_user['CompanyId'] : (int)$_GET['CompanyId'];

         if ($_companyId > 0) {
            // Specific company
            $query = sprintf (
                  'SELECT Company.*
                   FROM Company
                   WHERE Company.Id=%d AND Company.Active=1',$_companyId
            ) ;
            $res        = dbQuery ($query) ;
            $_record    = dbFetch ($res) ;
            dbQueryFree ($res) ;

            $_company = $_record ;
            // Default Values
            unset ($_record) ;
            $_record['Name']            = $_company['Name'] ;
            $_record['Number']          = $_company['Number'] ;
            $_record['SalesUserId']     = $_company['SalesUserId'] ;
            $_record['ToCompanyId']     = $this->_user['Extern'] ? $this->_user['CompanyId'] : $_company['ToCompanyId'] ;
            $_record['CompanyId']       = $_company['Id'] ;
            $_record['CurrencyId']      = $_company['CurrencyId'] ;

            $_invoices = getInvoices($_companyId);

            $_inv_keys = array_keys($_invoices);
            $_inv_selected = $_inv_keys[1] ? $_inv_keys[1] : '';
         } else {
            $_record = array();
            $_invoices = array();
         }
         if (count($this->_cart) == 0) {
            navigationPasive ('sales.cart.save') ;
         }
         $this->_smarty->assign(array(
               'header'        => sprintf ('%s (%d)', $_company['Name'], $_company['Number']),
               'record'        => $_record,
               'user'          => $this->_user,
               'types'         => getClaimTypes(),
               'states'        => getClaimStates(),
               'sales_ref'     => getSalesResponsible($this->_user['CompanyId']),
               'companies'     => getOwnerCompanies(),
               'userCompanies' => getUserCompanies($this->_user['Id'], $this->_user['CompanyId'], $this->_user['SalesRef']),
               'currencies'    => $this->get_collection_currency(),
               'invoices'      => $_invoices,
               'selected'      => $_inv_selected,
               'nid'           => $nid
         ));

         $this->_smarty->display('modules/sales/cart/edit_claim_defaults.tpl');
         return 0;
      }

      public function save_new() {
         global $Config;

         $currencyId = (int)$_POST['CurrencyId'];
         $fields = array (
               'CompanyId'		=> array ('type' => 'integer',  'mandatory' => true),
               'ToCompanyId'	=> array ('type' => 'integer',	'mandatory' => true),

               'Description'	=> array (),
               'Reference'		=> array (),
               'ClaimTypeId'	=> array ('type' => 'integer',	'mandatory' => true),
               'ClaimStateId'	=> array ('type' => 'integer',	'mandatory' => true),

               'SalesUserId'	=> array ('type' => 'integer'),
               'CurrencyId'	    => array ('type' => 'integer',	'mandatory' => true),
               'InvoiceId'	    => array ('type' => 'integer'),

               'SeasonId'		=> array ('type' => 'set'),

               'InternalComment'	=> array (),
               'ExternalComment'	=> array ()
         ) ;

         $_saved   = false;
         $_seasons = array();

         try {
            mysql_query("START TRANSACTION");

            foreach ($this->_cart as $seasonId => $season) {
               $fields['SeasonId']['value']     = $seasonId;

               $res = saveFields2 ('Claim', null, $fields, true) ;

               if ( $res ) {
                  throw new Exception($res);
               } else {
                  $this->saveLine($this->Id, $seasonId, $currencyId);
                  $_seasons[] = $seasonId;
               }
               $_saved  = true;
            }

            mysql_query("COMMIT");
         } catch (Exception $e) {
            $_saved  = false;
            mysql_query("ROLLBACK");

            $this->_smarty->assign(array(
               'error' => $e->getMessage()
            ));
            $this->_smarty->display('layout/error.tpl');
            return 0;
         }

         if ($_saved) {
            /*
             * send mail
            *
            */
            if ((int)$_POST['sendMail'] == 1) {
               $this->_smarty->assign(array(
                  'fileStore' => $Config['fileStore'],
                  'cart'      => $this->_cart
               ));
               $_param['body'] = $this->_smarty->fetch('modules/sales/cart/mail.tpl');
               $this->saveCustomerMail();
               $res = sendmail("claimcreated", $this->_user['Id'], null, $_param, null, false, $_POST['Email']);
            }
            //remove from cart
            $_temp = array();
            foreach ($this->_cart as $seasonId => $season) {
               if (!in_array($seasonId, $_seasons)) {
                  $_temp[$seasonId] = $season;
               }
            }
            $this->saveCart($_temp);

            setcookie("listSettings", json_encode(array(
               "filter" => array(
                  "Created" => array('value' => date('Y-m-d'))
               ),
               'sortDir'    => "DESC",
               'sortField'  => "Created"
            )));
            $_claimStates  = getClaimStates();
            $_claimStateId = (int)$_POST['ClaimStateId'];

            $this->_smarty->assign(array(
                  'nid'        => navigationMark('sales.claim'),
                  'defaultNID' => navigationMark('sales.claim.'.$_claimStates[$_claimStateId])
            ));
         } else {
            $this->_smarty->assign(array(
               'mark' => 'sales.claimcart.view'
            ));
         }
         $this->_smarty->display('lib/redirect.tpl');

         return 0;
      }

      public function view_cart($cart = null) {
         $this->_smarty->assign("detailsMark", "sales.claim.details");
         parent::view_cart($cart);
      }

      protected function redirectDraft() {
         $this->_smarty->assign(array(
               'nid'        => navigationMark('sales.claim'),
               'defaultNID' => navigationMark('sales.claim.request')
         ));
      }
      protected function saveLine($claimId, $seasonId, $currencyId) {
         $_no = 0;  //select max(No) from orderline where orderid = 10739

         // Find next position
         $query  = sprintf ('SELECT MAX(No) AS No FROM ClaimLine WHERE ClaimId=%d AND Active=1', (int)$claimId) ;
         $result = dbQuery ($query) ;
         $row    = dbFetch ($result) ;
         $_no    = (int)$row['No'] ;
         dbQueryFree ($result) ;

         // Default DeliveryDate
         if ($_no == 0) {
            // No existing OrderLine to default to
            unset ($row) ;
            $row['DeliveryDate'] = dbDateEncode (time() + 60*60*24*60) ;
            $row['Surplus']      = $this->_user['CompanySurplus'] ;
         }
          else {
            // Default to DeliveryDate in last OrderLine
            $query  = sprintf ('SELECT * FROM ClaimLine WHERE ClaimId=%d AND No=%d AND Active=1', (int)$claimId, $_no) ;
            $result = dbQuery ($query) ;
            $row    = dbFetch ($result) ;
            dbQueryFree ($result) ;
         }
         /*
          * check currency
         */
         $this->switchCurrency($seasonId, $currencyId);

         foreach ($this->_cart[$seasonId]['items'] as $article) {
            //check if collection memeber exists
            $query  = sprintf ('SELECT * FROM ClaimLine WHERE ClaimId=%d AND Active=1 AND ArticleId=%d AND ArticleColorId=%d', (int)$claimId, (int)$article['ArticleId'], (int)$article['ArticleColorId']) ;
            $result = dbQuery ($query) ;
            $row    = dbFetch ($result) ;
            dbQueryFree ($result) ;
            $_claimLineId = (int)$row['Id'];
            if ($_claimLineId > 0) {
               //update existing
               $_claimLine = array (
                     'Quantity'  => (int)$row['Quantity'] + (int)$article['qty']
               );
               $_claimLineId = tableWrite ('ClaimLine', $_claimLine, $_claimLineId);
            } else {
               //create new ClaimLine
               $_no++;
               $_claimLine = array (
                     'Active' 			  => 1,
                     'No'                => $_no,
                     'ArticleId'         => (int)$article['ArticleId'],
                     'Description' 	     => $article['Desc'],
                     'ArticleColorId'    => (int)$article['ArticleColorId'],
                     'ClaimId'           => (int)$claimId,
   //                  'PriceCost'         => 0,
                     'PriceSale'         => is_string($article['Price']) ? price_to_float($article['Price']) : $article['Price'],
                     'Discount'          => 0,
                     'DeliveryDate'      => $row['DeliveryDate'],
                     'Quantity'          => (int)$article['qty'],
                     'Surplus'	          => $row['Surplus']
               ) ;

               $_claimLineId = tableWrite ('ClaimLine', $_claimLine);
            }

            foreach ($article['Sizes'] as $sizeId => $size) {
               //check if size exists
               $query  = sprintf ('SELECT * FROM ClaimQuantity WHERE ClaimLineId=%d AND Active=1 AND ArticleSizeId=%d', (int)$_claimLineId, (int)$sizeId) ;
               $result = dbQuery ($query) ;
               $row    = dbFetch ($result) ;
               dbQueryFree ($result) ;
               $_quantityId = (int)$row['Id'];
               if ($_quantityId > 0) {
                  //update existing
                  $_quantity = array (
                        'Quantity'  => (int)$row['Quantity'] + (int)$size['qty']
                  );
                  $_quantityId = tableWrite ('ClaimQuantity', $_quantity, $_quantityId);
               } else {
                  $_quantity = array (
                        'Active' 		=> 1,
                        'ClaimLineId'	=> (int)$_claimLineId,
                        'ArticleSizeId'	=> (int)$sizeId,
                        'Quantity'		=> (int)$size['qty']
                  ) ;
                  $_quantityId = tableWrite ('ClaimQuantity', $_quantity);
               }
            }
         }
      }
   }

   return 0;
?>