<?php

   require_once 'lib/list.inc';
   require_once 'lib/save.inc';
   require_once 'lib/table.inc';
   require_once 'lib/currency.inc';
   require_once 'lib/lookups.inc';
   require_once 'lib/mail.inc';
   require_once  'styleutils.inc';
   /*
    * {
    *    seasonId : {seasonName, subTotal, qty, SubTotal,
    *       items : {
    *          ArticleId, ArticleTypeId, Price, Desc, Number, TopBottom, Discount, DeliveryDate, CurrencyId
    *          ArticleColorId, ColorNumber, ColorDesc, LOTId, LOTName, CollectionMemberId, DeliveryDate
    *          qty, SubTotal,
    *          Sizes : {
    *             SizeId, Q-ty, SizeName
    *          }
    *       }
    *    }
    * }
    *
    *
    */
   class Cart {
      protected $_user;
      protected $_smarty;
      protected $_cookieName = "";
      protected $_cart = array();
      protected $_userCart = array();

      public function __construct() {
         global $User, $smarty;

         $this->_user   = $User;
         $this->_smarty = $smarty;

         $this->_smarty->clearAllAssign();
         $this->_smarty->assign('cartMark', $this->_cookieName);

         $this->_userCart = $this->getCart($this->_cookieName);

         $this->_cart = json_decode(str_replace('\\', '', $this->_userCart['Cart']), true);
         if (is_null($this->_cart) || count($this->_cart) == 0) {
            $this->_cart = array();
         }
      }

      public function add_to_draft($fields, $headers, $styles, $queryFields, $queryTables, $queryClause, $queryOrder = '', $queryGroup = '') {
         $items = array();
         foreach ($this->_cart as $seasonId => $season) {
            $_queryClause     = sprintf($queryClause, $seasonId);
            $result           = listLoad($fields, $queryFields, $queryTables, $_queryClause, $queryOrder, $queryGroup, false);
            $items[$seasonId] = array();
            while ($row = dbFetch($result)) {
               $row['Total Price'] = float_to_currency((100 - (int)$row['Discount']) * $row["PriceSale"] * $row["Quantity"] * 0.01) . " " . $row["CurrencyName"];
               $row['Discount']    = $row['Discount'] . "%";
               array_push($items[$seasonId], $row);
            }
            dbQueryFree($result);

         }

         if (count($this->_cart) == 0) {
            navigationPasive('sales.cart.save');
         }

         $this->_smarty->assign(array(
                                     //params for list
                                     'headers' => $headers,
                                     'styles'  => $styles,
                                     'items'   => $items,
                                     'cart'    => $this->_cart
                                ));

         $this->_smarty->display('modules/sales/cart/add_to_draft.tpl');
         return 0;
      }

      public function add_to_cart() {
         $_articleInfo      = json_decode(str_replace('\\', '', $_POST['articleInfo']), true);
/*
         $_articleInfoStr      = str_replace('\\', '', $_POST['articleInfo']) ;
		 $_articleInfoStr      = htmlentities($_articleInfoStr, ENT_QUOTES | ENT_IGNORE, "UTF-8") ;
         $_articleInfo      = json_decode($_articleInfoStr, true);
*/
         $_articleId        = (int)$_articleInfo['ArticleId'];
         $_articleColorId   = (int)$_articleInfo['ArticleColorId'];
         $_seasonId         = (int)$_articleInfo['SeasonId'];
         $_currencyId       = (int)$_articleInfo['CurrencyId'];
         $_seasonCurrencyId = (int)$this->_cart[$_seasonId]['currency']['Id'];

         $_currencies = getCurrenciesArray();
//         $_categories      = getCategoriesGroupArray();
         $_exists = false;
         if (array_key_exists($_seasonId, $this->_cart)) {
            foreach ($this->_cart[$_seasonId]['items'] as &$item) {
               if (
                  ((int)$item['ArticleId'] == $_articleId)
                  && ((int)$item['ArticleColorId'] == $_articleColorId)
//                      && ($_seasonCurrencyId == $_currencyId)
               ) {
                  $item['Sizes'] = $_articleInfo['Sizes'];
                  $_exists       = true;
                  break;
               }
            }
         } else {
            $this->_cart[$_seasonId] = array(
               'seasonName' => $_articleInfo['SeasonName'],
               'currency'   => $_currencies[$_articleInfo['CurrencyId']],
//                  'categoryGroup' => $_categories,
               'subTotal'   => 0,
               'qty'        => 0,
               'items'      => array()
            );
         }

         if (!$_exists) {
            array_push($this->_cart[$_seasonId]['items'], $this->addAticleInfo($_articleInfo));
         }
         $_cartInfo = $this->get_cart_info($this->_cart);
         $this->saveCart($this->_cart);

         printf(json_encode($_cartInfo));
         return 0;
      }

      public function delete_from_cart() {
         $_deleted        = false;
         $_articleId      = (int)$_GET['ArticleId'];
         $_articleColorId = (int)$_GET['ArticleColorId'];
         $_seasonId       = (int)$_GET['SeasonId'];
         $_temp           = array();

         foreach ($this->_cart as $seasonId => $season) {
            if ($seasonId == $_seasonId) {
               $_temp_items = array();
               foreach ($this->_cart[$seasonId]['items'] as $item) {
                  if (((int)$item['ArticleId'] == $_articleId) && ((int)$item['ArticleColorId'] == $_articleColorId)) {
                     $_deleted = true;
                  } else {
                     $_temp_items[] = $item;
                  }
               }

               if (count($_temp_items) > 0) {
                  $_temp[$seasonId]          = $season;
                  $_temp[$seasonId]['items'] = $_temp_items;
               }
            } else {
               $_temp[$seasonId] = $season;
            }
         }

         $this->saveCart($_temp);
         $this->view_cart($_temp);

         if (count($_temp) == 0) {
         	if (!($this->_user['SalesRef'] and $this->_user['AgentCompanyId']>0)) {
            	setcookie('currencyId', NULL);
            	setcookie('customerId', NULL);
         	}
         }

         return 0;
      }

      public function get_info() {
         $_cartInfo = $this->get_cart_info();
         printf(json_encode($_cartInfo));
         return 0;
      }

      public function get_mark() {
         return $this->_cookieName;
      }

      public function view_cart($cart = NULL) {
         global $Navigation, $Config;

         if (is_null($cart)) {
            $cart = $this->_cart;
         }

         $this->get_cart_info($cart);

         if (count($cart) == 0) {
            navigationPasive('sales.' . $this->_cookieName . '.neworder');
            navigationPasive('sales.' . $this->_cookieName . '.newproposal');
            navigationPasive('sales.' . $this->_cookieName . '.add_to_dratf');
            navigationPasive('sales.' . $this->_cookieName . '.createnew');
         }
         $this->_smarty->assign(array(
                                     'cart'       => $cart,
                                     'cartMark'   => $this->_cookieName,
                                     'cartName'   => $Navigation['Header'],
                                     'categories' => $this->get_category_info($cart),
                                     'lots'       => $this->get_LOTs($cart),
                                     'imageStore'  => $Config['imageStore']
                                ));
         $this->_smarty->display('modules/sales/cart/view.tpl');
         return 0;
      }

      public function update_draft() {
         $_orders  = $_GET['orders'];
         $_saved   = false;
         $_seasons = array();
         if (is_array($_orders)) {
            try {
               mysql_query("START TRANSACTION");

               foreach ($_orders as $value) {
                  $this->saveLine($value['orderId'], $value['seasonId'], $value['currencyId']);
                  $_seasons[] = $value['seasonId'];
               }

               mysql_query("COMMIT");
               $_saved = true;
            } catch (Exception $e) {
               $_saved = false;
               mysql_query("ROLLBACK");
            }
         }
         if ($_saved) {
            //remove from cart
            $_temp = array();
            foreach ($this->_cart as $seasonId => $season) {
               if (!in_array($seasonId, $_seasons)) {
                  $_temp[$seasonId] = $season;
               }
            }
            $this->saveCart($_temp);
            $this->redirectDraft();
         } else {
            $this->_smarty->assign(array(
                                        'mark' => 'sales.' . $this->_cookieName . '.view'
                                   ));
         }
         $this->_smarty->display('lib/redirect.tpl');
         return 0;
      }

      public function get_LOTs($cart) {
         if (is_null($cart)) {
            $cart = $this->_cart;
         }
         $res = array();

         foreach ($cart as $seasonId => $season) {
            $res[$seasonId] = getDeliveryLOTsArray($seasonId);
         }
         return $res;
      }

      public function get_collection_currency() {
         $res = array();
         foreach ($this->_cart as $seasonId => $season) {
            foreach ($season['items'] as $article) {
               array_push($res, $article['CollectionMemberId']);
            }
         }

         return getCollectionCurrencies($res);
      }

      public function get_category_info($cart) {
         if (is_null($cart)) {
            $cart = $this->_cart;
         }
         $res         = array();
         $_categories = getCategoriesGroupArray();
         foreach ($cart as $seasonId => $season) {
            $res[$seasonId] = array() + $_categories;
            foreach ($season['items'] as $article) {
               if (isset($article['CategoryGroupId'])) {
                  $res[$seasonId][$article['CategoryGroupId']]['qty'] += $article['qty'];
                  $res[$seasonId][$article['CategoryGroupId']]['pcs'] += 1;
               }
            }
         }
         return $res;
      }

      public function get_cart_info(&$cart = NULL) {
         $_seasons  = $_styles = $_pcs = $_summ = $_tops = $_bottoms = 0;

         $_currencyId = isset($_COOKIE['currencyId'])
            ? $_COOKIE['currencyId'] : NULL;

         if ($_currencyId != NULL) {
            $_currency = StyleUtils::get_currency($_currencyId);
         }

         $_currencyName = empty($_currency)
            ? '' : $_currency['name'];

         if (is_null($cart)) {
            $cart = $this->_cart;
         }


         foreach ($cart as &$season) {
            $season['subTotal'] = 0;
            $season['qty']      = 0;

            $_styles += count($season['items']);

            foreach ($season['items'] as &$article) {
               $article['qty'] = 0;
               $article['subTotal'] = 0;
               if ($article['TopBottom'] == "Top") {
                  $_tops++;
               }
               if ($article['TopBottom'] == "Bottom") {
                  $_bottoms++;
               }

               foreach ($article['Sizes'] as $size) {
                  $article['qty'] += (int)$size['qty'];
				  $article['subTotal'] += (int)$size['qty'] * (float)$size['sizeprice'] ;
               }
//               $article['subTotal'] = $article['qty'] * (float)$article['Price'];
               $season['subTotal'] += $article['subTotal'];
               $season['qty'] += $article['qty'];

//               $season['categoryGroup'][$article['CategoryGroupId']]['qty'] += $article['qty'];
//               $season['categoryGroup'][$article['CategoryGroupId']]['pcs'] += 1;

               $_currencyName = $season['currency']['Name'];
            }
            $_pcs += $season['qty'];
            $_summ += $season['subTotal'];
         }

         if (($_tops + $_bottoms) > 0) {
            $_tops_perc    = round((100 * $_tops) / ($_tops + $_bottoms), 2);
            $_bottoms_perc = 100 - $_tops_perc;
         } else {
            $_tops_perc    = 0;
            $_bottoms_perc = 0;
         }

         return array(
            'styles'       => $_styles,
            'seasons'      => count($cart),
            'pcs'          => $_pcs,
            'summ'         => number_format($_summ, 2, ',', ' '),
            'tops'         => $_tops,
            'bottoms'      => $_bottoms,
            'tops_perc'    => $_tops_perc,
            'bottoms_perc' => $_bottoms_perc,
            'currency'     => $_currencyName
         );
      }

      private function addAticleInfo($articleInfo) {
         return array(
            'ArticleId'          => $articleInfo['ArticleId'],
            'Price'              => $articleInfo['Price'],
            'Desc'               => htmlentities($articleInfo['Desc'], ENT_QUOTES | ENT_IGNORE, "UTF-8"),
            'Number'             => $articleInfo['Number'],
            'ArticleColorId'     => $articleInfo['ArticleColorId'],
            'ArticleTypeId'      => $articleInfo['ArticleTypeId'],
            'ColorNumber'        => $articleInfo['ColorNumber'],
            'ColorDesc'          => $articleInfo['ColorDesc'],
            'TopBottom'          => $articleInfo['TopBottom'],
            'CurrencyId'         => $articleInfo['CurrencyId'],
            'CategoryGroupId'    => $articleInfo['CategoryGroupId'],
            'CollectionMemberId' => $articleInfo['CollectionMemberId'],
            'LOTName'            => $articleInfo['LOTName'],
            'LOTId'              => $articleInfo['LOTId'],
            'LotDate'            => $articleInfo['LotDate'],
            'qty'                => 0,
            'subTotal'           => 0,
            'Sizes'              => $articleInfo['Sizes']
         );
      }

      protected function saveLine($orderId, $seasonId, $currencyId) {
         throw new Exception("You should ovveride Cart::saveLine() method");
      }

      protected function redirectDraft() {
         throw new Exception("You should ovveride Cart::redirectDraft() method");
      }

      protected function switchCurrency($seasonId, $currencyId) {
         $_switchCurrency = false;

         if ($currencyId != $this->_cart[$seasonId]['currency']['Id']) {
            $_currencies                        = getCurrenciesArray();
            $this->_cart[$seasonId]['subTotal'] = 0;
            $this->_cart[$seasonId]['currency'] = $_currencies[$currencyId];
            $_switchCurrency                    = true;
         }

         if ($_switchCurrency) {
            foreach ($this->_cart[$seasonId]['items'] as &$article) {
               $article['Price']    = tableGetFieldWhere('collectionmemberprice', 'WholesalePrice',
                  sprintf("CollectionMemberId=%d AND CurrencyId=%d", (int)$article['CollectionMemberId'], $currencyId)
               );
               $article['subTotal'] = $article['Price'] * $article['qty'];
               $this->_cart[$seasonId]['subTotal'] += $article['subTotal'];
            }
         }

      }

      public static function getCart($cartName) {
         global $User;

         $query = sprintf("
            SELECT *
            FROM usercart WHERE CartName='%s' and UserId=%d", $cartName, $User['Id']);


         $res = dbQuery($query);
         $row = dbFetch($res);
         dbQueryFree($res);

         if ($row == NULL) {
            $row = array(
               "CartName" => $cartName,
               "UserId"   => $User['Id'],
               "Cart"     => ""
            );
         }
         return $row;
      }

      public static function getCartCurrency($cartName) {
         $cart = Cart::getCart($cartName);
         $cart = json_decode(str_replace('\\', '', $cart['Cart']), true);
         if (isset($cart)) {
            foreach ($cart as $seasonId => $season) {
               return $season['currency'];
            }
         }
      }

      protected function saveCart($cart) {
         $this->_userCart['Cart'] = json_encode($cart);
         tableWrite('usercart', $this->_userCart, (int)$this->_userCart['Id']);
      }

      protected function saveCustomerMail() {
         $mail       = $_POST['Email'];
         $update     = ($_POST['EmailUpdate'] == 'true');
         $customerId = (int)$_POST['CompanyId'];
         if ($update) {
            $query = sprintf("UPDATE Company SET Mail='%s' WHERE Id=%d", $mail, $customerId);
            dbQuery($query);
         }

      }
   }

   return 0;
?>