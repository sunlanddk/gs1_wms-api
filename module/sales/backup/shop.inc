<?php
    require_once  "lib/list.inc" ;
    require_once  "lib/table.inc" ;
    require_once  "lib/log.inc" ;
    require_once  "styleutils.inc" ;

	class Shop {
		private $_user;
        protected $_currency;
        protected $_customerId;

		public function __construct(){
			global $User;

			$this->_user = $User;

         $_currencyId = isset($_COOKIE['currencyId'])
            ? $_COOKIE['currencyId']
            : (isset($_POST['currencyId'])
               ? $_POST['currencyId']
               : $_GET['currencyId']
            );

         $_companyId = isset($_POST['companyId'])
            ? $_POST['companyId']
            : (isset($_GET['companyId'])
               ? $_GET['companyId']
               : $_COOKIE['customerId']);

         if ($_currencyId == NULL) {
            $this->_currency = Cart::getCartCurrency('ordercart');
            $_currencyId     = isset($this->_currency['Id'])
               ? $this->_currency['Id']
               : NULL;
         }

         //get default currency for External Users
         if ($_currencyId == NULL && $this->_user['Extern']) {
            $_companyId = $this->_user['CompanyId'];
            $_tmp_curr  = tableGetField('Company', 'CurrencyId', $this->_user['CompanyId']);
            if ($_tmp_curr > 0) {
               $_currencyId = $_tmp_curr;
            }
         }

         //get default currency for Agents
         if ($_currencyId == NULL && $this->_user['SalesRef'] && $this->_user['AgentCompanyId'] > 0) {
            $_tmp_curr = tableGetField('Company', 'CurrencyId', $this->_user['AgentCompanyId']);
            if ($_tmp_curr > 0) {
               $_currencyId = $_tmp_curr;
            }
         }

			if ($_currencyId != NULL) {
			   $this->_currency = StyleUtils::get_currency($_currencyId);
			   setcookie('currencyId', $_currencyId);
			}
			if ($_companyId != NULL) {
				$this->_customerId = $_companyId;
				setcookie('customerId', $_companyId);
			}
      }

		public function get_all_list() {
			global $smarty, $nid, $totalRecords, $Config;
			$_empty = true;

			$nid = navigationMark("sales.shop.all");
			$vnid = navigationMark("sales.shop.details");

			$_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;
//logPrintTime ('point 1') ;
			$queryFields = StyleUtils::get_query_fields();
			$queryTables = StyleUtils::get_query_tables();
			
			$user_company_id = StyleUtils::get_user_company_id($this->_user);
			$queryClause = $this->get_query_clause($user_company_id, $_currency_id);
			$queryOrder = "a.Number, a.Description";
//logPrintTime ('point 2') ;
			$queryFilter = StyleUtils::get_filters($user_company_id) ;
//logPrintTime ('point 3') ;
			$result = listLoad ($queryFilter, $queryFields, $queryTables, $queryClause, $queryOrder, '', true);
//logPrintTime ('point 4') ;

			if(mysql_num_rows($result)>0) {
		    	$_empty = false;
				$items = array();
	    		while ($row = dbFetch($result)) {
	    			$row["inCart"] = StyleUtils::is_article_in_cart('ordercart', $row["seasonId"], $row["Id"], $row["ColorId"]);

					if (!$row["PreSaleState"]) { // Check if sold out
						$_sizes = $this->get_article_sizes($row["Id"], $row["ColorId"], $row["seasonId"], $row["PreSaleState"], 787) ; 
						$_available = 0 ;
						foreach($_sizes as $i => $size) {
							$_available += $size['available'] ;
						}
						$row["soldOut"] = ($_available > 0) ? 0 : 1 ;
					}
					if ($row["ShowOnlyState"]) $row["soldOut"] = 1 ;
//					if ()
					$row['MinMax'] = StyleUtils::get_cm_minmax_price($row['CollectionMemberId'], $_currency_id) ;
	    			array_push($items, $row);
	    		}
			}
    		dbQueryFree($result);
//logPrintTime ('point 4') ;

    		if(!$_empty){
    			printf("<div class='item-panel'></div>");
	    		listFilterBar ();

		    	$total_pages = ceil($totalRecords/LINES);
		    	$current_page = 1;
				if ($_GET['offset'] > 0) {
		    		$current_page = ($_GET['offset']+LINES)/LINES;
		    	}

	    		$smarty->assign(array(
					'nid'			=> $nid,
					'vnid'			=> $vnid,
	    			'imageStore'		=> $Config['imageStore'],
					'listItems'		=> $items,
	    			'currency'		=> $this->_currency,

	    			//params for paging
	    			'overall_pages'  => $total_pages,
	    			'current_page'   => $current_page,
	    			'lines_per_page' => LINES
		        ));
		        $smarty->display('lib/paging_bar.tpl');
		        $smarty->display('modules/sales/styles.tpl');
		        $smarty->display('lib/paging_bar.tpl');
    		} else {
    			listFilterBar ();
    			$smarty->display('lib/no_records.tpl');
    		}
//logPrintTime ('point 5') ;

	        return 0;
		}

		public function get_top_ten_style() {
			global $smarty, $nid, $Config;
			$_empty = true;

			$vnid = navigationMark("sales.shop.details");

			$user_company_id = StyleUtils::get_user_company_id($this->_user);

			$_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;

			$queryFields = StyleUtils::get_query_fields().", qnty";
			
			$queryTables = StyleUtils::get_query_tables().sprintf("
				inner join (
					select l.ArticleId as Id, l.articlecolorid, sum(oq.Quantity) as qnty, o.SeasonId as SeasonId  from
					`orderline` l
					inner join `orderquantity` oq ON oq.orderlineid=l.id and oq.active=1
					inner join `Order` o ON o.Id = l.OrderId 
					left join `Season` s ON s.Id = o.SeasonId
					where s.Active=1 and s.Done=0 and o.Active=1 and l.Active=1 and o.Ready=1 and o.toCompanyId=%d  
					group by l.ArticleId, l.articlecolorid, o.SeasonId
					having sum(oq.Quantity) > 0
				) b on b.Id = a.Id and b.articlecolorid = ac.id and b.seasonid=s.id", $user_company_id);

			$queryClause = $this->get_query_clause($user_company_id, $_currency_id) ." and a.articletypeid=1";
			$queryOrder = "qnty desc limit 10";

	    	$result = listLoad (StyleUtils::get_filters($user_company_id), $queryFields, $queryTables, $queryClause, $queryOrder);

			if(mysql_num_rows($result)>0) {
		    	$items = array();
	    		while ($row = dbFetch($result)) {
	    			//if(intval($row["qnty"]) > 0){
		    			$_empty = false;
		    			$row["inCart"] = StyleUtils::is_article_in_cart('ordercart', $row["seasonId"], $row["Id"], $row["ColorId"]);
		    			array_push($items, $row);
	    			//}
	    		}
			}
    		dbQueryFree($result);

    		if(!$_empty){
    			printf("<div class='item-panel'></div>");
	    		listFilterBar ();
	    		$smarty->assign(array(
					'nid'			=> $nid,
					'vnid'			=> $vnid,
	    			'imageStore'		=> $Config['imageStore'],
					'listItems'		=> $items,
	    			'currency'		=> $this->_currency
		        ));
		        $smarty->display('modules/sales/styles.tpl');
    		} else {
    			listFilterBar ();
    			$smarty->display('lib/no_records.tpl');
    		}
	        return 0;
		}

		public function get_top_five_types(){
			global $smarty, $nid, $Config;
			$_empty = true;

			$vnid = navigationMark("sales.shop.details");

			$_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;

			$user_company_id = StyleUtils::get_user_company_id($this->_user);
			
			$queryFields = StyleUtils::get_query_fields().", acg.Id as keyId, acg.name as keyName, acg.DisplayOrder as acgDisplayOrder, b.qnty";

			$queryTables = StyleUtils::get_query_tables().sprintf("
				left join articlecategorygroup acg on acg.id=cat.ArticleCategoryGroupId 
				inner join (
					select l.ArticleId as Id, l.articlecolorid, sum(oq.Quantity) as qnty, o.SeasonId as SeasonId  from
					`orderline` l
					inner join `orderquantity` oq ON oq.orderlineid=l.id and oq.active=1
					inner join `Order` o ON o.Id = l.OrderId 
					left join `Season` s ON s.Id = o.SeasonId
					where s.Active=1 and s.Done=0 and o.Active=1 and l.Active=1 and o.Ready=1 and o.toCompanyId=%d  
					group by l.ArticleId, l.articlecolorid, o.SeasonId
					having sum(oq.Quantity) > 0
				) b on b.Id = a.Id and b.articlecolorid = ac.id and b.seasonid=s.id", $user_company_id);
//				left join `articlecategory` cat on cat.Category=substring(a.Number,2,3)

			$queryClause = $this->get_query_clause($user_company_id, $_currency_id) ." and a.articletypeid=1 and IFNULL(acg.Id,0)>0 ";
			$queryOrder = "acgDisplayOrder, b.qnty desc";

	    	$result = listLoad (StyleUtils::get_filters($user_company_id), $queryFields, $queryTables, $queryClause, $queryOrder);

			$items = $this->group_query_result($result);
    		dbQueryFree($result);
			
    		if(count($items) > 0){
    			printf("<div class='item-panel'></div>");
	    		listFilterBar ();
	    		$smarty->assign(array(
					'nid'			=> $nid,
	    			'vnid'			=> $vnid,
	    			'imageStore'		=> $Config['imageStore'],
					'listItems'		=> $items,
	    			'currency'		=> $this->_currency
		        ));
		        $smarty->display('modules/sales/style/types.tpl');
    		} else {
    			listFilterBar ();
    			$smarty->display('lib/no_records.tpl');
    		}
	        return 0;
		}

		public function get_top_countries(){
			global $smarty, $nid, $Config;
			$_empty = true;

			$vnid = navigationMark("sales.shop.details");

			$_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;
			
			$user_company_id = StyleUtils::get_user_company_id($this->_user);

			$queryFields = StyleUtils::get_query_fields().", b.countryId as keyId, b.qnty, (select Description from `country` ctr where ctr.Id=b.countryId) as keyName ";

			$queryTables = StyleUtils::get_query_tables().sprintf("
				inner join (
					select o.countryId, l.ArticleId as Id, l.articlecolorid, sum(oq.Quantity) as qnty, o.SeasonId as SeasonId  from
					`orderline` l
					inner join `orderquantity` oq ON oq.orderlineid=l.id and oq.active=1
					inner join `Order` o ON o.Id = l.OrderId 
					left join `Season` s ON s.Id = o.SeasonId
					where o.countryId>0 and s.Active=1 and s.Done=0 and o.Active=1 and l.Active=1 and o.Ready=1 and o.toCompanyId=%d  
					group by o.countryId, l.ArticleId, articlecolorid, o.SeasonId
					having sum(oq.Quantity) > 0
				) b on b.Id = a.Id and b.articlecolorid = ac.id and b.seasonid=s.id", $this->_user['CompanyId']);

			$queryClause = $this->get_query_clause($user_company_id, $_currency_id) ." and a.articletypeid=1";
			$queryOrder = "keyName, b.qnty desc";

	    	$result = listLoad (StyleUtils::get_filters($user_company_id), $queryFields, $queryTables, $queryClause, $queryOrder);

			$items = $this->group_query_result($result);
    		dbQueryFree($result);

    		if(count($items) > 0){
    			printf("<div class='item-panel'></div>");
	    		listFilterBar ();
	    		$smarty->assign(array(
					'nid'			=> $nid,
	    			'vnid'			=> $vnid,
	    			'imageStore'		=> $Config['imageStore'],
					'listItems'		=> $items,
	    			'currency'		=> $this->_currency
		        ));
		        $smarty->display('modules/sales/style/types.tpl');
    		} else {
 //   			$this->get_articles_for_categorygroup(0, 0, 0);
    			listFilterBar ();
    			$smarty->display('lib/no_records.tpl');
    		}
	        return 0;
		}

		public function get_focus_articles() {
			global $smarty, $nid, $Config;
			$_empty = true;

			$nid = navigationMark("sales.shop.focus");
			$vnid = navigationMark("sales.shop.details");

			$_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;
			
			$user_company_id = StyleUtils::get_user_company_id($this->_user);

			$queryFields = StyleUtils::get_query_fields();
			$queryTables = StyleUtils::get_query_tables();
			$queryClause = $this->get_query_clause($user_company_id, $_currency_id)." and m.Focus=1";
			$queryOrder = "a.Number, a.Description";

			$result = listLoad (StyleUtils::get_filters($user_company_id), $queryFields, $queryTables, $queryClause, $queryOrder);
			if(mysql_num_rows($result)>0) {
		    	$_empty = false;
		    	$items = array();
	    		while ($row = dbFetch($result)) {
	    			$row["inCart"] = StyleUtils::is_article_in_cart('ordercart', $row["seasonId"], $row["Id"], $row["ColorId"]);
	    			array_push($items, $row);
	    		}
			}
    		dbQueryFree($result);

    		if(!$_empty){
    			printf("<div class='item-panel'></div>");
	    		listFilterBar ();
	    		$smarty->assign(array(
					'nid'			=> $nid,
					'vnid'			=> $vnid,
	    			'imageStore'		=> $Config['imageStore'],
					'listItems'		=> $items,
	    			'currency'		=> $this->_currency
		        ));
		        $smarty->display('modules/sales/styles.tpl');
			} else {
				listFilterBar ();
    			$smarty->display('lib/no_records.tpl');
    		}
	        return 0;
		}

		public function show_details() {
			require_once 'lib/lookups.inc' ;

			global $smarty, $nid, $Config, $Defaults;

			$_article_id  = $_GET['articleId'];
			$_color_id    = $_GET['colorId'];
			$_season_id   = $_GET['seasonId'];
			$_list_nid    = $_GET['listnvid'];

			$vnid = $nid;
			$nid = $_list_nid;

			if (isset($this->_currency)) {
			   $_currency_id = isset($this->_currency['id']) ? $this->_currency['id'] : 0;
			} else {
            $this->selectCurrency($_article_id, $_color_id, $_season_id, $nid, $vnid);
            return 0;
			}

			$_def_cat = getArticleCategory($Defaults['articleCategoryId']);

			$query = sprintf("select distinct a.Id as articleId, a.Number, ac.Id as colorId, col.Number as colorNumber,
									sex.Name as sex, b.Name as brand, s.Id as seasonId, s.Name as seasonName, s.OwnerId as OwnerId,
									a.Description as name, a.comment, a.ArticleTypeId, col.Description as color,
									cmp.WholesalePrice, cmp.RetailPrice, cmp.CampaignPrice,
									cat.TopBottom, ifnull(cat.ArticleCategoryGroupId, %d) as categoryGroupId,
			                        m.Id as collectionMemberId, cl.Name as lotName, cl.Id as lotId, DATE(cl.Date) as lotDate, cl.PreSale as PreSaleState, cl.ShowOnly as ShowOnlyState
									from
									`season` s
												left join `collection` c on c.SeasonId=s.Id and c.Active=1
												left join `collectionmember` m on m.CollectionId=c.Id and m.Active=1 
			                                    left join `collectionlot` cl on cl.Id = m.CollectionLOTId
												left join `collectionmemberprice` cmp on cmp.CollectionMemberId=m.Id and cmp.Active=1
												left join `article` a on a.Id=m.ArticleId
												left join `articlecolor` ac on ac.id=m.ArticleColorId
												left join `color` col on col.id=ac.ColorId
												left join `articlecategory` cat on cat.Category=substring(a.Number,2,3)
												left join `sex` sex on sex.Id=ac.SexId
												left join `brand` b on b.Id=c.BrandId
									where a.Id=%d and ac.Id=%d and s.Id=%d and cmp.CurrencyId=%d", $_def_cat['ArticleCategoryGroupId'], $_article_id, $_color_id, $_season_id, $_currency_id);

			$res = dbQuery ($query) ;
		    $_style = dbFetch ($res) ;
		    dbQueryFree ($res) ;
			
		    $_lot_delivery_date = date($_style['lotDate']);
		    $_today_date = date("Y-m-d");
		   	$_style['lotDate'] =   (strtotime($_lot_delivery_date) > strtotime($_today_date)) ? $_lot_delivery_date :  date("Y-m-d",strtotime("+4 days", strtotime($_today_date)));
			
		   	$_from_next_season =   $_style['PreSaleState'] ; // (strtotime($_lot_delivery_date) > strtotime($_today_date));

			$_style['MinMax'] = StyleUtils::get_cm_minmax_price($_style['collectionMemberId'], $_currency_id) ;

			$smarty->assign(array(
				'nid'			=> $nid,
				'vnid'			=> $vnid,
				'currency'		=> $this->_currency,
				'style'			=> $_style,
				'colors'		=> $this->get_alternative_colors($_article_id, $_color_id, $_season_id),
				'unlimited'		=> $_from_next_season,
				'sizes'			=> $this->get_article_sizes($_article_id, $_color_id, $_season_id, $_from_next_season, $_style['OwnerId'], $_style['ShowOnlyState'],$_style['collectionMemberId'], $_currency_id),
		       'imageStore'		=> $Config['imageStore'],
			    'reload'        => $_GET['reload']
	        ));
	        $smarty->display('modules/sales/style/view.tpl');

			return 0;
		}

		private function get_query_clause($company_id, $currency_id = 0){
			return sprintf("a.Active=1 and s.Active=1 and s.Done=0 and s.ShowOnB2B=1 and c.Active=1 AND m.Active=1 and m.Cancel=0  and m.ShowOnB2B=1 and s.OwnerId=%d and cmp.CurrencyId=%d",
			$company_id, $currency_id);
		}
		
		private function group_query_result($result){
			$items = array();
			if(mysql_num_rows($result)>0) {
	    		while ($row = dbFetch($result)) {
	    			$_key_id = intval($row["keyId"]);
	    			if(array_key_exists($_key_id, $items)){
	    				$_val = &$items[$_key_id];
	    				if($_val["cnt"]<5){
	    					$_val["cnt"] = $_val["cnt"] + 1;
	    					array_push($_val["articles"], $row);
	    				}
	    				unset($_val);
	    			} else {
	    				$_val = array();
	    				$_val["cnt"] = 1;
	    				$_val["Id"] = $_key_id;
	    				$_val["Name"] = $row["keyName"];
	    				$_val["articles"] = array();
	    				array_push($_val["articles"], $row);
	    				$items[$_key_id] = $_val;
	    			}
	    		}
			}
			return $items;
		}

		private function get_article_sizes($article_id, $color_id, $season_id, $unlimited, $owner_id, $showonlystate, $cm_id, $curr_id){
			require_once 'lib/parameter.inc' ;

			$reqcontainer_id = ParameterGet('ReqContainerId');
			$show_max = ParameterGet('ShowMaxAvailable');
			if(empty($show_max))$show_max=10;

			$query = sprintf ("SELECT ArticleSize.Id as Id, ArticleSize.Name as Value, ArticleSize.DisplayOrder, CollectionmemberPrice.Id as CollectionmemberPriceId, CollectionmemberPriceSize.WholeSalePrice, CollectionmemberPriceSize.RetailPrice
						FROM ArticleSize
						left JOIN CollectionmemberPrice ON CollectionmemberPrice.CollectionMemberId=38  AND CollectionmemberPrice.CurrencyId=1  AND CollectionmemberPrice.Active=1
						left JOIN CollectionmemberPriceSize ON CollectionmemberPriceSize.ArticleSizeId=ArticleSize.Id AND CollectionmemberPriceSize.CollectionMemberPriceId=CollectionMemberPrice.Id AND CollectionmemberPriceSize.Active=1
						WHERE ArticleSize.ArticleId=16 AND ArticleSize.Active=1", $cm_id, $curr_id,  $article_id);
						
			$result = mysql_query($query);
			$items = array();
	    	while ($row = mysql_fetch_assoc($result)) {
//logPrintTime ('1 ' . $row['CollectionmemberPriceId'] . ' ') ;			
	    		$qnty = -1;
	    		if(!$unlimited) {
		    		$qnty = $this->get_article_qnty_from_pickstock($season_id, $article_id, $color_id, $row['Id']);
		    		$qnty_reqcon = $this->get_article_qnty_from_reqcontainer($reqcontainer_id, $article_id, $color_id, $row['Id']);
		    		$qnty_in_order = $this->get_article_qnty_in_orders($season_id, $article_id, $color_id, $row['Id'], $owner_id);
		    		if(($qnty+$qnty_reqcon-$qnty_in_order) < $show_max){
		    			$qnty = $qnty + $qnty_reqcon-$qnty_in_order;
		    		} else {
						if($this->_user['Extern'] ) { // or $this->_user['SalesRef']) {
							$qnty = $show_max ; // $qnty - $qnty_in_order;
						} else {
							$qnty = $qnty + $qnty_reqcon-$qnty_in_order;
						}
		    		}
					if($showonlystate) $qnty = 0 ;
		    		if($qnty < 0) $qnty = 0;
	    		}
	    		$row['available'] = $qnty;
	    		$row['qnty'] = StyleUtils::get_article_qnty_from_cart('ordercart', $season_id, $article_id, $color_id, $row['Id']);

	    		array_push($items, $row);
	    	}
			mysql_free_result ($result);
			return $items;
		}

		private function get_article_qnty_from_pickstock($season_id, $article_id, $color_id, $size_id){
			$qnty = 0;
			$query = sprintf("select sum(i.Quantity) as qnty from
							`season` s
							left join `container` c on c.StockId=s.pickstockid or c.StockId=s.fromstockid
							left join `item` i on i.ContainerId=c.Id
							where s.Id=%d
							and i.ArticleId=%d  and i.articlecolorId=%d  and i.ArticleSizeId=%d and i.Active=1", $season_id, $article_id, $color_id, $size_id);
			$result = mysql_query($query);
			if ($row = mysql_fetch_assoc($result)) {
	    		$qnty = $row['qnty'];
	    	}
			return $qnty;
		}

		private function get_article_qnty_from_reqcontainer($reqcontainer_id, $article_id, $color_id, $size_id){
			$qnty = 0;
return $qnty; // On request from Bubbkid.
			$query = sprintf("select sum(i.Quantity) as qnty from
							container c
							left join item i on i.ContainerId=c.Id
							where c.Id=%d
							and i.ArticleId=%d  and i.articlecolorId=%d  and i.ArticleSizeId=%d and i.Active=1", $reqcontainer_id, $article_id, $color_id, $size_id);
			$result = mysql_query($query);
			if ($row = mysql_fetch_assoc($result)) {
	    		$qnty = $row['qnty'];
	    	}
			return $qnty;
		}

		private function get_article_qnty_in_orders($season_id, $article_id, $color_id, $size_id, $owner_id){
			$qnty = 0;
			$query = sprintf("select sum(q.Quantity) as qnty, max(l.id) as orderlineid from
							(`order` o, season s)
							left join `orderline` l on l.OrderId=o.Id
							left join `orderquantity` q on q.OrderLineId=l.Id  and q.active=1
                            inner join collection c on c.seasonid=o.seasonid and c.active=1
                            inner join collectionmember cm on cm.CollectionId=c.id and cm.ArticleId=l.ArticleId and cm.ArticleColorId=l.ArticleColorId and cm.active=1
                            inner join collectionlot cl on cl.id=cm.CollectionLOTId and cl.active=1
							where o.Active=1 and l.active=1 and o.Done=0 and l.Done=0 and o.ToCompanyId = %d 
							and s.id=o.seasonid and s.ShowOnlyB2B=0 and l.active=1 
							and l.ArticleId=%d and l.ArticleColorId=%d and q.ArticleSizeId=%d and cl.presale=0 and cl.showonly=0
							group by q.articlesizeid", $owner_id, $article_id, $color_id, $size_id);
//							printf($query) ;
			$result = mysql_query($query);
			if ($row = mysql_fetch_assoc($result)) {
	    		$qnty = $row['qnty'];
	    	}

			$query = sprintf("select sum(quantity) as qnty from item where orderlineid=%d and articlesizeid=%d and active=1", $row['orderlineid'], $size_id);
			$result = mysql_query($query);
			if ($row = mysql_fetch_assoc($result)) {
	    		$qnty = ($row['qnty'] > $qnty) ? 0 : $qnty - $row['qnty'];
	    	}

			return $qnty;
		}

		private static function get_alternative_colors($article_id, $color_id, $season_id){
			require_once 'lib/lookups.inc' ;
			$query = sprintf("select distinct ac.Id, col.Description as Value from
							`season` s
							left join `collection` c on c.SeasonId = s.Id
							left join `collectionmember` m on m.CollectionId = c.Id
							left join `articlecolor` ac on ac.id = m.ArticleColorId
							left join `color` col on col.id = ac.ColorId
							where s.Id=%d and m.articleId=%d and m.active=1 and m.cancel=0", $season_id, $article_id);
			return getLookupValuesArray($query);
		}

		private function selectCurrency($articleId, $colorId, $seasonId, $nid, $vnid) {
		   global $smarty;
		   $smarty->assign(array(
		   		'articleId' => $articleId,
		   		'colorId' => $colorId,
		   		'seasonId' => $seasonId,
		   		'nid' => $nid,
		   		'vnid' => $vnid,
		   		'currencies'    => getSeasonCollectionCurrencies(StyleUtils::get_user_company_id($this->_user)),
		   		'userCompanies' => getCompaniesWithCurrency($this->_user['Id'], $this->_user['CompanyId'],  $this->_user['SalesRef']),
		   		'selectedCompany' => $this->_customerId
		   ));
		   $smarty->display('modules/sales/style/currency.tpl');
		}

		/*private function get_season_from_filters(){
			$listSettings    = json_decode(str_replace('\\', '', $_COOKIE['listSettings']), true);
			if(isset($listSettings) && isset($listSettings['filter'])){
				$filters = $listSettings['filter'];
				foreach ($filters as $key => $value) {
					if($key == 'Season' && !empty($value)){
						return $value;
 					}
				}
			}
			return 0;
		}*/

	}

   return 0;
?>