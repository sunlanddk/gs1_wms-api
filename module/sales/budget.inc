<?php

   require_once 'lib/navigation.inc';
   require_once 'lib/fieldtype.inc';
   require_once 'lib/save.inc';
   require_once 'lib/list.inc';
   require_once 'lib/lookups.inc';

   class Budget {
      private $_user;
      private $_currency_rate = 100;
      private $_currency_name = 'EUR';
      private $_seasons = array();
      private $_season_ids = '';
      private $_total = array();
      private $_salesResponsibleLogged = false;

      public function __construct() {
         global $User;
         $this->_user = $User;

         $this->_salesResponsibleLogged = ($User['SalesRef'] == 1);
         $this->_AgentManager           = ($User['AgentManager'] == 1);
         if ($this->_salesResponsibleLogged and $User['AgentCompanyId'] > 0) {
            $query = 'select Currency.Name, Currency.Rate from `Currency`, Company where Currency.Id=Company.CurrencyId and Company.Id=' . $User['AgentCompanyId'];
         } else {
            $query = 'select Name, Rate from `Currency` where Id=1';
         }
         $result = mysql_query($query);
         if ($row = mysql_fetch_assoc($result)) {
            $this->_currency_rate = $row['Rate'];
            $this->_currency_name = $row['Name'];
         }
         mysql_free_result($result);

      }

      public function get_sales() {
         global $smarty;
         $this->loadSeasons();
         $users    = array();
         $user_ids = '0';
         if (($this->_salesResponsibleLogged) and !($this->_AgentManager)) {
            $user_ids .= $this->_user['Id'];
            $users[$this->_user['Id']] = array(
               'uname' => $this->_user['FullName']
            );
            foreach ($this->_seasons as $i => $item) {
               $users[$this->_user['Id']][$i] = array(
                  'Budget'   => NULL,
                  'Sold'     => NULL,
                  'Invoiced' => NULL
               );
            }
         } else {
            $fields = array(
               NULL,
               // index 0 reserved
               array(
                  'name'    => 'Sales Responsible',
                  'db'      => 'u.Id',
                  'type'    => FieldType::SELECT,
                  'options' => $this->getSalesResponsible()
               ),
               array(
                  'name'    => 'Agent',
                  'db'      => 'ac.Id',
                  'type'    => FieldType::SELECT,
                  'options' => $this->getAgents()
               )
            );

            $queryFields = "u.Id, CONCAT(u.FirstName, ' ', u.LastName) AS FullName, ac.Name as AgentName";
            $queryTables = '(`company` c, `user` u) left join company ac on ac.id=c.AgentId and c.AgentId>0';
            $queryClause = sprintf("u.id=c.SalesUserId and c.tocompanyid=%d and c.TypeCustomer=1 and c.ListHidden = 0 and c.Active=1 and u.Active=1", $this->_user['CompanyId'])
				 . (($this->_AgentManager) ? sprintf(" and c.AgentId=%d and u.AgentCompanyId=c.AgentId ", $this->_user['AgentCompanyId'])  : '') ;

            $queryOrder  = 'u.FirstName, u.LastName';
            $queryGroup  = 'c.SalesUserId'; //, c.AgentId';
            $result      = listLoad($fields, $queryFields, $queryTables, $queryClause, $queryOrder, $queryGroup);
            while ($row = dbFetch($result)) {
               $user_ids .= ',' . $row['Id'];
               $users[$row['Id']] = array(
                  'uname' => $row['FullName'],
                  'aname' => $row['AgentName']
               );
               foreach ($this->_seasons as $i => $item) {
                  $users[$row['Id']][$i] = array(
                     'Budget'   => NULL,
                     'Sold'     => NULL,
                     'Invoiced' => NULL
                  );
               }
            }
            dbQueryFree($result);
         }
         $this->get_list_by_users($users, $user_ids);
         $nid = navigationMark("sales.budget.editbudget");
         listFilterBar();
         $smarty->assign(
            array(
                 'headers'      => $this->_seasons,
                 'users'        => $users,
                 'nid'          => $nid,
                 'currencyId'   => 1,
                 'currencyName' => $this->_currency_name,
                 'total'        => $this->_total
            ));
         $smarty->display('modules/sales/budget/budget_user.tpl');
         return 0;
      }

      public function get_customer() {
         global $smarty;
         $this->loadSeasons();
         $companies = array();
         if (($this->_salesResponsibleLogged) and !($this->_AgentManager)) {
            $fields = array(
               NULL,
               // index 0 reserved
               array(
                  'name' => 'Company',
                  'db'   => 'c.Name',
                  'type' => FieldType::STRING
               )
            );
         } else {
            $fields = array(
               NULL,
               // index 0 reserved
               array(
                  'name' => 'Customer',
                  'db'   => 'c.Name',
                  'type' => FieldType::STRING
               ),
               array(
                  'name'    => 'Sales Responsible',
                  'db'      => 'u.Id',
                  'type'    => FieldType::SELECT,
                  'options' => $this->getSalesResponsible(false)
               ),
               array(
                  'name'    => 'Agent',
                  'db'      => 'ac.Id',
                  'type'    => FieldType::SELECT,
                  'options' => $this->getAgents()
               )
            );
         }
         $queryFields = "c.Id, c.Name, CONCAT(u.FirstName, ' ', u.LastName) AS FullName, c.CurrencyId, m.Name AS CurrencyName, ac.Name as AgentName";
         $queryTables = '`company` c left join `user` u on u.Id=c.SalesUserId left join `currency` m on m.Id=c.CurrencyId left join company ac on ac.id=c.AgentId and c.AgentId>0';
         $queryClause = sprintf("c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 and c.ToCompanyId=%d", $this->_user['CompanyId'])
            . (($this->_salesResponsibleLogged and !($this->_AgentManager)) ? sprintf(" and c.SalesUserId=%d", $this->_user['Id']) : '')
			 . (($this->_AgentManager) ? sprintf(" and c.AgentId=%d  and u.AgentCompanyId=c.AgentId ", $this->_user['AgentCompanyId'])  : '') ;
         $queryOrder  = 'c.Name';
         $result      = listLoad($fields, $queryFields, $queryTables, $queryClause, $queryOrder);
         while ($row = dbFetch($result)) {
            $companies[$row['Id']] = array(
               'cname'        => $row['Name'],
               'uname'        => $row['FullName'],
               'aname'        => $row['AgentName'],
               'currencyid'   => $row['CurrencyId'],
               'currencyname' => $row['CurrencyName']
            );
            foreach ($this->_seasons as $i => $item) {
               $companies[$row['Id']][$i] = array(
                  'Budget'   => NULL,
                  'Sold'     => NULL,
                  'Invoiced' => NULL
               );
            }
         }
         dbQueryFree($result);
         $this->get_list_by_companies($companies);
         $nid = navigationMark("sales.budget.editbudget");
         listFilterBar();
         $smarty->assign(
            array(
                 'headers'      => $this->_seasons,
                 'companies'    => $companies,
                 'nid'          => $nid,
                 'currencyName' => $this->_currency_name,
                 'total'        => $this->_total
            ));
         $smarty->display('modules/sales/budget/budget_customer.tpl');
         return 0;
      }

      //Load recent 5 Seasons
      private function loadSeasons() {
         $this->_season_ids = '';
         $this->_seasons    = array();
         $this->_total      = array();
		 
		$query = sprintf("select `season`.Id as Id, `season`.Name 
							from `season` 
							left join `user` u on u.id=%d and u.Active=1
							left join `seasonallowuser` sau on sau.seasonid=`season`.id and sau.userid=u.id and sau.active=1
							where `season`.Active=1 and `season`.ShowOnBudget=1 and `season`.OwnerId=%d 
							and (u.agentcompanyid=0 or (`season`.RestrictAccess=0 or (`season`.RestrictAccess=1 and sau.id>0)))
							order by SeasonDisplayOrder Desc LIMIT 8", (int)$this->_user['Id'], $this->_user['CompanyId']);
//         $query             = sprintf('select Id, Name from `season` where Active=1 and ShowOnBudget=1 and OwnerId=%d ORDER BY SeasonDisplayOrder Desc LIMIT 8', $this->_user['CompanyId']);
         $result            = mysql_query(stripslashes($query));
         while ($row = mysql_fetch_assoc($result)) {
            $this->_season_ids          = ($this->_season_ids == '')
               ? $row['Id']
               : $this->_season_ids . ',' . $row['Id'];
            $this->_seasons[$row['Id']] = $row['Name'];
            $this->_total[$row['Id']]   = array(
               'Budget'   => 0,
               'Sold'     => 0,
               'Invoiced' => 0
            );
         }
         mysql_free_result($result);
      }

      private function get_list_by_companies(&$companies) {
         global $filterValue;
         $filterClaus = $this->genClauseItem('c.Name', $filterValue[1])
            . ((($this->_salesResponsibleLogged) and !($this->_AgentManager))
               ? sprintf("and c.SalesUserId=%d ", $this->_user['Id'])
               : ($this->genClauseItem('u.Id', $filterValue[2]) . $this->genClauseItem('ac.Id', $filterValue[3])))
			. (($this->_AgentManager) ? sprintf(" and c.AgentId=%d  and u.AgentCompanyId=c.AgentId ", (int)$this->_user['AgentCompanyId']) : '')
//               : (($this->_AgentManager) ? sprintf(" and c.AgentId=%d ", (int)$this->_user['AgentCompanyId']) : $this->genClauseItem('u.Id', $filterValue[2]) . $this->genClauseItem('ac.Id', $filterValue[3]))
			  ;
         $query = "select c.Id, s.Id as 'SeasonId', b.BudgetSum, m.Rate from (`company` c,`season` s) "
            . 'left join `budget` b on b.CompanyId=c.Id and b.SeasonId=s.id and b.CurrencyId=c.CurrencyId '
            . 'left join `user` u on u.Id=c.SalesUserId '
            . 'left join `currency` m on m.Id = b.CurrencyId '
            . 'left join company ac on ac.id=c.AgentId '
            . sprintf('where s.Id in (%s) and c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 and c.ToCompanyId=%d and b.BudgetSum is not null ', $this->_season_ids, $this->_user['CompanyId'])
            . $filterClaus
            . 'order by c.Name';
         $result      = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $companies[$row['Id']][$row['SeasonId']]['Budget'] = $row['BudgetSum'];
            $this->_total[$row['SeasonId']]['Budget'] += $row['BudgetSum'] * $row['Rate'] / $this->_currency_rate;
         }
         mysql_free_result($result);

         $query  = "select c.Id, s.Id as 'SeasonId', sum((100-ol.Discount)*ol.PriceSale*ol.Quantity*m.Rate)*0.01 as 'Sold', m.Rate 
		       from (`company` c,`season` s) "
            . 'left join `order` o on o.CompanyId = c.Id and o.SeasonId = s.Id '
            . 'left join `user` u on u.Id=c.SalesUserId '
            . 'left join `orderline` ol on ol.OrderId = o.Id '
            . 'left join `currency` m on m.Id = o.CurrencyId '
            . 'left join company ac on ac.id=c.AgentId '
            . sprintf('where s.Id in (%s) and c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 and c.ToCompanyId=%d and (o.proposal=0 or o.ready=1) and o.Active=1 and ol.active=1 and o.OrderDoneId in (0,4,5,6,8) ', $this->_season_ids,
               $this->_user['CompanyId'])
            . $filterClaus
            . 'group by c.id, s.id '
            . 'order by c.Name';
//echo $query ;			
         $result = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $companies[$row['Id']][$row['SeasonId']]['Sold'] = $row['Sold'] / $this->_currency_rate;
            $this->_total[$row['SeasonId']]['Sold'] += $row['Sold'] / $this->_currency_rate;
         }
         mysql_free_result($result);

         $query  = "select c.Id, s.Id as 'SeasonId', sum((100-il.Discount)*il.PriceSale*il.Quantity*(1-2*cast(i.Credit as signed integer))*m.Rate)*0.01 as 'Invoiced', m.Rate 
		      from (`company` c,`season` s) "
            . 'left join `order` o on o.CompanyId = c.Id and o.SeasonId = s.Id '
            . 'left join `user` u on u.Id=c.SalesUserId '
            . 'left join `orderline` ol on ol.OrderId = o.Id '
            . 'left join `invoiceline` il on il.OrderLineId = ol.Id '
            . 'left join `invoice` i on i.Id = il.InvoiceId '
            . 'left join `currency` m on m.Id = i.CurrencyId '
            . 'left join company ac on ac.id=c.AgentId '
            . sprintf('where s.Id in (%s) and c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 and c.ToCompanyId=%d and i.Done=1 and i.Ready=1 ', $this->_season_ids, $this->_user['CompanyId'])
            . $filterClaus
            . 'group by c.id, s.id '
            . 'order by c.Name';
//echo $query ;			
         $result = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $companies[$row['Id']][$row['SeasonId']]['Invoiced'] = $row['Invoiced'] / $this->_currency_rate ;
            $this->_total[$row['SeasonId']]['Invoiced'] += $row['Invoiced'] / $this->_currency_rate;
//            $this->_total[$row['SeasonId']]['Invoiced'] += $row['Invoiced'] * $row['Rate'] / $this->_currency_rate;
         }
         mysql_free_result($result);
      }

      private function get_list_by_users(&$users, $user_ids) {
         $query  = "select u.Id, s.Id as 'SeasonId', b.BudgetSum from (`user` u,`season` s) "
            . 'left join `budget` b on b.UserId = u.Id and b.SeasonId = s.id and b.active=1 '
            . sprintf('where u.id in (%s) and s.Id in (%s) and b.BudgetSum is not null and b.CurrencyId=1 ', $user_ids, $this->_season_ids)
            . 'order by u.FirstName, u.LastName';
         $result = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $users[$row['Id']][$row['SeasonId']]['Budget'] = $row['BudgetSum'];
            $this->_total[$row['SeasonId']]['Budget'] += $row['BudgetSum'];
         }
         mysql_free_result($result);
         $query  = "select u.Id, s.Id as 'SeasonId', sum((100-ol.Discount)*ol.PriceSale*ol.Quantity*m.Rate)*0.01 as 'Sold' from (`user` u,`season` s) "
            . 'left join `company` c on c.SalesUserId=u.Id and c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 '
            . 'left join `order` o on o.CompanyId = c.Id and o.SeasonId = s.Id '
            . 'left join `orderline` ol on ol.OrderId = o.Id '
            . 'left join `currency` m on m.Id = o.CurrencyId '
            . sprintf('where u.id in (%s) and s.Id in (%s) and c.ListHidden=0 and c.active=1 and o.Active=1  and (o.proposal=0 or o.ready=1) and ol.active=1 and o.OrderDoneId in (0,4,5,6,8) ', $user_ids, $this->_season_ids)
			. (($this->_AgentManager) ? sprintf(" and c.AgentId=%d  and u.AgentCompanyId=c.AgentId ", (int)$this->_user['AgentCompanyId']) : '')
            . 'group by u.id, s.id '
            . 'order by u.FirstName, u.LastName';
         $result = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $users[$row['Id']][$row['SeasonId']]['Sold'] = $row['Sold'] / $this->_currency_rate;
            $this->_total[$row['SeasonId']]['Sold'] += $row['Sold'] / $this->_currency_rate;
         }
         mysql_free_result($result);
         $query  = "select u.Id, s.Id as 'SeasonId', sum((100-il.Discount)*il.PriceSale*il.Quantity*m.Rate*(1-2*cast(i.Credit as signed integer)))*0.01 as 'Invoiced' from (`user` u,`season` s) "
            . 'left join `company` c on c.SalesUserId=u.Id and c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 '
            . 'left join `order` o on o.CompanyId = c.Id and o.SeasonId = s.Id '
            . 'left join `orderline` ol on ol.OrderId = o.Id '
            . 'left join `invoiceline` il on il.OrderLineId = ol.Id '
            . 'left join `invoice` i on i.Id = il.InvoiceId '
            . 'left join `currency` m on m.Id = i.CurrencyId '
            . sprintf('where u.id in (%s) and s.Id in (%s) and i.Done=1 and i.Ready=1 ', $user_ids, $this->_season_ids)
			. (($this->_AgentManager) ? sprintf(" and c.AgentId=%d  and u.AgentCompanyId=c.AgentId ", (int)$this->_user['AgentCompanyId']) : '')
            . 'group by u.id, s.id '
            . 'order by u.FirstName, u.LastName';
         $result = mysql_query($query);
         while ($row = mysql_fetch_assoc($result)) {
            $users[$row['Id']][$row['SeasonId']]['Invoiced'] = $row['Invoiced'] / $this->_currency_rate;
            $this->_total[$row['SeasonId']]['Invoiced'] += $row['Invoiced'] / $this->_currency_rate;
         }
         mysql_free_result($result);
      }

      public function edit_budget() {
         global $smarty;
         $companyId  = isset($_GET['companyId'])
            ? $_GET['companyId']
            : NULL;
         $userId     = isset($_GET['userId'])
            ? $_GET['userId']
            : NULL;
         $seasonId   = $_GET['seasonId'];
         $currencyId = $_GET['currencyId'];
         $query      = "select Id, BudgetSum from `budget` where SeasonId=$seasonId and CurrencyId=$currencyId and Active=1 ";
         if (isset($companyId)) {
            $query = $query . "and CompanyId=$companyId";
         } else {
            $query = $query . "and UserId=$userId";
         }
         $result = mysql_query($query);
         if ($row = mysql_fetch_assoc($result)) {
            $id        = $row['Id'];
            $budgetSum = $row['BudgetSum'];
         }
         mysql_free_result($result);
         $query  = "select Name from `Currency` where Id=$currencyId";
         $result = mysql_query($query);
         if ($row = mysql_fetch_assoc($result)) {
            $currencyName = $row['Name'];
         }
         mysql_free_result($result);
         $smarty->assign(
            array(
                 'recordId'    => $id,
                 'companyId'   => $companyId,
                 'userId'      => $userId,
                 'seasonId'    => $seasonId,
                 'currencyId'  => $currencyId,
                 'curencyName' => $currencyName,
                 'budgetSum'   => $budgetSum
            ));
         $smarty->display('modules/sales/budget/budget_edit.tpl');
         return 0;
      }

      public function save_cmd() {
         $fields   = array(
            'SeasonId'   => array(
               'type' => 'integer'
            ),
            'CurrencyId' => array(
               'type' => 'integer'
            ),
            'BudgetSum'  => array(
               'type'      => 'integer',
               'mandatory' => true
            )
         );
         $recordId = 0;
         if (isset($_POST['CompanyId'])) {
            $fields['CompanyId'] = array(
               'type' => 'integer'
            );
         } else {
            $fields['UserId'] = array(
               'type' => 'integer'
            );
         }
         if (isset($_POST['recordid'])) {
            $recordId = $_POST['recordid'];
         }
         return saveFields("budget", $recordId, $fields);
      }

      private function genClauseItem($db, $value) {
         $result = '';
         if (!empty($value)) {
            if (strpos($value, '*') === false and strpos($value, '%') === false) {
               $result = sprintf('AND %s="%s" ', $db, addslashes($value));
            } else {
               $result = sprintf('AND CAST((%s) AS CHAR) LIKE "%s" ', $db, addslashes(str_replace('*', '%', $value)));
            }
         }

         return $result;
      }

      private function getSalesResponsible($onlyActive = true) {
         Global $User;
         $query = "select u.Id, CONCAT(u.FirstName, ' ', u.LastName) AS Value from (`company` c, `user` u) "
            . 'where u.id=c.SalesUserId and c.TypeCustomer=1 and c.ListHidden = 0 and c.Active=1 '
            . sprintf('and c.tocompanyid=%d ', $this->_user['CompanyId']) 
			. ($onlyActive ? 'and u.Active=1 ' : '') 
			. ($this->_AgentManager ? sprintf('and u.AgentCompanyId=%d ', $User['AgentCompanyId']) : '')
			. 'group by c.SalesUserId order by u.FirstName, u.LastName';
         return getLookupValuesArray($query);
      }

      private function getAgents() {
         Global $User;
         if (($this->_AgentManager)) {
            $values[$User['AgentCompanyId']] = tableGetField('Company', 'Name', $User['AgentCompanyId']);
            return $values;
         } else {
            $query = sprintf("select ac.Id, ac.Name AS Value
               FROM (`company` c, `company` ac)
               WHERE ac.id=c.AgentId and c.TypeCustomer=1 and c.ListHidden = 0 and c.Active=1 and c.tocompanyid=%d and c.active=1
               GROUP BY ac.Name
               ORDER BY ac.Name", $this->_user['CompanyId']);
            return getLookupValuesArray($query);
         }
      }

      public function get_countries() {
         /* global $smarty, $filterValue;
         $this->loadSeasons();
         $companies = array();
         if($this->_salesResponsibleLogged){
             $fields = array (
                     NULL, // index 0 reserved
                     array ('name' => 'Company',		'db' => 'c.Name', 		'type' => FieldType::STRING)
             );
         }else{
             $fields = array (
                     NULL, // index 0 reserved
                     array ('name' => 'Company',				'db' => 'c.Name', 	'type' => FieldType::STRING),
                     array ('name' => 'Sales Responsible',	'db' => 'u.Id', 	'type' => FieldType::SELECT,	'options' => $this->getSalesResponsible(false))
             );
         }
         $queryFields = "c.Id, c.Name, CONCAT(u.FirstName, ' ', u.LastName) AS FullName, c.CurrencyId, m.Name AS CurrencyName";
         $queryTables = '`company` c left join `user` u on u.Id=c.SalesUserId left join `currency` m on m.Id=c.CurrencyId ';
         $queryClause = sprintf("c.TypeCustomer=1 and c.ListHidden=0 and c.Active=1 and c.ToCompanyId=%d", $this->_user['CompanyId'])
         .($this->_salesResponsibleLogged ? sprintf(" and c.SalesUserId=%d", $this->_user['Id']):'');
         $queryOrder = 'c.Name';
         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, $queryOrder);
         while ($row = dbFetch($result)){
             $companies[$row['Id']] = array('cname' => $row['Name'], 'uname' => $row['FullName'], 'currencyid' => $row['CurrencyId'], 'currencyname' => $row['CurrencyName']);
             foreach($this->_seasons as $i => $item){
                 $companies[$row['Id']][$i] = array('Budget' => NULL, 'Sold' => NULL, 'Invoiced' => NULL);
             }
         }
         dbQueryFree($result);
         $this->get_list_by_companies($companies);
         $nid = navigationMark("sales.budget.editbudget");
         listFilterBar();
         $smarty->assign(array(
                 'headers'		=> $this->_seasons,
                 'companies'		=> $companies,
                 'nid'			=> $nid,
                 'currencyName'	=> $this->_currency_name,
                 'total'			=> $this->_total
         ));
         $smarty->display('modules/sales/budget/budget_customer.tpl'); */
         return 0;
      }
   }

   return 0;
?>