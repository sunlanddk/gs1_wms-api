<?php

require_once  "module/xml/StyleParser.inc" ;
require_once  "styleProcessor.inc" ;

class FileUpload{
    private $skip_errors = false; 
    private $parse_error_count = 0;
    private $process_error_count = 0;
    private $process_error_messages = array();

    private $created_articles_count = 0;
    private $reused_articles_count = 0;
    private $created_collection_members_count = 0;
   
	public function main(){
	   global $smarty;
	   $nid = navigationMark("upload.parsexml");
	   $smarty->assign(
	         array(
	               'nid'          => $nid
	         ));
		$smarty->display('modules/sales/fileupload.tpl');
		return 0;
	}
	
	public function parse_xml() {
		global $nid, $Config;
		$nid = navigationMark("sales.fileupload");
		
		set_time_limit (600) ;
		$uploadStore = $Config['uploadStore'] . '/';
		$name = "file";  
		
		$skipErrors = false;
		if($_POST['skipErrors']) $skipErrors = true;
		$this->skip_errors=$skipErrors;
		
		$file_error = $this->validate_file($name);
		if(empty($file_error)){
			$file_name = $_FILES[$name]["name"];
			$tmp_name = $_FILES["file"]["tmp_name"];
			$file_path = $uploadStore.$file_name;
			
			if (file_exists($file_path)) {
				unlink($file_path);
			}
			
			move_uploaded_file($tmp_name, $file_path);
				
			$style_reader = new StyleParser($file_path, $skipErrors);
			$styles = $style_reader->parse();
			$parse_error_count = $style_reader->get_error_count();
			$parse_error_messages = $style_reader->get_error_messages();
			
			if($skipErrors || $parse_error_count == 0 ){
			   $this->processStyles($styles);

                $this->build_results_table("Import results", array(
                    sprintf("Number of successfully created articles: %s", $this->created_articles_count),
                    sprintf("Number of reused articles: %s", $this->reused_articles_count),
                    sprintf("Number of successfully created collection members: %s", $this->created_collection_members_count)
                ), "tblResult");
			}

            if($this->parse_error_count > 0){
                $this->build_results_table("Parsing Errors", $parse_error_messages);
            }
            if($this->process_error_count > 0){
                $this->build_results_table("Processing Errors", $this->process_error_messages);
            }

			unlink($file_path);	
		} else {
            $this->build_results_table("File Error", array($file_error));
        }

		return 0;
	}
	
	private function build_results_table($table_title, $error_messages, $table_class = "tblError"){
	    global $smarty;
	    $smarty->assign(              array(
                    'title'           => $table_title,
                    'error_messages'  => $error_messages,
                    'table_class'     => $table_class
              )
	    );
	    $smarty->display('modules/sales/import_results.tpl');
	}
	
	private function processStyles($styles){
		foreach ($styles as $style){
			$processor = new StyleProcessor($style, $this->skip_errors);
			$processor->process();
			if($processor->get_error_raised()){
			   $this->process_error_count++;
			   array_push($this->process_error_messages, $processor->get_error_message());
			   if(!$this->skip_errors) break;
			}

            $this->created_articles_count += $processor->get_created_articles_count();
            $this->reused_articles_count += $processor->get_reused_articles_count();
            $this->created_collection_members_count += $processor->get_created_collection_members_count();
		}
	}
	
	private function validate_file($name){
		if( empty($_FILES[$name])){
			return "Error: " . " There is no file uploaded";
		}
		if( $_FILES[$name]["error"]>0){
			return "Error: " . $_FILES["file"]["error"];
		}
		if($_FILES[$name]["type"] != "text/xml"){
			return "Error: " . " Should be XML file.";
		}
		if($_FILES[$name]["size"] == 0){
			return "Error: " . " File is empty.";
		}
		return null;
	}
}
return 0;
?>