<?php
    require_once "cart.inc";

	class StyleUtils {

		public static function get_query_fields(){
			return "distinct a.Id, ac.Id as ColorId, a.Number, col.Number as ColorNumber, a.Description as Name, col.Description as colorName, af.Description as fabricName,
					cmp.WholesalePrice, cmp.RetailPrice, cmp.CampaignPrice, m.Id as CollectionMemberId, s.Id as seasonId, s.Name as seasonName, cl.PreSale as PreSaleState, cl.ShowOnly as ShowOnlyState";
		}
		public static function get_query_tables($allowuserid){
			return sprintf("`season` s
							left join `collection` c on c.SeasonId = s.Id and c.active=1
							left join `collectionmember` m on m.CollectionId = c.Id and m.active=1
							left join `collectionlot` cl on cl.Id = m.CollectionLOTId
							left join `collectionmemberprice` cmp on cmp.CollectionMemberId=m.Id and cmp.active=1
							left join `article` a on a.Id = m.ArticleId
							left join `article` af on af.Id = a.FabricArticleId
							left join `articlecategory` cat on cat.Category=substring(a.Number,2,3)
							left join `articlecolor` ac on ac.id = m.ArticleColorId
							left join `color` col on col.id = ac.ColorId
							left join `collectionmemberprogram` mp on mp.CollectionmemberId = m.Id 
							left join `program` p on mp.programid=p.id
							left join `user` u on u.id=%d and u.Active=1
							left join `seasonallowuser` sau on sau.seasonid=s.id and sau.userid=u.id and sau.active=1", $allowuserid['Id']);
		}

		public static function get_currency($currency_id){
		   $_curr = 0;
		   $query = sprintf("select c.id, c.name, c.symbol from  `currency` c
							where c.Id = %d", $currency_id);
		   $result = mysql_query($query);
		   $_curr = mysql_fetch_assoc($result);
		   mysql_free_result($result);
		   return $_curr;
		}

		public static function get_cmp_currency($company_id){
			$_curr = 0;
			$query = sprintf("select c.id, c.name, c.symbol from `company` p
							left join `currency` c on c.Id = p.CurrencyId
							where p.Id = %d", $company_id);
			$result = mysql_query($query);
			$_curr = mysql_fetch_assoc($result);
	    	mysql_free_result($result);
			return $_curr;
		}

		public static function get_cm_minmax_price($cm_id, $currencyid){
			$_curr = 0;
			$query = sprintf("select max(cmps.wholesaleprice) as MaxWholeSalePrice, min(cmps.wholesaleprice) as MinWholeSalePrice
							from (`collectionmemberprice` cmp, `collectionmemberpricesize` cmps)
							where cmp.CollectionMemberId=%d and cmps.collectionmemberpriceid=cmp.id and currencyid=%d and cmp.active=1 and cmps.active=1", $cm_id, $currencyid);
			$result = mysql_query($query);
			$_curr = mysql_fetch_assoc($result);
	    	mysql_free_result($result);
			return $_curr;
		}

		public static function get_filters($company_id, $allowuserid=0){
		    global $listSettings;
		    $_seasonId = isset($_GET['filter']['Season']) ? $_GET['filter']['Season'] : $listSettings['filter']['Season']['id'];

			require_once 'lib/defaults.inc' ;
			require_once 'lib/lookups.inc' ;

			$_def_cat = getArticleCategory($Defaults['articleCategoryId']);
			$_cat_clause = sprintf('ifnull(cat.ArticleCategoryGroupId, %d)', $_def_cat['ArticleCategoryGroupId']);
			
			// List field specification
		    $fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Number',		'nosort' =>1, 'db' => 'a.Number',		'type' => FieldType::STRING),
				array ('name' => 'Name',		'nosort' =>1, 'db' => 'a.Description',		'type' => FieldType::STRING),
				array ('name' => 'Season',		'nosort' =>1, 'db' => 's.Id', 			'type' => FieldType::SELECT,	'options' => getFilterOwnerSeasons($company_id, $allowuserid)),
//				array ('name' => 'Type',		'nosort' =>1, 'db' => $_cat_clause, 	'type' => FieldType::SELECT,	'options' => getArticleCategories($company_id)),
//				array ('name' => 'Sex',			'nosort' =>1, 'db' => 'ac.SexId',		'type' => FieldType::SELECT,	'options' => getArticleSex()),
//				array ('name' => 'Program',		'nosort' =>1, 'db' => 'mp.ProgramId', 	'type' => FieldType::SELECT, 	'options' => getPrograms($_seasonId)),
//				array ('name' => 'Program',		'nosort' =>1, 'db' => 'p.Name', 	'type' => FieldType::SELECT, 	'options' => getProgramNames($_seasonId)),
//				array ('name' => 'Brand',		'nosort' =>1, 'db' => 'c.BrandId', 		'type' => FieldType::SELECT,	'options' => getBrands())
		    );

		    return $fields;
		}

		public static function get_article_qnty_from_cart($cart_name, $season_id, $article_id, $color_id, $size_id){
		    $usercart = str_replace('\\', '', Cart::getCart($cart_name));
			$cart = json_decode($usercart['Cart'], true);
			if(isset($cart)){
				if (array_key_exists($season_id, $cart)) {
					foreach ($cart[$season_id]['items'] as $item) {
		               if (((int)$item['ArticleId'] == $article_id) && ((int)$item['ArticleColorId'] == $color_id)) {
		               		foreach ($item['Sizes'] as $key => $value) {
		               			if((int)$key == $size_id){
		               				return $value['qty'];
		               			}
		               		}
		               }
		            }
				}
			}
			return 0;
		}

		public static function is_article_in_cart($cart_name, $season_id, $article_id, $color_id){
		    $usercart = str_replace('\\', '', Cart::getCart($cart_name));

			$cart = json_decode($usercart['Cart'], true);
			if(isset($cart)){
				if (array_key_exists($season_id, $cart)) {
					foreach ($cart[$season_id]['items'] as $item) {
		               if (((int)$item['ArticleId'] == $article_id) && ((int)$item['ArticleColorId'] == $color_id)) {
		               		return 1;
		               }
		            }
				}
			}
			return 0;
		}
		
		public static function get_user_company_id($_user){
			$user_company_id = 0;
			if ($_user['Extern']) {
				$user_company_id = TableGetField('Company', 'ToCompanyId', $_user['CompanyId']) ;
			} else {
				$user_company_id = $_user['CompanyId'];
			}
			return $user_company_id;
		}

	}

	return 0;
?>