<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    // Get Article number
    $ArticleNumber = stripslashes($_POST['Number']) ;

    // Find the Article
    $query = sprintf ('SELECT Article.*, ArticleType.Product AS ArticleTypeProduct FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Number="%s" AND Article.Active=1', addslashes($ArticleNumber)) ;
    $res = dbQuery ($query) ;
    $NewArticle = dbFetch ($res) ;
    dbQueryFree ($res) ;
    if (!$NewArticle) return sprintf ('Article not found, number "%s"', $ArticleNumber) ;
    
    // Validate ArticleType
    if (!$NewArticle['ArticleTypeProduct']) return ('Article is not a Product for Production') ;

    // Copy Style
    $query = sprintf ("SELECT Style.* FROM Style WHERE Style.Id=%d AND Style.Active=1", $Id) ;
    $res = dbQuery ($query) ;
    $NewStyle = dbFetch ($res) ;
    dbQueryFree ($res) ;
    unset ($NewStyle['PatternMade']) ;
    unset ($NewStyle['PatternMadeUserId']) ;
    unset ($NewStyle['PatternMadeDate']) ;
    unset ($NewStyle['Ready']) ;
    unset ($NewStyle['ReadyUserId']) ;
    unset ($NewStyle['ReadyDate']) ;
    unset ($NewStyle['Approved']) ;
    unset ($NewStyle['ApprovedUserId']) ;
    unset ($NewStyle['ApprovedDate']) ;   
    $NewStyle['ArticleId'] = (int)$NewArticle['Id'] ;

    // Lock Style
    $query = sprintf ('LOCK TABLES Style WRITE') ;
    dbQuery ($query) ;
     
    // Get next Style version
    $query = sprintf ('SELECT MAX(Version) AS Version FROM Style WHERE Style.ArticleId=%d AND Style.Active=1', $NewArticle['Id']) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    $NewStyle['Version'] = $row['Version'] + 1 ;

    // Create new version of Style
    $NewStyle['Id'] = tableWrite ('Style', $NewStyle) ;

    // Unlock Style
    $query = sprintf ('UNLOCK TABLES') ;
    dbQuery ($query) ;
        
    // Copy sketch
    require_once 'lib/file.inc' ;
    if (is_file(fileName('sketch', $Id))) {
	copy (fileName('sketch',$Id), fileName('sketch',$NewStyle['Id'], true)) ;
	chmod (fileName('sketch',$NewStyle['Id']), 0640) ;
    }

    // Copy all operations
    $query = sprintf ("SELECT * FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No", $Id) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	$row['StyleId'] = $NewStyle['Id'] ;
	tableWrite ('StyleOperation', $row) ;
    }
    dbQueryFree ($result) ;

    // Make log
    $s = sprintf ('Created by copying Style %s-%d', $Record['Number'], $Record['Version']) ;
    $row = array (
	'StyleId'	=> $NewStyle['Id'],
	'Header'	=> $s,
	'Text'		=> $s
    ) ;
    $LogId = tableWrite ('StyleLog', $row) ;

    // Find newest PDM version		
    $query = sprintf ('SELECT MAX(Version) as Version FROM StylePDM WHERE StyleId=%d AND Active=1', $Id) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    $version = $row['Version'] ;
    if ($version > 0) {
	// Get newest PDM from version number
	$query = sprintf ("SELECT * FROM StylePDM WHERE StyleId=%d AND Version=%d AND Active=1", $Id, $version) ;
	$res = dbQuery ($query) ;
	$NewPDM = dbFetch ($res) ;
	dbQueryFree ($res) ;
	$CopyPDMId = (int)$NewPDM['Id'] ;

	// Copy PDM
	$NewPDM['Version'] = 1 ;
	$NewPDM['StyleId'] = $NewStyle['Id'] ;
	$NewPDM['StyleLogId'] = $LogId ;
	$NewPDM['Id'] = tableWrite ('StylePDM', $NewPDM) ;
	copy (fileName('pdm',$CopyPDMId), fileName('pdm',$NewPDM['Id'], true)) ;
	chmod (fileName('pdm',$NewPDM['Id']), 0640) ;
    }


    return navigationCommandMark ('styleview', $NewStyle['Id']) ;
?>
