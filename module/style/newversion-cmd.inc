<?php

    define ('DEBUG', 0) ;

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/article/include.inc' ;

    // Get Style Template
    $query = sprintf ('SELECT Style.* FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Approved DESC, Style.Ready DESC, Style.Version DESC LIMIT 1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $StyleTemplate = dbFetch ($result) ;
    dbQueryFree ($result) ;

    if ((int)$StyleTemplate['Id'] > 0) {
	// Get PDM record
	$query = sprintf ("SELECT StylePDM.* FROM StylePDM WHERE StylePDM.StyleId=%d AND StylePDM.Active=1 ORDER BY StylePDM.Version DESC LIMIT 1", (int)$StyleTemplate['Id']) ;
	$res = dbQuery ($query) ;
	$StylePDMTemplate = dbFetch ($res) ;
	dbQueryFree ($res) ;	    

	// Generate Log Text
	$s = sprintf ("This version has been generated from version %d", $StyleTemplate['Version']) ;
	if ((int)$StylePDMTemplate['Id'] > 0) $s .= sprintf ("\nPDM Version %d", $StylePDMTemplate['Version']) ;
	if ($StyleTemplate['Ready']) $s .= sprintf ("\nApproved by constructor, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ReadyUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ReadyDate']))  ) ;
	if ($StyleTemplate['Approved']) $s .= sprintf ("\nApproved by sales, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ApprovedUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ApprovedDate']))) ;
	
	// Update Style Template
	$Style = $StyleTemplate ;
//	$Style['Version'] = (int)$Record['Version'] ;
	$Style['PatternMade'] = 0 ;
	$Style['Ready'] = 0 ;
	$Style['Approved'] = 0 ;

    	if (DEBUG) printf ("Style Id %d, Version %d, Approved %d, Ready %d, PDM Version %d<br>\n", (int)$StyleTemplate['Id'], (int)$StyleTemplate['Version'], $StyleTemplate['Approved'], $StyleTemplate['Ready'], (int)$StylePDMTemplate['Version']) ;

    } else {
	// Create new Style
	$Style = array () ;
	$Style['ArticleId'] = (int)$Record['Id'] ;
//	$Style['Version'] = 1 ;

	// Generate Log Text
	$s = 'New empty Style' ;
    }

    // Lock Style
    $query = sprintf ('LOCK TABLES Style WRITE') ;
    dbQuery ($query) ;
     
    // Get next Style version
    $query = sprintf ('SELECT MAX(Version) AS Version FROM Style WHERE Style.ArticleId=%d AND Style.Active=1', $Id) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    $Style['Version'] = $row['Version'] + 1 ;

    // Create new version of Style
    $Style['Id'] = tableWrite ('Style', $Style) ;

    // Unlock Style
    $query = sprintf ('UNLOCK TABLES') ;
    dbQuery ($query) ;

    if (DEBUG) logPrintVar ($Style, 'Style') ;
    
    // Generate Log
    $StyleLog = array (
	'StyleId' => $Style['Id'],
	'Header' => 'Created',
	'Text' => $s
    ) ;
    $StyleLog['Id'] = tableWrite ('StyleLog', $StyleLog) ;
    if (DEBUG) logPrintVar ($StyleLog, 'StyleLog') ;

    if ((int)$StyleTemplate['Id'] > 0) {
	// Copy sketch
	if (is_file(fileName('sketch', (int)$StyleTemplate['Id']))) {
	    copy (fileName('sketch',(int)$StyleTemplate['Id']), fileName('sketch',$Style['Id'], true)) ;
	    chmod (fileName('sketch',$Style['Id']), 0640) ;
	}

	// Copy all operations
	$query = sprintf ('SELECT * FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No', (int)$StyleTemplate['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['StyleId'] = (int)$Style['Id'] ;
	    $row['Id'] = tableWrite ('StyleOperation', $row) ;
	    if (DEBUG) logPrintVar ($row, 'StyleOperation') ;
	}
	dbQueryFree ($result) ;
    }

    if ((int)$StylePDMTemplate['Id'] > 0) {
	// Create new PDM record
	$StylePDM = $StylePDMTemplate ;
	$StylePDM['Version'] = 1 ;
	$StylePDM['StyleId'] = $Style['Id'] ;
	$StylePDM['StyleLogId'] = $StyleLog['Id'] ;
	$StylePDM['Id'] = tableWrite ('StylePDM', $StylePDM) ;
	if (DEBUG) logPrintVar ($StylePDM, 'StylePDM') ;

	// Copy PDM Scheed
	copy (fileName('pdm',(int)$StylePDMTemplate['Id']), fileName('pdm',(int)$StylePDM['Id'], true)) ;
	chmod (fileName('pdm',(int)$StylePDM['Id']), 0640) ;
    }

    // View new version of Style
    return navigationCommandMark ('styleview', (int)$Style['Id']) ;
?>
