<?php

    require_once 'lib/file.inc' ;

   
    if (((int)$_GET['newsketch']) > 0) {
		$query = sprintf('SELECT ss.documentid as id FROM StylePageVersion p 
			INNER JOIN StyleSketchVersion s ON p.PageId = s.Id AND styleId = %d
			LEFT JOIN StyleSketch ss ON ss.StyleSketchVersionId = s.Id
			WHERE p.Current=1 AND p.PageTypeId = 2 AND ss.StyleSketchCategoryId = 1', 
						$Id);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
	    // Make filename
	    $file = fileName ('stylesketch', $row['id']) ;
	    $file .= "thumb" ;
//	    $file = fileName ('stylesketch', $Id) ;
//	    $file .= "thumb" ;
    } else {
	// Make filename
	    $file = fileName ('sketch', $Id) ;
    }
    	

    // Check for file existance
    if (!is_file($file)) return 'No Style Sketch' ;

    // No caching
    switch ($_SERVER["SERVER_PROTOCOL"]) {
	case "HTTP/1.1":
	    header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	    break ;
	
	case "HTTP/1.0":
	    header ("Pragma: no-cache") ; 
	    break ;

	default:
	    return sprintf ("%s(%d) invalid server protocol %s", __FILE__, __LINE__, $_SERVER["SERVER_PROTOCOL"]) ;
    }

    // HTTP Headers
//    $size = filesize($file) ;
//    header ('Content-Length: '.$size) ;		// Can't be used when compressing
    header ('Content-Type: '.$Record['SketchType']) ;
    header ('Last-Modified: '.date("D, j M Y G:i:s T", dbDateDecode($Record['SketchDate'])));

    // Output file
    readfile ($file) ;

    return 0 ;

?>
