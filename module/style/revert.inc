<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    
    // Header
    printf ("<br><table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</td></tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Style version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    printf ("</table><br>\n") ;
  
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;

    function htmlFlagRevert (&$Record, $field, $name=NULL) {
	if (is_null($name)) $name = $field ;
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $name) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td><input type=checkbox name=%s style='margin-left:4px;' checked> %s, %s</td>", $field, date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td class=itemfield>no</td>") ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }

    print htmlFlagRevert ($Record, 'PatternMade', 'Pattern made') ;
    print htmlFlagRevert ($Record, 'Ready', 'Cons approve') ;
    print htmlFlagRevert ($Record, 'Approved', 'Sale approve') ;

    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
   
    return 0 ;
?>
