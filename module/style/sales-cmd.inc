<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Approved'		=> array ('type' => 'checkbox',	'check' => true),
	'ApprovedUserId'	=> array ('type' => 'set'),
	'ApprovedDate' 		=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Approved':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

		// Validate Approval
		if (!$Record['Ready']) return "The Style must be Ready before Approval" ;
		
		// Check if other versions of style has been approved
		$query = sprintf ('SELECT Style.Id, Style.Version FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 AND Style.Approved=1 AND Style.Id<>%d', (int)$Record['ArticleId'], (int)$Record['Id']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row) return sprintf ('version %d of the Style for this Article is allready Approved', (int)$row['Version']) ;

		// Set tracking information
		$fields['ApprovedUserId']['value'] = $User['Id'] ;
		$fields['ApprovedDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }
    
    return saveFields ('Style', $Id, $fields, true) ;
?>
