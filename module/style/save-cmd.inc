<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'SplitOperationNo'	=> array ('type' => 'integer'),
	'PatternMade'		=> array ('type' => 'checkbox',	'check' => true),
	'PatternMadeUserId'	=> array ('type' => 'set'),
	'PatternMadeDate' 	=> array ('type' => 'set'),
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate' 		=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'PatternMade':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

		// Set tracking information
		$fields['PatternMadeUserId']['value'] = $User['Id'] ;
		$fields['PatternMadeDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Ready':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

		// Validate that at least one PDM is pressent
		if ($Record['PDMVersion'] == 0) return 'no PDM available' ;

		// No edit locks for PDMs allowed
		if ($Record['TaskId'] > 0) return 'the PDM is currently locked for editing' ;

		// Must have some sewing operations
		if ($Record['OperationCount'] <= 0) return 'the Style has no sewing operations' ;

		// Sketch must be available
		if ($Record['SketchUserId'] == 0) return 'no Sketch available' ;

		// Sketch must be available
		// if ($Record['PatternMade'] == 0) return 'Patterns has not been made' ;

		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }

    // Check if document upload was succesfull
    switch ($_FILES['Doc']['error']) {
	case 0: 
	    if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		return 'sketch upload failed, file not uploaded' ;
//	    if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
//		return 'invalid sketch size' ;
	    if ($_FILES['Doc']['size'] > 20000)
		return 'sketch too big, max 20 kB' ;

	    // Set tracking information
	    $fields['SketchUserId'] = array('type' => 'set', 'value' => $User['Id']) ;
	    $fields['SketchDate'] = array('type' => 'set', 'value' => dbDateEncode(time())) ;
	    $fields['SketchType'] = array('type' => 'set', 'value' => $_FILES['Doc']['type']) ;

	    // Set flag for Sketch upload	
	    $SketchUpload = true ;		    
	    break ;
	    
	case 4: 		// No file specified
	    $SkecthUpload = false ;
	    break ;
	    
	default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
    }

    if ($Record['Ready']) return "no modifications to a Style beeing Ready" ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Set Style basic information
	    $fields['ArticleId'] = array('type' => 'set', 'value' => (int)$Record['Id']) ;
	    $fields['Version'] = array('type' => 'set', 'value' => (int)$Record['Version']) ;

	    // Verify that this version of the style has been created while editing
	    $query = sprintf ("SELECT Style.Id FROM Style WHERE Style.ArticleId=%d AND Style.Version=%d AND Style.Active=1", (int)$Record['Id'], (int)$Record['Version']) ;
	    $result = dbQuery ($query) ;
	    if (dbNumRows ($result) > 0) return ('This version of the style has been created by an other User') ;
	    dbQueryFree ($result) ;
 	    
	    $Id = -1 ;
	    break ;
    }
    
    // Save record
    $res = saveFields ('Style', $Id, $fields) ;
    if ($res) return $res ;
    if ($Id <= 0) $Id = dbInsertedId () ;
 
    if ($SketchUpload) {
	// Archive sketch
	require_once 'lib/file.inc' ;
	$file = fileName ('sketch', $Id, true) ;
	if (!move_uploaded_file ($_FILES['Doc']['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive document', __FILE__, __LINE__) ;
	chmod ($file, 0640) ;
    }
     
    return 0 ;    
?>
