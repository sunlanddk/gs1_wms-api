<?php

    require_once 'lib/table.inc' ;

    if ($StyleUsage > 0) return "This Style is used in ProductionOrders and can not be Deleted" ;

    tableDelete ('Style', $Id) ;
    tableDeleteWhere ('StyleOperation', sprintf ("StyleId=%d", $Id)) ;
    tableDeleteWhere ('StylePDM', sprintf ("StyleId=%d", $Id)) ;
    tableDeleteWhere ('StyleLog', sprintf ("StyleId=%d", $Id)) ;

    return 0
?>
