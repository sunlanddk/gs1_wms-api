<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'module/style/include.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    switch ($Navigation['Parameters']) {
		case 'Case' :
		    // Get parent Article Id from Case
		    $Id = tableGetField ('`Case`', 'ArticleId', $Id) ;
		    break ;	
	    }
	    
	    // Get parent Article
	    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d AND Article.Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // List specification
	    $queryFields = 'Style.*, CONCAT(CreateUser.FirstName," ",CreateUser.LastName," (",CreateUser.Loginname,")") AS CreateUserName, CONCAT(ReadyUser.FirstName," ",ReadyUser.LastName," (",ReadyUser.Loginname,")") AS ReadyUserName, CONCAT(ApprovedUser.FirstName," ",ApprovedUser.LastName," (",ApprovedUser.Loginname,")") AS ApprovedUserName, ' . $styleStateField . ' AS State' ;
	    $queryTables = 'Style LEFT JOIN User AS CreateUser ON CreateUser.Id=Style.CreateUserId LEFT JOIN User AS ReadyUser ON ReadyUser.Id=Style.ReadyUserId LEFT JOIN User AS ApprovedUser ON ApprovedUser.Id=Style.ApprovedUserId'  ;
	    $queryClause = sprintf ('Style.ArticleId=%d AND Style.Active=1', $Id) ;
	    $queryOrder	= 'Style.Version' ;	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad (NULL, $queryFields, $queryTables, $queryClause, $queryOrder) ;
	    if (is_string($Result)) return $Result ;

	    return 0 ;

	case 'newversion-cmd' :
	    // Get parent Article
	    $query = sprintf ("SELECT Article.*, ArticleType.Product AS ArticleTypeProduct FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Id=%d AND Article.Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Validate ArticleType
	    if (!$Record['ArticleTypeProduct']) return ('Article is not a Product for Production 1') ;
	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	case 'delete-cmd' :
	    // Check if Style is used in Approved Productions
	    $query = sprintf ("SELECT Id FROM Production WHERE StyleId=%d AND Active=1 AND Done=0", $Id) ;
	    $result = dbQuery ($query) ;
	    $StyleUsage = dbNumRows ($result) ;
	    dbQueryFree ($result) ;

	    // Navigation
	    if ($StyleUsage > 0) {
		navigationPasive ('delete') ;
	    }
	    
	    // Fall through

	case 'view' :
	case 'sketch' :
	case 'copy' :
	case 'copy-cmd' :
	case 'sales' :
	case 'sales-cmd' :
	case 'revert' :
	case 'revert-cmd' :
	case 'select-cmd' :
	    // Get Id for PDMLock TaskType
	    require_once 'lib/table.inc' ;
	    $TaskTypeId = tableMark2Id ('TaskType', 'pdmlock') ;

	    // Get Style
    if (((int)$_GET['newsketch']) > 0) {
	    $query = sprintf ("SELECT Style.*, %s AS State, Article.Number, Article.Description, Task.Id AS TaskId, Task.UserId AS TaskUserId, Task.CreateDate AS TaskCreateDate, Task.EndDate AS TaskEndDate, ArticleType.Product AS ArticleTypeProduct 
FROM Style 
LEFT JOIN Article ON Article.Id=Style.ArticleId 
LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId 
LEFT JOIN Task ON Task.RefId=Style.Id AND Task.TaskTypeId=%d AND Task.Active=1 AND Task.Done=0 
WHERE Style.Id=%d", $styleStateField, $TaskTypeId, $Id) ;
    } else {
	    $query = sprintf ("SELECT Style.*, %s AS State, Article.Number, Article.Description, Task.Id AS TaskId, Task.UserId AS TaskUserId, Task.CreateDate AS TaskCreateDate, Task.EndDate AS TaskEndDate, ArticleType.Product AS ArticleTypeProduct 
FROM Style LEFT JOIN Article ON Article.Id=Style.ArticleId 
LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId 
LEFT JOIN Task ON Task.RefId=Style.Id AND Task.TaskTypeId=%d AND Task.Active=1 AND Task.Done=0 
WHERE Style.Id=%d AND Style.Active=1", $styleStateField, $TaskTypeId, $Id) ;
    }
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Nagivation to Parent: set Id to use
	    $Record['ParentId'] = $Record['ArticleId'] ;

	    // Validate ArticleType
    if (!$Record['ArticleTypeProduct']) return (sprintf('Article is not a Product for Production art id: %d, type %d, style %d',$Record['ArticleId'] ,$Record['ArticleTypeProduct'], $Id)) ;

	    // Get PDM information
	    $query = sprintf ('SELECT MAX(Version) as PDMVersion FROM StylePDM WHERE StyleId=%d AND Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = array_merge ($Record, dbFetch ($res)) ;
	    dbQueryFree ($res) ;
	    if ($Record['PDMVersion'] > 0) {
		$query = sprintf ('SELECT StylePDM.CreateDate AS PDMCreateDate, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS PDMCreateUser FROM StylePDM LEFT JOIN User ON User.Id=StylePDM.CreateUserId WHERE StylePDM.StyleId=%d AND StylePDM.Version=%d AND StylePDM.Active=1', $Id, $Record['PDMVersion']) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		$Record = array_merge ($Record, $row) ;
	    }

	    // Get Operation information
	    $query = sprintf ('SELECT SUM(ProductionMinutes) as OperationProductionMinutes, COUNT(Id) AS OperationCount FROM StyleOperation WHERE StyleId=%d AND Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = array_merge ($Record, dbFetch ($res)) ;
	    dbQueryFree ($res) ;

	    // Navigation
	    if ($Record['PDMVersion'] <= 0) {
		navigationPasive ('viewpdm') ;
	    }

	    if ($Record['Ready']) {
		navigationPasive ('edit') ;
	    }

	    if (!$Record['Ready'] or $Record['Approved']) {
		navigationPasive ('approve') ;
	    }

	    return 0 ;
    }

    return 0 ;
?>
