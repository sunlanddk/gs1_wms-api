<?php

    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Name',		'db' => 'Certificate.Name'),
		array ('name' => 'Description',		'db' => 'Certificate.Description'),
		array ('name' => 'Supplier',		'db' => 'Supplier.Name'),
		array ('name' => 'Holder',		'db' => 'Holder.Name'),
		array ('name' => 'Type',		'db' => 'CertificateType.Name'),
		array ('name' => 'Valid until',		'db' => 'Certificate.ValidUntil')
	    ) ;
	    $queryFields = 'Certificate.*, CONCAT(Supplier.Name," (",Supplier.Number,")") AS SupplierName, CONCAT(Holder.Name," (",Holder.Number,")") AS HolderName, CertificateType.Name AS CertificateTypeName' ;
	    $queryTables = 'Certificate LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId LEFT JOIN Company AS Holder ON Holder.Id=Certificate.HolderCompanyId LEFT JOIN Company AS Supplier ON Supplier.Id=Certificate.SupplierCompanyId' ;
	    $queryClause = 'Certificate.Active=1' ;	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through
		
	case 'delete-cmd' :
	    // Get Article
	    $query = sprintf ("SELECT Certificate.* FROM Certificate WHERE Certificate.Id=%d AND Certificate.Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;
    }
 
    return 0 ;
?>
