<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'CertificateTypeId'	=> array ('type' => 'integer',	'mandatory' => true),
	'HolderCompanyId'	=> array ('type' => 'integer',	'mandatory' => true),
	'SupplierCompanyId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'ValidUntil'		=> array ('type' => 'date',	'mandatory' => true),
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		// $query = sprintf ('SELECT Id FROM Certificate WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		// $result = dbQuery ($query) ;
		// $count = dbNumRows ($result) ;
		// dbQueryFree ($result) ;
		// if ($count > 0) return 'Certificate allready existing' ;
		return true ;

	    case 'SupplierCompanyId' :
		if (!$changed) return false ;

		// Supplier can not be changed when the certificate are used in Articles
		$query = sprintf ('SELECT Id FROM ArticleCertificate WHERE ArticleCertificate.CertificateId=%d AND ArticleCertificate.Active=1', $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Supplier can not be changed when Certificate is used by Article(s)' ;

		return true ;
	}
	return false ;
    }
     
    return saveFields ('Certificate', $Id, $fields) ;
?>
