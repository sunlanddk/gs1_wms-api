<?php

    require_once 'lib/table.inc' ;

    // Ensure ArticleSize not used in ProductionOrder
    $query = sprintf ('SELECT Id FROM ProductionQuantity WHERE ArticleSizeId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Size used in ProductionOrder' ;
 
    // Ensure ArticleSize not used in Order
    $query = sprintf ('SELECT Id FROM OrderQuantity WHERE ArticleSizeId=%d AND Active=1 AND Quantity>0', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Size used in Order' ;

    // Ensure ArticleSize not used in Invoice
    $query = sprintf ('SELECT Id FROM InvoiceQuantity WHERE ArticleSizeId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Size used in Invoice' ;

    // Ensure ArticleSize not used for Items
    $query = sprintf ('SELECT Id FROM Item WHERE ArticleSizeId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Size used for Items' ;
    		       		   
    tableDelete ('ArticleSize', $Id) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, DisplayOrder FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND Active=1 ORDER BY DisplayOrder', (int)$Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['DisplayOrder']) {
	    $query = sprintf ("UPDATE ArticleSize SET DisplayOrder=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
