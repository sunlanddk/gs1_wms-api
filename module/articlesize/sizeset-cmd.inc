<?php

    require_once 'lib/table.inc' ;

    // Get SizeSetId
    $SizeSetId = (int)$_POST['SizeSetId'] ;
    if ($SizeSetId == 0) return 'no SizeSet selected' ;

    // Lookup SizeSet
    $query = sprintf ('SELECT SizeSet.Id FROM SizeSet WHERE SizeSet.Id=%d AND SizeSet.Active=1', $SizeSetId) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count == 0) return 'SizeSet not found' ;

    // Find highest current DisplayOrder
    $query = sprintf ("SELECT MAX(ArticleSize.DisplayOrder) AS DisplayOrder FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1", $Record['Id']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $DisplayOrder = (int)$row['DisplayOrder'] ;
    
    // Lookup Sizes in SizeSet
    $query = sprintf ('SELECT SizeValue.Name FROM SizeValue LEFT JOIN ArticleSize ON ArticleSize.ArticleId=%d AND ArticleSize.Active=1 AND ArticleSize.Name=SizeValue.Name WHERE SizeValue.SizeSetId=%d AND SizeValue.Active=1 AND ISNULL(ArticleSize.Name) ORDER BY SizeValue.DisplayOrder', $Record['Id'], $SizeSetId) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	$DisplayOrder++ ;
	unset ($Size) ;
	$Size['Active'] = 1 ;
	$Size['Name'] = $row['Name'] ;
	$Size['DisplayOrder'] = $DisplayOrder ;
	$Size['ArticleId'] = $Record['Id'] ;
	tableWrite ('ArticleSize', $Size) ;
    }
    dbQueryFree ($result) ;

    return 0 ;

?>
