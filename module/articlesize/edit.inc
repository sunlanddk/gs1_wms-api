<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Get highest DisplayOrder number
	    $query = sprintf ('SELECT MAX(ArticleSize.DisplayOrder) as DisplayOrder FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $row = dbFetch ($r) ;
	    dbQueryFree ($r) ;

	    // Default values
	    unset ($Record) ;
	    $Record['DisplayOrder'] = ($row) ? $row['DisplayOrder']+1 : 1 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Name</td><td><input type=text name=Name maxlength=7 size=7' value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=4 size=4' value=%d></td></tr>\n", $Record['DisplayOrder']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Customs Pos</td><td>%s</td></tr>\n", htmlDBSelect ("CustomsPositionId style='width:300px'", $Record['CustomsPositionId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM CustomsPosition WHERE Active=1 ORDER BY Description')) ;  
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
