<?php

    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query sizes to list
	    $query = sprintf ("SELECT ArticleSize.*, CustomsPosition.Name AS CustomsPositionName 
							FROM ArticleSize 
							LEFT JOIN CustomsPosition ON CustomsPosition.Id=ArticleSize.CustomsPositionId
							WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id) ;
	    $Result = dbQuery ($query) ;

	case 'sizeset' :
	case 'sizeset-cmd' :
	    // Load Article (parent)
	    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Get record
	    $query = sprintf ("SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.Id=%d AND ArticleSize.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    return 0 ;
    }

    return 0 ;
?>
