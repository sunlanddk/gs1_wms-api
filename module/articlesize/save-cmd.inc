<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'CustomsPositionId'	=> array ('type' => 'integer'),
	'DisplayOrder'		=> array ('type' => 'integer',	'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ArticleId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ArticleSize WHERE ArticleId=%d AND Active=1 AND Name="%s" AND Id<>%d', $Record['ArticleId'], addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Size allready existing' ;
		return true ;

	    case 'DisplayOrder':
		if (!$changed) return false ;
		$query = sprintf ("SELECT Id FROM ArticleSize WHERE ArticleId=%d AND Active=1 AND Id<>%d", $Record['ArticleId'], $Record['Id']) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($value <= 0 or $value > ($count+1)) return "invalid DisplayOrder number" ;
		return true ;
	}
	return false ;
    }
     
    $r = saveFields ('ArticleSize', $Id, $fields, true) ;
    if ($r) return $r ;
    
    // Renumber other entries
    $query = sprintf ("SELECT Id, DisplayOrder FROM ArticleSize WHERE ArticleId=%d AND Active=1 AND Id<>%d ORDER BY DisplayOrder", $Record['ArticleId'], $Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == $Record['DisplayOrder']) $i += 1 ;       
	if ($i != $row['DisplayOrder']) {
	    $query = sprintf ("UPDATE ArticleSize SET DisplayOrder=%d WHERE Id=%d", $i, $row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0 ;
?>
