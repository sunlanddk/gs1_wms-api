<?php

    require_once 'lib/log.inc' ;
    require_once 'lib/navigation.inc' ;

//    $query = sprintf ("SELECT Log.*, User.Loginname FROM Log LEFT JOIN User ON Log.UserId=User.Id ORDER BY Id DESC LIMIT 200") ;
//    $result = dbQuery ($query) ;
    
    printf ("<table class=list>") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=50><p class=listhead>Date</p></td>") ;
    printf ("<td class=listhead width=50><p class=listhead>User</p></td>") ;
    printf ("<td class=listhead width=18 align=center><p class=listhead>T</p></td>") ;
    printf ("<td class=listhead width=18><p class=listhead>C</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Text</p></td>") ;
    printf ("</tr>\n") ;
    $n = 0 ;
    while ($n++ < LINES and ($row = dbFetch($Result))) {
	$d = date ("Y-m-d", dbDateDecode ($row["Date"]));
       	if ($d != $lastd) {
	    printf ("<tr><td></td><td colspan=3><p class=list>%s</p></td></tr>\n", $d) ;
	    $lastd = $d ;
	}
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/log.gif', navigationOnClickLink ('show', $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", date("H:i:s", dbDateDecode($row["Date"]))) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Loginname'])) ;
	printf ("<td align=center><p class=list>%s</p></td>", substr(logLevelText($row["Level"]),0,1)) ;
	printf ("<td><p class=list>%s</p></td>", $row["Count"]) ;
	switch ($row['Level']) {
	    case logDUMP :
		// Get dump structure
		$dump = unserialize($row['Text']) ;
		printf ("<td><p class=list>%s%s</p></td>", htmlentities($dump['Navigation']['Module'].'/'.$dump['Navigation']['Function']), (is_array($dump['_POST']) and count($dump['_POST']) > 0) ? ' (POST)' : '') ;
		break ;

	    default :
		printf ("<td><p class=list>%s</p></td>", htmlentities($row["Text"])) ;
		break ;
	}
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
	printf ("<tr><td></td><td colspan=3><p class=list>No Logs available</p></td></tr>\n") ;
    }
    printf ("</table>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
