<?php

    require_once 'lib/navigation.inc' ;

    define ('LINES', 40) ;

    // Navigation
    switch ($Navigation["Function"]) {
	case "listmail" :
	    $query = sprintf ("SELECT LogMail.*, User.Loginname 
							FROM LogMail 
							LEFT JOIN User ON LogMail.UserId=User.Id 
							LEFT JOIN Company ON Company.Id=LogMail.ProjectId 
							Where 
								Level=5 and LogMail.ProjectId=%d ORDER BY Id", $Id) ;
	    $Result = dbQuery ($query) ;
		return 0 ;
	case "list" :
	    // Get page offset
	    $offset = (int)$_GET["offset"] ;

	    // Last page ?
	    if ($offset == -1) {
		$query = sprintf ("SELECT COUNT(Id) AS Count FROM Log") ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
	    	dbQueryFree ($result) ;

		$offset= $row['Count'] - LINES ;
		if ($offset < 0) $offset = 0 ;
	    }

	    // Query records to list
	    $query = sprintf ("SELECT Log.*, User.Loginname FROM Log LEFT JOIN User ON Log.UserId=User.Id ORDER BY Id DESC LIMIT %d, %d", $offset, LINES+1) ;
	    $Result = dbQuery ($query) ;
	    $count=dbNumRows ($Result) ;
	    
	    if ($offset > 0) {
		if ($offset < LINES) $offset=LINES ;
		navigationSetParameter ("previous", "offset=".((int)(($offset-LINES)/LINES)*LINES)) ;
	    } else {
		navigationPasive ("first") ;
		navigationPasive ("previous") ;
	    }

	    if ($count > LINES) {
		navigationSetParameter ("next", sprintf ("offset=%d", ((int)(($offset+LINES)/LINES))*LINES)) ;
		navigationSetParameter ("last", "offset=-1") ;
	    } else {
		navigationPasive ("next") ;
		navigationPasive ("last") ;
	    }
	    return 0 ;
    }
    
    // Get record
    if ($Navigation['Parameters'] == 'newest' or $Id > 0) {
		// Get newest record
		$query = "SELECT Log.Date AS Date, Log.Level AS Level, Log.Count AS Count, Log.Text AS Text, User.Loginname AS Loginname" ;
		if ($Config['proEnable']) $query .= ", Project.Name AS ProjectName" ;
			$query .= " FROM Log" ;
			if ($Config['proEnable']) $query .= " LEFT JOIN Project ON Log.ProjectId=Project.Id" ;
			$query .= " LEFT JOIN User ON Log.UserId=User.Id" ;
			if ($Navigation['Parameters'] != 'newest' and $Id > 0) $query .= sprintf (" WHERE Log.Id=%d", $Id) ;
		$query .= " ORDER BY Log.Date DESC LIMIT 1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if (!$Record) return sprintf ("%s(%d), record not found", __FILE__, __LINE__) ;
    }
    
    return 0 ;
?>
