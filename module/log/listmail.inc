<?php

    require_once 'lib/log.inc' ;
    require_once 'lib/navigation.inc' ;

    
    printf ("<table class=list>") ;
    printf ("<tr>") ;
//    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Date</p></td>") ;
    printf ("<td class=listhead width=50><p class=listhead>User</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Mail</p></td>") ;
    printf ("</tr>\n") ;
    $n = 0 ;
    while ($row = dbFetch($Result)) {
		$d = date ("Y-m-d", dbDateDecode ($row["Date"]));
        printf ("<tr>") ;
//		printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/log.gif', navigationOnClickLink ('show', $row["Id"])) ;
		printf ("<td><p class=list>%s</p></td>", date("Y:m:d H:i:s", dbDateDecode($row["Date"]))) ;
		printf ("<td><p class=list>%s</p></td>", htmlentities($row['Loginname'])) ;
		printf ("<td><p class=list>%s</p></td>", htmlentities($row["Text"])) ;
		printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
		printf ("<tr><td></td><td colspan=3><p class=list>No Mails in Log available</p></td></tr>\n") ;
    }
    printf ("</table>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
