<?php

    require_once "lib/log.inc" ;
    require_once "lib/html.inc" ;
    
    // Display any structured variable in html-format
    function htmlDisplayVariable (&$value, $name='', $level=0) {
	$type = gettype ($value) ;

	for ($n = 0 ; $n < $level ; $n++) printf ("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") ;
	if ($name != '') printf ("%s: ", htmlentities($name)) ;
	
	switch ($type) {
	    case 'array':
		printf ("array<br>\n") ;
		foreach ($value as $i => $v) {
		    htmlDisplayVariable ($v, (string)$i, $level+1) ;
		}
		return ;
	    case 'object':
		printf ("object<br>\n") ;
		$value = get_object_vars ($value) ;
		foreach ($value as $i => $v) {
		    htmlDisplayVariable ($v, (string)$i, $level+1) ;
		}
		return ;
	}

	// Special interperation
	switch ($name) {
	    default:
		if (is_null($value)) {
		    printf ("(null)") ;
		} else {
		    $v = strval($value) ;
		    if (strlen($v) > 20) $v = substr ($v, 0, 20) . '...' ;
		    printf ("%s", htmlentities(strval($v))) ;
		}
		break ;
	}

	printf ("<br>\n") ;
    }
    
    // Form
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td><p>Time</p></td><td><p>%s</p></td></tr>\n", date ("Y-m-d H:i:s", dbDateDecode($Record["Date"]))) ;   
    printf ("<tr><td><p>Level</p></td><td><p>%s</p></td></tr>\n", logLevelText($Record["Level"])) ;
    printf ("<tr><td><p>Count</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Count"])) ;  
    printf ("<tr><td><p>User</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Loginname"])) ;  
    if ($Config['proEnable']) printf ("<tr><td><p>Project</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["ProjectName"])) ;  

    print htmlItemSpace () ;
    switch ($Record['Level']) {
	case logDUMP :
	    // Get dump structure
	    $dump = unserialize($Record['Text']) ;
	    
	    // Primary information
	    printf ("<tr><td><p>Text</p></td><td><p>%s%s</p></td></tr>\n", htmlentities($dump['Navigation']['Module'].'/'.$dump['Navigation']['Function']), (is_array($dump['_POST']) and count($dump['_POST']) > 0) ? ' (POST)' : '') ;
	    print htmlItemSpace () ;
	    printf ("<tr><td><p>Exec time</p></td><td><p>%d mS</p></td></tr>\n", $dump['Exectime']) ;
	    unset ($dump['Exectime']) ;
	    printf ("<tr><td><p>Error</p></td><td><p>%s</p></td></tr>\n", htmlentities($dump['Error'])) ;
	    unset ($dump['Error']) ;
	    printf ("<tr><td><p>Id</p></td><td><p>%d</p></td></tr>\n", $dump['Id']) ;
	    unset ($dump['Id']) ;

	    // Primary structures
	    print htmlItemSpace () ;
	    printf ("<tr><td><p>Primary</p></td><td>") ;
	    printf ("<table class=item>\n<tr>\n") ;
	    foreach (array('Record', 'ParentObject', 'Navigation', 'Permission') as $name) {
		printf ("<td width=25%%><p>") ;
		if (is_null($name)) {
		    printf ("&nbsp;") ;
		} else {	    
		    htmlDisplayVariable ($dump[$name], $name) ;
		    unset ($dump[$name]) ;
		}
		printf ("</p></td>\n") ;
	    }
	    printf ("</tr>\n</table>\n") ;
	    printf ("</td></tr>\n") ;  

	    // Secondary structures
	    print htmlItemSpace () ;
	    printf ("<tr><td><p>Secondary</p></td><td>") ;
	    printf ("<table class=item>\n<tr>\n") ;
	    foreach (array('_GET', '_POST', 'User', 'Project') as $name) {
		printf ("<td width=25%%><p>") ;
		if (is_null($name)) {
		    printf ("&nbsp;") ;
		} else {	    
		    htmlDisplayVariable ($dump[$name], $name) ;
		    unset ($dump[$name]) ;
		}
		printf ("</p></td>\n") ;
	    }
	    printf ("</tr>\n</table>\n") ;
	    printf ("</td></tr>\n") ;    

	    // Aditional structures
	    print htmlItemSpace () ;
	    printf ("<tr><td><p>Aditional</p></td><td><p>") ;
	    htmlDisplayVariable ($dump) ;
	    printf ("</p></td></tr>\n") ;     
	    break ;

	default :
	    printf ("<tr><td><p>Text</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Text"])) ;  
	    break ;
    }
    
    printf ("</table>\n") ;
        
    return 0 ;
?>
