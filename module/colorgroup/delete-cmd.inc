<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Color WHERE ColorGroupId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the GroupGroup has associated Colors") ;

    // Do delete
    tableDelete ('ColorGroup', $Id) ;

    return 0
?>
