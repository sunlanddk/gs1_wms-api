<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 120) ;
    listHead ('Description') ;
    listHead ('', 60) ;
    listHead ('Red', 40, 'align=right') ;
    listHead ('Green', 40, 'align=right') ;
    listHead ('Blue', 40, 'align=right') ;
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'colorgroupedit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	listField ((int)$row['ValueRed'], 'align=right') ;
	listField ((int)$row['ValueGreen'], 'align=right') ;
	listField ((int)$row['ValueBlue'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No ColorGroups', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
