<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>ValueRed</td><td><input type=text name=ValueRed maxlength=3 size=3 value=%d></td></tr>\n", $Record['ValueRed']) ;
    printf ("<tr><td class=itemlabel>ValueGreen</td><td><input type=text name=ValueGreen maxlength=3 size=3 value=%d></td></tr>\n", $Record['ValueGreen']) ;
    printf ("<tr><td class=itemlabel>ValueBlue</td><td><input type=text name=ValueBlue maxlength=3 size=3 value=%d></td></tr>\n", $Record['ValueBlue']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
