<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Nunber', 80) ;
    listHead ('Name', 175) ;
    listHead ('Description') ;
    listHead ('Inventory Pct') ;
    listHead ('Owner') ;
    listHead ('Location') ;
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldIcon ($Navigation['Icon'], 'edit', (int)$row['Id']) ;
		listField ($row['StockNo']) ;
		listField ($row['Name']) ;
		listField ($row['Description']) ;
		listField ($row['InventoryPct']) ;
		listField ($row['OwnerName']) ;
		listField ($row['CompanyName']) ;
     }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Locations', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
