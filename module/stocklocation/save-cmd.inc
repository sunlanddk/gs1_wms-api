<?php

    require 'lib/save.inc' ;

    $fields = array (
		'Name'			=> array ('mandatory' => true,	'check' => true),
		'Description'	=> array (),
		'StockNo'		=> array (),
		'InventoryPct'	=> array ('type' => 'integer',	'mandatory' => true,	'format' => '3.0'),
		'CompanyId'		=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
		'FromCompanyId'	=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
		'Address1'		=> array (),
		'Address2'		=> array (),
		'ZIP'			=> array (),
		'City'			=> array (),
		'CountryId'		=> array ('type' => 'integer'),
		'Public'		=> array ('type' => 'checkbox')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    $fields['Type'] = array ('type' => 'set', 'value' => 'fixed') ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Navigation, $fields ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM Stock WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Stock location allready existing' ;
		return true ;

	    case 'FromCompanyId' :
		if (!$changed) return false ;
		
		// Validate CompanyId
		$query = sprintf ('SELECT * FROM Company WHERE Id=%d AND Active=1 AND ListHidden=0 AND Internal=1 ', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) invalid company, id %d', __FILE__, __LINE__, $value) ;
		return true ;
	    case 'CompanyId' :
		if (!$changed) return false ;
		
		// Validate CompanyId
		$query = sprintf ('SELECT * FROM Company WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) invalid company, id %d', __FILE__, __LINE__, $value) ;

		// Use default address for new locations
		if ($Navigation['Parameters'] == 'new') {
		    $fields['Address1'] = array ('type' => 'set', 'value' => $row['Address1']) ;
		    $fields['Address2'] = array ('type' => 'set', 'value' => $row['Address2']) ;
		    $fields['ZIP'] = array ('type' => 'set', 'value' => $row['ZIP']) ;
		    $fields['City'] = array ('type' => 'set', 'value' => $row['City']) ;
		    $fields['CountryId'] = array ('type' => 'set', 'value' => (int)$row['CountryId']) ;
		}

		return true ;		
	}
	return false ;
    }
     
    return saveFields ('Stock', $Id, $fields) ;
?>
