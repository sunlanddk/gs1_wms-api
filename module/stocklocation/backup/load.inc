<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list
	    $query = 'SELECT Stock.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName, CONCAT(OwnerCompany.Name," (",OwnerCompany.Number,")") AS OwnerName FROM Stock LEFT JOIN Company ON Company.Id=Stock.CompanyId LEFT JOIN Company OwnerCompany ON OwnerCompany.Id=Stock.FromCompanyId WHERE Stock.Type="fixed" AND Stock.Active=1 ORDER BY Stock.Name' ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query Stock
	    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Type="fixed" AND Stock.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
