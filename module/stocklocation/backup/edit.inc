<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Owner</td><td>%s</td></tr>\n", htmlDBSelect ('FromCompanyId style="width:300px"', $Record['FromCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE Active=1 AND ListHidden=0 AND Internal=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Location</td><td>%s</td></tr>\n", htmlDBSelect ('CompanyId style="width:300px"', $Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE Active=1 ORDER BY Name')) ;  
    if ($Navigation['Parameters'] != 'new') {
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Address</td><td><input type=text name=Address1 size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["Address1"])) ;
	printf ("<tr><td></td><td><input type=text name=Address2 size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["Address2"])) ;
	printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=ZIP size=15 value=\"%s\"></td></tr>\n", htmlentities($Record["ZIP"])) ;
	printf ("<tr><td class=itemlabel>City</td><td><input type=text name=City size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["City"])) ;
	printf ("<tr><td><p>Country</p></td><td>%s</td></tr>\n", htmlDBSelect ("CountryId style='width:250px'", $Record["CountryId"], "SELECT Id, Description AS Value FROM Country WHERE Active=1 ORDER BY Description")) ; 
	printf ("<tr><td class=itemlabel>Public</td><td><input type=checkbox name=Public size=50 %s></td></tr>\n", $Record["Public"]?'checked':'') ;
 
	print (htmlItemInfo ($Record)) ;
    }
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
