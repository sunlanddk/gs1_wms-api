<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Number'		=> array ('mandatory' => true),
	'Description'		=> array (),
	'WorkGroupId'		=> array ('mandatory' => true,	'type' => 'integer'),
	'Machines'		=> array ('mandatory' => true, 	'type' => 'integer'),
	'ProductionMinutes'	=> array ('mandatory' => true,	'type' => 'decimal',	'format' => '6.2'),
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }
     
    return saveFields ('MachineGroup', $Id, $fields) ;
?>
