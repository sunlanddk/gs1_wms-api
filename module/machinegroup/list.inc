<?php

    require "lib/dropmenu.inc" ;
    
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("%s", dropmenuTDHeader()) ;
    printf ("<td class=listhead width=57><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>WorkGroup</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Sewing</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Operations</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Machines</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("%s", dropmenuTDList($row["Id"], 'image/toolbar/machinegroup.gif')) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Number'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['WorkGroupNumber'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", ($row['Sewing']) ? 'yes' : 'no') ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['OperationCount'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['Machines'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['ProductionMinutes'])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr>%s<td colspan=4><p class=list>No workgroups</p></td></tr>\n", dropmenuTDList()) ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
