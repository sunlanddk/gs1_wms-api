<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    $query = sprintf ("SELECT MachineGroup.*, WorkGroup.Number AS WorkGroupNumber, WorkGroup.Sewing, COUNT(Operation.Id) AS OperationCount FROM MachineGroup LEFT JOIN WorkGroup ON MachineGroup.WorkGroupId=WorkGroup.Id LEFT JOIN Operation ON Operation.MachineGroupId=MachineGroup.Id AND Operation.Active=1 WHERE MachineGroup.Active=1 GROUP BY MachineGroup.Id ORDER BY MachineGroup.Number") ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
    }

    switch ($Navigation['Parameter']) {
        case 'new' :
	    return 0 ;	
    }
    
    // Get record
    if ($Id <= 0) return 0 ;
    $query = sprintf ("SELECT MachineGroup.* FROM MachineGroup WHERE MachineGroup.Active=1 AND MachineGroup.Id=%d", $Id) ;
    $Result = dbQuery ($query) ;
    $Record = dbFetch ($Result) ;
    dbQueryFree ($Result) ;
    return 0 ;
?>
