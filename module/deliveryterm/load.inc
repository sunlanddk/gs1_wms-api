<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT DeliveryTerm.* FROM DeliveryTerm WHERE DeliveryTerm.Active=1 ORDER BY DeliveryTerm.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query DeliveryTerm
	    $query = sprintf ('SELECT DeliveryTerm.* FROM DeliveryTerm WHERE DeliveryTerm.Id=%d AND DeliveryTerm.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
