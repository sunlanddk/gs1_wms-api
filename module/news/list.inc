<?php

    require "lib/dropmenu.inc" ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("%s", dropmenuTDHeader()) ;
    printf ("<td class=listhead><p class=listhead>Headline</p></td>") ;
    printf ("<td class=listhead width=180><p class=listhead>User</p></td>") ;
    printf ("<td class=listhead width=110><p class=listhead>Created</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    $n = 0 ;
    while ($n++ < LINES and ($row = dbFetch($Result))) {
        printf ("<tr>") ;
	printf ("%s", dropmenuTDList($row["Id"], 'image/toolbar/'.$Navigation['Icon'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Header'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['UserName'])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr>%s<td colspan=3><p class=list>No news</p></td></tr>\n", dropmenuTDList()) ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
