<?php

    require_once "lib/list.inc" ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Component', 120) ;
    listHead ('Description', 250) ;
    listHead ('Percent', 60, 'align=right') ;
    listHead () ;
    $total = 0 ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField (((int)$row['Percent']) .' %', 'align=right') ;
	$total += (int)$row['Percent'] ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Components', 'colspan=2') ;
    } else {
	listRow () ;
	listField (NULL, 'style="border-top: 1px solid #cdcabb;"') ;
	listField ('Total', 'style="border-top: 1px solid #cdcabb;"') ;
	listField (NULL, 'style="border-top: 1px solid #cdcabb;"') ;
	listField ($total .' %', 'align=right style="border-top: 1px solid #cdcabb;"') ;
	listField (NULL, 'style="border-top: 1px solid #cdcabb;"') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
