<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'ComponentId'		=> array ('mandatory' => true,	'check' => true),
	'Percent'		=> array ('type' => 'integer',	'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ArticleId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $fields ;
	switch ($fieldname) {
	    case 'ComponentId':
		if (!$changed) return false ;

		// Validate the component
		$query = sprintf ('SELECT Id FROM Component WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] != $value) return sprintf ('%s(%d) component not found, id %d', __FILE__, __LINE__, $value) ;
		
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ArticleComponent WHERE ArticleId=%d AND Active=1 AND ComponentId=%d AND Id<>%d', $Record['ArticleId'], $value, $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return sprintf ('%s(%d) component allready existing', __FILE__, __LINE__) ;

		return true ;

	    case 'Percent' :
		// Check that name does not allready exist
		$query = sprintf ('SELECT SUM(Percent) AS Percent FROM ArticleComponent WHERE ArticleId=%d AND Active=1 AND Id<>%d', $Record['ArticleId'], $Id) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Percent'] + $value > 100) return 'total percentage for components > 100%%' ;

		return true ;
	}
	return false ;
    }
     
    return saveFields ('ArticleComponent', $Id, $fields) ;
?>
