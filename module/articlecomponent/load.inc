<?php

//    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query colors to list
	    $query = sprintf ("SELECT ArticleComponent.*, Component.Name, Component.Description FROM ArticleComponent, Component WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1 AND Component.Id=ArticleComponent.ComponentId ORDER BY Name", $Id) ;
	    $Result = dbQuery ($query) ;

	    // Load Article (parent)
	    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Article.*, Article.Id AS ArticleId FROM Article WHERE Article.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Get record
	    $query = sprintf ("SELECT ArticleComponent.*, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription FROM ArticleComponent LEFT JOIN Article ON Article.Id=ArticleComponent.ArticleId WHERE ArticleComponent.Id=%d AND ArticleComponent.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    return 0 ;
    }

    return 0 ;
?>
