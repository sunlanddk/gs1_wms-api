<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Company', $Record['Number'] . ', ' . $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    $MyCompanyId = 787 ; parameterGet ('CompanyMy') ;

    formStart () ;

    itemStart () ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Purchase Ref</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseRefId style="width:250px"', $Record['PurchaseRefId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 AND User.Extern=0 ORDER BY Value',$MyCompanyId))) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Delivery Term</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseDeliveryTermId style="width:200px"', $Record['PurchaseDeliveryTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Carrier</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseCarrierId style="width:250px"', $Record['PurchaseCarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Currency</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseCurrencyId style="width:150px"', $Record['PurchaseCurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Payment Term</td><td>%s</td></tr>\n", htmlDBSelect ('PurchasePaymentTermId style="width:200px"', $Record['PurchasePaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Agent</td><td><input type=text name=PurchaseAgent size=20 maxlength=20 value=\"%s\"></td></tr>\n", htmlentities($Record['PurchaseAgent'])) ;
	print htmlItemSpace() ;
    itemEnd () ;

    formEnd () ;

    return 0 ;
?>
