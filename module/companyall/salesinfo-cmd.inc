<?php

    require 'lib/save.inc' ;
    
    $fields = array (
	'SalesUserId'		=> array ('type' => 'integer'),
	'DeliveryTermId'	=> array ('type' => 'integer'),
	'CarrierId'		=> array ('type' => 'integer'),
	'CurrencyId'		=> array ('type' => 'integer'),
	'PaymentTermId'		=> array ('type' => 'integer'),
	'AgentId'		=> array ('type' => 'integer'),
	'VATNumber'		=> array (),
	'InvoiceSplit'		=> array ('type' => 'checkbox'),
	'InvoiceComposition'	=> array ('type' => 'checkbox'),
	'InvoiceOrigin'		=> array ('type' => 'checkbox'),
	'CustomsDeclaration'	=> array ('type' => 'checkbox'),
	'OrderLineFooter'		=> array ('type' => 'checkbox'),
	'InvoiceHeader'		=> array ('type' => 'checkbox'),
	'Fee'		=> array ('type' => 'integer')
//	'Surplus'		=> array ('type' => 'integer'),
    ) ;
  
    return saveFields ('Company', $Id, $fields) ;
?>
