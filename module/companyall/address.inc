<?php

    require_once "lib/html.inc" ;
    require_once 'lib/item.inc' ;

    $prefix = $Navigation['Parameters'] ;

    itemStart () ;
    itemSpace () ;
    itemField ('Company', $Record['Number'] . ', ' . $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Record['Id']) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress1 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=%sAddress2 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=15 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    printf ("<tr><td class=itemlabel>Country</td><td>%s</td></tr>\n", htmlDBSelect ($prefix."CountryId style='width:250px'", $Record[$prefix."CountryId"], "SELECT Id, Description AS Value FROM Country WHERE Active=1 ORDER BY Description")) ;  
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
