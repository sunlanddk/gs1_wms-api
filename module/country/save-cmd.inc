<?php

    require 'lib/save.inc' ;
 //   require 'lib/table.inc' ;

    $fields = array (
		'Name'			=> array ('mandatory' => true,	'check' => true),
		'DisplayName'	=> array ('mandatory' => true),
		'IsoCode'		=> array ('type' => 'set'),
		'Description'		=> array (),
		'EUMember'		=> array ('type' => 'checkbox'),
		'EFTAMember'		=> array ('type' => 'checkbox'),
		'CountryGroup'		=> array (),
		'CurrencyId'		=> array (),
		'VATPercentage'		=> array ('type' => 'decimal',	'format' => '3.1'),
		'FreightPrice'		=> array ('type' => 'decimal',	'format' => '6.2'),
		'InvoiceCustomsInfo'	=> array ('type' => 'checkbox'),
		'InvoiceFooter'		=> array ()
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM Country WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Country allready existing' ;
		return true ;
	}
	return false ;
    }
     
	$companydef = array (
		CurrencyId 	=> (int)$_POST['DefaultCurrencyId'],
		DeliveryTermId => (int)$_POST['DeliveryTermId'],
		PaymentTermId => (int)$_POST['PaymentTermId'],
		CarrierId => (int)$_POST['CarrierId']
	) ; 
	$_id=tableGetFieldWhere('companydefaults','Id','CountryId='.$Id) ;
//	$query = 'Update companydefaults set  CurrencyId=' . (int)$_POST['DefaultCurrencyId'] . ', DeliveryTermId=' . (int)$_POST['DeliveryTermId'] . ',PaymentTermId=' . (int)$_POST['PaymentTermId'] . ',CarrierId=' . (int)$_POST['CarrierId'] . ' where CountryId=' . $Id ;
//	dbQuery ();
	tableWrite('companydefaults',$companydef,$_id) ;
	
	$fields['IsoCode']['value'] = $_POST['DisplayName'] ;
    return saveFields ('Country', $Id, $fields) ;
?>
