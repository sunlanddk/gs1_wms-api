<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 50) ;
    listHead ('Description') ;
    listHead ('FreightPrice', 80) ;
    listHead ('FreightCurrency', 80) ;
    listHead ('Group', 80) ;
    listHead ('EU', 50, 'align=right') ;
    listHead ('EFTA', 50, 'align=right') ;
    listHead ('VAT (%)', 50, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'editcountry', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['FreightPrice']>0?$row['FreightPrice']:'') ;
	listField ($row['CurrencyName']) ;
	listField ($row['CountryGroup']) ;
	listField ($row['EUMember'] ? 'yes' : 'no', 'align=right') ;
	listField ($row['EFTAMember'] ? 'yes' : 'no', 'align=right') ;
	listField ($row['VATPercentage'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Countries', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
