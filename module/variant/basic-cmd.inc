<?php

    require 'lib/save.inc' ;

    $fields = array (
		'VariantCode'			=> array ('mandatory' => true,	'check' => true),
		'VariantDescription'	=> array (),
		'Weight'				=> array ('type' => 'integer'),
		'Quantity'				=> array ('type' => 'integer'),
		'ArticleId'				=> array ('type' => 'set'),
		'OwnerCompanyId'		=> array ('type' => 'set')
    ) ;

    switch ($Navigation['Parameters']) {
		case 'new' :
			if ($_POST['ArticleId']>0) {
				$fields['ArticleId']['value'] = (int)$_POST['ArticleId'] ;
			} else {
				$_article = array (	"Number"		=>	"Default: " . $_POST['VariantCode'],
									"Description"	=>	"Default: " . $_POST['VariantDescription'] ,
									"ArticleTypeId"	=> 1
									) ;
				$fields['ArticleId']['value'] = (int)tableWrite('article',$_article) ;
			}
			$Id = -1 ;
			break ;
    }

    function checkfield ($fieldname, $value, $changed) {
		global $Id ;
		switch ($fieldname) {
			case 'VariantCode':
				if (!$changed) return false ;
				// Check that VariantCode does not allready exist
				$query = sprintf ('SELECT Id FROM variantcode WHERE Active=1 AND variantcode="%s" AND Id<>%d', addslashes($value), $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return 'VariantCode allready existing' ;
				return true ;
			default:
				break ;
		}
		return false ;
    }
	
    if (!($Navigation['Parameters']=='new')) {
		$fields['ArticleId']['value'] = (int)$_POST['ArticleId'] ;
	}
	$fields['OwnerCompanyId']['value'] = (int)$User['CustomerCompanyId'] ;     
	
	// and !((int)$Record['ItemQuantity']>0))
	if (!($_POST['GtinPickPos']==$Record['GtinPickPos']))  {  // New Pick Position
		if (is_null($_POST['GtinPickPos']) or $_POST['GtinPickPos']=='') {
			// Delete pick position if blank.
			if ($Record['ItemQuantity']>0)
				return "You cant unassigned with items still at pick position" ;
			$fields['PickContainerId']['type'] = 'set' ;
			$fields['PickContainerId']['value'] = 0 ;
		} else {
			// Relocate Pick Position
			$_stockNo = substr($_POST['GtinPickPos'],0,2) ;
			$_stockId = tableGetFieldWhere('Stock', 'Id', 'Type="fixed" and StockNo="'.$_stockNo.'"') ;
			if (!$_stockId>0) {
				return 'No Stock with stocknumber: ' . $_stockNo ;
			}
			
			$_position = substr($_POST['GtinPickPos'],2) ;
			$_layoutposition = tableGetFieldWhere('StockLayout', 'Id','Active=1 and Stockid='.(int)$_stockId.' and Position="'.$_position.'" and type="gtin"' ) ;
			if (!$_layoutposition>0) {
				return 'No position ' . $_position . ' for Gtin Pick Positions with stocknumber: ' . $_stockNo . ' in stock layout ';
			}
			
			$_pickcontainerid = tableGetFieldWhere('container','Id','Active=1 and Position="' . $_position . '" and StockId=' . $_stockId) ;
			if ($_pickcontainerid>0) {
				$_container = array (
					'StockId' => (int)$_stockId,
					'Position' => $_position ,
				) ;
				// Just move items 				$_pickcontainerid = tableWrite ('Container', $_container, $_pickcontainerid) ;
			} else {
				$_newContainer = array (
					'StockId' => (int)$_stockId,
					'ContainerTypeId' => 39,
					'Position' => $_position ,
					'TaraWeight' => 0,
					'GrossWeight' => 0,
					'Volume' => 0
				) ;
				$_pickcontainerid = tableWrite ('Container', $_newContainer) ;
			}
//die($_position.'-'.$_layoutposition.'-'.$_stockId.'-'.$_pickcontainerid) ;
			$fields['PickContainerId']['type'] = 'set' ;
			$fields['PickContainerId']['value'] = (int)$_pickcontainerid ;
		}
	}
	
	if (!($_POST['QtyOnPickPos']==$Record['ItemQuantity']))  {  // Adjusting GTIN Quantity in Pick Position
		$query = sprintf ('SELECT * 
							FROM Item 
							WHERE ContainerId=%d And VariantCodeId=%d AND Active=1 order by createdate', 
							$Record['ContainerId'], $Record['Id']);
		$res = dbQuery ($query) ;
		// Items available on Pick position
		if (dbNumRows($res)>0) {
			// Add qty
			if ((int)$_POST['QtyOnPickPos']>(int)$Record['ItemQuantity']) {
				$_item = dbFetch ($res) ;
				$_itemupd['Quantity'] = (int)$_POST['QtyOnPickPos'] ;
				if ((int)$_pickcontainerid>0) {
					$_itemupd['ContainerId'] = (int)$_pickcontainerid ;
				}
				tableWrite ('Item', $_itemupd, $_item['Id']) ;
			// subtract qty
			} else {
				$_qtyToSubstract = (int)$Record['ItemQuantity'] - (int) $_POST['QtyOnPickPos'] ;
				while ($_item = dbFetch ($res)) {
					unset ($_itemupd) ;
					if ((int)$_item['Quantity'] == $_qtyToSubstract) {
						tableDelete ('Item', $_item['Id']) ;
						break ;
					}
					if ((int)$_item['Quantity'] > $_qtyToSubstract) {
						$_itemupd['Quantity'] = (int)$_item['Quantity'] - (int)$_qtyToSubstract ;
						if ((int)$_pickcontainerid>0) {
							$_itemupd['ContainerId'] = (int)$_pickcontainerid ;
						}
						tableWrite ('Item', $_itemupd, $_item['Id']) ;
						break ;
					} else {
						$_qtyToSubstract -= $_item['Quantity'] ;
						tableDelete ('Item', $_item['Id']) ;
					}
				}
			}
		// No items: Create Item (no AI's for now)
		} else { 														
			$_item = array (
				'VariantCodeId'	=> (int)$Record['Id'],
				'ArticleId' 	=> (int)$Record['ArticleId'],
				'ArticleColorId'=> (int)$Record['ArticleColorId'],
				'ArticleSizeId' => (int)$Record['ArticleSizeId'],
				'ContainerId' 	=> (int)$Record['ContainerId'],
				'BatchNumber' 	=> 'ADM GEN' ,
				'Sortation' 	=> 1,
				'OwnerId' 		=> 787,
				'Quantity' 		=> (int)$_POST['QtyOnPickPos']
			) ;
			if ((int)$_pickcontainerid>0) {
				$_item['ContainerId'] = (int)$_pickcontainerid ;
			}
			tableWrite ('Item', $_item) ;
		}
		dbQueryFree ($res) ;
	} else {
		// Move items?
		if ((int)$_pickcontainerid>0) {
			$query = sprintf ('SELECT * 
								FROM Item 
								WHERE ContainerId=%d And VariantCodeId=%d AND Active=1 order by createdate', 
								$Record['ContainerId'], $Record['Id']);
			$res = dbQuery ($query) ;
			while ($_item = dbFetch ($res)) {
				$_itemupd['ContainerId'] = (int)$_pickcontainerid ;
				tableWrite ('Item', $_itemupd, $_item['Id']) ;
			}
		}
	}

    return saveFields ('variantcode', $Id, $fields) ;
?>
