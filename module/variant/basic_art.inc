<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default
	    $Record['PriceStock'] = '0.00' ;
//	    $Record['Comment'] = "Fabrics:\nLabels:\nHangtags:\nPoly Bags to be marked:\n\nBoxes to be marked:\n\nItem no.:\nOrder no.:\nPackinginstructions:\nRemarks:" ;
	    $Record['Doubles'] = false ;
	    $Record['UnitId'] = 3 ;
	    break ;

	default:
	    // Any doubles
	    if ($Record['SupplierNumber'] != '') {
		$query = sprintf ('SELECT COUNT(Id) AS Count FROM Article WHERE Article.Id<>%d AND Article.SupplierCompanyId=%d AND Article.SupplierNumber="%s" AND Article.Active=1', (int)$Record['Id'], (int)$Record['SupplierCompanyId'], addslashes($Record['SupplierNumber'])) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		$Record['Doubles'] = ((int)$row['Count'] > 0) ;
	    } else {
		$Record['Doubles'] = false ;
	    }
	    break ;
    }
 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    printf ("<tr><td class=itemlabel>Number</td><td><input type=text name=Number size=10 maxlength=10 value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Unit</td><td>%s</td></tr>\n", htmlDBSelect ("UnitId style='width:70px'", $Record['UnitId'], 'SELECT Id, Name AS Value FROM Unit WHERE Active=1 ORDER BY Name')) ;  
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Supplier</td><td>%s</td></tr>\n", htmlDBSelect ("SupplierCompanyId style='width:250px'", $Record['SupplierCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeSupplier=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Reference</td><td><input type=text name=SupplierNumber maxlength=20' value=\"%s\"></td></tr>\n", htmlentities($Record['SupplierNumber'])) ;
    itemFieldRaw ('Doubles', formCheckbox ('Doubles', $Record['Doubles'])) ;
	itemFieldRaw ('Hide from list', formCheckbox ('ListHidden', $Record['ListHidden'])) ;
//    itemSpace () ;
//    printf ("<tr><td class=itemlabel>Price</td><td><input type=text name=PriceStock maxlength=10 size=10 value=\"%s\" style='text-align:right;'>Kr.</td></tr>\n", htmlentities($Record['PriceStock'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    itemSpace() ;
//	itemFieldRaw ('Public', formCheckbox ('Public', $Record['Public'])) ;
//	printf ("<tr><td class=itemlabel>Sales Price</td><td><input type=text name=SalesPrice maxlength=10 size=10 value=\"%s\" style='text-align:right;'></td></tr>\n", htmlentities($Record['SalesPrice'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemfield>Delivery Comment</td><td><textarea name=DeliveryComment style='width:100%%;height:50px;'>%s</textarea></td></tr>\n", $Record['DeliveryComment']) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
