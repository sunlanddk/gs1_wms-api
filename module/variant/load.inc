<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
		case 'view' :
		case 'basic' :
		case 'basic-cmd' :
		case 'delete-cmd' :
			$query = sprintf ("SELECT VariantCode.*, concat(stock.StockNo,container.Position) as GtinPickPos, Container.Id as ContainerId, Sum(Item.Quantity) as ItemQuantity 
							  FROM VariantCode 
							 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
							 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1
							 LEFT JOIN Item ON Item.VariantCodeId=VariantCode.Id and Item.ContainerId=Container.Id and Item.Active=1
							WHERE VariantCode.Active=1 AND VariantCode.Id=%d Group By VariantCode.Id", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			return 0 ;

		case 'list' :
			$DescriptionField = 'if (VariantCode.VariantDescription="" or isnull(VariantCode.VariantDescription), Article.Description, VariantCode.VariantDescription)' ;

			// List field specification
			$fields = array (
			NULL, // index 0 reserved
				array ('name' => 'VariantCode',			'db' => 'VariantCode'),
				array ('name' => 'VariantDescription',	'db' => 'VariantCode.VariantDescription'),
				array ('name' => 'ArticleNumber',		'db' => 'Article.Number'),
				array ('name' => 'ArticleDescription',	'db' => $DescriptionField),
				array ('name' => 'PickPosition',				'db' => 'Container.Position'),
				array ('name' => 'Stock',				'db' => 'Stock.Name'),
				array ('name' => 'Container',			'db' => 'Container.Id')
			) ;

			require_once 'lib/list.inc' ;
			$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName, concat(stock.StockNo,container.Position) as GtinPickPos,
						VariantCode.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
						$DescriptionField . ' as VariantDescription,' .
						'Article.Number as ArticleNumber, Article.Description as ArticleDescription';
			$queryTables = 'VariantCode
				 LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
				 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
				 LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
				 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
				 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
				 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
			$queryOrderBy = '' ;
			// Clause
			$queryClause = "VariantCode.Active=1 and Variantcode.OwnerCompanyId=".$User['CustomerCompanyId'] ;

			switch ($Navigation['Parameters']) {
				case 'inbound':
					$Preadvice = true ;
					$break ;
				case 'outbound':
					$Preadvice = true ;
					$_query = 'select * from pickorder where current=1 and active=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id'] ;
					$_res = dbQuery($_query) ;
					$_row = dbFetch($_res) ;
					if ($_row['Id']>0) {
						navigationPasive('newssccbasket') ;
					} else {
						navigationPasive('editssccbasket') ;
						navigationPasive('orderssccbasket') ;
					}
					$break ;
				default :
					// Display
					break ;
			}

			$Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrderBy) ;
			
			if (is_string($Result)) return $Result ;
			return listView ($Result, 'view') ;
			
		default:
			break ;
    }

    if ($Id <= 0) return 0 ;
    

    return 0 ;
?>
