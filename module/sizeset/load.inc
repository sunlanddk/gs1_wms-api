<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of SizeSets
	    $query = sprintf ('SELECT SizeSet.*, COUNT(SizeValue.Id) AS Sizes FROM SizeSet LEFT JOIN SizeValue ON SizeValue.SizeSetId=SizeSet.Id AND SizeValue.Active=1 WHERE SizeSet.Active=1 GROUP BY SizeSet.Id ORDER BY SizeSet.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'view' :
	    // Query list of Sizes
	    $query = sprintf ('SELECT SizeValue.* FROM SizeValue WHERE SizeValue.SizeSetId=%d AND SizeValue.Active=1 ORDER BY SizeValue.DisplayOrder, SizeValue.Name', $Id) ;
	    $Result = dbQuery ($query) ;

	    // Fall through
	    
	case 'setedit' :
	case 'setsave-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'setdelete-cmd' :
	    // Query SizeSet
	    $query = sprintf ('SELECT SizeSet.* FROM SizeSet WHERE SizeSet.Id=%d AND SizeSet.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $Record = dbFetch ($r) ;
	    dbQueryFree ($r) ;
	    return 0 ;

	case 'sizeedit' :
	case 'sizesave-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    // Query SizeSet
		    $query = sprintf ('SELECT SizeSet.* FROM SizeSet WHERE SizeSet.Id=%d AND SizeSet.Active=1', $Id) ;
		    $r = dbQuery ($query) ;
		    $Record = dbFetch ($r) ;
		    dbQueryFree ($r) ;
		    return 0 ;
	    }

	    // Fall through

	case 'sizedelete-cmd' :
	    // Query SizeSet
	    $query = sprintf ('SELECT SizeValue.* FROM SizeValue WHERE SizeValue.Id=%d AND SizeValue.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $Record = dbFetch ($r) ;
	    dbQueryFree ($r) ;
	    return 0 ;

        }

    return 0 ;
?>
