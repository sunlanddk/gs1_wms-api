<?php

    require_once 'lib/table.inc' ;
   
    tableDelete ('SizeValue', $Id) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, DisplayOrder FROM SizeValue WHERE SizeValue.SizeSetId=%d AND SizeValue.Active=1 ORDER BY DisplayOrder', (int)$Record['SizeSetId']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['DisplayOrder']) {
	    $query = sprintf ("UPDATE SizeValue SET DisplayOrder=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
