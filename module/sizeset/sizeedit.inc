<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Get highest DisplayOrder number
	    $query = sprintf ('SELECT MAX(SizeValue.DisplayOrder) as DisplayOrder FROM SizeValue WHERE SizeValue.SizeSetId=%d AND SizeValue.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $row = dbFetch ($r) ;
	    dbQueryFree ($r) ;

	    // Default values
	    $Record['DisplayOrder'] = ($row) ? $row['DisplayOrder']+1 : 1 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=7 size=7 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=3 size=3 value=%d></td></tr>\n", htmlentities($Record['DisplayOrder'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
