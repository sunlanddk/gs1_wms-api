<?php

    require_once "lib/html.inc" ;

    switch ($Navigation["Parameters"]) {
	case "new":
	    $Id = -1 ;
	    unset ($Record) ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Mark</td><td><input type=text name=Mark size=40 value=\"%s\"></td></tr>\n", htmlentities($Record["Mark"])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description size=80 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record["Description"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Subject</td><td><input type=text name=Subject size=80 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record["Subject"])) ;
//    printf ("<tr><td class=itemfield>Header</td><td><input type=text name=Header size=80 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record["Header"])) ;
    printf ("<tr><td class=itemfield>Body</td><td><textarea name=Body style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record["Body"]) ;
    printf ("<tr><td class=itemfield>BodyText</td><td><textarea name=Body_default style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record["Body_default"]) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
