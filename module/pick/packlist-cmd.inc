<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/parameter.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    function GetCheckDigitSscc($barcode){
		//Compute the check digit
		$sum=0;
		$barcodeArray = str_split($barcode);
		$i = 0;
		foreach ($barcodeArray as $key => $value) {
			$i ++;
			# code...
			if($i%2 == 0){
				$sum += ( 1 * $value);
				continue;
			}

			$sum += ( 3 * $value);
			continue;
		}
		$r=$sum%10;
		if($r>0)
			$r=10-$r;
		return $r;
	}

	if ($Record['Type']=='SalesOrder'){
		$_reforderid = $Record['ReferenceId'] ;

		$query = 'SELECT Id FROM `order` WHERE `order`.Active=1 AND `order`.ConsolidatedId='.$Record['ConsolidatedId'];
		$row = dbQuery($query);
		$ordersArray = array();
		while ($orders = dbFetch($row)) {
			$ordersArray[] = $orders['Id'];
		}
		$_orderid = implode(',', $ordersArray);
	}
	
	// Is anything scanned?
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			break ;
		default:
			if (!is_array($_POST['PickQty'])) return 'No items to be packed' ;
			break ;
	}

	$TotalQuantity=0;
	foreach ($_POST['PickQty'] as $VariantCode => $qty) {
		$TotalQuantity += $qty ;
	}

    // Get and validate Fields
    $fields = array (
		'ContainerId'		=> array ('type' => 'integer'),
		'StockId'			=> array ('type' => 'integer'),
		'ContainerTypeId'	=> array ('type' => 'integer'),
		'Position'			=> array (),
		'GrossWeight'		=> array ('type' => 'decimal',	'format' => '12.3'),
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
	
	set_time_limit (300) ;
	if ($TotalQuantity>0) { // Only do anything with items and containers if something is scanned.
		// Container creation and validation
		if ($fields['ContainerId']['value'] > 0) {
			// Existing Container !
			// Lookup information
			$query = sprintf ('SELECT Container.*, Stock.Name AS StockName, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $fields['ContainerId']['value']) ;
			$res = dbQuery ($query) ;
			$Container = dbFetch ($res) ;
			dbQueryFree ($res) ;
			if ((int)$Container['Id'] != $fields['ContainerId']['value']) return sprintf ('Container %d not found', $fields['ContainerId']['value']) ;
			if ($Container['StockReady']) return sprintf ('no operations allowed on Container %d', (int)$Container['Id']) ;
			// Any Gross Weight?
			if ($fields['GrossWeight']['value'] == 0) return 'please type new Gross weight for Container after adding items' ;
			$Container_update = array (
				'GrossWeight' => $fields['GrossWeight']['value'],
			) ;
			tableWrite ('Container', $Container_update, (int)$Container['Id']) ;
			// 	$fields['StockId']['value'] = $Container['StockId'] ;
	 
		} else {
			// New Container
			// Any Stock location ?
			// if ($fields['StockId']['value'] == 0) return 'please select new Location' ;
			if ($fields['StockId']['value'] == 0) {
				// Make new shipment
				if ($Record['Type']=='SalesOrder') {
					$query = sprintf ('SELECT * FROM `Order` WHERE Id=%d AND Active=1', $Record['ReferenceId']) ;
					$result = dbQuery ($query) ;
					$Order = dbFetch ($result) ;
					dbQueryFree ($result) ;
					$shipment = array (
						'Name'				=> sprintf('%s SO: %s CO: %s', $Record['Type'], $Record['Reference'], $Record['ConsolidatedId']),
						'Description'		=> $Record['OrderTypeId']==10?'@':'',
						'DepartureDate'		=> $Record['DeliveryDate'],			    
						'CompanyId'			=> $Record['CompanyId'],
						'FromCompanyId'		=> $Record['OwnerCompanyId'],
						'Type'				=> 'shipment',
						'DeliveryTermId'	=> $Order['DeliveryTermId'],
						'CarrierId'			=> $Order['CarrierId'],
						'AltCompanyName'	=> $Order['AltCompanyName'],
						'Address1'			=> $Order['Address1'],
						'Address2'			=> $Order['Address2'],
						'ZIP'				=> $Order['ZIP'],
						'City'				=> $Order['City'],
						'CountryId'			=> $Order['CountryId']
					) ;
				} else {
					$shipment = array (
						'Name'				=> sprintf('Outbound stock %s', date('Y-m-d H:i:s',time())),
						'Description'		=> $Record['Reference'],
						'DepartureDate'		=> $Record['DeliveryDate'],			    
						'CompanyId'			=> $Record['CompanyId'],
						'FromCompanyId'		=> $Record['OwnerCompanyId'],
						'PickUpCompanyId'	=> $Record['PickUpCompanyId'],
						'Type'				=> 'shipment',
						'DeliveryTermId'	=> TableGetField('Company','DeliveryTermId',$Record['CompanyId']),
						'CarrierId'			=> TableGetField('Company','CarrierId',$Record['CompanyId']),
						'CompanyName'		=> $Record['CompanyName'],
						'Address1'			=> $Record['Address1'],
						'Address2'			=> $Record['Address2'],
						'ZIP'				=> $Record['ZIP'],
						'City'				=> $Record['City'],
						'CountryId'			=> $Record['CountryId']
					) ;
				}
				$fields['StockId']['value']=tableWrite ('Stock', $shipment) ;
			}	
			// Get Stock
			$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
			$result = dbQuery ($query) ;
			$Stock = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
			if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

			// Any ContainerType ?
			if ($fields['ContainerTypeId']['value'] == 0) return 'please select type for new Container' ;

			// Any Gross Weight?
			if ($fields['GrossWeight']['value'] == 0) 
				$fields['GrossWeight']['value'] = 1 ;
				//return 'please type Gross weight for new Container' ;

			// Get ContainerType
			$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
			$result = dbQuery ($query) ;
			$ContainerType = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;

			// Create new Container
			$Container = array (
				'StockId' => (int)$Stock['Id'],
				'ContainerTypeId' => (int)$ContainerType['Id'],
				'Position' => $fields['Position']['value'],
				'TaraWeight' => $ContainerType['TaraWeight'],
				'GrossWeight' => $fields['GrossWeight']['value'],
				'Volume' => $ContainerType['Volume']
			) ;
			$Container['Id'] = tableWrite ('Container', $Container) ;
			
			// Set SSCC on Container
		$totalprefix = parameterGet('ssccprefix');
		$containerNumberWZero = sprintf('%09d', $Container['Id']);
		$checkDigit = GetCheckDigitSscc((string) $totalprefix.$containerNumberWZero);
		$ssccCode = (string) $totalprefix.$containerNumberWZero.$checkDigit;
		$container = array('Sscc' => $ssccCode) ;
		$Container['Id'] = tableWrite('Container', $container, $Container['Id']) ;
			
			$Container['StockName'] = $Stock['Name'] ;
		}
		// END CONTAINER VALIDATION AND CREATION

		// Create ItemOperation referance
		$ItemOperation = array (
			'Description' => sprintf ('Items picked to Container %d at %s', (int)$Container['Id'], $Container['StockName'])
		) ;
		$ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
			
		$Error_txt=0 ;
		// Process scanned quantities for each pick line
		foreach ($_POST['PickQty'] as $VariantCode => $_consolidatedqty) {
			if ($_consolidatedqty==0) continue ; // Nothing picked: next line
			// multible consolidated picklines per variant
			$_consolidated=explode(',',$_POST['PickOrderLineId'][$VariantCode]) ;
			$_consolidatedlines = count($_consolidated) ;
			foreach ($_consolidated as $_pickorderlineid) {
				if ($_consolidatedqty<=0) continue ; // No need on this line: next line

				// Get Variant Information
				$query = sprintf ('SELECT * FROM pickorderline WHERE Id=%d', $_pickorderlineid) ;
				$res = dbQuery ($query) ;
				$_pickorderline = dbFetch ($res) ;
				dbQueryFree ($res) ;
				$_pickorderlineqty = (int)$_pickorderline["OrderedQuantity"]-(int)$_pickorderline["PackedQuantity"] ; // -(int)$_pickorderline["PickedQuantity"] ;
				if ($_pickorderlineqty<=0) continue ; // No need on this line: next line
			
				if ($_consolidatedlines > 1) {
					$qty = ($_consolidatedqty>$_pickorderlineqty)? $_pickorderlineqty : $_consolidatedqty ;
					$_consolidatedqty -= $qty ;
				} else {
					$qty = $_consolidatedqty ; // Allow surplus on last line
				}
				$_consolidatedlines-- ;
					// New code end			
				// process each consolidated pickline.
//				if ($VariantCode <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;

				// Get Variant Information
				$query = sprintf ('SELECT VariantCode.*, Container.StockId as PickStockId
								FROM VariantCode 
								LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
								WHERE VariantCode.VariantCode="%s" AND VariantCode.Active=1', $VariantCode) ;
				$res = dbQuery ($query) ;
				$VariantCodeInfo = dbFetch ($res) ;
				dbQueryFree ($res) ;
                if ($VariantCodeInfo['Id'] <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__,  $VariantCode) ; 
				// Is the PickStock the items main pick stock? If not use aleternative pickstock
//				if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0)) 
//					$VariantCodeInfo['PickContainerId'] = 0;	// Future opt: Use containerid from pickcontainer table (variantid, container)		

				// decrease item qty in pick location (in pick container) or make new item.
				if (($VariantCodeInfo['PickContainerId'] == 0) and ($Record['Type']=='SalesOrder')) {
					$query = sprintf ('SELECT Item.* 
										FROM Item, Container 
										WHERE Item.ContainerId=Container.Id And Container.StockId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Item.Active=1 order by createdate',
								 $Record['FromId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
				} else {
					$query = sprintf ('SELECT * 
										FROM Item 
										WHERE ContainerId=%d And VariantCodeId=%d AND Active=1 order by createdate',
										$VariantCodeInfo['PickContainerId'],$VariantCodeInfo['Id']) ;
				}
//die($query) ;			
				$res = dbQuery ($query) ;

				$PickedQuantity = 0;						
				if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
					$RemScannedQty = $qty * $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']] ;
				else
					$RemScannedQty = $qty ;
				$ReqQty=$RemScannedQty ;

				// Allocate Items to sales orderlines for pickorders based on salesorders.
				if ($Record['Type']=='SalesOrder') {
					If ((int)$_pickorderline["QlId"]>0) {
						$query = sprintf ('SELECT OrderQuantity.OrderLineId as Id 
											FROM OrderQuantity 
											WHERE OrderQuantity.Id=%d',(int)$_pickorderline["QlId"]) ;
					} else {
						$query = sprintf ('SELECT OrderLine.Id as Id 
											FROM OrderLine
											LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.ArticleSizeId=%d AND OrderQuantity.Active=1 And OrderQuantity.Quantity>0
											WHERE	OrderLine.OrderId IN(%s) And OrderLine.ArticleId=%d AND 
													OrderLine.ArticleColorId=%d AND OrderLine.Active=1  AND OrderLine.Done=0',
													$VariantCodeInfo['ArticleSizeId'],$_orderid,
													$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'] ) ;
					}
					$olres = dbQuery ($query) ;
					$OrderLine = dbFetch ($olres) ;
					dbQueryFree ($olres) ;
					if ($OrderLine['Id']>0) {
						$IOrderLineId = $OrderLine['Id'] ;	
					} else {
						return sprintf('SKU %s in pickorder but not found in salesorder %d',
									$VariantCode, $Record['ReferenceId'],
									$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId']) ;
					}
				}
			
				// Find items on stock to fulfil registered qty.
				while ($Item = dbFetch ($res)) {
					if ($Item['Id'] > 0) {
						if (($Item['Quantity'] > $RemScannedQty) or ($Item['Quantity'] == $RemScannedQty)) {
							$PickedQuantity += $RemScannedQty ;

							// Split Item and create new item with complete qty.
							$Item['Quantity'] -= $RemScannedQty ;
							tableWrite ('Item', $Item, $Item['Id']) ;
							If ($Item['Quantity'] == 0) 
								tableDelete ('Item', $Item['Id']) ;
							
							$Item['PreviousContainerId']=$Item['ContainerId'] ;
							$Item['ContainerId']=$Container['Id'] ;
							$Item['ParentId']=$Item['Id'] ;
							$Item['Quantity'] = $RemScannedQty ;
							$Item['OrderLineId'] = $IOrderLineId ;

							unset ($Item['Id']) ;
							tableWrite ('Item', $Item) ;
							break ;
						} else {
							$RemScannedQty -= $Item['Quantity'] ;
							$PickedQuantity += $Item['Quantity'] ;

							// delete Item on stock and create new item with item qty.
							$ItemQty = $Item['Quantity'];
							$Item['Quantity'] = 0 ;
							tableWrite ('Item', $Item, $Item['Id']) ;
							tableDelete ('Item', $Item['Id']) ;
							
							$Item['PreviousContainerId']=$Item['ContainerId'] ;
							$Item['ContainerId']=$Container['Id'] ;
							$Item['ParentId']=$Item['Id'] ;
							$Item['Quantity'] = $ItemQty ;
							$Item['OrderLineId'] = $IOrderLineId ;

							unset ($Item['Id']) ;
							tableWrite ('Item', $Item) ;
						}
					} else {
						return sprintf ('Invalid Item fetch') ;
					}
					$Clone_Item = $Item ;
				}		
				// Check if enough items found on stock to fulfil registration
				if ($PickedQuantity < $ReqQty) {
					// Nothing found at all: Make item from scratch.
					if ($PickedQuantity == 0) {
						$Item = array (
							'VariantCodeId'	=> (int)$VariantCodeInfo['Id'],
							'ArticleId' 	=> (int)$VariantCodeInfo['ArticleId'],
							'ArticleColorId'=> (int)$VariantCodeInfo['ArticleColorId'],
							'ArticleSizeId' => (int)$VariantCodeInfo['ArticleSizeId'],
							'ContainerId' 	=> (int)$Container['Id'],
							'OrderLineId'  	=> (int)$IOrderLineId ,
							'BatchNumber' 	=> 'PICK AUTO' ,
							'Sortation' 	=> 1,
							'OwnerId' 		=> 787,
							'Quantity' 		=> ($ReqQty - $PickedQuantity) 
						) ;
						unset ($Item['Id']) ;
						tableWrite ('Item', $Item) ;
					} else {
						// something found but not enough: Clone last item.
						$Clone_Item['Quantity'] 	= $ReqQty - $PickedQuantity ;
						$Clone_Item['BatchNumber'] 	= 'PICK AUTO' ;
						unset ($Clone_Item['Id']) ;
						tableWrite ('Item', $Clone_Item) ;
					}
					if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
						$qty=(int)($ReqQty / $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']]) ;
					else
						$qty=$ReqQty;
				}
				dbQueryFree ($res) ;
			
				// Update qty in PickOrderLine				
				$PickOrderLine['PackedQuantity'] = $_POST['PackQty'][$VariantCode] + $qty  ;
				$PickOrderLine['PickedQuantity'] = 0  ;
				tableWrite ('PickOrderLine', $PickOrderLine, $_pickorderlineid) ;
			} // END consolidated variants.
		} // END of each pickline.
	} // END TotalQuantity >0
	
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			// Update PickOrder.
			if ($TotalQuantity==0) {
				if ($Record['LastContainerId']==0) 
					return 'Nothing to ship' ;
				else 
					$Container['Id']=(int)$Record['LastContainerId'];
			}
			$PickOrder = array (
				'Packed'			=> 1,
				'PackedUserId'		=> $User['Id'],
				'PackedDate'		=> dbDateEncode(time()),
				'LastContainerId' 	=> (int)$Container['Id'],
				'ToId'				=> (int)$fields['StockId']['value']
			) ;
			if ($Navigation['Parameters'] == 'partship') {
				$PickOrder['Packed']=0 ;
				$PickOrder['Printed']=0 ;
			}
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			
			if ($Record['Type']=='SalesOrder') {
				$shipment = array (
					'Ready'			=> 1,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'			=> 1,
					'DepartedUserId'	=> $User['Id'],
					'DepartedDate'		=> dbDateEncode(time()),
					'PartShip'			=> 0
				) ;
				if ($Navigation['Parameters'] == 'partship') $shipment['PartShip'] = 1 ; 
			} else {
				$query = sprintf ("SELECT * FROM PickOrder Where consolidatedpickorderid=%d and `type`='SSCC' and active=1 and packed=0", $Record['ConsolidatedPickOrderId']) ;
				$_consres = dbQuery ($query) ;
				$ssccpick = dbFetch ($_consres) ;
				dbQueryFree ($_consres) ;
				
				if($ssccpick) {
					$_departed = 0 ;
				} else {
					$_departed = 1 ;
				}
				$shipment = array (
					'Ready'			=> $_departed,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'		=> $_departed,
					'DepartedUserId'=> $User['Id'],
					'DepartedDate'	=> dbDateEncode(time()),
					'Invoiced'		=> 1,
					'InvoicedUserId'=> $User['Id'],
					'InvoicedDate'	=> dbDateEncode(time()),
					'Done'			=> $_departed,
					'DoneUserId'	=> $User['Id'],
					'DoneDate'		=> dbDateEncode(time())
				) ;
			}
			$_shipmentid = tableWrite ('Stock', $shipment, (int)$fields['StockId']['value']) ;
			
			if ($Record['Type']=='SalesOrder') {
				// Genrate invoice
				$_ordertypeid = $Record['OrderTypeId'] ;
				$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $_shipmentid) ;
				$res = dbQuery ($query) ;
				$Record = dbFetch ($res) ;
				dbQueryFree ($res) ;
				if ($Record['OrderTypeId']==10) {
					$Record['SetReady'] = 0 ;
				} else {
					$Record['SetReady'] = 0 ;
				}
				$Record['SetReady'] = 0 ;
				$Record['PreInvoiceId'] = $_preinvoiceid ;
				$Record['PreInvoiceOrderId'] = $_reforderid ;
				$Record['OrderTypeId']=$_ordertypeid ;
				require_once 'module/invoice/generate-cmd.inc' ;
			} else {
			    return navigationCommandMark ('shipmentview', $_shipmentid) ;
			}
			break;
		default :
			if ($TotalQuantity==0) return 'No items to be packed' ;

			// Update PickOrder.
			$PickOrder = array (
				'LastContainerId' => (int)$Container['Id'],
				'ToId' => (int)$fields['StockId']['value']
			) ;
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			$query = sprintf ("SELECT * FROM PickOrder Where consolidatedpickorderid=%d and `type`='SSCC' and active=1 and packed=0", $Record['ConsolidatedPickOrderId']) ;
			$_consres = dbQuery ($query) ;
			$ssccpick = dbFetch ($_consres) ;
			dbQueryFree ($_consres) ;
			if($ssccpick) {
				tableWrite ('PickOrder', $PickOrder, $ssccpick['Id']) ;
			}
			break ;
    }

	return $Error_txt;	
?>
	
