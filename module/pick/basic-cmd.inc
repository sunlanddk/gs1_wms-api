<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
		'PickUserId'		=> array ('type' => 'integer',	'check' => true),
		'DeliveryDate'		=> array ('type' => 'date',	'mandatory' => true,	'check'	=> true),
		'Instructions'		=> array (						'check' => true),
		'Printed'			=> array ('type' => 'checkbox',	'check' => true),
		'Packed'			=> array ('type' => 'checkbox',	'check' => true),
		'Picked'			=> array ('type' => 'checkbox',	'check' => true),
		'Reference'			=> array (),
//		'Current'			=> array ('type' => 'set'),
		'Type'				=> array ('type' => 'set'),
		'Ready'				=> array ('type' => 'checkbox',	'check' => true),
		'ReadyUserId'		=> array ('type' => 'set'),
		'ReadyDate'			=> array ('type' => 'set'),
		'CompanyName'		=> array (					'check' => false),
		'Address1'			=> array (						'check' => false),
		'Address2'			=> array (						'check' => false),
		'ZIP'				=> array (						'check' => false),
		'City'				=> array (						'check' => false),
		'CountryId'			=> array ('type' => 'integer',	'check' => false),
		'CompanyId'			=> array ('type' => 'integer',	'check' => false),
		'OwnerCompanyId'	=> array ('type' => 'integer',	'check' => false),
		'PickUpCompanyId'	=> array ('type' => 'integer',	'check' => false),
		'ConsolidatedPickOrderId' => array ('type' => 'set'),
  ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'Printed':
				if (!$changed) return false ;
				return true ;

			case 'Packed':
			case 'Picked':
				// Ignore if not setting flag
				if (!$changed) return false ;
				return true ;

			case 'Ready':
				// Ignore if not setting flag
				if (!$changed or !isset($_POST['Ready'])) return false ;

				if ($Navigation['Parameters']=='new') return false;
				
/*				$_pickorderlineid = (int)tableGetField('pickorderline', 'Id', 'pickorderid='.$Id) ;
				if ($_pickorderlineid==0)
					return 'No orderlines - Cant set ready' ;
*/				
				// Set tracking information
				$fields['ReadyUserId']['value'] = $User['Id'] ;
				$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
				$fields['Current']['value'] = 0 ;
				dbQuery("update pickorder set current=0 where Id=".$Id) ;
				
				return true ;
				
			case 'PickUserId' :
				if (!$changed) return false ;
				break ;

			case 'DeliveryDate' :
				if (!$changed) return false ;
	//			if ($Record['Packed']) 
	//				return 'Cant change delivery when packed' ;
				break ;
		}

		if (!$changed) return false ;
			return true ;	
    }
    
    switch ($Navigation['Parameters']) {
		case 'pick' :
		case 'pack' :
			break ;

		case 'new' :
			$_currentpickorderid=tableGetFieldWhere('pickorder','Id','Current=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id']) ;
			if ($_currentpickorderid>0) 
				dbQuery("update pickorder set current=0 where Id=".$_currentpickorderid) ;
			$fields['Current']['type'] = 'set' ;
			$fields['Current']['value'] = 1 ;
			$_consorder = array ('current'=>1) ;
			$_consolidatedId = tablewrite('consolidatedpickorder', $_consorder) ;
			$fields['ConsolidatedPickOrderId']['value'] = $_consolidatedId;
			$fields['Type']['value'] = 'SSCC' ;
			break ;

		default :
			break ;
    }
//    if (($_POST['CompanyId'] > 0) and $Navigation['Parameters']=='new') {
    if (($_POST['CompanyId'] > 0)) {
		$query='select * from company where id='.(int)$_POST['CompanyId']; 
		$res = dbQuery($query) ;
		$row = dbFetch($res) ;
		if (!isset($_POST['CompanyName']) or $_POST['CompanyName']=='') $_POST['CompanyName'] = $row['Name'];
//return ('her ' . $row['Name']) ;
		if (!isset($_POST['Address1']) or $_POST['Address1']=='') $_POST['Address1'] = $row['Address1'];
		if (!isset($_POST['Address2']) or $_POST['Address2']=='') $_POST['Address2'] = $row['Address2'];
		if (!isset($_POST['ZIP']) or $_POST['ZIP']=='') $_POST['ZIP'] = $row['ZIP'];
		if (!isset($_POST['City']) or $_POST['City']=='') $_POST['City'] = $row['City'];
		if (!((int)$_POST['CountryId']>0)) $_POST['CountryId'] = $row['CountryId'];
	}
		
    $res = saveFields ('PickOrder', $Id, $fields, true) ;
    if ($res) return $res ;
	
	if ($Navigation['Parameters']=='new') {
		$_ssccId = $Record['Id'] ;
		
		$fields['Type']['value'] = 'GTIN' ;
		$fields['Current']['value'] = 0 ;
		$res = saveFields ('PickOrder', $Id, $fields, true) ;
		if ($res) return $res ;
	} else {
		$_type = ($Record['Type']=='GTIN') ? 'SSCC' : 'GTIN' ;
	
		$_id = tableGetFieldWhere('pickorder','Id',sprintf('Type="%s" and consolidatedpickorderid=%d and active=1', $_type, $Record['ConsolidatedPickOrderId'])) ;
		
		$fields['Type']['value'] = $_type ;
		$res = saveFields ('PickOrder', $_id, $fields, true) ;
		if ($res) return $res ;
		
	}
	
	foreach ($_POST['Ai'] as $aiid => $_order) {
		if ($Navigation['Parameters']=='new')
			$_pickorder_aiId = 0 ;
		else 
			$_pickorder_aiId = (int)$_POST['PAiId'][$aiid];
		if ($_order=='') {
			if ($_pickorder_aiId>0) {
				dbQuery("delete from pickorder_ai where id=".$_pickorder_aiId) ;
			}
		} else {
			if ($_pickorder_aiId>0) {
				dbQuery("update pickorder_ai set SortOrder=" . $_order . " where id=".$_pickorder_aiId) ;
			} else {
				$_query = sprintf("insert pickorder_ai set AiId=%d, PickOrderId=%d, SortOrder=%d", $aiid, $_ssccId, $_order) ;
				dbQuery($_query) ;
			}
		}
	}

    switch ($Navigation['Parameters']) {
		case 'pick' :
			return navigationCommandMark ('picklist', (int)$Record['Id']) ;
		case 'pack' :
			return navigationCommandMark ('packlist', (int)$Record['Id']) ;
		default:
			break;
    }

    return 0 ;    
?>
