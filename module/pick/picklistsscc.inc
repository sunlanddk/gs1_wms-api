<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;

	$_batchpreadvice = 1 ; 
	
    if (dbNumRows($Result) == 0) return "No lines to pick" ;

    $VariantCode='';

    itemStart () ;
    itemSpace () ;
    itemField ('PickOrder', sprintf('%d - %s - %s', $Record['Id'], $Record['Reference'], date('Y-m-d', dbDateDecode($Record['DeliveryDate'])))) ;
    if ($Record['PickUserId'])
	    itemField ('Picker', sprintf('%s %s (%s)', tableGetField('User','FirstName', $Record['PickUserId']),tableGetField('User','LastName', $Record['PickUserId']),tableGetField('User','LoginName', $Record['PickUserId']))) ;
    if (($Record['Packed']) AND ($Record['ToId']>0)){
	  itemFieldIcon ('PICK CLOSED', 'Packinglist', 'print.gif', 'shippackinglist', $Record['ToId']) ;
	  itemFieldIcon ('Shipment', TableGetField('Stock','Name',$Record['ToId']), 'shipment.gif', 'shipmentview', $Record['ToId']) ;
    }
    if ($Record['Instructions']>'')
	    itemField ('Instructions', $Record['Instructions']) ;
    itemSpace () ;
    itemEnd () ;

    // Filter Bar
//    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHead ('Variant', 95)  ;
    listHead ('Description',200) ;
    listHead ('Ordered Quantity',95) ;
    listHeadIcon () ;
    listHead ('',140) ;
    listHead ('') ;

	while ($row = dbFetch($Result)) {
		$_ordercols = '' ; $_joins=''; $_orderby ='' ; 
		$_rem = $row["OrderedQuantity"]-$row["PackedQuantity"]-$row["PickedQuantity"] ;
		$_remtotal += $_rem ;
		listRow () ;
		listField ($row["VariantCode"]) ;
		listField ($row["VariantDescription"]) ;
		listField ($_rem,'align="right"') ;
		listFieldIcon ('pen.gif', 'editoutbound', $row['Id']) ;
		if ($Record["Packed"]) continue ;
		
		$_query = sprintf('Select c.Position as "Position", c.SSCC as SSCC, group_concat(if(ai.id=4 or ai.id=6,concat(ai.description,": ",v_ai.value),"") order by ai.id) as AIs, min(i.batchnumber) as Itembatch 
							from containerreserved cr, container c, stock s, item i, variant_ai v_ai, ai
							where pickorderlineid=%d and c.id=cr.containerid and s.id=c.stockid AND s.type="fixed" and cr.active=1 and i.containerid=c.id and i.active=1 and v_ai.itemid=i.id and ai.id=v_ai.aiid
							GROUP BY c.id
							order by c.createdate
							limit %d', $row["Id"], $_rem); //die($_query) ;
		$_res = dbQuery($_query) ;
		$_reserved = dbNumRows($_res) ;
		$_onstock = 0 ;
		While ($_row = dbFetch($_res)) {
			$row['SSCC'] = $_row['SSCC'] ;
			$row['Position'] = $_row['Position'] ;
			$row['AIs'] = $_row['AIs'] ;
			$row['Itembatch'] = $_row['Itembatch'] ;
			$Lines[$row['Position']][]=$row ;
			$_onstock++ ;
		}
		dbQueryFree ($_res) ;
		
		if (($_onstock < $_rem) and ($_batchpreadvice==0)){
			// Get AI's to sort by
			$_query= sprintf('Select * from pickorder_ai where pickorderid=%d order by SortOrder', $Id);
			$_res = dbQuery($_query) ;
	/* Build		
							select 	i.id, c.Position as "Position", c.SSCC as SSCC, v1.value as ordervalue1, v2.value as ordervalue2
							from (item i, container c)
							Left JOIN variant_ai v1 ON v1.itemid=i.id and v1.aiid=4
							Left JOIN variant_ai v2 ON v2.itemid=i.id and v2.aiid=6
							where i.active=1 and c.active=1 and i.variantcodeid=4 AND c.id=i.containerid AND c.stockid=14321
							order by ordervalue1, ordervalue2;
							select * from ( 
							SELECT i.Id, c.Position as "Position", c.SSCC as SSCC, c.id as ContainerId, min(po.id) as PoId ,
							 v1.value as ordervalue1, ai1.description as ai1description, v1.aiid as aiid1, v2.value as ordervalue2 
							 FROM (item i, container c, stock s, stocklayout sl) 
							 LEFT JOIN ai ai1 ON ai1.id=4
							 LEFT JOIN variant_ai v1 ON v1.itemid=i.id and v1.aiid=ai1.id
							 LEFT JOIN variant_ai v2 ON v2.itemid=i.id and v2.aiid=6 
							 left JOIN containerreserved cr ON cr.variantcodeid=i.variantcodeid and cr.containerid=c.id and cr.active=1 
							 left join pickorderline pol ON pol.id=cr.pickorderlineid and pol.active=1 
							 left join pickorder po on po.id=pol.pickorderid and po.packed=0 and po.active=1 
							 WHERE i.active=1 and c.active=1 and i.variantcodeid=17 AND c.id=i.containerid AND s.id=c.stockid 
							 AND s.type="fixed" and sl.stockid=s.id and sl.position=c.position and sl.type="sscc" 
							 group by c.id 
							 ORDER BY ordervalue1, ordervalue2, Position 
							 ) tab 
							 where isnull(PoId) limit 7						
	*/					
			While ($_row = dbFetch($_res)) {
				$_ordercols .= sprintf(', v%d.value as ordervalue%d, ai%d.description as aidescription%d ',$_row['SortOrder'], $_row['SortOrder'],$_row['SortOrder'], $_row['SortOrder']) ;
				$_joins .= sprintf(' LEFT JOIN ai ai%d ON ai%d.id=%d LEFT JOIN variant_ai v%d ON v%d.itemid=i.id and v%d.aiid=%d ', $_row['SortOrder'], $_row['SortOrder'],$_row['AiId'], $_row['SortOrder'], $_row['SortOrder'], $_row['SortOrder'],$_row['AiId']) ;
				$_orderby .= sprintf(' ordervalue%d, ', $_row['SortOrder']) ;
			}
			dbQueryFree ($_res) ;
			$_query = 	'select * from (
							SELECT i.Id, c.Position as "Position", c.SSCC as SSCC, c.id as ContainerId,  min(po.id) as PoId ' . $_ordercols . 
							 ' FROM (item i, container c, stock s, stocklayout sl) ' .
							 $_joins .
							 'left JOIN containerreserved cr ON cr.variantcodeid=i.variantcodeid and cr.containerid=c.id and cr.active=1 
							  left join pickorderline pol ON pol.id=cr.pickorderlineid and pol.active=1
							  left join pickorder po on po.id=pol.pickorderid and po.packed=0 and po.active=1 ' .
							 ' WHERE i.active=1 and c.active=1 and i.variantcodeid=' . (int)$row['VariantCodeId'] . ' AND c.id=i.containerid AND s.id=c.stockid AND s.type="fixed" and sl.stockid=s.id and sl.position=c.position and sl.type="sscc"' . 
							 ' group by c.id ORDER BY ' . $_orderby . ' c.createdate
						 ) tab where isnull(PoId) limit ' . ($_rem-$_onstock) ;
			$_res = dbQuery($_query) ;
			
	//		$_onstock = 0 ;
			While ($_row = dbFetch($_res)) {
				$row['SSCC'] = $_row['SSCC'] ;
				$row['Position'] = $_row['Position'] ;
				$Lines[$row['Position']][]=$row ;
				$_reservation = array ('ContainerId'=>$_row['ContainerId'], 'VariantCodeId'=>$row['VariantCodeId'], 'PickOrderLineId'=>$row['Id']);
				tableWrite('containerreserved', $_reservation) ;
				$_onstock++ ;
			}
			dbQueryFree ($_res) ;
		}
		
		if ($_onstock < $_rem) {
			listField (($_rem-$_onstock) . ' missing On Stock') ;
			listField ('') ;
		}
		else {
			listField ('') ;
			listField ('') ;
		}
	}
	dbQueryFree ($Result) ;	
	
	listRow () ;
	listField ('Total') ;
	listField ('') ;
	listField ($_remtotal,'align="right"') ;
	listField ('') ;
    listEnd () ;
	echo('<br><br>') ;
	
	if ($Record["Packed"]) {
		itemStart () ;
		itemSpace () ;
		itemField ('Order Packed') ;
		itemEnd() ;
	} else {
		listStart () ;
		listRow () ;
		listHead ('Position', 80) ;
		listHead ('SSCC', 150) ;
		listHead ('Variant', 95)  ;
		listHead ('Description') ;
		listHead ('') ;
		ksort($Lines) ;
		foreach ($Lines as $posline) {
			foreach ($posline as $row ) {
				listRow () ;
				listField ($row["Position"]) ;
				listField ($row["SSCC"]) ;
				listField ($row["VariantCode"]) ;
				listField ($row["VariantDescription"]) ;
				listField ($row["AIs"] . ' - Batch by item: ' . $row["Itembatch"]) ;
			}
		}
		listEnd () ;
	}

    return 0 ;

?>
