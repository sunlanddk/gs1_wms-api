<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;

    function flagOrders (&$Record, $field, $title) {
		print formHidden ($field.'['.$Record['Id'].']', 1) ;
		if ($Record[$field]) {
			itemField ($title, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemField ($title, 'Draft') ;
		}
    }

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Consolidated', $Record['Id']) ;
    itemField ('Pick State', $Record['PickOrderState']) ;
    itemSpace () ;
    itemFieldRaw ('Confirm', formCheckbox ('ReadyAll', 1)) ;
    //flag ($Record, 'Ready') ;
    //itemFieldRaw ('Confirm') ;
    $query = 'SELECT * FROM `order` o WHERE o.ConsolidatedId='.$Record['Id'].' AND o.Active=1';
    $res = dbQuery($query);
    while ($row = dbFetch ($res)) {
        flagOrders ($row, 'Ready', 'Order '.$row[Id]) ;
//        flagOrders ($row, 'Order') ;
    }
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Handler', formDBSelect ('HandlerId', 787, 
		"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE Internal=1 AND Active=1 ORDER BY Name", 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Pick Stock', formDBSelect ('PickStockId', $Record['SeasonPickStockId'] , sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 ORDER BY Value'), 'width:250px;')) ;
    itemSpace () ;
    itemFieldRaw ('Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:100px;')) ;
    itemSpace () ;
 //   itemFieldRaw ('File to Alpi', formCheckbox ('TransferFile', 1)) ;
 //   itemSpace () ;
    itemFieldRaw ('File to Alpi', formCheckbox ('TransferFile', 1)) ;
    itemFieldRaw ('Only Create File', formCheckbox ('OnlyFile', 0)) ;
    itemSpace () ;
    itemSpace () ;
    itemEnd () ;

    listStart () ;
    listRow () ;
    listHead ('LOT', 70) ;
    listHead ('Planned Date', 90) ;
    listHead ('Generated until', 90) ;

	$query = sprintf ('Select Name, date(`Date`) as LOTDate,GeneratedPick,date(GeneratedDate) as GeneratedDate From CollectionLOT Where SeasonId=%d and active=1 Order By LOTDate', $Record['SeasonId']) ;
	$res = dbQuery ($query) ;
	$MaxDate = '' ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
		listField ($row['Name']) ;
		listField ($row['LOTDate']) ;
		if ($row['GeneratedPick']) {
			listField ($row['GeneratedDate']>'2001-01-01'? $row['GeneratedDate'] : 'ALL') ;
			$MaxDate = $row['GeneratedDate']>'2001-01-01'? $row['GeneratedDate'] : '' ;
		} else {
			listField ('') ;
		}
	}
	listEnd() ;

    itemStart () ;
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Include deliveries until', formDate  ('MaxDeliveryDate', '')) ; // dbDatedecode($MaxDate))) ;
    itemEnd () ;

    formEnd () ;

    return 0 ;
?>
