<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	 $_batchpreadvice = 1 ; 
	
    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['PickOrderId']) ; //(int)$Record['ConsolidatedPickOrderId']) ;     
    itemField ('Reference', $Record['PickOrderReference']) ;     
    itemSpace () ;
    itemField ('Product', $Record['VariantCode'] . ' - ' . $Record['VariantDescription']) ;     
    itemEnd () ;
    
    // Form
    formStart () ;
    itemStart () ;

    itemHeader () ;
	switch ($Record['Type']) {
		case 'SSCC':
			$_qtyTxt = 'SSCC Qty' ;
			break;
		case 'gtin':
			$_qtyTxt = 'GTIN Qty' ;
			break;
		default:
			$_qtyTxt = 'SSCC Qty!' ;
			break;
	}

	if ($Record['Type']=='SSCC' and $_batchpreadvice) {
		$_query = sprintf("
					select  i.id, if(i.batchnumber is null or i.batchnumber='0', '',i.batchnumber) as Batch, sum(i.quantity) as PcsQty, count(c.id) as SsscQty
					from (item i, container c, stock s, stocklayout sl) 
					where i.active=1 and i.variantcodeid=%d 
					and c.id=i.containerid  and c.active=1 
					and s.id=c.stockid and s.type='fixed' 
					and sl.stockid=c.stockid and sl.Position=c.Position and sl.type='sscc'
					group by if(i.batchnumber is null or i.batchnumber='0', '',i.batchnumber)
					order by if(i.batchnumber is null or i.batchnumber='0', '',i.batchnumber)", 
					$Record['VariantCodeId'] ); //die($_query) ;
		$_res = dbQuery($_query) ;
		listStart () ;
		listHead('Batch', 100) ;
		listHead('SsscQty', 80) ;
		listHead('Available', 80) ;
		listHead('On stock', 80) ;
		listHead('In pick other orders', 80) ;
		listHead('',10) ;
		listRow () ;
		while ($_row = dbFetch($_res)) {

			$_subquery = 'select sum(plb.OrderedQuantity-if(plb.PackedQuantity>0,plb.PackedQuantity,0)) as OtherOrderQuantity from (pickorder p, pickorderline pl, pickorderlinebatch plb) 
						where pl.pickorderid=p.id and p.active=1 and p.packed=0 and p.type="SSCC" and pl.active=1 and pl.done=0 and pl.variantcodeid='. $Record['VariantCodeId'] . 
						' and plb.pickorderlineid=pl.id and plb.active=1 and plb.batch="'.$_row['Batch'].'"' ;
			$_subres = dbQuery($_subquery) ;
			$_subrow = dbFetch ($_subres) ;
			if ($Record['PickOrderLineId']>0) 	
				$_orderedqty=(int)tableGetFieldWhere('PickOrderLineBatch','OrderedQuantity','PickOrderLineId='. $Record['PickOrderLineId']. ' and batch="'.$_row['Batch'].'"' . ' and active=1') ;
			else
				$_orderedqty=0 ;
			$_available =  (int)$_row['SsscQty'] - ((int)$_subrow["OtherOrderQuantity"]-(int)$_orderedqty); 
//if ($_row['Batch']=='73608951') die('test - ' .$_subquery) ;
			listField ($_row['Batch']) ;
			if ($_available>0) {
				listFieldRaw (formText ('OrderedQuantities['.$_row['Batch'].']', (int)$_orderedqty, 3)) ;
			} else {
				listField('No Available') ;
			}
			listField ($_available) ;
			listField ((int)$_row['SsscQty']) ;
			listField ((int)$_subrow["OtherOrderQuantity"]) ;
			if ($_row['Batch']=='' or is_null($_row['Batch']) or $_row['Batch']=='0') 
				$_batch = 'None' ;
			else 
				$_batch = $_row['Batch'] ;
			listFieldRaw (formHidden ('AvailableQuantities['.$_batch.']', (int)$_available, 3)) ;

			listRow () ;
		}
		listEnd () ;
		dbQueryFree($_res) ;
	} else {
		itemFieldRaw ($Record['Type'].' qty', formText ('OrderedQuantity', (int)$Record['OrderedQuantity']==0?1:(int)$Record['OrderedQuantity'], 3)) ;
	}

	
    itemSpace () ;

    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
