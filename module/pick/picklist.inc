<script type='text/javascript'>
  function VariantUpdate () {
  var col = document.appform.elements ;
  var n = 0;

  if (window.event.keyCode != 13)  return ;

  // Reset display and focus to Variant field
  var sku = document.getElementsByName ('VariantCode')[0].value ;
  document.getElementsByName ('VariantCode')[0].value = '' ;
  document.getElementsByName ('PrevCode')[0].value = sku ;
  appFocus ('VariantCode') ;
  appLoaded () ;

  // Update Remaining Qty
  var tmp = 'RemQty[' + sku + ']' ;
  if (!document.getElementsByName(tmp)[0]) {
  alert('SKU ' + sku + ' not found') ;
  return ;
  }
  if (document.getElementsByName(tmp)[0].value == 0)  {
  alert ('Variant ' + sku + ' allready fully picked') ;
  return ;
  } else
  document.getElementsByName ('PrevRemQty')[0].value = --document.getElementsByName(tmp)[0].value ;

  // Update Picked Qty
  var tmp = 'PickQty[' + sku + ']' ;
  document.getElementsByName(tmp)[0].value ++ ;

  // Update info fields
  var tmp = 'Desc[' + sku + ']' ;
  document.getElementsByName('PrevDesc')[0].value = document.getElementsByName(tmp)[0].value ;
  var tmp = 'Color[' + sku + ']' ;
  document.getElementsByName('PrevColor')[0].value = document.getElementsByName(tmp)[0].value ;
  var tmp = 'Size[' + sku + ']' ;
  document.getElementsByName('PrevSize')[0].value = document.getElementsByName(tmp)[0].value ;
  }

  function UpdateRem (variant, value) {
  var tmp = 'OrderQty[' + variant + ']' ;
  orderqty = document.getElementsByName(tmp)[0].value ;

  var tmp = 'PackQty[' + variant + ']' ;
  packqty = document.getElementsByName(tmp)[0].value ;

  var tmp = 'RemQty[' + variant + ']' ;

  document.getElementsByName(tmp)[0].value = orderqty - packqty - value ;
  }

</script>

<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;

    if (dbNumRows($Result) == 0) return "No lines to pack" ;

    $VariantCode='';

    itemStart () ;
    itemSpace () ;
    itemField ('PickOrder', sprintf('%d - %s - %s', $Record['Id'], tableGetField('Company','Name', $Record['CompanyId']), date('Y-m-d', dbDateDecode($Record['DeliveryDate'])))) ;
    itemFieldIcon ('Pick Container', (int)$Record['LastContainerId'], 'container.gif', 'containerview', (int)$Record['LastContainerId']) ; 
    if ($Record['PickUserId'])
	    itemField ('Picker', sprintf('%s %s (%s)', tableGetField('User','FirstName', $Record['PickUserId']),tableGetField('User','LastName', $Record['PickUserId']),tableGetField('User','LoginName', $Record['PickUserId']))) ;
    if (($Record['Packed']) AND ($Record['ToId']>0)){
	  itemFieldIcon ('PICK CLOSED', 'Packinglist', 'print.gif', 'shippackinglist', $Record['ToId']) ;
	  itemFieldIcon ('Shipment', TableGetField('Stock','Name',$Record['ToId']), 'shipment.gif', 'shipmentview', $Record['ToId']) ;
    }
    if ($Record['Instructions']>'')
	    itemField ('Instructions', $Record['Instructions']) ;
    itemSpace () ;
    itemEnd () ;

    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Position', 50) ;
    listHead ('Variant', 95)  ;
//    listHead ('Art.id', 95)  ;
    listHead ('Description', 300) ;
//    listHead ('Color', 95) ;
//    listHead ('Size', 95) ;
    listHead ('Remaining', 95) ;
//    listHead ('On PickPos') ;
	listhead('');

    while ($row = dbFetch($Result)) {
    	listRow () ;
    	listFieldIcon ('container.gif', 'picklistview', (int)$row['ContainerId']) ;
    	listField ($row["Position"]) ;
    	listField ($row["VariantCode"]) ;
//   	listField ($row["ArticleId"]) ;
    	listField ($row["VariantDescription"]) ;
//    	listField ($row["VariantColor"]) ;
//    	listField ($row["VariantSize"]) ;
    	ListField ($row["OrderedQuantity"]-$row["PackedQuantity"]-$row["PickedQuantity"]) ;
		listField ('') ;
/*
     	$query = sprintf ("select * from collection c, collectionmember cm
                          where c.seasonid=%d and cm.collectionid=c.id and cm.active=1 and c.active=1
                          and cm.articleid=%d and cm.articlecolorid=%d", $Record['SeasonId'], $row['ArticleId'],$row['ArticleColorId']) ;
		$res = dbQuery ($query) ;
 	  	$CollMem = dbFetch ($res) ;
		 	dbQueryFree ($res) ;
     	if ($CollMem['Cancel'])
        	listField ('CANCELLED') ;
		else {
			 if (((int)$row["OrderedQuantity"]-(int)$row["PackedQuantity"]-(int)$row["PickedQuantity"])>(int)$row['ItemQty']) {
				listField ('No ' . ' - ' . ' only ' . (int)$row['ItemQty']) ;
				// 		listField ('No ') ; // . ((int)$row["OrderedQuantity"]-(int)$row["PackedQuantity"]-(int)$row["PickedQuantity"]) . ' - ' . (int)$row['ItemQty']) ;
				//     	listField (sprintf('IQ %d, PolId %d', (int)$StockOk['ItemQty'], (int)$row['PickOrderLineId']) ) ;
			} else {
				listField ('') ;
			}
		}
*/
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;

?>
