<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;

    class PDF extends FPDF {
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now ;

	    $this->SetAutoPageBreak(false) ; 

	    // Borders
	    $this->Line(15,30,205,30) ;
	    $this->Line(15,44,205,44) ;
	    $this->Line(15,49,205,49) ;
	    $this->Line(15,280,205,280) ;

	    $this->Line(15,30,15,280) ;
	    $this->Line(205,30,205,280) ;

	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['ToCompanyId']) ;
//	    $this->Image ($ImageString, 140, 13, 64) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    // cell felter: feltbredde, felth�jde, indhold, ramme, linieskifte, Align
		
	    // Title text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(20,0,0) ;
	    $this->SetY (15) ;
		if (($Record['Packed']) AND ($Record['ToId']>0)) 
		    $this->Cell(30, 8, 'PICK ORDER CLOSED', 0, 1) ;
		else
		    $this->Cell(30, 8, 'PICK ORDER', 0, 1) ;
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(20,0,0) ;

	    $this->Cell(20, 4, 'Reference') ;
	    $this->Cell(50, 4, $Record['ConsolidatedPickOrderId'] . ' - ' . $Record['Id'] . ' - ' .$Record['Reference'], 0, 1) ;

	    $this->SetFont('Arial','',9);
	    $this->SetMargins(95,0,0) ;
	    $this->SetY (13) ;
	    $this->MultiCell(74, 4,  $Record['Instructions'], 0, 1) ;	    

	    // Header
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,0,0) ;
	    $this->SetY (31) ;
	    $this->Cell(30, 4, 'Customer') ;
	    $this->Cell(135, 4, tableGetField('Company','Name', $Record['CompanyId'])) ; // . ' in ' . $Record['Country']) ;
	    $this->Cell(10, 4, 'Page') ;
	    $this->Cell(50, 4, $this->PageNo().' of {nb}', 0, 1) ;

	    $this->Cell(30, 4, 'Delivery Date') ;
	    $this->Cell(74, 4, date('Y-m-d', dbDateDecode($Record['DeliveryDate'])), 0, 1) ;

//	    $this->Cell(30, 4, 'Picking for') ;
//	    $this->Cell(74, 4,  tableGetField('Company','Name', $Record['OwnerCompanyId']), 0, 1) ;


//		$this->Ln () ;

	    $this->SetY (45) ;
	    $this->SetFont('Arial','B',9);
	    $this->Cell(25, 4, 'Position') ;
	    $this->Cell(35, 4, 'SSCC') ;
	    $this->Cell(25, 4, 'Variant') ;
	    $this->Cell(65, 4, 'Description') ;
	    $this->Cell(15, 4, 'Ordered Quantity') ;

	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,51,6) ;
	    $this->SetY (50) ;
	    $this->SetAutoPageBreak(true, 30) ; 
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }
 
    // Variables
    $LastPage = false ;

    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 15) ; 
    $pdf->AddPage() ;
	
    $Street = ''; 
    $hl=0 ;
	$dl=100;
	
	$pdf->Cell(25, 4, 'Product Summary') ;
	$pdf->Ln () ;
	while ($row = dbFetch($Result)) {
		$_ordercols = '' ; $_joins=''; $_orderby ='' ;
		$_rem = $row["OrderedQuantity"]-$row["PackedQuantity"]-$row["PickedQuantity"] ;
		$_remtotal += $_rem ;
/*		listRow () ;
		listField ($row["VariantCode"]) ;
		listField ($row["VariantDescription"]) ;
		listField ($_rem,'align="right"') ;
*/
		$pdf->Cell(25, 4, '') ;
		$pdf->Cell(35, 4, '') ;
		$pdf->Cell(25, 4, $row['VariantCode']) ;
		$pdf->Cell(65, 4, substr($row['VariantDescription'],0,$dl)) ;
		$pdf->Cell(8, 4, $row['OrderedQuantity']-$row['PickedQuantity']-$row['PackedQuantity'], 0, 0, 'L') ;
		
		if ($Record["Packed"]) continue ;
		
		$_query = sprintf('Select c.Position as "Position", c.SSCC as SSCC, min(i.batchnumber) as Batch 
							from containerreserved cr, container c, stock s, item i
							where pickorderlineid=%d and c.id=cr.containerid and s.id=c.stockid AND s.type="fixed" and cr.active=1 and i.containerid=c.id and i.active=1 
							group by c.id
							order by position
							limit %d', $row["Id"], $_rem);
		$_res = dbQuery($_query) ;
		$_reserved = dbNumRows($_res) ;
		$_onstock = 0 ;
		While ($_row = dbFetch($_res)) {
			$row['SSCC'] = $_row['SSCC'] ;
			$row['Position'] = $_row['Position'] ;
			$row['Batch'] = $_row['Batch'] ;
			$Lines[$row['Position']][]=$row ;
			$_onstock++ ;
		}
		dbQueryFree ($_res) ;
		
		$_batchpreadvice = 1 ; 
		if (($_onstock < $_rem) and ($_batchpreadvice==0)){
			// Get AI's to sort by
			$_query= sprintf('Select * from pickorder_ai where pickorderid=%d order by SortOrder', $Id);
			$_res = dbQuery($_query) ;
	/* Build		
						select 	i.id, c.Position as "Position", c.SSCC as SSCC, v1.value as ordervalue1, v2.value as ordervalue2
						from (item i, container c)
						Left JOIN variant_ai v1 ON v1.itemid=i.id and v1.aiid=4
						Left JOIN variant_ai v2 ON v2.itemid=i.id and v2.aiid=6
						where i.active=1 and c.active=1 and i.variantcodeid=4 AND c.id=i.containerid AND c.stockid=14321
						order by ordervalue1, ordervalue2;
	*/					
			While ($_row = dbFetch($_res)) {
				$_ordercols .= sprintf(', v%d.value as ordervalue%d',$_row['SortOrder'], $_row['SortOrder']) ;
				$_joins .= sprintf(' LEFT JOIN variant_ai v%d ON v%d.itemid=i.id and v%d.aiid=%d ', $_row['SortOrder'], $_row['SortOrder'], $_row['SortOrder'],$_row['AiId']) ;
				$_orderby .= sprintf(' ordervalue%d, ', $_row['SortOrder']) ;
			}
			dbQueryFree ($_res) ;
			$_query = 	'SELECT i.Id, c.Position as "Position", c.SSCC as SSCC, c.id as ContainerId ' . $_ordercols . 
						 ' FROM (item i, container c, stock s) ' .
						 $_joins .
						 'left JOIN containerreserved cr ON cr.variantcodeid=i.variantcodeid and cr.containerid=c.id and cr.active=1
						  left join pickorderline pol ON pol.id=cr.pickorderlineid and pol.active=1
						  left join pickorder po on po.id=pol.pickorderid and po.packed=0 and po.active=1 ' .
						 ' WHERE i.active=1 and c.active=1 and i.variantcodeid=' . (int)$row['VariantCodeId'] . ' AND c.id=i.containerid AND s.id=c.stockid AND s.type="fixed" and isnull(po.id) ' .
						 ' ORDER BY ' . $_orderby . ' Position limit ' . ($_rem-$_onstock) ;
			$_res = dbQuery($_query) ;
			
	//		$_onstock = 0 ;
			While ($_row = dbFetch($_res)) {
				$row['SSCC'] = $_row['SSCC'] ;
				$row['Position'] = $_row['Position'] ;
				$Lines[$row['Position']][]=$row ;
				$_reservation = array ('ContainerId'=>$_row['ContainerId'], 'VariantCodeId'=>$row['VariantCodeId'], 'PickOrderLineId'=>$row['Id']);
				tableWrite('containerreserved', $_reservation) ;
				$_onstock++ ;
			}
			dbQueryFree ($_res) ;
		}

		if ($_onstock < $_rem) {
			$pdf->Cell(25, 4, ($_rem-$_onstock) . ' missing on Stock') ;
		} else {
			$pdf->Cell(25, 4, '') ;
		}
		$pdf->Ln () ;
	}
	dbQueryFree ($Result) ;	
	
	ksort($Lines) ;
	$pdf->Ln () ;
	$pdf->Cell(150, 4, 'SSCC Pick Lines') ;
	$pdf->Cell(15, 4, 'Batch') ;
//	$pdf->Ln () ;
    foreach ($Lines as $posline) {
		foreach ($posline as $row ) {
			if ($hl) {
				$pdf->SetFont('Arial','',9) ;
				$pdf->SetTextColor(80,80,80) ;
				$hl = 0 ;
				$dl = 35 ;
			} else {
				$pdf->SetTextColor(0,0,0) ;
				$pdf->SetFont('Arial','B',9);
				$hl = 1 ;
				$dl = 32 ;
			}

			if ($Street != substr($row['Position'],0,1)) {
				$Street = substr($row['Position'],0,1) ;
				$pdf->Ln () ;
			}
			// Lines
			$pdf->Cell(25, 4, $row['Position']) ;
			$pdf->Cell(35, 4, $row['SSCC']) ;
			$pdf->Cell(25, 4, $row['VariantCode']) ;
			$pdf->Cell(65, 4, substr($row['VariantDescription'],0,$dl)) ;
			$pdf->Cell(15, 4, $row['Batch'], 0, 0, 'R') ;
			$pdf->Ln () ;
		}
    }
     
    // Last Page generated
    $LastPage = True ;
    
    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;
    httpContent ('application/pdf', sprintf('PickOrder%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
    print ($pdfdoc) ; 

	dbQueryFree ($Result) ;
	
	// Update PickOrder.
	$PickOrder = array (
		'Printed' => 1,
		'Updated' => 0
	) ;
	tableWrite ('PickOrder', $PickOrder, (int)$Record['Id']) ;


    return 0 ;
?>
