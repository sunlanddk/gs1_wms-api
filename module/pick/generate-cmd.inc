<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'module/pick/packcsv-cmd.inc' ;
    require_once 'module/order/stockExportToWeb.php';

	global $User, $Navigation, $Record, $Size, $Id ;

	if ($_POST['OnlyFile'] == 'on') {
		return CreatePickCSV($Id) ;
		return 0 ;
	}

	
	if (isset($_POST['MaxDeliveryDate'])) $MaxDeliveryDate = $_POST['MaxDeliveryDate'] ;

	// Validate Order 
// allow
//	SELECT OrderLine.*, 
//						Article.Number AS ArticleNumber, Article.VariantColor, Article.VariantSize, Article.CustomsPositionId, 
//						Article.MaterialCountryId, 
//						Article.KnitCountryId, Article.WorkCountryId, Article.ArticleTypeId, vc.id AS VariantId
//						FROM OrderLine
//						LEFT JOIN OrderQuantity oq ON OrderLine.Id=oq.orderlineid and oq.active=1
//						LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
//						LEFT JOIN VariantCode vc ON OrderLine.articleid=vc.articleid and OrderLine.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid and vc.active=1
//						WHERE OrderLine.OrderId=%d AND OrderLine.Active=1
	$query = sprintf ("
	SELECT vc.id AS VariantId, OrderLine.*,
	  Article.Number AS ArticleNumber, Article.VariantColor, Article.VariantSize, Article.CustomsPositionId,
	  Article.MaterialCountryId,
	  Article.KnitCountryId, Article.WorkCountryId, Article.ArticleTypeId
	FROM OrderLine
	LEFT JOIN OrderQuantity oq ON OrderLine.Id=oq.orderlineid and oq.active=1
	LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	LEFT JOIN VariantCode vc ON OrderLine.articleid=vc.articleid and (OrderLine.articlecolorid=vc.articlecolorid or Article.VariantColor=0) and (oq.articlesizeid=vc.articlesizeid or Article.VariantSize=0) and vc.active=1
    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1", $Id) ;
//return $query ;
	$result = dbQuery ($query) ;
	$n = 0 ;
	while ($row = dbFetch ($result)) {
		// Validate OrderLine
	    if ($row['VariantColor'] and (int)$row['ArticleColorId'] == 0) return sprintf ('line %d, article %s: no color assigned', (int)$row['No'], $row['ArticleNumber']) ;
		if ((int)$row['Quantity'] == 0) 
			return sprintf ('line %d, article %s: zero Quantity', (int)$row['No'], $row['ArticleNumber']) ;

		// Validate Article ????
//	    if ((int)$row['CustomsPositionId'] <= 0) return sprintf ('line %d, article %s: no Customs Position', (int)$row['No'], $row['ArticleNumber']) ;
//	    if ((int)$row['MaterialCountryId'] <= 0) return sprintf ('line %d, article %s: no Material Country', (int)$row['No'], $row['ArticleNumber']) ;
//	    if ((int)$row['KnitCountryId'] <= 0) return sprintf ('line %d, article %s: no Knit Country', (int)$row['No'], $row['ArticleNumber']) ;
//	    if ((int)$row['WorkCountryId'] <= 0) return sprintf ('line %d, article %s: no Work Country', (int)$row['No'], $row['ArticleNumber']) ;

		if ($row['VariantDimension']) 
				return sprintf ('line %d, Cant generate picklines for article %s: with dimension', (int)$row['No'], $row['ArticleNumber']) ;

		// Special validation for pickOrders
		if ($row['ArticleTypeId']<>6) {
/* allow
			if (!$row['VariantColor']) 
				return sprintf ('line %d, Cant generate picklines for article %s: without colors', (int)$row['No'], $row['ArticleNumber']) ;
			if (!$row['VariantSize']) 
				return sprintf ('line %d, Cant generate picklines for article %s: without sizes', (int)$row['No'], $row['ArticleNumber']) ;
*/
			if ($row['VariantId'] > 0) 
				$VarCodeOk = 1 ; 
			else 
				return sprintf ('line %d, Cant generate picklines for article %s: without variant code', (int)$row['No'], $row['ArticleNumber']) ;
		}
		$n++ ;    
	}
	dbQueryFree ($result) ;

	if ($Record['Ready']==0) {
		if ($_POST['Ready'] != 'on')  return 'Cant generate PickOrder Since SalesOrder not ready' ;

		if ($n == 0) return 'the Order can not be set Ready when it has no OrderLines' ;

		// Set ready
		$UpdOrder = array (
			'ExchangeRate'	=> tableGetField ('Currency', 'Rate', (int)$Record['CurrencyId']),
			'Ready'			=> 1,
			'ReadyUserId'	=> $User['Id'] ,
			'ReadyDate'		=> dbDateEncode(time())
		) ;
		tableWrite ('Order',$UpdOrder, $Id) ;		

		sendStockDataToWebshop($Id) ;
	}
//return 'disabled - info at PassOn' ;

	// Find what to generate pickorderlines for.
	// First: date range if any
	if ($MaxDeliveryDate)
		$DeliveryDateLimit = " and ol.deliverydate<='" . $MaxDeliveryDate ."'" ;
	else
		$DeliveryDateLimit = "" ;
	// Second: Collectionmembers
	$query = sprintf("SELECT lot.Date as LotDate, lot.Id AS lotId, cm.* 
					  FROM (collection cl, collectionmember cm)
					  LEFT JOIN collectionlot lot on cm.collectionlotid=lot.id
					  WHERE cl.seasonid=%d and cm.collectionid=cl.id and cl.active=1 and cm.active=1", $Record['SeasonId']); 
	$result = dbQuery ($query) ;
	$CollectionMembers = array () ;
	while ($row = dbFetch ($result)) {
		$CollectionMembers[$row['ArticleId']][$row['ArticleColorId']] = $row ;
	}
    dbQueryFree ($result) ;


	$first=1 ;
	$ErrorTxt=0 ;
	$CurrentTime = time () -1  ;
/*
	$query = sprintf(
	$eval_res = dbQuery ($query) ;
	$NoOrderLines = dbNumRows($eval_res) ;
 	dbQueryFree ($eval_res) ;
*/
	$query = sprintf(
/* allow
		"SELECT o.id as OrderId, ol.id as OrderLineId, o.companyid as CompanyId, s.pickstockid as PickStockId,
				ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid, 
				oq.articlesizeid, oq.quantity as quantity, oq.Boxnumber as BoxNumber,  oq.quantityunpack as quantityunpack,
				vc.id as VariantCodeId, vc.VariantCode as VariantCode, 
				c.description as color, az.name as size
		FROM (`order` o, orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az, variantcode vc)
		LEFT JOIN season s ON o.seasonid=s.id and s.active=1 
		WHERE ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and ol.done=0 and oq.active=1
		and o.done=0 and o.ready=1 and ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid and vc.active=1
		and ol.articlecolorid=ac.id and  ac.colorid=c.id and oq.articlesizeid=az.id and o.id=%d "
*/		
		"SELECT o.id as OrderId, ol.id as OrderLineId, o.companyid as CompanyId, s.pickstockid as PickStockId, s.id as SeasonId,
				ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid,
				oq.articlesizeid, if(a.variantsize=1,oq.quantity,ol.quantity) as quantity, oq.Boxnumber as BoxNumber, oq.QuantityReceived, oq.quantityunpack as quantityunpack,
				vc.id as VariantCodeId, vc.VariantCode as VariantCode,
				c.description as color, az.name as size, a.ArticleTypeId
		FROM (`order` o, orderline ol, variantcode vc, article a)
		LEFT JOIN orderquantity oq ON  oq.orderlineid=ol.id and oq.active=1
		LEFT JOIN season s ON o.seasonid=s.id and s.active=1
	    LEFT JOIN articlecolor ac ON ol.articlecolorid=ac.id
	    LEFT JOIN color c ON  ac.colorid=c.id
	    LEFT JOIN articlesize az ON oq.articlesizeid=az.id
		WHERE ol.orderid=o.id and o.active=1 and ol.active=1 and ol.done=0
	    and o.done=0 and o.ready=1 and o.id=%d and a.id=ol.articleid
	    and ol.articleid=vc.articleid and (ol.articlecolorid=vc.articlecolorid or a.variantcolor=0) and ((oq.articlesizeid=vc.articlesizeid and (oq.quantity-oq.quantityunpack)>0) or a.variantsize=0) and vc.active=1"
		, $Id);
	$query .= $DeliveryDateLimit . ' order by o.id' ;
// return $query ;
	$res = dbQuery ($query) ;
	
	while ($row = dbFetch ($res)) {
		// Check if collectionmember is cancelled
		if  ($row['ArticleTypeId'] == 6) continue ; 
	    if ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['Cancel']) continue ;
	    // Check if Collection member is moved to a LOT later than date range
	    if (($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > '2001-01-01') AND
			($MaxDeliveryDate > '2001-01-01') AND 
	        ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > $MaxDeliveryDate)) continue ;

	   // Check if some quantity is already delivered.
//	   $query = sprintf ("SELECT sum(quantity) as Qty from item where orderlineid=%d and articlesizeid=%d", $row['OrderLineId'],$row['articlesizeid']) ;
/*
	   $query = sprintf ("SELECT sum(if(i.credit=1,-iq.quantity,iq.quantity)) as Qty from invoiceline il, invoicequantity iq, invoice i where il.orderlineid=%d and il.active=1 and il.id=iq.invoicelineid and articlesizeid=%d and il.invoiceid=i.id", $row['OrderLineId'],$row['articlesizeid']) ;
	   $itemres = dbQuery ($query) ;
	   $itemqty = dbFetch ($itemres) ;
	   if ((int)$itemqty['Qty'] >= $row['quantity']) continue ;
       $itemqty['Qty'] = ((int)$itemqty['Qty']) > 0 ? ((int)$itemqty['Qty']) : 0 ;
*/
	   $PickQty =  $row['quantity']; // - (int)$itemqty['Qty'] ; 
	    
	    
  		// Initialize
		$PickOrder = array (	
			'Type'			=>  "SalesOrder",					// Pick order from internal customer
			'Reference'			=>  0,						// Order number 
			'FromId'			=>  0, // $PickingStock[$Record['ToCompanyId']],	
			'CompanyId'			=>  0,						
			'OwnerCompanyId'	=>  $Record['ToCompanyId'],				
			'HandlerCompanyId'	=>  $_POST['HandlerId'],		
			'Instructions'		=>  $_POST['Instructions'],		
			'Packed'			=>  0,
			'DeliveryDate'		=>  ""			// Expected pick and pack complete
		) ;

		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		// Generate PickOrders
		$PickOrder['Reference'] = $row['OrderId'] ;
		$PickOrder['ReferenceId'] = $row['OrderId'] ;
		$PickOrder['CompanyId'] = $row['CompanyId'] ;
//		if ($row['PickStockId'] > 0) $PickOrder['FromId'] = $row['PickStockId'] ;
		$PickOrder['FromId'] = $_POST['PickStockId'] ;
		$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
		
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $PickQty  - $row['quantityunpack']  ;
		$PickOrderLine['VariantDescription'] = substr($row['ArticleDescription'],0,48) ;
		$PickOrderLine['VariantColor'] = substr ($row['color'],0,49) ;
		$PickOrderLine['VariantSize'] = $row['size'] ;
		$PickOrderLine['BoxNumber'] = $row['BoxNumber'] ;
		$PickOrderLine['PrePackedQuantity'] = $row['QuantityReceived']  - $row['quantityunpack'] ;

		// Update or insert into PickOrder table.
		if ($first) {
			$first=0 ;
			$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d and Active=1 and Packed=0", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
			$Order['Instructions'] = $PickOrder['Instructions'] ;
			tableWrite ('Order', $Order, $PickOrder['ReferenceId']) ;
			if ($Record['InstructionOrderId'] > 0) {
				$_donothing = 1 ;
			} else {
				$PickInstructions = array (	
					'CustomerId'		=>  (int)$row['CompanyId'],
					'SeasonId'			=>  (int)$row['SeasonId'],
					'OrderId'			=>  (int)$row['OrderId']			
				) ;
				tableWrite ('PickInstruction', $PickInstructions) ;
			}
		}

		// Update or insert into PickOrderLine table.
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$query = sprintf ("SELECT * FROM PickOrderLine WHERE VariantCodeId=%d and PickOrderId=%d and Active=1", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$result = dbQuery ($query) ;
		if (dbNumRows($result)==0) {
			$Line['Id'] = 0 ;
		} else {
			$Line = dbFetch ($result) ;
			if ($Line['PackedQuantity']>0) {
				$PickOrderLine['PackedQuantity'] = $Line['PackedQuantity'] ;
				$PickOrderLine['PickedQuantity'] = $Line['PickedQuantity'] ;
				if (dbDateDecode($Line['ModifyDate'])>=$CurrentTime) {
					$ErrorTxt  = $ErrorTxt . sprintf('<br>SalesOrder data for PickOrder invalid: Variant more than once in order.') ;
					dbQueryFree ($result) ;
					continue ;
				}
				if ($Line['PackedQuantity']>$PickOrderLine['OrderedQuantity']) {
					$ErrorTxt  = $ErrorTxt . sprintf('<br>Allready packed %d order cant be set to %d for %s %s %s', 
						$Line['PackedQuantity'], $PickOrderLine['OrderedQuantity'], $PickOrderLine['VariantDescription'], $PickOrderLine['VariantColor'], $PickOrderLine['VariantSize']) ;
					$PickOrderLine['OrderedQuantity'] = $Line['PackedQuantity'] ;
				}
			} 
		}
		dbQueryFree ($result) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $Line['Id']) ;
	 }
 	dbQueryFree ($res) ;

	// Delete pickorderlines not active in sales order anymore
	if ($first) return 'No lines created' ;

	$query = "UPDATE PickOrderLine SET ACTIVE=0 WHERE ModifyDate<" . "'" . dbDateEncode($CurrentTime) . "'" . " AND PickOrderId=" . $PickOrderId  ;
//return $ErrorTxt . $query ;
	$res2 = dbQuery ($query) ;
	dbQueryFree ($res2) ;

	IF ($Record['PickOrderState']!="")  {
		$PickOrder['Updated'] = 1 ;
		tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
	}
	
	if ($_POST['TransferFile'] == 'on' and $ErrorTxt==0) $ErrorTxt = CreatePickCSV($Id) ;

	return $ErrorTxt ;

?>
