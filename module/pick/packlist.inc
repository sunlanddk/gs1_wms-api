<script type='text/javascript'>
  function VariantUpdate (event) {
  var col = document.appform.elements ;
  var n = 0;

  var key = event.keyCode || event.charCode;
  if (key != 13)  return ; // CR is 13, tab is 9

  // Reset display and focus to Variant field
//  var sku = document.getElementsByName ('VariantCode')[0].value ;
  var sku = parseInt(document.getElementsByName ('VariantCode')[0].value) ;
  document.getElementsByName ('VariantCode')[0].value = '' ;
  //  document.getElementsByName ('PrevCode')[0].value = sku ;
  appFocus ('VariantCode') ;
  appLoaded () ;

  // Update Remaining Qty
  var tmp = 'RemQty[' + sku + ']' ;
  if (!document.getElementsByName(tmp)[0]) {
  alert('SKU ' + sku + ' not found') ;
  return ;
  }
  if (document.getElementsByName(tmp)[0].value == 0)  {
  alert ('Variant ' + sku + ' allready fully picked') ;
  return ;
  } else
  --document.getElementsByName(tmp)[0].value ;
  var tmp = 'TotalRemaining' ;
 --document.getElementsByName(tmp)[0].value ;

  // Update Picked Qty
  var tmp = 'PickQty[' + sku + ']' ;
  document.getElementsByName(tmp)[0].value ++ ;
  var tmp = 'TotalScanned' ;
 ++document.getElementsByName(tmp)[0].value ;

  // Update info fields
  xmlhttp=GetXmlHttpObject();
  if (xmlhttp==null) {
  alert ("Browser does not support HTTP Request");
  return;
  }
  var url="index.php?nid="+2062 ;
  url += "&sku="+sku ;
  var now = new Date () ;
  url += '&inst=' + now.getTime() ;
  xmlhttp.onreadystatechange=stateChanged;
  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);
  }

  function stateChanged()
  {
  if (xmlhttp.readyState==4) {
  document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
  }
  }

  function GetXmlHttpObject()
  {
  if (window.XMLHttpRequest) {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
  if (window.ActiveXObject) {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
  }

  function UpdateRem (variant, value) {
  var tmp = 'OrderQty[' + variant + ']' ;
  orderqty = document.getElementsByName(tmp)[0].value ;

  var tmp = 'PackQty[' + variant + ']' ;
  packqty = document.getElementsByName(tmp)[0].value ;

  var tmp = 'RemQty[' + variant + ']' ;

  document.getElementsByName(tmp)[0].value = orderqty - packqty - value ;
  }

  function ClearRest (variant, value) {
  if (!window.confirm('Do you want to reset values from this position')) return false ;

  var elmnts = document.all.tags("input");

  var below=0 ;
  for(var i=0; i< elmnts.length; i++)
		{
			if (!elmnts[i].name.indexOf('OrderQty[' + variant + ']')) {
        below=1;
      }
    
      if (!elmnts[i].name.indexOf("OrderQty"))
        if (below===1) {
          document.getElementsByName(elmnts[i].name.replace("Order", "Pick"))[0].value = 0;
          document.getElementsByName(elmnts[i].name.replace("Order", "Rem"))[0].value = elmnts[i].value - document.getElementsByName(elmnts[i].name.replace("Order", "Pack"))[0].value ;
        }
    }
  }


  function CheckAll () {
//  alert('Hallo') ;
    var n ;
//    var elmnts = document.all.tags("input");
   var elmnts = document.getElementsByTagName("input");
		for(var i=0; i< elmnts.length; i++)
		{
			if (!elmnts[i].name.indexOf("OrderQty")) {
				var orderqty = elmnts[i].value;
        var packqty = document.getElementsByName(elmnts[i].name.replace("Order", "Pack"))[0].value ;
        document.getElementsByName(elmnts[i].name.replace("Order", "Pick"))[0].value = orderqty - packqty;
        document.getElementsByName(elmnts[i].name.replace("Order", "Rem"))[0].value = 0;
      }
    }
  }

</script>

<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;


//    if (dbNumRows($Result) == 0) return "No lines to pack" ;
    while ($row = dbFetch($Result)) {
		$Lines[] = $row ; 
		$TotalRemaining += ($row["OrderedQuantity"]-$row["PackedQuantity"]) ;
		$TotalOrdered  += $row["OrderedQuantity"] ;
		$TotalPrevious += $row["PackedQuantity"] ;
	}

    $VariantCode='';
    formStart () ;

    itemStart () ;
    itemSpace () ;
    if ($Record['ImportError']) {  
      itemField ('ALERT !!!', '') ;
      itemField ('Import Error', $Record['ImportError']) ;
      itemField ('ALERT !!!', '') ;
      itemSpace () ;
      itemSpace () ;
    }
    if ($Record['Updated']) {  
      itemField ('ALERT !!!', '') ;
      itemField ('DONT PACK', 'Order changed since pickorder printed - print and pick again.') ;
      itemField ('ALERT !!!', '') ;
      itemSpace () ;
      itemSpace () ;
    }
//    itemField ('PickOrder', sprintf('%d - %s %s - %s', $Record['Reference'], tableGetField('Company','Name', $Record['CompanyId']), $Record['OrderReference'], date('Y-m-d', dbDateDecode($Record['DeliveryDate'])))) ;
    itemField ('PickOrder', sprintf('%d (CO%d) - %s - %s', $Record['Id'], $Record['ConsolidatedPickOrderId'], tableGetField('Company','Name', $Record['CompanyId']), date('Y-m-d', dbDateDecode($Record['DeliveryDate'])))) ;

	itemFieldIcon ('Last Container', (int)$Record['LastContainerId'], 'container.gif', 'containerview', (int)$Record['LastContainerId']) ; 
    if ($Record['ToId']>0){
      if ($Record['Packed']) {
		    itemFieldIcon ('PICK CLOSED', 'Packinglist', 'print.gif', 'shippackinglist', $Record['ToId']) ;
		    itemFieldIcon ('Shipment', TableGetField('Stock','Name',$Record['ToId']), 'shipment.gif', 'shipmentview', $Record['ToId']) ;
      } else {
        if (tableGetFieldWhere ('stock', 'Ready', sprintf('Id=%d', $Record['ToId']))) {
		      itemFieldIcon ('Prev. partly', 'Packinglist', 'print.gif', 'shippackinglist', $Record['ToId']) ;
		      itemFieldIcon ('Part Shipment', TableGetField('Stock','Name',$Record['ToId']), 'shipment.gif', 'shipmentview', $Record['ToId']) ;
        }
      }
	}
    if ($Record['Instructions']>'')
	    itemField ('Instructions', $Record['Instructions']) ;
    itemSpace () ;
    itemEnd () ;

if (!$Record['Packed']) {  
    // Outher table, 1st field
    printf ("<table class=item><tr><td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;    
    // Scanning header 
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Item Code', formText ('VariantCode', $VariantCode, 14, '', 'onkeypress="VariantUpdate(event)"')) ;
    itemSpace () ;
    itemFieldRaw ('Gross Weight', formText ('GrossWeight', '', 10, 'text-align:right;') . ' kg') ;
    itemSpace () ;
    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    print '<div id="txtHint"><b>Info on last scanned code will be listed here.</b></div>' ;

    // Outher table, 2nd field
    printf ("<br></td><td width=50%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
    itemStart () ;
    itemHeader () ;
    itemField ('', 'Pick to') ;
    itemFieldRaw ('Container', formText ('ContainerId', '', 6)) ;
    itemField ('', 'or new container at') ;
	$Record['ToId'] = TableGetFieldWhere('Stock','Id',sprintf('ID=%d AND Active=1 AND Done=0 AND Ready=0', (int)$Record['ToId'])) ;
    If ($Record['ToId']==0)
	    itemFieldRaw ('Stock', formDBSelect ('StockId', $Record['ToId'], sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    else {
		itemFieldRaw ('', formHidden('StockId', $Record['ToId'], 8,'','')) ;
		switch (TableGetField('Stock','Type',$Record['ToId'])) {
		case 'fixed' :
			itemFieldIcon ('Stock', TableGetField('Stock','Name',$Record['ToId']), 'stock.gif', 'stockview', $Record['ToId']) ;
			break ;

		case 'transport' :
			itemFieldIcon ('Transport', TableGetField('Stock','Name',$Record['ToId']), 'transport.gif', 'transportview', $Record['ToId']) ;
			break ;

		case 'shipment' :
			itemFieldIcon ('Shipment', TableGetField('Stock','Name',$Record['ToId']), 'shipment.gif', 'shipmentview', $Record['ToId']) ;
			break ;
		}
    }
    itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 33, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:150px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemEnd () ;
    // Outher table, end
    printf ("<br></td></tr></table>\n") ;
}
    // List
    listClear () ;
    listStart () ;
    listRow () ;
 
    listHead ('Variant', 35)  ;
    listHead ('Article', 30)  ;
    listHead ('Description', 50) ;
    listHead ('Color', 50) ;
    listHead ('Size', 20) ;
    listHead ('Ordered', 20) ;
    listHead ('Previous', 20) ;
    listHead ('Pending', 30) ;
//    listHead ('Position', 20) ;
    if (!$Record['Packed']) listHead ('Scanned', 30) ;
    listHead ('Import Error', 100) ;
    listHead ('', 1) ;
    listHead ('', 1) ;
//    listHead ('', 1) ;
//    listHead ('', 1) ;
//    listHead ('', 1) ;
    listHead ('', 1) ;
    $n = 0 ;

	listRow () ;
	listfield('Totals') ; listfield('') ; listfield('') ; listfield('') ; listfield('') ;
	listfield($TotalOrdered) ;
	listfield($TotalPrevious>0?$TotalPrevious:'') ;
	ListFieldRaw (formText('TotalRemaining', $TotalRemaining, 8,'','disabled')) ;
	if (!$Record['Packed']) 
		ListFieldRaw (formText('TotalScanned', (int)$TotalScanned, 8,'','disabled')) ;
	else 
		listField('Completed') ;
	listRow () ;
	listfield();
		
//    while ($row = dbFetch($Result)) {
	foreach ($Lines as $row) {
    	listRow () ;
    	listField ($row["VariantCode"]) ;
		if ($Record['Type'] == 'SalesOrder' and $row["VariantModelRef"]!= '')
			listField ($row["ArticleNumber"] . ' (' . $row["VariantModelRef"] . ')') ;
		else 
			listField ($row["ArticleNumber"]) ;
    	listField ($row["VariantDescription"]) ;
    	listField ($row["VariantColor"]) ;
    	listField ($row["VariantSize"]) ;
    	listField ($row["OrderedQuantity"]) ;
    	ListField ($row["PackedQuantity"]) ;
		if ($row["PickedQuantity"]>0) $row["PrePackedQuantity"] = 0 ; // FUB specific when prepached done by Alpi.
		
		ListFieldRaw (formText(sprintf('RemQty[%s]',$row["VariantCode"]), $row["OrderedQuantity"]-$row["PackedQuantity"]-$row["PickedQuantity"]-$row["PrePackedQuantity"], 8,'','disabled')) ;
/*
    	$query = sprintf ("select * from collection c, collectionmember cm
                          where c.seasonid=%d and cm.collectionid=c.id and cm.active=1 and c.active=1
                          and cm.articleid=%d and cm.articlecolorid=%d", $Record['SeasonId'], $row['ArticleId'],$row['ArticleColorId']) ;
        $res = dbQuery ($query) ;
 	  	$CollMem = dbFetch ($res) ;
		 	dbQueryFree ($res) ;
     	if ($CollMem['Cancel'])
        	listField ('Y-CANCEL') ;
		else
          listField ($row["Position"]) ;
*/
		if (!$Record['Packed']) 
			ListFieldRaw (formText(sprintf('PickQty[%s]',$row["VariantCode"]), 0, 8,'',sprintf('onchange="UpdateRem(this.id, this.value)" ondblclick="ClearRest(this.id, this.value)" id=%s',$row["VariantCode"]))) ;
		$n++ ;

    	$_errTxt=explode(':',$row["ImportError"]) ;
		listField($_errTxt[1]) ;
		ListFieldRaw (formHidden(sprintf('OrderQty[%s]',$row["VariantCode"]), $row['OrderedQuantity'], 8,'','')) ;
		ListFieldRaw (formHidden(sprintf('PackQty[%s]',$row["VariantCode"]), $row['PackedQuantity'], 8,'','')) ;
//      ListFieldRaw (formHidden(sprintf('Desc[%s]',$row["VariantCode"]), $row['VariantDescription'], 8,'','')) ;
//      ListFieldRaw (formHidden(sprintf('Color[%s]',$row["VariantCode"]), $row['VariantColor'], 8,'','')) ;
//      ListFieldRaw (formHidden(sprintf('Size[%s]',$row["VariantCode"]), $row['VariantSize'], 8,'','')) ;
		ListFieldRaw (formHidden(sprintf('PickOrderLineId[%s]',$row["VariantCode"]), $row['PickOrderLineId'], 8,'','')) ;
    }

    listEnd () ;
    dbQueryFree ($Result) ;

    formEnd () ;
    return 0 ;

?>
