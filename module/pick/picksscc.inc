<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/navigation.inc' ;

    ?>
    
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   	<?php 
		if((int)$Record['ToId'] > 0 && (int)$Record['Packed'] === 1){
			?>
			<div id="appoutbound">
		        <h2>Outbound: <?php echo (isset($Record['Id']) === true ? 'From preadvice #'.$Record['Id']: 'New') ?></h2>
		        <h3 style="color:green">Outbound shipment created</h3>

		    <?php 
			    itemStart () ;
			    itemFieldIcon ('Shipment', tableGetField('Stock','Name',(int)$Record['ToId']), 'shipment.gif', 'shipmentview', (int)$Record['ToId']) ;
			    itemEnd () ;
                if((int)$Record['Gtin'] > 0){
                    itemStart () ;
                    itemFieldIcon ('Pending GTIN pack', '', 'cart.gif', 'picklist', (int)$Record['Gtin']) ;
                    itemEnd () ;
                }
			    return;
			?>
		</div>
			<?php 
		}
	?>
    <div id="appoutbound">
        <p class="hiddenkeyboard">{{str}}</p>
        <h2>Outbound: <?php echo (isset($Record['Id']) === true ? 'preadvice #'.$Record['Id'] . ' - ' . $Record['Reference']: 'New shipment without preadvice') ?></h2>
        <h3>Ordered quantity: <?php echo (int)$Record['TotalQuantity'] ?> </h3>
        <h3>Scanned / Packed quantity: {{items.length + inTotalnotscanable}}</h3>
		<input class="idhidden" type="hidden" value="<?php echo $Id ?>">
        <input class="stockid" type="hidden" value="<?php echo (isset($Record['ToId']) ? $Record['ToId'] : 0)  ?>">

		</br>
        </br>
        </br>
        <h2># Pallet with SSCC</h2>
        <table id="alextest" class="list">
            <tr bgcolor="#FFFFFF">
                <td class="listhead" width="10"><p class="listhead">VariantCode</p></td>
                <td class="listhead" width="35"><p class="listhead">Description</p></td>
                <td class="listhead" width="35"><p class="listhead">Pending quantity</p></td>
                <td class="listhead" width="10"><p class="listhead">Ordered quantity</p></td>
                <td class="listhead" width="10"><p class="listhead">Scanned quantity</p></td>
            </tr>
            <template v-for="(item, index) in variants">
                <tr :key="index">
                    <td class="list"><p class="listtall">{{item.variant}}</p></td>
                    <td class="list"><p class="listtall">{{item.description}}</p></td>
                    <td class="list"><p class="listtall">{{item.pqty}}</p></td>
                    <td class="list"><p class="listtall">{{item.qty}}</p></td>
                    <td class="list"><p class="listtall">{{item.sqty}}</p></td>
                </tr>
            </template>
            <tr>
                <td class="list"><p class="listtall">Total</p></td>
                <td class="list"><p class="listtall"></p></td>
                <td class="list"><p class="listtall">{{totalpending}}</p></td>
                <td class="list"><p class="listtall">{{inTotal}}</p></td>
                <td class="list"><p class="listtall">{{total}}</p></td>
            </tr>
        </table>
        </br>
        <div v-if="notscanable.length > 0">
        <h2># Pallets without SSCC</h2>
        <table id="alextest" class="list">
            <tr bgcolor="#FFFFFF">
                <td class="listhead" width="10"><p class="listhead">VariantCode</p></td>
                <td class="listhead" width="35"><p class="listhead">Description</p></td>
                <td class="listhead" width="35"><p class="listhead">Pending quantity</p></td>
                <td class="listhead" width="10"><p class="listhead">Ordered quantity</p></td>
                <td class="listhead" width="10"><p class="listhead">Packed quantity</p></td>
            </tr>
            <template v-for="(item, index) in notscanable">
                <tr :key="index">
                    <td class="list"><p class="listtall">{{item.variant}}</p></td>
                    <td class="list"><p class="listtall">{{item.description}}</p></td>
                    <td class="list"><p class="listtall">{{item.pqty}}</p></td>
                    <td class="list"><p class="listtall">{{item.qty}}</p></td>
                    <td class="list"><p class="listtall">{{item.sqty}}</p></td>
                </tr>
            </template>
            <tr>
                <td class="list"><p class="listtall">Total</p></td>
                <td class="list"><p class="listtall"></p></td>
                <td class="list"><p class="listtall">{{totalnotscanable}}</p></td>
                <td class="list"><p class="listtall">{{inTotalnotscanable}}</p></td>
                <td class="list"><p class="listtall">{{totalscan}}</p></td>
            </tr>
        </table>
        </br>
        </br>
        </div>
        <table id="alextest" class="list">
            <tr bgcolor="#FFFFFF">
                <td class="listhead" width="10"><p class="listhead">Position</p></td>
                <td class="listhead" width="35"><p class="listhead">SSCC</p></td>
                <td class="listhead" width="35"><p class="listhead">Variant</p></td>
                <td class="listhead" width="35"><p class="listhead">Description</p></td>
            </tr>
            <template v-for="(item, index) in items">
                <tr :key="index">
                    <td class="list"><p class="listtall">{{item.position}}</p></td>
                    <td class="list"><p class="listtall">{{item.sscc}}</p></td>
                    <td class="list"><p class="listtall">{{item.variant}}</p></td>
                    <td class="list"><p class="listtall" :class="{ red: item.cansave == false }">{{item.description}}</p></td>
                </tr>
            </template>
        </table>
        </br>
        </br>
        </br>
        </br>
        </br>
        </br>
        <div class="form">
            <div class="button" v-if="cansave == 1 && '<?php echo $Record['twinPacked']; ?>' > 0  && totalpending == 0 && '<?php echo $Record['TotalQuantity']; ?>' > 0" v-on:click="createOutbound()" >Depart outbound shipment</div>
            <div class="button" v-if="cansave" v-on:click="saveToLoading()" >Save</div>
            <div class="button" v-if="!cansave">The shipment can't be departed because one or more SSCC is not in the system.</div>
            <div class="button" v-on:click="reset()" >Reset</div>
        </div>
        <input type="hidden" class="ownercompanyid" name="ownercompanyid" value="<?php echo $User['CustomerCompanyId'] ?>">
        <input type="hidden" class="ownercompanyid" name="ownercompanyid" value="<?php echo $User['CustomerCompanyId'] ?>">
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/shipping-outbound.js?v=8"></script>
<style>
    #appoutbound{
        position: relative;
    }
    #appoutbound table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .red{
        color: red !important;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        float: left;
        cursor: pointer;
    }
    .button:disabled{
        opacity: 0.5;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
</style>

<?php
    return 0 ;

?>
