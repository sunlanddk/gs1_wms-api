<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    if (dbNumRows($Result) == 0) return "No Orders to pick" ;

    formStart () ;
    
    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Order', 95)  ;
    listHead ('Customer', 150) ;
    listHead ('Delivery', 95) ;
    listHead ('Total Qty', 95) ;
    listHead ('Pick Lines', 95) ;
    listHead ('State', 95) ;
    listHead ('Picker', 95) ;
    listHead ('Print', 95) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
    	listFieldIcon ('cart.gif', 'pickorderlines', (int)$row['Id']) ;
    	listField ($row["Reference"]) ;
    	listField ($row["CompanyName"]) ;
    	listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
    	listField ($row["Qty"]) ;
    	listField ($row["NoLines"]) ;
    	listField ($row["State"]) ;
    	listField ($row["PickUser"]) ;
    	ListFieldRaw (formCheckbox (sprintf('Printnote[%d]', (int)$row['Id']), 0)) ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    formEnd () ;

?>

<?php

    return 0 ;
?>
