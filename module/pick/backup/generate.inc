<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Order', $Record['Id']) ;
    itemField ('Pick State', $Record['PickOrderState']) ;
    flag ($Record, 'Ready') ;
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Handler', formDBSelect ('HandlerId', 2, 
		"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE Internal=1 AND Active=1 ORDER BY Name", 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Pick Stock', formDBSelect ('PickStockId', $Record['SeasonPickStockId'] , sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 ORDER BY Value'), 'width:250px;')) ;
    itemSpace () ;
    itemFieldRaw ('Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:100px;')) ;

    itemEnd () ;
    formEnd () ;
    return 0 ;
?>
