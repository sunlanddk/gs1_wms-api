<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
	'PickUserId'		=> array ('type' => 'integer',	'check' => true),
	'DeliveryDate'		=> array ('type' => 'date',	'mandatory' => true,	'check'	=> true),
	'Packed'			=> array ('type' => 'checkbox',	'check' => true),
	'Picked'			=> array ('type' => 'checkbox',	'check' => true),
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Packed':
	    case 'Picked':
		// Ignore if not setting flag
		if (!$changed) return false ;
		return true ;

	    case 'PickUserId' :
			if (!$changed) return false ;
			break ;

	    case 'DeliveryDate' :
			if (!$changed) return false ;
//			if ($Record['Packed']) 
//				return 'Cant change delivery when packed' ;
			break ;
	}

	if (!$changed) return false ;
	return true ;	
    }
    
    switch ($Navigation['Parameters']) {
	case 'pick' :
	case 'pack' :
	    break ;

	default :
	    break ;
    }
     
    $res = saveFields ('PickOrder', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'pick' :
	    return navigationCommandMark ('pickorderlines', (int)$Record['Id']) ;
	case 'pack' :
	    return navigationCommandMark ('pickitems', (int)$Record['Id']) ;
    }

    return 0 ;    
?>
