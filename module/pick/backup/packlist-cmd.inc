<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/variant.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

	// Is anything scanned?
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			break ;
		default:
		if (!is_array($_POST['PickQty'])) return 'No items to be packed' ;
	}

	$TotalQuantity=0;
	foreach ($_POST['PickQty'] as $VariantCode => $qty) {
		$TotalQuantity += $qty ;
	}

    // Get Fields
    $fields = array (
		'ContainerId'		=> array ('type' => 'integer'),
		'StockId'		=> array ('type' => 'integer'),
		'ContainerTypeId'	=> array ('type' => 'integer'),
		'Position'		=> array (),
		'GrossWeight'		=> array ('type' => 'decimal',	'format' => '12.3'),
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
//return 'prut' ;

	if ($TotalQuantity>0) { // Only do anything with items and containers if something is scanned.

	// check item qty in pick locatiosn is sufficient
	foreach ($_POST['PickQty'] as $VariantCode => $qty) {
		if ($qty==0) continue ;
		if ($VariantCode <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;

		// Get Variant Information
		$query = sprintf ('SELECT VariantCode.*, Container.StockId as PickStockId
							FROM VariantCode 
							LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
							WHERE VariantCode.VariantCode=%s AND VariantCode.Active=1', $VariantCode) ;
// return  $query ;
		$res = dbQuery ($query) ;
		$VariantCodeInfo = dbFetch ($res) ;
		dbQueryFree ($res) ;
		
		// Is the PickStock the items main pick stock? If not use aleternative pickstock
		if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0)) 
			$VariantCodeInfo['PickContainerId'] = 0;	// Future opt: Use containerid from pickcontainer table (variantid, container)
		
		if (($VariantCodeInfo['PickContainerId'] == 0) and ($Record['Type']=='SalesOrder'))
			$query = sprintf ('SELECT * FROM Item, Container WHERE Item.ContainerId=Container.Id And Container.StockId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Item.Active=1',
							 $Record['FromId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
		else
			$query = sprintf ('SELECT * FROM Item WHERE ContainerId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1',
							 $VariantCodeInfo['PickContainerId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
// return  $query ;
		// Do the checking
		$res = dbQuery ($query) ;
		$PickedQuantity = 0;
		if ($VariantCodeInfo['VariantUnit'] and $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']]>0) // Handle when items are picked in packs of more pcs's
			$RemScannedQty = $qty * (int)$PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']] ;
		else
			$RemScannedQty = $qty ;
		$ReqQty=$RemScannedQty ;

		while ($Item = dbFetch ($res)) {
			if ($Item['Id'] > 0) {
				if ($Item['Quantity'] >= $RemScannedQty) {
					$PickedQuantity += $RemScannedQty ;
					break ;
				} else {
					$RemScannedQty -= $Item['Quantity'] ;
					$PickedQuantity += $Item['Quantity'] ;
				}
			} else {
				return sprintf ('Invalid Item fetch') ;
			}
		}		
		// Check if enough items
		if ($PickedQuantity < $ReqQty)
			return sprintf('Pack canceled: Variant %s has only %d items on stock but %d(%d) is scanned. FromId %d, PickStockId %d, query %s', $VariantCode, $PickedQuantity, $ReqQty, $qty, $Record['FromId'], $VariantCodeInfo['PickStockId'], $query  ) ;					
		dbQueryFree ($res) ;
	}
//return 0 ;
	// Container creation and validation
    if ($fields['ContainerId']['value'] > 0) {
	// Existing Container !
	// Lookup information
	$query = sprintf ('SELECT Container.*, Stock.Name AS StockName, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $fields['ContainerId']['value']) ;
	$res = dbQuery ($query) ;
	$Container = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$Container['Id'] != $fields['ContainerId']['value']) return sprintf ('Container %d not found', $fields['ContainerId']['value']) ;
	if ($Container['StockReady']) return sprintf ('no operations allowed on Container %d', (int)$Container['Id']) ;
	// Any Gross Weight?
	if ($fields['GrossWeight']['value'] == 0) return 'please type new Gross weight for Container after adding items' ;
	$Container_update = array (
	    'GrossWeight' => $fields['GrossWeight']['value'],
	) ;
	tableWrite ('Container', $Container_update, (int)$Container['Id']) ;
	// 	$fields['StockId']['value'] = $Container['StockId'] ;
 
    } else {
	// New Container
	// Any Stock location ?
	// if ($fields['StockId']['value'] == 0) return 'please select new Location' ;
	if ($fields['StockId']['value'] == 0) {
		// Make new shipment
		if ($Record['Type']=='SalesOrder') {
			$query = sprintf ('SELECT * FROM `Order` WHERE Id=%d AND Active=1', $Record['ReferenceId']) ;
			$result = dbQuery ($query) ;
			$Order = dbFetch ($result) ;
			dbQueryFree ($result) ;
		    $shipment = array (
				'Name'				=> sprintf('%s %s', $Record['Type'], $Record['Reference']),
				'Description'		=> $Record['OrderTypeId']==10?'@':'',
				'DepartureDate'		=> $Record['DeliveryDate'],			    
				'CompanyId'			=> $Record['CompanyId'],
				'FromCompanyId'		=> $Record['OwnerCompanyId'],
				'Type'				=> 'shipment',
				'DeliveryTermId'	=> $Order['DeliveryTermId'],
				'CarrierId'			=> $Order['CarrierId'],
				'AltCompanyName'	=> $Order['AltCompanyName'],
				'Address1'			=> $Order['Address1'],
				'Address2'			=> $Order['Address2'],
				'ZIP'				=> $Order['ZIP'],
				'City'				=> $Order['City'],
				'CountryId'			=> $Order['CountryId']
			) ;
		} else {
		    $shipment = array (
				'Name'				=> sprintf('%s %s', $Record['Type'], $Record['Reference']),
				'Description'		=> '',
				'DepartureDate'		=> $Record['DeliveryDate'],			    
				'CompanyId'			=> $Record['CompanyId'],
				'FromCompanyId'		=> $Record['OwnerCompanyId'],
				'Type'				=> 'shipment',
				'DeliveryTermId'	=> TableGetField('Company','DeliveryTermId',$Record['CompanyId']),
				'CarrierId'			=> TableGetField('Company','CarrierId',$Record['CompanyId']),
				'Address1'			=> TableGetField('Company','DeliveryAddress1',$Record['CompanyId']),
				'Address2'			=> TableGetField('Company','DeliveryAddress2',$Record['CompanyId']),
				'ZIP'				=> TableGetField('Company','DeliveryZip',$Record['CompanyId']),
				'City'				=> TableGetField('Company','DeliveryCity',$Record['CompanyId']),
				'CountryId'			=> TableGetField('Company','DeliveryCountryId',$Record['CompanyId'])
			) ;
		}
		$fields['StockId']['value']=tableWrite ('Stock', $shipment) ;
	}	
	// Get Stock
	$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
	$result = dbQuery ($query) ;
	$Stock = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
	if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

	// Any ContainerType ?
	if ($fields['ContainerTypeId']['value'] == 0) return 'please select type for new Container' ;

	// Any Gross Weight?
	if ($fields['GrossWeight']['value'] == 0) return 'please type Gross weight for new Container' ;

	// Get ContainerType
	$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
	$result = dbQuery ($query) ;
	$ContainerType = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;

	// Create new Container
	$Container = array (
	    'StockId' => (int)$Stock['Id'],
	    'ContainerTypeId' => (int)$ContainerType['Id'],
	    'Position' => $fields['Position']['value'],
	    'TaraWeight' => $ContainerType['TaraWeight'],
	    'GrossWeight' => $fields['GrossWeight']['value'],
	    'Volume' => $ContainerType['Volume']
	) ;
	$Container['Id'] = tableWrite ('Container', $Container) ;
	$Container['StockName'] = $Stock['Name'] ;
    }
	// END CONTAINER VALIDATION AND CREATION

    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => sprintf ('Items picked to Container %d at %s', (int)$Container['Id'], $Container['StockName'])
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
        
	$Error_txt=0 ;
    // Process scanned quantities
	foreach ($_POST['PickQty'] as $VariantCode => $qty) {
		//		return sprintf ('Var %s, qty %d', $VariantCode, $qty) ;
		if ($qty==0) continue ;
		if ($VariantCode <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;

		// Get Variant Information
		$query = sprintf ('SELECT VariantCode.*, Container.StockId as PickStockId
							FROM VariantCode 
							LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
							WHERE VariantCode.VariantCode=%s AND VariantCode.Active=1', $VariantCode) ;
		$res = dbQuery ($query) ;
		$VariantCodeInfo = dbFetch ($res) ;
		dbQueryFree ($res) ;
		
		// Is the PickStock the items main pick stock? If not use aleternative pickstock
		if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0)) 
			$VariantCodeInfo['PickContainerId'] = 0;	// Future opt: Use containerid from pickcontainer table (variantid, container)		

		// decrease item qty in pick location (in pick container) and make new item.
		if (($VariantCodeInfo['PickContainerId'] == 0) and ($Record['Type']=='SalesOrder'))
			$query = sprintf ('SELECT Item.* FROM Item, Container WHERE Item.ContainerId=Container.Id And Container.StockId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Item.Active=1',
							 $Record['FromId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
		else
			$query = sprintf ('SELECT * FROM Item WHERE ContainerId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1',
							 $VariantCodeInfo['PickContainerId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
		$res = dbQuery ($query) ;

		$PickedQuantity = 0;						
		if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
			$RemScannedQty = $qty * $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']] ;
		else
			$RemScannedQty = $qty ;
		$ReqQty=$RemScannedQty ;

		while ($Item = dbFetch ($res)) {
			if ($Item['Id'] > 0) {
				if (($Item['Quantity'] > $RemScannedQty) or ($Item['Quantity'] == $RemScannedQty)) {
					$PickedQuantity += $RemScannedQty ;

					// Split Item and create new item with complete qty.
					$Item['Quantity'] -= $RemScannedQty ;
					tableWrite ('Item', $Item, $Item['Id']) ;
					If ($Item['Quantity'] == 0) 
						tableDelete ('Item', $Item['Id']) ;
					
					$Item['PreviousContainerId']=$Item['ContainerId'] ;
					$Item['ContainerId']=$Container['Id'] ;
					$Item['ParentId']=$Item['Id'] ;
					$Item['Quantity'] = $RemScannedQty ;

					// Allocate Items to sales orderlines for pickorders based on salesorders.
//					if (($Record['Type']=='SalesOrder') and ($Stock['Type']=='shipment')){
					if ($Record['Type']=='SalesOrder') {
						$query = sprintf ('
							SELECT OrderLine.Id as Id FROM OrderLine
							LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.ArticleSizeId=%d AND OrderQuantity.Active=1 
							WHERE	OrderLine.OrderId=%d And OrderLine.ArticleId=%d AND 
									OrderLine.ArticleColorId=%d AND OrderLine.Active=1',
									$VariantCodeInfo['ArticleSizeId'],$Record['ReferenceId'],
									$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'] ) ;
						$olres = dbQuery ($query) ;
						$OrderLine = dbFetch ($olres) ;
						dbQueryFree ($olres) ;
						if ($OrderLine['Id']>0) 
							$Item['OrderLineId'] = $OrderLine['Id'] ;	
						else
							return sprintf('SKU %s in pickorder but not found in salesorder %d',
										$VariantCode, $Record['ReferenceId'],
										$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId']) ;
					}

					unset ($Item['Id']) ;
					tableWrite ('Item', $Item) ;
					break ;
				} else {
					$RemScannedQty -= $Item['Quantity'] ;
					$PickedQuantity += $Item['Quantity'] ;

					// delete Item on stock and create new item with item qty.
					$ItemQty = $Item['Quantity'];
					$Item['Quantity'] = 0 ;
					tableWrite ('Item', $Item, $Item['Id']) ;
					tableDelete ('Item', $Item['Id']) ;
					
					$Item['PreviousContainerId']=$Item['ContainerId'] ;
					$Item['ContainerId']=$Container['Id'] ;
					$Item['ParentId']=$Item['Id'] ;
					$Item['Quantity'] = $ItemQty ;

					// Allocate Items to sales orderlines for pickorders based on salesorders.
//					if (($Record['Type']=='SalesOrder') and ($Stock['Type']=='shipment')){
					if ($Record['Type']=='SalesOrder') {
						$query = sprintf ('
							SELECT OrderLine.Id as Id FROM OrderLine
							LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.ArticleSizeId=%d AND OrderQuantity.Active=1 
							WHERE	OrderLine.OrderId=%d And OrderLine.ArticleId=%d AND 
									OrderLine.ArticleColorId=%d AND OrderLine.Active=1',
									$VariantCodeInfo['ArticleSizeId'],$Record['ReferenceId'],
									$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'] ) ;
						$olres = dbQuery ($query) ;
						$OrderLine = dbFetch ($olres) ;
						dbQueryFree ($olres) ;
						if ($OrderLine['Id']>0) 
							$Item['OrderLineId'] = $OrderLine['Id'] ;	
						else
							return sprintf('SKU %s in pickorder but not found in salesorder %d',
										$VariantCode, $Record['ReferenceId'],
										$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId']) ;
					}

					unset ($Item['Id']) ;
					tableWrite ('Item', $Item) ;
				}
			} else {
				return sprintf ('Invalid Item fetch') ;
			}
		}		
		// Check if enough items
		if ($PickedQuantity < $ReqQty) {
			$Error_txt=$Error_txt . sprintf('Pack not complete for variant %s: Only %d items on stock for %d(%d) scanned', $VariantCode, $PickedQuantity, $ReqQty, $qty ) ;
			if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
				$qty=(int)($PickedQuantity / $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']]) ;
			else
				$qty=$PickedQuantity;
			}
		dbQueryFree ($res) ;
		
		// Update qty in PickOrderLine				
		$PickOrderLine['PackedQuantity'] = $_POST['PackQty'][$VariantCode] + $qty  ;
		tableWrite ('PickOrderLine', $PickOrderLine, $_POST['PickOrderLineId'][$VariantCode]) ;
	}
	} // END TotalQuantity >0
	
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			// Update PickOrder.
			if ($TotalQuantity==0) {
				if ($Record['LastContainerId']==0) 
					return 'Nothing to ship' ;
				else 
					$Container['Id']=(int)$Record['LastContainerId'];
			}
			$PickOrder = array (
				'Packed'			=> 1,
				'LastContainerId' => (int)$Container['Id'],
				'ToId'			=> (int)$fields['StockId']['value']
			) ;
			if ($Navigation['Parameters'] == 'partship') {
				$PickOrder['Packed']=0 ;
				$PickOrder['Printed']=0 ;
			}
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			
			if ($Record['Type']=='SalesOrder') {
				$shipment = array (
					'Ready'			=> 1,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'			=> 1,
					'DepartedUserId'	=> $User['Id'],
					'DepartedDate'		=> dbDateEncode(time()),
					'PartShip'			=> 0
				) ;
				if ($Navigation['Parameters'] == 'partship') $shipment['PartShip'] = 1 ; 
			} else {
				$shipment = array (
					'Ready'			=> 1,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'			=> 0,
					'DepartedUserId'	=> $User['Id'],
					'DepartedDate'		=> dbDateEncode(time()),
					'Invoiced'			=> 1,
					'InvoicedUserId'	=> $User['Id'],
					'InvoicedDate'		=> dbDateEncode(time()),
					'Done'			=> 0,
					'DoneUserId'	=> $User['Id'],
					'DoneDate'		=> dbDateEncode(time())
				) ;
			}
			tableWrite ('Stock', $shipment, (int)$fields['StockId']['value']) ;
//		    return navigationCommandMark ('shipmentview', $Stock['Id']) ;
			break;
		default :
			if ($TotalQuantity==0) return 'No items to be packed' ;

			// Update PickOrder.
			$PickOrder = array (
				'LastContainerId' => (int)$Container['Id'],
				'ToId' => (int)$fields['StockId']['value']
			) ;
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			break ;
    }

	return $Error_txt;	
?>
	
