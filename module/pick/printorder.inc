<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;

    class PDF extends FPDF {
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now ;

	    $this->SetAutoPageBreak(false) ; 

	    // Borders
	    $this->Line(15,30,205,30) ;
	    $this->Line(15,44,205,44) ;
	    $this->Line(15,49,205,49) ;
	    $this->Line(15,280,205,280) ;

	    $this->Line(15,30,15,280) ;
	    $this->Line(205,30,205,280) ;

	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['ToCompanyId']) ;
//	    $this->Image ($ImageString, 140, 13, 64) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    // cell felter: feltbredde, felth�jde, indhold, ramme, linieskifte, Align
		
	    // Title text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(20,0,0) ;
	    $this->SetY (15) ;
		if (($Record['Packed']) AND ($Record['ToId']>0)) 
		    $this->Cell(30, 8, 'PICK ORDER CLOSED', 0, 1) ;
		else
		    $this->Cell(30, 8, 'PICK ORDER', 0, 1) ;
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(20,0,0) ;

	    $this->Cell(20, 4, 'Reference') ;
	    $this->Cell(50, 4, $Record['ConsolidatedPickOrderId'] . ' - ' . $Record['Id'] . ' - ' .$Record['Reference'], 0, 1) ;

	    $this->SetFont('Arial','',9);
	    $this->SetMargins(95,0,0) ;
	    $this->SetY (13) ;
	    $this->MultiCell(74, 4,  $Record['Instructions'], 0, 1) ;	    

	    // Header
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,0,0) ;
	    $this->SetY (31) ;
	    $this->Cell(30, 4, 'Customer') ;
		if ($Record['CompanyId']>0)
			$this->Cell(135, 4, tableGetField('Company','Name', $Record['CompanyId'])) ;
		else
			$this->Cell(135, 4, '') ;
	    $this->Cell(10, 4, 'Page') ;
	    $this->Cell(50, 4, $this->PageNo().' of {nb}', 0, 1) ;

	    $this->Cell(30, 4, 'Delivery Date') ;
	    $this->Cell(74, 4, date('Y-m-d', dbDateDecode($Record['DeliveryDate'])), 0, 1) ;

	    $this->Cell(30, 4, 'Picking for') ;
		if ($Record['OwnerCompanyId']>0)
			$this->Cell(74, 4,  tableGetField('Company','Name', $Record['OwnerCompanyId']), 0, 1) ;


//		$this->Ln () ;

	    $this->SetY (45) ;
	    $this->SetFont('Arial','B',9);
	    $this->Cell(25, 4, 'ArticleNo') ;
//	    $this->Cell(55, 4, 'Description') ;
//   	$this->Cell(45, 4, 'Color') ;
//	    $this->Cell(15, 4, 'Size') ;
	    $this->Cell(80, 4, 'Description') ;
	    $this->Cell(35, 4, 'GTIN') ;
	    $this->Cell(15, 4, 'Position') ;
	    $this->Cell(15, 4, 'Pick Qty') ;
	    $this->Cell(15, 4, 'Conf. Qty') ;

	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,51,6) ;
	    $this->SetY (50) ;
	    $this->SetAutoPageBreak(true, 30) ; 
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }
 
    // Variables
    $LastPage = false ;

    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 15) ; 
    $pdf->AddPage() ;
	
    $Street = ''; 
    $hl=0 ;
    while ($row = dbFetch($Result)) {
       	$query = sprintf ("select * from collection c, collectionmember cm
                          where c.seasonid=%d and cm.collectionid=c.id and cm.active=1 and c.active=1
                          and cm.articleid=%d and cm.articlecolorid=%d", $Record['SeasonId'], $row['ArticleId'],$row['ArticleColorId']) ;
	    $res = dbQuery ($query) ;
 	  	$CollMem = dbFetch ($res) ;
		dbQueryFree ($res) ;
     	if ($CollMem['Cancel']) continue ;

        if ($hl) {
		    $pdf->SetFont('Arial','',9) ;
			$pdf->SetTextColor(80,80,80) ;
			$hl = 0 ;
			$dl = 35 ;
        } else {
			$pdf->SetTextColor(0,0,0) ;
		    $pdf->SetFont('Arial','B',9);
			$hl = 1 ;
			$dl = 32 ;
        }

		if ($Street != substr($row['Position'],0,1)) {
			$Street = substr($row['Position'],0,1) ;
			$pdf->Ln () ;
		}
		// Lines
/*		if (substr($row['VariantCode'],0,1)=='9') 
			$pdf->Cell(25, 4, $row['ArticleNumber']) ;
		else
			$pdf->Cell(25, 4, $row['VariantCode']) ;
*/
		$pdf->Cell(25, 4, $row['ArticleNumber']) ;

		$pdf->Cell(80, 4, substr($row['VariantDescription'],0,$dl+20)) ;
		$pdf->Cell(35, 4, $row['VariantCode']) ;
//		$pdf->Cell(45, 4, substr($row['VariantColor'],0,($dl-5))) ;
//		$pdf->Cell(15, 4, $row['VariantSize']) ;
		$pdf->Cell(15, 4, $row['Position']) ;
		$pdf->Cell(15, 4, $row['OrderedQuantity']-$row['PickedQuantity']-$row['PackedQuantity'], 0, 0, 'R') ;
		$pdf->Cell(15, 4, '_______') ;
		$pdf->Ln () ;
    }
     
    // Last Page generated
    $LastPage = True ;
    
    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;
    httpContent ('application/pdf', sprintf('PickOrder%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
    print ($pdfdoc) ; 

	dbQueryFree ($Result) ;
	
	// Update PickOrder.
	$PickOrder = array (
		'Printed' => 1,
		'Updated' => 0
	) ;
	tableWrite ('PickOrder', $PickOrder, (int)$Record['Id']) ;


    return 0 ;
?>
