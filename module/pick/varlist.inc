<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    itemStart () ;
    itemSpace () ;
    itemFieldIcon ('Project', $Record['Name'], 'season.png', 'seasonview', $Id) ; 
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemSpace () ;
    itemEnd () ;


   // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Variant', 95)  ;
    listHead ('Position', 45) ;
    listHead ('Article', 95) ;
    listHead ('Description', 95) ;
    listHead ('Color', 50) ;
    listHead ('ColorDesc', 95) ;
    listHead ('Size', 50) ;
    listHead ('Stock', 95) ;
    listHead ('Container', 95) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
	listFieldIcon ('stock.gif', 'variantinventory', (int)$row['Id']) ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["Position"]) ;
    	listField ($row["ArticleNumber"]) ;
    	listField ($row["VariantDescription"]) ;
    	listField ($row["VariantNumber"]) ;
    	listField ($row["VariantColor"]) ;
    	listField ($row["VariantSize"]) ;
    	listField ($row["StockName"]) ;
    	listField ($row["ContainerId"]) ;
	}    	

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
