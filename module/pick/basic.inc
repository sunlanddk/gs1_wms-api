<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, 
	    sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), 
	    tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }
    function flagRevert (&$Record, $field) {
	if ($Record[$field]) {
	    itemFieldRaw ($field, 
					   formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . 
					                       tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', 
					                       $Record[$field.'UserId'])) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
		$Id=-1 ;
	    // Default values
	    $Record['DeliveryDate'] = dbDateEncode(time()) ;
//	    $Record['OwnerCompanyId'] = $User['CompanyId'] ;
	    break ;
	case 'pick' : 
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Id', $Record['Id']) ;
//	    itemSpace () ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;


    switch ($Navigation['Parameters']) {
		case 'new' :
			break ;
    }
	itemFieldRaw ('Reference', formText ('Reference', $Record['Reference'], 36)) ;
//    itemSpace () ;
	if ((int)$Record['OwnerCompanyId']>0)
		$companyId = (int)$Record['OwnerCompanyId'] ;
	else
		$companyId = ($User['CustomerCompanyId']) ;
    itemFieldRaw ('Owner',  formDBSelect ('OwnerCompanyId', (int)$companyId, 'SELECT Id, CONCAT(Name," - ",Number) AS Value FROM Company Where Active=1 and TypeCustomer=1 ORDER BY Name', 'width:250px; ')) ;
    itemFieldRaw ('DeliveryDate', formDate ('DeliveryDate', dbDateDecode($Record['DeliveryDate']))) ;
    itemSpace () ;
    itemFieldRaw ('PickUp',  formDBSelect ('PickUpCompanyId', 0, 'SELECT Id, CONCAT(Name," - ",Number) AS Value FROM Company Where Active=1 and (Company.Internal=1 or TypePickUp=1) ORDER BY Name', 'width:250px; ')) ;
    itemFieldRaw ('Delivery',  formDBSelect ('CompanyId', $Record['CompanyId'], 'SELECT Id, CONCAT(Name," - ",Number) AS Value FROM Company Where Active=1 and (TypeCustomer=1 or TypeEndCustomer=1)ORDER BY Name', 'width:250px; ' )) ;
    itemSpace () ;
	echo '<tr><td colspan=2> Delivery address if no Delivery Company is selected:</td></tr>' ;
    itemFieldRaw ('Name', formText ('CompanyName', $Record['CompanyName'], 36)) ;
    itemFieldRaw ('Address1', formText ('Address1', $Record['Address1'], 36)) ;
    itemFieldRaw ('Address2', formText ('Address2', $Record['Address2'], 36)) ;
    itemFieldRaw ('ZIP', formText ('ZIP', $Record['ZIP'], 20)) ;
    itemFieldRaw ('City', formText ('City', $Record['City'], 36)) ;
    itemFieldRaw ('Country',  formDBSelect ('CountryId', (int)$Record['CountryId'], 'SELECT Id, CONCAT(Name," - ",Description) AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px; ')) ;
    itemSpace () ;
    itemFieldRaw ('Instructions', formtextArea ('Instructions', $Record['Instructions'], 'width:100%;height:50px;')) ;
//    itemSpace () ;
    itemFieldRaw ('Picker', formDBSelect ('PickUserId', (int)$Record['PickUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND User.Login=1 ORDER BY Value'), 'width:250px;', array(0 => '--none--'))) ;  
    itemSpace () ;
	if ($Navigation['Parameters']=='new' or $Record['Type']=='SSCC') {
		echo '<tr><td colspan=2>  Sort Option: (if nothing is specified, then neareast position is used)</td></tr>' ;
	//	itemField ('Sort Option', 'Order: (if nothing is specified, then neareast position is used)') ;
		$_query = 'SELECT * from ai WHERE SortOption>0' ;
		$_res=dbQuery($_query) ;
		while ($_row=dbFetch($_res)) {
			if ($Navigation['Parameters']=='new') {
				$_sortOrder = $_row['DefaultSort'] ;
				$_paiId = 0 ;
			} else {
				$_pquery = sprintf('SELECT * from pickorder_ai WHERE pickorderid=%d and aiid=%d', $Id, $_row['Id']) ;
				$_pres=dbQuery($_pquery) ;
				$_prow=dbFetch($_pres) ;
				$_sortOrder = $_prow['SortOrder'] ;
				$_paiId = $_prow['Id'] ;
			}
	
			itemFieldRaw ( $_row['Description'], formText ("Ai[" . $_row['Id'] . "]", $_sortOrder>0?$_sortOrder:'',2)) ;
			$_paiId = 
			itemFieldRaw ('',formHidden("PAiId[" . $_row['Id'] . "]",$_paiId , 8,'','')) ;
		}
	}
	
//    flagRevert ($Record, 'Printed') ;
    itemSpace () ;
    itemFieldRaw ('Printed', formCheckbox ('Printed', $Record['Printed'])) ;

    switch ($Navigation['Parameters']) {
		case 'new' :
			itemFieldRaw ('',formHidden("Ready", 0)) ;
			break ;
		default:
			flag ($Record, 'Ready') ;
			flagRevert ($Record, 'Packed') ;
//			flagRevert ($Record, 'Picked') ;
			break ;
    }

	itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
