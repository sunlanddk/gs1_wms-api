<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

	$sku=$_GET["sku"];
	
	// Get Variant Information
	$query = sprintf ('SELECT a.Number as ArtNumber, vc.VariantDescription as ArtDesc, co.Description as Color, az.name as Size  
				FROM (VariantCode vc, Article a) 
				LEFT JOIN ArticleSize az ON vc.articlesizeid=az.id
				LEFT JOIN ArticleColor ac ON  vc.articlecolorid=ac.id
				LEFT JOIN Color co ON ac.colorid=co.id
				WHERE vc.VariantCode=%s 
				AND vc.Active=1', 
				$sku) ;
//				die($query) ;
	$res = dbQuery ($query) ;
	$VariantCodeInfo = dbFetch ($res) ;
	dbQueryFree ($res) ;
		
    echo("<table>
		  <tr><td class=itemlabel>
				<b>Last Code:  
			</td>
			<td class=itemfield>" .
				 $sku . "<br>
			</td>
		  </tr>
		  <tr><td>
			</td>
		    <td class=itemfield><b> " . 
        	  $VariantCodeInfo['ArtDesc'] . " (" .  $VariantCodeInfo['ArtNumber'] . "); " . $VariantCodeInfo['Color'] . "; " . $VariantCodeInfo['Size'] . "</b>
			</td>
        	 
		  </tr>
		  </table>"
        );


    return 0 ;
?>
