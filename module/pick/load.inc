<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
	case 'lineedit' :
	case 'lineedit-cmd' :
	case 'linedelete-cmd' :
	    switch ($Navigation['Parameters']) {
			case 'deleteSsccLine' :
			case 'deleteGtinLine' :
			case 'newoutbound' :
			case 'newoutboundGtin' :
				$query = "SELECT * FROM VariantCode WHERE Active=1 AND Id=".$Id ;
				$result = dbQuery ($query) ;
				$Record = dbFetch ($result) ;
				dbQueryFree ($result) ;
				$Record['VariantCodeId'] = $Record['Id'] ;
				
				$_query = 'select * from pickorder where current=1 and active=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id'] ;
				$_res = dbQuery($_query) ;
				$_row = dbFetch($_res) ;
				if ($Navigation['Parameters'] == 'newoutboundGtin' or $Navigation['Parameters'] == 'deleteGtinLine' ) {
					dbQueryFree ($_res) ;
					$_query = 'select * from pickorder where active=1 and Type="GTIN" and ConsolidatedPickOrderId=' . $_row['ConsolidatedPickOrderId'] ;
					$_res = dbQuery($_query) ;
					$_row = dbFetch($_res) ;
				}
				$Record['PickOrderId'] = $_row['Id'] ;
				$Record['PickOrderReference'] = $_row['Reference'] ;
				$Record['Type'] = $_row['Type'] ;
				$Record['ConsolidatedPickOrderId'] = $_row['ConsolidatedPickOrderId'] ;
				dbQueryFree ($_res) ;

				$_query = 'select * from pickorderline where active=1 and pickorderid='.$Record['PickOrderId'].' and variantcodeid='.$Record['Id'] ;
				$_res = dbQuery($_query) ;
				$_row = dbFetch($_res) ;
				$Record['OrderedQuantity'] = $_row['OrderedQuantity'] ;
				$Record['PickOrderLineId'] = (int)$_row['Id'] ;
				dbQueryFree ($_res) ;
				break;
			Default:
				$query = 'select pl.id as Id, pl.id as PickOrderLineId, pl.OrderedQuantity as OrderedQuantity, p.id as PickOrderId, p.ConsolidatedPickOrderId as ConsolidatedPickOrderId, p.reference as PickOrderReference, 
								p.Type as Type, v.id as VariantCodeId, v.variantcode as VariantCode, v.VariantDescription 
							from (pickorderline pl, pickorder p, variantcode v) 
							where pl.active=1 and pl.id='.$Id.' and v.id=pl.variantcodeid and p.id=pl.pickorderid' ;
				$result = dbQuery ($query) ;
				$Record = dbFetch ($result) ;
				dbQueryFree ($result) ;
				
				break ;
		}
		return 0 ;
		
	case 'ziplist' :
		$query = "SELECT * FROM PickOrder WHERE Active=1 AND Id=1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id=1 ;
		$Record['Id']=1 ;
		
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

	case 'list' :
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Order',		'db' => 'po.Reference'),
			array ('name' => 'Owner',		'db' => 'oc.Name'),
			array ('name' => 'Customer',		'db' => 'c.Name'),
			array ('name' => 'CustomerNumber',		'db' => 'c.Number'),
			array ('name' => 'Country',		'db' => 'ct.Name'),
			array ('name' => 'Delivery',		'db' => 'po.DeliveryDate'),
			array ('name' => 'ImportState',			'db' => 'ImportError')
	    ) ;

//if(po.Packed>0,"Done",if(po.ToId>0,if(s.ready>0,"Part shipped","Packing"),if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")))) as State,   
	    require_once 'lib/list.inc' ;
		$queryFields =  'max(if(po.type="SSCC",po.Id,0)) as Id, max(if(po.type="GTIN",po.Id,0)) as GtinId, max(if(po.type="SSCC",po.Id,0)) as SsccId, 
					if(Po.ConsolidatedPickOrderId>0,Po.ConsolidatedPickOrderId,Po.Id) as GroupId,
					CONCAT(u.FirstName," ",u.LastName," (",u.Loginname,")") as PickUser, 
					if(po.Packed>0,"Done", if (po.updated, "Updated", if(po.printed>0, if(po.ToId>0,	
					if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId=0,"","Assigned"))))) as State, 
					po.Reference as Reference, po.Type as PickType, o.reference as OrderReference,
					c.Name as CompanyName, c.Number as CompanyNumber, ct.Name as CountryName,dt.Name as DeliveryCountryName, po.CompanyName as DeliveryCompanyName,
					oc.Name as OwnerCompanyName, po.DeliveryDate as DeliveryDate, 
					sum(if(po.type="SSCC",1,0)) as NoLines, 
					sum(if(po.type="GTIN",1,0)) as GtinNoLines, 
					if (po.ToId>0,DepartedDate,"2001-01-01") as DepartedDate, 
					sum(if(po.type="SSCC",pol.PackedQuantity,0)) as PackedQty,
					sum(if(po.type="SSCC",pol.OrderedQuantity,0)) as Qty,
					sum(if(po.type="GTIN",pol.PackedQuantity,0)) as GtinPackedQty,
					sum(if(po.type="GTIN",pol.OrderedQuantity,0)) as GtinQty
					' ;
	    
		$queryTables = '(PickOrder po, PickOrderLine pol, Company oc )
					LEFT JOIN Stock s ON po.toid=s.id 
					LEFT JOIN Company c ON c.id=po.CompanyId
					LEFT JOIN Country ct ON c.countryid=ct.id
					LEFT JOIN Country dt ON po.countryid=dt.id
					LEFT JOIN User u ON po.PickUserId=u.Id
					 LEFT JOIN `Order` o On o.id=po.referenceid' ;


	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }

		    $queryTables = $queryTables . 
					 ' LEFT JOIN `Order` ON `Order`.Id=po.ReferenceId' ;
				
		    // Clause
		    $queryClause = sprintf ('`Order`.SeasonId=%d', $Id) ;
				$queryClause = sprintf ('po.Active=1 And pol.PickOrderId=po.Id  
						and po.OwnerCompanyId=oc.Id and pol.Active=1 and po.Active=1 and `Order`.SeasonId=%d
						GROUP BY po.Id', $Id) ;
		    break ;

		case 'sscc' :
		case 'ssccpack' :
			$fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Id',			'db' => 'po.ConsolidatedPickOrderId'),
				array ('name' => 'Reference',	'db' => 'po.Reference'),
//				array ('name' => 'Customer',	'db' => 'c.Name'),
				array ('name' => 'Delivery',	'db' => 'po.CompanyName'),
				array ('name' => 'Date',		'db' => 'po.DeliveryDate'),
				array ('name' => 'Owner',		'db' => 'oc.Name')
			) ;
			$queryClause = 'po.Type="SSCC" and po.Ready=1 And po.Active=1 And pol.PickOrderId=po.Id And po.CompanyId=c.Id and po.OwnerCompanyId=oc.Id and pol.Active=1 and po.packed=0 GROUP BY GroupId' ;
			$queryClause = sprintf("po.Ready=1 And po.Active=1 And pol.PickOrderId=po.Id and po.OwnerCompanyId=oc.Id 
									and pol.Active=1 and po.packed=0 and (po.ownercompanyid=%d or po.handlerCompanyId=%d) 
									GROUP BY GroupId", $User['CustomerCompanyId'], $User['CustomerCompanyId']) ;
			break ;

		case 'inbound' :
			$fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Reference',		'db' => 'po.Reference'),
				array ('name' => 'Supplier',		'db' => 'c.Name'),
				array ('name' => 'Delivery',		'db' => 'po.CompanyName'),
				array ('name' => 'Date',		'db' => 'po.DeliveryDate'),
			) ;
			$queryClause = 'po.Type="Inbound" and po.Ready=1 And po.Active=1 And pol.PickOrderId=po.Id and po.OwnerCompanyId=oc.Id and pol.Active=1 and po.packed=0 GROUP BY po.Id' ;
			break ;

		default :
			if ($Navigation['Function'] == 'list') {
				$queryClause = sprintf ('po.Active=1 And po.Packed=0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1
						GROUP BY po.Id', $User['CompanyId']) ;
				// Navigation
				// Pass all parameters for the list function to the zip function
				$param = 'param=' . $Navigation['Parameters'] ;
				if ($listParam != '') $param .= '&' . $listParam ;
				navigationSetParameter ('ziplist', $param) ;
			} else {
				$queryClause = sprintf ('po.Active=1 And po.ToId>0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1
						GROUP BY po.Id', $User['CompanyId']) ;
			}
		    break ;
	    }
		
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $query = 
	     sprintf ("SELECT Container.`Position` AS `Position`, PickOrderLine.Id AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
				Article.Number as ArticleNumber, PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, PickOrderLine.VariantSize as VariantSize,
				PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, PickOrderLine.PickedQuantity as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity ORDER BY Container.`Position`", $Id) ;
	    $Result2 = dbQuery ($query) ;
    
	    $query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return listView ($Result2, 'packlist') ;

	case 'varview' :
		$query = sprintf ("SELECT * FROM VariantCode WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;


	case 'picksscc' :

		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;

		$query = sprintf ("SELECT sum(OrderedQuantity) as Quantity FROM PickOrderLine WHERE Active=1 AND PickorderId=%d", $Id) ;
		$result = dbQuery ($query) ;
		$res = dbFetch ($result) ;
		$Record['TotalQuantity'] = $res['Quantity'];
		dbQueryFree ($result) ;

		$query = sprintf ("SELECT * FROM Pickorder WHERE Active=1 AND Type='GTIN' AND ConsolidatedPickOrderId=%d", $Record['ConsolidatedPickOrderId']) ;
		$result = dbQuery ($query) ;
		$res = dbFetch ($result) ;
		if(isset($res['Packed']) === true){
			$Record['twinPacked'] = (int)$res['Packed'];
			$Record['Gtin'] = (int)$res['Id'];

		}
		else{
			$Record['twinPacked'] = 1;
			$Record['Gtin'] = 0;
		}
		dbQueryFree ($result) ;

		if((int)$Record['Packed'] > 1 && (int)$Record['ToId'] > 0){
			$Id = 0;
		}


		
		return;
	case 'varlist' :
	    $DescriptionField = 'if (VariantCode.VariantDescription="" or isnull(VariantCode.VariantDescription), Article.Description, VariantCode.VariantDescription)' ;
	    $ColorField = 'if (VariantCode.VariantColorDesc="" or isnull(VariantCode.VariantColorDesc), Color.Description,VariantCode.VariantColorDesc)' ;
	    $ColorCodeField = 'if (VariantCode.VariantColorCode="" or isnull(VariantCode.VariantColorCode), Color.Number,VariantCode.VariantColorCode)' ;
	    $SizeField = 'if (VariantCode.VariantSize="" or isnull(VariantCode.VariantSize), ArticleSize.Name,VariantCode.VariantSize)' ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Variant',		'db' => 'VariantCode'),
		array ('name' => 'Position',		'db' => 'Position'),
		array ('name' => 'Description',	'db' => $DescriptionField),
		array ('name' => 'Color',		'db' => $ColorCodeField),
		array ('name' => 'ColorDesc',		'db' => $ColorField),
		array ('name' => 'Size',		'db' => $SizeField),
		array ('name' => 'Stock',		'db' => 'Stock.Name'),
		array ('name' => 'Container',		'db' => 'Container.Id')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
					VariantCode.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
					$DescriptionField . ' as VariantDescription,' .
					$ColorField . ' as VariantColor,' .
					$ColorCodeField . ' as VariantNumber,' .
					$SizeField . ' as VariantSize,
					Article.Number as ArticleNumber';
		$queryTables = 'VariantCode
			 LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
			 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
			 LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
			 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
			 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
			 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
		$queryOrderBy = '' ;

	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }
			if ($Record['ExplicitMembers']==1) {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							CollectionMember.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(Collection, CollectionMember)
					 LEFT JOIN VariantCode ON CollectionMember.articleid=VariantCode.articleid AND CollectionMember.articleColorid=VariantCode.articleColorid AND VariantCode.Active=1
					 LEFT JOIN Article ON CollectionMember.ArticleId=Article.Id and Article.Active=1 
					 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
					 LEFT JOIN ArticleColor ON CollectionMember.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;

					// Clause
					$queryClause = sprintf ('Collection.SeasonId=%d AND CollectionMember.CollectionId=Collection.Id AND CollectionMember.Active=1', $Id) ;
			} else {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							Article.Id as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(`order` o, orderline ol, article)
					 LEFT JOIN ArticleSize ON article.Id=ArticleSize.ArticleId and ArticleSize.Active=1 
					 LEFT JOIN VariantCode ON ol.articleid=VariantCode.articleid AND ol.articleColorid=VariantCode.articleColorid AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.Active=1
					 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
				$queryClause = sprintf ('o.seasonid=%d and ol.orderid=o.id and ol.articleid=article.id
								and o.active=1 and ol.active=1 and article.active=1
								', $Id) ;
			}  
			$queryOrderBy = 'ArticleNumber, VariantColor, ArticleSize.DisplayOrder' ;
			break ;

		default :
		    // Display
		    $HideSeason = true ;

		    // Clause
		    $queryClause = "VariantCode.Active=1" ;
		    break ;
	    }
//return 'select ' . $queryFields . ' from ' . $queryTables . ' where ' . $queryClause ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrderBy) ;
		
	    if (is_string($Result)) return $Result ;
		$query='Select * from parameter' ;
	    $Result2 = dbQuery ($query) ;
	    return listView ($Result2, 'view') ;

	case 'picklistsscc' :
 	case 'printssccorder' :
		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
			array ('name' => 'Variant',			'db' => 'VariantCode'),
			array ('name' => 'Description',		'db' => 'PickOrderLine.VariantDescription')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'PickOrderLine.Id, variantcode.VariantCode, variantcode.VariantDescription, PickOrderLineBatch.Batch, PickOrderLineBatch.OrderedQuantity ';
		$queryTables = '(PickOrderLine, PickOrderLineBatch, variantcode)' ;
		$queryClause = sprintf ("variantcode.id=pickorderline.variantcodeid and PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 and PickOrderLineBatch.PickOrderLineId=PickOrderLine.Id and PickOrderLineBatch.Active=1", $Id) ;

        $queryFields =  ' PickOrderLine.*, variantcode.VariantDescription ';
        $queryTables = '(PickOrderLine, variantcode)' ;
        $queryClause = sprintf ("variantcode.id=pickorderline.variantcodeid and PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 ", $Id) ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'pickorderlines') ;

	case 'picklist' :
		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
 
		if ($Record['Type']=='SalesOrder') {
			$query = sprintf ("SELECT * FROM `Order` WHERE Active=1 AND Id=%d", $Record['ReferenceId']) ;
			$result = dbQuery ($query) ;
			$SalesOrder = dbFetch ($result) ;
			dbQueryFree ($result) ;
			$Record['SeasonId'] = $SalesOrder['SeasonId'] ;
 		}
 		
 
		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
			array ('name' => 'Position',			'db' => 'Container.Position'),
			array ('name' => 'Variant',			'db' => 'VariantCode.VariantCode'),
			array ('name' => 'Description',		'db' => 'VariantCode.VariantDescription')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  ' PickOrderLine.VariantDescription,PickOrderLine.OrderedQuantity,PickOrderLine.PackedQuantity,PickOrderLine.PickedQuantity, VariantCode.VariantCode, VariantCode.VariantDescription, Container.Position ';
		$queryTables = '(PickOrderLine, VariantCode) LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId' ;
		$queryClause = sprintf ("PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 AND VariantCode.Id=PickOrderLine.VariantCodeId", $Id) ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'pickorderlines') ;
	    
	case 'pickorderlist-cmd' :
	case 'ziplist-cmd' :
		$query = "SELECT * FROM PickOrder WHERE Active=1 AND Id=1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id=1 ;
		$Record['Id']=1 ;
		return 0 ;
 
	case 'pickorderlist' :
		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Reference',		'db' => 'Reference'),
		array ('name' => 'CompanyName',			'db' => 'CompanyName'),
		array ('name' => 'State',		'db' => 'State')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'po.Id as Id,CONCAT(u.FirstName," ",u.LastName," (",u.Loginname,")") as PickUser, 
							if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")) as State, po.Reference as Reference, 
							c.Name as CompanyName, po.DeliveryDate as DeliveryDate, pol.id as NoLines, 
							pol.OrderedQuantity as Qty';
	    
		$queryTables = '(PickOrder po, PickOrderLine pol, Company c)
					LEFT JOIN User u ON po.PickUserId=u.Id' ;

		$queryClause = 'po.Active=1 And po.Packed=0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id' ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'picklist') ;
	    
 	case 'basic' :
	case 'basic-cmd' :
	case 'delete-cmd' :
		if ($Navigation['Parameters']=='current') {
			$Id=(int)tableGetFieldWhere('pickorder','Id','current=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id']) ;
		}
		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;
		
	case 'generatecon' :
	case 'generatecon-cmd' :
		$query = sprintf ('SELECT o.*, o.ConsolidatedId as Id ,Season.PickStockId as SeasonPickStockId
, if(po.Packed>0,"Done", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId>0,"Assigned","")))) as PickOrderState
					FROM `Order` o
					LEFT JOIN Season ON o.SeasonId=Season.Id AND Season.Active=1
 					LEFT JOIN PickOrder po ON po.ReferenceId=O.Id and po.Type="SalesOrder" and po.packed=0 and po.Active=1 
					LEFT JOIN Stock s ON po.toid=s.id 
					WHERE o.Active=1 AND o.ConsolidatedId=%d', $Id) ;
	      $res = dbQuery ($query) ;
	      $Record = dbFetch ($res) ;
	      // dbQueryFree ($res) ;

		if (($Record['PickOrderState']=="Done")) { // or ($Record['Ready']==0)) {
			navigationPasive ('createpickorder') ;		
	    	}

		return 0 ;

 	case 'generate' :
	case 'generate-cmd' :
		$query = sprintf ('SELECT o.*, Season.PickStockId as SeasonPickStockId, Season.InitialPickInstructions as InitialPickInstructions, 
								  if(po.Packed>0,"Done", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId>0,"Assigned","")))) as PickOrderState,
								  pi.OrderId as InstructionOrderId
					FROM `Order` o
					LEFT JOIN Season ON o.SeasonId=Season.Id AND Season.Active=1
 					LEFT JOIN PickOrder po ON po.ReferenceId=O.Id and po.Type="SalesOrder" and po.packed=0 and po.Active=1 
					LEFT JOIN Stock s ON po.toid=s.id 
					LEFT JOIN pickinstruction pi ON pi.customerid=o.companyid and pi.seasonid=o.seasonid 
					WHERE o.Active=1 AND o.Id=%d', $Id) ;
	      $res = dbQuery ($query) ;
	      $Record = dbFetch ($res) ;
	      dbQueryFree ($res) ;

		if (($Record['PickOrderState']=="Done")) { // or ($Record['Ready']==0)) {
			navigationPasive ('createpickorder') ;		
	    	}

		return 0 ;

 	case 'printorder' :
 	case 'printssccorder' :
 	case 'packlist' :
	case 'packlist-cmd' :
	    if ($Id <= 0) return 0 ;

		// Open PickOrderlines
	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               if(VariantCode.PickContainerId>0,'X-NoPos+C','X-NoPos'), 
	                               Container.`Position`) AS `Position`,
				group_concat(PickOrderLine.Id) AS PickOrderLineId, VariantCode.VariantCode AS VariantCode, group_concat(PickOrderLine.ImportError) AS ImportError, 
				Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, VariantCode.ArticleSizeId as ArticleSizeId, 
				PickOrderLine.VariantCodeId AS VariantCodeId,
				VariantCode.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
				PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
				sum(PickOrderLine.OrderedQuantity) as OrderedQuantity, 
				sum(PickOrderLine.PackedQuantity) as PackedQuantity, 
				PickOrderLine.BoxNumber as BoxNumber, 
				sum(PickOrderLine.PrePackedQuantity) as PrePackedQuantity, 
				sum(PickOrderLine.PickedQuantity) as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And (PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity or isnull(PickOrderLine.PackedQuantity))
		GROUP BY VariantCode.id ORDER BY `Position`, variantcode.id, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
//		LEFT JOIN Item on Item.containerid=Container.Id and item.variantcodeid=variantcode.id and Item.active=1
//				sum(Item.Quantity) as ItemQty, 
//		GROUP BY VariantCode.id ORDER BY `Position`, variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
//die($query);
	    $Result = dbQuery ($query) ;
		$_openPickLines = (int)dbNumRows($Result) ;
		// PickOrder
	    $query = sprintf ("SELECT PickOrder.*, o.reference as OrderReference FROM PickOrder LEFT JOIN `Order` o ON o.id=PickOrder.ReferenceId WHERE PickOrder.Active=1 AND PickOrder.Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
 
		if ($Record['Type']=='SalesOrder') {
			$query = sprintf ("SELECT * FROM `Order` WHERE Active=1 AND Id=%d OR ConsolidatedId=%d", $Record['ReferenceId'], $Record['ConsolidatedId']) ;
			$result = dbQuery ($query) ;
			$SalesOrder = dbFetch ($result) ;
			dbQueryFree ($result) ;
			$Record['SeasonId'] = $SalesOrder['SeasonId'] ;
			$Record['CurrencyId'] = $SalesOrder['CurrencyId'] ;
			$Record['Country'] = tableGetField('Country','Description', $SalesOrder['CountryId']) ;
 		}
 		if (($Record['Packed']==1)) {
			navigationPasive ('packlist_pack') ;		
			navigationPasive ('packlist_ship') ;		
			navigationPasive ('packlist_partship') ;		
		}
		if (($_openPickLines>0) and ($Record['Packed']==0)) { // and toId>0 ???
			navigationPasive ('packlist_ship') ;		
			navigationPasive ('packlist_partship') ;		
		}

		return 0 ;
		
    }

    if ($Id <= 0) return 0 ;
    

    return 0 ;
?>
