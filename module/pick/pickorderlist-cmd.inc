<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;

    class PDF extends FPDF {
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now ;

	    $this->SetAutoPageBreak(false) ; 

	    // Borders
	    $this->Line(15,30,205,30) ;
	    $this->Line(15,44,205,44) ;
	    $this->Line(15,49,205,49) ;
	    $this->Line(15,280,205,280) ;

	    $this->Line(15,30,15,280) ;
	    $this->Line(205,30,205,280) ;

	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['ToCompanyId']) ;
//	    $this->Image ($ImageString, 140, 13, 64) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    // cell felter: feltbredde, felth�jde, indhold, ramme, linieskifte, Align
		
	    // Title text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(20,0,0) ;
	    $this->SetY (15) ;
		if (($Record['Packed']) AND ($Record['ToId']>0)) 
		    $this->Cell(30, 8, 'PICK ORDER CLOSED', 0, 1) ;
		else
		    $this->Cell(30, 8, 'PICK ORDER', 0, 1) ;

	    $this->SetFont('Arial','',9);
	    $this->Cell(20, 4, 'Number') ;
	    $this->Cell(50, 4, $Record['Reference'], 0, 1) ;

	    // Header
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,0,0) ;
	    $this->SetY (31) ;
	    $this->Cell(30, 4, 'Customer') ;
	    $this->Cell(135, 4, tableGetField('Company','Name', $Record['CompanyId'])) ;
	    $this->Cell(10, 4, 'Page') ;
	    $this->Cell(50, 4, $this->PageNo().' of {nb}', 0, 1) ;

	    $this->Cell(30, 4, 'Delivery Date') ;
	    $this->Cell(74, 4, date('Y-m-d', dbDateDecode($Record['DeliveryDate'])), 0, 1) ;

	    $this->Cell(30, 4, 'Picking for') ;
	    $this->Cell(74, 4,  tableGetField('Company','Name', $Record['OwnerCompanyId']), 0, 1) ;
//		$this->Ln () ;

	    $this->SetY (45) ;
	    $this->SetFont('Arial','B',9);
	    $this->Cell(15, 4, 'Position') ;
	    $this->Cell(25, 4, 'Variant') ;
	    $this->Cell(45, 4, 'Description') ;
	    $this->Cell(25, 4, 'Color') ;
	    $this->Cell(15, 4, 'Size') ;
	    $this->Cell(15, 4, 'Pick Qty') ;
	    $this->Cell(15, 4, 'Conf. Qty') ;

	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,51,6) ;
	    $this->SetY (50) ;
	    $this->SetAutoPageBreak(true, 30) ; 
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }

	if (!is_array($_POST['Printnote'])) return 'please select Pick Orders to print' ;
	// Make PDF
	$pdf=new PDF('P', 'mm', 'A4') ;
	$pdf->AliasNbPages() ;
//	$pdf->SetAutoPageBreak(true, 15) ; 
//	$pdf->AddPage() ;

	foreach ($_POST['Printnote'] as $id => $flag) {

		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;

	    $query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		$query = 
	     sprintf ("SELECT Container.`Position` AS `Position`, PickOrderLine.Id AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, PickOrderLine.VariantSize as VariantSize,
				PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, PickOrderLine.PickedQuantity as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 ORDER BY Container.`Position`", $id) ;
	    $res = dbQuery ($query) ;
	    if ($Id <= 0) return 0 ;

		$pdf->AddPage() ;		
		while ($row = dbFetch($res)) {
			// Lines
			$pdf->Cell(15, 4, $row['Position']) ;
			$pdf->Cell(25, 4, $row['VariantCode']) ;
			$pdf->Cell(45, 4, $row['VariantDescription']) ;
			$pdf->Cell(25, 4, $row['VariantColor']) ;
			$pdf->Cell(15, 4, $row['VariantSize']) ;
			$pdf->Cell(15, 4, $row['OrderedQuantity']-$row['PickedQuantity']-$row['PackedQuantity'], 0, 0, 'R') ;
			$pdf->Cell(15, 4, '_______') ;
			$pdf->Ln () ;
		}	     
	}
	// Last Page generated
	$LastPage = True ;
    
	// Generate PDF document
	$pdfdoc = $pdf->Output('', 'S') ;

	// Download
	if (headers_sent()) return 'stop' ;
	httpNoCache ('pdf') ;
	httpContent ('application/pdf', sprintf('PickOrder%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
	print ($pdfdoc) ; 
	dbQueryFree ($Result) ;

    return 0 ;
?>
