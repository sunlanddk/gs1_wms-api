<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

 	$_batchpreadvice = 1 ; 

    $fields = array (
		'PickOrderId'				=> array ('type' => 'set'),
		'VariantCodeId'				=> array ('type' => 'set'),
		'VariantCode'				=> array ('type' => 'set'),
//		'VariantDescription'		=> array ('type' => 'set'),
		'OrderedQuantity'			=> array ('type' => 'integer',		'check' => true),
	) ;

    switch ($Navigation['Parameters']) {
		case 'newoutbound' :
		case 'newoutboundGtin' :
			$fields['PickOrderId']['value'] = $Record['PickOrderId'] ;
			$fields['VariantCodeId']['value'] = $Id ;
			$fields['VariantCode']['value'] = $Record['VariantCode'] ;
			$Id = -1 ;
			break ;
		default ;
			break ;
    }
    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Size, $Id, $fields ;
		switch ($fieldname) {
			case 'OrderedQuantity' :
				if (!$changed) return false ;
				// Validate
				if ($value <= 0) return 'invalid Quantity ' . $value ;
				return true ;
			default:
				break ;
		}
		return false ;	
    }
	if ($Record['PickOrderLineId']>0) {
		$Id = $Record['PickOrderLineId'] ;
	}
	
	if ($Record['Type']=='SSCC' and $_batchpreadvice) {
		$_totalqty = 0;
		foreach ($_POST['OrderedQuantities'] as $_batch => $_orderedqty) {
			$_totalqty += $_orderedqty;
		}
		$_POST['OrderedQuantity'] = (int)$_totalqty ;
		$field['OrderedQuantity'] = array('type' => 'set', 'value' => (int)$_totalqty) ;
	}
	
    $res = saveFields ('PickOrderLine', $Id, $fields, true) ;
    if ($res) return $res ;

	if ($Record['Type']=='SSCC' and $_batchpreadvice) {
		$_pickorderlineid = ($Id>0) ? $Id : (int)$Record['Id'] ;
		tableDeleteWhere('PickOrderLineBatch','PickOrderLineId=' . $_pickorderlineid) ;
		foreach ($_POST['OrderedQuantities'] as $_batch => $_orderedqty) {
			if ($_batch=='' or is_null($_batch) or $_batch=='0') {
				$_batch='' ;
				$_postbatch = 'None' ;
			} else {
				$_postbatch = $_batch ;
			}
//return 'test ' . $_postbatch . ' - ' . $_POST['AvailableQuantities'][$_postbatch] . ' - ' . $_batch;
			if ($_orderedqty>0) {
				if ($_orderedqty>(int)$_POST['AvailableQuantities'][$_postbatch]) {
					tableDelete('PickOrderLine', $_pickorderlineid) ;
					tableDeleteWhere('PickOrderLineBatch','PickOrderLineId=' . $_pickorderlineid) ;
					return 'Not allowed to order ' .  $_orderedqty . ' for batch ' . $_batch . ', when only ' . $_POST['AvailableQuantities'][$_postbatch] . ' is available' ;
				}
				$_order = array ('PickOrderLineId'=>$_pickorderlineid, 'Batch'=> $_batch, 'OrderedQuantity'=>$_orderedqty) ;
				tableWrite('PickOrderLineBatch', $_order, $_pickOrderLineBatchId) ;
			}
		}
	}
    return 0 ;    
?>
