<?php

    require_once 'lib/table.inc' ;

	if ($Navigation['Parameters']=='reressscc') {
		$_valquery = 'select * from containerreserved cr, pickorderline pl where pl.pickorderid='. $Id . ' AND cr.pickorderlineid=pl.id and pl.active=1 and cr.active=1' ;
		$_valres = dbQuery($_valquery) ;
		if (dbNumRows($_valres)>0) return 'Reserve SSCCs can only be done when no reservations. Please delete reservation before reserving' ;
		$_query = 'select * from pickorder where id='.$Id ; 
	} else {
		$_query = 'select * from pickorder where current=1 and active=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id'] ;
	}
	$data = array (
		'Current'			=> 0,
		'Ready'				=> 1,
		'ReadyUserId'		=> $User['Id'] ,
		'ReadyDate'			=> dbDateEncode(time())
		) ;
	//SSCC pickorder
	$_res = dbQuery($_query) ;
	$_row = dbFetch($_res) ;
	dbQueryFree($_res) ;
	
	// Any pickorderlines
	$query = sprintf("SELECT pl.Id, pl.VariantCodeId, plb.Batch, plb.OrderedQuantity FROM PickOrderLine pl, PickOrderLineBatch plb 
						WHERE pl.PickOrderId=%d AND pl.Active=1
							AND plb.pickorderlineid=pl.id and plb.active=1
						", $_row['Id']) ;
//	die($query) ;
	$result = dbQuery($query) ;
	if (dbNumRows($result)>0) {
		tableWrite ('PickOrder', $data, $_row['Id']) ;

		// reserve containers for each line/batch
		while ($_linerow = dbFetch($result)) {
			if ($_linerow['Batch']=='' or is_null($_linerow['Batch']) or $_linerow['Batch']=='0') {
				$batchclause = "(i.batchnumber='' or isnull(i.batchnumber) or i.batchnumber=0)" ;
			} else {
				$batchclause = "i.batchnumber='" . $_linerow['Batch'] ."'" ;
			}
			$_reservequery = sprintf("select * from (
												Select c.id as ContainerId, c.Position as 'Position', c.SSCC as SSCC, i.batchnumber as Batch, cr.id as crid, cr.pickorderlineid, pol.pickorderid, po.id as poid, po.packed, max(PO.id) as maxPoId
												from (container c, stock s, stocklayout sl, item i)
												LEFT JOIN containerreserved cr ON cr.containerid=c.id and cr.active=1 
												left join pickorderline pol ON pol.id=cr.pickorderlineid and pol.active=1 
												left join pickorder po on po.id=pol.pickorderid and po.packed=0 and po.active=1 
												where
												c.active=1 
												and s.id=c.stockid AND s.type='fixed' 
												and sl.StockId=s.Id and sl.position=c.position AND sl.Active=1 AND sl.type='sscc'
												and i.containerid=c.id and i.active=1 and %s and i.variantcodeid=%d
												GROUP BY c.id 
												order by c.createdate 
												) tab
									where maxPoId is null limit %d", $batchclause, $_linerow['VariantCodeId'], $_linerow['OrderedQuantity']);			
//if (!($_linerow['Batch']=='' or is_null($_linerow['Batch'])))
//if($_linerow['VariantCodeId']==17) die ($_reservequery . ' - ' . $_linerow['Batch']) ;
			$_reserveresult = dbQuery($_reservequery) ;
			while ($_reserverow = dbFetch($_reserveresult)) {
				$_reservation = array ('ContainerId'=>$_reserverow['ContainerId'], 'VariantCodeId'=>$_linerow['VariantCodeId'], 'PickOrderLineId'=>$_linerow['Id']);
				tableWrite('containerreserved', $_reservation) ;
			}
			dbQueryFree($_reserveresult) ;
		}
	} else {
		tableDelete ('PickOrder', $_row['Id']) ;
	}
	dbQueryFree($result) ;
	
	// Gtin pickorder
	$_query = 'select * from pickorder where active=1 and Type="GTIN" and ConsolidatedPickOrderId=' . $_row['ConsolidatedPickOrderId'] ;
	$_res = dbQuery($_query) ;
	$_row = dbFetch($_res) ;
	dbQueryFree($_res) ;

	// Any pickorderlines
	$query = sprintf("SELECT * FROM PickOrderLine WHERE PickOrderId=%d AND Active=1", $_row['Id']) ;
	$result = dbQuery($query) ;
	if (dbNumRows($result)>0) {
		tableWrite ('PickOrder', $data, $_row['Id']) ;
	} else {
		tableDelete ('PickOrder', $_row['Id']) ;
	}
	dbQueryFree($result) ;

	if ($Navigation['Parameters']=='reressscc') {
		return navigationCommandMark('picklistsscc', $Id) ;
	}

    return 0 ;
?>
