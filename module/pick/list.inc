<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHead ('Id', 40)  ;
//    listHead ('SsccId', 30)  ;
  //  listHead ('GtinId', 30)  ;
    listHead ('Reference', 100)  ;
	switch ($Navigation['Parameters']) {
		case 'ssccpack' :
		case 'sscc' :
			break;
		default:
			listHeadIcon () ;
			break;
	}
//    listHead ('Supplier', 150) ;
    listHead ('Delivery', 180) ;
//    listHead ('Customer', 150) ;
    listHead ('Country', 50) ;
    listHead ('Date', 80) ;
    listHead ('SSCC Qty', 60) ;
    listHeadIcon () ;
//    listHead ('Packed', 50) ;
    listHead ('Lines', 45) ;
    listHead ('GTIN Qty', 60) ;
    listHeadIcon () ;
//    listHead ('Packed', 50) ;
    listHead ('Lines', 45) ;
    listHead ('State') ;
    listHead ('Owner', 150) ;
    listHead ('', 60) ;
    listHead ('', 60) ;
//    listHead ('State', 60) ;
 //   listHead ('Last Depart', 70) ;
 //   listHead ('Picker', 95) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
    	listField ($row["GroupId"]) ;
//    	listField ($row["SsccId"]) ;
//    	listField ($row["GtinId"]) ;
    	listField ($row["Reference"]) ;
  //  	listField ($row["OwnerCompanyName"] ) ;
  //  	listField ($row["CompanyName"]) ;
    	listField ($row["DeliveryCompanyName"]) ;
  //  	listField ($row["CompanyNumber"]) ;
  //  	listField ($row["CountryName"]) ;
    	listField ($row["DeliveryCountryName"]) ;
    	listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
		if ($row["Qty"]>0) {
			listField ($row["Qty"], 'align="right"') ;
			switch ($Navigation['Parameters']) {
				case 'ssccpack' :
					listFieldIcon ('pack.png', 'picksscc', (int)$row['Id']) ;
					break;
				case 'sscc' :
					listFieldIcon ('print.gif', 'picklistsscc', $row['Id']) ;
					break;
				default:
					listFieldIcon ('cart.gif', 'picklist', $row['Id']) ;
					break;
			}
	//    	listField ((int)$row["PackedQty"], 'align="right"') ;
			listField ($row["NoLines"], 'align="right"') ;
		} else {
			listField ('', 'colspan=3') ;
		}	
		if ($row["GtinQty"]>0) {
			listField ($row["GtinQty"], 'align="right"') ;
			switch ($Navigation['Parameters']) {
				case 'ssccpack' :
					listFieldIcon ('cart.gif', 'packlist', (int)$row['GtinId']) ;
					break ;
				case 'sscc' :
					listFieldIcon ('print.gif', 'picklist', (int)$row['GtinId']) ;
					break;
				default:
					listFieldIcon ('cart.gif', 'packlist', (int)$row['GtinId']) ;
					break;
			}
//	    	listField ($row["GtinPackedQty"], 'align="right"') ;
			listField ($row["GtinNoLines"], 'align="right"') ;
		} else {
			listField ('', 'colspan=3') ;
		}	
    	listField ($row["State"], 'align="left"') ;
    	listField ($row["OwnerCompanyName"] ) ;
/*
    	listField ($row["State"]) ;
		if ($row['DepartedDate']>'2001-01-01')
			listField (date('Y-m-d', dbDateDecode($row['DepartedDate']))) ;
		else
			listField ('') ;
    	listField ($row["PickUser"]) ;
*/
		$total['PackQuantity'] += $row["Qty"] ;
		$total['GtinPackQuantity'] += $row["GtinQty"] ;
//		$total['PackedQuantity'] += $row["PackedQty"] ;
		$total['LineQuantity'] += $row["NoLines"] ;
    }

    listRow () ;
    listField ('', 'colspan=5 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['PackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ('', 'colspan=2 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['GtinPackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
//    listField ($total['LineQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
