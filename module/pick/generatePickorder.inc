<?php 

function createPickorders($ordersIds, $MaxDeliveryDate, $handler, $instructions, $fromId, $transferedFile){
		global $User, $Navigation, $Record, $Size, $Id, $Config ;
		$query = sprintf ("
							SELECT vc.id AS VariantId, OrderLine.*,
							  Article.Number AS ArticleNumber, Article.VariantColor, Article.VariantSize, Article.CustomsPositionId,
							  Article.MaterialCountryId,
							  Article.KnitCountryId, Article.WorkCountryId, Article.ArticleTypeId,
							  `order`.Id as ConsolidatedId, `order`.Id, `order`.Ready
							FROM `order`
							LEFT JOIN OrderLine ON `order`.Id = Orderline.OrderId AND Orderline.Active=1
							LEFT JOIN OrderQuantity oq ON OrderLine.Id=oq.orderlineid and oq.active=1
							LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
							LEFT JOIN VariantCode vc ON OrderLine.articleid=vc.articleid and (OrderLine.articlecolorid=vc.articlecolorid or Article.VariantColor=0) and (oq.articlesizeid=vc.articlesizeid or Article.VariantSize=0) and vc.active=1 and not Article.ArticleTypeId=6
							WHERE `order`.Id IN(%s)", implode(',', $ordersIds)) ;
//return $query ;
		$result = dbQuery ($query) ;
		$n = 0 ;

		if(dbNumRows ($result) == 0) return 'No orders have benn choosen';

		while ($row = dbFetch ($result)) {
			if($row['Ready'] == 0) return 'Cant generate PickOrder Since SalesOrder '.$row['Id'].' are not ready' ;
			// Validate OrderLine
		    if ($row['VariantColor'] and (int)$row['ArticleColorId'] == 0) return sprintf ('line %d, article %s: no color assigned', (int)$row['No'], $row['ArticleNumber']) ;
			if ((int)$row['Quantity'] == 0) 
				return sprintf ('line %d, article %s: zero Quantity', (int)$row['No'], $row['ArticleNumber']) ;
			if ((int)$row['DeliveryDate'] < '2002-01-01') return sprintf ('line %d, article %s: No deliverydate', (int)$row['No'], $row['ArticleNumber']) ;

			if ($row['VariantDimension']) 
					return sprintf ('line %d, Cant generate picklines for article %s: with dimension', (int)$row['No'], $row['ArticleNumber']) ;

			// Special validation for pickOrders
			if ($row['ArticleTypeId']<>6) {

				if ($row['VariantId'] > 0) 
					$VarCodeOk = 1 ; 
				else 
					return sprintf ('line %d, Cant generate picklines for article %s: without variant code', (int)$row['No'], $row['ArticleNumber']) ;
			}
			$n++ ;    
		}

		// Find what to generate pickorderlines for.
		// First: date range if any
		if ($MaxDeliveryDate)
			$DeliveryDateLimit = " and ol.deliverydate<='" . $MaxDeliveryDate ."'" ;
		else
			$DeliveryDateLimit = "" ;
		// Second: Collectionmembers
		$query = sprintf("SELECT lot.Date as LotDate, lot.Id AS lotId, cm.* 
						  FROM (collection cl, collectionmember cm)
						  LEFT JOIN collectionlot lot on cm.collectionlotid=lot.id
						  WHERE cl.seasonid=%d and cm.collectionid=cl.id and cl.active=1 and cm.active=1", $Record['SeasonId']); 
		$result = dbQuery ($query) ;
		$CollectionMembers = array () ;
		while ($row = dbFetch ($result)) {
			$CollectionMembers[$row['ArticleId']][$row['ArticleColorId']] = $row ;
		}
	    dbQueryFree ($result) ;


		$first=1 ;
		$ErrorTxt=0 ;
		$CurrentTime = time () -1  ;

		$query = sprintf(
		
			"SELECT o.id as OrderId, ol.id as OrderLineId, o.companyid as CompanyId, s.pickstockid as PickStockId,
					ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid,
					oq.articlesizeid, if(a.variantsize=1,oq.quantity,ol.quantity) as quantity, oq.Boxnumber as BoxNumber, oq.QuantityReceived, oq.Id as QlId,
					vc.id as VariantCodeId, vc.VariantCode as VariantCode,
					c.description as color, az.name as size
			FROM (`order` o, orderline ol, variantcode vc, article a)
			LEFT JOIN orderquantity oq ON  oq.orderlineid=ol.id and oq.active=1
			LEFT JOIN season s ON o.seasonid=s.id and s.active=1
		    LEFT JOIN articlecolor ac ON ol.articlecolorid=ac.id
		    LEFT JOIN color c ON  ac.colorid=c.id
		    LEFT JOIN articlesize az ON oq.articlesizeid=az.id
			WHERE ol.orderid=o.id and o.active=1 and ol.active=1 and ol.done=0
		    and o.done=0 and o.ready=1 and o.Id IN(%s) and a.id=ol.articleid  and not a.ArticleTypeId=6
		    and ol.articleid=vc.articleid and (ol.articlecolorid=vc.articlecolorid or a.variantcolor=0) and ((oq.articlesizeid=vc.articlesizeid and oq.quantity>0) or a.variantsize=0) and vc.active=1"
			, implode(',', $ordersIds));
		$query .= $DeliveryDateLimit . ' order by o.id' ;

		$res = dbQuery ($query) ;
		
		while ($row = dbFetch ($res)) {
			// Check if collectionmember is cancelled
		    if ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['Cancel']) continue ;
		    // Check if Collection member is moved to a LOT later than date range
		    if (($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > '2001-01-01') AND
				($MaxDeliveryDate > '2001-01-01') AND 
		        ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > $MaxDeliveryDate)) continue ;

		   $PickQty =  $row['quantity'] ; // - (int)$itemqty['Qty'] ; 
		    
		    
	  		// Initialize
			$PickOrder = array (	
				'Type'			=>  "SalesOrder",					// Pick order from internal customer
				'Reference'			=>  0,						// Order number 
				'FromId'			=>  0, // $PickingStock[$Record['ToCompanyId']],	
				'CompanyId'			=>  0,						
				'OwnerCompanyId'	=>  $Record['ToCompanyId'],				
				'HandlerCompanyId'	=>  $handler,		
				'Instructions'		=>  $instructions,		
				'Packed'			=>  0,
				'DeliveryDate'		=>  ""			// Expected pick and pack complete
			) ;

			$PickOrderLine = array (	
				'VariantCodeId'			=>  "",			//
				'VariantCode'			=>  "",			// EAN code 
				'OrderedQuantity'		=>  0,			//			
				'PickedQuantity'		=>  0,			//
				'PackedQuantity'		=>  0,			//
				'PickOrderId'			=>	0,
				'VariantDescription'	=>	"",
				'VariantColor'			=>	"",
				'VariantSize'			=>	"",
				'Done'					=>  0
			) ;
		
			// Generate PickOrders
			$PickOrder['Reference'] = $row['OrderId'] ;
			$PickOrder['ReferenceId'] = $row['OrderId'] ;
			$PickOrder['ConsolidatedId'] = $Id ;
			$PickOrder['OwnerCompanyId'] = 787 ;
			$PickOrder['CompanyId'] = $row['CompanyId'] ;
	//		if ($row['PickStockId'] > 0) $PickOrder['FromId'] = $row['PickStockId'] ;
			$PickOrder['FromId'] = $fromId ;
			$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
			
			$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
			$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
			$PickOrderLine['OrderedQuantity'] = $PickQty ;
			$PickOrderLine['VariantDescription'] = substr($row['ArticleDescription'],0,48) ;
			$PickOrderLine['VariantColor'] = substr ($row['color'],0,49) ;
			$PickOrderLine['VariantSize'] = $row['size'] ;
			$PickOrderLine['BoxNumber'] = $row['BoxNumber'] ;
			$PickOrderLine['PrePackedQuantity'] = $row['QuantityReceived'] ;
			$PickOrderLine['QlId'] = $row['QlId'] ;

			// Update or insert into PickOrder table.
			if ($first) {
				$first=0 ;
				$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d and Active=1 and Packed=0", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
				$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
				$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
				//$PickOrderId = 0; // Dette er så der bliver lavet en ny.
				$Order['Instructions'] = $PickOrder['Instructions'] ;
				tableWrite ('Order', $Order, $PickOrder['ReferenceId']) ;
			}

			// Update or insert into PickOrderLine table.
			$PickOrderLine['PickOrderId'] = $PickOrderId ;
			$query = sprintf ("SELECT * FROM PickOrderLine WHERE VariantCodeId=%d and PickOrderId=%d and Active=1 and QlId=%d", $PickOrderLine['VariantCodeId'], $PickOrderId, $row['QlId']) ;
			$result = dbQuery ($query) ;
			if (dbNumRows($result)==0) {
				$Line['Id'] = 0 ;
			} else {
				$Line = dbFetch ($result) ;
				if ($Line['PackedQuantity']>0) {
					$PickOrderLine['PackedQuantity'] = $Line['PackedQuantity'] ;
					$PickOrderLine['PickedQuantity'] = $Line['PickedQuantity'] ;
					if (dbDateDecode($Line['ModifyDate'])>=$CurrentTime) {
						$ErrorTxt  = $ErrorTxt . sprintf('<br>SalesOrder data for PickOrder invalid: Variant more than once in order.') ;
						dbQueryFree ($result) ;
						continue ;
					}
					if ($Line['PackedQuantity']>$PickOrderLine['OrderedQuantity']) {
						$ErrorTxt  = $ErrorTxt . sprintf('<br>Allready packed %d order cant be set to %d for %s %s %s', 
							$Line['PackedQuantity'], $PickOrderLine['OrderedQuantity'], $PickOrderLine['VariantDescription'], $PickOrderLine['VariantColor'], $PickOrderLine['VariantSize']) ;
						$PickOrderLine['OrderedQuantity'] = $Line['PackedQuantity'] ;
					}
				} 
			}
			dbQueryFree ($result) ;
			tableWrite ('PickOrderLine', $PickOrderLine, $Line['Id']) ;
		 }
	 	dbQueryFree ($res) ;

		// Delete pickorderlines not active in sales order anymore
		if ($first) return 'No lines created' ;

		$query = "UPDATE PickOrderLine SET ACTIVE=0 WHERE ModifyDate<" . "'" . dbDateEncode($CurrentTime) . "'" . " AND PickOrderId=" . $PickOrderId  ;
	//return $ErrorTxt . $query ;
		$res2 = dbQuery ($query) ;
		dbQueryFree ($res2) ;

		IF ($Record['PickOrderState']!="")  {
			$PickOrder['Updated'] = 1 ;
			tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		}
		
		if ($transferedFile == 'on' and $ErrorTxt==0) $ErrorTxt = CreatePickConsolidatedCSV($Id) ;

		return $ErrorTxt ;

	}


?>