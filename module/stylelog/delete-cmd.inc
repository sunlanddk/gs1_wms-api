<?php

    require_once 'lib/table.inc' ;

    // Validate time
    if ((time() - dbDateDecode($Record['CreateDate'])) > 60*60*4) return 'logs older than 4 hours can not be deleted' ;

    // Check if it is a log associated a PDM release
    $query = sprintf ("SELECT Id FROM StylePDM WHERE StyleLogId=%d AND Active=1", $Id) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    if ($row['Id'] > 0) return 'Log can not be deleted because it is associated a PDM release' ;
    
    // Do Delete
    tableDelete ('StyleLog', $Id) ;

    return 0
?>
