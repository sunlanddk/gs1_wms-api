<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    break ;
    }
 
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Header</td><td><input type=text name=Header size=80 maxsize=80 value=\"%s\"></td></tr>\n", htmlentities($Record['Header'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Text</td><td><textarea name=Text style='width:100%%;height:280px;'>%s</textarea></td></tr>\n", $Record['Text']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
