<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    // Style header
    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield width=80>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Style Version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;

    // Log list
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Headline') ;
    listHead ('User', 180) ;
    listHead ('Created', 110) ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'view', $row['Id']) ;
	listField ($row['Header']) ;
	listField ($row['UserName']) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Logs', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;


    return 0 ;
?>
