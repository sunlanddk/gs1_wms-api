<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT Component.* FROM Component WHERE Component.Active=1 ORDER BY Component.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query Component
	    $query = sprintf ('SELECT Component.* FROM Component WHERE Component.Id=%d AND Component.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
