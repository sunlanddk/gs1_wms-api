<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
   
    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['Id']) ;     
    itemField ('Customer', sprintf ('%s (%s)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;     
    if ((int)$Record['CaseId'] > 0) itemField ('Case', (int)$Record['CaseId']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Pos', formText ('No', (int)$Record['No'], 2)) ;
    itemSpace () ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Delivery', formDate ('DeliveryDate', dbDateDecode($Record['DeliveryDate']))) ;
    if ($Record['VariantColor']) {
	itemSpace () ;
	itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Value', $Record['ArticleId']), 'width:200px')) ; 
    }
    itemSpace () ;
    if ($Record['VariantSize']) {
	$field = '<table><tr>' ;
	foreach ($Size as $s) $field .= '<td width=60 align=right><p style="margin-right=6px;">' . htmlentities($s['Name']) . '</p></td>' ;
	$field .= '<td width=60></td>' ;
	$field .= '</tr>' ;
	$field .= '<tr>' ;
	foreach ($Size as $s) $field .= '<td>' . formText (sprintf('Quantity[%d]', (int)$s['Id']), number_format((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 6, 'width:50px;text-align:right;margin-right:0;') . '</td>' ;
	$field .= '<td style="padding-top:6px;padding-left:4px;">' . htmlentities ($Record['UnitName']) . '</td>' ;
	$field .= '</tr></table>' ;	
	itemFieldRaw ('Quantity', $field) ; 
    } else {
	itemFieldRaw ('Quantity', formText ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 5, 'text-align:right;') . ' ' . htmlentities ($Record['UnitName'])) ;
    }
    itemSpace () ;
    itemFieldRaw ('Surplus', formText ('Surplus', (int)$Record['Surplus'], 2, 'text-align:right;') . ' %') ;
    itemSpace () ;
    itemFieldRaw ('Cost Price', formText ('PriceCost', $Record['PriceCost'], 12, 'text-align:right;') . ' Kr.') ;
    itemSpace () ;
    itemFieldRaw ('Sales Price', formText ('PriceSale', $Record['PriceSale'], 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
    itemFieldRaw ('Discount', formText ('Discount', (int)$Record['Discount'], 2, 'text-align:right;') . ' %') ;
    itemSpace () ;
    itemFieldRaw ('LineFooter', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
