<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;
    itemStart () ;
    itemHeader() ;
    foreach (array('Water','Wash','Light','Perspiration','Chlor','Sea','Durability') as $name) {
	printf ("<tr><td class=itemlabel>%s</td><td><input type=text name=ColorFastness%s size=20 maxlength=20 value=\"%s\"></td></tr>\n", $name, $name, htmlentities($Record['ColorFastness'.$name])) ;
    }
    itemInfo ($Record) ;
    itemEnd () ;    
    formEnd () ;

    return 0 ;
?>
