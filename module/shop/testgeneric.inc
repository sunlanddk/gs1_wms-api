<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;
    itemStart () ;
    itemHeader () ;
    printf ("<tr><td class=itemlabel>Width Usable</td><td><input type=text name=WidthUsable size=4 maxlength=4 value=%d> cm</td></tr>\n", (int)$Record['WidthUsable']) ;
    printf ("<tr><td class=itemlabel>Width Full</td><td><input type=text name=WidthFull size=4 maxlength=4 value=%d> cm</td></tr>\n", (int)$Record['WidthFull']) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>M2 Weight</td><td><input type=text name=M2Weight size=4 maxlength=4 value=%d> g</td></tr>\n", (int)$Record['M2Weight']) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Shrink Wash</td><td><input type=text name=ShrinkageWashLength size=5 maxlength=5 value=%s> %% length</td></tr>\n", $Record['ShrinkageWashLength']) ;
    printf ("<tr><td></td><td><input type=text name=ShrinkageWashWidth size=5 maxlength=5 value=%s> %% width</td></tr>\n", $Record['ShrinkageWashWidth']) ;
    printf ("<tr><td class=itemlabel>Shrink Work</td><td><input type=text name=ShrinkageWorkLength size=5 maxlength=5 value=%s> %% length</td></tr>\n", $Record['ShrinkageWorkLength']) ;
    printf ("<tr><td></td><td><input type=text name=ShrinkageWorkWidth size=5 maxlength=5 value=%s> %% width</td></tr>\n", $Record['ShrinkageWorkWidth']) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Rubbing Dry</td><td><input type=text name=RubbingDry maxlength=20 size=20' value=\"%s\"></td></tr>\n", htmlentities($Record['RubbingDry'])) ;
    printf ("<tr><td class=itemlabel>Rubbing Wet</td><td><input type=text name=RubbingWet maxlength=20 size=20' value=\"%s\"></td></tr>\n", htmlentities($Record['RubbingWet'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Wash instr.</td><td><input type=text name=WashingInstruction maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['WashingInstruction'])) ;

    switch ($Navigation['Parameters']) {
	case 'Item' :
	    itemSpace() ;
	    itemFieldRaw ('Test Done', formCheckbox ('TestDone', $Record['TestDone'])) ;
	    break ;
    }

    itemInfo ($Record) ;
    itemEnd () ;    
    formEnd () ;

    return 0 ;
?>
