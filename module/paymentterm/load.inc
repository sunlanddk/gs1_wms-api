<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of PaymentTerms
	    $query = sprintf ('SELECT PaymentTerm.* FROM PaymentTerm WHERE PaymentTerm.Active=1 ORDER BY PaymentTerm.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query PaymentTerm
	    $query = sprintf ('SELECT PaymentTerm.* FROM PaymentTerm WHERE PaymentTerm.Id=%d AND PaymentTerm.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
