<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>PrePay</td><td><input type=checkbox name=PrePay %s></td></tr>\n", ($Record['PrePay'])?'checked':'') ;
    printf ("<tr><td class=itemfield>RunningMonth</td><td><input type=checkbox name=RunningMonth %s></td></tr>\n", ($Record['RunningMonth'])?'checked':'') ;
    printf ("<tr><td class=itemfield>PayDays</td><td><input type=text name=PayDays maxlength=3 size=3 value=%d></td></tr>\n", $Record['PayDays']) ;

    printf ("<tr><td class=itemfield>PayMonths</td><td><input type=text name=PayMonths maxlength=3 size=3 value=%d></td></tr>\n", $Record['PayMonths']) ;
printf ("<tr><td class=itemfield>Discount Days</td><td><input type=text name=DiscountDays maxlength=3 size=3 value=%d></td></tr>\n", $Record['DiscountDays']) ;
printf ("<tr><td class=itemfield>Discount Pct</td><td><input type=text name=DiscountPct maxlength=3 size=3 value=%s>%%</td></tr>\n", $Record['DiscountPct']) ;
    
print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
