<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 120) ;
    listHead ('Description') ;
    listHead ('RunningMonth', 80, 'align=right') ;
    listHead ('PayDays', 80, 'align=right') ;
    listHead ('PayMonths', 80, 'align=right') ;
    listHead ('PrePay', 80) ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
		listField ($row['Name']) ;
		listField ($row['Description']) ;
		listField ($row['RunningMonth'] ? 'yes' : 'no', 'align=right') ;
		listField ((int)$row['PayDays'], 'align=right') ;
		listField ((int)$row['PayMonths'], 'align=right') ;
		listField ($row['PrePay'] ? 'yes' : 'no') ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No PaymentTerms', 'colspan=5') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
