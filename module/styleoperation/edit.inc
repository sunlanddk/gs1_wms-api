<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values	    
	    unset ($Record) ;
	    $Record['No'] = $OperationCount + 1 ;
	    $Record['ProductionMinutes'] = 0 ;
	    break ;
    }

    // Operation information
    if (isset($_GET['operation'])) {
    	// Locate entry
	$query = sprintf ("SELECT MachineGroup.Number AS MachineGroupNumber, Operation.Number AS OperationNumber, Operation.Description, Operation.BundleText, Operation.ProductionMinutes FROM MachineGroup, Operation WHERE MachineGroup.Active=1 AND Operation.Active=1 AND Operation.MachineGroupId=MachineGroup.Id AND CONCAT(MachineGroup.Number, Operation.Number)='%s'", $_GET['operation']) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Update record
	if ($row) {
	    $Record = array_merge ($Record, $row) ;
	    $Navigation['Focus'] = 'Description' ;
	} else {
	    $Record['MachineGroupNumber'] = $_GET['operation'] ;
	    $Record['OperationNumber'] = '' ;
	    $Record['Description'] = ' - Not Found - ' ;
	    $Record['BundleText'] = '' ;
	}
    }

    // Navigation script
    printf ("<script type='text/javascript'>\n") ;
    printf ("function operationselect() {location.replace(\"index.php?nid=%d&id=%d&operation=\"+document.appform.Operation.value)}\n", $Navigation["Id"], $Id) ;
    printf ("</script>\n") ;
    
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>No</td><td><input type=text name=No size=2 maxlength=2 value=\"%s\"></td></tr>\n", htmlentities($Record['No'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Operation</td><td><input type=text name=Operation size=5 maxlength=5 onChange='operationselect()' value=\"%s\"></td></tr>\n", htmlentities($Record['MachineGroupNumber'].$Record['OperationNumber'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>BundleText</td><td><input type=text name=BundleText size=10 maxlength=10 value=\"%s\"></td></tr>\n", htmlentities($Record['BundleText'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Minutes</td><td><input type=text name=ProductionMinutes maxlength=7 size=7 value=\"%s\"></td></tr>\n", htmlentities($Record['ProductionMinutes'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
