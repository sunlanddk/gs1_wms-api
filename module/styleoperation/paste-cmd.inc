<?php

    require_once 'lib/log.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/select.inc' ;
    
    $Line = array () ;
    $Operation = array () ;
    $ProductionMinutes = 0 ;
    $SequenseChange = false ;
    $ErrorText = 'while pasting the operations, the following errors has been found:<br><br>' ;
    $ErrorNote = '' ;
    $Changed = false ;

    // Validate type of selected item
    if (selectGetType() != 'style') return 'no Style is selected' ;

    // Inspect if not identical Style
    if (selectGetId() == (int)$Record['Id']) return 'source and destination is the same' ;

    // Read operations to paste
    $query = sprintf ("SELECT StyleOperation.* FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No", selectGetId()) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	$no = (int)$row['No'] ;
	unset ($row['Id']) ;
	unset ($row['CreateDate']) ;
	unset ($row['CreateUserId']) ;
	unset ($row['ModifyDate']) ;
	unset ($row['ModifyUserId']) ;
	unset ($row['Active']) ;
	unset ($row['StyleId']) ;
	unset ($row['No']) ;
	$Line[$no] = $row ;
	$ProductionMinutes += (float)$row['ProductionMinutes'] ;
    }
    dbQueryFree ($result) ;

    // Validate against all ProductionOrders using the Style and not beeing done
    $query = sprintf ("SELECT Production.Number, Production.CaseId, ProductionMinutes FROM Production INNER JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId AND ProductionLocation.TypeSample=0 WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Production.ProductionMinutes < %s", (int)$Record['Id'], $ProductionMinutes) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {		    
	$ErrorNote .= sprintf ("ProductionMinutes for %d/%d has been exceded by %01.2f minutes<br>", (int)$row['CaseId'], (int)$row['Number'], $ProductionMinutes-$row['ProductionMinutes']) ;
    }
    dbQueryFree ($result) ;

    // Read existing Operations and check if sequense has been changed
    while ($row = dbFetch ($Result)) {
	$Operation[(int)$row['No']] = $row ;
	if ((int)$row['OperationId'] != $Line[(int)$row['No']]['OperationId']) $SequenseChange = true ;
    }
    dbQueryFree ($Result) ;

    // Changing Operation Line No is only allowed when no bundles has been made for ProductionOrders not Done
    if ($SequenseChange) {
	$query = sprintf ("SELECT Production.Number, Production.CaseId, COUNT(Bundle.Id) FROM Production, Bundle WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Bundle.ProductionId=Production.Id AND Bundle.Active=1 GROUP BY Production.Id", $Record['Id']) ;
	$result = dbQuery ($query) ;
	$s = '' ;
	while ($row = dbFetch ($result)) {
	    if ($s != '') $s .= ', ' ;
	    $s .= $row['CaseId'] . '/' . $row['Number'] ;
	}
	dbQueryFree ($result) ;
	if ($s != '') $ErrorNote .= sprintf ('Sequense of operations can not be changed because bundles has been made for ProductionOrders (%s)<br>', $s) ;
    }

//logPrintVar ($SequenseChange, 'SequenseChange') ;
//logPrintVar ($Line, 'Line') ;
//logPrintVar ($Operation, 'Operation') ;
//return sprintf ('%s(%d) STOP', __FILE__, __LINE__) ;    

    // Errors so fare ?
    if ($ErrorNote != '') return $ErrorText . $ErrorNote ;

    // Opdate the records made
    foreach ($Line as $n => $line) {
	if (isset($Operation[$n])) {
	    // Update existing record
	    $operation = &$Operation[$n] ;
	    if ($line['OperationId'] == (int)$operation['OperationId']) unset ($line['OperationId']) ;
	    if ($line['MachineGroupId'] == (int)$operation['MachineGroupId']) unset ($line['MachineGroupId']) ;
	    if ($line['Description'] == $operation['Description']) unset ($line['Description']) ;	    
	    if ($line['BundleText'] == $operation['BundleText']) unset ($line['BundleText']) ;
	    if ($line['ProductionMinutes'] == (float)$operation['ProductionMinutes']) unset ($line['ProductionMinutes']) ;

	    if (count($line) > 0) {
		tableWrite ('StyleOperation', $line, (int)$operation['Id']) ;
		$Changed = true ;
//logPrintVar ($line, 'Operation Update') ;
	    }	
	    unset ($Operation[$n]) ;
	    
	} else {
	    // Create new operation
	    $line['No'] = $n ;
	    $line['StyleId'] = (int)$Record['Id'] ;
	    tableWrite ('StyleOperation', $line) ;
	    $Changed = true ;
//logPrintVar ($line, 'Operation Create') ;
	}
    }

    // Delete operations left over
    foreach ($Operation as $operation) {
	tableDelete ('StyleOperation', (int)$operation['Id']) ;
	$Changed = true ;
//printf ("Operation Delete, No %d, Id %d<br>\n", (int)$operation['No'], (int)$operation['Id']) ;
    }

    if ($Changed) tableTouch ('Style', (int)$Record['Id']) ;
    
    return 0 ;
?>
