<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/uts.inc' ;

    define ('LINES', 30) ;

    // Navigation
    switch ($Navigation["Function"]) {
	case 'list' :
	    $offset = (int)$_GET["offset"] ;

	    // Last page ?
	    if ($offset == -1) {
		$query = sprintf ("SELECT Capacity.ActualDate FROM Capacity WHERE Capacity.Active=1 GROUP BY Capacity.ActualDate") ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		$count = dbNumRows ($result) ;
	    	dbQueryFree ($result) ;

		$offset= $count - LINES ;
		if ($offset < 0) $offset = 0 ;
	    }

	    // Query records to list
	    $query = sprintf ("SELECT Capacity.ActualDate, COUNT(Capacity.Id) AS WorkGroupCount, SUM(Capacity.Operators) AS OperatorCount, SUM(Capacity.Operators*Capacity.ProductionMinutes) AS ProductionMinutes FROM Capacity WHERE Capacity.Active=1 GROUP BY Capacity.ActualDate ORDER BY Capacity.ActualDate DESC LIMIT %d, %d", $offset, LINES+1) ;
	    $Result = dbQuery ($query) ;
	    $count = dbNumRows ($Result) ;
	    
	    if ($offset > 0) {
		if ($offset < LINES) $offset=LINES ;
		navigationSetParameter ("previous", "offset=".((int)(($offset-LINES)/LINES)*LINES)) ;
	    } else {
		navigationPasive ("first") ;
		navigationPasive ("previous") ;
	    }

	    if ($count > LINES) {
		navigationSetParameter ("next", sprintf ("offset=%d", ((int)(($offset+LINES)/LINES))*LINES)) ;
		navigationSetParameter ("last", "offset=-1") ;
	    } else {
		navigationPasive ("next") ;
		navigationPasive ("last") ;
	    }
	    return 0 ;

	case 'edit':
	    if ($Navigation['Parameters'] == 'new') {
		// Get date for newest capacity
		$query = sprintf ("SELECT MAX(Capacity.ActualDate) AS NewestDate FROM Capacity WHERE Capacity.Active=1") ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
	    	dbQueryFree ($result) ;

		if ($row['NewestDate']) {
		    $Id = dbDateDecode($row['NewestDate']) ;
		} else {
		    $Id = utsDateOnly(time()) ;
		}
	    
	    } else {
		// Validate Id
		if ($Id != utsDateOnly($Id)) return sprintf ("%s(%d) invalid referance, id %s", __FILE__, __LINE__, $Id) ;
	    }
	    
	    $query = sprintf ("SELECT WorkGroup.Id, WorkGroup.Number, WorkGroup.Description, WorkGroup.Efficiency, Capacity.Operators, Capacity.ProductionMinutes FROM WorkGroup LEFT JOIN Capacity ON Capacity.WorkgroupId=WorkGroup.Id AND Capacity.Active=1 AND Capacity.ActualDate='%s' WHERE WorkGroup.Active=1 ORDER BY WorkGroup.Number", dbDateOnlyEncode($Id)) ;
	    $Result = dbQuery ($query) ;    

	    if ($Navigation['Parameters'] == 'new') {
		// Default to next day not in weekend
		do {
		    $Id += 24*60*60 ;
		    $weekday = date('w', $Id) ;
		} while ($weekday < '1' or $weekday > '5') ;
	    }
    }

    return 0 ;
?>
