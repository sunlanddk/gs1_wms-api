<?php

    require_once 'lib/form.inc' ;
    
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    // Header
    printf ("<br><table class=item>\n") ;
    if ($Navigation['Parameters'] == 'new') {
//	printf ("<tr><td class=itemlabel>Date</td><td><input type=text id=IdCapacityDate name=CapacityDate size=10 maxlength=10 value='%s' style='width:68px' readonly=1><img src='image/date.gif' id=IdCapacityDateButton style='vertical-align:bottom;margin-left:0px;cursor: pointer;'></td></tr>\n", dbDateOnlyEncode($Id)) ;
	printf ("<tr><td class=itemlabel>Date</td><td>%s</td></tr>\n", formDate('CapacityDate', $Id)) ;
     } else {
	printf ("<tr><td class=itemlabel>Date</td><td class=itemfield>%s</td></tr>\n", dbDateOnlyEncode($Id)) ;
    }
    printf ("</table>\n") ;
 
    // List of workgroups
    printf ("<br><table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>WorkGroup</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Efficiency(%%)</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Operators</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list style='cursor:default;' src='./image/toolbar/workgroup.gif'></td>") ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Number'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['Efficiency'])) ;
	printf ("<td align=right><input type=text style='width:30px;text-align:right;margin-right:0;' name=Operators[%d] maxlength=3 size=3 value='%s'></td>", $row['Id'], htmlentities($row['Operators'])) ;
	printf ("<td align=right><input type=text style='width:50px;text-align:right;margin-right:0;' name=ProductionMinutes[%d] maxlength=7 size=7 value='%s'></td>", $row['Id'], htmlentities($row['ProductionMinutes'])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No workgroups</p></td></tr>\n") ;
    }
    printf ("</table><br>\n") ;

    printf ("</form>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
