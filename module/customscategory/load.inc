<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT CustomsCategory.* FROM CustomsCategory WHERE CustomsCategory.Active=1 ORDER BY CustomsCategory.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query CustomsCategory
	    $query = sprintf ('SELECT CustomsCategory.* FROM CustomsCategory WHERE CustomsCategory.Id=%d AND CustomsCategory.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
