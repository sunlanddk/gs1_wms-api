<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM CustomsPosition WHERE CustomsCategoryId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the CustomsCategory has associated CustomsPositions") ;

    // Do delete
    tableDelete ('CustomsCategory', $Id) ;

    return 0
?>
