<?php

    require_once "lib/table.inc" ;
    require_once "lib/html.inc" ;
    require_once "lib/navigation.inc" ;
    require_once 'module/production/include.inc' ;
    require_once 'module/style/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    if (!$Record['Approved']) {
	printf ("<p>Production Order has not been Approved</p>\n") ;
	return 0 ;
    }
    if (count($Size) == 0) {
	printf ("<p>No Sizes assigned</p>\n") ;
	return 0 ;
    }
    if (count($Color) == 0) {
	printf ("<p>No Colors assigned</p>\n") ;
	return 0 ;
    }
    if ($Record['Cut']) {
	printf ("<p>All bundles has been made</p><br>\n") ;
	return 0 ;
    }	

   	// Making bundle is only allowed when no operation list work in progress.	
    $HasStyle = tableGetField ('Article', 'HasStyle', $Record['ArticleId']) ;
//printf ("<p>%d HasStyle %d  %d</p><br>\n", $Record['ArticleId'], $HasStyle, $Record['StyleId']) ;
   	if ($HasStyle)
   	if (isCurrentOperationsClosed ($Record['StyleId']) == 0) {
		printf ("<p>Operation list is being edited - contact construction!</p><br>\n") ;
		return 0 ;
	}

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    // Headline
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Color</p></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>Size</p></td>") ;
    foreach ($Size as $s) {
	printf ("<td align=right class=listhead width=60><p class=listhead>%s</p></td>", $s) ;
	printf ("<td class=listhead width=23></td>") ;    
    }
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;

    // Quantitys
    foreach ($Color as $cid => $c) {
	// Specified count
	printf ("<tr>") ;
	printf ("<td class=list><img class=list style='cursor:default;' src='%s'></td>", './image/toolbar/color.gif') ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($c)) ;
	printf ("<td><p class=list>specified</p></td>") ;
	foreach ($Size as $sid => $s) {
	    printf ("<td align=right><p>%d</p></td>", $Quantity[$cid][$sid]['Quantity']) ;
	    if (!$Record['Cut'] and $Quantity[$cid][$sid]['Id'] > 0) {
		printf ("<td class=list><img class=list src='%s' style='cursor:pointer;' %s></td>", './image/toolbar/'.$Navigation['Icon'], navigationOnClickLink ('count', $Quantity[$cid][$sid]['Id'])) ;
	    } else {
		printf ("<td></td>") ;
	    }
	}
	printf ("</tr>\n") ;

	// Allready bundled count
        printf ("<tr>") ;
	printf ("<td style='border-bottom: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-bottom: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-bottom: 1px solid #cdcabb;'><p class=list>bundeled</p></td>") ;
	foreach ($Size as $sid => $s) {
	    printf ("<td align=right style='border-bottom: 1px solid #cdcabb;'><p>%d</p></td>", $Quantity[$cid][$sid]['BundleQuantity']) ;
	    printf ("<td style='border-bottom: 1px solid #cdcabb;'></td>") ;
	}
	printf ("</tr>\n") ;
    }

    printf ("</table>\n") ;

    printf ("</form>\n") ;
    
    return 0 ;
?>
