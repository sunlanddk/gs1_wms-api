<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    require_once 'module/style/include.inc' ;
        
    // Header
    print productionHeader ($Record) ;
    
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Color</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ColorNumber'])) ;
    printf ("<tr><td class=itemlabel>Size</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['SizeName'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Quantity</td><td class=itemfield>%d</td></tr>\n", $Record['Quantity']) ;
    printf ("<tr><td class=itemlabel>Bundeled</td><td class=itemfield>%d</td></tr>\n", $Record['BundleQuantity']) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    if (!$Record['Approved']) {
	printf ("<p>ProductionOrder has not been Approved</p>\n") ;
	return 0 ;
    }
    if ($Record['Cut']) {
	printf ("<p>All bundles has been made</p><br>\n") ;
	return 0 ;
    }
    
	// Making bundle is only allowed when no operation list work in progress.	
	if (isCurrentOperationsClosed ($Record['Id'])) {
		printf ("<p>Operation list is being edited - contact construction!</p><br>\n") ;
		return 0 ;
	}
	
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    // Headline
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td class=listhead width=80><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>Bundles</p></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;

    // Bundles
    for ($n=1 ; $n <= 8 ; $n++) {
        printf ("<tr>") ;
	printf ("<td><p class=list>%d</p></td>", $n) ;
	printf ("<td><input type=text style='text-align:right;' name=Bundles[%d] maxlength=3 size=3 value=%s></td>", $n, $Line[$n]['Bundles']) ;
	printf ("<td><input type=text style='text-align:right;' name=Quantity[%d] maxlength=3 size=3 value=%s></td>", $n, $Line[$n]['Quantity']) ;
	printf ("</tr>\n") ;
    }	
    
    printf ("</form>\n") ;
    
    return 0 ;
?>
