<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/table.inc' ;

    // Validate state
    if (!$Record['Approved']) return "Production Order has not been Approved" ;
    if ($Record['Cut']) return "All Bundles for Production Order has been made" ;
    
    // Get parameters
    $BundleQuantityMax = (int)parameterGet ('BundleQuantityMax') ;
    
    // Calculate counts
    $QuantityMax = (int)((((int)$Record['Quantity'])*(parameterGet('BundleQuantityExtra')+100))/100) ;
    $QuantityMax -= (int)$Record['BundleQuantity'] ;

    // Get Bundles to make
    $total = 0 ;
    $i = 0 ;
    $Bundle = array() ;
    for ($no = 1 ; $no < 8 ; $no++) {
	$bundles = $_POST['Bundles'][$no] ;
	$count = $_POST['Quantity'][$no] ;
	if ($bundles == 0 or $count == 0) continue ;
	if ($bundles > 100) return "to many bundles, max 100" ;
	if ($count > $BundleQuantityMax) return sprintf ("too many pieces in bundle, max %d", $BundleQuantityMax) ;
	while ($bundles > 0) {
	    $Bundle[++$i]['Quantity'] = $count ;
	    $total += $count ;
	    $bundles-- ;
	}
    }

    // Validate count
    if ($total > $QuantityMax) return sprintf ("too many pieces, specified %d, allowed %d", $total, $QuantityMax) ;
    
    // Create bundles
    foreach ($Bundle as $i => $b) {

		// Create bundle
		$fields = array (
		    'ProductionQuantityId'	=> array ('type' => 'set',	'value' => $Record['Id']),
	    	'ProductionId'		=> array ('type' => 'set',	'value' => $Record['ProductionId']),
	    	'Quantity'			=> array ('type' => 'set',	'value' => $b['Quantity']) 
		) ;
		$res = saveFields ('Bundle', -1, $fields) ;
		if ($res) return sprintf ("1: %s", $res) ;
		$BundleId = dbInsertedId () ;

		// Make In Production Items
		// First get allready created items
		$query = sprintf ("SELECT * FROM Item WHERE ArticleColorId=%d AND ArticleSizeId=%d AND ContainerId=165413 AND FromProductionId=%d AND Active=1", 
		                     $Record['ArticleColorId'], $Record['ArticleSizeId'], $Record['ProductionId']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;

		if ($row['Id'] <= 0) {
			// Insert new item into production container and stock
			$item['Active'] = 1 ;
			$item['Sortation'] = 0 ;
			$item['ContainerId'] = 165413 ;
			// from record itself
			$item['FromProductionId'] = $Record['ProductionId'] ;
		    $item['ArticleColorId'] = $Record['ArticleColorId'] ;
	    	$item['ArticleSizeId'] = $Record['ArticleSizeId'] ;
			// Entered in UI
		    $item['Quantity'] = $b['Quantity'] ;
			// From production record
			$item['StyleId'] = tableGetField ('Production', 'StyleId', $Record['ProductionId']) ;
			$item['CaseID'] = 0 ;
			// From case record
		        $CaseId = tableGetField ('Production', 'CaseId', $Record['ProductionId']) ;
			$item['ArticleId'] = tableGetField ('`Case`', 'ArticleId', $CaseId) ;
			$item['ArticleCertificateId'] = tableGetField ('`Case`', 'ArticleCertificateId', $CaseId) ;
			tableWrite ('Item', $item) ;
		} else {
			// Update item: add quantity
			$query = sprintf ("UPDATE Item SET Quantity=%d WHERE Id=%d", ($row['Quantity']+$b['Quantity']), $row['Id']) ;
			$result = dbQuery ($query) ;
//			dbQueryFree ($result) ;
		}


		// Save bundle id
		$Bundle[$i]['Id'] = $BundleId ;

		// Print labels
		$printer = trim(parameterGet('PrinterLabel')) ;
		if ($printer != '') { 
		    require_once 'module/makebundle/label.inc' ;
		    $res = PrintLabel ($BundleId, $printer) ;
		    if ($res) return sprintf ("2: %s", $res) ;
		} 
    }

    return 0 ;
?>
