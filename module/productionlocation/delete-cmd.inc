<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT COUNT(Id) AS Count FROM Production WHERE ProductionLocationId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ($row['Count'] > 0) return "this Production Location can't be deleted because it is used in ProductionOrders" ;   

    // Verify references
    $query = sprintf ("SELECT COUNT(Id) AS Count FROM Production WHERE SourceLocationId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ($row['Count'] > 0) return "this Production Location can't be deleted because it is used in ProductionOrders" ;   

    // Do delete
    tableDelete ('ProductionLocation', $Id) ;

    return 0
?>
