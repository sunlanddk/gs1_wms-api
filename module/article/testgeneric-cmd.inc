<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'WidthUsable'		=> array ('type' => 'integer'),
	'WidthFull'			=> array ('type' => 'integer'),
	'M2Weight'			=> array ('type' => 'integer'),
	'ShrinkageWashLength'	=> array ('type' => 'decimal',		'format' => '3.1'),
	'ShrinkageWashWidth'	=> array ('type' => 'decimal',		'format' => '3.1'),
	'ShrinkageWorkLength'	=> array ('type' => 'decimal',		'format' => '3.1'),
	'ShrinkageWorkWidth'	=> array ('type' => 'decimal',		'format' => '3.1'),
//	'Twisting'				=> array ('type' => 'decimal',		'format' => '3.1'),
	'RubbingDry'			=> array (),
	'RubbingWet'			=> array (),
	'WashingInstruction'	=> array (),
	'TestDone'			=> array ('type' => 'checkbox',	'check' => true),
	'TestDoneUserId'	=> array ('type' => 'set'),
	'TestDoneDate' 		=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields ;
	switch ($fieldname) {		
	    case 'TestDone' :
		if (!$changed or !$value) return false ;
		
		if ($_POST['WidthUsable'] == 0) return 'Setting Useable Width is mandatory before test is complete and test done can be set' ;

		// Set tracking information when setting flag
		$fields['TestDoneUserId']['value'] = $User['Id'] ;
		$fields['TestDoneDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }

    switch ($Navigation['Parameters']) {
	case 'Item' :
	    if ($Record['TestDone']) return sprintf ('%s(%d) tests allready done, id %d', __FILE__, __LINE__, (int)$Record['Id']) ;
	    if ((int)$Record['ContainerId'] == 0 or $Record['StockType'] == 'shipment') return sprintf ('%s(%d) stock locked, id %d', __FILE__, __LINE__, (int)$Record['StockId']) ;
	    break ;

	default :
	    unset ($fields['TestDone']) ;
	    break ;
    }

    // Save record
    return saveFields ($Navigation['Parameters'], $Id, $fields) ;
?>
