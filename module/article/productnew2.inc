<?php

    require_once 'lib/parameter.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    define ('DEBUG', false) ;
    
	if (isset($_GET['CollectionId'])) {
		$collection_id = (int)$_GET['CollectionId'] ;
	} else {
		$collection_id = 0 ;
	}

	
    // Page header
    itemStart () ;
    itemSpace () ;
 //   itemField ('Case', (int)$Record['Id']) ;
    itemField ('Article', $Record['Number'] . ', ' . $Record['Description']) ;

    formStart () ;
    itemSpace () ;
	
	$query = 'SELECT Collection.Id AS Id,CONCAT(Season.Name, ", ", Collection.Name) AS Value FROM Season, collection WHERE collection.seasonid=season.id AND season.Active=1 AND collection.Active=1 ORDER BY Value' ;
	$reload = '' ; //sprintf('onchange="appLoad(%d,%d,\'CollectionId=\'+document.appform.CollectionId.options[document.appform.CollectionId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
	itemFieldRaw ('Add to Collection', formDBSelect ('CollectionId', $collection_id, $query, 'width:280px;', NULL, $reload)) ; 
/* Comment back in if more than one LOT for a season	
	if ($collection_id > 0) {
		$query = sprintf("SELECT cl.Id as Id, cl.Name AS Value FROM Collectionlot cl, collection c WHERE c.Id=%d and cl.SeasonId=c.SeasonId AND cl.Active=1 ORDER BY Date", $collection_id) ;
		$res = dbQuery ($query) ;
		$row = dbFetch($res) ;
		itemFieldRaw ('LOT', formDBSelect ('CollectionLOTId', (int)$row['Id'], $query, 'width:100px;')) ; 
		dbQueryFree ($res) ;
	}
*/

/*	
    itemFieldRaw ('Add to Collection', 
				formDBSelect ('CollectionId', 0, 'SELECT Collection.Id AS Id,CONCAT(Season.Name, ", ", Collection.Name) AS Value FROM Season, collection WHERE collection.seasonid=season.id AND season.Active=1 AND collection.Active=1 ORDER BY Value', 'width:280px;')
				) ;
*/				
    itemSpace () ;
    itemEnd () ;

    // Colors
    listStart () ;
    listHeader ('Colours') ;
    listRow () ;
    listHead ('', 30) ;
    listHead ('Description') ;
    listHead ('Colour', 40) ;
    listHead ('', 6) ;
    listHead ('Number', 75) ;
    foreach ($Color as $i => $row) {
		listRow () ;
		listFieldRaw (formCheckBox(sprintf('Color[%d]', $i), 0)) ;
		listField ($row['Description']) ;
		if ($row['ColorGroupId'] > 0) {
			listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
		} else {
			listField ('') ;
		}
		listField ('') ;
		listField ($row['Number']) ;
    }
    if (count($Color) == 0) {
	listRow () ;
	listField () ;
	listField ('No Colors', 'colspan=3') ;
    }
    listEnd () ;
    printf ("<br>\n") ;

    // Components    

    listStart () ;
    listHeader ('Components') ;
    listRow () ;
    listHead ('', 30) ;
    listHead ('Name', 121) ;
    listHead ('Description', 121) ;
    listHead ('Percent') ;
    foreach ($Components as $i => $row) {
		listRow () ;
		listFieldRaw (formCheckBox(sprintf('Components[%d]', $i), 0)) ;
		listField ($row['Name']) ;
		listField ($row['Description']) ;
		listFieldRaw (formText (sprintf('Percent[%d]', $i), 0, 3, 'text-align:right;' . ' ')) ;
//		listFieldRaw (formCheckBox(sprintf('CertificateCase[%d]', $i), 0)) ;
    }

    listEnd () ;
    printf ("<br>\n") ;

    formEnd () ;

    return 0 ;
?>
