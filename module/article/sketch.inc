<?php

    require_once 'lib/file.inc' ;
    
    // Make filename
    $file = fileName ('article', $Id) ;

    // Check for file existance
    if (!is_file($file)) return 'No Article Sketch' ;

    // No caching
    switch ($_SERVER["SERVER_PROTOCOL"]) {
	case "HTTP/1.1":
	    header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	    break ;
	
	case "HTTP/1.0":
	    header ("Pragma: no-cache") ; 
	    break ;

	default:
	    return sprintf ("%s(%d) invalid server protocol %s", __FILE__, __LINE__, $_SERVER["SERVER_PROTOCOL"]) ;
    }

    // HTTP Headers
//    $size = filesize($file) ;
//    header ('Content-Length: '.$size) ;		// Can't be used when compressing
    header ('Content-Type: '.$Record['SketchType']) ;
    header ('Last-Modified: '.date("D, j M Y G:i:s T", dbDateDecode($Record['SketchDate'])));

    // Output file
    readfile ($file) ;

    return 0 ;

?>
