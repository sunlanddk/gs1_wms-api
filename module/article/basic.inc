<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default
	    $Record['PriceStock'] = '0.00' ;
//	    $Record['Comment'] = "Fabrics:\nLabels:\nHangtags:\nPoly Bags to be marked:\n\nBoxes to be marked:\n\nItem no.:\nOrder no.:\nPackinginstructions:\nRemarks:" ;
	    $Record['Doubles'] = false ;
	    $Record['UnitId'] = 3 ;
        $Record['ArticleTypeId'] = 6 ;
        $Record['DefaultStockId'] = 14321 ;
	    break ;

	default:
	    // Any doubles
	    if ($Record['SupplierNumber'] != '') {
		$query = sprintf ('SELECT COUNT(Id) AS Count FROM Article WHERE Article.Id<>%d AND Article.SupplierCompanyId=%d AND Article.SupplierNumber="%s" AND Article.Active=1', (int)$Record['Id'], (int)$Record['SupplierCompanyId'], addslashes($Record['SupplierNumber'])) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		$Record['Doubles'] = ((int)$row['Count'] > 0) ;
	    } else {
		$Record['Doubles'] = false ;
	    }
	    break ;
    }
 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    printf ("<tr><td class=itemlabel>Number</td><td><input type=text name=Number size=10 maxlength=15 value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Default Stock</td><td>%s</td></tr>\n", htmlDBSelect ("DefaultStockId style='width:250px'", $Record['DefaultStockId'], 'SELECT Id, concat(StockNo," - ",Name) AS Value FROM Stock WHERE Active=1 AND Type="fixed" ORDER BY StockNo')) ;  
    itemSpace () ;
    itemFieldRaw ('SsccLabel Creation', formCheckbox ('SsccLabelCreation', $Record['SsccLabel']) . 'Check this box if pallets received are without SSCC labels, so internal labels need to be created by WMS') ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>ArticleType</td><td>%s</td></tr>\n", htmlDBSelect ("ArticleTypeId style='width:250px'", $Record['ArticleTypeId'], 'SELECT Id, Name AS Value FROM ArticleType WHERE Active=1 ORDER BY Leaddigit')) ;  
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Unit</td><td>%s</td></tr>\n", htmlDBSelect ("UnitId style='width:70px'", $Record['UnitId'], 'SELECT Id, Name AS Value FROM Unit WHERE Active=1 ORDER BY Name')) ;  
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Supplier</td><td>%s</td></tr>\n", htmlDBSelect ("SupplierCompanyId style='width:250px'", $Record['SupplierCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeSupplier=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Reference</td><td><input type=text name=SupplierNumber maxlength=30' value=\"%s\"></td></tr>\n", htmlentities($Record['SupplierNumber'])) ;
    itemFieldRaw ('Doubles', formCheckbox ('Doubles', $Record['Doubles'])) ;
	itemFieldRaw ('Hide from list', formCheckbox ('ListHidden', $Record['ListHidden'])) ;
    itemSpace () ;
	echo '<tr><td colspan=2>  Grouping and picking order: (if nothing is specified, then neareast position is used)</td></tr>' ;
	$_query = 'SELECT * from ai WHERE SortOption>0' ;
	$_res=dbQuery($_query) ;
	while ($_row=dbFetch($_res)) {
		$_pquery = sprintf('SELECT * from pickorder_ai WHERE pickorderid=%d and aiid=%d', $Id, $_row['Id']) ;
		$_pres=dbQuery($_pquery) ;
		$_prow=dbFetch($_pres) ;
	
		itemFieldRaw ( $_row['Description'], formText ("Ai[" . $_row['Id'] . "]", $_prow['SortOrder'],2)) ;
		itemFieldRaw ('',formHidden("PAiId[" . $_row['Id'] . "]", $_prow['Id'], 8,'','')) ;
	}
//    printf ("<tr><td class=itemlabel>Price</td><td><input type=text name=PriceStock maxlength=10 size=10 value=\"%s\" style='text-align:right;'>Kr.</td></tr>\n", htmlentities($Record['PriceStock'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    itemSpace() ;
//	itemFieldRaw ('Public', formCheckbox ('Public', $Record['Public'])) ;
//	printf ("<tr><td class=itemlabel>Sales Price</td><td><input type=text name=SalesPrice maxlength=10 size=10 value=\"%s\" style='text-align:right;'></td></tr>\n", htmlentities($Record['SalesPrice'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemfield>Delivery Comment</td><td><textarea name=DeliveryComment style='width:100%%;height:50px;'>%s</textarea></td></tr>\n", $Record['DeliveryComment']) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
