<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;
//    printf ("<tr><td class=itemfield>Delivery Comment</td><td><textarea name=DeliveryComment style='width:100%%;height:300px;'>%s</textarea></td></tr>\n", $Record['DeliveryComment']) ;
//    itemSpace () ;
    itemFieldRaw ('DK Webshop', formCheckbox ('OnWebshop[4]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=4 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DK Name</td><td><input type=text name=Name[4] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=4 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DK Description</td><td><textarea name=Desc[4] style='width:100%%;height:300px;'>%s</textarea></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','WebDescription','LanguageId=4 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('DE Webshop', formCheckbox ('OnWebshop[5]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=5 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DE Name</td><td><input type=text name=Name[5] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=5 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DE Description</td><td><textarea name=Desc[5] style='width:100%%;height:300px;'>%s</textarea></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','WebDescription','LanguageId=5 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('UK Webshop', formCheckbox ('OnWebshop[6]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=6 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>UK Name</td><td><input type=text name=Name[6] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=6 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>UK Description</td><td><textarea name=Desc[6] style='width:100%%;height:300px;'>%s</textarea></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','WebDescription','LanguageId=6 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
