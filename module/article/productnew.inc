<?php

    require_once 'lib/parameter.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Default values
    unset ($Record) ;
    $Record['Number'] = '' ;
    $Record['Comment'] =  '' ;"Labels:\nHangtags:\nPoly Bags to be marked:\n\nBoxes to be marked:\n\nItem no.:\nPackinginstructions:\nRemarks:" ;
//    $Record['SupplierCompanyId'] = parameterGet ('CompanyMy') ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Number', formText ('Number', $Record['Number'], 10)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Size Range', formDBSelect ('SizeSetId', 1, 'SELECT Id, CONCAT(Name, ", ", Description) AS Value FROM SizeSet WHERE Active=1 ORDER BY Value', 'width:280px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Supplier', formDBSelect ('SupplierCompanyId', (int)$Record['SupplierCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeSupplier=1 AND Active=1 AND not Id=1 ORDER BY Name', 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Reference', formText ('CustomerReference', $Record['SupplierNumber'], 20)) ;
//    itemFieldRaw ('Customer', formDBSelect ('CustomerCompanyId', 0, 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name', 'width:250px;')) ;  
//    itemFieldRaw ('Case Descr.', formText ('CaseDescription', $Record['CaseDescription'], 100, 'width:100%;')) ;
//    itemSpace () ;
//    itemFieldRaw ('Fabric', formText ('FabricNumber', '', 10)) ;
    itemSpace () ;
//    itemFieldRaw ('Style template', formText ('StyleNumber', '', 10)) ;
//    itemSpace () ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:170px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
