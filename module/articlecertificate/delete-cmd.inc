<?php

    require_once 'lib/table.inc' ;

    // Ensure ArticleCertificate not used in Case
    $query = sprintf ('SELECT Id FROM `Case` WHERE ArticleId=%d AND ArticleCertificateId=%d AND Active=1', (int)$Record['ArticleId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Certificate used in Case' ;
 
    // Ensure ArticleCertificate not used in Order
    $query = sprintf ('SELECT Id FROM OrderLine WHERE ArticleId=%d AND ArticleCertificateId=%d AND Active=1', (int)$Record['ArticleId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Certificate used in Order' ;

    // Ensure ArticleCertificate not used in Invoice
    $query = sprintf ('SELECT Id FROM InvoiceLine WHERE ArticleId=%d AND ArticleCertificateId=%d AND Active=1', (int)$Record['ArticleId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Certificate used in Invoice' ;

    // Ensure ArticleCertificate not used for Items
    $query = sprintf ('SELECT Id FROM Item WHERE ArticleId=%d AND ArticleCertificateId=%d AND Active=1', (int)$Record['ArticleId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Certificate used for Items' ;

    // Delete	   
    tableDelete ('ArticleCertificate', $Id) ;

    return 0
?>
