

<?php
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'module/purchase/process.inc' ;


	const OrderNumber 		= 0; 
	const OrderQuantityId 	= 5; 
	const ArticleNumber 	= 6; 
	const ArticleName	 	= 7; 
	const ArticleColor	 	= 8; 
	const ArticleSize	 	= 9; 
	const OrderQuantity 	= 10; 
	const PackedQuantity 	= 11; 
	const BoxNumber 		= 12;

    Global $PO_Data ;	

/* ------------------------------------
Prefill quantities by import quantities from csv file
--------------------------------------- */	
	
	$error_txt = '' ;
	$upload_file_name = 'Doc1' ;
	if ($_FILES[$upload_file_name]['tmp_name']=='') {
		printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
		printf ("<input type=hidden name=id value=%d>\n", $Id) ;
		printf ("<input type=hidden name=nid>\n") ;

		itemStart () ;
		itemSpace () ;
		printf ("<input type=hidden name=MAX_FILE_SIZE value=2000000>\n") ;
		printf ('<tr><td><p>Upload file</p></td><td><input type=file onchange="appform.submit();" name=Doc1 size=40> receive quantity according content</td></tr>') ;    
		itemSpace () ;
		itemSpace () ;

		itemEnd () ;
//		formEnd () ;
	} else {
		switch ($_FILES[$upload_file_name]['error']) {
			case 0: 
				if (!is_uploaded_file($_FILES[$upload_file_name]['tmp_name'])) {
					return 'Upload failed, file not uploaded' ;
				}
				if ($_FILES[$upload_file_name]['size'] != filesize($_FILES[$upload_file_name]['tmp_name']))
					return 'invalid photo size' ;
				if ($_FILES[$upload_file_name]['size'] > 10000000)
					return 'photo too big, max 50 MB' ;

				// Set flag for photo upload	
				$_upload = true ;		    
				break ;
			
			case 4: 		// No file specified
				$_upload = false ;
				break ;
			
			default: return sprintf ('Doc1ument upload failed, code %d', $_FILES[$upload_file_name]['error']) ;
		}

		itemStart () ;
		itemField ('Upload file', 'File uploaded succesfully - Content is being validated - press save to set prepack data, if validation succesful'  ) ;
		itemSpace () ;
		itemEnd () ;
		formStart () ;
		// validate file content

		if ($_upload) {
			if (($handle = fopen($_FILES[$upload_file_name]['tmp_name'], "r")) !== FALSE) {
				$error_txt = process_file($handle) ;
				fclose($handle);
			} else {
				return 'Cant open file ' ;
			}
		} else {
			$error_txt = 'No file specified' ;
		}
		// Process collected data after displayed on UI.
		// Create pickorder, set packed data, create containers, create/allocate items to order and receive to "repacked picking" stock and set orderlines done.

		if ($error_txt) {
//			printf($error_txt)  ;
			return $error_txt ;
		} else {
			return 'Validation successfully' ;
		}

	}

	formEnd () ;

?>
<?php
return 0;
?>

