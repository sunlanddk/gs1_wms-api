﻿<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
   	require_once 'lib/reader.inc';
    require_once 'lib/save.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
		require_once 'lib/variant.inc' ;

	function gen_ean ( $__collectionmemberid, $__assortid) {
		
		$_varid=tableGetFieldWhere('variantcode','Id', 'Reference='.(int)$__assortid) ;
		if ($_varid>0) {
			echo 'exists' ;
			return ;
		} 
		// Generate EAN for assortment
		// Find last ean according seasen basecode.
		$query = sprintf("SELECT BaseVarCode FROM Season, Collection, CollectionMember where  CollectionMember.Id=%d and Collection.Id=CollectionMember.CollectionId and Season.Id=Collection.SeasonId", $__collectionmemberid);
		$_res = dbQuery ($query) ;
		$baserow = dbFetch ($_res) ;
		$query = "SELECT max(variantcode) as MaxCode FROM variantcode where active=1 and variantcode like '" . $baserow['BaseVarCode'] . "%'" ;
		$_res = dbQuery ($query) ;
		if (dbNumRows($_res)==0) {
			$baserow['BaseVarCode'] = $baserow['BaseVarCode'] . '00001' ; // First code in new serie!
		} else {
			$_row = dbFetch ($_res) ;
			$baserow['BaseVarCode'] = substr($_row['MaxCode'],0,12) ;		// Continue from last number ion series
		}
		dbQueryFree ($_res) ;
		
		// if no Basecode defined then just find last (max) ean
		if (strlen($baserow['BaseVarCode'])==0){
			$query = sprintf("SELECT max(variantcode) as VariantCode FROM variantcode") ;
			$_res = dbQuery ($query) ;
			$_row = dbFetch ($_res) ;
			dbQueryFree($_res) ;
		
			$BaseVariantCode = (float)substr($_row['VariantCode'],0,12) ;
		} else {
			$BaseVariantCode = $baserow['BaseVarCode'] ;		// 
		}
		$VariantCode = array (	
			'VariantCode'		=>  "",			// EAN code
			'VariantUnit'		=>  "Pcs",		// 			
			'VariantmodelRef'	=>  "",			
			'VariantDescription'	=> "",			
			'VariantColorDesc'	=>  "",		
			'VariantColorCode'	=>  "",			
			'VariantSize'		=>  "",			
			'ArticleId'			=>	53569,			// Article id for pseudo article 9999000000 Assortments
			'ArticleColorId'	=>	0,				// 
			'ArticleSizeId'		=>	0,				// 
			'Reference'			=>	$__assortid,	// Connected assortmentid
			'Type'				=>	"assort"
		) ;
		
		$barcode = sprintf("%12.0f", ($BaseVariantCode + 1)) ;
		$barcode .= GetCheckDigit($barcode); 			// Add control digit
		$VariantCode['VariantCode'] = $barcode;
		tableWrite ('VariantCode', $VariantCode) ;
	
		echo $barcode . '!!!' . $__assortid .'<br>' ;

		return ;
	}
/*
    itemStart () ;
    itemSpace () ;
    itemField ('Project', $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;
	$query = sprintf("SELECT ol.id as Id, oq.id as OqId
	FROM   (Requisitionline ol, RequisitionlineQuantity oq)
  	 LEFT JOIN Article ON ol.ArticleId=Article.Id and Article.Active=1
	 LEFT JOIN ArticleSize ON oq.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1
	 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1
	 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1
     LEFT JOIN VariantCode ON VariantCode.ArticleId=Article.Id AND VariantCode.ArticleColorId=ArticleColor.Id AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.active=1
	WHERE ol.requisitionid=%d and ol.id=oq.requesitionlineid 
	GROUP BY ol.ArticleId,Color.Id,ArticleSize.Id
	order by ol.requisitionid
	", $Id);
*/
	// Loop through purchase order lines.
	$query = sprintf("SELECT ol.No as No, ol.Id as Id, ArticleColor.Id as ArticleColorId, CollectionMember.Id as CollectionMemberId
						FROM   (Requisitionline ol)
						 LEFT JOIN Article ON ol.ArticleId=Article.Id and Article.Active=1
						 LEFT JOIN CollectionMember ON CollectionMember.ArticleId=ol.ArticleId AND CollectionMember.ArticleColorId=ol.ArticleColorId and CollectionMember.Active=1
						 LEFT JOIN Collection ON Collection.Id=CollectionMember.CollectionId AND Collection.Active=1
						 LEFT JOIN Season ON Season.Id=Collection.SeasonId
						 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1
						 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1
						WHERE ol.requisitionid=22474 and ol.active=1 and Season.Id=728
						order by ol.No
			", $Id);
	$res = dbQuery ($query) ;

	// process each purchase order line
	while ($row = dbFetch ($res)) {
		$_asortmentid = tableGetFieldWhere('collectionmemberassortment','AssortmentId','CollectionMemberId='.$row['CollectionMemberId'].' and active=1') ;
		echo '<br>' . $row['No'].','.$row['Id'].','.$_asortmentid.','.$row['CollectionMemberId'].'<br>' ;

		// Find assortment qtys
		$assort_query = sprintf("SELECT az.id as SizeId, ca.CollectionmemberId, ca.AssortmentId,
									az.Name as Sizes, 
									ca.Quantity as Qty 
							FROM devel.collectionmemberassortment ca 
							inner join articlesize az on  ca.articlesizeid=az.id
							where ca.active=1 and az.active=1 and AssortmentId=%d
						", $_asortmentid);
		$assort_res = dbQuery ($assort_query) ;
	    if (dbNumRows($assort_res) == 0) continue ;
		while ($assort_row = dbFetch ($assort_res)) {
			$assorts[$assort_row['SizeId']] = $assort_row ;
			echo $assorts[$assort_row['SizeId']]['Sizes'] . ',' .$assorts[$assort_row['SizeId']]['Qty']  . '<br>' ;
		}

		gen_ean ($row['CollectionMemberId'],$_asortmentid) ;

		// Loop through ordered quantities
		$_line_assorts = 0; $_no_assorts = 0 ; $_first=1;
		$qty_query = sprintf("SELECT ArticleSize.Id as ArticleSizeId, ArticleSize.Name, oq.Quantity
							FROM   (RequisitionlineQuantity oq)
							 LEFT JOIN ArticleSize ON ArticleSize.Id=oq.ArticleSizeId and ArticleSize.Active=1
							WHERE oq.requesitionlineid=%d
							order by ArticleSize.displayorder
						", $row['Id']);
		$qty_res = dbQuery ($qty_query) ;
		while ($qty_row = dbFetch ($qty_res)) {
			$order_query = sprintf("SELECT oq.articlesizeid as SizeId, sum(oq.Quantity) as OrderQty
									FROM   `order` o, orderline ol, orderquantity oq
									WHERE o.seasonid=728
											and o.active=1 and o.done=0
											and ol.orderid=o.id and ol.active=1 and ol.done=0 and ol.articlecolorid=%d
											and oq.orderlineid=ol.id and oq.active=1 and oq.articlesizeid=%d group by oq.articlesizeid
											", $row['ArticleColorId'], $qty_row['ArticleSizeId']) ;
	//							", $SeasonId);
			$order_res = dbQuery ($order_query) ;
			$order_row = dbFetch ($order_res) ;
			echo '  ' . $qty_row['Name'].': '. $qty_row['Quantity'] . ',' . $order_row['OrderQty'] . ',' . $assorts[$order_row['SizeId']]['Qty'] . '<br>' ;
			if ($assorts[$order_row['SizeId']]['Qty'] > 0) {
				if ($order_row['OrderQty']>$qty_row['Quantity']) { 
					$_line_assorts = $qty_row['Quantity'] / $assorts[$order_row['SizeId']]['Qty'] ;
				} else {
					$_line_assorts = $order_row['OrderQty'] / $assorts[$order_row['SizeId']]['Qty'] ;
				}
			} else {
				$scr=1;//$_line_assorts = 0 ;
			}
			if (($_no_assorts>$_line_assorts) or $_first==1) {
				$_no_assorts = $_line_assorts ;
				$_first=0 ;
			}
			echo '  SO qty ' . $order_row['OrderQty'].',' .  $order_row['SizeId'] .',' . $_no_assorts.',' . $_line_assorts. '<br>' ;
			$_assort_id=tableGetFieldWhere('requisitionassortment','Id','requisitionassortment.requisitionlineid=' .$row['Id']. ' and requisitionassortment.assortmentid='.$assorts[$order_row['SizeId']]['AssortmentId']) ;
			if ($_assort_id>0) {
				$query = sprintf ("UPDATE `requisitionassortment` SET `requisitionlineid`=%d, `assortmentid`=%d, quantity=%d WHERE Id=%d", (int)$row['Id'],(int)($assorts[$order_row['SizeId']]['AssortmentId']),$_no_assorts,$_assort_id) ;
				dbQuery ($query) ;
			} else {
				$query = sprintf ("INSERT INTO `requisitionassortment` SET `requisitionlineid`=%d, `assortmentid`=%d, quantity=%d", (int)$row['Id'],(int)($assorts[$order_row['SizeId']]['AssortmentId']),$_no_assorts ) ;
				dbQuery ($query) ;
			}
		}
		dbQueryFree ($qty_res) ;
	}
	dbQueryFree ($res) ;
	
    printf ("<table>\n") ;
  
    itemSpace () ;
    itemFieldRaw ('Do something special', formCheckbox ('Something', $Record['Something'])) ;
	echo "<tr><td colspan=3>Generate number of assortments for this PO</td></tr>" ;
	printf ("</table>\n") ;
    
    printf ("</form>\n") ;

	return 0 ;
?>
