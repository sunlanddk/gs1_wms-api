<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    $MyCompanyId = parameterGet ('CompanyMy') ;

    // Form
    //formStart () ;
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    itemStart () ;
    itemSpace () ;
    if ($Navigation['Parameters'] == 'new') {
		if (isset($_GET['SupplierId'])) {
			$company_id = (int)$_GET['SupplierId'] ;
		} else if ($Id > 0) {
			$company_id = $Id ;
		} else {
			$company_id = 0 ;
		}

		$query = 'SELECT Company.Id, concat(Company.Name, " - ", Company.Number ) AS Value FROM Company WHERE TypeSupplier AND Active=1 ORDER BY Value' ;
		$reload = sprintf('onchange="appLoad(%d,%d,\'SupplierId=\'+document.appform.SupplierId.options[document.appform.SupplierId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
		itemFieldRaw ('Supplier', formDBSelect ('SupplierId', $company_id, $query, 'width:250px;', NULL, $reload)) ; 
		itemSpace () ;

		if ($company_id > 0) {
			// Specific Supplier
			$query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $company_id) ;
			$res = dbQuery ($query) ;
			$Company = dbFetch ($res) ;
		}

	} else {
		// Specific company
		$query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Record['CompanyId']) ;
		$res = dbQuery ($query) ;
		$Company = dbFetch ($res) ;
		dbQueryFree ($res) ;
		itemField ('Supplier', sprintf ('%s (%d)', $Company['Name'], $Company['Number'])) ;     
		itemSpace () ;
		itemEnd () ;
	}
    switch ($Navigation['Parameters']) {
		case 'new' :
			// Default Values
			unset ($Record) ;
			$Record['PurchaseUserId'] 	= $User['Id'] ;
			$Record['FromCompanyId'] 	= $User['CompanyId'] ;
			$Record['CompanyId'] 		= $company_id  ;
			$Record['CurrencyId']   	= $Company['PurchaseCurrencyId'] ;
			$Record['PaymentId'] 		= $Company['PurchasePaymentTermId'] ;
			$Record['DeliveryTermId'] 	= $Company['PurchaseDeliveryTermId'] ;
			$Record['CarrierId'] 		= $Company['PurchaseCarrierId'] ;
			$Record['DeliveryId'] 		= parameterGet('DefDeliveryCompanyId') ;
			$Record['OrdertypeId'] 		= 1 ;
			break ;
			
		Default:	
			break ;
    }

    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:250px;')) ;
    itemFieldRaw ('', formHidden ('CompanyId', $Record['CompanyId'],40)) ;
    itemFieldRaw ('Order Type', formDBSelect ('OrderTypeId', (int)$Record['OrdertypeId'], 'SELECT Id, CONCAT(Name," (",Description,")") AS Value FROM OrderType WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
   	itemFieldRaw ('Season', formDBSelect ('SeasonId', (int)$Record['SeasonId'], sprintf('SELECT Id, Name AS Value FROM Season WHERE Active=1 and Done=0 and OwnerId=%d ORDER BY Name', $Record['FromCompanyId']), 'width:250px;', array (0 => 'none'))) ;  
    itemSpace () ;
    itemFieldRaw ('Reference', formText ('Reference', $Record['Reference'], 40)) ;
    itemSpace () ;
    itemFieldRaw ('Purchase Ref', formDBSelect ('PurchaseUserId', (int)$Record['PurchaseUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND (User.Login=1 OR User.CompanyId=1) ORDER BY Value'), 'width:250px;')) ;  
//    itemFieldRaw ('Owner Company', formDBSelect ('FromCompanyId', (int)$Record['FromCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Currency', formDBSelect ('CurrencyId', (int)$Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    itemFieldRaw ('Payment Term', formDBSelect ('PaymentId', (int)$Record['PaymentId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Delivery Terms', formDBSelect ('DeliveryTermId', (int)$Record['DeliveryTermId'], 'SELECT Id, CONCAT(Name," (",Description,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
    itemFieldRaw ('Carrier', formDBSelect ('CarrierId', (int)$Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
    itemFieldRaw ('Delivery at', formDBSelect ('DeliveryId', (int)$Record['DeliveryId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Header', formTextArea ('RequisitionHeader', $Record['RequisitionHeader'], 'width:450px;height:100px;')) ;
    itemFieldRaw ('Footer', formtextArea ('RequisitionFooter', $Record['RequisitionFooter'], 'width:450px;height:100px;')) ;
    itemSpace () ;

    if (($Navigation['Parameters'] != 'new')) {
		flag ($Record, 'Ready') ;
		itemSpace () ;
		itemFieldRaw ('Done condition', formDBSelect ('DoneConditionId', (int)$Record['DoneConditionId'], 'SELECT Id, concat(Name, " (", Description, " )") AS Value FROM OrderDone WHERE Active=1 ORDER BY Name', 'width:250px;')) ;
		itemSpace () ;
		flag ($Record, 'Done') ; 
		itemInfo ($Record) ;
    }
    itemEnd () ;
    //formEnd () ;
    printf ("</form>\n") ;

    return 0 ;
?>
