<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

    $fields = array (
		'No'				=> array ('type' => 'integer',							'check' => true),
		'CaseId'			=> array ('type' => 'integer'),
		'Description'		=> array (),
		'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
		'LotId'				=> array ('type' => 'integer',								'check' => true),

		'PurchasePrice'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'ActualPrice'		=> array ('type' => 'set'), // array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'LogisticCost'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'Discount'			=> array ('type' => 'integer',	'mandatory' => true),

		'PurchasePriceConsumption'		=> array ('type' => 'set'),
		'ActualPriceConsumption'		=> array ('type' => 'set'),
		'LogisticCostConsumption'		=> array ('type' => 'set'),
		
		'RequestedDate'		=> array ('type' => 'date',								'check' => true),
		'ConfirmedDate'		=> array ('type' => 'date',								'check' => true),
		'AvailableDate'		=> array ('type' => 'date',								'check' => true),
	//	'Quantity'			=> array ('type' => 'decimal',							'check' => true),
		'Surplus'			=> array ('type' => 'integer'),

	//	'CustArtRef'		=> array (),
		'InvoiceFooter'		=> array ()
		
   ) ;
   $specialfields = array (
	 // 'ProductionId'      => array( 'type' => 'integer'),

		'LotId'				=> array ('type' => 'set'),

		'Done'			=> array ('type' => 'checkbox'),
		'DoneUserId'	=> array ('type' => 'set'),
		'DoneDate' 		=> array ('type' => 'set'),
		
	);

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Size, $Id, $fields ;
		switch ($fieldname) {
			case 'Done':
				// Ignore if not setting flag
				if ((!$changed)) return false ;

				$fields['DoneUserId']['value'] = $User['Id'] ;
				$fields['DoneDate']['value'] = dbDateEncode(time()) ;

				return true ;
			
			case 'LotId':
				// Ignore if not setting flag
				if ((!$changed)) return false ;

				return true ;
			
			case 'No' :
				if (!$changed) return false ;

				// Validate number
				$query = sprintf ('SELECT Id FROM RequisitionLine WHERE RequisitionId=%d AND Active=1 AND Id<>%d', (int)$Record['RequisitionId'], (int)$Record['Id']) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($value <= 0 or $value > ($count+1)) return sprintf ('invalid OrderLine Position number, max %d', $count+1) ;

				return true ;

			case 'ArticleColorId' :
				if ($value <= 0) return 'Not allowed without Color: Please select a color before saving' ;
				if (!$changed) return false ;

				// Validate number
				$query = sprintf ('SELECT * FROM ArticleColor WHERE Id=%d AND Active=1', $value) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
				dbQueryFree ($result) ;
				if ((int)$row['Id'] <= 0) return sprintf ('%s(%d) not found, id %d', __FILE__, __LINE__, $value) ;
				if ((int)$row['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) ArticleColor mismatch, id %d', __FILE__, __LINE__) ;

				return true ;
			
			case 'ConfirmedDate' :
				if (!$changed) return false ;

				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t < utsDateOnly(time())) return 'Confirmed Date can not be in the past' ;
				return true ;
				
			case 'AvailableDate' :
				if (!$changed) return false ;

				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t < utsDateOnly(time())) return 'Available Date can not be in the past' ;
				return true ;
			
			case 'RequestedDate' :
				if ((!$changed) or ($value=='')) return false ;
				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t <= 0) return 'Requested Date must be set' ;
				return true ;
				
			case 'Quantity' :
			if (!$changed) return false ;
			
			// Validate
			if ($value <= 0) return 'invalid Quantity' ;

			return true ;
		}
		
		return false ;	
    }
    // Update field list
//	$fields['ActualPrice']['value'] = $_POST['PurchasePrice'] ;
	$fields['PurchasePriceConsumption']['value'] = (float)str_replace(',','.',$_POST['PurchasePrice']) ;
	$fields['ActualPriceConsumption']['value'] = (float)str_replace(',','.',$_POST['PurchasePrice']) ; //$_POST['ActualPrice'] ; 
	$fields['ActualPrice']['value'] = (float)str_replace(',','.',$_POST['PurchasePrice']) ; //$_POST['ActualPrice'] ; 
	$fields['LogisticCostConsumption']['value'] = (float)str_replace(',','.',$_POST['LogisticCost']); //$_POST['LogisticCost'] ;

	$OrderLineDone=false;
//return 'test ' . $_POST['LotId'] . ' ' . $fields['LotId']['value'] ;
	$Post_Done = ($_POST['Done'] == 'on') ? 1 : 0 ;
	if (($Record['Done'] != $Post_Done) or ($Record['LotId'] != $_POST['LotId'])) {
		$specialfields['LotId']['value'] = $_POST['LotId'] ;
		// Update orderlinedone regardless of orderstate, 
		if ($Record['Done'] != $Post_Done) {		
			if($Record['OrderReady']) {
				$specialfields['Done']['value'] = ($_POST['Done'] == 'on') ? 1 : 0  ;
				$specialfields['DoneUserId']['value'] = $User['Id'] ;
				$specialfields['DoneDate']['value'] = dbDateEncode(time()) ;	
			} else return 'Order not ready' ;
			$OrderLineDone=true;
		}
		$res = saveFields ('RequisitionLine', $Id, $specialfields, true) ;
		if ($res) return $res ;

		if ($OrderLineDone) {
			// last line? set order done
			$query = sprintf('SELECT id from RequisitionLine where RequisitionId=%d and (Done=0 or isnull(Done))', $Record['RequisitionId']) ;

			$res = dbQuery ($query) ;

			if (!($row = dbFetch ($res))) {
				$Requisition = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
					'DoneConditionId'	=>  9
				) ;
				tableWrite ('Requisition', $Requisition, $Record['RequisitionId']) ;
			}
		}    
    
		if ($Record['OrderDone'] || $Record['OrderReady'] || $OrderLineDone) {
		  return 0;
		}
	}

    // Validate state of Order
    if ($Record['OrderDone']) return 'the OrderLine can not be modified when Order is Done' ;
    if ($Record['OrderReady']) return 'the OrderLine can not be modified when Order is Ready' ;

    if (!$Record['VariantColor']) {
		unset ($fields['ArticleColorId']) ;
	}	
    
    if ($Record['VariantSize']) {
		// Generat verify format
		$format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", 9-(int)$Record['UnitDecimals'], 9-(int)$Record['UnitDecimals'], (int)$Record['UnitDecimals']) ;

		$Quantity = 0 ;
		foreach ($Size as $i => $s) {
			$value_string = trim($_POST['Quantity'][(int)$s['Id']]) ;
			if ($value_string == '') $value_string = '0' ;

			// verify format
			if (!ereg($format,$value_string)) return sprintf ("invalid quantity for %s", $s['Name']) ;
			
			// Substitute , -> .
			$value_string = str_replace (',', '.', $value_string) ;
			
			$Size[$i]['Value'] = $value_string ;
			$Quantity += (float)$value_string ;
		}

		// Set total quantity for orderline
		//$fields['Quantity'] = array ('type' => 'set', 'value' => number_format ($Quantity, (int)$Record['UnitDecimals'], '.', '')) ;
	}

	// Process and save OrderLine
	$res = saveFields ('RequisitionLine', $Id, $fields, true) ;
	if ($res) return $res ;
	
	if ($Record['VariantSize']) {
		// Update Quantities by Sizes
		foreach ($Size as $i => $s) {
			if ((float)$s['Quantity'] != (float)$s['Value']) {
				if ((int)$s['OrderQuantityId'] > 0) {
					if ((float)$s['Value'] > 0) {
					// Update existing record
						$fields = array (
							'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
						) ;
						$res = saveFields ('RequisitionLineQuantity', (int)$s['OrderQuantityId'], $fields) ;
					} else {
						// Delete record
						tableDelete ('RequisitionLineQuantity', (int)$s['OrderQuantityId']) ;
					}
				} else {
					// Create new record
					$fields = array (
						'RequesitionLineId'		=> array ('type' => 'set',	'value' => (int)$Record['Id']),
						'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$s['Id']),
						'Quantity'			=> array ('type' => 'set',	'value' => $s['Value']) 
					) ;
					$res = saveFields ('RequisitionLineQuantity', -1, $fields) ;
				}
				if ($res) return $res ;
			}			    
		}
    } else {
		$fields = array (
			'Quantity'						=> array ('type' => 'set',	'value' => $_POST['Quantity'])
		) ;
		$fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
		$res = saveFields ('RequisitionLineQuantity', (int)$Record['OrderQuantityId'], $fields) ;
	}
    
    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM RequisitionLine WHERE RequisitionId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['RequisitionId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
		if ($i == (int)$Record['No']) $i += 1 ;       
		if ($i != (int)$row['No']) {
			$query = sprintf ("UPDATE RequisitionLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
			dbQuery ($query) ;
		}
		$i++ ;
    }
    dbQueryFree ($result) ;
    
    return 0 ;    
?>
