<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;
//echo 'hallo' ;
    switch ($Navigation['Parameters']) {
		case 'company' :
			itemStart () ;
			itemSpace () ;
			itemFieldIcon ('Company', tableGetField('Company','Name',$Id), 'company.gif', 'companyview', $Id) ;
			itemEnd () ;
			printf ('<br>') ;
			break ;	    
		case 'season' :
			itemStart () ;
			itemSpace () ;
			itemFieldIcon ('Season', tableGetField('Season','Name',$Id), 'project.gif', 'seasonview', $Id) ;
			itemEnd () ;
			printf ('<br>') ;
			break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 70) ;
    listHead ('State', 70) ;
    if (!$HideCompany) {
		listHead ('Company', 140) ;
    }
    listHead ('Season', 140) ;
    listHead ('Reference', 90) ;
    listHead ('Description') ;
    listHead ('Purchase resp', 160) ;
    listHead ('Created', 120) ;
    if (!$HideDone) {
		listHead ('Done', 75) ;
    }

    while ($row = dbFetch($Result)) {
		listRow () ;
		if ($Navigation['Parameters']=='season') 
			listFieldIcon ($Navigation['Icon'], 'poviewseason', (int)$row['Id']) ;
		else
			listFieldIcon ($Navigation['Icon'], 'purchaseview', (int)$row['Id']) ;
		listField ((int)$row['Id']) ;
		listField ($row['State']) ;
		if (!$HideCompany) {
			listField (sprintf ('%s (%s)', $row['CompanyName'], $row['CompanyNumber'])) ;
		}
		listField ($row['SeasonName']) ;
		listField ($row['Reference']) ;
		listField ($row['Description']) ;
		listField ($row['PurchaseUserName']) ;
		listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
		if (!$HideDone) {
			listField (($row['Done']) ? date ("Y-m-d", dbDateDecode($row['DoneDate'])) : 'No') ;
		}	
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Orders', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
