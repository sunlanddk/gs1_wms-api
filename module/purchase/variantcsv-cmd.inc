<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	
	
	global $User, $Navigation, $Record, $Size, $Id ;

	$ErrorTxt = 0 ;

    // header
    // header('Content-Length: '.$size); 
    header ('Content-Type: text/plain') ;
    header ('Content-Disposition: attachment; filename=DeliveryNote.csv') ;
    header ('Last-Modified: '.date('D, j M Y G:i:s T')) ;

	$query = sprintf ('SELECT
							if (VariantCode.VariantCode is null, "",VariantCode.VariantCode) AS VariantCode,
							VariantCode.Id AS Id,
							Article.Number as ArticleNumber, 
							if (VariantCode.VariantDescription="", Article.Description, VariantCode.VariantDescription) as VariantDescription,
							if (VariantCode.VariantColorCode="", Color.Number,VariantCode.VariantColorCode) as ColorNumber,
							if (VariantCode.VariantColorDesc="", Color.Description,VariantCode.VariantColorDesc) as VariantColor,
							if (VariantCode.VariantSize="", ArticleSize.Name,VariantCode.VariantSize) as VariantSize,
							oq.Quantity as Quantity
						FROM   (Requisitionline ol, RequisitionlineQuantity oq)
							 LEFT JOIN Article ON ol.ArticleId=Article.Id and Article.Active=1
							 LEFT JOIN CustomsPosition on Article.CustomsPositionId=CustomsPosition.Id and CustomsPosition.Active=1
							 LEFT JOIN ArticleSize ON oq.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1
							 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1
							 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1
							   LEFT JOIN VariantCode ON VariantCode.ArticleId=Article.Id AND VariantCode.ArticleColorId=ArticleColor.Id AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.active=1
						WHERE ol.requisitionid=%d and ol.id=oq.requesitionlineid 
						GROUP BY Article.Id,Color.Id,ArticleSize.Id
						ORDER BY ArticleNumber, VariantColor, DisplayOrder', $Id) ;
	$result = dbQuery ($query) ;

//	$file = fopen("test.txt","w");
	$n = 0 ;
	while ($row = dbFetch ($result)) {
		$csv_data = array_2_csv($row);
		$csv_data .= "\r\n" ;
//		echo $n . ":;; <pre>";
//		fwrite($file,$csv_data.PHP_EOL);
		print_r($csv_data);
//		echo '</pre>'   ; 
		$n++ ;  
	}
	dbQueryFree ($result) ;
//	fclose($file);
	
	return 0; // navigationCommandMark ('purchaseview', (int)$Record['Id']) ;

?>
