<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/fieldtype.inc' ;
    require_once 'lib/lookups.inc';
   
	global $User, $Navigation, $Record, $Size, $Id ;

	// query definitions.
    $StateField    = 'IF(`requisition`.Done=0 AND `requisition`.Ready=0, "Draft",
                       IF(`requisition`.Done=0 AND `requisition`.Ready=1, "Confirmed",
                       IF(`requisition`.Done=1 AND DoneConditionId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Done",
                       IF(`requisition`.Done=1 AND DoneConditionId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Done" ))))';

    $queryFields = '`requisition`.*,
		    Currency.Symbol AS CurrencySymbol,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, Company.Mail AS Mail,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS PurchaseUserName,
		    OrderDone.Name AS OrderDoneName,
		    Season.Name as SeasonName,
		    ' . $StateField . ' AS State' ;

    $queryTables = '`requisition`
		    LEFT JOIN Currency ON Currency.Id=requisition.CurrencyId
		    LEFT JOIN Company ON Company.Id=requisition.CompanyId
		    LEFT JOIN User ON User.Id=requisition.PurchaseUserId
		    LEFT JOIN Season ON Season.Id=requisition.SeasonId
		    LEFT JOIN OrderDone ON OrderDone.Id=requisition.DoneConditionId' ;
	
	// functions
    switch ($Navigation['Function']) {
		case 'list' :
			$state_options = array('Ready' => 'Ready', 'Defined' => 'Defined', 'Done' => 'Done');

			// List field specification
			$fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Number',		'db' => 'requisition.Id',									'direct' => 'requisition.Active=1', 	'type' => FieldType::INTEGER,	'sort'=>'desc'),
				array ('name' => 'Company',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")', 	'type' => FieldType::STRING),
		//		array ('name' => 'State',		'db' => $StateField, 										'type' => FieldType::SELECT, 	'options' => $state_options),
				array ('name' => 'State',		'db' => $StateField, 										'type' => FieldType::STRING),
		//		array ('name' => 'Season',		'db' => 'Season.Id', 										'type' => FieldType::SELECT, 	'options' => getOwnerSeasons($User['CompanyId'])),
				array ('name' => 'Season',		'db' => 'Season.Name', 										'type' => FieldType::STRING),
				array ('name' => 'Reference',	'db' => 'requisition.Reference', 							'type' => FieldType::STRING),
				array ('name' => 'Description',	'db' => 'requisition.Description', 							'type' => FieldType::STRING),
				array ('name' => 'Purchase resp','db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', 'type' => FieldType::STRING),
				array ('name' => 'Created',		'db' => 'requisition.CreateDate', 							'type' => FieldType::DATE)
			) ;
			switch ($Navigation['Parameters']) {
				case 'season' :
					// Display
					$HideDone = false ;

					// Clause
					$queryClause = 'requisition.seasonid='. $Id . '  AND requisition.Active=1  ' ;
					break ;
				case 'company' :
					// Display
					$HideDone = false ;

					// Clause
					$queryClause = 'requisition.companyid='. $Id . '  AND requisition.Active=1  ' ;
					break ;
				case 'all' :
					// Display
					$HideDone = false ;

					// Clause
					$queryClause = '  requisition.Active=1  ' ;
					break ;
				default :
					// Display
					$HideDone = true ;

					// Clause
					$queryClause = 'requisition.Done=0 AND requisition.Active=1  AND (requisition.FromCompanyId=787 or requisition.FromCompanyId=0)' ;
					break ;
			}

			require_once 'lib/list.inc' ;
			$Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
			if (is_string($Result)) return $Result ;

			$res = listView ($Result, 'requisitionview') ;
			return $res ;
			break ;
			
		case 'import-lines' :
		case 'import-lines-cmd' :
			$query = sprintf ("SELECT * FROM Season WHERE Active=1 AND Id=%d", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			return 0 ;

		case 'basic' :
 		case 'basic-cmd' :
 		case 'delete-cmd' :
		case 'linenew' :
 		case 'linenew-cmd' :
			$query = sprintf ("SELECT Requisition.*, Company.Name AS CompanyName, Company.Number AS CompanyNumber FROM Requisition LEFT JOIN Company ON Company.Id=Requisition.CompanyId WHERE Requisition.Active=1 AND Requisition.Id=%d ", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			navigationEnable('editrequisition') ;
			navigationEnable('editreqlines') ;
			return 0 ;

		case 'view' :
		case 'revert' :
		case 'revert-cmd' :
			$query = sprintf ("SELECT *,  %s AS State FROM Requisition WHERE Active=1 AND Id=%d", $StateField, $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			navigationEnable('editrequisition') ;
			navigationEnable('editreqlines') ;
			return 0 ;
			
		case 'lines' :
		case 'lines-cmd' :
		case 'importprepack' :
		case 'importprepack-cmd' :
			$query = sprintf ("SELECT * FROM Requisition WHERE Active=1 AND Id=%d", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			return 0 ;
			
		case 'lineview' :
		case 'lineedit' :
		case 'lineedit-cmd' :
		case 'linedelete-cmd' :
			global $Color, $Size, $Dim, $Quantity ;		// For direct view only

			// Query requisition
			$query = sprintf ('SELECT RequisitionLine.*,
								requisition.Id AS OrderId, requisition.Ready AS OrderReady, requisition.Done AS OrderDone, requisition.FromCompanyId as FromCompanyId, requisition.SeasonId AS SeasonId,
								Company.Id AS CompanyId, Company.Number AS CompanyNumber, Company.Name AS CompanyName,
								Currency.Symbol AS CurrencySymbol, CollectionLot.Name as CollectionLotName,
								Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, 
								Article.VariantSize,Article.VariantDimension, Article.HasStyle, CostCurrency.Symbol as CostCurrencySymbol,
								Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
								sum(rlq.quantity) as Quantity, 
								sum(rlq.quantityreceived) as QuantityReceived, 
								rlq.id as OrderQuantityId
								FROM RequisitionLine
								LEFt JOIN RequisitionLineQuantity rlq ON rlq.requesitionlineid=RequisitionLine.id
								LEFT JOIN Article ON Article.Id=requisitionLine.ArticleId
								LEFT JOIN Unit ON Unit.Id=Article.UnitId
								LEFT JOIN `requisition` ON requisition.Id=requisitionLine.requisitionId
								LEFT JOIN Currency ON Currency.Id=requisition.CurrencyId
								LEFT JOIN Currency CostCurrency ON CostCurrency.Id=Article.CostCurrencyId
								LEFT JOIN Company ON Company.Id=requisition.CompanyId
								LEFT JOIN CollectionLot ON CollectionLot.Id=RequisitionLine.LotId
								WHERE requisitionLine.Id=%d AND requisitionLine.Active=1 group by RequisitionLine.Id', $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;

			if ($Record['VariantSize']) {
				// Get sizes
				$query = sprintf ("SELECT ArticleSize.*, RequisitionLineQuantity.Id AS OrderQuantityId, RequisitionLineQuantity.Quantity, RequisitionLineQuantity.QuantityReceived 
									FROM ArticleSize 
									LEFT JOIN RequisitionLineQuantity ON RequisitionLineQuantity.RequesitionLineId=%d AND RequisitionLineQuantity.Active=1 AND RequisitionLineQuantity.ArticleSizeId=ArticleSize.Id 
									WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
				$result = dbQuery ($query) ;
				$Size = array() ;
				while ($row = dbFetch ($result)) {
					$Size[] = $row ;
				}
				dbQueryFree ($result) ;
				if (count($Size) == 0) return 'no Sizes specified for Article' ;
			} else {
			}

			if ($Record['VariantDimension']) {
				// Get sizes
				$query = sprintf ("SELECT RequisitionLineQuantity.Dimension as Name, RequisitionLineQuantity.Id AS OrderQuantityId, RequisitionLineQuantity.Quantity FROM RequisitionLineQuantity 
									WHERE RequisitionLineQuantity.RequisitionLineId=%d AND RequisitionLineQuantity.Active=1 ORDER BY RequisitionLineQuantity.Dimension", $Id) ;
				$result = dbQuery ($query) ;
				$Size = array() ;
				while ($row = dbFetch ($result)) {
					$Size[] = $row ;
				}
				dbQueryFree ($result) ;
	//			if (count($Dim) == 0) return 'no Sizes specified for Article' ;
			}

			if ($Record['VariantColor']) {
				// Get Color information
				$query = sprintf ('SELECT ArticleColor.Id, Color.Description AS ColorDescription, Color.Number AS ColorNumber, ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue
									FROM ArticleColor
									LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
									LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
									WHERE ArticleColor.Id=%d', (int)$Record['ArticleColorId']) ;
				$res = dbQuery ($query) ;
				$Color = dbFetch ($res) ;
				dbQueryFree ($res) ;
			}
			break ;

		case 'printorder' :
			global $Line;
			// Query Order
			$query = sprintf ('SELECT %s FROM %s WHERE Requisition.Id=%d AND Requisition.Active=1', $queryFields, $queryTables, $Id) ;
//			$query = sprintf ('SELECT * FROM Requisition WHERE Requisition.Id=%d AND Requisition.Active=1', $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;

			// Variables
			$SizeCount = 0 ;

			// Get OrderLines
			$Line = array () ;
			$query = sprintf ('SELECT RequisitionLine.Id, min(RequisitionLine.No) as No, 
									 group_concat(RequisitionLine.No) as NoGroup,
									 count(RequisitionLine.Id) as NoGroupLines,
									`RequisitionLine`.`Description`,
									`RequisitionLine`.`RequisitionId`,
									`RequisitionLine`.`CaseId`,
									`RequisitionLine`.`ArticleId`,
									`RequisitionLine`.`ArticleColorId`,
									`RequisitionLine`.`Surplus`,
									`RequisitionLine`.`PurchasePrice`,
									`RequisitionLine`.`LogisticCost`,
									`RequisitionLine`.`RequestedDate`,
									`RequisitionLine`.`ArticleCertificateId`,
									`RequisitionLine`.`ProductionId`,
									`RequisitionLine`.`Done`,
									`RequisitionLine`.`DoneUserId`,
									`RequisitionLine`.`DoneDate`,
									`RequisitionLine`.`ConfirmedDate`,
									`RequisitionLine`.`LineFooter`,
									Case.CustomerReference, Case.ArticleCertificateId, ArticleColor.Id as ArticleColorId,
									Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantDimension, VariantSortation,
									Color.Description AS ColorDescription, Color.Number AS ColorNumber,
									ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
									Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, 
									Country.Name as WorkCountryName, Country.Name as WorkCountryId
				FROM RequisitionLine
				LEFT JOIN `Case` ON Case.Id=RequisitionLine.CaseId
				LEFT JOIN Article ON Article.Id=RequisitionLine.ArticleId
				LEFT JOIN ArticleColor ON ArticleColor.Id=RequisitionLine.ArticleColorId
				LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
				LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
				LEFT JOIN Unit ON Unit.Id=Article.UnitId
				LEFT JOIN Country ON Article.WorkCountryId=Country.Id
				WHERE RequisitionLine.RequisitionId=%d AND RequisitionLine.Active=1 
				GROUP BY RequisitionLine.ArticleId, RequisitionLine.ArticleColorId
				ORDER BY RequisitionLine.RequestedDate, No', $Id) ;
			$result = dbQuery ($query) ;
			while ($row = dbFetch ($result)) {
				$Line[(int)$row['Id']] = $row ;
				if (isset($row['RequestedDate']))
					$Record['RequestedDate'] = $row['RequestedDate'] ;
				if (isset($row['ConfirmedDate']))
					$Record['ConfirmedDate'] = $row['ConfirmedDate'] ;
			}
			dbQueryFree ($result) ;

			// Get size specific quantities for each Line with VariantSize/Dimension
			foreach ($Line as $i => $l) {
				$Line[$i]['Size'] = array () ;
				if ($l['VariantSize']) {
					$query = sprintf ("SELECT ArticleSize.*, RequisitionLineQuantity.Id AS RequisitionLineQuantityId, RequisitionLineQuantity.Quantity, 
											vc.VariantCode as VariantCode, RequisitionLine.PurchasePrice, RequisitionLine.No as OrderLineNo
										FROM ArticleSize
										INNER JOIN RequisitionLineQuantity ON RequisitionLineQuantity.Active=1 AND RequisitionLineQuantity.ArticleSizeId=ArticleSize.Id
										INNER JOIN RequisitionLine On RequisitionLine.requisitionid=%d and RequisitionLine.Id=RequisitionLineQuantity.RequesitionLineId 
																		and RequisitionLine.articlecolorid=%d and RequisitionLine.active=1
										LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
										WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 And RequisitionLineQuantity.Quantity>0  AND RequisitionLineQuantity.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", 
										$Id, (int)$l['ArticleColorId'],(int)$l['ArticleId'],(int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
				} else if ($l['VariantDimension']) {
					$query = sprintf ("SELECT RequisitionLineQuantity.Dimension as Name, RequisitionLineQuantity.Dimension as Id, RequisitionLineQuantity.Id AS RequisitionLineQuantityId, RequisitionLineQuantity.Quantity
										FROM RequisitionLineQuantity
										WHERE RequisitionLineQuantity.RequesitionLineId=%d AND RequisitionLineQuantity.Active=1", $i) ;
				} else continue ;

				$result = dbQuery ($query) ;
				$n = 0 ;
				while ($row = dbFetch ($result)) {
					$Line[$i]['Size'][$row['Id']] = $row ;
					$Line[$i]['Quantity'] += $row['Quantity'] ;
					$Line[$i]['PriceSaleSubTotal'] += $row['Quantity'] * $row['PurchasePrice'];
					$n++ ;
				}
				dbQueryFree ($result) ;

				// Update max size count
				if ($n > $SizeCount) $SizeCount = $n ;
			}
			break ;

		default:
			break ;
		
    }

    return 0 ;
?>
