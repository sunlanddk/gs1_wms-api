<?php

    require_once 'lib/navigation.inc' ;

	$LotId = (int)$_POST['LotId'];
	$RequestedDate = $_POST['RequestedDate'] ;
	$AvailableDate = $_POST['AvailableDate'] ;
	$ConfirmedDate = $_POST['ConfirmedDate'] ;
	

	// Update lines
    foreach ($_POST['SelectFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		
		// Make some validations?
		$SomethingSet=1 ;
/*
		$query = sprintf ("SELECT 
							FROM 
							WHERE  ", $id) ;
		$res = dbQuery ($query) ;
		$row = dbFetch($res) ;
		dbQueryFree ($res) ;
*/
		$RequisitionLine = array (
			'LotId'			=> $LotId,
			RequestedDate => $RequestedDate,
			ConfirmedDate => $ConfirmedDate,
			AvailableDate => $AvailableDate
		) ;
		tableWrite ('RequisitionLine', $RequisitionLine, $id) ;
	}

	if ($SomethingSet)
	    return 0 ;
	else
		return "No lines selected - Nothing changed" ;
?>
