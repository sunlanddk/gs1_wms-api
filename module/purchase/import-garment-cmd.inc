<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
   	require_once 'lib/reader.inc';
    require_once 'lib/save.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

	$StartRow = 7 ;
    $ColomnConfig = array (    
		'Article'		=> 1,
		'Color'			=> 3,
		'Size'			=> 5,
		'Quantity'		=> 7,
		'Price'			=> 8,
		'LogisticPrice'		=> 9,
		'RequestedDate'		=> 10,
		'AvailableDate'		=> 11,
		'AltSupplierId'		=> 16
    ) ;

	$Error = 0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);
		
		$k = 0 ;

		$First = 1 ;
		$NextLineNo = 1 ;
		set_time_limit (300) ;

		// Process lines
		for ($i = $StartRow; $i <= $data->sheets[0]['numRows']; $i++) {
			if ($data->sheets[$k]['cells'][$i][$ColomnConfig['Article']] == '') continue ; // Ignore line if no articlenumber 

			// Get article information
			$query = sprintf ("SELECT * FROM Article WHERE Active=1 AND Number='%s'", $data->sheets[$k]['cells'][$i][$ColomnConfig['Article']]) ;
		      $res = dbQuery ($query) ;
			$Article = dbFetch ($res) ;
			dbQueryFree ($res) ;
			if ($Article['Id'] == 0) return sprintf('Article %s in line %d dosnt exist',$data->sheets[$k]['cells'][$i][$ColomnConfig['Article']], $i) ;

			// Get supplier information
			if (((int)$data->sheets[$k]['cells'][$i][$ColomnConfig['AltSupplierId']]) > 0)
				$ArticleSupplierCompanyId = (int)$data->sheets[$k]['cells'][$i][$ColomnConfig['AltSupplierId']] ;
			else
				$ArticleSupplierCompanyId = $Article['SupplierCompanyId'] ;
				
			// Existing or new purchase order
			if ($PurchaseOrders[$ArticleSupplierCompanyId] > 0 ) {
				$PurchaseOrderId = $PurchaseOrders[$ArticleSupplierCompanyId] ;
			} else {
				// Create new PurchaseOrder for supplier of article
				$First = 0 ;
				$query = sprintf ("SELECT * FROM Company WHERE Active=1 AND Id=%d", $ArticleSupplierCompanyId) ;
				$res = dbQuery ($query) ;
				$Supplier = dbFetch ($res) ;
				dbQueryFree ($res) ;
				if ($Supplier['Id'] == 0) return sprintf('Supplier for Article %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
				
				// Create new Purchase Order
				$PurchaseOrder = array (
//					'Id' => 0,
					'CompanyId' => $Supplier['Id'],
					'SeasonId' => $_POST['SeasonId'],
//					'Description' => sprintf('%s - %s', $Record['Name'], tableGetField('Company','Name',$Record['CompanyId'])) ,
					'Reference' => '' ,
					'PurchaseUserId' => $Supplier['PurchaseRefId'],
					'CurrencyId' => $Supplier['PurchaseCurrencyId'],
					'PaymentId' => $Supplier['PurchasePaymentTermId'],
					'DeliveryTermId' => $Supplier['PurchaseDeliveryTermId'],
					'CarrierId' => $Supplier['PurchaseCarrierId'],
					'DeliveryId' => $User['CompanyId'], 
//					'RequisitionHeader' => $data->sheets[$k]['cells'][3][2],
//					'RequisitionFooter' => $data->sheets[$k]['cells'][4][2],
					'FromCompanyId' => $User['CompanyId']
				) ;
				$PurchaseOrderId = tableWrite ('requisition', $PurchaseOrder) ;
				$PurchaseOrders[$Article['SupplierCompanyId']] = $PurchaseOrderId ;
			}


			if ($data->sheets[$k]['cellsInfo'][$i][$ColomnConfig['Price']]['type']) {
				$PricePurchase=$data->sheets[$k]['cells'][$i][$ColomnConfig['Price']] ;
			} else {
				$PriceArray = explode(',', $data->sheets[$k]['cells'][$i][$ColomnConfig['Price']]) ;
				switch (Count($PriceArray)) {
				   case 1:
					$PricePurchase = $PriceArray[0] ;
					break ;
				   case 2:
					$PricePurchase = $PriceArray[0] . '.' . $PriceArray[1] ;
					break ;
				  default:
					$PricePurchase = 0 ; // Error msg?
				}
			}
			
			if ($data->sheets[$k]['cellsInfo'][$i][$ColomnConfig['LogisticPrice']]['type']) {
				$PriceLogistic=$data->sheets[$k]['cells'][$i][$ColomnConfig['LogisticPrice']] ;
			} else {
				$PriceArray = explode(',', $data->sheets[$k]['cells'][$i][$ColomnConfig['LogisticPrice']]) ;
				switch (Count($PriceArray)) {
				   case 1:
					$PriceLogistic = $PriceArray[0] ;
					break ;
				   case 2:
					$PriceLogistic = $PriceArray[0] . '.' . $PriceArray[1] ;
					break ;
				  default:
					$PriceLogistic = 0 ; // Error msg?
				}
			}
			

			// Get articleColor information
			if ($Article['VariantColor']) {
				$Color = $data->sheets[$k]['cells'][$i][$ColomnConfig['Color']] ;
				$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s' and Active=1",$data->sheets[$k]['cells'][$i][$ColomnConfig['Color']]));
				if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $data->sheets[$k]['cells'][$i][$ColomnConfig['Color']], $i) ;
				$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d And Active=1",$Article['Id'],$ColorId));
				if ($ArticleColorId == 0) return sprintf('ArticleColor <%s>, in line %d dosnt exist %d,%d', $data->sheets[$k]['cells'][$i][$ColomnConfig['Color']], $i,$Article['Id'],$ColorId ) ;

				$queryWhereClause = sprintf('RequisitionId=%d AND ArticleId=%d AND ArticleColorId=%d AND PurchasePrice=%f AND Active=1',$PurchaseOrderId,$Article['Id'],$ArticleColorId,(float)$PricePurchase);
			} else {
				$ArticleColorId = 0 ;

				$queryWhereClause = sprintf('RequisitionId=%d AND ArticleId=%d AND Active=1',$PurchaseOrderId,$Article['Id']);
			}
			
			// Get article size info
			if ($Article['VariantSize']) {
				$ArticleSizeId = tableGetFieldWhere ('articlesize','Id', sprintf("Active=1 And ArticleId=%d AND Name='%s'", $Article['Id'],$data->sheets[$k]['cells'][$i][$ColomnConfig['Size']])) ;
				//return sprintf('%d - %s', $ArticleSizeId, sprintf("Active=1 And ArticleId=%d AND Name='%s'", $Article['Id'],$data->sheets[$k]['cells'][$i][$ColomnConfig['Size']])) ;
			}
			// Get allocation information
//			$CaseId = (int)($data->sheets[$k]['cells'][$i][6]) ;
//			$ProductionId = (int)tableGetFieldWhere ('production', 'Id', sprintf("active=1 AND caseid=%d AND Number=%d", (int)$data->sheets[$k]['cells'][$i][6], (int)$data->sheets[$k]['cells'][$i][7])) ;
			$queryAllocWhereClause = '' ; //sprintf(' AND CaseId=%d AND ProductionId=%d', $CaseId, $ProductionId) ;
//return $queryAllocWhereClause ;
			// Create quantity and/or line

			
			$PurchaseOrderLineId = tableGetFieldWhere ('requisitionline', 'Id', $queryWhereClause . $queryAllocWhereClause) ;
			if ($PurchaseOrderLineId>0) {
				// Purchase OrderLine allready exist.
				$LineExist = 1;
				if ($Article['VariantSize']) {
					if (tableGetFieldWhere ('requisitionlinequantity', 'Id', sprintf("Active=1 And ArticleSizeId=%d And RequesitionLineId=%d", $ArticleSizeId, $PurchaseOrderLineId)) > 0)
						return sprintf('Same size variant for the 2nd time in line %d', $i) ;
				} else if ($Article['VariantDimension']) {
					if (tableGetFieldWhere ('requisitionlinequantity', 'Id', sprintf("Active=1 And Dimension=%s And RequesitionLineId=%d", $data->sheets[$k]['cells'][$i][$ColomnConfig['Size']], $PurchaseOrderLineId)) > 0)
						return sprintf('Same size variant for the 2nd time in line %d', $i) ;
				} else 
					return sprintf('Same variant for the 2nd time in line %d', $i) ;
			} else {
				$query = sprintf('select max(`No`) as LineNo from requisitionline where requisitionid=%d', $PurchaseOrderId ) ;
			      $res = dbQuery ($query) ;
				$Line = dbFetch ($res) ;
				dbQueryFree ($res) ;
				

 				$ArticlePurchaseUnitId = tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$Article['Id'], $Article['UnitId'])) ;
				if ($ArticlePurchaseUnitId > 0) {
					$gryf = 1;
				} else {
					unset ($ArticlePurchaseUnit) ;
					$ArticlePurchaseUnit = array (
		//				'Id' => 0,
						'ArticleId' => $Article['Id'],
						'UnitId' => $Article['UnitId'],
						'Ratio' => 1
					) ;
					$ArticlePurchaseUnitId = tableWrite ('articlepurchaseunit', $ArticlePurchaseUnit) ;
				}
				
				// Create new Purchase Order Line
				unset ($PurchaseOrderLine) ;
				$PurchaseOrderLine = array (
					'Id' => 0,
					'RequisitionId' => $PurchaseOrderId,
					'ArticleId' => $Article['Id'],
					'No' => (int)$Line['LineNo']+1,
					'Description' => $Article['Description'],
					'ArticleCertificateId' => 0,
					'Surplus' => $Supplier['PurchaseSurplus'],
					'PurchasePriceConsumption' => $PricePurchase,
					'ActualPriceConsumption' => $PricePurchase,
					'LogisticCostConsumption' => $PriceLogistic,
					'PurchasePrice' => $PricePurchase,
					'ActualPrice' => $PricePurchase,
					'LogisticCost' => $PriceLogistic,
					'CaseId' => 0,
					'ProductionId' => 0,
//					'RequestedDate' => $data->sheets[$k]['cells'][$i][8],
//					'ConfirmedDate' => ,
//					'ExpectedDate' => 0,
//					'AvailableDate' => $data->sheets[$k]['cells'][$i][9],
					'LineFooter' => '' , //$data->sheets[$k]['cells'][$i][15],
					'ArticleComment' => $Article['Comment'],
					'Group' => 0,
					'ArticlePurchaseUnitId' => $ArticlePurchaseUnitId,								
					'ArticlePURatio' => 1,								
					'ArticlePUName' => tableGetField('Unit','Name',$Article['UnitId']),								
					'ArticlePUDecimals' => tableGetField('Unit','Decimals',$Article['UnitId'])				
				) ;
				
				if ($ArticleColorId>0)
					$PurchaseOrderLine['ArticleColorId'] = $ArticleColorId ;
				if (ereg ("^([0-9]{4})-([0-9]{2})-([0-9]{2})$", $data->sheets[$k]['cells'][$i][$ColomnConfig['RequestedDate']], $regs))
					$PurchaseOrderLine['RequestedDate'] = $data->sheets[$k]['cells'][$i][$ColomnConfig['RequestedDate']];
				if (ereg ("^([0-9]{4})-([0-9]{2})-([0-9]{2})$", $data->sheets[$k]['cells'][$i][$ColomnConfig['AvailableDate']], $regs))
					$PurchaseOrderLine['AvailableDate'] = $data->sheets[$k]['cells'][$i][$ColomnConfig['AvailableDate']];
				$PurchaseOrderLineId = tableWrite ('requisitionline', $PurchaseOrderLine) ;
				$NextLineNo ++ ;
			}
			
			// Create new Purchase Order Line Quantity
			unset ($PurchaseOrderLineQty) ;
			$PurchaseOrderLineQty = array (
				'Id' => 0,
				'RequesitionLineId' => $PurchaseOrderLineId,
				'ArticleSizeId' => 0,
//				'Dimension' => NULL,
				'Quantity' => (int)$data->sheets[$k]['cells'][$i][$ColomnConfig['Quantity']],
			) ;
			if ($Article['VariantSize']) {
				$PurchaseOrderLineQty['ArticleSizeId'] =  $ArticleSizeId;
				if ($PurchaseOrderLineQty['ArticleSizeId'] == 0) return sprintf('ArticleSize <%s>, in line %d dosnt exist %d, %d', $data->sheets[$k]['cells'][$i][$ColomnConfig['Size']], $i,$Article['Id'],$ArticleSizeId) ;
			}
			if ($Article['VariantDimension']) $PurchaseOrderLineQty['Dimension'] = $data->sheets[$k]['cells'][$i][$ColomnConfig['Size']];
			$PurchaseOrderLineQtyId = tableWrite ('requisitionlinequantity', $PurchaseOrderLineQty) ;
		}
	}
	if (sizeof($PurchaseOrders) > 1)
		return navigationCommandMark ('purchaselist', (int)$Id) ;
	else if (sizeof($PurchaseOrders)== 1)
		return navigationCommandMark ('purchaseview', (int)$PurchaseOrderId) ;
	else return 'No Purchase Orders created' ;
?>
