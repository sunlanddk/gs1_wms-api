<?php

    require_once 'lib/table.inc' ;

    // Ready ?
    if ($Record['Ready']) return 'An Order that is Ready cant be deleted' ;

    $query = sprintf ("SELECT Id FROM RequisitionLine WHERE RequisitionId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    while ($row = dbFetch ($result)) {
//    	tableDelete ('RequisitionLine', $row['Id']) ;
		$query = sprintf ("UPDATE RequisitionLineQuantity SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d where RequesitionLineId=%d", dbDateEncode(time()), $User["Id"], $row['Id']) ;
		dbQuery ($query) ;
    }		
    dbQueryFree ($result) ;

    $query = sprintf ("UPDATE RequisitionLine SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE RequisitionId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
    dbQuery ($query) ;

    // Do delete  
    tableDelete ('Requisition', $Id) ;

    return 0;
?>
