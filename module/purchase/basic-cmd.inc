<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc';
	require_once 'lib/parameter.inc';
    
	$_reqcontainer = parameterGet('ReqContainerId') ;
	
	$ReadySet=0 ;
	
    $fields = array (
		'CompanyId'			=> array  ('type' => 'integer',	'mandatory' => false),
//		'FromCompanyId'		=> array ('type' => 'set'),

		'Description'		=> array (),
		'Reference'			=> array (),
		'OrderTypeId'		=> array ('type' => 'integer',	'mandatory' => false),
		'SeasonId'			=> array ('type' => 'integer',	'mandatory' => false),

		'PurchaseUserId'	=> array ('type' => 'integer',	'mandatory' => true),
		'CurrencyId'		=> array ('type' => 'integer',	'mandatory' => true),
		'PaymentId'			=> array ('type' => 'integer',	'mandatory' => true),
		'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true),
		'CarrierId'			=> array ('type' => 'integer',	'mandatory' => false),
		'DeliveryId'			=> array ('type' => 'integer',	'mandatory' => true),

		'RequisitionHeader'	=> array (),
		'RequisitionFooter'	=> array (),

		'Ready'				=> array ('type' => 'checkbox',							'check' => true),
		'ReadyUserId'		=> array ('type' => 'set'),
		'ReadyDate'			=> array ('type' => 'set'),
		'DoneConditionId'	=> array ('type' => 'integer'),
		'Done'				=> array ('type' => 'checkbox',							'check' => true),
		'DoneUserId'		=> array ('type' => 'set'),
		'DoneDate' 			=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields, $_reqcontainer, $ReadySet;

		switch ($fieldname) {
			case 'Ready':
				// Ignore if not setting flag
				if (!$changed or !$value) return false ;
				
				// Validate if any Order Positions and orderlines?
				$query = sprintf ("SELECT * FROM RequisitionLine WHERE RequisitionId=%d AND Active=1", $Id) ;
				$result = dbQuery ($query) ;
				$n = dbNumRows($result) ;
				dbQueryFree ($result) ;
				if ($n == 0) return 'the Order can not be set Ready when it has no OrderLines' ;

				// Set tracking information
				$fields['ReadyUserId']['value'] = $User['Id'] ;
				$fields['ReadyDate']['value'] = dbDateEncode(time()) ;

				$ReadySet=1 ;// Create items
				
				return true ;

			case 'Done':
				// Ignore if not setting flag
				if (!$changed or !$value) return false ;

				// Check for Allocation
				if (!$Record['Ready']) return 'the Order can not be Done before it has been set Ready' ;	

				// Check that OrderDone condition exists
				if ((int)$fields['DoneConditionId']['value'] <= 0) return 'Done condition has not been set' ;

				// Set tracking information
				$fields['DoneUserId']['value'] = $User['Id'] ;
				$fields['DoneDate']['value'] = dbDateEncode(time()) ;
				
				// Get orderlines
				$query = sprintf ("SELECT * FROM RequisitionLine WHERE RequisitionId=%d AND Active=1 And Done=0", $Id) ;
				$res = dbQuery ($query) ;

				// Set Orderlines done
				$OrderLine = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time())
				) ;
				while ($row = dbFetch ($res)) {
					tableWrite ('RequisitionLine', $OrderLine, $row['Id']) ;
				}
				
				// Delete items not received
				$query = sprintf('update item set active=0 where requisitionid=%d and containerid=%d', $Id, $_reqcontainer) ;
				dbQuery ($query) ;
				return true ;

			default:
				break;
		}
		if (!$changed) return false ;

		return true ;	
    }

    // Main start
    switch ($Navigation['Parameters']) {
	case 'new' :
//	    unset ($fields['Ready']) ;
//	    unset ($fields['Done']) ;
	    $Id = -1 ;
	    break ;

	default :
	// 	if ($Record['Ready']) return sprintf ('the Order can not be modified when Ready, field %s', $fieldname) ;
	    if ($Record['Done']) return 'the Order can not be modified when Done' ;
	    break ;
    }
    
    $res = saveFields ('requisition', $Id, $fields, true) ;
    if ($res) return $res ;

	// Create items if PO is set ready.
if ($ReadySet) {
//return 'creating items not ready yet - use .net' ;
	$query = sprintf( 
			"SELECT rl.*,
					a.Number as ArticleNumber, a.Description as ArticleDescription, c.number as ColorNumber, CollectionLot.Name as CollectionLotName,
					 rlq.articlesizeid as ArticleSizeId, rlq.quantity as Quantity
			  FROM (requisitionline rl, article a, requisitionlinequantity rlq)
			  LEFT JOIN articlecolor ac ON ac.id=rl.articlecolorid
			  LEFT JOIN color c ON c.id=ac.colorid
			  LEFT JOIN CollectionLot ON CollectionLot.Id=rl.LotId
			  WHERE a.Id=rl.articleid and rl.RequisitionId=%d and rl.Active=1 
					and rlq.requesitionlineid=rl.id and rlq.quantity>0 and rlq.active=1
			  ORDER BY No", $Id) ;
	$res = dbQuery ($query) ;

	$ReqContainerId = (int)parameterGet('ReqContainerId') ;
    while ($row = dbFetch($res)) {
		$item = array ();
		$item = array (
			'RequisitionLineId'		=> $row['Id'],
			'RequisitionId'			=> $row['RequisitionId'],

			'ContainerId'			=> $ReqContainerId,
			'ArticleId'				=> $row['ArticleId'],
			'ArticleColorId'		=> $row['ArticleColorId'],
			'ArticleSizeId'			=> $row['ArticleSizeId'],

//			'ItemOperationId'		=> $row['Id'],

//			'Price'					=> $row['PurchasePrice']*$CurrencyRate+$row['LogisticCost'],
			'Quantity'				=> $row['Quantity'],
			
			'Sortation'				=> 1,
			'OwnerId'				=> 787
		) ;
		tableWrite ('Item', $item) ;
	}
}
    switch ($Navigation['Parameters']) {
		case 'new' :
			// View
			return navigationCommandMark ('purchaseview', (int)$Record['Id']) ;
    }

    return 0 ;    
?>
