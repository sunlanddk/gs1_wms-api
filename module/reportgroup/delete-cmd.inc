<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Report WHERE ReportGroupId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the ReportGroup has associated Report") ;

    // Do deleting
    tableDelete ('ReportGroup', $Id) ;
    tableDeleteWhere ('ReportRole', 'ReportGroupId='.$Id) ;

    return 0
?>
