<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array ()
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ReportGroup WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'ReportGroup allready existing' ;
		return true ;
	}
	return false ;
    }
     
    $res = saveFields ('ReportGroup', $Id, $fields, true) ;
    if ($res) return $res ;

    // Walk through the Roles
    $n = 0 ;
    while ($row = dbFetch($Result)) {
	if ($_POST['Access'][$row['Id']] == "on") {
	    // Access requested for this Role
	    if ($row['ReportRoleId'] > 0) continue ;

	    // Create new access antry
	    tableWrite ('ReportRole', array('ReportGroupId' => $Record['Id'], 'RoleId' => $row['Id'])) ;
	    
	} else {
	    // No access for this Role
	    if ($row['ReportRoleId'] == 0) continue ;
	    
	    // Delete current entry
	    tableDelete ('ReportRole', $row['ReportRoleId']) ;
	}
    	$n++ ;
    }
    dbQueryFree ($Result) ;

    if ($n > 0) tableTouch ('ReportGroup', $Record['Id']) ;

    return 0 ;   
?>
