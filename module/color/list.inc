<?php

    require_once 'lib/list.inc' ;
    
    // Filter Bar
    listFilterBar () ;
    
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Number', 80) ;
    listHead ('Colour', 60) ;
    listHead ('', 10) ;
    listHead ('Description') ;
    listHead ('Group', 150) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'coloredit', $row['Id']) ;
	listField ($row['Number']) ;
	if ($row['ColorGroupId'] > 0) {
	    listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	} else {
	    listField ('') ;
	}
	listField ('') ;
	listField ($row['Description']) ;
	if ($row['ColorGroupId'] > 0) {
	    listField ($row['ColorGroupName']) ;
	}
	
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Colors', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
