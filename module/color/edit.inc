<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Number</td><td><input type=text name=Number maxlength=10 size=10' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemfield>Name</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=AltDescription maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['AltDescription'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td>%s</td></tr>\n", htmlDBSelect ("ColorGroupId style='width:150px'", $Record['ColorGroupId'], 'SELECT Id, Name AS Value FROM ColorGroup WHERE Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>DK name</td><td><input type=text name=Desc[4] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['DescDK'])) ;
    printf ("<tr><td class=itemfield>DK Description</td><td><textarea name=LongDesc[4] style='width:100%%;height:150px;'>%s</textarea></td></tr>\n",htmlentities($Record['LongDescDK'])) ;
	
    printf ("<tr><td class=itemfield>DE name</td><td><input type=text name=Desc[5] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['DescDE'])) ;
    printf ("<tr><td class=itemfield>DE Description</td><td><textarea name=LongDesc[5] style='width:100%%;height:150px;'>%s</textarea></td></tr>\n",htmlentities($Record['LongDescDE'])) ;
 
	printf ("<tr><td class=itemfield>UK name</td><td><input type=text name=Desc[6] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['DescUK'])) ;
    printf ("<tr><td class=itemfield>UK Description</td><td><textarea name=LongDesc[6] style='width:100%%;height:150px;'>%s</textarea></td></tr>\n",htmlentities($Record['LongDescUK'])) ;
    print htmlItemSpace() ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
