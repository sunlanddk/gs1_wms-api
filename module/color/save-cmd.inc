<?php

    require_once 'lib/save.inc' ;
    require_once 'module/color/include.inc' ;

    $AutoFill = false ;

    $fields = array (
	'Number'		=> array ('mandatory' => true,		'check' => true),
	'Description'		=> array (),
	'AltDescription'		=> array (),
	'ColorGroupId'		=> array ('mandatory' => true, 		'type' => 'integer') 
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }
     
    function checkfield ($fieldname, $value) {
	global $Id, $fields, $AutoFill ;
	switch ($fieldname) {
	    case 'Number':
		$AutoFill = colorNumberFill ($value) ;
		if (is_string($AutoFill)) return $AutoFill ;

		// Check that Number does not allready exist
		$query = sprintf ('SELECT Id FROM Color WHERE Active=1 AND Number="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Color allready existing' ;

		// Save Color number
		$fields[$fieldname]['value'] = $value ;

		return true ;
	}
	return false ;
    }

    $res = saveFields ('Color', $Id, $fields, true) ;
    if ($res) return $res ;

	foreach ($_POST['Desc'] as $key => $value) {
		$_color_lang_id = tableGetFieldWhere('color_lang','Id','LanguageId='. $key .' and ColorId='. $Id) ;
		if ($_color_lang_id > 0) {
			$_query = sprintf('Update color_lang set Description="%s" where id=%d', $value, $_color_lang_id) ;
		} else {
			$_query = sprintf('Insert into color_lang set ColorId=%d, Description="%s", LanguageId=%d', $Id, $value, $key) ;
		}
		dbQuery($_query) ;
	}
	foreach ($_POST['LongDesc'] as $key => $value) {
		$_color_lang_id = tableGetFieldWhere('color_lang','Id','LanguageId='. $key .' and ColorId='. $Id) ;
		if ($_color_lang_id > 0) {
			$_query = sprintf('Update color_lang set LongDescription="%s" where id=%d', $value, $_color_lang_id) ;
		} else {
			$_query = sprintf('Insert into color_lang set ColorId=%d, LongDescription="%s", LanguageId=%d', $Id, $value, $key) ;
		}
		dbQuery($_query) ;
	}
	

	switch ($Navigation['Parameters']) {
	  case 'new' :
	    // View new article
	    if ($AutoFill) return navigationCommandMark ('coloredit', (int)$Record['Id']) ;
    }
    
    return 0 ;
?>
