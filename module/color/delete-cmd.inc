<?php

    require_once 'lib/table.inc' ;

    // Ensure Color not used in Articles
    $query = sprintf ("SELECT Id FROM ArticleColor WHERE ColorId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Color used in Articles" ;

    tableDelete ('Color', $Id) ;

    return 0
?>
