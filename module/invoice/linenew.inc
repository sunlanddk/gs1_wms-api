<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Description', $Record['Description']) ;     
    itemField ('Company', $Record['CompanyName']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Article', formText ('ArticleNumber', '', 10)) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
