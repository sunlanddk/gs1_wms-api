<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'InvoiceId'		=> array ('type' => 'integer',	'mandatory' => true),
	'CustomsPositionId'	=> array ('type' => 'integer',	'mandatory' => true),
	'CountryId'		=> array ('type' => 'integer',	'mandatory' => true),

	'Field35'		=> array ('type' => 'integer'),
	'Field37'		=> array (),
	'Field38'		=> array ('type' => 'integer'),
	'Field41'		=> array ('type' => 'decimal'),
	'Field44'		=> array (),
	'Field46'		=> array ('type' => 'decimal',	'format' => '12.2'),
    	'Field49'		=> array ()
    ) ;

    // Update field list
    $fields['Field41']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    // Preconditions
    if ($Record['InvoiceDone']) return 'the Customs Forms can not be modified when Invoice is Done' ;

    if ($Id > 0) {
	// Existing Form
	unset ($fields['InvoiceId']) ;
	unset ($fields['CustomsPositionId']) ;
	unset ($fields['CountryId']) ;
    } else {
	// New Form
	$Id = -1 ;
    }
     
    return saveFields ('CustomsFormA12', $Id, $fields) ;
?>
