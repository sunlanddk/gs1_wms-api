<?php


function sendInvoiceDataToWebshop($_invoiceId){
	$query = sprintf ('SELECT DISTINCT `order`.`Id`, `order`.`CompanyId`, `invoice`.`Credit`
					FROM invoice
					LEFT JOIN invoiceline on invoiceline.InvoiceId = invoice.Id
					LEFT JOIN orderline on invoiceline.OrderLineId = orderline.id
					LEFT JOIN `order` on orderline.OrderId = `order`.`Id`

					WHERE invoice.id = %d', $_invoiceId) ;
	$getOrderId = dbQuery ($query) ;
	$orderId = '';
	$companyId = '';
	$invoicedAmount = 0;
	$articles = array();
	$creditMemo = 0;
	while ($row = dbFetch($getOrderId)) {
		$orderId = $row['Id'];
		$companyId = $row['CompanyId'];
		$creditMemo = $row['Credit'];
	}

	if($creditMemo === 1){
		return;
	}

	// Set correct company id 
	if($companyId == 7779 || $companyId == '7779'){
		if($orderId == ''){
			return;
		}
		$query = sprintf ('SELECT InvoiceLine.*, 
					invoicequantity.ArticleSizeId, 
					invoicequantity.Quantity as sizeQty,
					OrderLine.Id AS OrderLineId, 
					OrderLine.No AS OrderLineNo, 
					OrderLine.OrderId, 
					Article.Number AS ArticleNumber, 
					Article.VariantColor,
					Article.VariantSize AS VariantSize,
					Color.Number AS ColorNumber,
					Unit.Name AS UnitName, 
					Unit.Decimals AS UnitDecimals,
					variantcode.VariantCode AS sku,
					Country.VATPercentage 

					FROM InvoiceLine
					LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
					LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
					LEFT JOIN Country ON Country.Id=Order.CountryId
					LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
					LEFT JOIN ArticleColor ON ArticleColor.Id=InvoiceLine.ArticleColorId
					LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
					LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
					LEFT JOIN invoicequantity ON invoicequantity.InvoiceLineId=invoiceline.id
					LEFT JOIN Unit ON Unit.Id=Article.UnitId
					LEFT JOIN variantcode ON variantcode.ArticleId = invoiceline.ArticleId AND (Article.variantcolor=0 or (Article.variantcolor=1 and variantcode.ArticleColorId=invoiceline.ArticleColorId)) AND (Article.variantsize=0 or (Article.variantsize=1 and variantcode.ArticleSizeId = invoicequantity.ArticleSizeId))

					WHERE InvoiceLine.InvoiceId=%d AND InvoiceLine.Active=1 ORDER BY InvoiceLine.No', $_invoiceId) ;

		$getInvoiceLines = dbQuery ($query);
		while ($row = dbFetch($getInvoiceLines)){
			$price = $row['PriceSale']*((100-$row['Discount'])/100)*((100+$row['VATPercentage'])/100);
			if($row['VariantSize'] > 0) {
				$artQty = $row['sizeQty'];
			} else {
				$artQty = $row['Quantity'];
			}
			$invoicedAmount += ($price  * $artQty);
			$variant = array("VariantCode" => $row['sku'], "Quantity" => $artQty, "Price" => number_format($price, 2, ',', '.'));
			array_push($articles, $variant);
		}

		$postBack = array(
	      "orderId"         =>  $orderId,
	      "orderState"      =>  'shipped',
	      "InvoicedAmount"  =>  number_format($invoicedAmount, 2, ',', '.'),
//	      "Articles"        =>  $articles,
	    );


		$potdata = json_encode(array('fromPot' => array('request' => 'order_update','data'    => $postBack)));
//print_r($potdata) ; die() ;
		$ch = curl_init(WEBSHOP_API_UPDATE_URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		//curl_setopt($ch, CURLOPT_TIMEOUT_MS, 6000);
		curl_setopt($ch, CURLOPT_TIMEOUT, 600);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
//print_r($result) ; die() ;
		$result = json_decode($result);
	}

}

?>