<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc';
    
    if ($Record['Ready']) return 'Delivery dates can not be set when Ready' ;	
	
    $query = sprintf('SELECT id from InvoiceLine where InvoiceId=%d and Active=1', $Record['Id']) ;
    $res = dbQuery ($query) ;

	// Set Orderlines done
	$InvoiceLine = array (	
		'Discount' => (int)$_POST['Discount']
    ) ;
    while ($row = dbFetch ($res)) {
		tableWrite ('InvoiceLine', $InvoiceLine, $row['id']) ;
	}		
	return 0 ;
?>
