<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/mail.inc';

	global $_ready ;



	$_ready = ($_POST['Ready']=='on') ? 1 : 0 ;

	
	function generatePdfFile() {
		 global $Id, $Config, $Record, $_ready;

		 $Config['savePdfToFile'] = true;
		 $Record['Ready'] = $_ready ;
		 require_once 'printinvoice.inc';
		 $Config['savePdfToFile'] = false;

		 return fileName('invoice', (int)$Id);
	}

	
if ($Record['Ready']==0 and $_ready) {
	// Any Invoice Lines
	$query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count == 0) return 'the Invoice can not be set Ready when it has no InvoiceLines' ;

	// set ready.
	// Get last used number
	if ($Record['InvoiceCountryId']>0)
		$CustomerCountryId = $Record['InvoiceCountryId'] ;
	else
		$CustomerCountryId = $Record['CompanyCountryId'] ;
		
	if ($Record['FromCompanyId'] == 0) { // Norway samlefaktura - not active
		switch ($CustomerCountryId) {
			case 15: //NO
			case 85: //NO+
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
			default:
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and not (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
		}
	} else {
		$query = sprintf('SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND FromCompanyID=%d', $Record['FromCompanyId']) ;
	}
	
//return 'test ' . $query . 'CountryId:' . $CustomerCountryId;	
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Set InvoiceDate
	if ($Record['InvoiceDate']>'2001-01-01') {
		$InvDate = $Record['InvoiceDate'] ;
	} else {
		$t = time() ;
		// Validate
		if ($t < time()-30*24*60*60 or $t > time()+30*24*60*60) return "Invoice Date must be with +/- one month from today" ;
		$InvDate = dbDateOnlyEncode($t) ;
	}
	// Set DueBaseDate
	if ($Record['DueBaseDate']>'2001-01-01') {
		$DueBaseDate = $Record['DueBaseDate'] ;
		$t = dbDateDecode($DueBaseDate) ;
	} else {
		$t = time() ;
		// Validate
		if ($t < dbDateDecode($InvDate)) return "DueBase can not be before Invoice date" ;
		if ($t > dbDateDecode($InvDate)+365*24*60*60) return "DueBase must be within one year from Invoice date" ;

		$DueBaseDate = dbDateOnlyEncode($t) ;
	}
	// Set DueDate
	if (!$Record['Credit']) {
		    // Get PaymentTerms and 
		    $query = sprintf ('SELECT * FROM PaymentTerm WHERE Id=%d', (int)$Record['PaymentTermId']) ;
		    $result = dbQuery ($query) ;
		    $p_row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    
		    $t = getdate ($t) ;
		    if ($p_row['RunningMonth']) {
				$t['mday'] = 1 ;
				$t['mon'] += 1 ;
		    }
		    $t['mon'] += (int)$p_row['PayMonths'] ;
		    $t['year'] += ($t['mon'] - 1) / 12 ;		
		    $t['mon'] = ($t['mon'] - 1) % 12 + 1 ;
		    $t = mktime (0, 0, 0, $t['mon'], $t['mday'], $t['year']) ;
		    $t += 60*60*24*(int)$p_row['PayDays'] ;
		    $DueDate = dbDateOnlyEncode($t) ;
	} else {
		$DueDate = '0000-00-00' ;
	}

	$Invoice = array (	
			'Ready'			=>  1,
			'ReadyUserId'	=>  $User['Id'],
			'ReadyDate'		=>  dbDateEncode(time()),
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time()),
			'InvoiceDate'	=>  $InvDate,
			'DueBaseDate'	=>  $DueBaseDate,
			'DueDate'		=>  $DueDate,
			'Number'		=> (int)$row['Number'] + 1, 
			'CurrencyRate'		=>  tableGetField('Currency','Rate',(int)$Record['CurrencyId'])
	) ;
	tableWrite ('Invoice', $Invoice, $Id) ;

	// Query Invoice
	$_orderid = $Record['OrderId'] ;
	$_ordertypeid = $Record['OrderTypeId'] ;
	$query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	$res = dbQuery ($query) ;
	$Record = dbFetch ($res) ;
	dbQueryFree ($res) ;
	$Record['OrderId'] = $_orderid ;
	$Record['OrderTypeId'] = $_ordertypeid ;
}
	if (($_POST['CompletelyDone'] == NULL and !$Record['Credit']))
	{
		// set áll orders done
		$query = sprintf ('select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $row['orderid']) ;
			
			$OrderId = $row['orderid'] ;
			
			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $row['orderid'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

		}
		dbQueryFree ($result) ;
		
		// set all lines done that are not already done
		$query = sprintf ('select orderline.* from orderline join
										(select ol.orderid as OLorderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.OLorderid and orderline.active=1', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Orderline = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
			) ;
			tableWrite ('Orderline', $Orderline, $row['Id']) ;
		}
		
		//return 'test ' . $row['OrderId'] . ' M ' .  $row['orderid'] . ' M '. $OrderId . ' M '. $OrderTypeId . ' Test OT:' . $OrderTypeID . ' ShopNo ' . $ShopNo . ' WEBID ' . $WEBID . ' query ' . $query ;
		
		dbQueryFree ($result) ;
		//return 'Test OT:' . $OrderTypeID . ' ShopNo ' . $ShopNo . ' WEBID ' . $WEBID . ' query ' . $query ;
	}


	// Generate pdf and email
	$_pdffile = generatePdfFile() ; //fileName('invoice', (int)$Id); 
	$_returnpdf = $Config['photoLib'] . 'returnnote.pdf' ;

	//preparing pdf attachment for the mail
	if (($Record['OrderTypeId']==10) and !$Record['Credit']) {
	   $_attachmentArray = array(
			array(
				'path'     => $_pdffile,
				'mimeType' => 'application/pdf',
				'name'     => sprintf('Invoice #%s.pdf', $Record['Number'])
			),
			array(
				'path'     => $_returnpdf,
				'mimeType' => 'application/pdf',
				'name'     => sprintf('ReturnNote.pdf', $Record['Number'])
			)
		);
	} else {
	   $_attachmentArray = array(
			array(
				'path'     => $_pdffile,
				'mimeType' => 'application/pdf',
				'name'     => sprintf('Invoice #%s.pdf', $Record['Number'])
			)
		);
	}


   /////////////////
	if (!($Config['Instance'] == 'Test')) {
		require_once 'module/invoice/invoiceExportToWeb.php' ;
		sendInvoiceDataToWebshop($Id);
	}
   /////////////////

//Return 'hallo' ;
//	$_param['body'] = nl2br(htmlentities($_POST['BodyTxt'])) . '<br>' ;
	$_param['body'] = nl2br($_POST['BodyTxt']) . '<br>' ;
	if ($Record['OrderTypeId']==10) {
		$_mark="webinvoicecreated" ;
	} else {
		$_mark="b2binvoicecreated" ;
	}
	
	if ($Config['Instance'] == 'Test')
		sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false, $_POST['Mail'], false, $_attachmentArray, sprintf(' to %s Invoice #%s', $Record['CompanyName'], $Record['Number']));
	else
		sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false, $_POST['Mail'] . ', sales@wms.dk', false, $_attachmentArray, sprintf(' to %s Invoice #%s', $Record['CompanyName'], $Record['Number']));
	if (is_file($_pdffile) AND !$_POST['Ready']=='on') unlink ($_pdffile) ;
	
    return 0 ;	

    return 0 ;	
?>
