<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    $MyCompanyId = parameterGet ('CompanyMy') ;

    itemStart () ;
    itemSpace () ;
    if ($Navigation['Parameters'] != 'new') itemField ('Type', $Record['Type']) ;
    itemField ('Company', sprintf ('%s (%d)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemSpace () ;
    itemEnd () ;
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Company = $Record ;

	    // Default Values
	    unset ($Record) ;
	    $Record['SalesUserId'] = $Company['SalesUserId'] ;
	    $Record['FromCompanyId'] = $User['CompanyId'] ;
	    $Record['CurrencyId'] = $Company['CurrencyId'] ;
	    $Record['PaymentTermId'] = $Company['PaymentTermId'] ;
	    $Record['CompanyFooter'] = $Company['CompanyFooter'] ;
//	    $Record['DeliveryTermId'] = $Company['DeliveryTermId'] ;
//	    $Record['CarrierId'] = $Company['CarrierId'] ;
//	    if ((int)$Company['DeliveryCountryId'] > 0) {
//		$Record['Address1'] = $Company['DeliveryAddress1'] ;
//		$Record['Address2'] = $Company['DeliveryAddress2'] ;
//		$Record['ZIP'] = $Company['DeliveryZIP'] ;
//		$Record['City'] = $Company['DeliveryCity'] ;
//		$Record['CountryId'] = $Company['DeliveryCountryId'] ;
//	    } else {
//		$Record['Address1'] = $Company['Address1'] ;
//		$Record['Address2'] = $Company['Address2'] ;
//		$Record['ZIP'] = $Company['ZIP'] ;
//		$Record['City'] = $Company['City'] ;
//		$Record['CountryId'] = $Company['CountryId'] ;
//	    }	    
	    break ;
    }

    if (!$Record['Ready']) {
	if ($Record['InvoiceDate']<'2001-01-01')
		$Record['InvoiceDate'] = dbDateOnlyEncode(time()) ;
	if ($Record['DueBaseDate']<'2001-01-01')
		$Record['DueBaseDate'] = $Record['InvoiceDate'] ;
    }
     
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemFieldRaw ('Reference', formText ('Reference', $Record['Reference'], 40)) ;
    if ($Navigation['Parameters'] == 'new') {
	itemSpace () ;
	itemFieldRaw ('Credit', formCheckbox ('Credit')) ;
    } else {
	itemSpace () ;
	itemFieldRaw ('Date', formDate('InvoiceDate', dbDateDecode($Record['InvoiceDate']), null, 'tR')) ;
    }
    itemSpace () ;
    itemFieldRaw ('Sales Ref', formDBSelect ('SalesUserId', (int)$Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Owner Company', formDBSelect ('FromCompanyId', (int)$Record['FromCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Currency', formDBSelect ('CurrencyId', (int)$Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:200px;')) ;  
    itemFieldRaw ('Payment Term', formDBSelect ('PaymentTermId', (int)$Record['PaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value', 'width:250px;')) ;  
    if ($Navigation['Parameters'] != 'new') {
		itemSpace () ;
		itemFieldRaw ('Due Base', formDate('DueBaseDate', dbDateDecode($Record['DueBaseDate']), null, 'tR')) ;
    }
    itemSpace () ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Discount', formText ('Discount', 0, 3, 'text-align:right;') . ' %') ;
	
//  itemFieldRaw ('Delivery Terms', formDBSelect ('DeliveryTermId', (int)$Record['DeliveryTermId'], 'SELECT Id, CONCAT(Name," (",Description,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Name', 'width:200px;')) ;  
//  itemFieldRaw ('Carrier', formDBSelect ('CarrierId', (int)$Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Name', 'width:150px;')) ;  
//  itemSpace () ;
//  itemFieldRaw ('Street', formText ('Address1', $Record['Address1'], 50)) ;
//  itemFieldRaw ('', formText ('Address2', $Record['Address2'], 50)) ;
//  itemFieldRaw ('ZIP', formText ('ZIP', $Record['ZIP'], 20)) ;
//  itemFieldRaw ('City', formText ('City', $Record['City'], 50)) ;
//  itemFieldRaw ('Country',  formDBSelect ('CountryId', (int)$Record['CountryId'], 'SELECT Id, CONCAT(Name," - ",Description) AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px;')) ;
    itemSpace () ;
    itemFieldRaw ('InvoiceHeader', formTextArea ('InvoiceHeader', $Record['InvoiceHeader'], 'width:100%;height:100px;')) ;
    itemFieldRaw ('InvoiceFooter', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
    itemFieldRaw ('CompanyFooter', formtextArea ('CompanyFooter', $Record['CompanyFooter'], 'width:100%;height:100px;')) ;
    if ($Navigation['Parameters'] != 'new') {
	itemSpace () ;
//	flag ($Record, 'Ready') ;
//	flag ($Record, 'Done') ; 
	itemInfo ($Record) ;
    }
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
