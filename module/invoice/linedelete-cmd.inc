<?php

    require_once 'lib/table.inc' ;

    // Invoice Ready ?
    if ($Record['InvoiceDone']) return 'the Invoice is Done' ;
    if ($Record['InvoiceReady']) return 'the Invoice is Ready' ;
 
    // Do delete  
    tableDelete ('InvoiceLine', $Id) ;
    tableDeleteWhere ('InvoiceQuantity', sprintf ('InvoiceLineId=%d', $Id)) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM InvoiceLine WHERE InvoiceId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['InvoiceId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['No']) {
	    $query = sprintf ("UPDATE InvoiceLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
