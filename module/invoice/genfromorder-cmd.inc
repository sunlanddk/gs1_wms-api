<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    
    // Validate Shipment
    if (!$Record['Ready']) return 'Order is not Ready' ;

//    if ($Record['PreInvoiceId'] >  0) return 'Invoice already pre generated for this order' ;

    // Check if orderlines already invoices
/*
    $query = sprintf ('SELECT Id FROM Invoice WHERE StockId=%d AND Active=1', (int)$Record['Id']) ;
    $res = dbQuery ($query) ;
    $count = dbNumRows ($res) ;
    dbQueryFree ($res) ;
    if ($count > 0) return 'Invoice(s) has allready been generated for this Shipment' ;
*/
    
    // Get Company information
    $query = sprintf ('SELECT * FROM Company WHERE Id=%d', (int)$Record['CompanyId']) ;
    $res = dbQuery ($query) ;
    $Company = dbFetch ($res) ;
    dbQueryFree ($res) ;
//	logPrintVar ($Company, 'Company') ;
   
    // Valiables
    $Part = 0 ;
    $OrderId = 0 ;
    $Invoice = array () ;
    $InvoiceLine = array () ;
    $InvoiceQuantity = array () ;
    $Quantity = (float)0 ;   

    $query = sprintf ( 'SELECT
						`Order`.Id AS OrderId, `Order`.CurrencyId, `Order`.PaymentTermId, `Order`.SalesUserId,
						OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
						Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
						Orderline.ArticleColorId as ArticleColorId, OrderQuantity.ArticleSizeId as ArticleSizeId, 0 as ArticleCertificateId, if(Article.VariantSize,orderquantity.Quantity,OrderLine.Quantity) AS Quantity
						FROM `Order`
						JOIN Orderline On `Order`.Id=OrderLine.OrderId and OrderLine.Active=1
						LEFT JOIN InvoiceLine On `InvoiceLine`.OrderLineId=OrderLine.Id and InvoiceLine.Active=1 
						LEFT JOIN orderquantity ON orderquantity.OrderlineId=Orderline.Id and orderquantity.Active=1
						JOIN Article ON Article.Id=Orderline.ArticleId
						WHERE order.id=%d and orderline.active=1 and invoiceline.id is null
						ORDER BY OrderId, OrderLineNo, ArticleNumber', 
						(int)$Record['Id']) ;
 //return 'test ' . $query ;
// add orderline.done=0  or maybe better invoiceid to orderline?
    $res = dbQuery ($query) ;
    $count = dbNumRows ($res) ;
    if ($count == 0) return 'Invoice(s) has allready been generated for all orderlines' ;
    while ($row = dbFetch ($res)) {
		// Check if Order associated the Item
		if ((int)$row['OrderId'] == 0) {
			// No Order
			// Fill in default information from Company
			$row['CurrencyId'] = $Company['CurrencyId'] ;
			$row['PaymentTermId'] = $Company['PaymentTermId'] ;
			$row['SalesUserId'] = $Company['SalesUserId'] ;
			$row['CompanyFooter'] = $Company['CompanyFooter'] ;
			$row['Description'] = $row['ArticleDescription'] ;
		}

//		logPrintVar ($row, 'row') ;

		// New Invoice ?
		if ((int)$Invoice['Id'] == 0 or
			($Company['InvoiceSplit'] and ((int)$row['OrderId'] != $OrderId)) or
			$Invoice['CurrencyId'] != (int)$row['CurrencyId'] or
			$Invoice['PaymentTermId'] != (int)$row['PaymentTermId'] or
			$Invoice['SalesUserId'] != (int)$row['SalesUserId']				) {
			
			// Generate the Invoice
			$Invoice = array (
				'Active'			=> 1,
				'CompanyId'			=> (int)$Company['Id'],
				'FromCompanyId'		=> (int)$Record['ToCompanyId'],
				'StockId'			=> 0, 
				'Credit'			=> 0,
				'Description'		=> sprintf ('Generated from Order %d, part %d', (int)$Record['Id'], ++$Part),
				'CurrencyId'		=> (int)$row['CurrencyId'],
				'PaymentTermId'		=> (int)$row['PaymentTermId'],
				'SalesUserId'		=> (int)$row['SalesUserId'],
				'CompanyFooter'		=> sprintf ("%s", $Company['CompanyFooter']),
			) ;

			$Invoice['Id'] = tableWrite ('Invoice', $Invoice) ;
//			$Invoice['Id'] = 9999 ;
//			logPrintVar ($Invoice, 'Invoice') ;
			
			$No = 0 ;
			$OrderId = (int)$row['OrderId'] ;
		}

		// New Invoice Line ?
		if ($InvoiceLine['Id'] == 0 or
			$InvoiceLine['InvoiceId'] != $Invoice['Id'] or
			$InvoiceLine['OrderLineId'] != (int)$row['OrderLineId'] or
			$InvoiceLine['ArticleId'] != (int)$row['ArticleId'] or
			!$row['VariantSize'] or
			($row['VariantColor'] and ($InvoiceLine['ArticleColorId'] != (int)$row['ArticleColorId'])) or
			($row['VariantCertificate'] and ($InvoiceLine['ArticleCertificateId'] != (int)$row['ArticleCertificateId']))  ) {

			// Update Invoice with correct Quantity
			if ($InvoiceLine['Id'] > 0 and $Quantity > $InvoiceLine['Quantity']) {
				$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $Quantity, $InvoiceLine['Id']) ;
				dbQuery ($query) ;
//				printf ("%s<br>\n", $query) ;
			}
			
			// Generate the InvoiceLine
			$InvoiceLine = array (
				'Active'		=> 1,
				'InvoiceId'		=> $Invoice['Id'],
				'OrderLineId'	=> (int)$row['OrderLineId'],
				'No'			=> ++$No,
				'Description'	=> $row['Description'],
				'InvoiceFooter'	=> $row['InvoiceFooter'],
				'CustArtRef'	=> $row['CustArtRef'],
				'ArticleId'		=> (int)$row['ArticleId'],
				'Quantity'		=> $row['Quantity'],
				'Discount'		=> (int)$row['Discount'],
				'PriceSale'		=> (float)$row['PriceSale']
			) ;
			if ($row['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$row['ArticleColorId'] ;
			if ($row['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$row['ArticleCertificateId'] ;

			$InvoiceLine['Id'] = tableWrite ('InvoiceLine', $InvoiceLine) ;
//			$InvoiceLine['Id'] = 9999 ;
//			logPrintVar ($InvoiceLine, 'InvoiceLine') ;

			$Quantity = (float)0 ;   
		}

		// Size Variants
		if ($row['VariantSize']) {
			$InvoiceQuantity = array (
			'Active'		=> 1,
			'InvoiceLineId'		=> $InvoiceLine['Id'],
			'ArticleSizeId'		=> (int)$row['ArticleSizeId'],
			'Quantity'		=> $row['Quantity']
			) ;
			
			tableWrite ('InvoiceQuantity', $InvoiceQuantity) ;
//			logPrintVar ($InvoiceQuantity, 'InvoiceQuantity') ;

			$Quantity += (float)$row['Quantity'] ;
		}
		
    }
    dbQueryFree ($res) ;

    // Update Invoice with correct Quantity
    if ($InvoiceLine['Id'] > 0 and $Quantity > (float)$InvoiceLine['Quantity']) {
		$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $Quantity, $InvoiceLine['Id']) ;
		dbQuery ($query) ;
		//printf ("%s<br>\n", $query) ;
    }
	$_order = array (
	    'PreInvoiceId' => (int)$Invoice['Id']
    ) ;
	tableWrite ('Order', $_order, $Record['Id']) ;

		
   //Update Order done?
 /*  
	if ($Record['Departed']) {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time()),
	    'Done' => (int)1,
	    'DoneUserId' => $User['Id'],
	    'DoneDate' => dbDateEncode(time())
	    ) ;
	} else {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time())
	    ) ;
	}
	tableWrite ('Stock', $Stock, $Record['Id']) ;
*/
    switch ($Part) {
		case 0 :
			return 'no Invoices generated' ;

		case 1 :
			return navigationCommandMark ('invoiceview', (int)$Invoice['Id']) ;

		default :
			return navigationCommandMark ('companyinvoices', (int)$Company['Id']) ;
    }
?>
