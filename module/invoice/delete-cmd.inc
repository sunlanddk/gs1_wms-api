<?php

    require_once 'lib/table.inc' ;

    // Ready ?
    if ($Record['Ready']) return 'the Invoice is Ready' ;

    // Loop through InvoiceLines
    $query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	tableDeleteWhere ('InvoiceQuantity', sprintf ('InvoiceLineId=%d', (int)$row['Id'])) ;
	tableDelete ('InvoiceLine', (int)$row['Id']) ;
    }
    
    // Do delete  
    tableDelete ('Invoice', $Id) ;
	
	$query = 'Update `order` set PreInvoiceId=0 where PreInvoiceId=' . $Id ;
	dbQuery ($query) ;
    return 0
?>
