<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;   
/*    
    itemStart() ;
    itemSpace () ;
    itemFieldRaw ('Credit Complete Invoice', formCheckbox ('CompleteCredit', 0)) ;
    itemSpace () ;
    itemEnd() ;
*/
    // List
    listClear () ;
    listStart () ;
    listRow () ;
 
    listHead ('Pos', 10) ;
    listHead ('Article', 35)  ;
    listHead ('Description', 50)  ;
    listHead ('Color', 35) ;
    listHead ('Description', 50)  ;
    listHead ('Size', 25) ;
    listHead ('Credit Qty', 35) ;
    listHead ('invoice Qty', 35) ;
    listHead ('Unit', 30) ;

    // Loop all the Invoice Lines
    foreach ($Line as $line) {
   	listRow () ;
    	listField ($line['No']) ;
    	listField ($line['ArticleNumber']) ;
    	listField ($line['ArticleDescription']) ;
    	listField ($line['VariantColor']?$line['ColorNumber']:'') ;
    	listField ($line['VariantColor']?$line['ColorDescription']:'') ;

    	if ($line['VariantSize']) {
        $first = 1 ;
  	    foreach ($line['Size'] as $size) {
         if ($first) {
            $first = 0 ;
            listField ('') ;
            listField ('') ;
            ListFieldRaw (formHidden(sprintf('LineQuantity[%d]', (int)$line['Id']), $line['Quantity'], 8,'','')) ;
            listField (sprintf('%s',$line['UnitName'])) ;
          }
         	listRow () ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ($size['Name']) ;
          if ((float)$size['Quantity'] > 0)
      	    // listFieldRaw (formText (sprintf('SizeQuantity[%d]', (int)$size['Id']), number_format ((float)$size['Quantity'], (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:70px;margin-right:0;'), 'align=right') ;
      	    listFieldRaw (formText (sprintf('SizeQuantity[%d]', (int)$size['Id']), number_format (5, (int)$line['UnitDecimals'], ',', '.'), 7, 'text-align:right;width:70px;margin-right:0;'), 'align=right') ;
          else
            listField ('') ;
          listField (number_format((float)$size['Quantity'], (int)$size['UnitDecimals'], ',', '.'), 'align=left') ;
          if ($first) {
            $first = 0 ;
            listField (sprintf('%s',$line['UnitName'])) ;
          } else {
            listField ('') ;
          }
        }
      } else {
        listField ('---') ;
//   	    listFieldRaw (formText (sprintf('LineQuantity[%d]', (int)$line['Id']), number_format ((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:60px;margin-right:0;'), 'align=right') ;
   	    listFieldRaw (formText (sprintf('LineQuantity[%d]', (int)$line['Id']), number_format (0, (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:60px;margin-right:0;'), 'align=right') ;
        listField (number_format((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', '.'), 'align=left') ;
        listField (sprintf('%s',$line['UnitName'])) ;
      }
    }
    if (0) {
	    listRow () ;
	    listField () ;
	    listField ('No Invoicelines', 'colspan=4') ;
    }
    listEnd () ;
    formEnd () ;
    return 0 ;
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
