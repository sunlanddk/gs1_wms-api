<?php
	global $Record, $Company, $CompanyMy, $Now, $ProformaInvoice, $ProformaCustoms, $Lang  ;

    $Lang = 'en' ; // default language 

	switch ((int)$Record['FromCompanyId']) {
		case 1169:	// NT Japan
		case 787:	// GC
		case 1430:  // Drappa Dot
		case 2:		// Novotex
			switch ((int)$Record['CompanyId']) {
				case 4665:		// Green Cotton Living Webshop 4665
				    $Lang = 'dk' ;
					require_once 'printinvoiceWEB.inc' ;
					break ;
				default:
					require_once 'printinvoicePL.inc' ;
					break;
			}
			break;
		case 3592:		// Lvivtex
				    $Lang = 'ua' ;
		default:
		    require_once 'printinvoicePL.inc' ;
		    break;
    }
   
    return 0 ;
?>
