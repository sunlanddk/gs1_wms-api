<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    function flag (&$Record, $field, $name='') {
		if ($name=='') $name=$field ;
		if ($Record[$field]) {
			itemField ($name, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($name, formCheckbox ($field, $Record[$field])) ;
		}
    }
	
	// in cmd: nl2br($_POST['Body']) . '<br>'
	//'Please confirm the attached order confirmation from FUB to your sales agent.
	$BodyTxt =  
'Once again thank you for your order.

Attached your invoice for the order from FUB.

Best regards,

FUB ApS
Postvej 101, Foldby
DK-8382 Hinnerup
P: +45 22 93 18 90
www.fub.dk'; 

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Invoice', (int)$Record['Number']) ;
    itemField ('Customer', $Record['CompanyName']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;
	
	if ($Record['Ready'])
			itemField ('Confirmed', sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record['ReadyDate'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ReadyUserId']))) ;
	itemFieldRaw ($Record['Ready']?'':'Confirm', formCheckbox ('Ready', 1, '', $Record['Ready']?'Hidden':'')) ;
    itemSpace () ;
    itemFieldRaw ('Email', formText ('Mail', $Record['Mail'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('BodyTxt', formtextArea ('BodyTxt', $BodyTxt, 'width:100%;height:200px;')) ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;
   
    return 0 ;
?>
