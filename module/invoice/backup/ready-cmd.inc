<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

	
	// Any Invoice Lines
	$query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count == 0) return 'the Invoice can not be set Ready when it has no InvoiceLines' ;

	// set ready.
	// Get last used number
	if (($Record['FromCompanyId'] == 2) or ($Record['FromCompanyId'] == 787) or ($Record['FromCompanyId'] == 1430)) {
		$query = 'SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND (FromCompanyID=2 or FromCompanyID=787 or FromCompanyID=1430)' ;
	} else {
		$query = sprintf('SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND FromCompanyID=%d', $Record['FromCompanyId']) ;
	}
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Set InvoiceDate
	if ($Record['InvoiceDate']>'2001-01-01') {
		$InvDate = $Record['InvoiceDate'] ;
	} else {
		$t = time() ;
		// Validate
		if ($t < time()-30*24*60*60 or $t > time()+30*24*60*60) return "Invoice Date must be with +/- one month from today" ;
		$InvDate = dbDateOnlyEncode($t) ;
	}
	// Set DueBaseDate
	if ($Record['DueBaseDate']>'2001-01-01') {
		$DueBaseDate = $Record['DueBaseDate'] ;
		$t = dbDateDecode($DueBaseDate) ;
	} else {
		$t = time() ;
		// Validate
		if ($t < dbDateDecode($InvDate)) return "DueBase can not be before Invoice date" ;
		if ($t > dbDateDecode($InvDate)+365*24*60*60) return "DueBase must be within one year from Invoice date" ;

		$DueBaseDate = dbDateOnlyEncode($t) ;
	}
	// Set DueDate
	if (!$Record['Credit']) {
		    // Get PaymentTerms and 
		    $query = sprintf ('SELECT * FROM PaymentTerm WHERE Id=%d', (int)$Record['PaymentTermId']) ;
		    $result = dbQuery ($query) ;
		    $p_row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    
		    $t = getdate ($t) ;
		    if ($p_row['RunningMonth']) {
				$t['mday'] = 1 ;
				$t['mon'] += 1 ;
		    }
		    $t['mon'] += (int)$p_row['PayMonths'] ;
		    $t['year'] += ($t['mon'] - 1) / 12 ;		
		    $t['mon'] = ($t['mon'] - 1) % 12 + 1 ;
		    $t = mktime (0, 0, 0, $t['mon'], $t['mday'], $t['year']) ;
		    $t += 60*60*24*(int)$p_row['PayDays'] ;
		    $DueDate = dbDateOnlyEncode($t) ;
	} else $DueDate = '0000-00-00' ;
	$Invoice = array (	
			'Ready'			=>  1,
			'ReadyUserId'	=>  $User['Id'],
			'ReadyDate'		=>  dbDateEncode(time()),
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time()),
			'InvoiceDate'	=>  $InvDate,
			'DueBaseDate'	=>  $DueBaseDate,
			'DueDate'		=>  $DueDate,
			'Number'		=> (int)$row['Number'] + 1 
	) ;
	tableWrite ('Invoice', $Invoice, $Id) ;
	
//	if (!is_array($_POST['OrderLineId'])) return 'please select Orderline(s) to be doned' ;
    // Set Orderlines to done!
	if ($_POST['CompletelyDone'] == 'on')
	{
		// set �ll orders done
		$query = sprintf ('select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1
										 group by ol.orderid', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $row['orderid']) ;
		}
		dbQueryFree ($query) ;
		
		// set all lines done that are not already done
		$query = sprintf ('select * from orderline join
										(select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.orderid and orderline.active=1', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Orderline = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
			) ;
			tableWrite ('Orderline', $Orderline, $row['Id']) ;
		}
		dbQueryFree ($query) ;
	} else {
	foreach ($_POST['OrderLineId'] as $ol_id => $flag) {
		if ($ol_id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;
		$Orderline = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
		) ;
		tableWrite ('Orderline', $Orderline, $ol_id) ;

		// Set Orderdone?
		// last line? set order done
		$OrderId=tableGetField('OrderLine', 'OrderId', $ol_id) ;
		$query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0', $OrderId) ;
		$res = dbQuery ($query) ;
		if (!($row = dbFetch ($res))) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $OrderId) ;
		}
	}
	}
	
    return 0 ;
?>
	
