<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/mail.inc';

	global $_ready ;

	$_ready = ($_POST['Ready']=='on') ? 1 : 0 ;
 // return 'test ' .  $_POST['Ready'] . ' ' . $_ready; 

	function generatePdfFile() {
		 global $Id, $Config, $Record, $_ready;

		 $Config['savePdfToFile'] = true;
		 $Record['Ready'] = $_ready ;
		 require_once 'printinvoice.inc';
		 $Config['savePdfToFile'] = false;

		 return fileName('invoice', (int)$Id);
	}

		
	// Any Invoice Lines
	$query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count == 0) return 'the Invoice can not be set Ready when it has no InvoiceLines' ;

	// set ready.
	// Get last used number
	if ($Record['InvoiceCountryId']>0)
		$CustomerCountryId = $Record['InvoiceCountryId'] ;
	else
		$CustomerCountryId = $Record['CompanyCountryId'] ;
		
	if ($Record['FromCompanyId'] == 0) { // Norway samlefaktura - not active
		switch ($CustomerCountryId) {
			case 15: //NO
			case 85: //NO+
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
			default:
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and not (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
		}
	} else {
		$query = sprintf('SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND FromCompanyID=%d', $Record['FromCompanyId']) ;
	}
	
//return 'test ' . $query . 'CountryId:' . $CustomerCountryId;	
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Set InvoiceDate
	if ($Record['InvoiceDate']>'2001-01-01') {
		$InvDate = $Record['InvoiceDate'] ;
	} else {
		$t = time() ;
		// Validate
		if ($t < time()-30*24*60*60 or $t > time()+30*24*60*60) return "Invoice Date must be with +/- one month from today" ;
		$InvDate = dbDateOnlyEncode($t) ;
	}
	// Set DueBaseDate
	if ($Record['DueBaseDate']>'2001-01-01') {
		$DueBaseDate = $Record['DueBaseDate'] ;
		$t = dbDateDecode($DueBaseDate) ;
	} else {
		$t = time() ;
		// Validate
		if ($t < dbDateDecode($InvDate)) return "DueBase can not be before Invoice date" ;
		if ($t > dbDateDecode($InvDate)+365*24*60*60) return "DueBase must be within one year from Invoice date" ;

		$DueBaseDate = dbDateOnlyEncode($t) ;
	}
	// Set DueDate
	if (!$Record['Credit']) {
		    // Get PaymentTerms and 
		    $query = sprintf ('SELECT * FROM PaymentTerm WHERE Id=%d', (int)$Record['PaymentTermId']) ;
		    $result = dbQuery ($query) ;
		    $p_row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    
		    $t = getdate ($t) ;
		    if ($p_row['RunningMonth']) {
				$t['mday'] = 1 ;
				$t['mon'] += 1 ;
		    }
		    $t['mon'] += (int)$p_row['PayMonths'] ;
		    $t['year'] += ($t['mon'] - 1) / 12 ;		
		    $t['mon'] = ($t['mon'] - 1) % 12 + 1 ;
		    $t = mktime (0, 0, 0, $t['mon'], $t['mday'], $t['year']) ;
		    $t += 60*60*24*(int)$p_row['PayDays'] ;
		    $DueDate = dbDateOnlyEncode($t) ;
	} else {
		$DueDate = '0000-00-00' ;
	}

	$Invoice = array (	
			'Ready'			=>  1,
			'ReadyUserId'	=>  $User['Id'],
			'ReadyDate'		=>  dbDateEncode(time()),
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time()),
			'InvoiceDate'	=>  $InvDate,
			'DueBaseDate'	=>  $DueBaseDate,
			'DueDate'		=>  $DueDate,
			'Number'		=> (int)$row['Number'] + 1, 
			'CurrencyRate'		=>  tableGetField('Currency','Rate',(int)$Record['CurrencyId'])
	) ;
	tableWrite ('Invoice', $Invoice, $Id) ;
	// Query Invoice
	$query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	$res = dbQuery ($query) ;
	$Record = dbFetch ($res) ;
	dbQueryFree ($res) ;

	// Generate and email
	$_pdffile = generatePdfFile() ; //fileName('invoice', (int)$Id); 

	//preparing pdf attachment for the mail
   $_attachmentArray = array(
		array(
			'path'     => $_pdffile,
			'mimeType' => 'application/pdf',
			'name'     => sprintf('Invoice #%s.pdf', $Record['Number'])
		)
	);

//Return 'hallo' ;
	$_param['body'] = nl2br(htmlentities($_POST['BodyTxt'])) . '<br>' ;
	sendmail("invoicecreated", $User['Id'], NULL, $_param, NULL, false, $_POST['Mail'] . ', lb@fub.dk', false, $_attachmentArray, sprintf(' to %s Invoice #%s', $Record['CompanyName'], $Record['Number']));
	if (is_file($_pdffile) AND !$_POST['Ready']=='on') unlink ($_pdffile) ;
	
    return 0 ;	

    return 0 ;	
?>
