<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Number', ($Record['InvoiceReady']) ? (int)$Record['InvoiceNumber'] : 'Draft') ;
    itemField ('Description', $Record['InvoiceDescription']) ;     
    itemField ('Company', sprintf ('%s (%s)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemSpace () ;
    itemField ('Position', sprintf ('%s (%s)', $Record['CustomsPositionName'], $Record['CustomsPositionDescription'])) ;     
    itemField ('Country', sprintf ('%s (%s)', $Record['CountryDescription'], $Record['CountryName'])) ;
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    print formHidden ('InvoiceId', (int)$Record['InvoiceId']) ;
    print formHidden ('CustomsPositionId', (int)$Record['CustomsPositionId']) ;
    print formHidden ('CountryId', (int)$Record['CountryId']) ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Procedure', formText ('Field37', $Record['Field37'], 8)) ;
    itemFieldRaw ('Supplement', formTextArea ('Field44', $Record['Field44'], 'width:100%;height:100px;')) ;
    itemFieldRaw ('Stock', formText ('Field49', $Record['Field49'], 20)) ;
    itemSpace () ;
    itemFieldRaw ('Gross Weight', formText ('Field35', (int)$Record['Field35'], 6, 'text-align:right;'). ' Kg') ;
    itemFieldRaw ('Net Weight', formText ('Field38', (int)$Record['Field35'], 6, 'text-align:right;'). ' Kg') ;
    itemFieldRaw ('Quantity', formText ('Field41', number_format((float)$Record['Field41'], (int)$Record['UnitDecimals'], ',', '.'), 9, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ;
    itemFieldRaw ('Value', formText ('Field46', $Record['Field46'], 12, 'text-align:right;') . ' Kr') ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
