<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    // Validate
    if ($Record['Done']) return 'the InvoiceLine can not be added when Invoice is Done' ;
    if ($Record['Ready']) return 'the InvoiceLine can not be added when Invoice is Ready' ;
 
    // Find next position
    $query = sprintf ('SELECT MAX(No) AS No FROM InvoiceLine WHERE InvoiceId=%d AND Active=1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $No = (int)$row['No'] ;

    $fields = array (
		'ArticleNumber'		=> array (				'check' => true),
		'ArticleId'		=> array ('type' => 'set'),
		'Description'		=> array ('type' => 'set'),
		'InvoiceId'		=> array ('type' => 'set',		'value' => (int)$Record['Id']),
		'No'			=> array ('type' => 'set',		'value' => $No + 1)
    ) ;
 
   function checkfield ($fieldname, $value, $changed) {
	global $Id, $fields, $Record, $ArticleTypeFields ;
	switch ($fieldname) {
	    case 'ArticleNumber':
		// Validate
		if ($value == '') return 'please enter Article' ;
		
		// Get Article
		$query = sprintf ('SELECT Id, Description FROM Article WHERE Number="%s" AND Active=1', addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] == 0) return 'Article not found' ;

		// Save Article number
		$fields['ArticleId']['value'] = (int)$row['Id'] ;
		$fields['Description']['value'] = $row['Description'] ;

		// ArticleNumber are not to be used
		return false ;
	}
	return false ;
    } 

    // Read fields
    $res = saveFields ('InvoiceLine', -1, $fields) ;
    if ($res) return $res ;

    return navigationCommandMark ('invoicelineedit', dbInsertedId ()) ;

?>
