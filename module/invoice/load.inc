<?php

    require_once 'lib/navigation.inc' ;

    $StateField = 'IF(Invoice.Done,"Done",IF(NOT Invoice.Ready,"Defined","Ready"))' ;
    $TypeField = 'IF(Invoice.Credit,"Credit","Invoice")' ;

    $companylimit = sprintf(' And stock.FromCompanyId = %d', $User['CompanyId']);

    $queryFields = 'Invoice.*,
		    Currency.Symbol AS CurrencySymbol, Currency.Rate AS CurrRate,
		    Stock.Id AS StockId2, Stock.Name AS StockName2, Stock.Description AS StockDescription2, Stock.CarrierId, Stock.DeliveryTermId, Stock.Address1, Stock.Address2, Stock.ZIP, Stock.City, Stock.CountryId,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, FromCompany.Name AS FromCompanyName, if(Company.MailInvoice=\'\', Company.Mail, Company.MailInvoice) AS Mail,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName, Country.Description as CountryDescription,
		    Stock.Name AS StockName, Stock.Description AS StockDescription, ' .
		    $StateField . ' AS State,' .
		    $TypeField . ' AS Type' ;

    $queryTables = 'Invoice
		    LEFT JOIN Currency ON Currency.Id=Invoice.CurrencyId
		    LEFT JOIN Company ON Company.Id=Invoice.CompanyId
		    LEFT JOIN Company FromCompany On FromCompany.id =Invoice.FromCompanyId
		    LEFT JOIN User ON User.Id=Invoice.SalesUserId
		    LEFT JOIN Stock ON Stock.Id=Invoice.StockId
			LEFT JOIN Country ON Country.Id=Stock.CountryId' ;

    switch ($Navigation['Function']) {
	case 'list' :

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Invoice.Number',	'desc' => true,		'direct' => 'Invoice.Ready=1 AND Invoice.Active=1'),
		array ('name' => 'Date',		'db' => 'Invoice.ReadyDate'),
		array ('name' => 'Company',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")'),
		array ('name' => 'TypeC',		'db' => $TypeField),
		array ('name' => 'State',		'db' => $StateField),
		array ('name' => 'Description',		'db' => 'Invoice.Description'),
		array ('name' => 'Reference',		'db' => 'Invoice.Reference'),
		array ('name' => 'Shipment',		'db' => 'Stock.Name'),
		array ('name' => 'Sales person',	'db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")'),
		array ('name' => 'Owner',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")'),
		array ('name' => 'Done',		'db' => 'Invoice.DoneDate')
	    ) ;

	    switch ($Navigation['Parameters']) {
		case 'company' :
		case 'company-new' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Display
		    $HideCompany = true ;

		    // Navigation
		    if ($Record['TypeCustomer']) {
			navigationEnable ('new') ;
		    }

		    // Clause
		    $queryClause = sprintf ('Invoice.CompanyId=%d AND Invoice.Active=1', $Id) ;
		    break ;

		case 'all' :
		    // Display
		    $HideDone = false ;

		    // Clause
			if ($User['Extern']) {
				$queryClause = sprintf('Invoice.CompanyId=%d AND Invoice.Active=1 and Invoice.Id<>41666', $User['CompanyId']) ;
			} else {
				if ($User['SalesRef']) {
					$queryClause = sprintf('Invoice.FromCompanyId=%d AND Invoice.Active=1 and Invoice.Id<>41666 AND Company.salesuserid=%d', $User['CompanyId'], $User['Id']) ;
				} else {
					$queryClause = sprintf('Invoice.FromCompanyId=%d AND Invoice.Active=1 and Invoice.Id<>41666', $User['CompanyId']) ;
				}
		    }
			break ;

		default :
		    // Display
		    $HideDone = true ;

		    // Clause
		    $queryClause = 'Invoice.Done=0 AND Invoice.Active=1' ;
		    break ;
	    }
	    if ($User['CompanyId']==1169)
	 	    $queryClause = $queryClause . $companylimit ;
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'invoiceview') ;
	    return $res ;

	case 'ready' :
	case 'ready-cmd' :
	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		$query = sprintf ('SELECT inv.Number as InvNo, il.No as InvLineNo, ol.No as OrderLineNo, ol.orderid As OrderNo, ol.id as OrderLineId,
								sum(if(iq.quantity>0,iq.quantity,il.quantity)) as InvQty,
								Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
								if ((select sum(oq.quantity) as Qty from orderquantity oq where oq.orderlineid=il.orderlineid and oq.active=1 group by oq.orderlineid) is null,
									ol.quantity,
									(select sum(oq.quantity) as Qty from orderquantity oq where oq.orderlineid=il.orderlineid and oq.active=1 group by oq.orderlineid)) as OQty,
								(select sum(prev_inv.Quantity) as Qty from invoiceline prev_inv, invoice gryf where prev_inv.orderlineid=ol.id and prev_inv.active=1 and prev_inv.invoiceid=gryf.id and gryf.ready=1) as PrevInvQty
							FROM (invoice inv, invoiceline il)
							LEFT JOIN invoicequantity iq on iq.invoicelineid=il.id and iq.active=1
							LEFT JOIN orderline ol ON ol.id=il.orderlineid and ol.active=1
							LEFT JOIN Article a on a.id=il.articleid
						    LEFT JOIN Unit ON Unit.Id=a.UnitId
							WHERE il.invoiceid=inv.id and inv.id=%d and il.active=1
							GROUP BY il.id', $Id) ;
		$Result = dbQuery ($query) ;
	    return 0 ;

	case 'basic' :
	case 'basic-cmd' :
	case 'upd-discount-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    return 0 ;
	    }

	    // Fall through

	case 'linenew' :
	case 'linenew-cmd' :
	case 'revert' :
	case 'revert-cmd' :
	case 'delete-cmd' :
	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;

	case 'view' :
	    global $Result ;				// For direct view only

	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Query InvoiceLines
	    $query = sprintf ('SELECT InvoiceLine.*,
	    OrderLine.Id AS OrderLineId, OrderLine.No AS OrderLineNo, OrderLine.OrderId, OrderLine.CustArtRef,
	    Article.Number AS ArticleNumber, Article.VariantColor,
	    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
	    FROM InvoiceLine
	    LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
	    LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
	    LEFT JOIN ArticleColor ON ArticleColor.Id=InvoiceLine.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId
	    WHERE InvoiceLine.InvoiceId=%d AND InvoiceLine.Active=1 ORDER BY InvoiceLine.No', $Id) ;
	    $Result = dbQuery ($query) ;

	    if (!$Record['Credit']) {
		navigationEnable ('credit') ;
		navigationEnable ('printcustoms') ;
	    }

	    // Navigation
	    if ($Record['Done']) {
		navigationPasive ('new') ;
	    } else {
		navigationPasive ('credit') ;
	    }

	    if ($Record['Ready']) {
		navigationPasive ('new') ;
		navigationPasive ('ready') ;
		navigationPasive ('edit') ;
	    } else {
		navigationPasive ('printcustoms') ;
	    }

	    return 0 ;

	case 'credit' :
	case 'credit-cmd' :
	case 'credit-part-cmd' :
		$_credit = 1 ;
	case 'confirm' :
	case 'confirm-cmd' :
	case 'printinvoice' :
		if ($_credit == 1)
			$_groupclause = ' GROUP BY InvoiceLine.No ' ;
		else
//			$_groupclause = ' GROUP BY InvoiceLine.ArticleId, InvoiceLine.ArticleColorId ' ;
			$_groupclause = ' GROUP BY Order.Id, GroupArticleId, InvoiceLine.ArticleColorId ' ;
//			$_groupclause = ' GROUP BY GroupArticleId, InvoiceLine.ArticleColorId ' ;
			
	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Variables
	    $SizeCount = 0 ;

	    // Get InvoiceLines
	    $Line = array () ;
	    $query = sprintf ('SELECT
if(Article.ArticleTypeId=6,InvoiceLine.Id+10000000,InvoiceLine.ArticleId) as GroupArticleId,
		    InvoiceLine.*,min(InvoiceLine.No) as No, 
			 group_concat(InvoiceLine.No) as NoGroup,
			 group_concat(InvoiceLine.Id) as IdGroup,
			 ConsolidatedOrder.PartDelivery as PartDelivery,
			 count(InvoiceLine.Id) as NoGroupLines,
			 sum(`InvoiceLine`.`Quantity`) as QuantityTotal,
			 sum(`InvoiceLine`.`PriceSale`*`InvoiceLine`.`Quantity`) as PriceSaleSubTotal,
			Order.ConsolidatedId as ConsolidatedId,
		    Order.Id AS OrderId, Order.Reference AS OrderReference, Order.SeasonId as SeasonId, Order.InvoiceHeader AS OrderInvoiceHeader, Order.InvoiceFooter AS OrderInvoiceFooter, Order.VariantCodes as VariantCodes,Order.Phone as OrderPhone,
			max(Order.CountryId) as AltCountryId, Order.OrderTypeId, Order.Email,
		    Case.Id AS CaseId, Case.CustomerReference AS OrderLineReference, Case.ArticleCertificateId,
		    Article.Id AS ArticleId, Article.Number AS ArticleNumber,
		    Article.Description AS ArticleDescription, Article.DeliveryComment as DeliveryComment,
		    Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		    Color.Description AS ColorDescription, Color.AltDescription AS ColorAltDescription,
		    Color.Number AS ColorNumber, Color.Id as ColorId,
		    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
		    CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
		    MaterialCountry.Description AS MaterialCountryDescription,
		    KnitCountry.Description AS KnitCountryDescription,
		    WorkCountry.Id AS WorkCountryId, WorkCountry.Description AS WorkCountryDescription, WorkCountry.Name AS WorkCountryName,
		    CustomsPreference.Id AS CustomsPreferenceId, COUNT(CustomsPreference.Id) AS CustomsPreferenceCount, CustomsPreference.MaterialDrop, CustomsPreference.KnitDrop, CustomsPreference.WorkDrop
		    FROM InvoiceLine
		    LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
		    LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
		    LEFT JOIN ConsolidatedOrder ON ConsolidatedOrder.Id=`Order`.ConsolidatedId
		    LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		    LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
		    LEFT JOIN ArticleColor ON ArticleColor.Id=InvoiceLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN CustomsPosition ON CustomsPosition.Id=Article.CustomsPositionId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    LEFT JOIN Country AS MaterialCountry ON MaterialCountry.Id=Article.MaterialCountryId
		    LEFT JOIN Country AS KnitCountry ON KnitCountry.Id=Article.KnitCountryId
		    LEFT JOIN Country AS WorkCountry ON WorkCountry.Id=Article.WorkCountryId
		    LEFT JOIN CustomsPreference ON CustomsPreference.MaterialCountryGroup=IF(Article.MaterialCountryId>0,MaterialCountry.CountryGroup,"none") AND CustomsPreference.KnitCountryGroup=KnitCountry.CountryGroup AND CustomsPreference.WorkCountryGroup=WorkCountry.CountryGroup AND CustomsPreference.Active=1
		    WHERE InvoiceLine.InvoiceId=%d AND InvoiceLine.Active=1
			 %s 
		    ORDER BY InvoiceLine.No', $Id, $_groupclause) ;
	    $result = dbQuery ($query) ;
		$l = 0;
	    while ($row = dbFetch ($result)) {
			$Line[(int)$row['Id']] = $row ;
			if ($row['OrderId']>0) {
				if(!($Record['OrderId']==$row['OrderId'])) $Record['NoOrders']++ ;
				$Record['PartDelivery'] = $row['PartDelivery'] ;
				$Record['ConsolidatedId'] = $row['ConsolidatedId'] ;
				$Record['OrderId'] = $row['OrderId'] ;
				$Record['OrderTypeId'] = $row['OrderTypeId'] ;
				$Record['Email'] = $row['Email'] ;
			}
			if ($row['AltCountryId']>0) {
				$Record['AltCountryId'] = $row['AltCountryId'] ;
			}
			$l++ ;
			$Line[(int)$row['Id']]['No'] = $l ;
	    }
	    dbQueryFree ($result) ;
		
	    // Get size specific quantities for each Line with VariantSize
	    foreach ($Line as $i => $l) {
			if (!$l['VariantSize']) continue ;

			$Line[$i]['Size'] = array () ;
			if ($_credit == 1)
				$query = sprintf ("SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity, vc.VariantCode as VariantCode
								FROM ArticleSize
								LEFT JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId in (%s) AND InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id
								LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
								WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name",
								 $l['IdGroup'], (int)$l['ArticleId'], (int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
			else
				 $query = sprintf ("SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity, vc.VariantCode as VariantCode, InvoiceLine.PriceSale, 
									InvoiceLine.No as OrderLineNo, CustomsPosition.Name AS CustomsPositionName
								FROM ArticleSize
								INNER JOIN InvoiceQuantity ON InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id
								INNER JOIN InvoiceLine On InvoiceLine.id in (%s) and InvoiceLine.Id=InvoiceQuantity.InvoiceLineId and InvoiceLine.articlecolorid=%d and InvoiceLine.active=1
								LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
								LEFT JOIN CustomsPosition ON CustomsPosition.Id=ArticleSize.CustomsPositionId
								WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name",
								 $l['IdGroup'], (int)$l['ArticleColorId'], (int)$l['ArticleId'], (int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
			$result = dbQuery ($query) ;
			$n = 0 ;
			while ($row = dbFetch ($result)) {
				$Line[$i]['Size'][(int)$row['Id']] = $row ;
				$n++ ;
			}
			dbQueryFree ($result) ;

			// Update max size count
			if ($n > $SizeCount) $SizeCount = $n ;
	    }

	    return 0 ;

	case 'lineview' :
	case 'lineedit' :
	case 'lineedit-cmd' :
	case 'linedelete-cmd' :
	    global $Color, $Size, $Quantity ;		// For direct view only

	    // Query Invoice
	    $query = sprintf ('SELECT InvoiceLine.*,
	    Invoice.Id AS InvoiceId, Invoice.Description AS InvoiceDescription, Invoice.Ready AS InvoiceReady, Invoice.Done AS InvoiceDone,
	    Company.Id AS CompanyId, Company.Number AS CompanyNumber, Company.Name AS CompanyName,
	    Currency.Symbol AS CurrencySymbol,
	    Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
	    OrderLine.Id AS OrderLineId, OrderLine.No AS OrderLineNo, OrderLine.OrderId
	    FROM InvoiceLine
	    LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId
	    LEFT JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId
	    LEFT JOIN Currency ON Currency.Id=Invoice.CurrencyId
	    LEFT JOIN Company ON Company.Id=Invoice.CompanyId
	    LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
	    WHERE InvoiceLine.Id=%d AND InvoiceLine.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    if ($Record['VariantSize']) {
		// Get sizes
		$query = sprintf ("SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity FROM ArticleSize LEFT JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId=%d AND InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
		$result = dbQuery ($query) ;
		$Size = array() ;
		while ($row = dbFetch ($result)) {
		    $Size[] = $row ;
		}
		dbQueryFree ($result) ;
		if (count($Size) == 0) return 'no Sizes specified for Article' ;
	    }

	    if ($Record['VariantColor']) {
		// Get Color information
		$query = sprintf ('SELECT ArticleColor.Id,
		Color.Description AS ColorDescription, Color.Number AS ColorNumber,
		ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue
		FROM ArticleColor
		LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		WHERE ArticleColor.Id=%d', (int)$Record['ArticleColorId']) ;
		$res = dbQuery ($query) ;
		$Color = dbFetch ($res) ;
		dbQueryFree ($res) ;
	    }

	    return 0 ;

	case 'generate-cmd' :
	    // Query Shipment
	    $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;

	case 'genfromorder-cmd' :
	    // Query order
	    $query = sprintf ('SELECT * FROM `Order` WHERE Id=%d AND Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;

	case 'a12list' :
	case 'a12print' :
	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Get InvoiceLines grouped-by CustomsPosition and WorkCountry
	    $query = sprintf ('SELECT
		    SUM(InvoiceLine.Quantity) AS Quantity, SUM(InvoiceLine.Quantity*InvoiceLine.PriceSale*(100-InvoiceLine.Discount)/100) AS Price,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
		    CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName, CustomsPosition.Description AS CustomsPositionDescription,
		    Country.Id AS CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription
		    FROM InvoiceLine
		    LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
		    LEFT JOIN CustomsPosition ON CustomsPosition.Id=Article.CustomsPositionId
		    LEFT JOIN Unit ON Unit.Id=CustomsPosition.UnitId
		    LEFT JOIN Country ON Country.Id=Article.WorkCountryId
		    WHERE InvoiceLine.InvoiceId=%d AND InvoiceLine.Active=1
		    GROUP BY CustomsPosition.Id, Country.Id
		    ORDER BY CustomsPosition.Name, Country.Name', $Id) ;
	    $result = dbQuery ($query) ;
	    $Form = array () ;
	    while ($row = dbFetch ($result)) {
		$Form[(int)$row['CustomsPositionId']][(int)$row['CountryId']] = $row ;
		$Form[(int)$row['CustomsPositionId']][(int)$row['CountryId']]['Form'] = array() ;
	    }
	    dbQueryFree ($result) ;

	    // Get Specific CustomsFormA12 information
	    $query = sprintf ('SELECT CustomsFormA12.* FROM CustomsFormA12 WHERE CustomsFormA12.InvoiceId=%d AND CustomsFormA12.Active=1', (int)$Record['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
		if (!isset($Form[(int)$row['CustomsPositionId']][(int)$row['CountryId']])) continue ;
		$Form[(int)$row['CustomsPositionId']][(int)$row['CountryId']]['Form'][] = $row ;
	    }
	    dbQueryFree ($result) ;

	    return 0 ;

	case 'a12edit' :
	case 'a12save-cmd' :
	    if ($Id <= 0) {
		// The CustomsFormA12 does not exit
		// Load basic information (Invoice, CustomsPosition and Country) selected by parameters
		$query = sprintf ('SELECT
		    Invoice.Id AS InvoiceId, Invoice.Number AS InvoiceNumber, Invoice.Description AS InvoiceDescription, Invoice.Ready AS InvoiceReady, Invoice.Done AS InvoiceDone,
		    Company.Name AS CompanyName, Company.Number AS CompanyNumber,
		    CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName, CustomsPosition.Description AS CustomsPositionDescription,
		    Country.Id AS CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
		    FROM Invoice, Country, CustomsPosition
		    LEFT JOIN Unit ON Unit.Id=CustomsPosition.UnitId
		    LEFT JOIN Company ON Company.Id=Invoice.CompanyId
		    WHERE Invoice.Id=%d AND CustomsPosition.Id=%d AND Country.Id=%d', (int)$_GET['i'], (int)$_GET['p'], (int)$_GET['c']) ;
	    	$result = dbQuery ($query) ;
	   	$Record = dbFetch ($result) ;
	    	dbQueryFree ($result) ;

		// Navigation
		navigationPasive ('delete') ;
		navigationPasive ('copy') ;

		return 0 ;
	    }

	    // The CustomsFormA12 does exist
	    // Fall through !!

	case 'a12copy-cmd' :
	case 'a12delete-cmd' :
	    // Get specific CustomsFormA12
	    $query = sprintf ('SELECT
	    	CustomsFormA12.*,
		Invoice.Id AS InvoiceId, Invoice.Number AS InvoiceNumber, Invoice.Description AS InvoiceDescription, Invoice.Ready AS InvoiceReady, Invoice.Done AS InvoiceDone,
		Company.Name AS CompanyName, Company.Number AS CompanyNumber,
		CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName, CustomsPosition.Description AS CustomsPositionDescription,
		Country.Id AS CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription,
		Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
	       	FROM CustomsFormA12
		LEFT JOIN Invoice ON Invoice.Id=CustomsFormA12.InvoiceId
		LEFT JOIN Company ON Company.Id=Invoice.CompanyId
		LEFT JOIN CustomsPosition ON CustomsPosition.Id=CustomsFormA12.CustomsPositionId
		LEFT JOIN Unit ON Unit.Id=CustomsPosition.UnitId
		LEFT JOIN Country ON Country.Id=CustomsFormA12.CountryId
		WHERE CustomsFormA12.Id=%d AND CustomsFormA12.Active=1', $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    return 0 ;
    }

    return 0 ;
?>
