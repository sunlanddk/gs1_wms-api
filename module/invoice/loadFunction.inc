<?php


    require_once $Config['legacyLib'].'lib/navigation.inc' ;
    require_once $Config['legacyLib'].'lib/fieldtype.inc' ;
    require_once $Config['legacyLib'].'lib/lookups.inc';

//	Global $User, $Record, $Navigation, $Line;

function printinvoiceload () {
	Global $Id, $User, $Record, $Navigation, $Line;

    $StateField = 'IF(Invoice.Done,"Done",IF(NOT Invoice.Ready,"Defined","Ready"))' ;
    $TypeField = 'IF(Invoice.Credit,"Credit","Invoice")' ;

    $companylimit = sprintf(' And stock.FromCompanyId = %d', $User['CompanyId']);

    $queryFields = 'Invoice.*,
		    Currency.Symbol AS CurrencySymbol, Currency.Rate AS CurrRate,
		    Stock.Id AS StockId2, Stock.Name AS StockName2, Stock.Description AS StockDescription2, Stock.CarrierId, Stock.DeliveryTermId, Stock.Address1, Stock.Address2, Stock.ZIP, Stock.City, Stock.CountryId,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, FromCompany.Name AS FromCompanyName, if(Company.MailInvoice=\'\', Company.Mail, Company.MailInvoice) AS Mail,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName, Country.Description as CountryDescription,
		    Stock.Name AS StockName, Stock.Description AS StockDescription, ' .
		    $StateField . ' AS State,' .
		    $TypeField . ' AS Type' ;

    $queryTables = 'Invoice
		    LEFT JOIN Currency ON Currency.Id=Invoice.CurrencyId
		    LEFT JOIN Company ON Company.Id=Invoice.CompanyId
		    LEFT JOIN Company FromCompany On FromCompany.id =Invoice.FromCompanyId
		    LEFT JOIN User ON User.Id=Invoice.SalesUserId
		    LEFT JOIN Stock ON Stock.Id=Invoice.StockId
			LEFT JOIN Country ON Country.Id=Stock.CountryId' ;

	// Function code
	
	if ($_credit == 1)
		$_groupclause = ' GROUP BY InvoiceLine.No ' ;
	else
//			$_groupclause = ' GROUP BY InvoiceLine.ArticleId, InvoiceLine.ArticleColorId ' ;
		$_groupclause = ' GROUP BY GroupArticleId, InvoiceLine.ArticleColorId ' ;
		
	// Query Invoice
	$query = sprintf ('SELECT %s FROM %s WHERE Invoice.Id=%d AND Invoice.Active=1', $queryFields, $queryTables, $Id) ;
	$res = dbQuery ($query) ;
	$Record = dbFetch ($res) ;
	dbQueryFree ($res) ;

	// Variables
	$SizeCount = 0 ;

	// Get InvoiceLines
	$Line = array () ;
	$query = sprintf ('SELECT
		if(Article.ArticleTypeId=6,InvoiceLine.Id+10000000,InvoiceLine.ArticleId) as GroupArticleId,
		InvoiceLine.*,min(InvoiceLine.No) as No, 
		 group_concat(InvoiceLine.No) as NoGroup,
		 ConsolidatedOrder.PartDelivery as PartDelivery,
		 count(InvoiceLine.Id) as NoGroupLines,
		 sum(`InvoiceLine`.`Quantity`) as QuantityTotal,
		 sum(`InvoiceLine`.`PriceSale`*`InvoiceLine`.`Quantity`) as PriceSaleSubTotal,
		Order.ConsolidatedId as ConsolidatedId,
		Order.Id AS OrderId, Order.Reference AS OrderReference, Order.SeasonId as SeasonId, Order.InvoiceHeader AS OrderInvoiceHeader, Order.InvoiceFooter AS OrderInvoiceFooter, Order.VariantCodes as VariantCodes,Order.Phone as OrderPhone,
		max(Order.CountryId) as AltCountryId, Order.OrderTypeId,
		Case.Id AS CaseId, Case.CustomerReference AS OrderLineReference, Case.ArticleCertificateId,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber,
		Article.Description AS ArticleDescription, Article.DeliveryComment as DeliveryComment,
		Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Color.Description AS ColorDescription, Color.AltDescription AS ColorAltDescription,
		Color.Number AS ColorNumber, Color.Id as ColorId,
		ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
		CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName,
		Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
		MaterialCountry.Description AS MaterialCountryDescription,
		KnitCountry.Description AS KnitCountryDescription,
		WorkCountry.Id AS WorkCountryId, WorkCountry.Description AS WorkCountryDescription, WorkCountry.Name AS WorkCountryName,
		CustomsPreference.Id AS CustomsPreferenceId, COUNT(CustomsPreference.Id) AS CustomsPreferenceCount, CustomsPreference.MaterialDrop, CustomsPreference.KnitDrop, CustomsPreference.WorkDrop
		FROM InvoiceLine
		LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
		LEFT JOIN ConsolidatedOrder ON ConsolidatedOrder.Id=`Order`.ConsolidatedId
		LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
		LEFT JOIN ArticleColor ON ArticleColor.Id=InvoiceLine.ArticleColorId
		LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		LEFT JOIN CustomsPosition ON CustomsPosition.Id=Article.CustomsPositionId
		LEFT JOIN Unit ON Unit.Id=Article.UnitId
		LEFT JOIN Country AS MaterialCountry ON MaterialCountry.Id=Article.MaterialCountryId
		LEFT JOIN Country AS KnitCountry ON KnitCountry.Id=Article.KnitCountryId
		LEFT JOIN Country AS WorkCountry ON WorkCountry.Id=Article.WorkCountryId
		LEFT JOIN CustomsPreference ON CustomsPreference.MaterialCountryGroup=IF(Article.MaterialCountryId>0,MaterialCountry.CountryGroup,"none") AND CustomsPreference.KnitCountryGroup=KnitCountry.CountryGroup AND CustomsPreference.WorkCountryGroup=WorkCountry.CountryGroup AND CustomsPreference.Active=1
		WHERE InvoiceLine.InvoiceId=%d AND InvoiceLine.Active=1
		 %s 
		ORDER BY InvoiceLine.No', $Id, $_groupclause) ;
	$result = dbQuery ($query) ;
	$l = 0;
	while ($row = dbFetch ($result)) {
		$Line[(int)$row['Id']] = $row ;
		if ($row['OrderId']>0) {
			if(!($Record['OrderId']==$row['OrderId'])) $Record['NoOrders']++ ;
			$Record['PartDelivery'] = $row['PartDelivery'] ;
			$Record['ConsolidatedId'] = $row['ConsolidatedId'] ;
			$Record['OrderId'] = $row['OrderId'] ;
			$Record['OrderTypeId'] = $row['OrderTypeId'] ;
		}
		if ($row['AltCountryId']>0) {
			$Record['AltCountryId'] = $row['AltCountryId'] ;
		}
		$l++ ;
		$Line[(int)$row['Id']]['No'] = $l ;
	}
	dbQueryFree ($result) ;
	
	// Get size specific quantities for each Line with VariantSize
	foreach ($Line as $i => $l) {
		if (!$l['VariantSize']) continue ;

		$Line[$i]['Size'] = array () ;
		if ($_credit == 1)
			$query = sprintf ("SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity, vc.VariantCode as VariantCode
							FROM ArticleSize
							LEFT JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId=%d AND InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id
							LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
							WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name",
							 $i, (int)$l['ArticleId'], (int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
		else
			 $query = sprintf ("SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity, vc.VariantCode as VariantCode, InvoiceLine.PriceSale, 
								InvoiceLine.No as OrderLineNo, CustomsPosition.Name AS CustomsPositionName
							FROM ArticleSize
							INNER JOIN InvoiceQuantity ON InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id
							INNER JOIN InvoiceLine On InvoiceLine.invoiceid=%d and InvoiceLine.Id=InvoiceQuantity.InvoiceLineId and InvoiceLine.articlecolorid=%d and InvoiceLine.active=1
							LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
							LEFT JOIN CustomsPosition ON CustomsPosition.Id=ArticleSize.CustomsPositionId
							WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name",
							 $Id, (int)$l['ArticleColorId'], (int)$l['ArticleId'], (int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
		$result = dbQuery ($query) ;
		$n = 0 ;
		while ($row = dbFetch ($result)) {
			$Line[$i]['Size'][(int)$row['Id']] = $row ;
			$n++ ;
		}
		dbQueryFree ($result) ;

		// Update max size count
		if ($n > $SizeCount) $SizeCount = $n ;
	}

    return 0 ;
}
?>
