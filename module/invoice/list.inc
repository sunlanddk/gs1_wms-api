<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
	case 'company-new' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'customerview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 70) ;
    listHead ('Date', 70) ;
    if (!$HideCompany) {
	listHead ('Company') ;
    }
    listHead ('Type', 70) ;
    listHead ('State', 70) ;
    listHead ('Description') ;
    listHead ('Collection', 250) ;
//    listHead ('Reference', 90) ;
    listHead ('Shipment', 200) ;
    listHead ('Sales person', 160) ;
//    listHead ('Owner', 160) ;
	//listHead('Acct #', 60);
    if (!$HideDone) {
	listHead ('Done', 75) ;
    }

    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'invoiceview', (int)$row['Id']) ;
	if ($row['Ready']) {
	    listField ((int)$row['Number']) ;
	    listField (date ("Y-m-d", dbDateDecode($row['InvoiceDate']))) ;
	} else {
	    listfield ('', 'colspan=2') ;
	}
	if (!$HideCompany) {
	    listField (sprintf ('%s (%s)', $row['CompanyName'], $row['CompanyNumber'])) ;
	}
	listField ($row['Type']) ;
	listField ($row['State']) ;
	listField ($row['Description']) ;
	$_query = sprintf("select group_concat(tab.name) as CollectionName from 
						(select c.name as name from invoiceline il, collectionmember cm, collection c 
						 where il.invoiceid=%d and il.active=1 and cm.articleid=il.articleid and cm.articlecolorid=il.articlecolorid and cm.active=1 and c.id=cm.collectionid
						 group by c.id) tab", (int)$row['Id']) ;
	$_res = dbQuery($_query) ;
	$_row = dbFetch($_res) ;
    dbQueryFree ($_res) ;
	listField ($_row['CollectionName']) ;
//	listField ($row['Reference']) ;
	listField ($row['StockName']) ;
	listField ($row['SalesUserName']) ;
//	listField ($row['FromCompanyName']) ;
	//listField ($row['AccountingNo']);
	if (!$HideDone) {
	    listField (($row['Done']) ? date ("Y-m-d", dbDateDecode($row['DoneDate'])) : 'No') ;
	}	
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Invoices', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
