<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	if ($Record['Credit']) {
		echo 'Are sure you want to make a Credit Note? <br><br>Then press "Set Ready"' ;
		
		return 0 ;
    }
	formStart () ;   
    
    itemStart() ;
    itemSpace () ;
    itemFieldRaw ('Order(s) Completely done', formCheckbox ('CompletelyDone', 0)) ;
    itemSpace () ;
    itemEnd() ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
 
    listHead ('Pos', 20) ;
    listHead ('Orderline', 45)  ;
    listHead ('Order Qty', 50) ;
    listHead ('Prev Inv Qty', 50) ;
    listHead ('Inv. Qty', 45) ;
    listHead ('Remaining', 45) ;
    listHead ('Unit', 20) ;
    listHead ('Done', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result)) and $row['OrderNo']) {
    	listRow () ;
    	listField (sprintf('%d',$row['InvLineNo'])) ;
    	listField (sprintf('O%d.%d',$row['OrderNo'],$row['OrderLineNo'])) ;
        listField(number_format ((float)$row['OQty'], (int)$row['UnitDecimals'], ',', '.')) ;
        listField(number_format ((float)$row['PrevInvQty'], (int)$row['UnitDecimals'], ',', '.')) ;
        listField(number_format ((float)$row['InvQty'], (int)$row['UnitDecimals'], ',', '.')) ;
        listField(number_format (((float)$row['OQty']-(float)$row['PrevInvQty']-(float)$row['InvQty']), (int)$row['UnitDecimals'], ',', '.')) ;
      
    	listField (sprintf('%s',$row['UnitName'])) ;
      
    	listFieldRaw (formCheckbox (sprintf('OrderLineId[%d]', (int)$row['OrderLineId']), 1)) ;
    }
    if (dbNumRows($Result) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Orderlines', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;


    formEnd () ;

    return 0 ;
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
