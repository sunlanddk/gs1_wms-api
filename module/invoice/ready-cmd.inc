<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/webshop.inc' ;

    
	global $User, $Navigation, $Record, $Size, $Id ;

	function curl_post_async($url, $params)
	{
		foreach ($params as $key => &$val) {
		  if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);

		$parts=parse_url($url);
		$fp = fsockopen($parts['host'], isset($parts['port'])?$parts['port']:80, $errno, $errstr, 30);

		If ($fp==0) echo "Couldn't open a socket to ".$url." (".$errstr.")" ;

		$out = "POST ".$parts['path']." HTTP/1.1\r\n";
		$out.= "Host: ".$parts['host']."\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: ".strlen($post_string)."\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) $out.= $post_string;

		fwrite($fp, $out);
		fclose($fp);
	}
	
	// Any Invoice Lines
	$query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count == 0) return 'the Invoice can not be set Ready when it has no InvoiceLines' ;

	// set ready.
	// Get last used number
	if ($Record['InvoiceCountryId']>0)
		$CustomerCountryId = $Record['InvoiceCountryId'] ;
	else
		$CustomerCountryId = $Record['CompanyCountryId'] ;
		
	if ($Record['FromCompanyId'] == 0) { // Norway samlefaktura - not active
		switch ($CustomerCountryId) {
			case 15: //NO
			case 85: //NO+
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
			default:
				$query = sprintf("SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
									WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=%d AND
										  Company.id=Invoice.CompanyId and not (Company.CountryId=15 or Company.CountryId=85)", $Record['FromCompanyId']) ;
				break ;
		}
	} else {
		$query = sprintf('SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND FromCompanyID=%d', $Record['FromCompanyId']) ;
	}
	
//return 'test ' . $query . 'CountryId:' . $CustomerCountryId;	
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Set InvoiceDate
	if ($Record['InvoiceDate']>'2001-01-01') {
		$InvDate = $Record['InvoiceDate'] ;
	} else {
		$t = time() ;
		// Validate
		if ($t < time()-30*24*60*60 or $t > time()+30*24*60*60) return "Invoice Date must be with +/- one month from today" ;
		$InvDate = dbDateOnlyEncode($t) ;
	}
	// Set DueBaseDate
	if ($Record['DueBaseDate']>'2001-01-01') {
		$DueBaseDate = $Record['DueBaseDate'] ;
		$t = dbDateDecode($DueBaseDate) ;
	} else {
		$t = time() ;
		// Validate
		if ($t < dbDateDecode($InvDate)) return "DueBase can not be before Invoice date" ;
		if ($t > dbDateDecode($InvDate)+365*24*60*60) return "DueBase must be within one year from Invoice date" ;

		$DueBaseDate = dbDateOnlyEncode($t) ;
	}
	// Set DueDate
	if (!$Record['Credit']) {
		    // Get PaymentTerms and 
		    $query = sprintf ('SELECT * FROM PaymentTerm WHERE Id=%d', (int)$Record['PaymentTermId']) ;
		    $result = dbQuery ($query) ;
		    $p_row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    
		    $t = getdate ($t) ;
		    if ($p_row['RunningMonth']) {
				$t['mday'] = 1 ;
				$t['mon'] += 1 ;
		    }
		    $t['mon'] += (int)$p_row['PayMonths'] ;
		    $t['year'] += ($t['mon'] - 1) / 12 ;		
		    $t['mon'] = ($t['mon'] - 1) % 12 + 1 ;
		    $t = mktime (0, 0, 0, $t['mon'], $t['mday'], $t['year']) ;
		    $t += 60*60*24*(int)$p_row['PayDays'] ;
		    $DueDate = dbDateOnlyEncode($t) ;
	} else $DueDate = '0000-00-00' ;
	$Invoice = array (	
			'Ready'			=>  1,
			'ReadyUserId'	=>  $User['Id'],
			'ReadyDate'		=>  dbDateEncode(time()),
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time()),
			'InvoiceDate'	=>  $InvDate,
			'DueBaseDate'	=>  $DueBaseDate,
			'DueDate'		=>  $DueDate,
			'Number'		=> (int)$row['Number'] + 1, 
			'CurrencyRate'		=>  tableGetField('Currency','Rate',(int)$Record['CurrencyId'])
	) ;
	tableWrite ('Invoice', $Invoice, $Id) ;
	
	if ($Record['Credit']) return 0 ;
	
//	if (!is_array($_POST['OrderLineId'])) return 'please select Orderline(s) to be doned' ;
    // Set Orderlines to done!
	if ($_POST['CompletelyDone'] == 'on')
	{
		// set �ll orders done
		$query = sprintf ('select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1
										 group by ol.orderid', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $row['orderid']) ;
			
			$OrderId = $row['orderid'] ;
			
			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $row['orderid'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

		}
		dbQueryFree ($result) ;
		
		// set all lines done that are not already done
		$query = sprintf ('select orderline.* from orderline join
										(select ol.orderid as OLorderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.OLorderid and orderline.active=1', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Orderline = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
			) ;
			tableWrite ('Orderline', $Orderline, $row['Id']) ;
		}

		// Set state on WEbshop order if there is one.
		if ($OrderId > 0)
			$OrderTypeId=(int)tableGetField('`Order`', 'OrderTypeId', $OrderId) ;
		else $OrderTypeId=0 ;
		
//return 'test ' . $row['OrderId'] . ' M ' .  $row['orderid'] . ' M '. $OrderId . ' M '. $OrderTypeId;
		if ($OrderTypeId == 10) {
			// Set state in weborder.
			$ShopNo=tableGetField('`Order`', 'WebShopNo', $OrderId) ;
			$WEBID=tableGetField('`Order`', 'Reference', $OrderId) ;
			dbReConnect($Webshop[$ShopNo]['Ip'], $Webshop[$ShopNo]['name']) ;
			$query = sprintf ("UPDATE ws_order SET STATUS=5 Where WEBID='%s'", $WEBID) ;
			$res = dbQuery ($query) ;
			dbReConnect('localhost', 'devel') ;

//			$Params['WEBID'] = $WEBID ;
//			curl_post_async ('http://192.168.2.247/fredsworld/send_ship_email.php', $Params) ;

		}
//return 'test ' . $row['OrderId'] . ' M ' .  $row['orderid'] . ' M '. $OrderId . ' M '. $OrderTypeId . ' Test OT:' . $OrderTypeID . ' ShopNo ' . $ShopNo . ' WEBID ' . $WEBID . ' query ' . $query ;
		
		dbQueryFree ($result) ;
//return 'Test OT:' . $OrderTypeID . ' ShopNo ' . $ShopNo . ' WEBID ' . $WEBID . ' query ' . $query ;
	} else {
	foreach ($_POST['OrderLineId'] as $ol_id => $flag) {
		if ($ol_id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;
		$Orderline = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
		) ;
		tableWrite ('Orderline', $Orderline, $ol_id) ;

		// Set Orderdone?
		// last line? set order done
		$OrderId=tableGetField('OrderLine', 'OrderId', $ol_id) ;
		$query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0 and Active=1', $OrderId) ;
		$res = dbQuery ($query) ;
		if (!($row = dbFetch ($res))) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $OrderId) ;

			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $OrderId)) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
				
			// Set state on WEbshop order if there is one.
			$OrderTypeId=(int)tableGetField('`Order`', 'OrderTypeId', $OrderId) ;
			if ($OrderTypeId == 10) {
				// Set state in weborder.
				$WEBID=tableGetField('`Order`', 'Reference', $OrderId) ;
				$ShopNo=tableGetField('`Order`', 'WebShopNo', $OrderId) ;
				dbReConnect($Webshop[$ShopNo]['Ip'], $Webshop[$ShopNo]['name']) ;
				$query = sprintf ("UPDATE ws_order SET STATUS=5 Where WEBID='%s'", $WEBID) ;
				$res = dbQuery ($query) ;
				dbReConnect('localhost', 'devel') ;
			}		
		}
	}
	}
	
    return 0 ;
?>
	
