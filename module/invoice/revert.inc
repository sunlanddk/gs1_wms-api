<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Invoice', ($Record['Ready']) ? (int)$Record['Number'] : '-') ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemField ('Warning', 'Reverting Ready-flag may lead to inconsistent sequense of Invoice Numbers', 'style="color:#ff0000;"') ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;

    function flagRevert (&$Record, $field) {
	if ($Record[$field]) {
	    itemFieldRaw ($field, formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    itemField ($field, 'no') ;
	}
    }
    flagRevert ($Record, 'Ready') ;
    flagRevert ($Record, 'Done') ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;
   
    return 0 ;
?>
