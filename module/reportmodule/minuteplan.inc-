<?php

    define ('DEBUG', 0) ;

if (DEBUG) require_once 'lib/log.inc' ;
    
    // Compute date intervals
    $FromDate = $Parameter['Week']['Value'] ;
    $ToDate = $FromDate + 7*24*60*60 ;
if (DEBUG) printf ("From %s to %s<br><br>\n", date('Y-m-d', $FromDate), date('Y-m-d', $ToDate)) ;

    // Get WorkGroups
    $query = sprintf ("SELECT * FROM WorkGroup WHERE Active=1 AND Sewing=1 ORDER BY Number") ;
    $result = dbQuery ($query) ;
    $WorkGroup = array () ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	$WorkGroup[$id] = $row ;
    }
    dbQueryFree ($result) ;
    if (count($WorkGroup) == 0) return sprintf ("No WorkGroups defined") ;

    // Add additional WorkGroup for work outsourced
    $WorkGroup[0] = array ('Number' => 'Outsourced') ;

    // Get Capacities
    $query = sprintf ("SELECT WorkGroupId AS Id, Count(Id) AS Days, SUM(Operators*ProductionMinutes) AS OperatorMinutes FROM Capacity WHERE ActualDate>='%s' AND ActualDate<'%s' AND Active=1 GROUP BY WorkGroupId", date('Y-m-d', (time() > $FromDate) ? time() : $FromDate), date('Y-m-d',$ToDate)) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($WorkGroup[$id])) continue ;
	$WorkGroup[$id]['OperatorMinutes'] = (int)($row['OperatorMinutes'] * $WorkGroup[$id]['Efficiency'] / 100) ;
	$WorkGroup[$id]['Days'] = (int)$row['Days'] ;
    }
    dbQueryFree ($result) ;
if (DEBUG) logPrintVar ($WorkGroup, 'WorkGroup') ;

    // Get Productions
    $clause = sprintf ("Production.Done=0 AND Production.Packed=0 AND Production.Active=1 AND Production.ProductionEndDate<'%s'", date('Y-m-d', $ToDate)) ;
    if (time() < $FromDate) $clause .= sprintf(" AND Production.ProductionEndDate>='%s'", date('Y-m-d', $FromDate)) ;
    if ($Parameter['Requests']['Value']) { 
	$clause .= ' AND (Production.Allocated=1 OR Production.Fabrics=1)' ;
    } else {
	$clause .= ' AND Production.Allocated=1' ;
    }
    $clause .= sprintf (' AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
    $query = sprintf ("SELECT Production.*, Article.Number AS ArticleNumber, Article.Description 
AS ArticleDescription, Style.Version AS StyleVersion, CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName, 
IF(Production.ProductionEndDate<'%s','Yes','No') AS Late FROM Production 
LEFT JOIN Style ON Style.Id=Production.StyleId LEFT JOIN Article ON Article.Id=Style.ArticleId LEFT JOIN `Case` 
ON Case.Id=Production.CaseId LEFT JOIN Company ON Company.Id=Case.CompanyId WHERE %s ORDER BY Article.Number", 
date('Y-m-d', $FromDate), $clause) ;
if (DEBUG) printf ("Production query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    $Production = array () ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	$row['Work'] = array () ;
	$Production[$id] = $row ;
    }
    dbQueryFree ($result) ;
    if (count($Production) == 0) return sprintf ("No ProductionOrders") ;

    // Get real bundle quantity for Productions cut
    $group = '' ;
    foreach ($Production as $id => $row) {
	if (!$row['Cut']) continue ;
	$Production[$id]['Quantity'] = 0 ;
	if ($group != '') $group .= ',' ;
	$group .= $row['Id'] ;		
    }
    if ($group != '') {
	$query = sprintf ("SELECT Bundle.ProductionId AS Id, SUM(Bundle.Quantity) AS Quantity FROM Bundle WHERE Bundle.ProductionId IN (%s) AND Bundle.Active=1 GROUP BY Bundle.ProductionId", $group) ;
if (DEBUG) printf ("Bundle query: %s<br>\n", $query) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch($result)) {
	    $id = (int)$row['Id'] ;
	    $Production[$id]['Quantity'] = (int)$row['Quantity'] ;
if (DEBUG) printf ("Bundle quantity %d, ProductionId %d<br>\n", $row['Quantity'], $id) ;
	}
	dbQueryFree ($result) ;
    }

    // Get Work Done
    $group = '' ;
    foreach ($Production as $id => $row) {
	if ($group != '') $group .= ',' ;
	$group .= $row['Id'] ;		
    }
    $query = sprintf ("SELECT BundleWork.ProductionId AS Id, MachineGroup.WorkGroupId, SUM(BundleWork.Quantity*StyleOperation.ProductionMinutes) AS ProductionMinutes FROM BundleWork, StyleOperation, MachineGroup WHERE BundleWork.ProductionId IN (%s) AND BundleWork.Active=1 AND StyleOperation.Id=BundleWork.StyleOperationId AND MachineGroup.Id=StyleOperation.MachineGroupId GROUP BY BundleWork.ProductionId, MachineGroup.WorkGroupId", $group) ;
if (DEBUG) printf ("Work query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$Production[(int)$row['Id']]['Work'][(int)$row['WorkGroupId']]['ProductionMinutes'] = $row['ProductionMinutes'] ;
    }
    dbQueryFree ($result) ;
if (DEBUG) logPrintVar ($Production, 'Production') ;

    // Get ProductionMinutes for WorkGroups done internally
    $query = sprintf ("SELECT Production.Id, MachineGroup.WorkGroupId, SUM(StyleOperation.ProductionMinutes) AS ProductionMinutes FROM Production, StyleOperation, MachineGroup WHERE %s AND StyleOperation.StyleId=Production.StyleId AND StyleOperation.Active=1 AND (Production.SourceLocationId=0 OR StyleOperation.No<Production.SourceFromNo OR StyleOperation.No>Production.SourceToNo) AND MachineGroup.Id=StyleOperation.MachineGroupId GROUP BY Production.Id, MachineGroup.WorkGroupId", $clause) ;
if (DEBUG) printf ("StyleOperation query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$Production[(int)$row['Id']]['WorkGroup'][(int)$row['WorkGroupId']]['ProductionMinutes'] = (float)$row['ProductionMinutes'] ;
    }
    dbQueryFree ($result) ;

    // Get ProductionMinutes for WorkGroups done externally
    $query = sprintf ("SELECT Production.Id, SUM(StyleOperation.ProductionMinutes) AS ProductionMinutes FROM Production, StyleOperation WHERE %s AND StyleOperation.StyleId=Production.StyleId AND StyleOperation.Active=1 AND Production.SourceLocationId>0 AND StyleOperation.No>=Production.SourceFromNo AND StyleOperation.No<=Production.SourceToNo GROUP BY Production.Id", $clause) ;
if (DEBUG) printf ("StyleOperation query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	$Production[$id]['WorkGroup'][0]['ProductionMinutes'] = (float)$row['ProductionMinutes'] ;
	if ($Production[$id]['SourceIn']) $Production[$id]['Work'][0]['ProductionMinutes'] = (float)$row['ProductionMinutes'] * $Production[$id]['Quantity'] ;
    }
    dbQueryFree ($result) ;

//get quantity pr group
foreach ($Production as $id=>$prod) {
    if(!$prod['Cut']) {
    $query = sprintf("SELECT SUM(q.Quantity) AS Quantity, m.Id AS GroupId
from Production p INNER JOIN  StyleOperation o ON p.StyleId=o.StyleID AND o.Active=1
INNER JOIN OutSourcingPlan s ON s.ProductionId = p.Id 
	AND s.OperationFromNo > o.No AND s.OperationToNo < o.No
INNER JOIN ProductionQuantity q ON q.ProductionId=p.Id
	AND (q.ArticleColorId = s.ArticleColorId OR s.ArticleColorId is null)
	AND (q.ArticleSizeId = s.ArticleSizeId OR s.ArticleSizeId is null)
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d GROUP BY m.Id", $id);
  } else {
    $query = sprintf("
SELECT coalesce(SUM(b.Quantity), 0) AS Quantity, m.Id AS GroupId
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.StyleId = p.StyleId AND o.Active=1
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d AND s.InDate is null
GROUP BY m.Id", $id);
  }
  $tab = dbRead($query);
  
  $Production[$id]['WorkGrup'] = $tab[0]['OutMinutes'];
}

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('Minute Plan') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (3, 1) ;
    $sheet->freezePanes (array(6,5)) ;

    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'Minute Plan', $format) ;
    $sheet->writeString (1, 0, sprintf (" %s: %s, Week %d, from %s to %s%s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']), date('W',$FromDate), date ('Y-m-d', $FromDate), date ('Y-m-d', $ToDate), ($Parameter['Requests']['Value']) ? ', including requested ProductionOrders' : '')) ;
    $sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

    // Table Headline formats
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;

    $formatBorder =& $book->addFormat () ;
    $formatBorder->setFgColor (HEADLINECOLOR) ;
    $formatBorder->setLeft (1) ;

    $formatRight =& $book->addFormat () ;
    $formatRight->setFgColor (HEADLINECOLOR) ;
    $formatRight->setAlign ('right') ;

    $formatRightBorder =& $book->addFormat () ;
    $formatRightBorder->setFgColor (HEADLINECOLOR) ;
    $formatRightBorder->setAlign ('right') ;
    $formatRightBorder->setLeft (1) ;

    $formatDec3 =& $book->addFormat () ;
    $formatDec3->setFgColor (HEADLINECOLOR) ;
    $formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

    // Table Headline 1
    $collumn = 0 ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    $sheet->setColumn($collumn, $collumn, 8) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    $sheet->setColumn($collumn, $collumn, 5) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    foreach ($WorkGroup as $row) {
        $sheet->setColumn($collumn, $collumn, 8) ;
	$sheet->writeString (HEADLINE, $collumn++, $row['Number'], $formatBorder) ;
        $sheet->setColumn($collumn, $collumn, 8) ;
	$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
        $sheet->setColumn($collumn, $collumn, 8) ;
	$sheet->writeNumber (HEADLINE, $collumn++, $row['OperatorMinutes'], $formatDec3) ;
    }

    // Table Headline 2
    $collumn = 0 ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Customer', $format) ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Production', $format) ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Style', $format) ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Quantity', $formatRight) ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Late', $formatRight) ;
    for ($n = 0 ; $n < count($WorkGroup) ; $n++) {
//    foreach ($WorkGroup as $row) {
	$sheet->writeString (HEADLINE+1, $collumn++, 'Time', $formatRightBorder) ;
	$sheet->writeString (HEADLINE+1, $collumn++, 'Total', $formatRight) ;
	$sheet->writeString (HEADLINE+1, $collumn++, 'Rest', $formatRight) ;
    }

    // Table Data formats
    $formatDec3 =& $book->addFormat () ;
    $formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

    $formatDec4 =& $book->addFormat () ;
    $formatDec4->setNumFormat (4) ;		// Format: decimal #,##0.00

    $formatDec4Border =& $book->addFormat () ;
    $formatDec4Border->setNumFormat (4) ;	// Format: decimal #,##0.00
    $formatDec4Border->setLeft (1) ;

    $formatBorder =& $book->addFormat () ;
    $formatBorder->setLeft (1) ;

    $formatRight =& $book->addFormat () ;
    $formatRight->setAlign ('right') ;

    // Table Data
    $line = HEADLINE + 2 ;
    foreach ($Production as $row) {
	$collumn = 0 ;

	$sheet->writeString ($line, $collumn++, $row['CompanyName']) ;
	$sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number']) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion']) ;
	$sheet->writeNumber ($line, $collumn++, $row['Quantity'], $formatDec3) ;
	$sheet->writeString ($line, $collumn++, $row['Late'], $formatRight) ;

	foreach ($WorkGroup as $WorkGroupId => $w) {
	    $ProductionMinutes = $row['WorkGroup'][$WorkGroupId]['ProductionMinutes'] ;
	    if ($ProductionMinutes > 0) {
		$sheet->writeNumber ($line, $collumn++, $ProductionMinutes, $formatDec4Border) ;

		$v = (int)($row['Quantity'] * $ProductionMinutes) ;
		$WorkGroup[$WorkGroupId]['TotalTime'] += $v ;
		$sheet->writeNumber ($line, $collumn++, $v, $formatDec3) ;

		$v -= (int)($row['Work'][$WorkGroupId]['ProductionMinutes']) ;
		if ($v < 0) $v = 0 ;
		$WorkGroup[$WorkGroupId]['TotalRest'] += $v ;
		$sheet->writeNumber ($line, $collumn++, $v, $formatDec3) ;
	    } else {
		$sheet->writeBlank ($line, $collumn++, $formatBorder) ;
		$collumn++ ;
		$collumn++ ;
	    }
	}
	$line++ ;
    }

    // Total formats
    $format =& $book->addFormat () ;
    $format->setTop (1) ;

    $formatBorder =& $book->addFormat () ;
    $formatBorder->setTop (1) ;
    $formatBorder->setLeft (1) ;

    $formatDec3 =& $book->addFormat () ;
    $formatDec3->setTop (1) ;
    $formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

    // Total
    $collumn = 0 ;
    $sheet->writeString ($line, $collumn++, 'Total', $format) ;
    $sheet->writeBlank ($line, $collumn++, $format) ;
    $sheet->writeBlank ($line, $collumn++, $format) ;
    $sheet->writeBlank ($line, $collumn++, $format) ;
    $sheet->writeBlank ($line, $collumn++, $format) ;
    foreach ($WorkGroup as $row) {
	$sheet->writeBlank ($line, $collumn++, $formatBorder) ;
	$sheet->writeNumber ($line, $collumn, $row['TotalTime'], $formatDec3) ;
	$collumn++ ;
	$sheet->writeNumber ($line, $collumn, $row['TotalRest'], $formatDec3) ;
	$collumn++ ;
    }

    if (headers_sent()) {
	printf ("<br>Header allready send, stopping<br>\n") ;
	return 0 ;	
    }

    // Let's send the file
    $book->send ('plan.xls') ;
    $book->close();
    
    return 0 ;
?>
