<?php

    require_once 'lib/navigation.inc' ;

    printf ("<table hight=100%% class=item><tr><td style='border-bottom: 1px solid #cdcabb;'>\n") ;

    // Query records to list
    $query = sprintf ("SELECT News.* FROM News WHERE News.Active=1 AND News.Public = 0 ORDER BY News.CreateDate DESC LIMIT 5") ;
    $Result = dbQuery ($query) ;
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead><p class=listhead>News</p></td>") ;
    printf ("<td class=listhead width=110><p class=listhead>Created</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/news.gif', navigationOnClickMark ('newsview', $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Header'])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No news</p></td></tr>\n") ;
    }
    printf ("</table>\n") ;
    dbQueryFree ($Result) ;

    printf ("</td><td width=490 hight=100%% style='border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;'>\n") ;

    // Novotex Logo
    printf ("<center><br>") ;
    include './module/home/logo.inc' ;
    printf ("<br></center>") ;

    printf ("</td></tr></table>\n") ;
    
    return 0 ;
?>
