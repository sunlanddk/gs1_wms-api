<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/reader.inc';
    require_once 'lib/save.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

    $fields = array (    
		'CollMembersReady'		=> array ('type' => 'checkbox','check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'CollMembersReady':
				// Ignore if not setting flag
				if (!$changed) return false ;
				return true ;
		}
		if (!$changed) return false ;
		return true ;	
    }
    
    $res = saveFields ('Season', $Id, $fields, true) ;
    if ($res) return $res ;

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	
	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
//		$data->setOutputEncoding('cp1251');//CP1251
		$data->setOutputEncoding('UTF-8');//was CP1251
		$data->read($_FILES['Doc']['tmp_name']);
		
//		print_r($data->sheets[0]);
//		return 'smth';
		// Format selection
		switch($data->sheets[0]['cells'][1][1]) {
		  case '1':
			$SheetConfig = array (
				'ArticleInfoFromVariant' => 0,
				'ArticleColomn' => 3,
				'ColorColomn' => 4,
				'LOTColomn' => 6,
				'UsePriceGroup' => 0,
	//			'PriceColomn' => 0,
	//			'RetailColomn' => 0,
	//			'CampaignColomn' => 0,
				'UseCampaignColomn' => 0,
				'PriceStart' => 24,
				'PriceEnd' => 31
			) ;
			break ;
		  case '2':
			$SheetConfig = array (
				'ArticleInfoFromVariant' => 0,
				'ArticleColomn' => 3,
				'ColorColomn' => 4,
				'LOTColomn' => 6,
				'UsePriceGroup' => 1,
	//			'PriceColomn' => 0,
	//			'RetailColomn' => 0,
	//			'CampaignColomn' => 0,
				'UseCampaignColomn' => 1,
				'PriceStart' => 24,
				'PriceEnd' => 35
			) ;
			break ;
		  default:
		    return 'Import format (' . $data->sheets[0]['cells'][1][1]  . ') specified in cell A1 is not a valid format' ;
			break ;
		}

		$AltLanguage = (int)$data->sheets[0]['cells'][6][1] ;

		if ($AltLanguage > 0)
		    require_once 'lib/db_utf8.inc' ;

		for ($k = 0; $k <= 4; $k++) {
		// LOT sheet?
		if ($data->sheets[$k]['cells'][10][1] == 'LOTS') {
		    $LOTSheet = $k ;
			for ($i = 11; $i <= $data->sheets[$LOTSheet]['numRows']; $i++) {
				if ($data->sheets[$LOTSheet]['cells'][$i][1] == '') continue ;

				$CollectionLOT['Name'] = $data->sheets[$LOTSheet]['cells'][$i][1] ;
				if ($data->sheets[$LOTSheet]['cellsInfo'][$i][2]['type'] == 'date') {
					$CollectionLOT['Date'] = date("Y-m-d", $data->sheets[$LOTSheet]['cellsInfo'][$i][2]['raw']) ;
				} else {
					return sprintf('Illegal date type (%s) in %d LOT %s', $data->sheets[$LOTSheet]['cells'][$i][2], $i, $CollectionLOT['Name']) ;
				}
				$CollectionLOT['SeasonId'] = (int)$Id ;

				$whereclause = sprintf ("SeasonId=%d and name='%s' and active=1", (int)$CollectionLOT['SeasonId'], $CollectionLOT['Name']) ;
				$CollectionLOTId = tableGetFieldWhere ('CollectionLOT', 'Id', $whereclause );
				$CollectionLOTId = tableWrite ('CollectionLOT', $CollectionLOT, $CollectionLOTId) ;
			}
			Continue ;
		}
		
		// New Collection
		if ($data->sheets[$k]['cells'][2][1] == '') continue ;
		
		$Collection['Name'] = $data->sheets[$k]['cells'][2][1] ;
		$Collection['SeasonId'] = $Id ;	
		$Collection['OwnerId'] = $Record['OwnerId'] ;
		$Collection['CompanyId'] = $Record['CompanyId'] ;

		$whereclause = sprintf ("Name='%s' and seasonid=%d and active=1", $Collection['Name'], $Id) ;
		$CollectionId=tableGetFieldWhere ('Collection', 'Id', $whereclause ) ;
		$CollectionId=tableWrite ('Collection', $Collection, $CollectionId) ;
		
		unset ($SheetConfig['Price']) ;

		// Get price colomns
		for ($x = $SheetConfig['PriceStart']; $x <= ($SheetConfig['PriceEnd']-1); $x+=(2+(int)$SheetConfig['UseCampaignColomn'])) {
			// Find PriceGroupId
			if ($SheetConfig['UsePriceGroup']) {
				$whereclause = sprintf ("name='%s'", $data->sheets[$k]['cells'][9][$x]) ;
				$SheetConfig['Price'][$data->sheets[$k]['cells'][9][$x]][$data->sheets[$k]['cells'][10][$x]]['PriceGroupId'] = tableGetFieldWhere ('CollectionPriceGroup', 'Id', $whereclause );
			} else {
				$SheetConfig['Price'][0][$data->sheets[$k]['cells'][10][$x]]['PriceGroupId'] = 0 ;
			}
			
			// Find CurrencyId
			$whereclause = sprintf ("name='%s' and active=1", $data->sheets[$k]['cells'][10][$x]) ;
			$SheetConfig['Price'][$data->sheets[$k]['cells'][9][$x]][$data->sheets[$k]['cells'][10][$x]]['CurrencyId'] = tableGetFieldWhere ('Currency', 'Id', $whereclause );

			$SheetConfig['Price'][$data->sheets[$k]['cells'][9][$x]][$data->sheets[$k]['cells'][10][$x]]['WholesaleColomn'] = $x;
			$SheetConfig['Price'][$data->sheets[$k]['cells'][9][$x]][$data->sheets[$k]['cells'][10][$x]]['RetailColomn'] = $x + 1;
			if ($SheetConfig['UseCampaignColomn']) 
				$SheetConfig['Price'][$data->sheets[$k]['cells'][9][$x]][$data->sheets[$k]['cells'][10][$x]]['CampaignColomn'] = $x + 2;
		}		
		
		// Create Collectionmembers
		for ($i = 0; $i <= $data->sheets[$k]['numRows']; $i++) {

			// Only proces lines with "active" in colomn A
			if ($data->sheets[$k]['cells'][$i][1] != 'active') continue ;

			// Get and check articlenumber
			$query = sprintf ("SELECT * from Article Where Number='%s' and Active=1", $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']]) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
 	   		dbQueryFree ($result) ;
			if ((int)$row['Id'] == 0)
				return sprintf ('Article "%s" in line %d not found', $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']], $i) ;
			else
				$CollectionMember['ArticleId']=(int)$row['Id'] ;
			// set alternative article description
			if ($AltLanguage > 0) {
				$Article_langId = tableGetFieldWhere('Article_lang','Id','LanguageId='.$AltLanguage.' AND ArticleId='.(int)$row['Id']) ;
				if ($Article_langId > 0) {
					$query = sprintf ("UPDATE Article_lang SET Description='%s' Where Id=%d", $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']-1], $Article_langId) ;
					$result = dbQuery ($query) ;
				} else {
				    $query = sprintf ('INSERT INTO Article_lang SET `Description`="%s", `LanguageId`=%d, `ArticleId`=%d', $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']-1], $AltLanguage, (int)$row['Id']) ;
				    dbQuery ($query) ;
				}
			}
			
			// Get and check articlecolor
			if (strlen($data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']])>0) {
				$query = sprintf ("SELECT ac.id as Id, c.id as ColorId from ArticleColor ac, Color c 
							Where ac.colorid=c.id and ac.articleid=%d and c.number='%s' and ac.Active=1 and c.Active=1", 
							$CollectionMember['ArticleId'], $data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']]) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
    			dbQueryFree ($result) ;
				if ((int)$row['Id'] == 0)
					return sprintf ('ArticleColor "%s" in line %d not found', $data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']], $i) ;
				else
					$CollectionMember['ArticleColorId'] = (int)$row['Id'] ;
			} else
				$CollectionMember['ArticleColorId'] = 0 ;

			// set alternative color description
			if ($AltLanguage > 0) {
				$Color_langId = tableGetFieldWhere('Color_lang','Id','LanguageId='.$AltLanguage.' AND ColorId='.(int)$row['ColorId']) ;
				if ($Color_langId > 0) {
					$query = sprintf ('UPDATE Color_lang SET Description="%s" Where Id=%d', data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']+1],$Color_langId) ;
					$result = dbQuery ($query) ;
				} else {
				    $query = sprintf ("INSERT INTO Color_lang SET `Description`='%s', `LanguageId`=%d, `ColorId`=%d", data->sheets[$k]['cells'][$i][$SheetConfig['ColorColomn']+1], $AltLanguage, (int)$row['ColorId']);
				    dbQuery ($query) ;
				}
			}
			
			// Get and check there is articlesizes
			$query = sprintf ("SELECT * from ArticleSize Where ArticleId='%d' and Active=1", $CollectionMember['ArticleId']) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
    		dbQueryFree ($result) ;
			if ((int)$row['Id'] == 0)
				return sprintf ('No ArticleSizes for article "%s" in line %d found', $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']], $i) ;

			$query = sprintf ("select max(id) as Id from `case` c where c.articleid=%d group by c.articleid", $CollectionMember['ArticleId']) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
   			dbQueryFree ($result) ;   	
			$CollectionMember['CaseId']=(int)$row['Id'] ;

			$CollectionMember['CollectionId']=$CollectionId ;

			// LOT
			$whereclause = sprintf ("SeasonId=%d and name='%s' and active=1", (int)$Collection['SeasonId'], $data->sheets[$k]['cells'][$i][$SheetConfig['LOTColomn']]) ;
			$CollectionMember['CollectionLOTId'] = (int)tableGetFieldWhere ('CollectionLOT', 'Id', $whereclause );
//			$CollectionMember['CollectionLOTId'] = $CollectionMember['CollectionLOTId'] > 0 ? $CollectionMember['CollectionLOTId'] : 0 ;

			// Insert or overwrite collectionmember
			$whereclause = sprintf ("collectionid=%d and articleid=%d and articlecolorid=%d and active=1", (int)$CollectionMember['CollectionId'],(int)$CollectionMember['ArticleId'], (int)$CollectionMember['ArticleColorId']) ;
			$ColletionMemberId=tableGetFieldWhere ('CollectionMember', 'Id', $whereclause ) ;
			$ColletionMemberId=tableWrite ('CollectionMember', $CollectionMember, $ColletionMemberId) ;

			// Insert or overwrite collectionmemberprices
			foreach ($SheetConfig['Price'] as $PriceGroupName => $PriceGroup) {
			foreach ($PriceGroup as $CurrencyName => $Currency) {
				$CollectionMemberPrice['CollectionMemberId'] = $ColletionMemberId ;
//				$CollectionMemberPrice['WholesalePrice'] = $data->sheets[$k]['cells'][$i][$SheetConfig['PriceColomn']] ;
//				$CollectionMemberPrice['RetailPrice'] = $data->sheets[$k]['cells'][$i][$SheetConfig['RetailColomn']] ;
				$CollectionMemberPrice['WholesalePrice'] = (float)$data->sheets[$k]['cells'][$i][$Currency['WholesaleColomn']] ;
				$CollectionMemberPrice['RetailPrice'] = (float)$data->sheets[$k]['cells'][$i][$Currency['RetailColomn']] ;
				$CollectionMemberPrice['CampaignPrice'] = (float)$data->sheets[$k]['cells'][$i][$Currency['CampaignColomn']] ;
				$CollectionMemberPrice['CurrencyId'] = (int)$Currency['CurrencyId'];
				$CollectionMemberPrice['CollectionPriceGroupId'] = (int)$Currency['PriceGroupId'];
				$whereclause = sprintf ("collectionmemberid=%d and CurrencyId=%d and CollectionPriceGroupId=%d and active=1", 
										(int)$ColletionMemberId,(int)$CollectionMemberPrice['CurrencyId'],(int)$CollectionMemberPrice['CollectionPriceGroupId']) ;
				$ColletionMemberPriceId=tableGetFieldWhere ('CollectionMemberPrice', 'Id', $whereclause ) ;
				$ColletionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice, $ColletionMemberPriceId) ;
			}
			}
		} // end for rows
		} // end for sheets
		
	} 

    return 0 ;    
?>
