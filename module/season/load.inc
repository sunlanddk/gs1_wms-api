<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
	    
	case 'list' :
	    require_once 'lib/list.inc' ;
		$queryFields =  's.Id as Id, s.Name as Name, c.Name as CompanyName, oc.Name as OwnerCompanyName';
	    
		$queryTables = '(Season s) LEFT JOIN Company c ON s.companyid=c.Id LEFT JOIN Company oc ON s.ownerid=oc.Id' ;

	    switch ($Navigation['Parameters']) {
		case 'company' :
			// List field specification
		      $fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Name',			'db' => 's.Name'),
				array ('name' => 'Customer',			'db' => 'c.Name'),
				array ('name' => 'Owner',			'db' => 'oc.Name')
		      ) ;

			$queryClause = sprintf('s.Active=1 and s.CompanyId=%d', $Id) ;
//			$queryClause = sprintf('s.Active=1') ;
			$queryOrder = 'c.Name, s.id' ;
			$SeasonNameFirst = 1 ;
			break ;
		default:
			// List field specification
		      $fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Customer',			'db' => 'c.Name'),
				array ('name' => 'Name',			'db' => 's.Name'),
				array ('name' => 'Owner',			'db' => 'oc.Name')
		      ) ;

			$queryClause = 's.Active=1 and s.done=0' ;
			$queryClause = sprintf('s.Active=1 and s.done=0 and s.OwnerId=%d', $User['CompanyId']) ;
			$queryOrder = 'c.Name, s.Name' ;
			$SeasonNameFirst = 1 ;
			break ;
	    }
	    
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrder) ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'view') ;
	    
	case 'view' :
		$query = sprintf ("SELECT * FROM Season WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		navigationDisable ('PickOrders') ;
		if ($Record['WebshopId']==0) {
			navigationPasive ('season-to-webshop') ;
		}
//		navigationPasive ('collmemberlist') ;
//	    if ($Record['ExplicitMembers']) {
//		    navigationEnable ('PickOrders') ;
//	    } else {
//		navigationPasive ('PickOrders') ;
//		navigationPasive ('collmemberlist') ;
//		    navigationDisable ('PickOrders') ;
//	    }

  	// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Collection',		'db' => 'c.Name'),
		array ('name' => 'Brand',			'db' => 'b.name'),
		array ('name' => 'Description',		'db' => 'c.Description')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'c.Id as Id, c.Name as Collection, c.Description as Description, b.Name as BrandName';
	    
		$queryTables = '(Collection c) LEFT JOIN Brand b on b.id=c.BrandId' ;

		$queryClause = sprintf(' c.Active=1 and c.SeasonId=%d', $Id) ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'collmemberlist') ;
	    
 	case 'basic' :		
	case 'basic-cmd' :
 	case 'combine' :		
	case 'combine-cmd' :
 	case 'batchgen-pick' :
	case 'batchgen-pick-cmd' :
 	case 'generate-ean' :
	case 'generate-ean-cmd' :
 	case 'export-to-web':
 	case 'import-pos' :
	case 'import-pos-cmd' :
	case 'delete-cmd' :
	case 'weborders' :
	case 'export-web-cmd' :
		$query = sprintf ("SELECT * FROM Season WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
// return sprintf ('owner %d name %s ', (int)$Record['OwnerId'], $Record['Name']) ;
		return 0 ;
    }

    return 0 ;
?>
