<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;

	// return 'scr ' ; 
	
	if ($Navigation['Parameters'] == 'new') {
		// Create Assortment record
		$SeasonAllowUser['SeasonId'] = (int)$Id;
		$SeasonAllowUser['UserId'] = (int)$_POST['UserId'];
		$SeasonAllowUser['AgentFee'] = (int)$_POST['AgentFee'];
		tableWrite('SeasonAllowUser',$SeasonAllowUser) ;
		
		return navigationCommandMark ('Seasonview', (int)$Id) ;
	} else {
		// Edt agent fee
		$SeasonId=tableGetField('SeasonAllowUser','SeasonId',(int)$Id) ;
		$SeasonAllowUser['AgentFee'] = (int)$_POST['AgentFee'];
		tableWrite('SeasonAllowUser',$SeasonAllowUser, $Id) ;
		
		return navigationCommandMark ('Seasonview', (int)$SeasonId) ;
	}
 
 return 0 ;    

?>
