<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
   	require_once 'lib/reader.inc';
    require_once 'lib/save.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

    $fields = array (    
		'PickPosGenerated'		=> array ('type' => 'checkbox','check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'PickPosGenerated':
				// Ignore if not setting flag
				if (!$changed) return false ;
				return true ;
		}
		if (!$changed) return false ;
		return true ;	
    }
    
    $res = saveFields ('Season', $Id, $fields, true) ;
    if ($res) return $res ;

	$Error = 0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);
		
		$k = 0 ;

		if ($data->sheets[0]['numRows']<6) return sprintf ('Less than 6 rows (%d) in Excelsheet 0', $data->sheets[0]['numRows']) ;

		for ($i = 6; $i <= $data->sheets[0]['numRows']; $i++) {

			// Find articleid
			if ($data->sheets[$k]['cells'][$i][3] == '') continue ; // Ignore line if no articlenumber 
			$ArticleId = tableGetFieldWhere('Article','Id',sprintf("Number='%s' and Active=1",$data->sheets[$k]['cells'][$i][3]));
			if ($ArticleId == 0) return sprintf('Article %s in line %d dosnt exist',$data->sheets[$k]['cells'][$i][3], $i) ;

			// Find articleColorId if article has colors
			if ( tableGetFieldWhere('Article','VariantColor',sprintf("Id='%d' and Active=1", $ArticleId))) {
				$Color = $data->sheets[$k]['cells'][$i][6] ;
				$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s' and Active=1",$data->sheets[$k]['cells'][$i][6]));
				if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $data->sheets[$k]['cells'][$i][6], $i) ;
				$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d And Active=1",$ArticleId,$ColorId));
				if ($ArticleColorId == 0) return sprintf('ArticleColor <%s>, in line %d dosnt exist %d,%d', $data->sheets[$k]['cells'][$i][6], $i,$ArticleId,$ColorId ) ;
			} else $ArticleColorId = 0 ;

			// Configuration of street and number
			$NoSizes = (int)$data->sheets[$k]['cells'][$i][7];
			$Street  = $data->sheets[$k]['cells'][$i][10];
			$Numbers = explode(';',strval($data->sheets[$k]['cells'][$i][11]));
			
			// Syntax check of what to do
			if ($Street=='#') { // Reset position
				$Numbers[0]=1; 		// Pseudo number
			} else {
				if ($Street=='' or $Numbers[0]=='') {
					if ($Street=='' and $Numbers[0]=='') {
						continue ; // Ignore line if both street and Number is blank
					} else {
						$Error=$Error . sprintf('Position conf error at line %d: Street %s, Numbers %s (%d)<br>', $i, $Street,$data->sheets[$k]['cells'][$i][11], sizeof($Numbers)) ;
						continue ;
					}
				}
			}

			// Optional: Set start floor letter if only one street number	
			$StartFloor = substr(strval($data->sheets[$k]['cells'][$i][12]),0,1) ;
			//if ($i==7) return $StartFloor; 
			//return sprintf ('Street %s Start %s', $Street, $StartFloor) ;

			// Optional: Set number of floors per number. Otherwise calculated.
			if ((int)$data->sheets[$k]['cells'][$i][13] > 0)
				$NoFloors = (int)$data->sheets[$k]['cells'][$i][13] ;
			else
				$NoFloors = (int)$NoSizes/sizeof($Numbers);
			//if ($i==8) return //sprintf("%d, %d, %d, %d",$NoFloors,(int)$NoSizes, sizeof($Numbers), $Numbers[0]) ;

			// Read size array of missing sizes not to be stored on stock.
			$Missings = explode(';',strval($data->sheets[$k]['cells'][$i][14]));
			unset ($IgnoreSize) ;
			for ($p = 0; $p <= sizeof($Missings); $p++) {
				$IgnoreSize[$Missings[$p]] = 1 ;
			} 

			// Run trough articelsizes and assign pick position.
  			$query = sprintf("select * from articlesize where active=1 and articleid=%d order by displayorder", $ArticleId) ;	
			$res = dbQuery ($query) ;

			$size_index = 0 ;
			if ($StartFloor == '')
				$floor_offset = 0 ;
			else
				$floor_offset = ord($StartFloor) - ord('a') ;

			// if NoFloors is set with only 1 streetno, then create seq of streetno's, if more streets required.
			if ((sizeof($Numbers)==1) and ($NoFloors>0)) {
				if (($NoSizes+$floor_offset) > $NoFloors) {
					for ($p = 1; $p <= (int)(($NoSizes+$floor_offset)/$NoFloors); $p++) { // Add misiing street numbers
						$Numbers[$p] = $Numbers[$p-1] + 1 ;
					} 
				} 
			} 
//return $ArticleId ;		

			while ($sizerow = dbFetch ($res)) {
				if ($IgnoreSize[$sizerow['Name']]) continue ;

				// Configuration of street, number, floor
				$displayorder=$size_index ;
				$seqno=(int)(($displayorder+$floor_offset)/$NoFloors) ; // Which street in list to use?
				// PUT rest of the floors on last street number
				if ($seqno > sizeof($Numbers)-1)	
					$seqno= sizeof($Numbers)-1 ;
				$No=$Numbers[$seqno] ;

				if ($seqno > 0)
					$StartFloor = '' ;
				if ($StartFloor == '')
					$Floor = chr(ord('a')+($displayorder+$floor_offset-$seqno*$NoFloors)) ;
				else
					$Floor = chr(ord($StartFloor)+($displayorder-$seqno*$NoFloors)) ;
				$size_index++ ;

				$VariantCodeId=tableGetFieldWhere('VariantCode','Id',sprintf("ArticleId=%d and ArticleColorId=%d and ArticleSizeId=%d and active=1",$ArticleId, $ArticleColorId, $sizerow['Id'])) ;
				if ($VariantCodeId) {
					$PickContainerId = (int)tableGetFieldWhere('VariantCode','PickContainerId',sprintf("Id=%d",$VariantCodeId)) ;
//return sprintf ('varid %d cont %d', $VariantCodeId, $PickContainerId ) ;
					if ($PickContainerId>0) {
						// Pick position exits already (Variant exists with Container)
						$whereclause = sprintf ("Id=%d", $PickContainerId) ;
						$StockId=tableGetFieldWhere ('Container', 'StockId', $whereclause ) ;
						if ($StockId == (int)$Record['PickStockId']) {
							if ($Street == '#') {
								$ContainerPos['Position'] = '' ;
							} else {
								$ContainerPos['Position'] = $Street . sprintf('%02d',$No) . $Floor ;
							}
							tableWrite ('Container', $ContainerPos, $PickContainerId ) ;
							continue ;
						} else {
							$Error=$Error . sprintf('Warning: Container at different picking stock replaced for art %d color %d, size %d at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $i) ;
						}
					} 
					// Create new Container
					$Container = array (
						'StockId' => (int)$Record['PickStockId'],
						'ContainerTypeId' => 39,
						'Position' => $Street . sprintf('%02d',$No) . $Floor ,
						'TaraWeight' => 0,
						'GrossWeight' => 0,
						'Volume' => 0
					) ;

					$Container['Id'] = tableWrite ('Container', $Container) ;
					$VariantCode['PickContainerId']=$Container['Id'] ;
					tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;
				} else 
					$Error=$Error . sprintf('Variant not found for art %d color %d, size %d at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $i) ;
			}
		}
	}
	return $Error ;
?>
