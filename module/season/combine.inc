<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Season', $Record['Name']) ;
	    itemSpace () ;
	    itemField ('WARNING', 'All collections, LOTs and orders in this season will be moved to new season') ;
		$query='select * from `order` where seasonid=' . $Id ;
		$res = dbQuery ($query) ;
		$Noorders = dbNumRows($res) ;
		if ($Noorders > 0) {
			itemSpace () ;
			itemField ('WARNING', 'You have ' .$Noorders. ' orders connected to this Season. They will all be moved') ;
			itemSpace () ;
		}
		dbQueryFree ($res) ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;
    itemSpace () ;
 	itemFieldRaw ('New Season', formDBSelect ('SeasonId', $Id, 'SELECT Season.Id, Season.Name AS Value FROM Season WHERE Done=0 And OwnerId=' . $Record['OwnerId'] . ' And Active=1 ORDER BY Value', 'width:200px')) ; 
// 	itemFieldRaw ('LOT', formDBSelect ('SeasonId', $Record['BrandId'], 'SELECT Season.Id, Season.Name AS Value FROM Season WHERE Active=1 ORDER BY Value', 'width:200px')) ; 
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
