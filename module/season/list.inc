<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    if ($SeasonNameFirst) {
   	listHead ('Name', 60)  ;
    	listHead ('Customer', 150) ;
    } else {
    	listHead ('Customer', 150) ;
   	listHead ('Name', 60)  ;
    }
    listHead ('Owner', 150) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'seasonview', (int)$row['Id']) ;
	if ($SeasonNameFirst) {
	    	listField ($row["Name"]) ;
	    	listField ($row["CompanyName"]) ;
	} else {
	    	listField ($row["CompanyName"]) ;
	    	listField ($row["Name"]) ;
	}
    	listField ($row["OwnerCompanyName"]) ;
    }

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
