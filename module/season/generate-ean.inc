<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

/*
    $query = "SELECT max(variantcode) as MaxCode FROM variantcode where active=1 and variantcode like '5704302%'" ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	$Record['BaseVarCode'] = substr($row['MaxCode'],0,12) ;
*/  
    itemStart () ;
    itemSpace () ;
    itemField ('Project', $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;

    itemSpace () ;
    itemSpace () ;
 
    itemFieldRaw ('Base Code', formText ('BaseVarCode', $Record['BaseVarCode'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Codes Ready', formCheckbox ('VarCodesReady', $Record['VarCodesReady'])) ;

//    flagRevert ($Record, 'Picked') ;

	itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
