<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/webshop.inc' ;

	$WebOrders = array () ;
	foreach ($Webshop as $key => $web_row) {
		dbReConnect($web_row['Ip'], $web_row['name']) ;

		$query = "SELECT o.id as ID, WEBID, LASTNAME,CITY, TOPAY FROM ws_order o, ws_customer c 
			where o.status=3 and o.customerid=c.id order by webid" ;
		$res = dbQuery ($query) ;

		while ($row = dbFetch($res)) {
			$WebOrders[$row['WEBID']] = $row ;
			$WebOrders[$row['WEBID']]['ShopName'] = $web_row['name'] ;
			$WebOrders[$row['WEBID']]['ShopNo'] = $key ;
		}
		dbQueryFree ($res) ;
		unset($row) ;
	}

    formStart () ;
    listStart () ;
    listRow () ;
    listHead ('Webshop', 70) ;
    listHead ('WebOrder  Number', 70) ;
    listHead ('Customer', 90) ;
    listHead ('City', 70) ;
    listHead ('Amount', 90) ;
//    listHead ('ID', 30) ;
    listHead ('Import', 30) ;
    listHead ('', 1) ;

   foreach ($WebOrders as $webid => $row) {
		listRow () ;
		listField ($row['ShopName']) ;
		listField ($row['WEBID']) ;
		listField ($row['LASTNAME']) ;
		listField ($row['CITY']) ;
		listField ($row['TOPAY']) ;
//		listField ($row['ID']) ;
		listFieldRaw (formCheckbox (sprintf('ImportFlag[%d]', (int)$row['ID']), 1, '')) ;
		listFieldRaw (formHidden(sprintf('ShopNo[%d]', (int)$row['ID']), $row['ShopNo'])) ;
    }
    listEnd () ;
    formEnd () ;

    return 0 ;
?>
