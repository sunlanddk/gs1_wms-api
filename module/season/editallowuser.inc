<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
	require_once 'lib/navigation.inc' ;

 	if ($Navigation['Parameters'] == 'new') {
		$new = 1 ;
	} else {
		$query = sprintf('SELECT * FROM SeasonAllowUser WHERE Id=%d AND Active=1', $Id) ;
		$res = dbQuery ($query) ;
		$Record = dbFetch ($res) ;
		dbQueryFree ($res) ;

		$new = 0 ;
	}
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

	if ($new) {
		itemFieldRaw ('Allow User', formDBSelect ('UserId', (int)$Record['UserId'], 
					sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM (Company, User) WHERE Company.TypeAgent=1 AND Company.Active=1 AND User.AgentCompanyId=Company.id AND User.Active=1 ORDER BY Value'), 'width:250px;')) ;  
	} else {
		itemFieldRaw ('Allow User',tableGetField('User', 'concat(firstname," ",lastname)', $Record['UserId'])) ;
	}
    itemSpace () ;
	itemFieldRaw ('Agent Fee', formText ('AgentFee',  (int)$Record['AgentFee'], 2, 'text-align:right;') . ' %' . ' - set to 0 for default agent fee') ;
    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
