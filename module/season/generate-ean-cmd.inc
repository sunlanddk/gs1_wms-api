<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/variant.inc' ;
    require_once 'lib/save.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

    $fields = array (    
		'VarCodesReady'		=> array ('type' => 'checkbox','check' => true),
		'BaseVarCode'		=> array ('check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'VarCodesReady':
				// Ignore if not setting flag
				if (!$changed) return false ;
				return true ;
			case 'BaseVarCode' :
				if (!$changed) return false ;
				if (strlen ($value) <> 7 and strlen ($value) <> 0) return sprintf ('Invalid string length %d of basecode (Only Blank or 7 digits allowed)', strlen ($value)) ;
				break ;
		}
		if (!$changed) return false ;
		return true ;	
    }
    
    $res = saveFields ('Season', $Id, $fields, true) ;
    if ($res) return $res ;

	// Generate Variant codes
	$query = "SELECT max(variantcode) as MaxCode FROM variantcode where active=1 and variantcode like '" . $Record['BaseVarCode'] . "%'" ;	
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
//return 'test ' . (int)$row['MaxCode'] . ' ' . $Record['BaseVarCode'] ;	
	if (((int)$row['MaxCode'])==0) {
		$BaseVariantCode = $Record['BaseVarCode'] . '00001' ; // First code in new serie!
	} else {
		$BaseVariantCode = substr($row['MaxCode'],0,12) ;		// Continue from last number ion series
	}
	dbQueryFree ($res) ;
//return $Record['BaseVarCode'] . ' ' . $query ;
	
	if ($Record['ExplicitMembers']==1) {
		$query = sprintf("
		SELECT c.name as Collection, s.pickstockid as PickStockId, colm.caseid as `Case`, a.Id as ArticleId, ac.Id as ArticleColorId, az.Id as ArticleSizeId, 
					vc.id as VariantId, vc.PickContainerId as ContainerId
			FROM (season s, collection c, collectionmember colm, article a)
			LEFT JOIN articlecolor ac on colm.ArticleColorId=ac.id and ac.active=1
			LEFT JOIN color co on co.id=ac.colorid and co.active=1
			LEFT JOIN articlesize az on az.articleid=a.id and az.active=1
			LEFT JOIN variantcode vc on colm.articleid=vc.articleid and (ac.id is null or ac.id=vc.articlecolorid) and (az.id Is Null or az.id=vc.articlesizeid) and vc.active=1
			WHERE s.id=%d and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
				and s.active=1 and c.active=1 and colm.active=1 and a.active=1 
			GROUP BY a.Id, ac.Id, az.id", $Id );
	} else {
		$query = sprintf("
			SELECT a.Id as ArticleId, ac.Id as ArticleColorId, az.Id as ArticleSizeId,
					vc.id as VariantId, vc.PickContainerId as ContainerId
			FROM (`order` o, orderline ol, article a)
					left join articlecolor ac on ol.ArticleColorId=ac.id and ac.active=1
					left join color co on co.id=ac.colorid and co.active=1
					left join articlesize az on az.articleid=a.id and az.active=1
					left join variantcode vc on vc.articleid=a.id and vc.articlecolorid=ac.id and vc.articlesizeid=az.id and vc.active=1
			WHERE o.seasonid=%d and ol.orderid=o.id and ol.articleid=a.id
								and o.active=1 and ol.active=1 and a.active=1 
			group by a.id,ac.id,az.id
			order by a.id, ac.id, az.displayorder
			", $Id );
	}
	$res = dbQuery ($query) ;
	
	$row_index=1 ;
	while ($row = dbFetch ($res)) {
		$VariantCode = array (	
			'VariantCode'		=>  "",			// EAN code
			'VariantUnit'		=>  "Pcs",		// 			
			'VariantmodelRef'	=>  "",			
			'VariantDescription'	=> "",			
			'VariantColorDesc'	=>  "",		
			'VariantColorCode'	=>  "",			
			'VariantSize'		=>  "",			
			'ArticleId'			=>	0,			// 
			'ArticleColorId'	=>	0,			// 
			'ArticleSizeId'		=>	0,			// 
			'Reference'			=>	0,			// None
			'Type'				=>	"case"
		) ;
 		
		$barcode = sprintf("%12.0f", ($BaseVariantCode + $row_index)) ;
		$barcode .= GetCheckDigit($barcode); 			// Add control digit

		$VariantCode['VariantCode'] = $barcode;
		$VariantCode['ArticleId'] = (int)$row['ArticleId'];
		$VariantCode['ArticleColorId'] = (int)$row['ArticleColorId'];
		$VariantCode['ArticleSizeId'] = (int)$row['ArticleSizeId'];
	 			
		// Update or insert into VariantCode table.
		if ((int)$row['VariantId'] > 0) {
			$whereclause = sprintf ("Id='%s'", $row['ContainerId']) ;
//			$StockId=tableGetFieldWhere ('Container', 'StockId', $whereclause ) ;
//			if (($StockId <> $row['PickStockId']) and ($StockId <> null)) {
//				$VariantCodeCont['PickContainerId'] = 0 ;				
//				tableWrite ('VariantCode', $VariantCodeCont, $row['VariantId']) ;				
//			}
//			Next 3 lines for updating EAN codes!
//			$VariantCodeCont['VariantCode'] = $barcode ;				
//			tableWrite ('VariantCode', $VariantCodeCont, $row['VariantId']) ;				
//			$row_index++;
		} else {
			tableWrite ('VariantCode', $VariantCode) ;
			$row_index++;
		}		
    }
    dbQueryFree ($res) ;	
    return 0 ;
?>
