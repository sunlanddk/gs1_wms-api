// Complete Sets/Sortiments statistics (without Limango)

select article.number as ArticleNumber, color.number as ColorNumber, sum(OlQty) as Qty, sum(MinQty) as Sets, sum(OlQty)- sum(MinQty) * NoSizes as Singles, NoSizes
from (
select ol.id, ol.Articleid as ArticleId, ol.ArticleColorid as ArticleColorId,
count(az.id) as NoSizes,
sum(if(oq.quantity>0,1,0)) as NoOrderedSizes,
min(if(oq.quantity>0,oq.quantity,0)) as MinQty,
sum(oq.quantity) as OlQty
from (`order` o, orderline ol, articlesize az)
Left join orderquantity oq on oq.orderlineid=ol.id and oq.active=1 and oq.articlesizeid=az.id
where o.seasonid=633 and o.active=1
and ol.orderid=o.id and ol.active=1
and az.articleid=ol.articleid and az.active=1
and not ol.orderid=58214
group by ol.id
) OL
join article on article.id=OL.articleid
join articlecolor on articlecolor.id=OL.articlecolorid
join color on color.id=articlecolor.colorid
where NoSizes>1 and MinQty>0
group by OL.articleid, OL.articlecolorid
order by Sets desc

// Combined
select article.number as ArticleNumber, color.number as ColorNumber, CompleteSets.NoSizes, CompleteSets.Qty as Quantity,
CompleteSets.Sets as CompleteSet, CompleteSets.Singles as CompleteSingles, CompleteSets.Sets+CompleteSets.Singles as CompletePicks,
CompleteSets.Qty-(CompleteSets.Sets+CompleteSets.Singles) as PickReduction,
ReducedSets.Sets as LimitedSet, ReducedSets.Singles as LimitedSingles, ReducedSets.Sets+ReducedSets.Singles as LimitedPicks,
-(CompleteSets.Sets+CompleteSets.Singles)+(ReducedSets.Sets+ReducedSets.Singles) as PickReductionByLimitedSets
FROM
(select OL.articleid, OL.articlecolorid, sum(OlQty) as Qty, sum(MinQty) as Sets, sum(OlQty)- sum(MinQty) * NoSizes as Singles, NoSizes
from (
select ol.id, ol.Articleid as ArticleId, ol.ArticleColorid as ArticleColorId,
count(az.id) as NoSizes,
sum(if(oq.quantity>0,1,0)) as NoOrderedSizes,
min(if(oq.quantity>0,oq.quantity,0)) as MinQty,
sum(oq.quantity) as OlQty
from (`order` o, orderline ol, articlesize az)
Left join orderquantity oq on oq.orderlineid=ol.id and oq.active=1 and oq.articlesizeid=az.id
where o.seasonid=633 and o.active=1
and ol.orderid=o.id and ol.active=1
and az.articleid=ol.articleid and az.active=1
and not ol.orderid=58214
group by ol.id
) OL
where NoSizes>1 and MinQty>0
group by OL.articleid, OL.articlecolorid
) CompleteSets

join article on article.id=CompleteSets.articleid
join articlecolor on articlecolor.id=CompleteSets.articlecolorid
join color on color.id=articlecolor.colorid
left join

(select OL.articleid, OL.articlecolorid, sum(OlQty) as Qty, sum(MinQty) as Sets, sum(OlQty)- sum(MinQty) * NoSizes as Singles, NoSizes
from (
select ol.id, ol.Articleid as ArticleId, ol.ArticleColorid as ArticleColorId,
count(az.id) as NoSizes,
sum(if(oq.quantity>0,1,0)) as NoOrderedSizes,
min(if(oq.quantity>0,oq.quantity,0)) as MinQty,
sum(oq.quantity) as OlQty
from (`order` o, orderline ol, articlesize az)
Left join orderquantity oq on oq.orderlineid=ol.id and oq.active=1 and oq.articlesizeid=az.id
where o.seasonid=633 and o.active=1
and ol.orderid=o.id and ol.active=1
and az.articleid=ol.articleid and az.active=1

and not az.displayorder=1 and not az.displayorder=(select max(displayorder) from articlesize where articleid=ol.articleid)

and not ol.orderid=58214
group by ol.id
) OL
where NoSizes>1 and MinQty>0
group by OL.articleid, OL.articlecolorid
) ReducedSets on ReducedSets.articleid=CompleteSets.articleid and ReducedSets.articlecolorid=CompleteSets.articlecolorid
group by ReducedSets.articleid, ReducedSets.articlecolorid
order by PickReduction desc