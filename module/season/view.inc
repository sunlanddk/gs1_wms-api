<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

//    if (dbNumRows($Result) == 0) return "No Collections assigned to this Season" ;

    printf ("<table class=item><tr><td>\n") ;

    itemStart () ;
    itemHeader ('General') ;
    itemFieldIcon ('Season', $Record['Name'], 'season.png', 'seasonlist', (int)$Record['CompanyId']) ; 
    itemField ('CollectionType', tableGetField('collectiontype','Name',$Record['CollectionTypeId'])) ;
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemSpace () ;
    itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader ('Status') ;
    if ($Record['ExplicitMembers']) {
		$query = sprintf("select count(a.id) as NoVariants, sum(if(vc.id>0,1,0)) as NoCodes
					From (collection c, collectionmember colm, article a)
					left join articlecolor ac on colm.ArticleColorId=ac.id and ac.active=1
					left join color co on co.id=ac.colorid and co.active=1
					left join articlesize az on az.articleid=a.id and az.active=1
					left join variantcode vc on vc.articleid=a.id and (vc.articlecolorid=ac.id or a.variantcolor=0) and (vc.articlesizeid=az.id or a.variantsize=0) and vc.active=1
					where c.seasonid=%d and colm.collectionid=c.id and colm.articleid=a.id and colm.active=1 and c.active=1 and a.active=1", $Id) ;
    } else {
		$query = sprintf("select count(a.id) as NoVariants, sum(if(vc.id>0,1,0)) as NoCodes
					From (`order` o, orderline ol, article a)
					left join articlecolor ac on ol.ArticleColorId=ac.id and ac.active=1
					left join color co on co.id=ac.colorid and co.active=1
					left join articlesize az on az.articleid=a.id and az.active=1
					left join variantcode vc on vc.articleid=a.id and vc.articlecolorid=ac.id and vc.articlesizeid=az.id and vc.active=1
					where o.seasonid=%d and ol.orderid=o.id and ol.articleid=a.id	and o.active=1 and ol.active=1 and a.active=1", $Id) ;
    }
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	$NotCompleted	=  "Not complete: " . $row['NoCodes'] . " out of " . $row['NoVariants'] . " generated" ;
	$Completed		=  "Codes for all " . $row['NoVariants'] . " variants has been generated" ;
    itemField ('Variant codes', ($row['NoVariants']>$row['NoCodes']) ?  $NotCompleted : $Completed) ;
	dbQueryFree ($res) ;
    itemField ('Wholesale', $Record['ExplicitMembers']?'Yes':'No') ;
    itemSpace () ;
    itemField ('Info to Alpi', $Record['VarListTransferTime']>'2001-01-01'?$Record['VarListTransferTime']:'Never') ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;

	// LOTs
	if ($Record['ExplicitMembers']) {
	    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
		listStart () ;
		listHeader ('LOTs', 200) ;
		listRow () ;
		listHead ('LOT', 30) ;
		listHead ('Planned Date', 50) ;
		listHead ('In Presale', 30) ;
		listHead ('NoRetail', 30) ;
		listHead ('Pick Generated Until', 80) ;

		$query = sprintf ('Select Name, date(`Date`) as Date,GeneratedPick,date(GeneratedDate) as GeneratedDate, PreSale, NoRetail From CollectionLOT Where SeasonId=%d and active=1 Order By Date', $Record['id']) ;
		$res = dbQuery ($query) ;
		while ($row = dbFetch ($res)) {
			listRow () ;
			listField ($row['Name']) ;
			listField ($row['Date']) ;
			listField ($row['PreSale']>0?'Yes':'') ;
			listField ($row['NoRetail']>0?'Yes':'') ;
			if ($row['GeneratedPick'])
				listField ($row['GeneratedDate']>'2001-01-01'?$row['GeneratedDate']:'ALL') ;
			else
				listField ('') ;
		}
		listEnd() ;
		printf ("</td>\n") ;
	}

	// Allowed user if restricted access
	if ($Record['RestrictAccess']) {
	    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
		listStart () ;
		listHeader ('Allowed users', 200) ;
		listRow () ;
		listHead ('Login', 30) ;
		listHead ('Name', 50) ;
		listHead ('Fee', 50) ;
		listHead ('', 10) ;
		listHead ('', 10) ;

		$query = sprintf ('Select SeasonAllowUser.Id as Id, Loginname as Login, concat(FirstName," ",LastName) as Name, Company.Name as CompanyName, SeasonAllowUser.AgentFee as AgentFee
							From (SeasonAllowUser, User, Company) Where SeasonAllowUser.SeasonId=%d AND SeasonAllowUser.Active=1 AND User.Id=SeasonAllowUser.UserId And User.active=1 and Company.Id=User.CompanyId Order By Loginname', $Record['id']) ;
		$res = dbQuery ($query) ;
		while ($row = dbFetch ($res)) {
			listRow () ;
			listField ($row['Login']) ;
			listField ($row['Name']) ;
			listField (($row['AgentFee']>0 ? $row['AgentFee'] : 'default') . ' %') ;
			listFieldIcon ('pen.gif', 'editallowuser', (int)$row['Id']) ;
			listFieldIcon ('delete.gif', 'deleteallowuser', (int)$row['Id']) ;
		}
		listRow () ;
		listFieldIcon ('add.gif', 'addallowuser', (int)$Record['id']) ;
		listEnd() ;
		printf ("</td>\n") ;
	}

    printf ("</tr></table>\n") ;


    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Collection', 50) ;
    listHead ('Brand', 95)  ;
    listHead ('Description', 95)  ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
    	listFieldIcon ('collection.png', 'collmemberlist', (int)$row['Id']) ;
    	listField ($row["Collection"]) ;
    	listField ($row["BrandName"]) ;
    	listField ($row['Description']) ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;

?>
