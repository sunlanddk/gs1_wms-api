<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'module/pick/packcsv-cmd.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

	set_time_limit(600);
	$MaxDeliveryDate = $_POST['MaxDeliveryDate'] ;

	// Validate no open pickorders
	$query = sprintf("Select * From `order` o, pickorder po where o.seasonid=%d and o.active=1 and po.`Type`='SalesOrder' and po.referenceid=o.id and po.packed=0 and po.active=1", $Id) ;
	$res = dbQuery ($query) ;
	$NoPickOrders = dbNumRows($res) ;
//	if ($NoPickOrders > 0) return 'Cant batch generate with ' . $NoPickOrders . ' open pick orders for project. Please finish pick orders and try again.' ;
    dbQueryFree ($res) ;

	$query = sprintf("select group_concat(cast(seasonid as char (5))) as seasonIds 
					from (
						select co.SeasonId from `order` o 
						inner JOIN `order` co ON co.consolidatedid=o.consolidatedid and co.active=1 and co.done=0 
						where o.done=0 and o.active=1 and o.seasonid=%d  
						group by co.seasonid
					) tab", $Id) ;
	$res = dbQuery ($query) ;
	$_seasons = dbFetch ($res) ;
    dbQueryFree ($res) ;

	// Set state for LOT
	$query = sprintf ('Select * From CollectionLOT Where SeasonId in (%s) and active=1 Order By Date', $_seasons['seasonIds']) ;
	$res = dbQuery ($query) ;
	while ($row = dbFetch ($res)) {
		if  ($row['GeneratedPick']) continue;					// already generated
		if  ($MaxDeliveryDate ) {
			if ($row['Date'] > $MaxDeliveryDate) continue;		// include this LOT?
			$query = sprintf ("UPDATE collectionlot SET GeneratedPick=1, GeneratedDate='%s' Where Id=%d", $MaxDeliveryDate, $row['Id']) ;
		} else {
			$query = sprintf ("UPDATE collectionlot SET GeneratedPick=1 Where Id=%d", $row['Id']) ;
		}
		$upd_res = dbQuery ($query) ;
	}
    dbQueryFree ($res) ;

	// Find what to generate pickorderlines for.
	// First: date range if any
	if ($MaxDeliveryDate)
		$DeliveryDateLimit = " and ol.deliverydate<'" . $MaxDeliveryDate ."'" ;
	else
		$DeliveryDateLimit = "" ;
	// Second: Collectionmembers
	$query = sprintf("SELECT lot.Date as LotDate, lot.Id AS lotId, cm.* 
					  FROM (collection cl, collectionmember cm)
					  LEFT JOIN collectionlot lot on cm.collectionlotid=lot.id
					  WHERE cl.seasonid in (%s)  and cm.collectionid=cl.id and cl.active=1 and cm.active=1", $_seasons['seasonIds']); 
	$result = dbQuery ($query) ;
	$CollectionMembers = array () ;
	while ($row = dbFetch ($result)) {
		$CollectionMembers[$row['ArticleId']][$row['ArticleColorId']] = $row ;
		$CollectionMembers[$row['ArticleId']][$row['ArticleColorId']]['AssortmentId'] = (int)tableGetFieldWhere('Assortment','Id','CollectionMemberId='.$row['id'].' AND Active=1') ;
	}
    dbQueryFree ($result) ;

	$SeasonRecord = array (	
		'PresalePickGenerated' => 1
	) ;	
	tableWrite ('Season', $SeasonRecord, $Id) ;
	
	$query = sprintf("select group_concat(cast(ConsolidatedId as char (6))) as consolidatedIds 
						from (
							select o.ConsolidatedId from `order` o, paymentterm pt 
							where o.done=0 and o.active=1 and o.seasonid=%d and pt.id=o.paymenttermid and not pt.prepay>0
							group by o.ConsolidatedId
						) tab", $Id) ;
	$res = dbQuery ($query) ;
	$_consolidated = dbFetch ($res) ;
    dbQueryFree ($res) ;

	$query = sprintf("SELECT o.id as OrderId, o.ConsolidatedId as ConsolidatedId, ol.id as OrderLineId, o.companyid as CompanyId, o.Instructions as Instructions, vc.id as VariantCodeId, vc.variantcode as VariantCode,
							ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid,
							oq.articlesizeid, oq.quantity as quantity, oq.Boxnumber as BoxNumber, oq.QuantityReceived, oq.Id as QlId,
							c.description as color, az.name as size, pi.OrderId as InstructionOrderId  
					FROM (`order` o, orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az, variantcode vc)
					LEFT JOIN paymentterm pt ON pt.id=o.paymenttermid
					LEFT JOIN pickinstruction pi ON pi.customerid=o.companyid and pi.seasonid=o.seasonid
					LEFT JOIN Company Customer ON Customer.id=o.companyid
					WHERE ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and ol.done=0 and oq.active=1
					and o.done=0 and o.ready=1
					and ol.articlecolorid=ac.id and  ac.colorid=c.id and oq.articlesizeid=az.id
					and ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid and vc.active=1
					and o.tocompanyid=%d and o.consolidatedid in (%s) and Customer.IncludeBatchPick=1", $Record['CompanyId'], $_consolidated['consolidatedIds']) ;
	$query .= $DeliveryDateLimit . " and not pt.prepay>0 ORDER BY o.ConsolidatedId, o.id" ;
	// Add to skip orders with open pickorders.
	//           LEFT JOIN pickorder p ON p.referenceid=o.id  and p.OwnerCompanyId=787 and p.active=1 and p.packed=0
	//			 and isnull(p.id)
	
	//	return sprintf ("query -%s-",$query) ;
	$res = dbQuery ($query) ;
	
	$CurrentOrderId = 0 ;
	while ($row = dbFetch ($res)) {
		// Check if collectionmember is cancelled
	    if ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['Cancel']) continue ;
	
	    // Check if Collection member is moved to a LOT later than date range
	    if (($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > '2001-01-01') AND
			($MaxDeliveryDate > '2001-01-01') AND 
	        ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['LotDate'] > $MaxDeliveryDate)) continue ;

	   // Check if some quantity is already delivered.
//	   $Query = sprintf ("SELECT sum(quantity) as Qty from item where orderlineid=%d and articlesizeid=%d", $row['OrderLineId'],$row['articlesizeid']) ;
	   $query = sprintf ("SELECT sum(if(i.credit=1,-iq.quantity,iq.quantity)) as Qty from invoiceline il, invoicequantity iq, invoice i where il.orderlineid=%d and il.id=iq.invoicelineid and articlesizeid=%d and il.invoiceid=i.id", $row['OrderLineId'],$row['articlesizeid']) ;
	   $itemres = dbQuery ($query) ;
	   $itemqty = dbFetch ($itemres) ;
	   if ((int)$itemqty['Qty'] >= $row['quantity']) continue ;
         $itemqty['Qty'] = ((int)$itemqty['Qty']) > 0 ? ((int)$itemqty['Qty']) : 0 ;
	   $PickQty =  $row['quantity'] - (int)$itemqty['Qty'] ; 
	    
		// Update or insert into PickOrder table.
		if (($CurrentOrderId <> $row['ConsolidatedId']) or ($PickOrder['DeliveryDate'] < $row['DeliveryDate'])) {
			if ($CurrentOrderId>0) {
				CreatePickConsolidatedCSV($CurrentOrderId) ;
			}
 			// Initialize
			$PickOrder = array (	
				'Type'				=>  "SalesOrder",			// Pick order from internal customer
				'Reference'			=>  0,			
				'FromId'			=> $Record['PickStockId'],		
				'CompanyId'			=>  0,						// Shop Code	
				'OwnerCompanyId'	=>  $Record['OwnerId'],		
				'HandlerCompanyId'	=>  $_POST['HandlerId'],					
				'Instructions'		=>  "",		
				'Packed'				=>  0,
				'DeliveryDate'		=>  ""						// Expected pick and pack complete
			) ;
				// Generate PickOrders
			$PickOrder['ConsolidatedId'] = $row['ConsolidatedId'] ;

			$PickOrder['Reference'] = $row['OrderId'] ;
			$PickOrder['ReferenceId'] = $row['OrderId'] ;
			$PickOrder['CompanyId'] = $row['CompanyId'] ;
			$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
			$PickOrder['Instructions'] = $row['Instructions'] ;
			if (($row['CompanyId']==7547) or ($row['InstructionOrderId'] > 0)) { // Dont add if already generated once or for Private customers
				$_donothing = 1 ;
			} else {
				$PickOrder['Instructions'] = $PickOrder['Instructions'] . ' - ' . $_POST['Instructions'] ;
				$PickInstructions = array (	
					'CustomerId'		=>  (int)$row['CompanyId'],
					'SeasonId'			=>  (int)$row['SeasonId'],
					'OrderId'			=>  (int)$row['OrderId']			
				) ;
				tableWrite ('PickInstruction', $PickInstructions) ;
			}
		
			$CurrentOrderId = $row['ConsolidatedId'] ;
			$whereclause = sprintf ("ConsolidatedId='%s' and OwnerCompanyId=%d and active=1 and packed=0", $PickOrder['ConsolidatedId'], $PickOrder['OwnerCompanyId']) ;
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		}
		
		// Update or insert into PickOrderLine table.
 		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $PickQty ;
		$PickOrderLine['VariantDescription'] = substr($row['ArticleDescription'],0,48) ;
		$PickOrderLine['VariantColor'] = $row['color'] ;
		$PickOrderLine['VariantSize'] = $row['size'] ;
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$PickOrderLine['BoxNumber'] = $row['BoxNumber'] ;
		$PickOrderLine['PrePackedQuantity'] = $row['QuantityReceived'] ;
		$PickOrderLine['QlId'] = $row['QlId'] ;
		
		// Assortments
	    if ($CollectionMembers[$row['articleid']][$row['articlecolorid']]['AssortmentId'] > 0) {
			$query = sprintf ("select count(*) as Cnt from (assortment c, collectionmemberassortment ca) where c.id=%d and ca.assortmentid=c.id and c.active=1 and ca.active=1", (int)$CollectionMembers[$row['articleid']][$row['articlecolorid']]['AssortmentId']) ;
			$res2 = dbQuery ($query) ;
			$_assortcnt = dbFetch ($res2) ;
			dbQueryFree ($res2) ;
			$query = sprintf ("select count(*) as Cnt from orderquantity where orderlineid=%d and quantity>0 and active=1", (int)$row['OrderLineId']) ;
			$res3 = dbQuery ($query) ;
			$_qtycnt = dbFetch ($res3) ;
			dbQueryFree ($res3) ;
			
			If ($_assortcnt['Cnt']>0 and $_assortcnt['Cnt']==$_qtycnt['Cnt']) {
				$PickOrderLine['AssortVariantCodeId'] =	(int)tableGetFieldWhere('VariantCode','Id','Type="assort" AND Reference='.(int)$CollectionMembers[$row['articleid']][$row['articlecolorid']]['AssortmentId']. ' AND ACTIVE=1') ;
				$PickOrderLine['AssortVariantCode'] = tableGetFieldWhere('VariantCode','VariantCode','Type="assort" AND Reference='.(int)$CollectionMembers[$row['articleid']][$row['articlecolorid']]['AssortmentId']. ' AND ACTIVE=1') ;	
				$PickOrderLine['AssortQuantity'] = (int)tableGetFieldWhere('collectionmemberassortment','Quantity','Active=1 AND AssortmentId='.(int)$CollectionMembers[$row['articleid']][$row['articlecolorid']]['AssortmentId'].' AND '.'ArticleSizeId='.(int)$row['articlesizeid']) ;
			} else {
				$PickOrderLine['AssortVariantCodeId'] =	0 ;
				$PickOrderLine['AssortVariantCode'] = '' ;	
				$PickOrderLine['AssortQuantity'] = 0 ;
			}
		}
		$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d and QlId=%d", $PickOrderLine['VariantCodeId'], $PickOrderId, $row['QlId']) ;
		$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;
    }
    dbQueryFree ($res) ;
	if ($CurrentOrderId>0) {
		CreatePickConsolidatedCSV($CurrentOrderId) ;
	}
	
    return 0 ;
?>
