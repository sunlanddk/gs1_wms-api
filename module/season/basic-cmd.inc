<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
		'Name'				=> array ('check' => true),
		'CompanyId'		=> array ('type' => 'integer',				'check' => true),
		'OwnerId'		=> array ('type' => 'integer',			'check' => true),
		'Description'		=> array ('check' => true),
//		'ExplicitMembers'		=> array ('type' => 'checkbox','check' => true),
		'Done'		=> array ('type' => 'checkbox','check' => true),
		'ShowOnBudget'		=> array ('type' => 'checkbox','check' => true),
		'ShowOnB2B'		=> array ('type' => 'checkbox','check' => true),
		'PickStockId'		=> array ('type' => 'integer',	'mandatory' => true),
		'FromStockId'		=> array ('type' => 'integer',	'mandatory' => true),
		'CollectionTypeId'		=> array ('type' => 'integer',	'mandatory' => true),
		'SeasonDisplayOrder'		=> array ('check' => true),
		'BaseVarCode'		=> array ('check' => true),
		'RestrictAccess'		=> array ('type' => 'checkbox','check' => true),
		'RestrictAssort'		=> array ('type' => 'checkbox','check' => true),
		'InitialPickInstructions'				=> array ('check' => true),
		'WebshopId'		=> array ('type' => set)
   ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'SeasonDisplayOrder' :
				if (!$changed) return false ;
				break ;
			case 'BaseVarCode' :
				if (!$changed) return false ;
				if (strlen ($value) <> 7 and strlen ($value) <> 0) return sprintf ('Invalid string length %d of basecode (Only Blank or 7 digits allowed)', strlen ($value)) ;
				break ;
	//	    case 'ExplicitMembers':
			case 'Done':
			case 'ShowOnBudget':
			case 'ShowOnB2B':
			case 'RestrictAccess':
			case 'RestrictAssort':
			// Ignore if not setting flag
			if (!$changed) return false ;
			return true ;
			case 'PickStockId':
			case 'FromStockId':
			case 'Name' :
			case 'Description' :
				if (!$changed) return false ;
				break ;
			case 'CompanyId' :
			if ($Navigation['Parameters'] == 'new') {
				if ($value == 0) return 'please select Customer' ;
				if (!$changed) return false ;
			}
			break ;
			case 'OwnerId' :
			if ($value == 0) return 'please select Owner Company' ;
			if (!$changed) return false ;
		}

		if (!$changed) return false ;
		return true ;	
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;

	default :
	    break ;
    }
   	if (empty($_POST['LotName'][1])) {
		return 'Season LOT in first LOT field is mandatory' ;
	}
 
	if (($_POST['ShowOnWebshop'])=='on') {
		$fields['WebshopId']['value'] = 3 ;
	} else {
		$fields['WebshopId']['value'] = 0 ;
	}
    $res = saveFields ('Season', $Id, $fields, true) ;
    if ($res) return $res ;

	for ($i=1; $i<9; $i++)  {
		$LotId = (int)$_POST['LotId'][$i] ;
		if (!empty($_POST['LotName'][$i])) {
			$CollectionLot['SeasonId'] = (int)$Record['Id']>0?(int)$Record['Id']:$Id ;
			$CollectionLot['Name'] = $_POST['LotName'][$i] ;
			$CollectionLot['Date'] = $_POST['Date'][$i] ;
			$CollectionLot['PreSale'] = ($_POST['PreSale'][$i] == 'on') ? 1 : 0 ;
			$CollectionLot['NoRetail'] = ($_POST['NoRetail'][$i] == 'on') ? 1 : 0 ;

			$ColletionLotId=tableWrite ('CollectionLot', $CollectionLot, $LotId) ;
		} else {
			if ($LotId>0) { // Name has been deleted so remove element.
				tableDelete ('CollectionLot', $LotId) ;
			}
		}
	}
	
    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Season
	    return navigationCommandMark ('seasonview', (int)$Record['Id']) ;
    }


    return 0 ;    
?>
