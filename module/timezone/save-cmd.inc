<?php

    require "lib/save.inc" ;

    $fields = array (
	"Name"		=> array ("mandatory" => true,	"permission" => "admin"),
	"Zone1"		=> array ("permission" => "admin"),
	"Zone2"		=> array ("permission" => "admin"),
    ) ;
  
    return saveFields ("TimeZone", $Id, $fields) ;
?>
