<?php

    require_once "lib/html.inc" ;

    unset ($row) ;

    switch ($Navigation["Parameters"]) {
	case "new":
	    $Id = -1 ;
	    break ;

	case "timezoneid" :
	    break ;
	
	default:
	    return sprintf ("%s(%d), invalid parameter, value '%s'", __FILE__, __LINE__, $Navigation["Parameters"]) ;
    }

    // Get record
    if ($Id > 0) {
	$query = sprintf ("SELECT TimeZone.* FROM TimeZone WHERE TimeZone.Active=1 AND TimeZone.Id=%d", $Id) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if (!$row) return "Record does not exist" ;
    }
    
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td width=80><p>Timezone</p></td><td><input type=text name=Name size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Name"])) ;
    printf ("<tr><td>&nbsp;</td></tr>\n") ;
    printf ("<tr><td><p>Zone1</p></td><td><input type=text name=Zone1 size=10 value=\"%s\"></td></tr>\n", htmlentities($row["Zone1"])) ;
    printf ("<tr><td><p>Zone2</p></td><td><input type=text name=Zone2 size=10 value=\"%s\"></td></tr>\n", htmlentities($row["Zone2"])) ;
    print htmlItemInfo ($row) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
