<?php

    $query = sprintf ("SELECT * FROM TimeZone WHERE Active=1 ORDER BY Name") ;
    $result = dbQuery ($query) ;
    if (dbNumRows($result) == 0) return "No timezones available" ;
        
    require "lib/dropmenu.inc" ;
    
    printf ("<table width=100%% style='table-layout:fixed;'>\n") ;
    printf ("<tr>%s<td class=listhead width=150><p class=listhead>Timezone</p></td><td class=listhead width=60 style='text-align:right;'><p class=listhead>NT diff</p></td><td class=listhead width=60 style='text-align:right;'><p class=listhead>ST diff</p></td><td class=listhead>&nbsp;</td></tr>\n", dropmenuTDHeader()) ;
    while ($row = dbFetch($result)) {
	printf ("<tr>%s<td><p class=list>%s</p></td><td><p class=list style='text-align:right;'>%s</p></td><td style='text-align:right;'><p class=list>%s</p></td></tr>\n", dropmenuTDList($row["Id"]), htmlentities($row["Name"]), htmlentities($row["Zone1"]), htmlentities($row["Zone2"])) ;
    }
    printf ("</table>\n") ;

    dbQueryFree ($result) ;

    return 0 ;
?>
