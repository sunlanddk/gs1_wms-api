<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/select.inc' ;

    // News
    $query = sprintf ("SELECT COUNT(Id) FROM News WHERE News.CreateDate>'%s' AND News.Active=1 GROUP BY Id", $User['LastNewsDate']) ;
    $result = dbQuery ($query) ;
    $count=dbNumRows ($result) ;
    dbQueryFree ($result) ;
    printf ("<td class=barfield style='color:#7f7c73;'>News</td>\n<td class=barfield style='padding-left:0px;%s' %s>%s new item%s</td>\n", ($count > 0) ? 'color:#0000ff;' : '', ($count > 0) ? navigationOnClickMark('homenew') : '', ($count > 0) ? $count : 'No', ($count != 1) ? 's' : '') ;
    printf ("<td class=barfield><div class=barsplit></div></td>\n") ;

    // Selected item
    $s = '' ;
    switch (selectGetType()) {
	case 'style' :
	    // Style
	    $i = selectGetId () ;
	    $query = sprintf ('SELECT Article.Number, Style.Version FROM Style LEFT JOIN Article ON Article.Id=Style.ArticleId WHERE Style.Id=%d AND Style.Active=1 AND Article.Active=1', $i) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (!row) break ;
	    $n = navigationOnClickMark('styleview', $i) ;
	    $s = sprintf ('Style %s-%d', $row['Number'], (int)$row['Version']) ;
	    break ;
	    
	case 'article' :
	    // Style
	    $i = selectGetId () ;
	    $query = sprintf ('SELECT Article.Number FROM Article WHERE Article.Id=%d AND Article.Active=1', $i) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (!row) break ;
	    $n = navigationOnClickMark('articleview', $i) ;
	    $s = sprintf ('Article %s', $row['Number']) ;
	    break ;
    }
    if ($s != '') {
	printf ("<td class=barfield style='color:#7f7c73;'>Selected</td>\n<td class=barfield style='padding-left:0px' %s>%s</td>\n", $n, $s) ;
	printf ("<td class=barfield><div class=barsplit></div></td>\n") ;
    }
    
    return 0 ;
?>
