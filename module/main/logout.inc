<?php

require_once "lib/log.inc" ;

// Terminate any active logins at this session
$query = sprintf ("SELECT Id FROM Login WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
$result = dbQuery ($query) ;
while ($row = dbFetch ($result)) {
    $query = sprintf ("UPDATE Login SET Active=0, LogoutDate='%s' WHERE Id=%d", dbDateEncode(time()), $row["Id"]) ;
    dbQuery ($query) ;
    logPrintf (logINFO, "logout, LoginId %d last access: %s, %s", $row["Id"], $Login["AccessDate"],$Login["Id"]) ; 
}
dbQueryFree ($result) ;

return 0 ;
?>
