<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'module/color/include.inc' ;
    require_once 'lib/uts.inc' ;

    $ColorCreated = false ;
    
    $fields = array (
	'Number'		=> array ('mandatory' => true,	'check' => true),
//	'ColorId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Reference'		=> array (),
	'Comment'		=> array (),
	'PriceAdjustPercentage'	=> array ('type' => 'decimal',	'format' => '4.1'),
	'HeavyMetals'		=> array ('type' => 'checkbox'),
	'ColorId'		=> array ('type' => 'set'),
	'OrderedDate'		=> array ('type' => 'date',				'check' => true),
	'ReceivedDate'		=> array ('type' => 'date',				'check' => true),
	'RejectedDate'		=> array ('type' => 'date',				'check' => true),
	'ApprovedDate'		=> array ('type' => 'date',				'check' => true),
	'CustomerCompanyId'	=> array ('type' => 'integer'),
	'SalesUserId'		=> array ('type' => 'integer'),
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ArticleId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }
    $ReceivedDateChanged = false ;
   
    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $fields, $ColorCreated, $ReceivedDateChanged ;
	switch ($fieldname) {
	    case 'OrderedDate':
	    case 'ApprovedDate':
		if (!$changed) return false ;
		return true ;

		case 'ReceivedDate':
		if (!$changed) return false ;
		if ($fields['ReceivedDate']['value']) {
 	    	$ReceivedDateChanged = true ;
	   		$_POST['RejectedDate'] = '' ;
		}
 	    return true ;
			
	    case 'RejectedDate':
		if ($ReceivedDateChanged) {
	   		$_POST['RejectedDate'] = '' ;
	   		return true ;
		}
	    if (!$changed) return false ;
		return true ;
	
	    case 'ColorId':
		if (!$changed) return false ;
	
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ArticleColor WHERE ArticleId=%d AND Active=1 AND ColorId=%d AND Id<>%d', $Record['ArticleId'], $value, $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Color allready existing' ;

		return true ;

	    case 'Number':
		if (!$changed) return false ;

		// Fill out color number
		$r = colorNumberFill ($value) ;
		if (is_string($r)) return $r ;

		// Locate the color number
		$query = sprintf ('SELECT Id FROM Color WHERE Number="%s" AND Active=1', addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$ColorId = (int)$row['Id'] ;
		if ($ColorId <= 0) {
		    // Color not found
		    // Create it 
		    $row = array ('Number' => $value) ;
		    $ColorId = tableWrite ('Color', $row) ;

		    $ColorCreated = true ;
		    
		} else {
		    // Color found
		    // Check that name does not allready exist
		    $query = sprintf ('SELECT Id FROM ArticleColor WHERE ArticleId=%d AND Active=1 AND ColorId=%d AND Id<>%d', $Record['ArticleId'], $ColorId, $Id) ;
		    $result = dbQuery ($query) ;
		    $count = dbNumRows ($result) ;
		    dbQueryFree ($result) ;
		    if ($count > 0) return 'Color allready existing' ;
		}
		
		// Set new color
		$fields['ColorId']['value'] = $ColorId ;
		return false ;
	}
	return false ;
    }

    $r = saveFields ('ArticleColor', $Id, $fields, true) ;
    if ($r) return $r ;
    
    if ($ColorCreated) {
	// View color created
	return navigationCommandMark ('coloredit', $fields['ColorId']['value']) ;
    }

    return 0 ;
?>
