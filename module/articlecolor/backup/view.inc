<?php

//    require_once 'lib/navigation.inc' ;
//    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;

    // Article header
    itemStart () ;
    itemHeader () ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemField ('Supplier', sprintf ('%s (%s)', $Record['SupplierCompanyName'], $Record['SupplierCompanyNumber'])) ;
    itemField ('Reference', $Record['SupplierNumber']) ;
    if ($Record['PrintNumber']!='')
     itemField ('Print number', $Record['PrintNumber']) ;
    itemSpace () ;
    itemField ('Colour', sprintf ('%s, %s', $Record['Number'], $Record['Description'])) ;
    itemSpace () ;
	if ($Record['TypeFabric']) {
	    itemField ('Customer', tableGetField('Company', 'Name', (int)$Record['CustomerCompanyId'])) ;
	    itemField ('Sales Ref', tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$Record['SalesUserId'])) ;
	    itemSpace () ;
	    ItemField ('Lab dye dates:', '') ;  
	    itemField ('Ordered', $Record['OrderedDate']) ;
	    itemField ('Received', $Record['ReceivedDate']) ;
	    itemField ('Rejected', $Record['RejectedDate']) ;
	    itemField ('Approved', $Record['ApprovedDate']) ;
	    itemSpace () ;
	}
    itemField ('Supplier Ref', $Record['Reference']) ;
    itemField ('Price Adjust', $Record['PriceAdjustPercentage'] . ' %') ;
    itemField ('Heavy Metals', ($Record['HeavyMetals']) ? 'no' : 'yes') ;
    itemSpace () ;
    itemFieldText ('Comment', $Record['Comment']) ;
    itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    // Color Line List
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Line', 35) ;
    listHead ('Description', 130) ;
    listHead ('Number', 75) ;
    listHead ('', 30) ;
    listHead ('') ;
    for ($n = 0 ; true ; $n++) {
	// Line existing
	$ColorId = $Record[sprintf('Line%02dColorId', $n)] ;
	if (!isset($ColorId)) break ;

	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'line', $Record['Id'], 'line='.$n) ;
	listField ((int)$n) ;
	listField ($Record[sprintf('Line%02dDescription', $n)]) ;	

	if ($ColorId > 0) {
	    // Get record
	    $query = sprintf ("SELECT Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM Color LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId WHERE Color.Id=%d", $ColorId) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    listField ($row['Number']) ;
	    if ($row['ColorGroupId'] > 0) {
		listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	    } else {
		listField () ;
	    }
	    listField ($row['Description']) ;
	}
    }
    listEnd () ;

    return 0 ;
?>
