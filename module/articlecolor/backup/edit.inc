<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record) ;
	    $Record['PriceAdjustPercentage'] = 0 ;
	    $Record['ArticleId'] = $Id ;
	    break ;
    }
    $MyCompanyId = parameterGet ('CompanyMy') ;
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
//    itemFieldRaw ('Colour', formDBSelect ('ColorId', (int)$Record['ColorId'], sprintf ('SELECT Color.Id, CONCAT(Color.Number," ",Color.Description) AS Value FROM Color LEFT JOIN ArticleColor ON ArticleColor.ColorId=Color.Id AND ArticleColor.ArticleId=%d AND ArticleColor.Active=1 WHERE Color.Active=1 AND (ArticleColor.Id=%d OR (ArticleColor.Id IS NULL)) ORDER BY Color.Number', (int)$Record['ArticleId'], (int)$Record['Id']), 'width:250px;')) ;
    printf ("<tr><td class=itemlabel>Colour</td><td><input type=text name=Number maxlength=9 size=9' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    print htmlItemSpace() ;
    itemSpace () ;
	printf ("<tr><td class=itemlabel>Customer</td><td>%s</td></tr>\n", htmlDBSelect ("CustomerCompanyId style='width:250px'", $Record['CustomerCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 ORDER BY Value',$MyCompanyId))) ;  
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Lab dye dates:</td></tr>\n") ;  
    itemFieldRaw ('Ordered', formDate ('OrderedDate', dbDateDecode($Record['OrderedDate']))) ;
    itemFieldRaw ('Received', formDate ('ReceivedDate', dbDateDecode($Record['ReceivedDate']))) ;
    itemFieldRaw ('Rejected', formDate ('RejectedDate', dbDateDecode($Record['RejectedDate']))) ;
    itemFieldRaw ('Approved', formDate ('ApprovedDate', dbDateDecode($Record['ApprovedDate']))) ;
    printf ("<tr><td class=itemlabel>Supplier Ref</td><td><input type=text name=Reference maxlength=20 size=20' value=\"%s\"></td></tr>\n", htmlentities($Record['Reference'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Price Adjust</td><td><input type=text name=PriceAdjustPercentage maxlength=6 size=6' value=\"%s\"> %%</td></tr>\n", $Record['PriceAdjustPercentage']) ;
    printf ("<tr><td class=itemlabel>No Heavy Metals</td><td><input type=checkbox name=HeavyMetals %s></td></tr>\n", ($Record['HeavyMetals'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
