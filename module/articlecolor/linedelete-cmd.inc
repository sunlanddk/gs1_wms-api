<?php

    require_once 'lib/save.inc' ;

    // Get Line No
    $Line = (int)$_POST['line'] ;

    // Line existing
    $ColorId = $Record[sprintf('Line%02dColorId', $Line)] ;
    if (!isset($ColorId)) return sprintf ('%s(%s) invalid line, id %d, no %d', $Id, $Line) ;

    $fields = array (
	sprintf ('Line%02dColorId', $Line)	=> array ('type' => 'set', 'value' => 0),
	sprintf ('Line%02dDescription', $Line)	=> array ('type' => 'set', 'value' => '')
    ) ;

    return saveFields ('ArticleColor', $Id, $fields) ;
?>
