<?php

    require_once 'lib/table.inc' ;

    // Ensure ArticleColor not used in ProductionOrder
    $query = sprintf ('SELECT Id FROM ProductionQuantity WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
//return  $query ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in ProductionOrder' ;
 
    // Ensure ArticleColor not used in Order
    $query = sprintf ('SELECT Id FROM OrderLine WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in Order' ;

    // Ensure ArticleColor not used in Invoice
    $query = sprintf ('SELECT Id FROM InvoiceLine WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in Invoice' ;

    // Ensure ArticleColor not used for Items
    $query = sprintf ('SELECT Id FROM Item WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for Items' ;
    		   
    // Ensure ArticleColor not used for Purchase Orders
    $query = sprintf ('SELECT Id FROM requisitionline WHERE ArticleColorId=%d', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for Purchase Order' ;
    		   
    // Ensure ArticleColor not used for Material lists
    $query = sprintf ('SELECT Id FROM stylematerial WHERE MaterialArticleColorId=%d', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for Material list' ;
    		   
	// Ensure ArticleColor not used for stylematerialColorvariants
    $query = sprintf ('SELECT Id FROM stylematerialcolorvariation WHERE AltMaterialArticleColorId=%d or ArticleColorId=%d', $Record['Id'], $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for stylematerialcolorvariation' ;
    		   
	// Ensure ArticleColor not used for collectionmember lists
    $query = sprintf ('SELECT Id FROM collectionmember WHERE ArticleColorId=%d', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for collectionmember' ;
	
    // Ensure ArticleColor not used for variant lists
    $query = sprintf ('SELECT Id FROM variantcode WHERE ArticleColorId=%d', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for variantcode' ;
 
    tableDelete ('ArticleColor', $Id) ;

    return 0
?>
