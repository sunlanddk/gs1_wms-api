<?php

//    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
//	    return 0 ;
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Color.Number'),
		array ('name' => 'Description',		'db' => 'Color.Description'),
	    ) ;
	    $queryFields = 'ArticleColor.*, Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen' ;
	    $queryTables = sprintf ("(ArticleColor, Color) LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId", $Id) ;
	    $queryClause = sprintf ("ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId", $Id) ;	 
		$queryClause .= $LimitClause;   
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;



	    // Load Article (parent)
	    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    $res = listView ($Result, 'articlecolorview') ;
	    return $res ;


	    return 0 ;

	case 'approve' :
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'printorder' :
	case 'view' :
	case 'line' :
	case 'linesave-cmd' :
	case 'linedelete-cmd' :
	case 'delete-cmd' :
	    // Get record
	    $query = sprintf ("SELECT 
	    ArticleColor.*, Color.Number, Color.Description, Unit.Name As UnitName, Unit.Decimals as UnitDecimals,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen, 
	    Article.Number AS ArticleNumber, Article.SupplierNumber, Article.Description AS ArticleDescription, Article.PrintNumber AS PrintNumber, 
	    Article.Id as ArticleId,
	    ArticleType.Fabric AS TypeFabric, 
	    Company.Id as CompanyId, Company.Name AS SupplierCompanyName, Company.Number AS SupplierCompanyNumber 
FROM (ArticleColor, Color) 
LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId 
LEFT JOIN Article ON Article.Id=ArticleColor.ArticleId 
LEFT JOIN Company ON Company.Id=Article.SupplierCompanyId 
LEFT JOIN Unit ON Article.UnitId=Unit.id
LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE ArticleColor.Id=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    return 0 ;
    }

    return 0 ;
?>
