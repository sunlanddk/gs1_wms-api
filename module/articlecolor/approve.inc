<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

    $MyCompanyId = parameterGet ('CompanyMy') ;
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemField ('Supplier', sprintf ('%s (%s)', $Record['SupplierCompanyName'], $Record['SupplierCompanyNumber'])) ;
    itemField ('Reference', $Record['SupplierNumber']) ;
    if ($Record['PrintNumber']!='')
     itemField ('Print number', $Record['PrintNumber']) ;
    itemSpace () ;
    itemField ('Colour', sprintf ('%s, %s', $Record['Number'], $Record['Description'])) ;
    itemSpace () ;
	if ($Record['TypeFabric']) {
	    itemField ('Customer', tableGetField('Company', 'Name', (int)$Record['CustomerCompanyId'])) ;
	    itemField ('Sales Ref', tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$Record['SalesUserId'])) ;
	    itemSpace () ;
	}
//	return dbDateDecode($Record['ReceivedDate']) ;
    itemFieldRaw ('Received', formDate ('ReceivedDate', $Record['ReceivedDate']>'2001-02-01'?dbDateDecode($Record['ReceivedDate']):'')) ;
    itemSpace () ;
    itemFieldRaw ('Approved', formDate ('ApprovedDate', $Record['ApprovedDate']>'2001-02-01'?dbDateDecode($Record['ApprovedDate']):'')) ;
	if ($Record['ApprovedUserId']>0)
	    itemField ('By:', tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ApprovedUserId'])) ;
    itemSpace () ;
    flag ($Record, 'Archived') ;
    itemFieldRaw ('Sample Qty', formText ('Quantity', number_format(0, (int)$Record['UnitDecimals'], ',', ''), 14, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
