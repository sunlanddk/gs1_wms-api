<?php

    require_once "lib/navigation.inc" ;

//    $type2text = array ("m"=>"Menu", "d"=>"Drop", "t"=>"Top", "b"=>"But", "l"=>"Link") ;
    $type2icon = array (
       "m"=>"image/navigation/menu.gif",
       "d"=>"image/navigation/drop.gif",
       "t"=>"image/navigation/tool.gif",
       "l"=>"image/navigation/link.gif",
       "n"=>"image/navigation/tool.gif"
    );

    printf ("<td class=explorer>\n") ;
    printf ("<link rel='StyleSheet' href='layout/default/dtree.css' type='text/css'>\n") ;
    printf ("<script type='text/javascript' src='lib/dtree.js'></script>\n") ;

    // Generate object tree
    printf ("<script type='text/javascript'>\n") ;
    printf ("n = new dTree('n');\n") ;

    $icon = "image/navigation/page.gif" ;
    printf ("n.add(%d,%d,'%s','','%s');\n", 0, -1, "Page", $icon) ;

    $nid = navigationMark ("navigation") ;
    $n = 0 ;
    $query = sprintf ("SELECT * FROM Navigation WHERE Active=1 ORDER BY ParentId, RefType DESC, DisplayOrder") ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	if ($row["ParentId"] == 0 and $rootid == 0) $rootid = $row["Id"] ;
	$href = sprintf ("index.php?nid=%d&id=%d", $nid, $row["Id"]) ;
	$icon = $type2icon[$row["RefType"]] ;
	printf ("n.add(%d,%d,\"%s%s\",\"%s\",\"%s\");\n", $row["Id"], $row["ParentId"], htmlentities($row["Name"]), ($row["Mark"] != "") ? sprintf (" (%s)", htmlentities($row["Mark"])) : "", $href, $icon) ;
	$n++ ;
    }

    if ($n > 0) {
	// Output folder tree
	printf ("document.write(n);\n") ;

//printf ("document.write('<br>getSelected(): '+d.getSelected()+'<br>');\n") ;

	if (isset($_COOKIE["csn"])) {
	    if ($Id == -1) $Id = $_COOKIE["csn"] ;
	} else {
	    if ($rootid > 0) {
		$Id = $rootid ;
		printf ("n.selectNode(%d);\n", $Id) ;
	    } else {
		$Id = 0 ;
	    }
	}
    } else {
	// No folders available
	$Id = 0 ;
    }

    printf ("</script>\n") ;

    printf ("</td>\n") ;

    return 0 ;
?>
