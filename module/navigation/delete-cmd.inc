<?php

    require_once 'lib/table.inc' ;

    // Get the record
    $query = sprintf ("SELECT Id FROM Navigation WHERE Active=1 AND Id=%d", $Id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if (!$row) return "Record does not exist" ;   
    
    function navigationDelete ($id) {
	tableDelete ('Navigation', $id) ;

	// Find and delete childs
	$query = sprintf ("SELECT Id FROM Navigation WHERE Active=1 AND ParentId=%d", $id) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    navigationDelete ($row['Id']) ;
	} 
	dbQueryFree ($result) ;
    }

    navigationDelete ($Id) ;

    return 0
?>
