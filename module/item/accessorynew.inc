<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Initialization
    define ('DIMENSIONS', 8) ;
    
    // Validate Article
    if (!$Record['ArticleTypeMaterial']) return 'the Article must be of type Material' ;
    if ($Record['VariantDimension'] and $Record['VariantSize']) return 'the Article can not have both Size and Dimension variants' ;

    // Get variant information
    if ($Record['VariantSize']) {
	// Get sizes
	$query = sprintf ('SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name', (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	$Size = array() ;
	while ($row = dbFetch ($result)) {
	    $Size[(int)$row['Id']] = $row ;
	}
	dbQueryFree ($result) ;
	if (count($Size) <= 0) return 'no Sizes defined for Article' ;
    }

    // Form
    formStart () ;
 
    // Header fields
    itemStart () ;
    itemSpace () ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Unit', $Record['UnitName']) ;
    itemSpace () ;
    itemHeader () ;

    // Batch
    itemFieldRaw ('Batch', formText ('BatchNumber', '', 10)) ;
    itemSpace () ;

    // Variants
    $n = 0 ;
    if ($Record['VariantCertificate']) {
	$n++ ;
	itemFieldRaw ('Certificate', formDBSelect ('ArticleCertificateId', 0, sprintf ('SELECT ArticleCertificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Value', $Record['ArticleId']), 'width:250px', array (0 => 'none'))) ;
    }
    if ($Record['VariantSortation']) {
	$n++ ;
	itemFieldRaw ('Sortation', formText ('Sortation', 1, 1)) ; 
    }
    if ($n > 0) {
    	itemSpace() ;
    }

    // Stock and Container
    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemSpace () ;
    itemEnd () ;

    // Header
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('No.', 80) ;
    listHead ('Case', 55, 'align=right') ;
    listHead ('Prod.', 55, 'align=right') ;
    listHead ('Order', 95) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Type', 95) ;
    if ($Record['VariantColor']) listHead ('Colour', 175) ;
    listHead ('Quantity', 55) ;
    if ($Record['VariantSize']) {
	for ($n = 1 ; $n < count($Size) ; $n++) listHead ('', 55) ;
    } else if ($Record['VariantDimension']) {
	for ($n = 1 ; $n < DIMENSIONS ; $n++) listHead ('', 55) ;
    }
    listHead ('', 4) ;
    listHead ('Comment', 150) ;

    // Variant information
    if ($Record['VariantSize']) {
	listRow () ;
	listField ('Sizes', 'height=23') ;
	listField ('', 'colspan=5') ;
	if ($Record['VariantColor']) listField () ;
	foreach ($Size as $size) listField ($size['Name'], 'align=right') ;
    } else if ($Record['VariantDimension']) {
	listRow () ;
	listField ('Dimension', 'height=23') ;
	listField ('', 'colspan=5') ;
	if ($Record['VariantColor']) listField () ;
	for ($n = 0 ; $n < DIMENSIONS ; $n++) {
		if ($Record['Dimension'])
		listFieldRaw (formText (sprintf('Dimension[%d]', $n), number_format($Record['Dimension'], 2, ',', ''), 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
		else
		listFieldRaw (formText (sprintf('Dimension[%d]', $n), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	}
    }

    for ($l = 1 ; $l <= 20 ; $l++) {
	listRow () ;
	listField ($l) ;
	listFieldRaw (formText (sprintf('CaseId[%d]', $l), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	listFieldRaw (formText (sprintf('ProductionId[%d]', $l), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ; 
	listFieldRaw (formDBSelect (sprintf ('OrderLineId[%d]', $l), 0, sprintf ('SELECT OrderLine.Id, CONCAT(Order.Id,".",OrderLine.No) AS Value FROM OrderLine INNER JOIN `Order` ON Order.Id=OrderLine.OrderId AND Order.Ready=1 AND Order.Done=0 AND Order.Active=1 WHERE OrderLine.ArticleId=%d AND OrderLine.Active=1 ORDER BY Value', (int)$Record['ArticleId']), 'width:80px;')) ; 
	listFieldRaw (formText (sprintf('ContainerId[%d]', $l), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	listFieldRaw (formDBSelect (sprintf ('ContainerTypeId[%d]', $l), 0, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:80px;')) ;
	if ($Record['VariantColor']) {
	    listFieldRaw (formDBSelect (sprintf('ArticleColorId[%d]', $l), 0, sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number,", ",Color.Description) AS Value FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Value', $Record['ArticleId']), 'width:160px')) ; 
	}
	if ($Record['VariantSize']) {
	    foreach ($Size as $size) listFieldRaw (formText (sprintf('Quantity[%d][%d]', $l, (int)$size['Id']), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	} else if ($Record['VariantDimension']) {
	    for ($n = 0 ; $n < DIMENSIONS ; $n++) listFieldRaw (formText (sprintf('Quantity[%d][%d]', $l, $n), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	} else {
	    listFieldRaw (formText (sprintf('Quantity[%d]', $l), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	}
	listField () ;
	listFieldRaw (formText (sprintf ('Comment[%d]', $l), '', 100, 'width:100%;')) ;
    } 
   
    listEnd () ;
    formEnd () ;
    
    return 0 ;
?>
