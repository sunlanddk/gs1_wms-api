<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    unset ($Record['Id']) ;
	    unset ($Record['Comment']) ;
	    $Record['Sortation'] = 1 ;
	    break ;
    }
     
    // Header
    itemStart () ;
    itemSpace () ;
    if ((int)$Record['Id'] > 0) {
	itemField ('Item', (int)$Record['Id']) ;
    }
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;

	itemFieldRaw ('Variant', formDBSelect ('VariantCodeId', (int)$Record['VariantCodeId'], sprintf ('SELECT Variantcode.Id, CONCAT(Variantcode.Variantcode," (",Variantcode.VariantDescription,")") AS Value FROM Variantcode WHERE Active=1 ORDER BY Value'), 'width:200px')) ; 

    // Container
    itemFieldRaw ('Container', formText ('ContainerId', ((int)$Record['ContainerId'] > 0) ? (int)$Record['ContainerId'] : '', 8)) ; 
    itemSpace() ;
	
    // Quantity
    itemFieldRaw ('Quantity', formText ('Quantity', number_format($Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 14, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 
    itemSpace() ;

	// Stock Value
    itemFieldRaw ('Stock value', formText('Price', number_format($Record['Price'], 4, ',', ''), 14, 'text-align:right;') . ' Kr per ' . htmlentities($Record['UnitName'])) ; 
	

    // Variants
    $n = 0 ;
    if ($Record['VariantColor']) {
	$n++ ;
	itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Value', $Record['ArticleId']), 'width:200px')) ; 
    }
    if ($Record['VariantSize']) {
	$n++ ;
	itemFieldRaw ('Size', formDBSelect ('ArticleSizeId', (int)$Record['ArticleSizeId'], sprintf ('SELECT ArticleSize.Id, ArticleSize.Name AS Value FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', $Record['ArticleId']), 'width:70px')) ; 
    }
    if ($Record['VariantCertificate']) {
	$n++ ;
	itemFieldRaw ('Certificate', formDBSelect ('ArticleCertificateId', (int)$Record['ArticleCertificateId'], sprintf ('SELECT ArticleCertificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Value', $Record['ArticleId']), 'width:250px', array (0 => 'none'))) ;
    }
    if ($Record['VariantDimension']) {
	$n++ ;
	itemFieldRaw ('Dimension', formText ('Dimension', number_format($Record['Dimension'], 2, ',', ''), 14, 'text-align:right;') . ' cm') ; 
    }
    if ($Record['VariantSortation']) {
	$n++ ;
	itemFieldRaw ('Sortation', formText ('Sortation', (int)$Record['Sortation'], 1)) ; 
    }
    if ($n > 0) {
    	itemSpace() ;
    }

	// Requistion
	itemFieldRaw ('Requistion', formText ('RequisitionId', ((int)$Record['RequisitionId'] > 0) ? (int)$Record['RequisitionId'] : '', 6)) ; 
	itemFieldRaw ('Requistion Line', formText ('RequisitionLineId', ((int)$Record['RequisitionNo'] > 0) ? (int)$Record['RequisitionNo'] : '', 6)) ; 
	itemSpace() ;

    // Batch
//    if ((int)$Record['ArticleTypeFabric']) {
	itemFieldRaw ('Batch', formText ('BatchNumber', $Record['BatchNumber'], 20)) ; 
	itemFieldRaw ('Receival', formText ('ReceivalNumber', $Record['ReceivalNumber'], 10)) ; 
	itemSpace() ;
//    }

    // Case
    if ((int)$Record['ArticleTypeMaterial']) {
	itemFieldRaw ('Case', formText ('CaseId', ((int)$Record['CaseId'] > 0) ? (int)$Record['CaseId'] : '', 6)) ; 
	itemFieldRaw ('Production', formText ('ProductionId', ((int)$Record['ProductionId'] > 0) ? (int)tableGetField ('Production', 'Number', $Record['ProductionId']) : '', 6)) ; 
	itemSpace() ;
    }
    itemFieldRaw ('Reserved to', formDBSelect ('ReservationId', (int)$Record['ReservationId'], sprintf("Select 0 as Id, '  -- None --  ' as Value Union SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeCustomer=1 AND Company.Active=1 AND (Company.ToCompanyId=0 or Company.ToCompanyId=%s or %s=0) ORDER BY Value", $Record[OwnerId], $Record[OwnerId]), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Purchased to', formDBSelect ('PurchasedForId', (int)$Record['PurchasedForId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeCustomer=1 AND Company.Active=1 AND (Company.ToCompanyId=0 or Company.ToCompanyId=%s or %s=0) ORDER BY Value', $Record[OwnerId], $Record[OwnerId]), 'width:250px;')) ;  
    itemFieldRaw ('Owner Company', formDBSelect ('OwnerId', (int)$Record['OwnerId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;

    // Flag for tests
    if ((int)$Record['Id'] > 0 and (int)$Record['ArticleTypeTest']) {
	itemFieldRaw ('Test Done', formCheckbox ('TestDone', $Record['TestDone'])) ;
	itemSpace() ;
    }
    
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
