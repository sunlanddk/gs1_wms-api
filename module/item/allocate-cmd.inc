<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'lib/string.inc' ;

    // Initialization
    $Line = array () ;
    $Quantity = 0 ;

    // Validate Article
    if ($Record['VariantDimension'] and $Record['VariantSize']) return 'the Article can not have both Size and Dimension variants' ;
   
    // Get Fields
    $fields = array (
	'OrderLineId'		=> array ('type' => 'integer'),
	'CaseId'		=> array ('type' => 'integer'),
	'ProductionId'		=> array ('type' => 'integer')
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
//  logPrintVar ($fields, 'Fields') ;

    // Get parameters
    if ($fields['CaseId']['value'] > 0) {
	// Lookup Case
	$query = sprintf ('SELECT * FROM `Case` WHERE Id=%d AND Done=0 AND Active=1', $fields['CaseId']['value']) ;
	$result = dbQuery ($query) ;
	$Case = dbFetch ($result) ;
	dbQueryFree ($result) ;

	// Validate case
	if ((int)$Case['Id'] != $fields['CaseId']['value']) return sprintf ('Case %d not found2', $value) ;

	// OrderLine not allowed together with Case
	if ($fields['OrderLineId']['value'] > 0) return 'please select either OrderLine or Case' ;

	if ($fields['ProductionId']['value']) { 
	    // Get Production
	    $query = sprintf ('SELECT Production.* FROM Production WHERE Production.Id=%d AND Production.Active=1', $fields['ProductionId']['value']) ;
	    $result = dbQuery ($query) ;
	    $Production = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ((int)$Production['Id'] != $fields['ProductionId']['value']) return sprintf ('%s(%d) Production not found, id %d', __FILE__, __LINE__,  $fields['ProductionId']['value']) ;
	    if ((int)$Production['CaseId'] != (int)$Case['Id']) return sprintf ('%s(%d) Production mismatch with Case, id %d', __FILE__, __LINE__,  $fields['ProductionId']['value']) ;
	}
	
    } else if ($fields['OrderLineId']['value'] > 0) {
		// Lookup OrderLine
		$query = sprintf ('SELECT OrderLine.* FROM OrderLine INNER JOIN `Order` ON Order.Id=OrderLine.OrderId AND Order.Ready=1 AND Order.Done=0 AND Order.Active=1 WHERE OrderLine.Id=%d AND OrderLine.Active=1', $fields['OrderLineId']['value']) ;
		$result = dbQuery ($query) ;
		$OrderLine = dbFetch ($result) ;
		dbQueryFree ($result) ;

		// Validate OrderLine
		if ((int)$OrderLine['Id'] != $fields['OrderLineId']['value']) return sprintf ('OrderLine not found, id %d', $fields['OrderLineId']['value']) ;
		if ((int)$OrderLine['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('Article mismatch with orderline') ;

    } else return 'please select either OrderLine or Case' ;
    
    // Get each quantity
    $n = -1 ;
    if (!is_array($_POST['StockId'])) return 'no Items' ;
    foreach ($_POST['StockId'] as $l => $s) {
	$StockId = strInteger ($s) ;
	$BatchNumber = trim(stripslashes($_POST['BatchNumber'][$l])) ;
	if ($Record['VariantColor']) $ArticleColorId = strInteger ($_POST['ArticleColorId'][$l]) ;
	if ($Record['VariantCertificate']) $ArticleCertificateId = strInteger ($_POST['ArticleCertificateId'][$l]) ;
	if ($Record['VariantSortation']) $Sortation = strInteger ($_POST['Sortation'][$l]) ;
	
	if (!is_array($_POST['Quantity'][$l])) continue ;
	foreach ($_POST['Quantity'][$l] as $i => $s) {
	    $Quantity = strFloat ($s, (int)$Record['UnitDecimals']) ;
	    $QuantityAllocated = 0 ;
	    $QuantityFree = 0 ;
	    if ($Quantity === false) return sprintf ('invalid Quantity in line %d', $l) ;
	    if ($Quantity == 0) continue ;

	    // Lookup Items
	    $query = 'SELECT SUM(Item.Quantity) AS Quantity' ;
	    if ((int)$Case['Id'] > 0) $query .= ', IF(Item.CaseId=0,0,1) AS Allocated' ;
	    else $query .= ', IF(Item.OrderLineId=0,0,1) AS Allocated' ;
	    $query .= ' FROM Item' ;
	    $query .= ' INNER JOIN Container ON Container.Id=Item.ContainerId AND Container.Active=1' ;
	    $query .= sprintf (' INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Id=%d AND Stock.Active=1 AND Stock.Type="fixed"', $StockId) ;
	    $query .= sprintf (' WHERE Item.ArticleId=%d', (int)$Record['Id']) ;
	    if ($Record['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $ArticleColorId) ;
	    $query .= sprintf (' AND Item.BatchNumber="%s"', addslashes($BatchNumber)) ;
	    if ($Record['VariantCertificate']) $query .= sprintf (' AND Item.ArticleCertificateId=%d', $ArticleCertificateId) ;
	    if ($Record['VariantSortation']) $query .= sprintf (' AND Item.Sortation=%d', $Sortation) ;
	    if ($Record['VariantSize']) $query .= sprintf (' AND Item.ArticleSizeId=%d', $i) ;
	    if ($Record['VariantDimension']) $query .= sprintf (' AND Item.Dimension=%d', $i) ;
	    $query .= ' AND ((Item.CaseId=0 AND Item.OrderLineId=0 AND Item.ReservationId=0) ' ;
	    if ((int)$Case['Id'] > 0) 
			if ((int)$Production['Id'] > 0) {
				$query .= sprintf (' OR (Item.CaseId=%d AND Item.ProductionId=%d AND (Item.Reservationid=0 or Item.Reservationid=%d))', (int)$Case['Id'], (int)$Production['Id'], (int)$Case['CompanyId']) ;
			} else {
				$query .= sprintf (' OR (Item.CaseId=%d  AND Item.ProductionId<=0  AND (Item.Reservationid=0 or Item.Reservationid=%d))', (int)$Case['Id'], (int)$Case['CompanyId']) ;
			}
	    else 
			$query .= sprintf ('Item.OrderLineId=%d', (int)$OrderLine['Id']) ;
	    $query .= ') AND Item.Active=1' ;
	    $query .= ' GROUP BY Allocated' ;
// printf ("query %d.%d: %s<br>\n", $l, $i, $query) ;
	    $result = dbQuery ($query) ;
	    $count = dbNumRows ($result) ;
	    if ($count > 2) return sprintf  ('%s(%d) too many items, line %d, collumn %d, count %d', __FILE__, __LINE__, $l, $i, $count) ;
	    while ($row = dbFetch ($result)) {
// printf ("row: allocated %d, quantity %s<br>\n", (int)$row['Allocated'], number_format ((float)$row['Quantity'], (int)$Record['UnitDecimals'], ',', '.')) ;
		if ($row['Allocated']) {
		    $QuantityAllocated = (float)$row['Quantity'] ;
		} else {
		    $QuantityFree = (float)$row['Quantity'] ;
		}		
	    }
	    dbQueryFree ($result) ;
	    
	    // Validate new Quantity
	    if ($Quantity < $QuantityAllocated) return sprintf ('Quantity can not be less than allready allocated, line %d', $l) ;
	    if ($Quantity > $QuantityAllocated + $QuantityFree) return sprintf ('not enough free Quantity, line %d', $l) ;

	    // Save Line
	    $Line[++$n] = array (
		'Line' => $l,
		'Quantity' => $Quantity,
		'QuantityFree' => $QuantityFree,
		'QuantityAllocated' => $QuantityAllocated,
		'StockId' => $StockId,
		'ArticleColorId' => $ArticleColorId,
		'BatchNumber' => $BatchNumber,
		'ArticleCertificateId' => $ArticleCertificateId,
		'Sortation' => $Sortation,
		'ArticleSizeId' => $i,
		'Dimension' => $i
	    ) ;
	}
    }
    if (count($Line) == 0) return 'please specify new Quantities' ;
//logPrintVar ($Line, 'Line') ;

    // Do allocation
    foreach ($Line as $n => $line) {
	
	// Quantity to allocate
	$Quantity = $line['Quantity'] - $line['QuantityAllocated'] ;
	if ($Quantity == 0) continue ;	
// printf ("to allocate %s<br>\n", number_format ($Quantity, (int)$Record['UnitDecimals'], ',', '.')) ;

	// Get items to allocate
	$query = 'SELECT Item.*' ;
	$query .= ' FROM Item' ;
	$query .= ' INNER JOIN Container ON Container.Id=Item.ContainerId AND Container.Active=1' ;
	$query .= sprintf (' INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Id=%d AND Stock.Active=1 AND Stock.Type="fixed"', $line['StockId']) ;
	$query .= sprintf (' WHERE Item.ArticleId=%d', (int)$Record['Id']) ;
	if ($Record['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $line['ArticleColorId']) ;
	$query .= sprintf (' AND Item.BatchNumber="%s"', addslashes($line['BatchNumber'])) ;
	if ($Record['VariantCertificate']) $query .= sprintf (' AND Item.ArticleCertificateId=%d', $line['ArticleCertificateId']) ;
	if ($Record['VariantSortation']) $query .= sprintf (' AND Item.Sortation=%d', $line['Sortation']) ;
	if ($Record['VariantSize']) $query .= sprintf (' AND Item.ArticleSizeId=%d', $line['ArticleSizeId']) ;
	if ($Record['VariantDimension']) $query .= sprintf (' AND Item.Dimension=%d', $line['Dimension']) ;
	$query .= sprintf(' AND Item.CaseId=0 AND Item.OrderLineId=0 AND (Item.ReservationId=0 or Item.ReservationId=%d) AND Item.Active=1', $Case['CompanyId']) ;
	$query .= ' ORDER BY Item.Id' ;
// printf ("query %d: %s<br>\n", $n, $query) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
// printf ("item: id %d, quantity %s<br>\n", (int)$row['Id'], number_format ((float)$row['Quantity'], (int)$Record['UnitDecimals'], ',', '.')) ;
	    // More to allocate ?
	    if ($Quantity <= 0) break ;

	    if ($Quantity < (float)$row['Quantity']) {
		// Split Item and Finalize allocation
		// Reduce quantity for existing Item
		$Item = array (
		    'Quantity' => number_format ((float)$row['Quantity'] - $Quantity, (int)$Record['UnitDecimals'], '.', '')
		) ;
// logPrintVar ($Item, sprintf ('tableWrite (Item, %d)', (int)$row['Id'])) ;
	    	tableWrite ('Item', $Item, (int)$row['Id']) ;

		// Create new Item for the allocated Quantity
		$Item = $row ;
		$Item['Quantity'] = number_format ($Quantity, (int)$Record['UnitDecimals'], '.', '') ;
		$Item['ParentId'] = (int)$row['Id'] ;
		$Item['PreviousContainerId'] = 0 ;
		$Item['PreviousId'] = 0 ;
		$Item['ItemOperationId'] = 0 ;
		if ((int)$Case['Id'] > 0) {
		    $Item['CaseId'] = (int)$Case['Id'] ;
			if ((int)$Production['Id'] > 0) {
				$Item['ProductionId'] = (int)$Production['Id'] ;
			} else {
 				$Item['ProductionId'] = 0;
			}
		} else {
		    $Item['OrderLineId'] = (int)$OrderLine['Id'] ;
		}
		if ($row['TestDone']) {
		    $Item['TestDone'] = 0 ;
		    $Item['TestItemId'] = (int)$row['Id'] ;
		}
// logPrintVar ($Item, sprintf ('tableWrite (Item)', (int)$row['Id'])) ;
	    	tableWrite ('Item', $Item) ;

		// Remaining allocation
		$Quantity = 0 ;
		break ;
	    }
	
	    // Allocate the whole Item
	    if ((int)$Case['Id'] > 0) {
		$Item = array ('CaseId' => (int)$Case['Id']) ;
			if ((int)$Production['Id'] > 0) {
				$Item['ProductionId'] = (int)$Production['Id'] ;
			} else {
 				$Item['ProductionId'] = 0;
			}
	    } else {
		$Item = array ('OrderLineId' => (int)$OrderLine['Id']) ;
	    }
// logPrintVar ($Item, sprintf ('tableWrite (Item, %d)', (int)$row['Id'])) ;
	    tableWrite ('Item', $Item, (int)$row['Id']) ;

	    // Adjust Quantity remaining to be allocated
	    $Quantity -= (float)$row['Quantity'] ;
	}
	dbQueryFree ($result) ;

	// Validate that the whole Quantit has been allocated
	if ($Quantity > 0) return sprintf ('%s(%d) not the whole quantity could be allocated, remaining %s', __FILE__, __LINE__, number_format ($Quantity, (int)$Record['UnitDecimals'], ',', '.')) ;

    }
    
	if ($Navigation['Parameters'] == 'consume') {
    // Consume items ?
    if ((int)$Case['Id'] > 0 and (int)$Production['Id'] > 0) {
	// Create ItemOperation referance
	$ItemOperation = array (
	    'Description' => sprintf ('Consumption of Items for %d/%d', (int)$Case['Id'], (int)$Production['Number'])
	) ;
	$ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
	
	foreach ($Line as $n => $line) {   
	    // Get items to allocate
	    $query = 'SELECT Item.*' ;
	    $query .= ' FROM Item' ;
	    $query .= ' INNER JOIN Container ON Container.Id=Item.ContainerId AND Container.Active=1' ;
	    $query .= sprintf (' INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Id=%d AND Stock.Active=1 AND Stock.Type="fixed"', $line['StockId']) ;
	    $query .= sprintf (' WHERE Item.ArticleId=%d', (int)$Record['Id']) ;
	    if ($Record['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $line['ArticleColorId']) ;
	    $query .= sprintf (' AND Item.BatchNumber="%s"', addslashes($line['BatchNumber'])) ;
	    if ($Record['VariantCertificate']) $query .= sprintf (' AND Item.ArticleCertificateId=%d', $line['ArticleCertificateId']) ;
	    if ($Record['VariantSortation']) $query .= sprintf (' AND Item.Sortation=%d', $line['Sortation']) ;
	    if ($Record['VariantSize']) $query .= sprintf (' AND Item.ArticleSizeId=%d', $line['ArticleSizeId']) ;
	    if ($Record['VariantDimension']) $query .= sprintf (' AND Item.Dimension=%d', $line['Dimension']) ;
	    $query .= sprintf (' AND Item.CaseId=%d AND Item.Active=1', (int)$Case['Id']) ;
	    $query .= ' ORDER BY Item.Id' ;
// printf ("query %d: %s<br>\n", $n, $query) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
// printf ("item: id %d, quantity %s<br>\n", (int)$row['Id'], number_format ((float)$row['Quantity'], (int)$Record['UnitDecimals'], ',', '.')) ;
		// Consume
		$Item = array (
		    'ContainerId' => 0,
		    'PreviousContainerId' => (int)$row['ContainerId'],
		    'PreviousId' => 0,
		    'ItemOperationId' => $ItemOperation['Id'],
		    'CaseId' => 0,
		    'ConsumeProductionId' => (int)$Production['Id']
		) ;
// logPrintVar ($Item, sprintf ('tableWrite (Item, %d)', (int)$row['Id'])) ;
	    	tableWrite ('Item', $Item, (int)$row['Id']) ;
	    }
	    dbQueryFree ($result) ;
	}

	// View pick
	return navigationCommandMark ('itemlistoperation', $ItemOperation['Id']) ;
    }
	}   

    return 0 ;
?>
