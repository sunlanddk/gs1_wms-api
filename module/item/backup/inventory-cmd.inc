<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
	'Quantity'		=> array ('type' => 'decimal',	'check' => true),
	'BatchNumber'		=> array (),
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields, $Quantity ;
	switch ($fieldname) {
	    case 'Quantity':
		// Validate Quantity
//		if ($value == 0) return 'please specify Quantity' ;		
//		if ($value > $Quantity) return sprintf ('requested Quantity (%d) higher than Item quantity (%d)', $value, $Quantity) ;
		
		if ($value == $Quantity) {
		    // The whole Item is present
		    // Just leave original Quantity
		    return false ;    
		}

    	// Leave original Quantity and create additional item for surplus.
		if ($value > $Quantity) {
			return false ;
		} 
		// Set remaining Quantity in the parent Item and consume missing qty for loss
		return true ;
	}
		return true ;		
    }

    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
    
    // Is Item real
    if ($Record['ContainerId'] == 0) return sprintf ('%s(%d) Item not real, id %d', __FILE__, __LINE__, $Record['Id']) ;

    // Is it allowed operate Items on this Stock
//    if ($Record['StockType'] != 'fixed') return 'only Items on fixed Stock Location can be split' ;
		
    // Get Original Item quantity
    $Quantity = (float)$Record['Quantity'] ;
    
    // Save record
    $res = saveFields ('Item', $Id, $fields, true) ;
    if ($res) return $res ;

    // Check is the whole Quantity of the parent Item is left on stock!
    if ((float)$fields['Quantity']['value'] == $Quantity) return 0 ;
    
    // Create specific item for consumption to Production
    // Copy columns for Items
    unset ($item) ;
    $query = 'SHOW COLUMNS FROM Item' ;
    $result = dbQuery ($query) ;
    while ($column = dbFetch ($result)) {
        $field = $column['Field'] ;
        $item[$field] = $Record[$field] ;
    }
    dbQueryFree ($result) ;
	
    // Create surplus
    if ((float)$fields['Quantity']['value'] > $Quantity) {
        $Loss = -1 ;
	// Consume for loss
	} else {
        $Loss = 1 ;
	}
	$item['ContainerId'] = 0 ;
	$item['Quantity'] = number_format (($Quantity - $fields['Quantity']['value'])*$Loss, (int)$Record['UnitDecimals'], '.', '') ;
	$item['BatchNumber'] = $fields['BatchNumber']['value'] ;
	$item['ParentId'] = $Record['Id'] ;
	$item['PreviousContainerId'] = 0 ;
	$item['PreviousId'] = 0 ;
	$item['ItemOperationId'] = 0 ;
	
	if ($Record['TestDone']) {
		$item['TestDone'] = 0 ;
		$item['TestItemId'] = $Record['Id'] ;
	}
    // Create Consumption Item
    tableWrite ('Item', $item) ;
    
    return 0 ;    
?>
