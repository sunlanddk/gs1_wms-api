<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    // Outher table, 1st field
    printf ("<table class=item><tr><td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
    
    // Header
    itemStart () ;
    itemHeader ('Basic Info') ;
    itemField ('Item', (int)$Record['Id']) ;
    itemSpace () ;
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
    itemField ('Description', $Record['ArticleDescription']) ;
    itemSpace () ;

    // Quantity and variants
    itemField ('Quantity', number_format($Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ; 
    itemField ('Stock value', number_format($Record['Price'], 4, ',', '.') . ' Kr per ' . $Record['UnitName']) ; 
    itemSpace () ;
    $n = 0 ;    
    if ($Record['VariantColor']) {
	$n++ ;
	if ((int)$Record['ColorGroupId'] > 0) {
	    $color = sprintf ('<div style="width:80px;height:15px;margin-left:8px;margin-top:1px;background:#%02x%02x%02x;display:inline;"></div>', (int)$Record['ValueRed'], (int)$Record['ValueGreen'], (int)$Record['ValueBlue']) ;
	} else {
	    $color = '' ;
	}
	itemFieldRaw ('Colour', sprintf ('<p class=list style="display:inline;">%s (%s)</p>%s', $Record['ColorNumber'], $Record['ColorDescription'], $color)) ;
    }
    if ($Record['VariantSize']) {
	$n++ ;
	itemField ('Size', $Record['SizeName']) ;
    }
    if ($Record['VariantDimension']) {
	$n++ ;
	itemField ('Dimension', $Record['Dimension']) ;
    }
    if ($Record['VariantSortation']) {
	$n++ ;
	itemField ('Sortation', (int)$Record['Sortation']) ;
    }
    if ($Record['VariantCertificate']) {
	$n++ ;
	itemField ('Certificate', ($Record['ArticleCertificateId'] == 0) ? 'none' : $Record['CertificateTypeName']) ;
    }
    if ($n == 0) {
    	itemField ('Variants', 'None') ;
    }	
    itemSpace () ;

    // ProductionOrder
    if ($Record['ArticleTypeProduct']) {
	if ((int)$Record['FromProductionId'] > 0) {
	    itemFieldIcon ('Production',  tableGetField ('Production', 'CONCAT(Production.CaseId,"/",Production.Number)', (int)$Record['FromProductionId']), 'production.gif', 'productionview', (int)$Record['FromProductionId']) ;
	} else {
	    itemField ('Production', 'None') ;
	}
	itemSpace () ;
    }
    
    // Batch
//    if ($Record['ArticleTypeFabric']) {
    itemFieldIcon ('Req. Line', $Record['ReqLineName'], 'requisitionline.gif', 'requisitionlineview', (int)$Record['RequisitionLineId']) ;
	itemField ('Batch', $Record['BatchNumber']) ;
	itemField ('Receival', $Record['ReceivalNumber']) ;
	itemSpace () ;
//    }
    
    // Test
    if ($Record['TestDone']) {
	itemField ('Tests', date ('Y-m-d H:i:s', dbDateDecode($Record['TestDoneDate'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName,", (",Loginname,")")', (int)$Record['TestDoneUserId'])) ;
    } else {
	itemField ('Tests', 'No') ;
    }
    itemSpace () ;

    // Comment
    itemFieldText ('Comment', $Record['Comment']) ;
    itemInfo ($Record) ;
    itemEnd () ;

    // Outher table, 2nd field
    printf ("<br></td><td width=50%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;

    // Header
    itemStart () ;
    itemHeader ('Stock Info') ;
    
    if ((int)$Record['ContainerId'] == 0) { 
	if ((int)$Record['ConsumeProductionId'] > 0) {
		// Status 
		itemField ('Status', 'Consumed by Production') ;
		itemSpace () ;
		itemField ('Production', tableGetField ('Production', 'CONCAT(CaseId,"/",Number)', (int)$Record['ConsumeProductionId'])) ;
		itemField ('Arrived', $Record['ArrivedDate']>'2001-01-01' ? sprintf("%s (%s)", $Record['ArrivedDate'], $Record['TransportName']) : 'No') ;
       } else {
		itemField ('Status', 'Consumed by Inventory - ' . $Record['BatchNumber']) ;
	 }
    } else {
	switch ($Record['StockType']) {
	    case 'fixed' :
		itemField ('Status', 'On Stock') ;
		itemSpace () ;
		itemFieldIcon ('Stock', $Record['StockName'], 'stock.gif', 'stockview', $Record['StockId']) ;
		break ;

	    case 'transport' :
		itemField ('Status', 'On Stock') ;
		itemSpace () ;
		itemFieldIcon ('Transport', $Record['StockName'], 'transport.gif', 'transportview', $Record['StockId']) ;
		break ;

	    case 'shipment' :
		itemField ('Status', 'Shipped') ;
		itemSpace () ;
		itemFieldIcon ('Shipment', $Record['StockName'], 'shipment.gif', 'shipmentview', $Record['StockId']) ;
		break ;
	    default :
		itemField ('Stock', sprintf ('unknown, text "%s"', $Record['StockType'])) ;
		break ;
	}

	itemField ('Arrived', $Record['ArrivedDate']>'2001-01-01' ? sprintf("%s (%s)", $Record['ArrivedDate'], $Record['TransportName']) : 'No') ;


	// Container
	itemSpace () ;
	itemFieldIcon ('Container', (int)$Record['ContainerId'], 'container.gif', 'containerview', (int)$Record['ContainerId']) ; 
	itemField ('Type', $Record['ContainerTypeName']) ;
	itemField ('Description', $Record['ContainerDescription']) ;
     
	// Allocatation
	itemSpace () ;
	if ((int)$Record['PurchasedForId'] > 0) 
	    itemField ('Purchased for', tableGetField('Company','Name',$Record['PurchasedForId'])) ;
	if ((int)$Record['ReservationId'] > 0) 
	    itemField ('Reserved for', tableGetField('Company','Name',$Record['ReservationId']) .  '    (by "' . tableGetField('User','Loginname',$Record['ReservationUserId']) . '" at ' . $Record['ReservationDate'] . ')') ;

	if ((int)$Record['OrderLineId'] > 0) {
	    itemFieldIcon ('Allocation', sprintf ('Order %d,%d', (int)$Record['OrderId'], (int)$Record['OrderLineNo']), 'orderline.gif', 'orderlineview', (int)$Record['OrderLineId']) ;
	} else if ((int)$Record['CaseId'] > 0) {
	 	itemFieldIcon ('Allocation', sprintf ('Case %d', (int)$Record['CaseId']), 'case.gif', 'caseview', (int)$Record['CaseId']) ;
	 	if ((int)$Record['ProductionId']>0) {
		 	itemFieldIcon (' ', sprintf ('Prod %d', (int)tableGetField ('Production', 'Number', $Record['ProductionId'])), 'production.gif', 'productionview', (int)$Record['ProductionId']) ;			
		}
	} else {
	    itemField ('Allocation', 'None') ;
	}
    }
    itemSpace () ;
    itemField ('Owner', tableGetField('Company','Name',$Record['OwnerId'])) ;

    itemEnd () ;

    // Outher table, end
    printf ("<br></td></tr></table>\n") ;

    // test data
    if ($Record['ArticleTypeTest']) {
	// Outher table, 1st field
	printf ("<table class=item><tr><td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;

	itemStart () ;
	itemHeader('Tests General', 'Name', 'Value') ;
	itemField ('Width Usable', $Record['WidthUsable'] . ' cm') ;
	itemField ('Width Full', $Record['WidthFull'] . ' cm') ;
	itemSpace () ;
	itemField ('M2 Weight', $Record['M2Weight'] . ' g') ;
	itemSpace () ;
	itemField ('Shrink Wash', $Record['ShrinkageWashLength'] . ' % length') ;
	itemField ('', $Record['ShrinkageWashWidth'] . ' % width') ;
	itemField ('Shrink Work', $Record['ShrinkageWorkLength'] . ' % length') ;
	itemField ('', $Record['ShrinkageWorkWidth'] . ' % width') ;
	itemSpace () ;
	itemField ('Rubbing Dry', $Record['RubbingDry']) ;
	itemField ('Rubbing Wet', $Record['RubbingWet']) ;
	itemSpace () ;
	itemField ('Washinginstr.', $Record['WashingInstruction']) ;
    	itemEnd () ;

	// Outher table, 2nd field
	printf ("<br></td><td width=50%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;

	itemStart () ;
	itemHeader ('Tests Colorfastness', 'Type', 'Value') ;
	foreach (array('Water','Wash','Light','Perspiration','Chlor','Sea','Durability') as $name) {
	    itemField ($name, $Record['ColorFastness'.$name]) ;
	}
	itemEnd () ;

	// Outher table, end
	printf ("<br></td></tr></table>\n") ;
    }

    return 0 ;
?>
