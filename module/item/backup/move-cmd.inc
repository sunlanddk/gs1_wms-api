<?php

    require_once 'lib/save.inc' ;
    require_once 'module/container/include.inc' ;

    $fields = array (
	'ContainerId'		=> array ('type' => 'integer',	'check' => 'true'),
	'PreviousContainerId'	=> array ('type' => 'set'),
	'PickId'		=> array ('type' => 'set',	'value' => 0)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields ;
	switch ($fieldname) {
	    case 'ContainerId' :
		if ($value == 0) return 'please enter number for new Container' ;
		
		// Get Container
		$query = sprintf ('SELECT Container.*, Stock.Id AS StockId, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone FROM Container LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] == 0) return sprintf ('Container not found, id %d', $value) ;

	        // Is it allowed operate Items on this Stock
		if ($row['StockType'] != 'fixed') return 'Items can only be moved to Containers on fixed Stock Location' ;

		// Set the old container
		$fields['PreviousContainerId']['value'] = $Record['ContainerId'] ;

		return true ;
	}
    }
    
    // Is Item real
    if ($Record['ContainerId'] == 0) return 'pseudo Items can not be moved' ;

    // Is it allowed operate Items on this Stock
    if ($Record['StockType'] != 'fixed') return 'only Items on Stock Location can be moved' ;
		
    // Save record
    $res = saveFields ('Item', $Id, $fields) ;
    if ($res) return $res ;
    
    // Check if the container has become empty
    containerDeleteEmpty ((int)$Record['ContainerId']) ;

    return 0 ;
?>
