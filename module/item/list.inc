<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    // Filter Bar
    listFilterBar () ;

   function formCheckboxParam ($name, $value=0, $style='', $param) {
	$html = '<input type=checkbox name=' . $name ;
	if ($param != '') $html .= ' ' . $param ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	if ($value) $html .= ' checked' ;
	$html .= '>' ;
	return $html ;
    }

    
    // Page Header
    switch ($Navigation['Parameters']) {
	case 'article' :
	    formStart () ;
   printf ("<table class=item><tr>\n") ;
   printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription']), 'article.gif', 'articleview', $Record['Id']) ;
	    //   	    itemFieldRaw ('List grouped', formCheckboxParam ('ListGrouped', $_POST['ListGrouped'],'', 'onclick="document.appform.submit();"')) ;
	    	    itemEnd () ;
	    printf ("</td>") ;
	    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
   	    itemFieldRaw ('Incl Transports', formCheckboxParam ('ListTransports', $_POST['ListTransports'],'', 'onclick="document.appform.submit();"')) ;
   	    itemFieldRaw ('Incl Shipments', formCheckboxParam ('ListShipments', $_POST['ListShipments'],'', 'onclick="document.appform.submit();"')) ;
   	    itemFieldRaw ('Incl Consumed', formCheckboxParam ('ListConsumed', $_POST['ListConsumed'],'', 'onclick="document.appform.submit();"')) ;
	    itemEnd () ;
  	printf ("</td>") ;
    printf ("</tr></table>\n") ;
	    formEnd () ;
//	    printf ('<br>') ;
	    break ;

	case 'container' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Stock', $Record['StockName'], 'stock.gif', 'stockview', $Record['StockId']) ;
	    itemField ('Container', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'stock' :
	    itemStart () ;
	    itemSpace () ;
	    switch ($Record['Type']) {
		case 'fixed' :
		    itemFieldIcon ('Stock', $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
		    break ;

		case 'transport' :
		    itemFieldIcon ('Transport', $Record['Name'], 'transport.gif', 'transportview', $Record['Id']) ;
		    break ;

		case 'shipment' :
		    itemFieldIcon ('Shipment', $Record['Name'], 'shipment.gif', 'shipmentview', $Record['Id']) ;
		    break ;
	    }
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'case' :
	case 'producedcase' :
	case 'consumedcase' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Case', $Record['Id'], 'case.gif', 'caseview', $Record['Id']) ;
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'allocated' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Case', $Record['CaseId'], 'case.gif', 'caseview', $Record['CaseId']) ;
	    itemField ('Description', $Record["CaseDescription"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'produced' :
	case 'consumed' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Production', $Record['CaseId'].'/'.$Record['Number'], 'production.gif', 'productionview', $Record['Id']) ;
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;

	case 'orderline' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Customer', sprintf ('%s, %s', $Record['CompanyName'], $Record['CompanyNumber']), 'company.gif', 'companyview', $Record['CompanyId']) ;
	    itemFieldIcon ('Order', sprintf ('%d, %s', $Record['OrderId'], $Record['OrderDescription']), 'order.png', 'orderview', (int)$Record['OrderId']) ;
	    itemFieldIcon ('Line', sprintf ('%d, %s', $Record['No'], $Record['Description']), 'orderline.gif', 'orderlineview', $Record['Id']) ;
	    itemFieldIcon ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription']), 'article.gif', 'articleview', $Record['ArticleId']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ; 

	case 'order' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Customer', sprintf ('%s, %s', $Record['CompanyName'], $Record['CompanyNumber']), 'company.gif', 'companyview', $Record['CompanyId']) ;
	    itemFieldIcon ('Order', sprintf ('%d, %s', $Record['Id'], $Record['Description']), 'order.png', 'orderview', (int)$Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ; 
    }

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    if (!$HideArticle) {
	listHead ('Article', 75) ;
	listHead ('Description') ;
    }
    listHead ('Item', 60, 'align=right') ;
    listHead ('Colour', 90) ;
	listHead ('Description',100) ;
    listHead ('Size', 40) ;
    listHead ('Dim.', 40) ;
    listHead ('Sort', 25) ;
    listHead ('Certificate', 80) ;
    listHead ('Batch', 55) ;
    listHead ('Test', 50) ;
    if (!$HideProduced) {
	listHead ('Origin', 70) ;
    }
    if (!$HideAllocation) {
	listHead ('Reserved', 90) ;
	listHead ('Alloc.', 70) ;
	listHead ('State', 70) ;
    }
    if (!$HideStock) {
	listHead ('Stock', 100) ;
    }
    listHead ('MoveTo', 110) ;
    if (!$HideContainer) {
	listHead ('Cont.', 50, 'align=right') ;
	listHead ('Type', 50) ;
	listHead ('Pos.', 50) ;
    }
    listHead ('Qty', 50, 'align=right') ;
    listHead ('Unit', 40) ;
    listHead ('Value', 50) ;
    listHead ('Owner', 110) ;

    $ProdContainerId = (int)parameterGet ('ProdContainerId') ;
    $ReqContainerId = (int)parameterGet ('ReqContainerId') ;

	// List lines
    $total = array () ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	if (($row['ContainerId']==$ProdContainerId) or ($row['ContainerId']==$ReqContainerId))
		listFieldIcon ('reqitem.gif', 'itemview', $row['Id']) ;
	else if ($row['StockName']=='In Purchase')
		listFieldIcon ('quantity.gif', 'itemview', $row['Id']) ;
	else 
		listFieldIcon ('item.gif', 'itemview', $row['Id']) ;
	if (!$HideArticle) {
	    listField ($row['ArticleNumber']) ;			// Article Number
	    listField ($row['ArticleDescription']) ;		// Description
	}
	listField ($row['Id'], 'align=right') ;			// Id
	if ($row['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	    listField ($row['ColorDescription']) ;
	} else {
	    listField () ;
	    listField () ;
	}
	if ($row['VariantSize']) {
	    listField ($row['SizeName']) ;			// Size
	} else {
	    listField () ;
	}
	if ($row['VariantDimension']) {
	    listField ($row['Dimension']) ;		// Dimension
	} else {
	    listField () ;
	}
	if ($row['VariantSortation']) {
	    listField ((int)$row['Sortation']) ;		// Sort
	} else {
	    listField () ;
	}
	if ($row['VariantCertificate']) {
	    listField ($row['CertificateTypeName']) ;		// Certification
	} else {
	    listField () ;
	}
	listField ($row['BatchNumber']) ;			// Batch
	listField (($row['TestDone']) ? 'Yes' : 'No') ;		// Tests
	if (!$HideProduced) {
	    if ($row['Produced']>0) listField ($row['Produced']) ;
		else listField (($row['ReqLineName']>0) ? sprintf("R%s",$row['ReqLineName']) : '') ;
		;			
	}
	if (!$HideAllocation) {
	    if ($row['ReservationId']>0) listField (tableGetField('Company','Name',$row['ReservationId'])) ;			// Resservation
	    else listField ('') ;
	    listField ($row['Allocation']) ;			// Allocation
	    listField ($row['StateAlloc']) ;			// State Allocation
	}
	if (!$HideStock) {
	    listField ($row['StockName']) ;			// Stock
	}
	if ($row['StockId'] == $row['AltStockId'])
	    listField ('') ;			// Stock
	else
	    listField ($row['MoveToName']) ;			// Stock
	if (!$HideContainer) {
	    if ((int)$row['ContainerId'] > 0) {
		listField ((int)$row['ContainerId'], 'align=right') ;	// Container
		listField ($row['ContainerTypeName']) ;			// Container Type
		listField ($row['ContainerPosition']) ;			// Container Position
	    } else {
		listField ('', 'colspan=3') ;
	    }
	}
	listField (number_format($row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
	listField ($row['UnitName']) ;				// Unit
	listField (number_format($row['Price'], 4, ',', '.'), 'align=right') ;	// Stock unit value
//	listField ($row['Price'], 'align=right') ;				// Stock unit value
    listField ($row['Owner']) ;

	// Compute totals
	if ($n == 1) {
	    $total['Unit'] = $row['UnitName'] ;
	    $total['UnitDecimals'] = (int)$row['UnitDecimals'] ;
	} else {
	    if ($total['Unit'] != $row['UnitName']) $total['Unit'] = '' ;
	}
	$total['Quantity'] += $row['Quantity'] ;
    } 

    if (dbNumRows($Result) == 0) {
	// No Items found
	listRow () ;
	listField () ;
	listField ('No Items', 'colspan=2') ;
    } else {
	if ($total['Unit'] != '') {
	    // Total
	    listRow () ;
	    listField ('', 'style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideArticle) listField ('', 'colspan=2 style="border-top: 1px solid #cdcabb;"') ;
	    listField ('', 'colspan=8 style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideProduced) listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideAllocation) listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideAllocation) listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideStock) listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;
	    listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;
	    if (!$HideContainer) listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;
//	    listField ($total['Quantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
	    listField (number_format($total['Quantity'], $total['UnitDecimals'], ',', '.'), 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
	    listField ($total['Unit'], 'style="border-top: 1px solid #cdcabb;"') ;			// Unit
	    listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;

	}
    }
    dbQueryFree ($Result) ;
   
    listEnd () ;
 
    return 0 ;
?>
