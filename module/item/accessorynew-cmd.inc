<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'lib/string.inc' ;

    // Initialization
    $Line = array () ;
    $Quantity = 0 ;

    // Validate Article
    if (!$Record['ArticleTypeMaterial']) return 'the Article must be of type Material' ;
    if ($Record['VariantDimension'] and $Record['VariantSize']) return 'the Article can not have both Size and Dimension variants' ;
   
    // Get Fields
    $fields = array (
	'ArticleCertificateId'	=> array ('type' => 'integer'),
	'Sortation'		=> array ('type' => 'integer'),
	'BatchNumber'		=> array (),
	'StockId'		=> array ('type' => 'integer'),
	'Position'		=> array (),
    ) ;

    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;

    // Get Stock location
    if ($fields['StockId']['value'] == 0) {
	$Stock = array () ;
    } else {
	$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
	$result = dbQuery ($query) ;
	$Stock = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
	if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on specified Stock, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;
    }
//logPrintVar ($Stock, 'Stock') ;

    // Get Certificate
    if ($Record['VariantCertificate']) {
	if ($fields['ArticleCertificateId']['value'] > 0) {
	    // Get Article Certificate
	    $query = sprintf ('SELECT ArticleCertificate.* FROM ArticleCertificate WHERE ArticleCertificate.Id=%d AND ArticleCertificate.Active=1', $fields['ArticleCertificateId']['value']) ;
	    $result = dbQuery ($query) ;
	    $Certificate = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ((int)$Certificate['Id'] != $fields['ArticleCertificateId']['value']) return sprintf ('%s(%d) ArticleCertificate not found, id %d', __FILE__, __LINE__, $fields['ArticleCertificateId']['value']) ;
	    if ((int)$Certificate['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleCertificate, id %d', __FILE__, __LINE__, $fields['ArticleCertificateId']['value']) ;
	} else {
	    $Certificate = array ('Id' => 0) ;
	}
//logPrintVar ($Certificate, 'Certificate') ;
    }

    // Get Sortation
    if ($Record['VariantSortation']) {
	if ($fields['Sortation']['value'] < 1 or $fields['Sortation']['value'] > 3) return 'invalid Sortation' ;
    }

    // Get variant information
    if ($Record['VariantSize']) {
	// Get sizes
	$query = sprintf ('SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name', (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	$Size = array() ;
	while ($row = dbFetch ($result)) {
	    $Size[(int)$row['Id']] = $row ;
	}
	dbQueryFree ($result) ;
	if (count($Size) <= 0) return 'no Sizes defined for Article' ;
//logPrintVar ($Size, 'Size') ;
    } else if ($Record['VariantDimension']) {
	// Get Posted dimensions
	$Dimension = array() ;
	if (!is_array($_POST['Dimension'])) return sprintf ('%s(%d) no Dimension posted', __FILE__, __LINE__) ;
	foreach ($_POST['Dimension'] as $n => $s) {
		$value = strFloat ($s, 2) ;
	    if (is_null($value)) continue ; 
// ken allow decimals!	    if ($value === false) return sprintf ('invalid Dimension in collumn %d', $n+1) ;

	    // Allready specified ?
	    if (array_search ($value, $Dimension) !== false) return 'same Dimension specified twice' ;
	    
	    // Save
	    $Dimension[$n] = $value ;
	}
	if (count($Dimension) == 0) return 'please specify Dimensions' ;
//logPrintVar ($Dimension, 'Dimension') ;
    }
   
    // Get fields for each line
    if (!is_array($_POST['CaseId'])) return sprintf ('%s(%d) no CaseId posted', __FILE__, __LINE__) ;
    foreach ($_POST['CaseId'] as $n => $s) {
	$line = &$Line[$n] ;

	// CaseId
	$CaseId = strInteger ($s) ;
	if ($CaseId === false) return sprintf ('invalid Case number in line %d', $n) ;
	if (!is_null($CaseId)) {
	    // Lookup Case
	    $query = sprintf ('SELECT * FROM `Case` WHERE Id=%d AND Done=0 AND Active=1', $CaseId) ;
	    $result = dbQuery ($query) ;
	    $Case = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Validate case
	    if ((int)$Case['Id'] != $CaseId) return sprintf ('Case %d not found', $CaseId) ;

	    // Save
	    $line['CaseId'] = $CaseId ;
	    unset ($OrderLine) ;
	    
    	// Get Production 
		$value = strInteger ($_POST['ProductionId'][$n]) ;
		if ($value === false) return sprintf ('invalid Production number in line %d', $n) ;
		if (is_null($value)) {
		    unset ($Production) ;
		} else {
			$query = sprintf ("SELECT Id FROM Production WHERE Number=%d And CaseId=%d", $value, $CaseId) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
			dbQueryFree ($result) ;
	
	 		if ($row['Id'] <= 0)
				return sprintf ('Production %d not found on case %d', $value, $CaseId) ;

		    $line['ProductionId'] = $row[Id] ;
		}	    
    	}

	// OrderLine
	$value = strInteger ($_POST['OrderLineId'][$n]) ;
	if ($value === false) return sprintf ('invalid OrderLineId in line %d', $n) ;
	if ($value > 0) {
	    // Verify that CaseId hase been left blank
	    if ($CaseId > 0) return sprintf ('Case and Order can not be specified at the same time, line %d', $n) ;
	    
	    // Lookup OrderLine
	    $query = sprintf ('SELECT OrderLine.* FROM OrderLine INNER JOIN `Order` ON Order.Id=OrderLine.OrderId AND Order.Ready=1 AND Order.Done=0 AND Order.Active=1 WHERE OrderLine.Id=%d AND OrderLine.Active=1', $value) ;
	    $result = dbQuery ($query) ;
	    $OrderLine = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    // Validate OrderLine
	    if ((int)$OrderLine['Id'] != $value) return sprintf ('OrderLine not found, id %d, line %d', $value, $n) ;
	    if ((int)$OrderLine['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('Article mismatch with orderline, line %d', $n) ;

	    // Save
	    $line['OrderLineId'] = $value ;
	    unset ($Case) ;
    	}

	// Get Container
	$value = strInteger ($_POST['ContainerId'][$n]) ;
	if ($value === false) return sprintf ('invalid Container number in line %d', $n) ;
	if (is_null($value)) {
	    unset ($Container) ;
	} else {
	    // Lookup Container
	    $query = sprintf ('SELECT Container.*, Stock.Ready AS StockReady, Stock.Name AS StockName FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Active=1 WHERE Container.Id=%d AND Container.Active=1', $value) ;
	    $result = dbQuery ($query) ;
	    $Container = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Validate Container
	    if ((int)$Container['Id'] != $value) return sprintf ('Container number %d not found', $value) ;
	    if ($Container['Ready']) return sprintf ('Items can not be stored in Container %d at location %s', $value, $Container['StockName']) ;

	    // Save
	    $line['ContainerId'] = $value ;
	}

	// Get ContainerType
	$value = strInteger ($_POST['ContainerTypeId'][$n]) ;
	if ($value === false) return sprintf ('invalid ContainerType in line %d', $n) ;
	if ($value == 0) {
	    unset ($ContainerType) ;
	} else {
	    // Verify that ContainerId hase been left blank
	    if ((int)$Container['Id'] > 0) return sprintf ('Container number and ContainerType can not be specified at the same time, line %d', $n) ;
	    
	    // Lookup ContainerType
	    $query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $value) ;
	    $result = dbQuery ($query) ;
	    $ContainerType = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ((int)$ContainerType['Id'] != $value) return sprintf ('ContainerType not found, line %d', $n) ;

	    // Verify that Stock location has been selected
	    if ((int)$Stock['Id'] <= 0) return 'please select Stock location when creating new Containers' ;
	    
	    // Save
	    $line['ContainerType'] = $ContainerType ;
	}

	// Validate that a container has been specified for the first line
	if ($n == 1 and (int)$Container['Id'] == 0 and (int)$ContainerType['Id'] == 0) return 'please specify container for the first line' ;

	// Get Color
	if ($Record['VariantColor']) {
	    $value = strInteger ($_POST['ArticleColorId'][$n]) ;
	    if ($value === false) return sprintf ('invalid ArticleColorId, line %d', $n) ;
	    if ($value > 0) {
		$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id=%d AND ArticleColor.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$Color = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$Color['Id'] != $value) return sprintf ('%s(%d) ArticleColor not found, line %d', __FILE__, __LINE__, $n) ;
		if ((int)$Color['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('Article mismatch with Color, line %d', __FILE__, __LINE__, $n) ;

		// Save
		$line['ArticleColorId'] = $value ;
	    }

	    // First line must have color specified
	    if ($n == 1 and $value == 0) return 'please specify Colour for the first line' ;

	    // Validate color for associated OrderLine
	    if ((int)$OrderLine['Id'] > 0 and (int)$OrderLine['ArticleColorId'] != (int)$Color['Id']) return sprintf ('Colour mismatch with OrderLine %d.%d, line %d', (int)$OrderLine['OrderId'], (int)$OrderLine['No'], $n) ;
	}

	// Get Quantities
	if ($Record['VariantSize']) {
	    // By Sizes
	    $line['Quantity'] = array () ;
	    foreach ($Size as $size) {
		$value = strFloat ($_POST['Quantity'][$n][(int)$size['Id']], (int)$Record['UnitDecimals']) ;
		if (is_null ($value)) continue ;
		if ($value === false or $value == 0) return sprintf ('invalid Quantity, line %d, Size %s', $n, $size['Name']) ;
		
		// Save
		$line['Quantity'][(int)$size['Id']] = $value ;
		$Quantity += $value ;
	    }
	    
	} else if ($Record['VariantDimension']) {
	    // By Dimension
	    $line['Quantity'] = array () ;
	    foreach ($Dimension as $c => $dim) {
		$value = strFloat ($_POST['Quantity'][$n][$c], (int)$Record['UnitDecimals']) ;
		if (is_null ($value)) continue ;
		if ($value === false or $value == 0) return sprintf ('invalid Quantity, line %d, collumn %d', $n, $c+1) ;
//KEN		
		// Save
		$dim = (int)($dim*100) ;
		$line['Quantity'][$dim] = $value ;
		$Quantity += $value ;
	    }

	} else {
	    // Standalone Quantity
	    $value = strFloat ($_POST['Quantity'][$n], (int)$Record['UnitDecimals']) ;
	    if (!is_null ($value)) {
		if ($value === false or $value == 0) return sprintf ('invalid Quantity, line %d', $n) ;
	    
		// Save
		$line['Quantity'] = $value ;
		$Quantity += $value ;
	    }
	}

	// Comment
	$line['Comment'] = $_POST['Comment'][$n] ;
    }
    if ($Quantity == 0) return 'please specify Quantity' ;

//logPrintVar ($Line, 'Line') ;
    
    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => sprintf ('New Accessories to %s', $Stock['Name'])
    ) ;
    if ($fields['Position']['value'] != '') $ItemOperation['Description'] .= sprintf (', Position %s', $fields['Position']['value']) ;
    if ($fields['BatchNumber']['value'] != '') $ItemOperation['Description'] .= sprintf (', Batch %s', $fields['BatchNumber']['value']) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

    // Item prototype
    $Item = array (
	'ArticleId' => (int)$Record['Id'],
	'ItemOperationId' => (int)$ItemOperation['Id']
    ) ;
    if ($fields['BatchNumber']['value'] != '') $Item['BatchNumber'] = $fields['BatchNumber']['value'] ;
    if ($fields['Sortation']['value'] > 0) $Item['Sortation'] = $fields['Sortation']['value'] ;
    if ((int)$Certificate['Id'] > 0) $Item['ArticleCertificateId'] = (int)$Certificate['Id'] ;
    
    // Container prototype
    $Container = array (
	'StockId' => (int)$Stock['Id'],
	'Position' => $fields['Position']['value'],
    ) ;
    if ($fields['Position']['value'] != '') $Container['Position'] = $fields['Position']['value'] ;

    // Do create
    foreach ($Line as $n => $line) {
	// New CaseId or OrderLineId ?
	if ($line['CaseId'] > 0) {
	    $Item['CaseId'] = $line['CaseId'] ;
		if ($line['ProductionId'] > 0) {
		    $Item['ProductionId'] = $line['ProductionId'] ;
		} else 
		    $Item['ProductionId'] = 0 ;
	    unset ($Item['OrderLineId']) ;
	} if ($line['OrderLineId'] > 0) {
	    $Item['OrderLineId'] = $line['OrderLineId'] ;
	    unset ($Item['CaseId']) ;
	    unset ($Item['ProductionId']) ;
	}

	// New Color ?
	if ($Record['VariantColor']) {
	    if ($line['ArticleColorId'] > 0) {
		$Item['ArticleColorId'] = $line['ArticleColorId'] ;
	    }
	    if ($Item['ArticleColorId'] <= 0) return sprintf ('please select Color, line %d', $n) ;
	}

	// Comment
	if ($line['Comment'] != '') $Item['Comment'] = $line['Comment'] ;

	// New Container ?
	if ($line['ContainerId'] > 0) {
	    // New existing Container specified
	    $Item['ContainerId'] = $line['ContainerId'] ;

	} else if ((int)$line['ContainerType']['Id'] > 0) {
	    // Create new container
	    if ($Container['StockId'] <= 0) return sprintf ('%s(%d) no Stock', __FILE__, __LINE__) ; 	// Just to be sure
	    $ContainerType = $line['ContainerType'] ;
	    $Container['ContainerTypeId'] = (int)$ContainerType['Id'] ;
	    $Container['TaraWeight'] = $ContainerType['TaraWeight'] ;
	    $Container['GrossWeight'] = $ContainerType['TaraWeight'] ;
	    $Container['Volume'] = $ContainerType['Volume'] ;
	    $Container['Id'] = tableWrite ('Container', $Container) ;
//logPrintVar ($Container,'Container') ;
	    $Item['ContainerId'] = $Container['Id'] ;
    	}	    
	if ($Item['ContainerId'] <= 0) return sprintf ('please select Container, line %d', $n) ;

	// Process Quantities
	if ($Record['VariantSize']) {
	    // By Sizes
	    foreach ($line['Quantity'] as $id => $quantity) {
		$Item['Quantity'] = number_format ($quantity, (int)$Record['UnitDecimals'], '.', '') ;
		$Item['ArticleSizeId'] = $id ;
	    	tableWrite ('Item', $Item) ;
//logPrintVar ($Item, 'Item') ;
	    }
	} else if ($Record['VariantDimension']) {
	    // By Dimension
	    foreach ($line['Quantity'] as $dim => $quantity) {
		$Item['Quantity'] = number_format ($quantity, (int)$Record['UnitDecimals'], '.', '') ;
		$Item['Dimension'] = (float)($dim/100) ;
	   	tableWrite ('Item', $Item) ;
//logPrintVar ($Item, 'Item') ;
	    }
	} else {
	    // Standalone Quantity
	    if ($line['Quantity'] > 0) {
		$Item['Quantity'] = number_format ($line['Quantity'], (int)$Record['UnitDecimals'], '.', '') ;
		tableWrite ('Item', $Item) ;
//logPrintVar ($Item, 'Item') ;
	    }
	}
    }

    // View pick
    return navigationCommandMark ('itemlistoperation', $ItemOperation['Id']) ;
?>
