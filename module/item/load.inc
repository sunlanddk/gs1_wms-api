<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;

    $fieldReqLineName = 'IF(Item.RequisitionLineId>0,CONCAT(Requisition.id,"/",RequisitionLine.No),"")' ;
//    $fieldReqLineName = 'Item.RequisitionLineId' ;
//    $fieldStockName = 'IF(Item.ContainerId>0, Stock.Name, IF((int)$Record['ConsumeProductionId'] > 0, CONCAT("Consumed ",Production.CaseId,"/",Production.Number),CONCAT("Consumed ", Item.BatchNumber)))' ;
    $fieldStockName = 'IF(Item.ContainerId>0, Stock.Name, IF(Item.ConsumeProductionId > 0, CONCAT("Consumed ",Production.CaseId,"/",Production.Number),CONCAT("Inv consume: ", Item.BatchNumber)))' ;
    $fieldAlloc = 'IF(Item.CaseId>0,CONCAT("C",Item.CaseId),IF(Item.OrderLineId>0,CONCAT("O",OrderLine.OrderId,",",OrderLine.No),"-"))' ;
//    $fieldAlloc = 'IF(Item.ContainerId=0,"",IF(Item.OrderLineId>0,CONCAT("O",OrderLine.OrderId,",",OrderLine.No),IF(Item.CaseId>0,CONCAT("C",Item.CaseId,"/", IF(ProductionTO.Id>0,ProductionTo.Number,"")   ),"")))' ;
    $fieldStateAlloc = "IF(ProductionTO.Id>0 and Item.CaseId>0,
    
				IF(ProductionTO.Number>99 or ProductionTO.Number<50,
					IF(ProductionTO.Done,'Done',
						IF(ProductionTO.Packed,'Packed',
							IF(ProductionTO.Cut,'Prod-Cut',
								IF(ProductionTO.Approved,IF(ProductionTO.Allocated,IF(ProductionTO.ProdDocRec,'Prod-adjusted','Prod'),IF(ProductionTO.Request,'Request(Approved)','Draft(Approved)')),
									IF(ProductionTO.Allocated,if(ProductionTO.Ready,'Alloc(Ready)','Allocated'),
										IF(ProductionTO.Request,if(ProductionTO.Ready,'Request(Ready)','Request'),
											IF(ProductionTO.Ready,'UnreqReady',
												'Draft')))))))
				,
					IF(ProductionTO.Done,'Processed',
						IF(ProductionTO.Packed,'Approved',
							IF(ProductionTO.ready,'Shipped',
								IF(ProductionTO.allocated,'Allocated',
									IF(ProductionTO.request,'Requested','Draft')))))
				)    
    
    ,'')" ;
    $fieldProduced = 'CONCAT(ProductionFrom.CaseId,"/",ProductionFrom.Number)' ;
    $fieldOwner = 'OwnerCompany.Name' ;
    $fieldCertificate = 'IF(Item.ArticleCertificateId=0,"none",CertificateType.Name)' ;

    $fieldArticle = 'ArticleType.Product AS ArticleTypeProduct, ArticleType.Material AS ArticleTypeMaterial, ArticleType.Test AS ArticleTypeTest, ArticleType.Fabric AS ArticleTypeFabric,
	Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantCertificate, Article.VariantDimension, Article.VariantSortation,
	Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals' ;
    
    $queryFields = 'Item.*, 
	Container.Description AS ContainerDescription, Container.ContainerTypeId, Container.Position AS ContainerPosition,
	ContainerType.Name AS ContainerTypeName,
	Stock.Id AS StockId, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Arrived AS StockArrived, Stock.Verified AS StockVerified, Stock.Done AS StockDone, ' .
	$fieldArticle . ', ' .
	'Color.Number AS ColorNumber, Color.Description AS ColorDescription, ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	ArticleSize.Name AS SizeName, ' .
	$fieldCertificate . ' AS CertificateTypeName,
	Transport.ArrivedDate As ArrivedDate, 
	Transport.Name As TransportName, 
	AltStock.Name As MoveToName, 
	Item.AltStockId As MoveToStockId, 
	VariantCode.VariantCode,
	OrderLine.OrderId, OrderLine.No AS OrderLineNo,' .
	$fieldStockName . ' AS StockName,' .
	$fieldAlloc . ' AS Allocation,' .
	$fieldStateAlloc . ' AS StateAlloc,' .
	$fieldProduced . ' AS Produced,' .
	$fieldOwner . ' AS Owner,' .
	$fieldReqLineName . ' AS ReqLineName, ' .
	'requisitionline.no AS RequisitionNo'
	;

    $queryTables = 'Item
	LEFT JOIN Container ON Container.Id=Item.ContainerId
	LEFT JOIN ContainerType ON ContainerType.Id=Container.ContainerTypeId 
	LEFT JOIN Stock ON Stock.Id=Container.StockId
	LEFT JOIN Stock AltStock ON AltStock.Id=Item.AltStockId 
	LEFT JOIN VariantCode  ON VariantCode.Id=Item.VariantCodeId
	LEFT JOIN Article ON Article.Id=VariantCode.ArticleId
	LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId
	LEFT JOIN Unit ON Unit.Id=Article.UnitId 
	LEFT JOIN ArticleColor ON ArticleColor.Id=VariantCode.ArticleColorId
	LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	LEFT JOIN ArticleSize ON ArticleSize.Id=VariantCode.ArticleSizeId
	LEFT JOIN ArticleCertificate ON ArticleCertificate.Id=Item.ArticleCertificateId
	LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId
	LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId
	LEFT JOIN Stock AS Transport ON Transport.Id=Item.TransportId 
	LEFT JOIN Production ON Production.Id=Item.ConsumeProductionId
	LEFT JOIN Production AS ProductionFrom ON ProductionFrom.Id=Item.FromProductionId
	LEFT JOIN Production AS ProductionTo ON ProductionTo.Id=Item.ProductionId and ProductionTo.Active=1
	LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
	LEFT JOIN Requisition ON Requisition.Id=Item.RequisitionId
	LEFT JOIN RequisitionLine ON RequisitionLine.Id=Item.RequisitionLineId
	LEFT JOIN Company OwnerCompany ON OwnerCompany.Id=Item.OwnerId
	' ;

    $queryFieldsSum = '' ;
	$queryClauseGroup = '' ;
    $ArticleClauseGroup = TRUE ;

    switch ($Navigation['Function']) {
	case 'returnlist' :
		$queryOrder = 'Article.Number, Color.Description, ArticleSize.DisplayOrder' ;
	case 'movelist' :
	case 'consumelist' :
	case 'upgrade' :
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;
	    $ArticleClauseGroup = FALSE ;

	    // Fall through
	
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Item',		'db' => 'Item.Id',		'desc' => true,		'direct' => 'Item.Active=1'),
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Description',	'db' => 'Article.Description'),
		array ('name' => 'Color',		'db' => 'Color.Number'),
		array ('name' => 'ColorDescription',		'db' => 'Color.Description'),
		array ('name' => 'Size',		'db' => 'ArticleSize.Name'),
//		array ('name' => 'Dim.',		'db' => 'Item.Dimension'),
//		array ('name' => 'Sort.',		'db' => 'Item.Sortation'),
//		array ('name' => 'Certificate',		'db' => $fieldCertificate),
		array ('name' => 'Batch',		'db' => 'Item.BatchNumber'),
//		array ('name' => 'Test',		'db' => 'Item.TestDone'),
		array ('name' => 'Stock',		'db' => $fieldStockName),
//		array ('name' => 'MoveTo',		'db' => 'AltStock.Name'),
		array ('name' => 'Allocation',		'db' => $fieldAlloc),
//		array ('name' => 'State',		'db' => $fieldStateAlloc),
		array ('name' => 'Cont',			'db' => 'Container.Id',		'desc' => true),
//		array ('name' => 'Type',		'db' => 'ContainerType.Name'),
		array ('name' => 'Pos.',		'db' => 'Container.Position'),
//		array ('name' => 'Qty',			'db' => 'Item.Quantity'),
//		array ('name' => 'Unit',		'db' => 'Unit.Name'),
//		array ('name' => 'Prod.',		'db' => $fieldProduced),
//		array ('name' => 'Owner',		'db' => $fieldOwner)
//		array ('name' => 'Value',		'db' => 'Item.Price')
	    ) ;

	    switch ($Navigation['Parameters']) {
		case 'article' :
		    // Specific Article
//		    $query = sprintf ('SELECT Article.* FROM Article WHERE Article.Id=%d AND Article.Active=1', $Id) ;
		    $query = sprintf ("SELECT Article.Id, Article.Id AS ArticleId, %s FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Id=%d AND Article.Active=1", $fieldArticle, $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideArticle = true ;

		    $queryClause = sprintf ('Item.ArticleId=%d AND Item.Active=1 AND (Item.OwnerId=%d or Item.OwnerId=0)', $Id, $User['CompanyId']==18?2:$User['CompanyId']) ;
		    if ($ArticleClauseGroup) {
			    $queryClause .= ' AND (Stock.Type="fixed"' ;
			    if ($_POST['ListTransports']) {
				    $queryClause .= ' OR Stock.Type="transport"' ;
			    }
				if ($_POST['ListShipments']) {
				    $queryClause .= ' OR Stock.Type="shipment"' ;
			    }
				if ($_POST['ListConsumed']) {
				    $queryClause .= ' OR !(Item.ContainerId>0)' ;
			    }
			    $queryClause .= ')' ;
		    } 
			
		    // Grouping filter in List view to sum Quantity
		    if (($ArticleClauseGroup) and ($_POST['ListGrouped'])) {
				$queryFieldsSum = ' ,SUM(Item.Quantity) AS Quantity, 
									"Grouped" AS Id,
									"--" AS Allocation,
									"--" AS CertificateTypeName,
									"--" AS Produced,
									"--" AS ContainerId,
									"--" AS ContainerTypeName ' ;
				$queryClauseGroup = ' Item.ArticleColorId, Item.ArticleSizeId, Item.Dimension, Item.Sortation, Item.BatchNumber, StockName ' ;
		    }

		    // Navigation
		    navigationEnable ('itemnew') ;
		    navigationEnable ('itemallocate') ;
		    if ($Record['ArticleTypeMaterial'] and !$Record['ArticleTypeFabric']) navigationEnable ('itemaccessorynew') ;
		    if ($Record['ArticleTypeFabric']) navigationEnable ('itemfabricnew') ;
		    break ;	

		case 'stock' :
		case 'shipment' :
		    // Specific stock
		    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideStock = true ;

		    $queryClause = sprintf ('Container.StockId=%d AND Item.Active=1', $Id) ;
		    break ;

		case 'container' :
		    // Specific container
		    $query = sprintf ('SELECT Container.*, Stock.Name AS StockName FROM Container LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideStock = true ;
		    $HideContainer = true ;

		    $queryClause = sprintf ('Item.ContainerId=%d AND Item.Active=1', $Id) ;
		    break ;
		
		case 'allocated' :
		    // Allocated items for Case specified by Production
		    $query = sprintf ('SELECT Production.*, Case.Description AS CaseDescription FROM Production INNER JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.Id=%d AND Production.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideCase = true ;

		    $queryClause = sprintf ('Item.CaseId=%d AND (Item.ProductionId=%d or Item.ProductionId<=0)AND Item.Active=1 AND Item.ContainerId>0  AND (Item.OwnerId=%d or Item.OwnerId=0)', (int)$Record['CaseId'], (int)$Record['Id'], $User['CompanyId']==18?2:$User['CompanyId']) ;
		    break ;
		
		case 'case' :
		    // Specific case
		    $query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideCase = true ;

		    $queryClause = sprintf ('Item.CaseId=%d AND Item.ProductionId=0 AND Item.Active=1 AND Item.ContainerId>0 AND (Item.OwnerId=%d or Item.OwnerId=0)', $Id, $User['CompanyId']==18?2:$User['CompanyId']) ;
		    break ;

  		case 'producedcase' :
		    // Item produced from specified Case
		    $query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Get the ProductionsOrders in a list
		    $query = sprintf ('SELECT Id FROM Production WHERE CaseId=%d AND Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $s = '' ;
		    while ($row = dbFetch ($res)) {
		       	if ($s != '') $s .= ',' ;
			$s .= $row['Id'] ;
		    }
		    dbQueryFree ($res) ;
		    if ($s == '') $s = '-1' ;

		    $HideAllocation = true ;
		    $HideProduced = true ;

		    $queryClause = sprintf ('Item.FromProductionId IN (%s) AND Item.Active=1', $s) ;
		    break ;
		
		case 'consumedcase' :
		    // Item produced from specified Case
		    $query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Get the ProductionsOrders in a list
		    $query = sprintf ('SELECT Id FROM Production WHERE CaseId=%d AND Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $s = '' ;
		    while ($row = dbFetch ($res)) {
		       	if ($s != '') $s .= ',' ;
			$s .= $row['Id'] ;
		    }
		    dbQueryFree ($res) ;
		    if ($s == '') $s = '-1' ;

		    $HideStock = true ;
		    $HideAllocation = true ;
		    $HideProduced = true ;

		    $queryClause = sprintf ('Item.ConsumeProductionId IN (%s) AND Item.Active=1', $s) ;
		    break ;

		case 'produced' :
		    // Item produced from specified Production
		    $query = sprintf ('SELECT Production.* FROM Production WHERE Production.Id=%d AND Production.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideAllocation = true ;
		    $HideProduced = true ;

		    $queryClause = sprintf ('Item.FromProductionId=%d AND Item.Active=1', $Id) ;
		    break ;

		case 'consumed' :
		    // Item consumed by specified Production
		    $query = sprintf ('SELECT Production.* FROM Production WHERE Production.Id=%d AND Production.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideStock = true ;
		    $HideAllocation = true ;
		    $HideContainer = true ;

		    $queryClause = sprintf ('Item.ConsumeProductionId=%d AND Item.ContainerId=0 AND Item.Active=1', $Id) ;
		    break ;

		case 'orderline' :
		    // Get OrderLines
		    $query = sprintf ('SELECT OrderLine.*,
		    Order.CompanyId, Order.Description AS OrderDescription,
		    Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName
		    FROM OrderLine
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		    LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    WHERE OrderLine.Id=%d AND OrderLine.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Clause
//		    $queryClause = sprintf ('Item.ArticleId=%d AND ', (int)$Record['ArticleId']) ;
//		    if ($Record['VariantColor']) $queryClause .= sprintf ('Item.ArticleColorId=%d AND ', (int)$Record['ArticleColorId']) ;
//		    $queryClause .= sprintf ('(Stock.Type="fixed" OR Item.OrderLineId=%d)', $Id) ;
		    $queryClause .= sprintf ('Item.OrderLineId=%d AND Item.Active=1', $Id) ;

		    // Display
		    $HideArticle = true ;
		    break ;
		    
		case 'order' :
		    // Get Order
		    $query = sprintf ('SELECT
		    `Order`.*,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName
		    FROM `Order`
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    WHERE Order.Id=%d AND Order.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Get OrderLine Id's
		    $s = '' ;
		    $query = sprintf ('SELECT OrderLine.Id FROM OrderLine WHERE OrderLine.OrderId=%d AND OrderLine.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    while ($row = dbFetch ($res)) {
			if ($s != '') $s .= ',' ;
			$s .= $row['Id'] ;
		    }
		    dbQueryFree ($res) ;

		    // Clause
		    if ($s != '') {
			$queryClause = sprintf ('Item.OrderLineId IN (%s) AND Item.Active=1', $s) ;
		    } else {
			$queryClause = 'Item.Id=-1' ;
		    }
		    
		    break ;

		case 'consorder' :
		    // Get Orders
		    $query = sprintf ('SELECT
								`Order`.*,
								Company.Number AS CompanyNumber, Company.Name AS CompanyName
								FROM `Order`
								LEFT JOIN Company ON Company.Id=Order.CompanyId
								WHERE Order.ConsolidatedId=%d AND Order.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $s = '' ;
		    while ($row = dbFetch ($res)) {
				if ($s != '') {
					$s .= ',' ;
				} else {
					$Record = $row ;
				}
				$s .= $row['Id'] ;
		    }
//		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Get OrderLine Id's
		    $query = sprintf ('SELECT OrderLine.Id FROM OrderLine WHERE OrderLine.OrderId IN (%s) AND OrderLine.Active=1', $s) ;
		    $res = dbQuery ($query) ;
		    $s = '' ;
		    while ($row = dbFetch ($res)) {
				if ($s != '') $s .= ',' ;
				$s .= $row['Id'] ;
		    }
		    dbQueryFree ($res) ;

		    // Clause
		    if ($s != '') {
				$queryClause = sprintf ('Item.OrderLineId IN (%s) AND Item.Active=1', $s) ;
		    } else {
				$queryClause = 'Item.Id=-1' ;
		    }
		    
		    break ;
		case 'purchase' :
		    // Get Order
		    $query = sprintf ('SELECT
		    `Requisition`.*,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName
		    FROM `Requisition`
		    LEFT JOIN Company ON Company.Id=Requisition.CompanyId
		    WHERE Requisition.Id=%d AND Requisition.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Clause
			$queryClause = sprintf ('Item.RequisitionId=%d AND Item.Active=1', $Record['Id']) ;
			break ;
		case 'purchaseline' :
		    // Get Order
		    $query = sprintf ('SELECT
		    `Requisitionline`.*,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName
		    FROM `RequisitionLine`, `Requisition`
		    LEFT JOIN Company ON Company.Id=Requisition.CompanyId
		    WHERE RequisitionLine.Id=%d AND RequisitionLine.Active=1 AND Requisition.Id=RequisitionLine.RequisitionId', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Clause
			$queryClause = sprintf ('Item.RequisitionLineId=%d AND Item.Active=1', $Id) ;
			break ;
		default :
		    // Unknown Item list creteria
		    return sprintf ('%s(%d) unknown criteria, parameter "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
	    }

	    // Additional clauses
	    switch ($Navigation['Function']) {
		case 'upgrade' :
		    // Only 2. sort for upgrading
		    $queryClause .= ' AND Item.ContainerId>0 AND Article.VariantSortation=1 AND Item.Sortation=2' ;
		    break ;

		case 'consumelist' :
		    $queryClause .= ' AND Item.ContainerId>0 AND Item.CaseId>0' ;
		    break ;
	    }
	    
	    if ($Navigation['Mark']=='reservelist') $queryClause .= ' AND Item.ReservationId=0 AND Item.CaseId=0 and Item.ProductionId=0' ;
	    
	    
	    // Query Items to list
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, sprintf("%s%s", $queryFields, $queryFieldsSum), $queryTables, $queryClause, $queryOrder, $queryClauseGroup) ;
	    if (is_string($Result)) return $Result ;

	    if ($Navigation['Function'] == 'list') {
		// Navigation
		// Pass all parameters for the list function to the move function
		$param = 'param=' . $Navigation['Parameters'] ;
		if ($listParam != '') $param .= '&' . $listParam ;
		navigationSetParameter ('consumelist', $param) ;
		navigationSetParameter ('movelist', $param) ;
		navigationSetParameter ('reservelist', $param) ;
		navigationSetParameter ('upgrade', $param) ;
	    
		if (dbNumRows($Result) == 0) {
		    navigationPasive ('consumelist') ;
		    navigationPasive ('movelist') ;
		    navigationPasive ('upgrade') ;
		    navigationPasive ('itemallocate') ;
		}
	    
		// return listView ($Result, 'itemview') ;
	    }
	    
	    return 0 ;    

	case 'listoperation' :
	    // Get ItemOperation
	    $query = sprintf ('SELECT * FROM ItemOperation WHERE Id=%d AND Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Get Items in Operation
	    $query = sprintf ('SELECT %s, PreviousStock.Name AS PreviousStockName FROM %s LEFT JOIN Container AS PreviousContainer ON PreviousContainer.Id=Item.PreviousContainerId LEFT JOIN Stock AS PreviousStock ON PreviousStock.Id=PreviousContainer.StockId WHERE Item.ItemOperationId=%d AND Item.Active=1', $queryFields, $queryTables, $Id) ;
	    
   	    switch ($Navigation['Parameters']) {
			case 'storing' :
				$Params = sprintf('&ToStock=%d&StockId=%d',$_GET['ToStock'],$_GET['FromStock']) ;
				if  (isset($_GET['CollMemberId'])) 
					$Params .= sprintf('&CollMemberReturnId=%d',(int)$_GET['CollMemberId']) ;
				navigationSetParameter ('storenext', $Params) ;
				navigationEnable ('storenext') ;
				break ;
			case 'returns':
				navigationDisable ('storenext') ;
				$query .= ' Order By ContainerPosition' ;
				break ;
			default:
				navigationDisable ('storenext') ;
				break ;
		}
	    $Result = dbQuery ($query) ;

	    return 0 ;

	case 'basic':
	case 'basic-cmd' :
    	    switch ($Navigation['Parameters']) {
		case 'new' :
		    // Get Article
		    $query = sprintf ("SELECT Article.Id, Article.Id AS ArticleId, %s, %s as OwnerId FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Id=%d AND Article.Active=1", $fieldArticle, $User['CompanyId'], $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    return 0 ;
	    }

	    // fall through
	    
	case 'comment' :
	case 'comment-cmd' :
	case 'variant' :
	case 'variant-cmd' :
	case 'testgeneric':
	case 'testgeneric-cmd':
	case 'testcolor' :
	case 'testcolor-cmd' :
	case 'clone' :
	case 'clone-cmd' :
	case 'move' :
	case 'move-cmd' :
	case 'consume' :
	case 'consume-cmd' :
	case 'release-cmd' :
	case 'split' :
	case 'split-cmd' :
	case 'inventory' :
	case 'inventory-cmd' :
	case 'view' :
	case 'delete-cmd' :
	    // Query Item
	    $query = sprintf ('SELECT %s FROM %s WHERE Item.Id=%d AND Item.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    
	    // Item tests
	    if ($Record['ArticleTypeTest']) {
		navigationEnable ('testgeneral') ;
		navigationEnable ('testcolor') ;
	    }

	    // Materials
	    if ($Record['ArticleTypeMaterial']) {
		navigationEnable ('consume') ;
	    }

	    // Fabrics
	    if ($Record['ArticleTypeFabric']) {
		navigationEnable ('clone') ;
	    }

	    // Is it allowed operate Items on this Stock
	    if ((int)$Record['ContainerId'] == 0) {
		navigationPasive ('release') ;
		navigationPasive ('releaseall') ;
		navigationPasive ('consume') ;
		navigationPasive ('testgeneral') ;	       
		navigationPasive ('testcolor') ;	       
		navigationPasive ('split') ;	       
		navigationPasive ('move') ;	       
	    }

	    if ((int)$Record['ContainerId'] == ParameterGet('ProdContainerId')) {
		navigationPasive ('release') ;
		navigationPasive ('releaseall') ;
		navigationPasive ('consume') ;
		navigationPasive ('testgeneral') ;	       
		navigationPasive ('testcolor') ;	       
		navigationPasive ('split') ;	       
		navigationPasive ('move') ;	       
		navigationPasive ('basic') ;	       
		navigationPasive ('comment') ;	       
		navigationPasive ('variants') ;	       
		navigationPasive ('edit') ;	       
	    }

	    if ((int)$Record['ContainerId'] == ParameterGet('ReqContainerId')) {
		//navigationPasive ('release') ;
		navigationPasive ('consume') ;
		navigationPasive ('testgeneral') ;	       
		navigationPasive ('testcolor') ;	       
		//navigationPasive ('split') ;	       
		navigationPasive ('move') ;	       
		navigationPasive ('basic') ;	       
		navigationPasive ('comment') ;	       
		navigationPasive ('variants') ;	       
		navigationPasive ('edit') ;	       
	    }

	    if ($Record['StockType'] == 'shipment') {
			navigationPasive ('testgeneral') ;	       
			navigationPasive ('testcolor') ;	       
			if ($Record['StockReady']) {
				navigationPasive ('release') ;
				navigationPasive ('releaseall') ;
				navigationPasive ('split') ;	       
				navigationPasive ('move') ;
			}
	    } else if ($Record['StockType'] != 'fixed') {
//			navigationPasive ('release') ;
//			navigationPasive ('consume') ;
//			navigationPasive ('split') ;	       
			navigationPasive ('move') ;	       
	    }
	    
	    if ($Record['TestDone']) {
		navigationPasive ('testgeneral') ;	       
	    }
	    
	    if ((int)$Record['CaseId'] > 0) {
		// Item assigned to Case
		$x=1;//navigationPasive ('split') ;	       
	    } else {
		// Item not assigned to Case
		navigationPasive ('consume') ;
	    }

	    if ((int)$Record['OrderLineId'] > 0) {
		// Item assigned to OrderLine
		navigationPasive ('split') ;	       
	    }

	    if ((int)$Record['CaseId'] == 0 and (int)$Record['OrderLineId'] == 0) {
		// Item not assigned at all
		navigationPasive ('release') ;
	    }
	    if ((int)$Record['ReservationId'] == 0) {
		// Item not assigned at all
		navigationPasive ('releaseall') ;
	    }
	
	    return 0 ;

	case 'accessorynew' :
	case 'accessorynew-cmd' :
    	case 'allocate' :
	case 'allocate-cmd' :
	case 'fabricnew' :
	case 'fabricnew-cmd' :
	    // Get Article
	    $query = sprintf ("SELECT Article.*, Article.Id AS ArticleId, %s FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Id=%d AND Article.Active=1", $fieldArticle, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    return 0 ;

	case 'upgrade-cmd' :
	case 'movelist-cmd' :
	case 'consumelist-cmd' :
	    // Everything handled in the command module
	    return 0 ;
    }

    return 0 ;
?>
