<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Item', (int)$Record['Id']) ;
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Quantity', number_format ($Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('New Container', formText ('ContainerId', '', 8, 'text-align:right;')) ; 
    itemSpace() ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
