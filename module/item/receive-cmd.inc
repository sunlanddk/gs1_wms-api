<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/db.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;


    if (!($_POST['ToStockId']>0)) return 'please select stock location to receive items' ;

    // Initialization
    $Line = array () ;

    // Get Id's of Items to move
    $Line = array () ;
    if (!is_array($_POST['Item'])) return 'please select Items to return' ;
    foreach ($_POST['Item'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$Line[(int)$id] = NULL ;
//return $_POST['ItemQty'][$id] ;
    }
    if (count($Line) == 0) return 'please select Items to return' ;

    // Get Item information
    foreach ($Line as $id => $l) {
		$line = &$Line[$id] ;
		
		// Lookup Item
		$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone 
		FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId 
		WHERE Item.Id=%d AND Item.Active=1', $id) ;
		$res = dbQuery ($query) ;
		$line['Item'] = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
		if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more', $id) ;
		//	if ($line['Item']['StockType']=='transport' or $line['Item']['StockType']=='shipment') return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
		//	if ($line['Item']['StockDone'] or ($line['Item']['StockReady'] and !$line['Item']['StockVerified'])) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
    }    

    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => 'Receive Items from shipments'
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

    $error_note='' ;
    
    // Do the moving
	$n = 0 ;
    foreach ($Line as $id => $line) {
    $n++ ;
		// Lookup Item
		$query = sprintf ('SELECT * FROM Item WHERE Item.Id=%d AND Item.Active=1 And Item.Sortation<2', $id) ;
		$res = dbQuery ($query) ;
		$Item = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ($Item['Id'] > 0) {
			// Find container
			$query=sprintf('SELECT * FROM container WHERE StockId=%d And Active=1 Order By Id',(int)$_POST['ToStockId']) ;
			$res = dbQuery ($query) ;
			$Container = dbFetch ($res) ;
			dbQueryFree ($res) ;

			if ($Container['Id']>0) {
//return 'Debug 1 ' . $_POST['ItemQty'][$id] ;
	
				$ReturnQty = (float)$_POST['ItemQty'][$id] ;
				if ($ReturnQty < 0) $ReturnQty = 0 ;
				if ($ReturnQty > $Item['Quantity']) $ReturnQty = $Item['Quantity'] ;
//		return 'Debug ' . $ReturnQty . ' Item ' . $Item['Quantity'] ;
				
				$ShipQty = (float)$Item['Quantity'] - (float)$ReturnQty ;
//		return 'Debug ' . $ShipQty ;

				// Split item and create new item on shipment
				if ($ShipQty > 0) {
					unset ($Item['Id']) ;
					$Item['Quantity'] = $ShipQty ;
					tableWrite ('Item', $Item) ;
				}
	
				// Update the Item to the new location
				$Item = array (
					'ItemOperationId' => $ItemOperation['Id'],
//					'PreviousId' => $id,
					'PreviousContainerId' => (int)$Item['ContainerId'],
					'OrderLineid' => 0,
					'Quantity' => (int)$ReturnQty,
					'ContainerId' => (int)$Container['Id']
				) ;
				tableWrite ('Item', $Item, $id) ;
				// Maintain containers even if empty				containerDeleteEmpty ((int)$Item['PreviousContainerId']) ;
			} else {
				$error_note=$error_note.sprintf('<br>Item %d not moved due to no container on receiving stock', (int)$line['Item']['Id']) ;
			}
		}
    }

	$query = sprintf ('SELECT count(item.id) as NoItems FROM container, item WHERE container.stockid=%d and item.containerid=container.id and container.active=1 and item.active=1', $Id) ;
	$res = dbQuery ($query) ;
	$Item = dbFetch ($res) ;
	dbQueryFree ($res) ;

	if ($Item['NoItems']>0) {
		$items=1 ;
	} else { // Close shipment if everything received
		$Shipment = array (	
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time())
		) ;
		tableWrite ('Stock', $Shipment, $Id) ;
	}
    $ItemOperation['Description']= $ItemOperation['Description'] . $error_note ;
    $ItemOperation['Description']= substr($ItemOperation['Description'],0,99) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation, $ItemOperation['Id']) ;

    // View pick
    return navigationCommandMark ('itemlistopreturn', $ItemOperation['Id']) ;
?>
