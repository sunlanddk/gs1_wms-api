<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'Comment'		=> array ()
    ) ;

    // Validate
    if ((int)$Record['ContainerId'] == 0) return 'consumed Items can not be modified' ;
    if ($Record['StockType'] == 'shipment') return 'shipped Items can not be modified' ;

    // Save record
    return saveFields ('Item', $Id, $fields) ;
?>
