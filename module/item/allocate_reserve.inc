<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Initialize
    $Line = array () ;
    $Dimension = array () ;
    $Size = array() ;

    // Validate Article
    if ($Record['VariantDimension'] and $Record['VariantSize']) return 'the Article can not have both Size and Dimension variants' ;

    // Get parameters
    if (isset($_GET['production'])) {
		// Case specified	
		$value = (int)$_GET['production'] ;
		
		// Lookup production
		$query = sprintf ('SELECT * FROM `Production` WHERE Id=%d AND Packed=0 AND Done=0 AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$Production = dbFetch ($result) ;
		dbQueryFree ($result) ;


		// Lookup Case
		$query = sprintf ('SELECT * FROM `Case` WHERE Id=%d AND Done=0 AND Active=1', $Production['CaseId']) ;
		$result = dbQuery ($query) ;
		$Case = dbFetch ($result) ;
		dbQueryFree ($result) ;

		//	return sprintf ('prodno %d prodid %d, v%d CAse %d', $Production['Number'], $Production['Id'], $value, $Case['Id']) ;

	} else if (isset($_GET['case'])) {
		// Case specified	
		$value = (int)$_GET['case'] ;
		
		// Lookup Case
		$query = sprintf ('SELECT * FROM `Case` WHERE Id=%d AND Done=0 AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$Case = dbFetch ($result) ;
		dbQueryFree ($result) ;

		// Validate case
		if ((int)$Case['Id'] != $value) return sprintf ('Case %d not found1', $value) ;
		
		// Focus
		$Navigation['Focus'] = 'ProductionId' ;

    } else if (isset($_GET['orderline'])) {
		// OrderLine specified
		$value = (int)$_GET['orderline'] ;

		// Lookup OrderLine
		$query = sprintf ('SELECT OrderLine.* FROM OrderLine INNER JOIN `Order` ON Order.Id=OrderLine.OrderId AND Order.Ready=1 AND Order.Done=0 AND Order.Active=1 WHERE OrderLine.Id=%d AND OrderLine.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$OrderLine = dbFetch ($result) ;
		dbQueryFree ($result) ;

		// Validate OrderLine
		if ((int)$OrderLine['Id'] != $value) return sprintf ('OrderLine not found, id %d, line %d', $value, $n) ;
		if ((int)$OrderLine['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('Article mismatch with orderline, line %d', $n) ;
    } else if (isset($_GET['reservationid'])) {
		$ReservationId= (int)$_GET['reservationid'] ;
    }
    
    // Get Size information
    if ($Record['VariantSize']) {
	$query = sprintf ('SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name', (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $Size[(int)$row['Id']] = $row ;
	}
	dbQueryFree ($result) ;
	if (count($Size) <= 0) return 'no Sizes defined for Article' ;
    }

    // Lookup items on stock
    $query = 'SELECT SUM(Item.Quantity) AS Quantity, Stock.Id AS StockId, Stock.Name AS StockName, Stock.Type AS StockType, Item.BatchNumber' ;
    if ($Record['VariantColor']) $query .= ', ArticleColor.Id AS ArticleColorId, Color.Number AS ColorNumber, Color.Description AS ColorDescription, ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue' ;
    if ($Record['VariantCertificate']) $query .= ', ArticleCertificate.Id AS ArticleCertificateId, CertificateType.Name AS CertificateTypeName' ;
    if ($Record['VariantSortation']) $query .= ', Item.Sortation' ;
    if ($Record['VariantSize']) $query .= ', ArticleSize.Id AS ArticleSizeId, ArticleSize.Name AS ArticleSizeName' ;
    if ($Record['VariantDimension']) $query .= ', Item.Dimension' ;
    if ((int)$Case['Id'] > 0) $query .= ', IF(Item.CaseId=0,0,1) AS Allocated' ;
    else if ((int)$OrderLine['Id'] > 0) $query .= ', IF(Item.OrderLineId=0,0,1) AS Allocated' ;
    else $query .= ', 0 AS Allocated' ;
    $query .= ' FROM Item' ;
    $query .= ' INNER JOIN Container ON Container.Id=Item.ContainerId AND Container.Active=1' ;
    $query .= ' INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Active=1 AND (Stock.Type="fixed" OR Item.CaseId>0 OR Item.OrderLineId>0)' ;
    if ($Record['VariantColor']) $query .= ' LEFT JOIN ArticleColor ON ArticleColor.Id=Item.ArticleColorId LEFT JOIN Color ON Color.Id=ArticleColor.ColorId LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId' ;
    if ($Record['VariantCertificate']) $query .= ' LEFT JOIN ArticleCertificate ON ArticleCertificate.Id=Item.ArticleCertificateId LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId' ;
    if ($Record['VariantSize']) $query .= ' LEFT JOIN ArticleSize ON ArticleSize.Id=Item.ArticleSizeId' ;   
    $query .= sprintf (' WHERE Item.ArticleId=%d', (int)$Record['Id']) ;
    if ($Record['VariantColor'] and (int)$OrderLine['Id'] > 0) $query .= sprintf (' AND Item.ArticleColorId=%d', (int)$OrderLine['ArticleColorId']) ;
    $query .= ' AND ((Item.CaseId=0 AND Item.OrderLineId=0 AND Item.ReservationId=0)' ;
    if ((int)$Case['Id'] > 0) {
//		$query .= sprintf (' AND (Item.ReservationId=0 OR Item.ReservationId=%d) ', (int)$Case['CompanyId']) ;
	    if ((int)$Production['Id'] > 0) {
			$query .= sprintf (' OR (Item.CaseId=%d AND Item.ProductionId=%d)', (int)$Case['Id'], (int)$Production['Id']) ;
		} else {
			$query .= sprintf (' OR Item.CaseId=%d  AND Item.ProductionId<=0 ', (int)$Case['Id']) ;
		}
    } else if ((int)$OrderLine['Id'] > 0) {
		$query .= sprintf (' OR Item.OrderLineId=%d', (int)$OrderLine['Id']) ;
    } else if ((int)$ReservationId > 0) {
		$query .= sprintf ('  OR (Item.ReservationId=0 OR Item.ReservationId=%d) ', (int)$ReservationId) ;
	}
    $query .= ')' ;
    $query .= ' AND Item.Active=1' ;
    $query .= ' GROUP BY Stock.Id' ;
    if ($Record['VariantColor']) $query .= ', Item.ArticleColorId' ;
    $query .= ', Item.BatchNumber' ;
    if ($Record['VariantCertificate']) $query .= ', Item.ArticleCertificateId' ;
    if ($Record['VariantSortation']) $query .= ', Item.Sortation' ;
    if ($Record['VariantSize']) $query .= ', Item.ArticleSizeId' ;
    if ($Record['VariantDimension']) $query .= ', Item.Dimension' ;
    $query .= ', Allocated' ;
    $query .= ' ORDER BY Stock.Name' ;
    if ($Record['VariantColor']) $query .= ', Color.Number' ;
    $query .= ', Item.BatchNumber' ;
    if ($Record['VariantCertificate']) $query .= ', CertificateType.Name' ;
    if ($Record['VariantSortation']) $query .= ', Item.Sortation' ;
    if ($Record['VariantSize']) $query .= ', ArticleSize.DisplayOrder' ;
    if ($Record['VariantDimension']) $query .= ', Item.Dimension' ;
    $query .= ', Allocated' ;
//return $query ;
    $result = dbQuery ($query) ;
    $n = 0 ;
    while ($row = dbFetch ($result)) {
//printf ("Item: stock %s, type %s, color %d, size %d, dimension %d, allocated %d, quantity %s<br>\n", $row['StockName'], $row['StockType'], $row['ArticleColorId'], $row['ArticleSizeId'], $row['Dimension'], $row['Allocated'], number_format((float)$row['Quantity'], (int)$Record['UnitDecimals'], ',', '.')) ;

    	// Variants for collumns
	if ($Record['VariantDimension']) {
	    // Dimmension
	    $i = (int)$row['Dimension'] ;

	    // Define dimension
	    $Dimension[$i] = true ;

	} else if ($Record['VariantSize']) {
	    // Size
	    $i = (int)$row['ArticleSizeId'] ;

	    // Verify that Size exists
	    if (!isset($Size[$i])) continue ;

	} else {
	    // No variant
	    // Set dummy index
	    $i = 0 ;
	}

	// Any Items
	if ((float)$row['Quantity'] == 0) continue ;

	// New line ?
	if ($n <= 0 or 
	    $Line[$n]['StockId'] != (int)$row['StockId'] or
	    ($Record['VariantColor'] and $Line[$n]['ArticleColorId'] != (int)$row['ArticleColorId']) or
//	    $Line[$n]['BatchNumber'] != $row['BatchNumber'] or
	    strcmp($Line[$n]['BatchNumber'], $row['BatchNumber']) or
	    ($Record['VariantCertificate'] and $Line[$n]['ArticleCertificateId'] != (int)$row['ArticleCertificateId']) or
	    ($Record['VariantSortation'] and $Line[$n]['Sortation'] != (int)$row['Sortation'])) {
	    // Create new line
	    $n++ ;
	    $Line[$n] = $row ;
	    unset ($Line[$n]['Quantity']) ;
	    unset ($Line[$n]['Allocated']) ;
	    unset ($Line[$n]['Dimmension']) ;
	    unset ($Line[$n]['ArticleSizeId']) ;
	    $Line[$n]['QuantityAllocated'] = array () ;
	    $Line[$n]['QuantityFree'] = array () ;
	}

	// Save Quantity
	if ($row['Allocated']) {
	    $Line[$n]['QuantityAllocated'][$i] = (float)$row['Quantity'] ;
	} else {
	    $Line[$n]['QuantityFree'][$i] = (float)$row['Quantity'] ;
	}
    }
    dbQueryFree ($result) ;

    if ($Record['VariantDimension']) {
	// Sort dimensions
	ksort ($Dimension) ;
    }
    
    // Form
    formStart () ;

    // Header fields
    itemStart () ;
    itemSpace () ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Unit', $Record['UnitName']) ;
    itemSpace () ;
    itemHeader () ;

    // OrderLine
    $query = 'SELECT OrderLine.Id, CONCAT(Order.Id,".",OrderLine.No,", ",OrderLine.Description' ;
    if ($Record['VariantColor']) $query .= ',", ",Color.Number," ",Color.Description' ;
    $query .= ') AS Value FROM OrderLine INNER JOIN `Order` ON Order.Id=OrderLine.OrderId AND Order.Ready=1 AND Order.Done=0 AND Order.Active=1 ' ;
    if ($Record['VariantColor']) $query .= 'LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId LEFT JOIN Color ON Color.Id=ArticleColor.ColorId ' ;
    $query .= sprintf ('WHERE OrderLine.ArticleId=%d AND OrderLine.Active=1 ORDER BY Value', (int)$Record['ArticleId']) ;
    itemFieldRaw ('OrderLine', formDBSelect ('OrderLineId', (int)$OrderLine['Id'], $query, 'width:400px;', NULL, sprintf('onchange="appLoad(%d,%d,\'orderline=\'+document.appform.OrderLineId.options[document.appform.OrderLineId.selectedIndex].value);"', (int)$Navigation['Id'], $Id))) ; 

    itemField ('', 'or') ;

    // Case
    itemFieldRaw ('Case', formText ('CaseId', $Case['Id'], 6, '', sprintf('onchange="appLoad(%d,%d,\'case=\'+document.appform.CaseId.value);"', (int)$Navigation['Id'], $Id))) ;
    if ((int)$Case['Id'] > 0) {
//        $query = sprintf ('SELECT Production.Id, CONCAT(Production.Number,", ",Production.Description) AS Value FROM Production LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId WHERE Production.CaseId=%d AND Production.Active=1 AND Production.Done=0 AND Production.Ready=1 AND (Production.Approved=1 OR ProductionLocation.TypeSample=1) ORDER BY Value', (int)$Case['Id']) ;
    $query = sprintf ('SELECT Production.Id, CONCAT(Production.Number,", ",Production.Description) AS Value FROM Production LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId WHERE Production.CaseId=%d AND Production.Active=1 AND Production.Done=0 AND Production.Packed=0 ORDER BY Value', (int)$Case['Id']) ;

    itemFieldRaw ('Production', formDBSelect ('ProductionId', (int)$Production['Id'], $query, 'width:250px;', NULL, sprintf('onchange="appLoad(%d,%d,\'production=\'+document.appform.ProductionId.options[document.appform.ProductionId.selectedIndex].value);"', (int)$Navigation['Id'], $Id))) ; 
    }
    itemField ('', 'or') ;

	// Reservation
	$query = sprintf("Select 0 as Id, '  -- None --  ' as Value Union SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeCustomer=1 AND Company.Active=1 AND (Company.ToCompanyId=0 or Company.ToCompanyId=%s) ORDER BY Value", $User['CompanyId']) ;
	$load_exp = sprintf('onchange="appLoad(%d,%d,\'reservationid=\'+document.appform.ReservationId.options[document.appform.ReservationId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
    itemFieldRaw ('Reserve to', formDBSelect ('ReservationId', (int)$ReservationId, $query , 'width:250px;', NULL, $load_exp)) ;  

    itemSpace () ;
    itemEnd () ;

    // List Header
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('No.', 25) ;
    listHead ('Stock', 120) ;
    if ($Record['VariantColor']) listHead ('Colour', 90) ;
    listHead ('Batch', 80) ;
    if ($Record['VariantCertificate']) listHead ('Certificate', 95) ;
    if ($Record['VariantSortation']) listHead ('Sortation', 55, 'align=right') ;
    listHead ('', 4) ;
    listHead ('Quantity', 60, 'style="border-left:1px solid #cdcabb;"') ;
    listHead ('', 70) ;
    if ($Record['VariantSize']) $n = count($Size) ;
    else if ($Record['VariantDimension']) $n = count($Dimension) ;
    else $n = 0 ;
    for ( ; $n > 1 ; $n--) {
	listHead ('', 60) ;
	listHead ('', 70) ;
    }
    listHead ('') ;
    
    // Variant information
    if (count($Line) > 0) {
	if ($Record['VariantSize']) {
	    listRow () ;
	    listField ('Sizes', 'colspan=3 height=23') ;
	    if ($Record['VariantColor']) listField () ;
	    if ($Record['VariantCertificate']) listField () ;
	    if ($Record['VariantSortation']) listField () ;
	    listField () ;
	    foreach ($Size as $size) {
		listField ($size['Name'], 'style="border-left:1px solid #cdcabb;"') ;
		listField () ;
	    }
       } else if ($Record['VariantDimension']) {
	    listRow () ;
	    listField ('Dimension', 'colspan=3 height=23') ;
	    if ($Record['VariantColor']) listField () ;
	    if ($Record['VariantCertificate']) listField () ;
	    if ($Record['VariantSortation']) listField () ;
	    listField () ;
	    foreach ($Dimension as $dimension => $dummy) {
		listField ($dimension, 'style="border-left:1px solid #cdcabb;"') ;
		listField () ;
	    }
	}
    }

    foreach ($Line as $l => $line) {
	listRow () ;
	listField ($l) ;
	listField ($line['StockName']) ;
	if ($line['StockType'] == 'fixed') print formHidden (sprintf ('StockId[%d]', $l), (int)$line['StockId']) ;	    

	if ($Record['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$line['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$line['ValueRed'], (int)$line['ValueGreen'], (int)$line['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $line['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	    print formHidden (sprintf ('ArticleColorId[%d]', $l), (int)$line['ArticleColorId']) ;	    
	}

	listField ($line['BatchNumber']) ;
	print formHidden (sprintf ('BatchNumber[%d]', $l), $line['BatchNumber']) ;

    	if ($Record['VariantCertificate']) {
	    listField ($line['CertificateTypeName']) ;
	    print formHidden (sprintf ('ArticleCertificateId[%d]', $l), (int)$line['ArticleCertificateId']) ;
	}

	if ($Record['VariantSortation']) {
	    listField ((int)$line['Sortation'], 'align=right') ;
	    print formHidden (sprintf ('Sortation[%d]', $l), (int)$line['Sortation']) ;
	}

	listField () ;

	if ($Record['VariantSize']) $Collumn = $Size ;
	else if ($Record['VariantDimension']) $Collumn = $Dimension ;
	else $Collumn = array (0 => true) ;
	
	foreach ($Collumn as $i => $collumn) {
	    listField (number_format((float)$line['QuantityFree'][$i], (int)$Record['UnitDecimals'], ',', '.'), 'align=right style="border-left:1px solid #cdcabb;"') ;	
	    listFieldRaw (formText (sprintf('Quantity[%d][%d]', $l, $i), number_format((float)$line['QuantityAllocated'][$i], (int)$Record['UnitDecimals'], ',', ''), 8, 'text-align:right;width:55px;margin-right:0;', ($line['StockType']!='fixed' or ((int)$Case['Id']==0 and (int)$OrderLine['Id']==0) and (int)$ReservationId==0) ? 'readonly' : '')) ;
	}
    } 

    if (count($Line) == 0) {
	listRow () ;
	listField ('No Items', 'colspan=2') ;
    }
   
    listEnd () ;
    formEnd () ;
    
    return 0 ;
?>
