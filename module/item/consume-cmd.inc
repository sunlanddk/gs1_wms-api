<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'module/container/include.inc' ;

    $fields = array (
	'Quantity'		=> array ('type' => 'decimal',	'check' => true),
	'ConsumeProductionId'	=> array ('type' => 'integer',	'check' => true),
	'ContainerId'		=> array ('type' => 'set'),
	'PreviousContainerId'	=> array ('type' => 'set'),
	'CaseId'		=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields, $Quantity ;
	switch ($fieldname) {
	    case 'Quantity':
		if ($value == 0) {
		    // Nothing left from production
		    // Directly consume Item
		    $fields['ContainerId']['value'] = 0 ;
		    $fields['PreviousContainerId']['value'] = $Record['ContainerId'] ;
		    $fields['CaseId']['value'] = 0 ;
		    return false ;    
		}

		// Validate Quantity
		if ($value > $Quantity) return sprintf ('left Quantity (%d) higher than Item quantity (%d)', $value, $Record['Quantity']) ;
		if ($value == $Quantity) return sprintf ('nothing consumed') ;	
		return true ;

	    case 'ConsumeProductionId' :
		if ($value == 0) return 'please select Production' ;
		
		// Get Production
		$query = sprintf ('SELECT Production.*, ProductionLocation.TypeSample FROM Production LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId WHERE Production.Id=%d AND Production.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] == 0) return sprintf ('%s(%d) Production not found, id %d', __FILE__, __LINE__, $value) ;

		// Validate Case
		if ($row['CaseId'] != $Record['CaseId']) return sprintf ('%s(%d) Case mismatch', __FILE__, __LINE__) ;

		// Validate State of production
		if ($row['Done'] == 1 or $row['Ready'] == 0 or ($row['Approved'] == 0 and $row['TypeSample'] == 0)) return sprintf ('%s(%d) invalid state for Production', __FILE__, __LINE__) ;
	
		// All consumed ?
		if ($fields['Quantity']['value'] > 0) {
		    // Not all of the Item has been consumed
		    // A new Item will be created for the comsumption
		    // then this field must not be set for the original Item
		    return false ;
		}
		
		return true ;
	}
    }
    
    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    // Is Item real
    if ($Record['ContainerId'] == 0) return sprintf ('%s(%d) Item not real, id %d', __FILE__, __LINE__, $Record['Id']) ;
    $ContainerId = (int)$Record['ContainerId'] ;

    // Is it allowed operate Items on this Stock
    if ($Record['StockType'] != 'fixed') return 'only Items on fixed Stock Location can be consumed' ;
		
    // Get Original Item quantity
    $Quantity = (float)$Record['Quantity'] ;
    
    // Save record
    $res = saveFields ('Item', $Id, $fields, true) ;
    if ($res) return $res ;

    // All items consumed (the original Item has allready been used for the consumption) ?
    if ($Quantity > (float)$Record['Quantity']) {
	// Create specific item for consumption to Production
	// Copy columns for Items
	unset ($item) ;
	$query = 'SHOW COLUMNS FROM Item' ;
	$result = dbQuery ($query) ;
	while ($column = dbFetch ($result)) {
	    $field = $column['Field'] ;
	    $item[$field] = $Record[$field] ;
	}
	dbQueryFree ($result) ;
	
	// Update for consumption
    	$item['ContainerId'] = 0 ;
    	$item['PreviousContainerId'] = $Record['ContainerId'] ;
	$item['Quantity'] = number_format ($Quantity - (float)$Record['Quantity'], (int)$Record['UnitDecimals'], '.', '') ;
	$item['ParentId'] = $Record['Id'] ;
	$item['CaseId'] = 0 ;
	$item['ConsumeProductionId'] = $fields['ConsumeProductionId']['value'] ;
	
	if ($Record['TestDone']) {
	    $item['TestDone'] = 0 ;
	    $item['TestItemId'] = $Record['Id'] ;
	}
	
	// Create Consumption Item
	tableWrite ('Item', $item) ;
    }

    // Check if the container has become empty
    containerDeleteEmpty ($ContainerId) ;
    
    return 0 ;    
?>
