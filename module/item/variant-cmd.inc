<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'ArticleSizeId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'ArticleCertificateId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Dimension'		=> array ('type' => 'decimal',	'mandatory' => true),
	'BatchNumber'		=> array (),
	'Sortation'		=> array ('type' => 'integer',	'mandatory' => true)
    ) ;
    $fields['Dimension']['format'] = sprintf ('9.2', '') ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields ;
	switch ($fieldname) {
	    case 'ArticleColorId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please select Color' ;
		
		// Get Article Color
		$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id=%d AND ArticleColor.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleColor not found, id %d', __FILE__, __LINE__, $value) ;
		if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleColor, id %d', __FILE__, __LINE__, $value) ;
		
		return true ;

	    case 'ArticleSizeId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please select Size' ;
		
		// Get Article Size
		$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.Id=%d AND ArticleSize.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleSize not found, id %d', __FILE__, __LINE__, $value) ;
		if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleSize, id %d', __FILE__, __LINE__, $value) ;
		
		return true ;

	    case 'ArticleCertificateId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please select Certification' ;
		
		// Get Article Certificate
		$query = sprintf ('SELECT ArticleCertificate.* FROM ArticleCertificate WHERE ArticleCertificate.Id=%d AND ArticleCertificate.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleCertificate not found, id %d', __FILE__, __LINE__, $value) ;
		if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleCertificate, id %d', __FILE__, __LINE__, $value) ;
		
		return true ;
	}
	return false ;
    }

    // Validate
    if ((int)$Record['ContainerId'] == 0) return 'consumed items can not be modified' ;
    if ($Record['StockType'] != 'fixed') return 'only items at fixed Stock can be modified' ;

    // Remove not active fields
    if (!$Record['VariantColor']) unset ($fields['ArticleColorId']) ;
    if (!$Record['VariantSize']) unset ($fields['ArticleSizeId']) ;
    if (!$Record['VariantCertificate']) unset ($fields['ArticleCertificateId']) ;
    if (!$Record['VariantDimension']) unset ($fields['Dimension']) ;
    if (!$Record['VariantSortation']) unset ($fields['Sortation']) ;
    
    // Save record
    return saveFields ('Item', $Id, $fields) ;
?>
