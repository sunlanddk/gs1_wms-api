<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'lib/string.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Validate that line array is POSTed
    if (!is_array($_POST['Quantity'])) return 'no lines' ;
    
    // Get Quantity and Container of items to upgrade
    $n = 1 ;
    foreach ($_POST['Quantity'] as $id => $s) {
	// Quantity
	$quantity = strInteger ($s) ;
	if (is_null($quantity)) continue ;
	if ($quantity === false or $quantity <= 0) return sprintf ('invalid Quantity, line %d', $n) ;

	// Container
	$container = strInteger ($_POST['Container'][$id]) ;
	if (is_null($container)) return sprintf ('please specify Container, line %d', $n) ;
	if ($container === false or $container <= 0) return sprintf ('invalid Container, line %d', $n) ;

	// Allready specified
	if (isset($Line[(int)$id])) return sprintf ('Item %d specified twice', $id) ;
	
	// Save
	$Line[(int)$id] = array (
	    'ContainerId'	=> $container,
	    'Quantity'		=> $quantity
	) ;
	$n++ ;
    }

    // Anything selected
    if (count($Line) == 0) return 'nothing selected to upgrade' ;

    // Lookup information
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;
	
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$line['Item'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more') ;
	if ($line['Item']['StockReady']) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
	if ((int)$line['Item']['Sortation'] != 2) return sprintf ('Item %d is not 2nd sortation', $id) ;
	if ($line['Quantity'] > (int)$line['Item']['Quantity']) return sprintf ('Quantity too high for Item %d', $id) ;
	
	// Lookup new container
	$query = sprintf ('SELECT Container.*, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $line['ContainerId']) ;
	$res = dbQuery ($query) ;
	$line['Container'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Container']['Id'] != $line['ContainerId']) return sprintf ('Container %d not found', $line['ContainerId']) ;
	if ($line['Container']['StockReady']) return sprintf ('no operations allowed on Container %d', $line['ContainerId']) ;
    }
    
    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => 'Upgrade 2nd Sortation to 1st Sortation'
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
        
    // Do the opgrade
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;

	// Any to upgrade ?
	if ($line['Quantity'] == 0) continue ;

	if ($line['Quantity'] == (int)$line['Item']['Quantity']) {
	    // The whole Quantity has been upgraded
	    // Just update the Item
	    $Item = array (
		'ItemOperationId' => $ItemOperation['Id'],
		'ContainerId' => $line['ContainerId'],
		'PreviousId' => $id,
		'PreviousContainerId' => (int)$line['Item']['ContainerId'],
		'Sortation' => 1
	    ) ;
	    tableWrite ('Item', $Item, $id) ;	    
//          logPrintVar ($Item, sprintf ('tableWrite, id %d', $id)) ;

	} else {
	    // Just part of the Item are updated
	    // Update Quantity for original Item
	    $Item = array (
		'Quantity' => (int)$line['Item']['Quantity'] - $line['Quantity']
	    ) ;
	    tableWrite ('Item', $Item, $id) ;	    
//	    logPrintVar ($Item, sprintf ('tableWrite, id %d', $id)) ;

	    // Create new Item for the Quantity upgraded
	    unset ($line['Item']['StockReady']) ;
	    $line['Item']['Quantity'] = $line['Quantity'] ;
	    $line['Item']['PreviousContainerId'] = (int)$line['Item']['ContainerId'] ;
	    $line['Item']['ContainerId'] = $line['ContainerId'] ;
	    $line['Item']['Sortation'] = 1 ;
	    $line['Item']['ItemOperationId'] = $ItemOperation['Id'] ;
	    $line['Item']['PreviousId'] = $id ;
	    $line['Item']['ParentId'] = $id ;
	    if ($line['Item']['TestDone']) {
		$line['Item']['TestItemId'] = $id ;
		unset ($line['Item']['TestDone']) ;
		unset ($line['Item']['TestDoneUserId']) ;
		unset ($line['Item']['TestDoneDate']) ;
	    }

	    tableWrite ('Item', $line['Item']) ;
//	    logPrintVar ($line['Item'], sprintf ('tableWrite, new')) ;
	}

	containerDeleteEmpty ((int)$line['Item']['ContainerId']) ;
    }

    // View pick
    return navigationCommandMark ('itemlistoperation', $ItemOperation['Id']) ;
?>
