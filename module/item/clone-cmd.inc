<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'Quantity'		=> array ('type' => 'decimal'),
	'Weight'		=> array ('type' => 'decimal',	'format' => '12.3')
    ) ;

    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    // Validate
    if ((int)$Record['ContainerId'] == 0) return 'consumed items can not be cloned' ;
    if ($Record['StockType'] != 'fixed') return 'only items at fixed Stock can be cloned' ;
    if (!$Record['ArticleTypeFabric']) return 'only supported for Fabrics Articles' ;
    
    // Read fields
    $res = saveFields (NULL, -1, $fields) ;
    if ($res) return $res ;

    // Get full Article information
    $query = sprintf ('SELECT * FROM Article WHERE Id=%d', (int)$Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Article = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Article['Id'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) Article not found, id %d', __FILE__, __LINE__, (int)$Record['ArticleId']) ;

    // Validate Fabrics information
    if ($Article['WidthFull'] == 0) return 'Full Width not entered for this Article' ;
    if ($Article['M2Weight'] == 0) return 'M2 Weight not entered for this Article' ;

    // Get full Container information
    $query = sprintf ('SELECT * FROM Container WHERE Id=%d', (int)$Record['ContainerId']) ;
    $result = dbQuery ($query) ;
    $Container = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Container['Id'] != (int)$Record['ContainerId']) return sprintf ('%s(%d) Container not found, id %d', __FILE__, __LINE__, (int)$Record['ContainerId']) ;

    // Get full Item information
    $query = sprintf ('SELECT * FROM Item WHERE Id=%d', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $Item = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Item['Id'] != (int)$Record['Id']) return sprintf ('%s(%d) Item not found, id %d', __FILE__, __LINE__, (int)$Record['Id']) ;

    // Any weight
    if ($fields['Weight']['value'] > 0) {
	// Validate that no quantity has been entered
	if ($fields['Quantity']['value'] > 0) return 'please only enter either Weight or Quantity' ;
	$Item['Quantity'] = number_format ($fields['Weight']['value'] / ((int)$Article['WidthFull']*(int)$Article['M2Weight']/100/1000), (int)$Record['UnitDecimals'], '.', '') ;
	$Container['GrossWeight'] = $fields['Weight']['value'] + $Container['TaraWeight'] ;

    // Any Quantity
    } else if ($fields['Quantity']['value'] > 0) {	
	$Item['Quantity'] = $fields['Quantity']['value'] ;
	$Container['GrossWeight'] = $Item['Quantity']*(int)$Article['WidthFull']*(int)$Article['M2Weight']/100/1000 + $Container['TaraWeight'] ;

    } else {
	return 'please enter Weight or Quantity' ;
    }	
   
    // Generate Container
    unset ($Container['PreviousTransportId']) ;
    unset ($Container['PreviousStockId']) ;
    tableWrite ('Container', $Container) ;
        
    // Generate new item
    $Item['ContainerId'] = dbInsertedId () ;
    if ($Item['TestDone']) $Item['TestItemId'] = (int)$item['Id'] ;
    unset ($Item['TestDone']) ;
    unset ($Item['PreviousContainerId']) ;
    unset ($Item['PreviousTransportId']) ;
    if ((int)$Item['ParentId'] == 0) $Item['ParentId'] = (int)$Item['Id'] ;
    tableWrite ('Item', $Item) ;

//  require_once 'lib/log.inc' ;
//  logPrintVar ($Container, 'container') ;
//  logPrintVar ($Item, 'item') ;
//  return 'Stop' ;

    return navigationCommandMark ('itemview', dbInsertedId ()) ;

?>
