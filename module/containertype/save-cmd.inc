<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'TaraWeight'		=> array ('type' => 'decimal',	'format' => '6.3'),
	'Volume'		=> array ('type' => 'decimal',	'format' => '6.3')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ContainerType WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'ContainerType allready existing' ;
		return true ;
	}
	return false ;
    }
     
    return saveFields ('ContainerType', $Id, $fields) ;
?>
