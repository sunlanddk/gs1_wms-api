<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    $MyCompanyId = parameterGet ('CompanyMy') ;
    
		
    itemStart () ;
    itemSpace () ;
	itemField ('Consolidated order', (int)$Record['Id']) ;     
    // Form
    //formStart () ;
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;


    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Part Delivery', formCheckbox ('PartDelivery', $Record['PartDelivery'])) ;
    //itemFieldRaw ('Part Delivery Line', formCheckbox ('PartDeliveryLine', $Record['PartDeliveryLine'])) ;
	itemFieldRaw ('Split Invoice', formCheckbox ('ConsolidatedInvoice', $Record['ConsolidatedInvoice'])) ;
    itemSpace () ;
   
    itemEnd () ;
    //formEnd () ;
    printf ("</form>\n") ;

    return 0 ;
?>
