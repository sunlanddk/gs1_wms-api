<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	itemStart();
	itemField('ConsolidateId', $Id);
	itemField('Customer', $Record['CompanyName']);
	itemSpace() ;
	itemEnd ;
	
    listStart () ;
    listRow () ;
    listHead ('Add', 25) ;
    listHead ('Number', 50) ;
    listHead ('ConsolidateId', 80) ;
    listHead ('Description', 150) ;
    listHead ('Season', 120) ;
    listHead ('State', 70) ;
    listHead ('DeliveryDate') ;
 //   listHead ('Sales person', 120) ;
 //   listHead ('Created', 70) ;

	$query = sprintf("SELECT o.*, s.Name as SeasonName
						FROM `Order` o, Season s  
						WHERE o.CompanyId=".$Record['CompanyId']." AND o.Done=0 AND o.Active=1 AND not o.ConsolidatedId=".$Id." and s.id=o.seasonid
						GROUP BY o.Id
						Order by o.id, o.consolidatedid") ;
	$res = dbQuery($query) ;
	formStart() ;
	while ($row = dbFetch($res)) {
		listRow () ;
		listFieldRaw (formCheckBox(sprintf('Order[%d]', $row['Id']), 0)) ;
		listField ($row['Id']) ;
		listField ($row['ConsolidatedId']) ;
		listField ($row['Description']) ;
		listField ($row['SeasonName']) ;
		listField ($row['Ready']?'Confirmed':'Draft') ;
		$query = "SELECT min(deliverydate) as DeliveryDate from orderline where orderid=".$row['Id']." and active=1 group by orderid" ;
		$olres = dbQuery($query) ;
		$olrow = dbFetch($olres) ;
		listField (date ("Y-m-d", dbDateDecode($olrow['DeliveryDate']))) ;
		dbQueryFree ($olres) ;
//		listField ($row['SalesUserName']) ;
//		listField (date ("Y-m-d", dbDateDecode($row['CreateDate']))) ;
	}
	formEnd() ;
    if (dbNumRows($res) == 0) {
		listRow () ;
		listField () ;
		listField ('No open Orders', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($res) ;

    return 0 ;
?>
