<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
	require_once 'lib/table.inc';

	if ($User['Restricted']) {
		$LimitClause = sprintf(' AND User.Loginname IN ( %s ) ', $User['UserList']);
	} else {
		$LimitClause = '';
	}

//    $StateField = 'IF(Order.Done,"Done",IF(NOT Order.Ready,"Defined","Ready"))' ;
    $StateField    = 'IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0, "Draft",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1, "Proposal",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=1, "Confirmed",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Shipped",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Undefined" )))))';

    $queryFields = '`Order`.*,
		    Currency.Symbol AS CurrencySymbol,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, Company.Mail AS Mail,
		    Company.Surplus AS CompanySurplus,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName,
		    OrderDone.Name AS OrderDoneName,
		    Season.Name as SeasonName,
		    ' . $StateField . ' AS State' ;

    $queryTables = '`Order`
		    LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    LEFT JOIN User ON User.Id=Order.SalesUserId
		    LEFT JOIN Season ON Season.Id=Order.SeasonId
		    LEFT JOIN OrderDone ON OrderDone.Id=Order.OrderDoneId' ;

	$companylimit = sprintf('`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) AND `Order`.Draft = 0 ', $User['Id']);
	$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);

	if ($User['Extern']) 
			$companylimit = sprintf('(`Order`.CompanyId = %d) AND `Order`.Draft = 0 ', $User['CompanyId']);


	$query = 'select Id FROM `order` WHERE Active=1 AND ConsolidatedId='.(int)$Record['Id'];
	$ress = dbQuery($query);
	while ($rows = dbFetch($ress)) {

	
		$query = sprintf ('SELECT %s FROM %s WHERE Order.Id=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $rows['Id'], $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Variables
	    $SizeCount = 0 ;
	    
	    // Get OrderLines
	    $Line = array () ;
	    $query = sprintf ('SELECT OrderLine.Id, min(OrderLine.No) as No, 
								 group_concat(OrderLine.No) as NoGroup,
								 count(OrderLine.Id) as NoGroupLines,
								`orderline`.`Description`,
								`orderline`.`InvoiceFooter`,
								`orderline`.`OrderId`,
								`orderline`.`CaseId`,
								`orderline`.`ArticleId`,
								`orderline`.`ArticleColorId`,
								 sum(`orderline`.`Quantity`) as Quantity,
								`orderline`.`Surplus`,
								 sum(`orderline`.`PriceSale`*`orderline`.`Quantity`) as PriceSaleSubTotal,
								`orderline`.`PriceCost`,
								`orderline`.`Discount`,
								`orderline`.`DeliveryDate`,
								`orderline`.`ArticleCertificateId`,
								`orderline`.`PrefDeliveryDate`,
								`orderline`.`RequestComment`,
								`orderline`.`CustArtRef`,
								`orderline`.`ProductionId`,
								`orderline`.`Done`,
								`orderline`.`DoneUserId`,
								`orderline`.`DoneDate`,
								`orderline`.`ConfDeliveryDate`,
								`orderline`.`LineFooter`,
								`orderline`.`AltColorName`,
									Case.CustomerReference, Case.ArticleCertificateId, ArticleColor.Id as ArticleColorId,
									Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantDimension, VariantSortation,
									Color.Description AS ColorDescription, Color.Number AS ColorNumber,
									ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
									Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, (SELECT Number FROM Production WHERE Id = OrderLine.ProductionId) AS ProductionNo,
									CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Description AS CustomsPositionDesc, CustomsPosition.Name as CustomsPos, Country.Name as WorkCountryName, Country.Name as WorkCountryId
		    FROM OrderLine
		    LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
			LEFT JOIN CustomsPosition ON Article.CustomsPositionId=CustomsPosition.Id and CustomsPosition.active=1
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    LEFT JOIN Country ON Article.WorkCountryId=Country.Id
		    WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 
			GROUP BY OrderLine.ArticleId, OrderLine.ArticleColorId
			ORDER BY OrderLine.DeliveryDate, No', $rows['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
//		$Line[(int)$row['Id']] = $row ;
		$Line[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($result) ;

	    // Get size specific quantities for each Line with VariantSize/Dimension
	    foreach ($Line as $i => $l) {
//			if (!$l['VariantSize'] or !$l['VariantDimension']) continue ;

			$Line[$i]['Size'] = array () ;
			if ($l['VariantSize']) {
									$query = sprintf ("SELECT ArticleSize.*, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity, vc.VariantCode as VariantCode, OrderLine.PriceSale, OrderLine.No as OrderLineNo
									FROM ArticleSize
									INNER JOIN OrderQuantity ON OrderQuantity.Active=1 AND OrderQuantity.ArticleSizeId=ArticleSize.Id
									INNER JOIN OrderLine On orderline.orderid=%d and OrderLine.Id=OrderQuantity.OrderLineId and OrderLine.articlecolorid=%d and orderline.active=1
									LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
									WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name
									", 
									$rows['Id'], (int)$l['ArticleColorId'],(int)$l['ArticleId'],(int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
			} else if ($l['VariantDimension']) {
				$query = sprintf ("SELECT OrderQuantity.Dimension as Name, OrderQuantity.Dimension as Id, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity
									FROM OrderQuantity
									WHERE OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1", $i) ;
			} else continue ;
			$result = dbQuery ($query) ;
			$n = 0 ;
			while ($row = dbFetch ($result)) {
				$Line[$i]['Size'][$row['Id']] = $row ;
				$n++ ;
			}
			dbQueryFree ($result) ;

			// Update max size count
			if ($n > $SizeCount) $SizeCount = $n ;
	    }

	    //get quantities from production orders
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Production'] = array () ;
	      $query = sprintf ("SELECT Quantity, ArticleSizeId as Id FROM ProductionQuantity
WHERE ProductionId= %d AND ArticleColorId = %d", $l['ProductionId'], $l['ArticleColorId']) ;
	      $result = dbQuery ($query) ;
	      while ($row = dbFetch ($result)) {
	          $Line[$i]['Production'][(int)$row['Id']] = $row['Quantity'] ;
	      }
	      dbQueryFree ($result) ;

	    }

	    // Get Items Shipped for each Line
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Shipments']= array();
	      $quantity = 0 ;
	      if ($l['VariantSize']) {
		// Get Shipped Quantity by Size
		$query = sprintf ('SELECT Item.ArticleSizeId AS Id, SUM(Item.Quantity) AS Quantity FROM Item INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Item.OrderLineId=%d', $i) ;
			$query = sprintf ('SELECT Item.ArticleSizeId AS Id, 
								SUM(Item.Quantity) AS Quantity 
								FROM orderline, Item 
								INNER JOIN Container ON Container.Id=Item.ContainerId 
								INNER JOIN Stock ON Stock.Id=Container.StockId 
								WHERE OrderLine.OrderId=%d and Item.OrderLineId=Orderline.Id',  $rows['Id']) ;
/*								
		$query = sprintf ('SELECT Item.ArticleSizeId AS Id, SUM(Item.Quantity) AS Quantity 
							FROM Item INNER JOIN OrderLine ON Item.OrderLineId=OrderLine.Id  AND OrderLine.Active=1 INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId 
							WHERE OrderLine.OrderId=%d', $Id) ;
*/
		if ($l['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $l['ArticleColorId']) ;
		$query .= ' AND Item.Active=1 AND Stock.Type="shipment" GROUP BY Item.ArticleSizeId' ;
//die($query) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch ($result)) {
		  if (!isset($l['Size'][(int)$row['Id']])) {
		    logPrintf (logWARNING, '%s(%d) invalid ArticleSizeId (%d) for OrderLine (%d)', __FILE__, __LINE__, (int)$row['Id'], $i) ;
		    continue ;
		  }
		  $Line[$i]['Size'][(int)$row['Id']]['QuantityShipped'] = $row['Quantity'] ;
		  $quantity += $row['Quantity'] ;
		}
		dbQueryFree ($result) ;

		//get shipments
		foreach($Line[$i]['Size'] as $size =>$quant) {
				if ($l['VariantColor']) $color_clause = sprintf (' AND i.ArticleColorId=%d ', $l['ArticleColorId']) ;
				else  $color_clause = '' ;
				$query = sprintf('SELECT s.Id AS ShipmentId, s.Name AS ShipmentName,
					SUM(i.Quantity) AS ShipmentQuantity
					FROM orderline ol, Item i INNER JOIN Container c ON i.ContainerId = c.Id
					INNER JOIN Stock s ON s.Id = c.StockId
					WHERE s.Type = \'shipment\' AND i.Active=1
					and ol.orderid=%d AND i.OrderLineId = ol.id AND i.ArticleSizeId = %d %s
					GROUP BY s.Id, s.Name',  $rows['Id'], $size, $color_clause);

		  $res = dbQuery($query);
		  while ($row = dbFetch($res)){
		    $Line[$i]['Shipments'][$row['ShipmentId']][$size]=$row['ShipmentQuantity'];
		  }
		  dbQueryFree($res);

		}

	      } else {

		    // Get Shipped Quantity
		    $query = sprintf ('SELECT SUM(Item.Quantity) AS Quantity FROM Item INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Item.OrderLineId=%d', $i) ;
		    if ($l['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', $l['ArticleColorId']) ;
		    $query .= ' AND Item.Active=1 AND Stock.Type="shipment"' ;
		    $result = dbQuery ($query) ;
		    $row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    $quantity = $row['Quantity'] ;
	      }
	      $Line[$i]['QuantityShipped'] = $quantity ;

	      $query = sprintf('SELECT s.Id AS ShipmentId, s.Name AS ShipmentName,
SUM(i.Quantity) AS ShipmentQuantity
FROM Item i INNER JOIN Container c ON i.ContainerId = c.Id
INNER JOIN Stock s ON s.Id = c.StockId
WHERE s.Type = \'shipment\' AND i.Active=1
AND i.OrderLineId = %d
GROUP BY s.Id, s.Name', $i);

	      $res = dbQuery($query);
	      while ($row = dbFetch($res)){
		$Line[$i]['Shipments'][$row['ShipmentId']][0]=$row['ShipmentQuantity'];
	      }
	      dbQueryFree($res);
	    }


	    // Get Items Invoices for each Line
	    foreach ($Line as $i => $l) {
	      $Line[$i]['Invoices'] = array();
	      $quantity = 0 ;
	      if ($l['VariantSize']) {
			if ($l['VariantColor']) $color_clause = sprintf (' AND InvoiceLine.ArticleColorId=%d ', $l['ArticleColorId']) ;
			else  $color_clause = '' ;
			// Get Invoiced Quantity by Size
			$query = sprintf ('SELECT InvoiceQuantity.ArticleSizeId AS Id, SUM(InvoiceQuantity.Quantity *IF(Invoice.Credit=1,-1,1)) AS Quantity 
							FROM InvoiceLine INNER JOIN OrderLine ON OrderLine.OrderId=%d and OrderLine.Active=1 INNER JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId INNER JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId=InvoiceLine.Id AND InvoiceQuantity.Active=1 
							WHERE InvoiceLine.OrderLineId=OrderLine.Id AND InvoiceLine.Active=1 %s
							GROUP BY InvoiceQuantity.ArticleSizeId',  $rows['Id'], $color_clause) ;
							// AND Invoice.Ready=1 - also show draft.
		$result = dbQuery ($query) ;
		while ($row = dbFetch ($result)) {
		  if (!isset($l['Size'][(int)$row['Id']])) {
		    logPrintf (logWARNING, '%s(%d) invalid ArticleSizeId (%d) for OrderLine (%d)', __FILE__, __LINE__, (int)$row['Id'], $i) ;
		    continue ;
		  }
		  $Line[$i]['Size'][(int)$row['Id']]['QuantityInvoiced'] = $row['Quantity'] ;
		  $quantity += $row['Quantity'] ;
		}
		dbQueryFree ($result) ;

		//get Invoices
		foreach($Line[$i]['Size'] as $size =>$quant) {
		  $query = sprintf('SELECT c.InvoiceId AS InvoiceId,
						SUM(i.Quantity)*IF(Invoice.Credit=1,-1,1) AS InvoiceQuantity
						FROM InvoiceQuantity i INNER JOIN InvoiceLine c ON c.Id = i.InvoiceLineId INNER JOIN OrderLine ON OrderLine.OrderId=%d and OrderLine.Active=1 
						INNER JOIN Invoice ON c.InvoiceId = Invoice.Id
						WHERE i.Active=1 
						AND c.OrderLineId = OrderLine.Id AND i.ArticleSizeId = %d
						GROUP BY c.Id', $rows['Id'], $size);

		  $res = dbQuery($query);
		  while ($row = dbFetch($res)){
		    $Line[$i]['Invoices'][$row['InvoiceId']][$size]=$row['InvoiceQuantity'];
		  }
		  dbQueryFree($res);

		}
	      } else {
		// Get Invoiced Quantity
		$query = sprintf ('SELECT SUM(InvoiceLine.Quantity)*IF(Invoice.Credit=1,-1,1) AS Quantity FROM InvoiceLine INNER JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId WHERE InvoiceLine.OrderLineId=%d AND InvoiceLine.Active=1 AND Invoice.Ready=1', $i) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$quantity = $row['Quantity'] ;

		$query = sprintf('SELECT c.InvoiceId AS InvoiceId,
SUM(c.Quantity)*IF(Invoice.Credit =1,-1,1) AS InvoiceQuantity
FROM InvoiceLine c INNER JOIN Invoice ON c.InvoiceId = Invoice.Id
WHERE c.Active=1 AND Invoice.Active=1 AND Invoice.Ready=1
AND c.OrderLineId = %d
GROUP BY c.InvoiceId', $i);


		$res = dbQuery($query);
		while ($row = dbFetch($res)){
		  $Line[$i]['Invoices'][$row['InvoiceId']][0]=$row['InvoiceQuantity'];
		}
		dbQueryFree($res);
	      }

	      $Line[$i]['QuantityInvoiced'] = $quantity ;
	    }
		


	    itemStart () ;
	    itemSpace () ;
	    itemField ('Order', (int)$Record['Id']) ;
	    itemField ('Description', $Record['Description']) ;
	    itemSpace () ;
	    itemField ('State', $Record['State']) ;
	    itemSpace () ;
	    itemEnd () ;


	 
	    listStart () ;
	    listRow () ;
	//    listHeadIcon () ;
	    listHead ('Pos', 30) ;
	    listHead ('Article', 75) ;
	    listHead ('Colour', 70) ;
	    listHead ('Description') ;
	    listHead ('Done', 70) ;
	    listHead ('Delivery', 70) ;
		listHead ('', 23) ;
		listHead('', 59);
	    if ($SizeCount > 0) {
			listHead ('Quantity', 50) ;
			for ($n = 1 ; $n < $SizeCount ; $n++) listHead ('', 60) ; 
	    }
	    listHead ('Total', 100, 'align=right') ;
	    listHead ('', 8) ;

	    foreach ($Line as $i => $row1) {
			$SortedLines[(int)$row1['No']] = $row1 ;
	    }
	//    asort($Line) ;

	    foreach ($Line as $i => $row) {
		// Spacing
		listRow () ;
		listField () ;
		//print_r($row['Size']);
		// Size Header
		if ($row['VariantSize']) {
		    listRow () ;
		    listField ('', 'colspan=8') ;
		    foreach ($row['Size'] as $s) listField ($s['Name'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		}

		// Items Ordered
		listRow () ;
	//	listFieldIcon ('orderline.gif', 'orderlineview', (int)$row['Id']) ;
		listField ((int)$row['No']) ;
		listField ($row['ArticleNumber']) ;

		if ($row['VariantColor']) {
		    listField ($row['ColorNumber']) ;	    
		} else {
		    listField () ;
		}

		listField ($row['Description']) ;

		$s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
		listfield ($s);
		
		$t = dbDateDecode($row['DeliveryDate']) ;
		listField (($t > 0) ? date ('Y-m-d', $t) : '') ;

		listField ('Ordered', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
		
		if ($row['VariantSize']) {
	//	    foreach ($row['Size'] as $s) listField ((int)$s['Quantity'], 'align=right') ;
		    foreach ($row['Size'] as $s) listField (number_format((float)$s['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		}
		if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d', $SizeCount-count($row['Size']))) ;

		listField (number_format((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		//link to production order
		listRow();
		listField ('', 'colspan=2') ;
		if ($row['VariantColor'] and (int)$row['ColorGroupId'] > 0) {
		    listFieldRaw (sprintf ('<div style="width:57px;height:15px;margin-left:6px;margin-top:1px;background:#%02x%02x%02x;"></div>', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
		} else {
		    listField () ;
		}
		if($row['ProductionId']){
		  listField ('', 'colspan=2') ;
		  listField(tableGetField('Production','SubDeliveryDate',$row['ProductionId']));
		  listFieldIcon('production.gif', 'ord_production', $row['ProductionId']);
		  listField($row['CaseId'] . "/" . $row['ProductionNo']);
		} else {
		  listField ('', 'colspan=4') ;
		  listField('');
		}
		if($row['VariantSize']){
		  foreach($row['Size'] as $s=>$dmp) {
		    //listField($sizes[$s]);
		    listField (number_format((float)$row['Production'][$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
		  }
		}
		   if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d', $SizeCount-count($row['Size']))) ;

		   //listField (number_format((float)$row['Production']['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;

		// Items Shipped
		listRow () ;
		listField ('', 'colspan=6') ;
		listField ('Shipped', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
		if ($row['VariantSize']) {
		    foreach ($row['Size'] as $s) {
		      listField (number_format((float)$s['QuantityShipped'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		    }
		}

		if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;"', $SizeCount-count($row['Size']))) ;

		listField (number_format((float)$row['QuantityShipped'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;

		// shipments
		foreach($row['Shipments'] as $shp =>$sizes) {
		  listRow();
		  listField('','colspan=4');
		  $t = date('Y-m-d', dbDateDecode(tableGetField('Stock','DepartedDate',$shp))) ;
		  $t = ($t > '0001-01-01') ? $t : '' ;
		  listField($t);
		  listField(tableGetField('Stock','DepartureDate',$shp));
		  listFieldIcon('shipment.gif','ord_shipment',$shp);
		  listField(tableGetField('Stock','Name',$shp));
		  if($row['VariantSize']) {
		    foreach($row['Size'] as $s=>$dmp) {
		      listField (number_format((float)$sizes[$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
		    }
		  }
		     if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style=""', $SizeCount-count($row['Size']))) ;


		}
		// Items Invoiced
		listRow () ;
		listField ('', 'colspan=6') ;
		listField ('Invoiced', 'colspan=2 style="border-bottom: 1px solid #cdcabb;"') ;
		if ($row['VariantSize']) {
		  foreach ($row['Size'] as $s) listField (number_format((float)$s['QuantityInvoiced'], (int)$row['UnitDecimals'], ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		}
		if (count($row['Size']) < $SizeCount) listField ('', sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;"', $SizeCount-count($row['Size']))) ; 
		
		listField (number_format((float)$row['QuantityInvoiced'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
		listField ('') ;
	    
		foreach($row['Invoices'] as $inv =>$sizes) {
		  listRow();
		  listField('','colspan=4');
	  	  $t = date('Y-m-d', dbDateDecode(tableGetField('Invoice','ReadyDate',$inv))) ;
		  $t = ($t > '0001-01-01') ? $t : '' ;
		  listField($t);

	//	  listField(tableGetField('Invoice','ReadyDate',$inv));
		  listField('','colspan=1');
		  listFieldIcon('invoice.gif','ord_invoice',$inv);
		  $_invNo = (int)tableGetField('Invoice','Number',$inv) ;
		  listField($_invNo>0?$_invNo:'draft');
		  if($row['VariantSize']) {
		    foreach($row['Size'] as $s=>$dmp) {
		      //listField($sizes[$s]);
		      listField (number_format((float)$sizes[$s], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
		    }
		  }

		}
		listRow();
		listField('',sprintf('colspan=%d style="border-bottom: 1px solid #cdcabb;height:5px"', $SizeCount + 9));
	    }

	    if (count($Line) == 0) {
		listRow () ;
		listField () ;
		listField ('No OrderLines', 'colspan=2') ;
	    }
	    
	    listEnd () ;
	}

    return 0 ;
?>
