<?php


    require_once 'lib/navigation.inc' ;
    require_once 'lib/fieldtype.inc' ;
    require_once 'lib/lookups.inc';

	if ($User['Restricted']) {
		$LimitClause = sprintf(' AND User.Loginname IN ( %s ) ', $User['UserList']);
	} else {
		$LimitClause = '';
	}

//    $StateField = 'IF(Order.Done,"Done",IF(NOT Order.Ready,"Defined","Ready"))' ;
    $StateField    = 'IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0, "Draft",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1, "Proposal",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=1, "Confirmed",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Shipped",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Undefined" )))))';

    $queryFields = '`Order`.*, `Order`.ConsolidatedId as Id, 
			ConsolidatedOrder.ConsolidatedInvoice, ConsolidatedOrder.PartDelivery,
		    Currency.Symbol AS CurrencySymbol,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, Company.Mail AS Mail, Company.Id as CompanyId,
		    Company.Surplus AS CompanySurplus,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName,
		    OrderDone.Name AS OrderDoneName,
		    Season.Name as SeasonName,
		    ' . $StateField . ' AS State' ;

    $queryTables = '`Order`
		    LEFT JOIN ConsolidatedOrder ON ConsolidatedOrder.Id=Order.ConsolidatedId
		    LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    LEFT JOIN User ON User.Id=Order.SalesUserId
		    LEFT JOIN Season ON Season.Id=Order.SeasonId
		    LEFT JOIN OrderDone ON OrderDone.Id=Order.OrderDoneId' ;

	$companylimit = sprintf('`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) AND `Order`.Draft = 0 ', $User['Id']);
	$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);

    switch ($Navigation['Function']) {
	case 'list' :
		$companylimit = sprintf('`Order`.ToCompanyId = %d AND `Order`.Draft = 0 ', $User['CompanyId']);


		$state_options = array('Ready' => 'Ready', 'Defined' => 'Defined', 'Done' => 'Done');

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',			'db' => 'Order.Id',			'direct' => 'Order.Active=1', 	'type' => FieldType::INTEGER),
		array ('name' => 'Consolidated Id',	'db' => 'Order.ConsolidatedId',								'type' => FieldType::INTEGER),
		array ('name' => 'Company',			'db' => 'CONCAT(Company.Name," (",Company.Number,")")', 	'type' => FieldType::STRING),
		array ('name' => 'State',			'db' => $StateField, 										'type' => FieldType::STRING),
		array ('name' => 'Season',			'db' => 'Season.Id', 										'type' => FieldType::SELECT, 	'options' => getOwnerSeasons($User['CompanyId'])),
		array ('name' => 'Reference',		'db' => 'Order.Reference', 									'type' => FieldType::STRING),
		array ('name' => 'Description',		'db' => 'Order.Description', 								'type' => FieldType::STRING),
		array ('name' => 'Sales person',	'db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', 'type' => FieldType::STRING),
		array ('name' => 'Created',			'db' => 'Order.CreateDate', 								'type' => FieldType::DATE)
	    ) ;

	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Clause
		    $queryClause = sprintf ('Order.SeasonId=%d AND Order.Active=1 AND %s', $Id, $companylimit) ;
		    $HideCompany = false ;
		    break ;

		case 'company' :
		case 'company-new' :
		    // Specific company
		    $query = sprintf ('SELECT Company.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Display
		    $HideCompany = true ;

		    // Navigation
		    if ($Record['TypeCustomer']) {
			navigationEnable ('new') ;
		    }
			navigationEnable ('orderlinesrep') ;

		    // Clause
		    $queryClause = sprintf ('Order.CompanyId=%d AND Order.Active=1 AND %s', $Id, $companylimit) ;
		    break ;

		default :
		    // Display
		    $HideDone = true ;

		    // Clause
		    $queryClause = 'Order.Done=0 AND Order.Active=1 AND ' . $companylimit ;
		    break ;
	    }

		$queryClause .= $LimitClause;

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'orderview') ;
	    return $res ;

	case 'list-req' :
		$queryFields = '`requisition`.*,
				Company.Number AS CompanyNumber, Company.Name AS CompanyName, `order`.id as ToId' ;

		$queryTables = '`requisition`
				LEFT JOIN Company ON Company.Id=requisition.FromCompanyId
				LEFT JOIN `order` ON requisition.id=order.purchaseorderid and `order`.active=1' ;

//		$queryClause = sprintf('`requisition`.CompanyId = %d AND `Order`.Id is null ', $User['CompanyId']);
		$queryClause = sprintf('`requisition`.CompanyId = %d and `requisition`.done=0 and `requisition`.ready=1
		and `order`.id is null', $User['CompanyId']);

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'requisition.Id'),
		array ('name' => 'Company',		'db' => 'CONCAT(Company.Name," (",Company.Number,")")'),
		array ('name' => 'Description',	'db' => 'requisition.Description'),
		array ('name' => 'Created',		'db' => 'requisition.CreateDate')
	    ) ;

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'orderview') ;
	    return $res ;

	case 'deleteso-cmd':
		$query = sprintf ('SELECT o.* FROM `Order` o WHERE o.Id=%d', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;
	case 'addso':
	case 'addso-cmd':
	case 'gen-purchase-cmd' :
	case 'gen-purch-line-cmd' :
	case 'gen-purchase' :
	case 'gen-purchase-line' :
		$query = sprintf ('SELECT %s FROM %s WHERE Order.ConsolidatedId=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;
	case 'basic':
	case 'basic-cmd':
	case 'shop-to-consolidated':
	case 'ready':
	case 'ready-cmd':
		$query = sprintf ('SELECT * FROM ConsolidatedOrder WHERE ConsolidatedOrder.Id=%d', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0;
	case 'confirm' :
	case 'confirm-cmd' :
	case 'view' :
	    global $Result ;				// For direct view only
	    
//	dbQuery("SET CHARACTER SET 'utf8'");
//	dbQuery("SET collation_connection = 'utf8_danish_ci'");
		$companylimit = sprintf('`Order`.ToCompanyId = %d AND `Order`.Draft = 0 ', $User['CompanyId']);
		if ($User['Extern']) {
			$companylimit = sprintf('(`Order`.CompanyId = %d) AND `Order`.Draft = 0 ', $User['CompanyId']);
//			$companylimit = ' 1=1' ;
		} else {
			$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);
		}
	    // Query Order
	    $query = sprintf ('SELECT %s, po.Id as PickOrderId,
					if(po.Packed>0,"Done", if (po.updated, "Updated", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId=0,"Ready","Assigned"))))) as PickOrderState
					FROM %s
					LEFT JOIN PickOrder po ON po.ConsolidatedId=Order.ConsolidatedId  and po.Type="SalesOrder" and po.Active=1 and po.packed=0 
					LEFT JOIN Stock s ON po.toid=s.id
					WHERE Order.ConsolidatedId=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
		$query .= $LimitClause;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    if($Record['OrderTypeId'] == 10){
	    	 // Query OrderLines
		    $query = sprintf ('SELECT OrderLine.*,
		    Article.Number AS ArticleNumber, Article.VariantColor,
		    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
		    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
		    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
		    FROM `order`
		    LEFT JOIN orderline ON orderline.OrderId=`order`.Id AND orderline.Active=1
		    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		    LEFT JOIN Unit ON Unit.Id=Article.UnitId
		    WHERE `order`.ConsolidatedId=%d AND OrderLine.Active=1 ORDER BY OrderLine.No', $Id) ;
		    $Result = dbQuery ($query) ;
	    }

        $query = sprintf ('SELECT `order`.*, season.id as SeasonId, season.Name as SeasonName
            FROM `order`
			LEFT JOIN season ON season.id=`order`.seasonid
            WHERE `order`.ConsolidatedId=%d and `order`.active=1 ', $Id) ;
	    $cosolidatedorders = dbQuery ($query) ;

		$_orders = '' ;
		$res = dbQuery($query);
		while ($row = dbFetch ($res)) {
			if ($_recordset) {
				$_orders .=  ', ' . $row['Id'] ;
			} else {
				$_recordset = 1;
				$_orders .=  $row['Id'] ;
			}
			if ($row['Ready']>0) $Record['Ready'] = 1 ;
		}
		$Record['Orders'] =$Record['Id'].' (S0 ' . $_orders . ')';
		$Record['OrderIds'] = $_orders ;
	    dbQueryFree ($res) ;

//		if ($User['SalesRef']) 
//			navigationPasive ('new') ;
	    // Navigation
	    if ($Record['Done']) {
			navigationPasive ('new') ;
			navigationPasive ('shipment') ;
			navigationPasive ('process') ;
			navigationPasive ('production') ;
	    }

	    if ($Record['Ready']) {
			navigationPasive ('new') ;
	    } else {
			navigationPasive ('shipment') ;
			navigationPasive ('process') ;
		// navigationPasive ('production') ;
	    }

	    return 0 ;

	    return 0 ;

	case 'printorder' :
		global $Result;
		$ProformaInvoice=0;
		$ProformaCustoms=0;
		$query = sprintf ('SELECT * FROM consolidatedorder WHERE consolidatedorder.Id=%d AND consolidatedorder.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    $query = sprintf ('SELECT * FROM `order` WHERE `order`.ConsolidatedId=%d AND `order`.Active=1 AND `order`.Ready=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    //$Result = dbFetch ($res) ;
	    //dbQueryFree ($res) ;

		return 0;
	case 'status' :
		$query = sprintf ('SELECT * FROM consolidatedorder WHERE consolidatedorder.Id=%d AND consolidatedorder.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    
	    return 0;

	case 'linelist' :
	    switch ($Navigation['Parameters']) {
		case 'case' :
		    // Get Case
		    $query = sprintf ("SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1", $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Clause
		    $queryClause = sprintf (' OrderLine.CaseId=%d AND OrderLine.Active=1 AND Order.Id=OrderLine.OrderId', $Id) ;
		    break ;

		default :
		    return sprintf ('%s(%d) invalid parameter "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
	    }
	    // Query OrderLines
	    $query = 'SELECT OrderLine.*,
	    Article.Number AS ArticleNumber, Article.VariantColor,
	    Color.Description AS ColorDescription, Color.Number AS ColorNumber,
	    ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
	    Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals' ;
	    $query .= ', ' . $StateField . ' AS State' ;
	    $query .= ' FROM (OrderLine, `Order`)
	    LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	    LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	    LEFT JOIN Unit ON Unit.Id=Article.UnitId' ;
	    $query .= ' WHERE ' . $queryClause . ' ORDER BY OrderLine.OrderId, OrderLine.No' ;
	    $Result = dbQuery ($query) ;
	    break ;

	case 'add2cart-cmd' :
	case 'add2cart' :
		$query = sprintf ("SELECT cm.articleid as ArticleId, s.name as SeasonName, s.Id as SeasonId, 
								  a.description as ArticleName, a.number as ArticleNumber, a.articletypeid as ArticleTypeId, a.description as ArticleDescription,
								  co.number as ColorNumber, cm.articleColorid as ArticleColorId   
							FROM (collectionmember cm, article a, collection c, season s, articlecolor ac, color co) 
							WHERE cm.Id=%d and a.id=cm.articleid and c.id=cm.collectionid and s.id=c.seasonid and ac.id=cm.articlecolorid and co.id=ac.colorid", $Id) ;
	    $res = dbQuery ($query) ;

	    $Record = dbFetch ($res) ;
  	    dbQueryFree ($res) ;

	    break ;

    }

    return 0 ;
?>
