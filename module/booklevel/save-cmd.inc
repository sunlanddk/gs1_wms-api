<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Level'			=> array ('type' => 'integer',	'mandatory' => true),
	'Name'			=> array (			'mandatory' => true),
	'Description'		=> array (),
	'Delimiter'		=> array (			'mandatory' => true),
	'DisplayOrder'		=> array ('type' => 'integer')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['BookId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Id = -1 ;
	    break ;
    }
     
    return saveFields ('BookLevel', $Id, $fields) ;
?>
