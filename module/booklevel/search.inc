<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    // Filter Bar
    printf ("<div class=bar>\n<table height=100%%>\n<tr>\n") ;
    printf ("<td class=barfield style='color:#7f7c73;'>Keywords</td>\n") ;
    printf ("<td class=barfield style='padding-left:0px;padding-top:1px;padding-bottom:0px;''>\n") ;
    printf ("<form method=GET name=filterform>\n") ;
    printf ("<input type=hidden name=nid value=%d>\n", $Navigation['Id']) ;
    printf ("<input type=text name=filter value=\"%s\" style='margin:0px;padding:0px;width=250px;'>\n", htmlentities($FilterValue)) ;
    printf ("</form>\n") ;
    printf ("</td>\n") ;
    printf ("</tr>\n</table>\n</div>\n") ;
    
    // Header
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Book</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BookName'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BookDescription'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Level', 40) ;
    listHead ('Document', 90) ;
    listHead ('Description') ;
    $lastDocId = 0 ;
    while ($row = dbFetch($Result)) {
	if ($lastDocId == $row['DocId']) continue ;
	$lastDocId = $row['DocId'] ;
        listRow () ;
	listFieldIcon ('doc.gif', 'bookview', $row['Id']) ;
	listField ($row['Level']) ;
	listField ($row['BookLevelName'].$row['BookLevelDelimiter'].$row['Name']) ;
	listField ($row["Description"]) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No documents found', 'colspan=3') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
