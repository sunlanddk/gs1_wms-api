<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['Level'] = 1 ;
	    $Record['DisplayOrder'] = 1 ;
	    $Record['Delimiter'] = '-' ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Level</td><td><input type=text name=Level maxlength=3 style='width:30px' value=%d></td></tr>\n", $Record['Level']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 size=20' value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Delimiter</td><td><input type=text name=Delimiter maxlength=1 style='width:14px' value=%s></td></tr>\n",  htmlentities($Record['Delimiter'])) ;
    printf ("<tr><td class=itemfield>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=3 style='width:30px' value=%d></td></tr>\n", $Record['DisplayOrder']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
