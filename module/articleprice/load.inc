<?php

    define ('MAXQUANTITY', 999999.999) ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query sizes to list
	    $query = sprintf ('SELECT ArticlePrice.*, 
								CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS UserName,
								Supplier.Name as SupplierName, Owner.Name as OwnerName
							FROM ArticlePrice 
							LEFT JOIN User ON User.Id=ArticlePrice.ModifyUserId
							LEFT JOIN Company Supplier ON ArticlePrice.SupplierId=Supplier.Id
							LEFT JOIN Company Owner ON ArticlePrice.OwnerId=Owner.Id
							WHERE ArticlePrice.ArticleId=%d AND ArticlePrice.Active=1 
							ORDER BY ArticlePrice.QuantityStart', $Id) ;
	    $Result = dbQuery ($query) ;

	    // Load Article (parent)
	    $query = sprintf ('SELECT Article.*, Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals FROM Article LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Id=%d', $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Article.*, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals FROM Article LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Article.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Get record
	    $query = sprintf ("SELECT ArticlePrice.*, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantDimension, Article.PriceDimensionAdjust, Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals FROM ArticlePrice INNER JOIN Article ON Article.Id=ArticlePrice.ArticleId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE ArticlePrice.Id=%d AND ArticlePrice.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    return 0 ;
    }

    return 0 ;
?>
