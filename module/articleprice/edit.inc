<?php


    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record['Comment']) ;
	    unset ($Record['QuantityStart']) ;
	    unset ($Record['Purchase']) ;
	    unset ($Record['Logistic']) ;
	    $Id = -1 ;
	    break ;
    }
     
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Supply To', formDBSelect ('OwnerId', $Record['OwnerId']>0?$Record['OwnerId']:$User['CompanyId'], 
		"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE (Internal=1) AND Active=1 ORDER BY Name", 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Quantity', formText ('QuantityStart', number_format($Record['QuantityStart'], (int)$Record['UnitDecimals'], ',', ''), 6, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ;
    itemSpace () ;
    itemFieldRaw ('Purchase Price', formText ('Purchase', number_format($Record['Purchase'],4,',',''), 12, 'text-align:right;') . '') ;
    itemFieldRaw ('Logistic Cost', formText ('Logistic',  number_format($Record['Logistic'],4,',',''), 12, 'text-align:right;') . '') ;
    itemSpace () ;
    itemFieldRaw ('Supplier', formDBSelect ('SupplierId', $Record['SupplierId'], 
		"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE (TypeSupplier=1 or Internal=1) AND Active=1 ORDER BY Name", 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:140px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
