<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/file.inc' ;

    // PDM Locked ?
    if ($Record['TaskId'] <= 0) return 'PDM is not locked' ;
    if ($Record['Ready']) return 'Style has allready been approved' ;

    // User rights
    if (!permAdmin()) {
	if ($User['Id'] != $Record['TaskUserId']) return 'you can not create a new PDM version unless you have locked it your self' ;
    }
    
    // Find newest version number		
    $query = sprintf ('SELECT MAX(Version) as Version FROM StylePDM WHERE StyleId=%d AND Active=1', $Id) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    $version = (int)$row['Version'] ;

    // Check if document upload was succesfull
    switch ($_FILES['Doc']['error']) {
	case 0: break ;
	case 4: return 'Please specify file' ;
	default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
    }
    if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
	return 'document upload failed, file not uploaded' ;
//    if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
//	return 'invalid document size' ;
    if (substr($_FILES['Doc']['type'], -5) != 'excel')
	return sprintf ('not a Microsoft Excel spreadsheet, type %s', $_FILES['Doc']['type']) ;

    // Check version
    if ($version >= 1) {
	// Validate filename
	$file = sprintf ('%s-%dv%d.xls', $Record['Number'], (int)$Record['Version'], $version) ;
	if ($_FILES['Doc']['name'] != $file) return sprintf ('invalid filename, must be: %s', $file) ;
	$version++ ;
    } else {
	$version = 1 ;
    }
	
    // StyleLog record
    $logfields = array (
	'StyleId'	=> array ('type' => 'set',	'value' => $Record['Id']),
	'Header'	=> array ('type' => 'set',	'value' => sprintf ('PDM version %d created', $version)),
	'Text'		=> array (),
	'Approve'	=> array ('type' => 'checkbox',	'check' => true) 
    ) ;
    function checkfield ($fieldname, $value, $changed) {
	switch ($fieldname) {
	    case 'Approve':
		return false ;
	}
    }
    if (($res = saveFields ('StyleLog', -1, $logfields)) !== 0) return $res ;

    // StylePDM record
    $pdmfields = array (
	'StyleId'	=> array ('type' => 'set',	'value' => $Record['Id']),
	'StyleLogId'	=> array ('type' => 'set',	'value' => dbInsertedId ()),
	'Version'	=> array ('type' => 'set',	'value' => $version),
	'Size'		=> array ('type' => 'set',	'value' => $_FILES['Doc']['size']),	
	'Filename'	=> array ('type' => 'set',	'value' => $_FILES['Doc']['name']),
	'Type'		=> array ('type' => 'set',	'value' => $_FILES['Doc']['type'])	
    ) ;
    if (($res = saveFields ('StylePDM', -1, $pdmfields)) !== 0) return $res ;

    //Archive document
    $file = fileName ('pdm', dbInsertedId (), true) ;
    if (!move_uploaded_file ($_FILES['Doc']['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive document', __FILE__, __LINE__) ;
    chmod ($file, 0640) ;
    
    // Set Task done (remove lock)
    $taskfields = array (
	'Done'		=> array ('type' => 'set',	'value' => 1),
	'DoneUserId'	=> array ('type' => 'set',	'value' => $User['Id']),
	'DoneDate'	=> array ('type' => 'set',	'value' => dbDateEncode(time()))
    ) ;
    if (($res = saveFields ('Task', $Record['TaskId'], $taskfields)) !== 0) return $res ;

    // Approve Style
    if ($logfields['Approve']['value']) {
	$Style = array (
	    'Ready'		=> 1,
	    'ReadyUserId'	=> (int)$User['Id'],
	    'ReadyDate'		=> dbDateEncode(time())
	) ;
	tableWrite ('Style', $Style, (int)$Record['Id']) ;
    }

    return 0 ;   
?>
