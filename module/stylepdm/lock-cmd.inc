<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/uts.inc' ;

    // Get date/time for next release
    if (is_null($EndDate = utsDateString($_POST['EndDate']))) return 'invalid date' ;
    if (is_null($t = utsTimeString($_POST['EndTime']))) return 'invalid time' ;
    $EndDate += $t ;
    
    // Initialize
    $dbNow = dbDateEncode(time()) ;
    
    // Validate time interval
    if ($EndDate < time()) return 'time of release can not be in the past' ;    
    
    // Check that Style is not ready
    if ($Record['Ready']) return 'the Style has been set Ready' ;
    
    // Check that not allready locked
    if ($Record['TaskId'] > 0) return 'the PDM is allready locked' ;
    
    // Validate TaskType
    if ($TaskTypeId <= 0) return sprintf ('%s(%d) invalid TaskTypeId, value %d', __FILE__, __LINE__, $TaskTypeId) ;
    
    // Check that Style is not used in approved styles
    $s = '' ;
    $query = sprintf ("SELECT Production.Number FROM Production WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Production.Approved=1 ORDER BY Production.Number", $Record['Id']) ;
    $res = dbQuery ($query) ;
    while ($row = dbFetch ($res)) {
 	if ($s != '') $s .= ', ' ;
	$s .= $row['Number'] ;
    }
    dbQueryFree ($res) ;
    if ($s != '') return sprintf ('the Style is used in the approved ProductionOrders: %s', $s) ;

    // Create Task
    $row = array (
	'RefId'			=> $Record['Id'],
	'TaskTypeId'		=> $TaskTypeId,
	'UserId'		=> $User['Id'],
	'StartDate'		=> $dbNow,
	'EndDate'		=> dbDateEncode($EndDate),
	'Ready'			=> 1,
	'ReadyUserId'		=> $User['Id'],
	'ReadyDate'		=> $dbNow,
	'Accepted'		=> 1,
	'AcceptedUserId'	=> $User['Id'],
	'AcceptedDate'		=> $dbNow
    ) ;
    tableWrite ('Task', $row) ;
        
    return 0 ;
?>
