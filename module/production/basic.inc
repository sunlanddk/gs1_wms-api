<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Header
    print productionHeader ($Record) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Get Id for default Style
	    $query = sprintf ("SELECT Style.Id AS StyleId, Style.ArticleId FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Approved DESC, Style.Version DESC LIMIT 1", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch($result) ;	    
	    dbQueryFree ($result) ;
	    
	    // Default values
	    $Record['MaterialDate'] = dbDateEncode(time()+7*24*60*60) ;
	    $Record['SampleDate'] = dbDateEncode(time()+10*24*60*60) ;
	    $Record['SubDeliveryDate'] = dbDateEncode(time()+14*24*60*60) ;
	    $Record['DeliveryDate'] = dbDateEncode(time()+21*24*60*60) ;
	    $Record['Quantity'] = 0 ;
	    $Record['Efficiency'] = 100 ;
	    $Record['ProductionMinutes'] = 0 ;

	    $Record['Request'] = 1 ;
	    break ;
    }
 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Location', formDBSelect ('ProductionLocationId', (int)$Record['ProductionLocationId'], 'SELECT Id, Name AS Value FROM ProductionLocation WHERE TypeProduction=1 AND Active=1 ORDER BY Name', 'width:150px;')) ;  
    itemSpace () ;
    if ($Record['POType'] == 'devel') {
	    itemFieldRaw ('Project', formDBSelect ('SeasonId', (int)$Record['SeasonId'], sprintf('SELECT Id, Name AS Value FROM Season WHERE CompanyId=%d AND Done=0 AND Active=1 ORDER BY Name', $Record['CompanyId']), 'width:150px;')) ;  
	    itemSpace () ;
	    itemFieldRaw ('Shipping', formDate('MaterialDate', dbDateDecode($Record["MaterialDate"]), null, 'tR')) ;
	    itemFieldRaw ('Approval', formDate('SubDeliveryDate', dbDateDecode($Record["SubDeliveryDate"]), null, 'tR')) ;
	    itemSpace () ;
	    itemFieldRaw ('Quantity', formText ('Quantity', $Record['Quantity'], 5)) ;
	    if ($Record['Id'] > 0) {
		itemSpace () ;
		print htmlFlag ($Record, 'Request') ;
		print htmlFlag ($Record, 'Done', 'Processed') ;
	    }	
	    itemSpace () ;
	    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
	    printf ("<tr><td class=itemfield>Plan</td><td><input type=text name=PlanText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['PlanText'])) ;
	    printf ("<tr><td class=itemfield>Missing</td><td><input type=text name=MissingText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['MissingText'])) ;
    } else {
		if ($Record['TypeSample']) {
		    itemFieldRaw ('Project', formDBSelect ('SeasonId', (int)$Record['SeasonId'], sprintf('SELECT Id, Name AS Value FROM Season WHERE CompanyId=%d AND Done=0 AND Active=1 ORDER BY Name', $Record['CompanyId']), 'width:150px;')) ;  
		    itemSpace () ;
		}
	    itemFieldRaw ('Style version', formDBSelect ('StyleId', (int)$Record['StyleId'], sprintf ('SELECT Id, Version AS Value FROM Style WHERE ArticleId=%d AND Active=1 ORDER BY Version', $Record['ArticleId']), 'width:50px;')) ;  
	    itemSpace () ;
	    itemFieldRaw ('Quantity', formText ('Quantity', $Record['Quantity'], 5)) ;
	    itemFieldRaw ('Minutes', formText ('ProductionMinutes', $Record['ProductionMinutes'], 7)) ;
	    itemFieldRaw ('Efficiency (%)', formText ('Efficiency', $Record['Efficiency'], 5)) ; 
	    itemSpace () ;
	    itemFieldRaw ('Material', formDate('MaterialDate', dbDateDecode($Record["MaterialDate"]), null, 'tR')) ;
	    itemFieldRaw ('SubDeliv', formDate('SubDeliveryDate', dbDateDecode($Record["SubDeliveryDate"]), null, 'tR')) ;
	    itemFieldRaw ('Delivery', formDate('DeliveryDate', dbDateDecode($Record["DeliveryDate"]), null, 'tR')) ;
	    itemSpace () ;
	    itemFieldRaw ('Sample', formCheckbox ('Sample', $Record['Sample'])) ;
	    itemFieldRaw ('SampleDate', formDate('SampleDate', dbDateDecode($Record["SampleDate"]), null, 'tR')) ;
	    itemFieldRaw ('SampleText', formText ('SampleText', $Record['SampleText'], 100, 'width:100%;')) ;
	    if ($Record['Id'] > 0) {
		itemSpace () ;
		print htmlFlag ($Record, 'Request') ;
		print htmlFlag ($Record, 'MaterialSent', 'Req. Fabric') ;
		print htmlFlag ($Record, 'AccSent', 'Req. Acc.') ;
		print htmlFlag ($Record, 'Ready') ;
		print htmlFlag ($Record, 'Fabrics', 'Fabric alloc') ;
		print htmlFlag ($Record, 'Accessories', 'Acc. alloc') ;
		print htmlFlag ($Record, 'Done') ;
	    }	
	    itemSpace () ;
	    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
    }
//	itemFieldRaw ('Base Material', formDate('MaterialDate_Act', dbDateDecode($Record["MaterialDate_Act"]), null, 'tR')) ;
//	itemFieldRaw ('Base SubDeliv.', formDate('SubDeliveryDate_Act', dbDateDecode($Record["SubDeliveryDate_Act"]), null, 'tR')) ;
//	itemFieldRaw ('Base Delivery', formDate('DeliveryDate_Act', dbDateDecode($Record["DeliveryDate_Act"]), null, 'tR')) ;

    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;
    
    return 0 ;
?>
