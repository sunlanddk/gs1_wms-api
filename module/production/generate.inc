<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    switch ($Navigation['Parameters']) {
    	case 'season':
    	case 'seasonwithsizes':
		break ;
	default:
	    itemField ('Order', $Record['Id']) ;
	    itemField ('Description', $Record['Description']) ;
	    itemField ('Customer', $Record['CustomerNumber'] . ', ' . $Record['CustomerName']) ;
		break ;
    }
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Location', formDBSelect ('ProductionLocationId', (int)$Record['ProductionLocationId'], 'SELECT Id, Name AS Value FROM ProductionLocation WHERE TypeProduction=1 AND Active=1 ORDER BY Name', 'width:150px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Sample', formCheckbox ('Sample', $Record['Sample'])) ;
    itemFieldRaw ('SampleDate', formDate('SampleDate', dbDateDecode($Record["SampleDate"]), null, 'tR')) ;
    itemFieldRaw ('SampleText', formText ('SampleText', $Record['SampleText'], 100, 'width:100%;')) ;
    itemSpace() ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:80px;')) ;
    itemEnd () ;
    formEnd () ;
printf('<hr>');
$Preview = true;
include 'module/production/generate-cmd.inc';
printf('');    
    return 0 ;
?>
