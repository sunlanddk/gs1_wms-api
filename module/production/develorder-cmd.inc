<?php

    require_once 'lib/string.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'ProductionLocationId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Number'		=> array ('type' => 'set'),
	'Description'		=> array (),
	'Comment'		=> array (),
	'MaterialDate'		=> array ('type' => 'date',	'check' => true),
	'SubDeliveryDate'	=> array ('type' => 'date',	'check' => true),
	'Request'		=> array ('type' => 'checkbox',	'check' => true),
	'RequestUserId'		=> array ('type' => 'set'),
	'RequestDate'		=> array ('type' => 'set'),
	'Quantity'		=> array ('type' => 'set'),
	'Fabrics'			=> array ('type' => 'set',	'value' => 0),
	'FabricsUserId'		=> array ('type' => 'set',	'value' => (int)$User['Id']),
	'FabricsDate'		=> array ('type' => 'set',	'value' => dbDateEncode(time())),
	'Ready'			=> array ('type' => 'set',	'value' => 0),
	'ReadyUserId'		=> array ('type' => 'set',	'value' => (int)$User['Id']),
	'ReadyDate'		=> array ('type' => 'set',	'value' => dbDateEncode(time())),
	'Accessories'		=> array ('type' => 'set',	'value' => 0),
	'AccessoriesUserId'	=> array ('type' => 'set',	'value' => (int)$User['Id']),
	'AccessoriesDate'	=> array ('type' => 'set',	'value' => dbDateEncode(time())),
	'ProductionStartDate'	=> array ('type' => 'set'),
	'ProductionEndDate'	=> array ('type' => 'set'),
		'PlanText'		=> array (),
		'MissingText'		=> array (),
	'SeasonId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CaseId'		=> array ('type' => 'set',	'value' => (int)$Record['Id'])
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'ProductionLocationId' :
		if ($value <= 0) return 'please select ProductionLocation' ;

		// Validate referance to ProductionLocation
		$query = sprintf ("SELECT ProductionLocation.Id AS ProductionLocationId, ProductionLocation.Name AS ProductionLocationName, ProductionLocation.IndexStart, ProductionLocation.TypeProduction, ProductionLocation.TypeSample, ProductionLocation.DoBundles FROM ProductionLocation WHERE ProductionLocation.Id=%d AND ProductionLocation.Active=1", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['ProductionLocationId'] <= 0) return sprintf ('%s(%d) ProductionLocation not found, id %d', __FILE__, __LINE__, $value) ;
		if (!$row['TypeProduction'] or !$row['TypeSample'])  return sprintf ('%s(%d) invalid ProductionLocation, id %d', __FILE__, __LINE__, $value) ;

		// Save new ProductionLocation information in Record
		$Record = array_merge ($Record, $row) ;

		// Get start index
		$i = (int)$row['IndexStart'] ;

		// Get next index to use
		$query = sprintf ("SELECT MAX(Production.Number) AS Number FROM Production WHERE Production.CaseId=%d AND Production.Id<>%d AND Production.Active=1 AND Production.Number>=%d AND Production.Number<%d", $Record['CaseId'], $Id, $i, $i+49) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if (((int)$row['Number']) >= $i) $i = ((int)$row['Number']) + 1 ;
		$fields['Number']['value'] = $i ;
		
		return true ;

	    case 'SeasonId' :
		if (!$changed) return false ;
		if ($value == 0) return 'no Project selected' ;

		// Save new ProductionLocation information in Record
		$Record['SeasonId'] = $value ;

		return true ;
		

	    case 'Request':
		// Ignore if not setting flag
		if (!$value) return false ;
		
		// Set tracking information
		$fields['RequestUserId']['value'] = $User['Id'] ;
		$fields['RequestDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'MaterialDate':
		$fields['ProductionStartDate']['value'] = $value ;
		return true ;

	    case 'SubDeliveryDate':
		$fields['ProductionEndDate']['value'] = $value ;
		return true ;
	}
	return false ;
    }

    function saveValidate ($changed) {
	global $User, $Record, $Id, $fields ;

	// Validate dates MaterialDate
	if (dbDateDecode($Record['MaterialDate']) > dbDateDecode ($Record['SubDeliveryDate'])) return "Approval date is before shipping date" ;
	
	return true ;
    }

    // Validate Case
    if ($Record['Done']) return 'Case has state Done' ;

    // Get colors
    $query = sprintf ("SELECT ArticleColor.Id, Color.Number AS ColorNumber, Color.Description AS ColorDescription, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM ArticleColor, Color LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Color.Number", $Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Color = array() ;
    while ($row = dbFetch ($result)) $Color[(int)$row['Id']] = $row ;
    dbQueryFree ($result) ;
    if (count($Color) == 0) return 'no Colours assigned' ;

    // Get sizes
    $query = sprintf ("SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Size = array() ;
    while ($row = dbFetch ($result)) $Size[(int)$row['Id']] = $row ;
    dbQueryFree ($result) ;
    if (count($Size) == 0) return 'no Sizes assigned' ;

    // Get Quantities
    $ProductionQuantity = array () ;
    $Quantity = 0 ;
    foreach ($Color as $c) {
	foreach ($Size as $s) {
	    $value = strInteger ($_POST['Quantity'][(int)$s['Id']][(int)$c['Id']]) ;
	    if ($value === false) return sprintf ('invalid Quantity for Colour %s and Size %s', $c['ColorNumber'], $s['Name']) ;
	    if (is_null($value)) continue ;
	    $ProductionQuantity[] = array (
		'ArticleSizeId' 	=> (int)$s['Id'],
		'ArticleColorId' 	=> (int)$c['Id'],
		'Quantity'		=> $value
	    ) ;
	    $Quantity += $value ;
	}
    }

    // Any thing to produce ?
    if ($Quantity == 0) return 'please specify Quantity' ;
    $fields['Quantity']['value'] = $Quantity ;

//require_once 'lib/log.inc' ;
//logPrintVar ($ProductionQuantity, 'ProductionQuantity') ;
//return 'STOP' ;
    
    // Create Production Order
    $res = saveFields ('Production', -1, $fields) ;
    if ($res) return $res ;
    $ProductionId = dbInsertedId () ;

    // Generate specific quantities
    foreach ($ProductionQuantity AS $row) {
	$row['ProductionId'] = $ProductionId ;
	tableWrite ('ProductionQuantity', $row) ;
	
			// Insert new item into production container and stock
//			$item['Active'] = 1 ;
//			$item['Sortation'] = 0 ;
//			$item['ContainerId'] = 135484 ;
			// from record itself
//			$item['FromProductionId'] = $row['ProductionId'] ;
//		    $item['ArticleColorId'] = $row['ArticleColorId'] ;
//	    	$item['ArticleSizeId'] = $row['ArticleSizeId'] ;
			// Entered in UI
//		    $item['Quantity'] = $row['Quantity'] ;
			// Get from record
//			$item['StyleId'] = $Record['StyleId'] ;
//		    $item['CaseId'] = $Record['CaseId'] ;
//			$item['ArticleId'] = $Record['ArticleId'] ;
			// From case record
//			$item['ArticleCertificateId'] = tableGetField ('`Case`', 'ArticleCertificateId', $item['CaseId']) ;
//			tableWrite ('Item', $item) ;
    }
    
    // View new Production
    return navigationCommandMark ('productionview', $ProductionId) ;
?>
