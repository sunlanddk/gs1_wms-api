<?php


    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;

    formStart () ;

    switch ($Navigation['Parameters']) {
	case 'case' :
	    require_once 'lib/item.inc' ;
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Case', $Record['Id'], 'case.gif', 'caseview', $Record['Id']) ;
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }
    
    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 65) ;
    if ($Navigation['Parameters'] <> 'devel') {
	    listHead ('Location', 70) ;
	    listHead ('State', 85) ;
	    listHead ('Fabric', 55) ;
	    listHead ('Acc.', 55) ;		
	    listHead ('Style', 55) ;
    } else {
	    listHead ('Location', 120) ;
	    listHead ('State', 85) ;
    }
    listHead ('Article', 70) ;
    listHead ('Ver', 45) ;
    listHead ('Description') ;
    listHead ('Customer') ;
    listHead ('Quantity', 65, 'align=right') ;
    listHead ('Material', 80, 'align=right') ;
    listHead ('Start', 70, 'align=right') ;
    listHead ('End', 70, 'align=right') ;
    listHead ('SubDel.', 70, 'align=right') ;
    listHead ('Delivery', 70, 'align=right') ;
    listHead ('Allocate', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	switch ($Navigation['Parameters']) {
		case 'devel':
		case 'sample':
		      $ListViewMark = $Navigation['Parameters'] . 'view' ;
			break;
		default:
		      $ListViewMark = 'productionview' ;
			break ;
	}
	listFieldIcon ($Navigation['Icon'], $ListViewMark, $row['Id']) ;
	listField ($row['Number']) ;
	listField ($row['ProductionLocationName']) ;
	listField ($row['State']) ;

	if ($row['ArticleId']) {
		$HasStyle = tableGetField ('Article', 'HasStyle', $row['ArticleId']) ;
	} else {
		$HasStyle = 0 ;
	}
	if ($HasStyle) {
		if ($row['StyleCanceled'] == 1)
			$row['StyleState'] = 'Canceled' ;
		else
			$row['StyleState'] = StyleState_NewStyles ($row['StyleId']) ;
	}

    if ($Navigation['Parameters'] <> 'devel') {
		listField ($row['Fabrics']?'Alloc':($row['MaterialSent']?'Req':'')) ;
		listField ($row['Accessories']?'Alloc':($row['AccSent']?'Req':'')) ;		
		listField ($row['StyleState']) ;
    }
	
	listField ($row['ArticleNumber']) ;
	listField ((int)$row['StyleVersion']) ;
	listField ($row['ArticleDescription']) ;
	listField ($row['CompanyName']) ;
	listField (number_format ((float)$row['Quantity'], 0, ',', '.'), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["MaterialDate"])), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["ProductionStartDate"])), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["ProductionEndDate"])), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["SubDeliveryDate"])), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["DeliveryDate"])), 'align=right') ;
	listFieldRaw (formCheckbox (sprintf('Alloc[%d]', (int)$row['Id']), 0)) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No ProductionOrders', 'colspan=5') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
