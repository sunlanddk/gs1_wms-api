<?php

    require_once 'lib/table.inc' ;

    // Validate Approval
    if ($Record['Ready']) return "ProductionOrders beeing Ready can not be deleted" ;
	
	$query = sprintf("SELECT * FROM item where productionid=%d and caseid=%d and active=1 and containerid>0", $Id, $Record['CaseId']) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	$Alloc = dbNumRows ($result) ;
	dbQueryFree ($result) ;

	if ($Alloc > 0)
		return 'Cant delete production, when raw materials still allocated on stock' ;


    // Delete Production
    tableDelete ('Production', $Id) ;
    tableDeleteWhere ('ProductionQuantity', 'ProductionId='.$Id) ;
    tableDeleteWhere ('Bundle', 'ProductionId='.$Id) ;
    tableDeleteWhere ('BundleWork', 'ProductionId='.$Id) ;
    tableDeleteWhere ('BundleQuantity', 'ProductionId='.$Id) ;
    
    // Reset PO link in sales orders
    $query = sprintf ('UPDATE OrderLine SET ProductionId=0, `ModifyDate`="%s", `ModifyUserId`=%d WHERE `ProductionId`=%d', dbDateEncode(time()), (int)$User['Id'], $Id) ;
    dbQuery ($query) ;


    return 0
?>
