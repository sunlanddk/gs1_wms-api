<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
require_once 'lib/navigation.inc';
    
    // Header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Production</td><td width=100 class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ArticleNumber']), htmlentities($Record['ArticleDescription'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    // Output Bundles
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=65><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Made</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Color</p></td>") ;
    printf ("<td class=listhead width=70 align=right><p class=listhead>Size</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>Total</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>Remaining</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    $sum = array() ;
    foreach ($Bundle as $row) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' style='cursor:pointer;' %s></td>", './image/toolbar/bundle.gif', navigationOnClickMark ('bundle', $row["Id"])) ;
	printf ("<td><p class=list>%08d</p></td>", $row['Id']) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['ColorNumber'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['SizeName'])) ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['Quantity']) ;
	$sum['Quantity'] += $row['Quantity'] ;

	$v = $row['Quantity'] * $Record['RealMinutes'] ;
	$sum['Total'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;

	$v -= $row['UsedMinutes'] ;
       $v -=$row['OutMinutes']*$row['Quantity'] ;
	if ($v < 0) $v = 0 ;
	$sum['Remaining'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;

	printf ("</tr>\n") ;
	$sum['Bundles'] += 1 ;
    }
    if (count($Bundle) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No bundles</p></td></tr>\n") ;
    } else {
	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $sum['Bundles']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $sum['Quantity']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Total']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Remaining']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;
    }
    printf ("</table>\n") ;
    
    return 0 ;
?>
