<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'module/production/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;

    function htmlFlagRevert (&$Record, $field, $name=NULL) {
	if (is_null($name)) $name = $field ;
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $name) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td><input type=checkbox name=%s style='margin-left:4px;' checked> %s, %s</td>", $field, date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td class=itemfield>no</td>") ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }

    print htmlFlagRevert ($Record, 'Request') ;
    print htmlFlagRevert ($Record, 'MaterialSent', $Record['POType']=='devel'?'NA':'Req. Fabric') ;
    print htmlFlagRevert ($Record, 'AccSent', $Record['POType']=='devel'?'NA':'Req. Acc.') ;
    print htmlFlagRevert ($Record, 'Allocated') ;
    print htmlFlagRevert ($Record, 'Fabrics',$Record['POType']=='devel'?'NA':'Fabrics Alloc.') ;
    print htmlFlagRevert ($Record, 'Accessories', $Record['POType']=='devel'?'NA':'Acc. Alloc.') ;
    print htmlFlagRevert ($Record, 'Ready', $Record['POType']=='devel'?'Shipped':'Ready') ;
    print htmlFlagRevert ($Record, 'Approved', $Record['POType']=='devel'?'NA':'Approved') ;
    print htmlFlagRevert ($Record, 'Cut', $Record['POType']=='devel'?'NA':'Cut') ;
//    print htmlFlagRevert ($Record, 'SourceOut') ;
//    print htmlFlagRevert ($Record, 'SourceIn') ;
    print htmlFlagRevert ($Record, 'Packed', $Record['POType']=='devel'?'Approved':'Packed') ;
    print htmlFlagRevert ($Record, 'Done', $Record['POType']=='devel'?'Processed':'Done') ;

    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
   
    return 0 ;
?>
