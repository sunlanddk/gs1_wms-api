<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    
    $fields = array (
	'Request'		=> array ('type' => 'checkbox',	'check' => true),
	'Allocated'		=> array ('type' => 'checkbox',	'check' => true),
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'Fabrics'		=> array ('type' => 'checkbox',	'check' => true),
	'Accessories'		=> array ('type' => 'checkbox',	'check' => true),
	'MaterialSent'		=> array ('type' => 'checkbox',	'check' => true),
	'AccSent'		=> array ('type' => 'checkbox',	'check' => true),
	'Approved'		=> array ('type' => 'checkbox',	'check' => true),
	'Cut'			=> array ('type' => 'checkbox',	'check' => true),
	//	'SourceOut'		=> array ('type' => 'checkbox',	'check' => true),
	//	'SourceIn'		=> array ('type' => 'checkbox',	'check' => true),
	'Packed'		=> array ('type' => 'checkbox',	'check' => true),
	'Done'			=> array ('type' => 'checkbox',	'check' => true)
    ) ;

    $flaglist = '' ;
    $flagcount = 0 ;
    
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	global $flagcount, $flaglist ;
	switch ($fieldname) {
	    default:
		if (!$changed or $value) return false ;
		$flaglist .= sprintf ("        %s\n", $fieldname) ;
		$flagcount++ ;
		return true ;
	}
	return false ;
    }
    
    $res = saveFields ('Production', $Id, $fields, true) ;
    if ($res) return $res ;
    
    // Generate news
    if ($flagcount > 0) {
	unset ($row) ;
	$row['Header'] = sprintf ("ProductionOrder %s reverted", $Record['Number']) ;
	$row['Text'] = sprintf ("The flag%s:\n\n%s\nhas been reverted by %s (%s) at %s", ($flagcount > 1) ? 's' : '', $flaglist, $User['FullName'], $User['Loginname'], date('Y-m-d H:i:s', dbDateDecode($Record['ModifyDate']))) ;
	tableWrite ('News', $row) ;
    }

    return 0 ;
	
?>
