<?php

    require_once "lib/table.inc" ;
    require_once "lib/html.inc" ;
    require_once "lib/navigation.inc" ;

    require_once 'module/production/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    // Header
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Quantity</td><td class=itemfield>%d</td></tr>\n", $Record['Quantity']) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    if (count($Size) == 0) {
	printf ("<p>No Sizes assigned</p>\n") ;
	return 0 ;
    }
    if (count($Color) == 0) {
	printf ("<p>No Colours assigned</p>\n") ;
	return 0 ;
    }

    if ($Record['Approved']) {
	printf ("<p>The orderline has been Approved</p><br>\n") ;
    }
 
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    // Headline
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=150><p class=listhead>Colour/Size</p></td>") ;
    foreach ($Size as $s) printf ("<td align=right class=listhead width=55><p class=listhead>%s</p></td>", $s) ;
    printf ("<td class=listhead width=15></td>") ;
    printf ("<td align=right class=listhead width=45><p class=listhead>Sum</p></td>") ;
    printf ("<td class=listhead width=15></td>") ;
    printf ("<td align=right class=listhead width=75><p class=listhead>Fabric Usage</p></td>") ;
    printf ("<td class=listhead width=15></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;

    // Quantitys
    $total = 0 ;
    foreach ($Color as $cid => $c) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list style='cursor:default;' src='%s'></td>", './image/toolbar/color.gif') ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($c['Number'])) ;
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    printf ("<td align=right><input type=text style='width:45px;text-align:right;margin-right:0;' name=Quantity[%d][%d] maxlength=5 size=5 value='%d'%s></td>", $cid, $sid, $Quantity[$cid][$sid]['Quantity'], ($Record['Ready'])?' readonly':'') ;
	    $sum += $Quantity[$cid][$sid]['Quantity'] ;
	}
	printf ("<td></td>") ;
	printf ("<td align=right class=list><p>%s</p></td>", number_format((float)$sum, 0, ',', '.')) ;
	printf ("<td></td>") ;
      printf ("<td align=left><input type=text style='width:65px;text-align:right;margin-right:0;' name=Consumption[%d][%d] maxlength=5 size=5 value='%s'%s></td>", $cid, $sid, number_format($Quantity[$cid][$sid]['Consumption'],3,',',''), ($Record['Ready'])?'':'') ;
	printf ("<td></td>") ;
	printf ("</tr>\n") ;
	$total += $sum ;
    }

    // Total
    printf ("<tr>") ;
    printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
    printf ("<td style='border-top: 1px solid #cdcabb;'><p class=listhead>Total</p></td>") ;
    foreach ($Size as $s) printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
    printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
    printf ("<td align=right class=list style='border-top: 1px solid #cdcabb;'><p>%s</p></td>", number_format((float)$total, 0, ',', '.')) ;
    printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
    printf ("</tr>\n") ;

    printf ("</table>\n") ;

    printf ("</form>\n") ;
    
    return 0 ;
?>
