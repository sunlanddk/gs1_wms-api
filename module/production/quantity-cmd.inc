<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/string.inc' ;

unset($res);
    
    if ($Record['Ready']) return "Quantities for Ready Production Orders can not be updated" ;

    // Validate the counts
    foreach ($Color as $cid => $c) {
	foreach ($Size as $sid => $s) {
	    $value_string = trim($_POST['Quantity'][$cid][$sid]) ;
	    if ($value_string != '0') $value_string = ltrim($value_string, '0') ;
	    $value = (int)$value_string ;
	    if ($value_string != (string)$value) return sprintf ("invalid count in '%s/%s'", $c['Number'], $s['Name']) ;
	    $Quantity[$cid][$sid]['Value'] = $value ;
	}
      $Consumption[$cid] = strFloat($_POST['Consumption'][$cid][$sid], 3) ;
    }

    // Do database update
//print_r($Quantity);
    foreach ($Color as $cid => $c) {
	foreach ($Size as $sid => $s) {
	    $x = $Quantity[$cid][$sid] ;
	    if (($x['Quantity'] != $x['Value']) or ($x['Consumption'] != $Consumption[$cid]))  {
		if ((int)$x['Id'] > 0) {
		    if ((int)$x['Value'] > 0) {
			// Update existing record
			$fields = array (
			    'Quantity'		=> array ('type' => 'set',	'value' => $x['Value']), 
			    'Consumption'		=> array ('type' => 'set',	'value' => $Consumption[$cid]) 
			) ;
			$res = saveFields ('ProductionQuantity', (int)$x['Id'], $fields) ;
		    } else {
		      $nobundle = dbRead(sprintf('SELECT count(*) as cnt FROM bundle WHERE ProductionId = %s AND Active = 1', $Id));
  			if ($nobundle[0]['cnt'] > 0) return 'Quantity can not be set to 0 when bundles has been created' ;

			// Delete record
			tableDelete ('ProductionQuantity', (int)$x['Id']) ;
		    }
		} else {
		    // Create new record
		    $fields = array (
			'ProductionId'		=> array ('type' => 'set',	'value' => $Id),
			'ArticleSizeId'		=> array ('type' => 'set',	'value' => $sid),
			'ArticleColorId'	=> array ('type' => 'set',	'value' => $cid),
			'Quantity'		=> array ('type' => 'set',	'value' => $x['Value']), 
			'Consumption'		=> array ('type' => 'set',	'value' => $Consumption[$cid]) 
		    ) ;
		    $res = saveFields ('ProductionQuantity', -1, $fields) ;
		}
		
		if ($res) return $res ;
	    }		
	}
    }

    return 0 ;
?>
