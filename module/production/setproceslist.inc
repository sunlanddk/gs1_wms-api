<?php


    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Number', 65) ;
    listHead ('Location', 120) ;
    listHead ('State', 85) ;
    listHead ('Article', 70) ;
    listHead ('Ver', 45) ;
    listHead ('Description') ;
    listHead ('Customer') ;
    listHead ('Quantity', 65, 'align=right') ;
    listHead ('Ship', 80, 'align=right') ;
    listHead ('Approval', 80, 'align=right') ;
    listHead ('Processed', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listField ($row['Number']) ;
	listField ($row['ProductionLocationName']) ;
	listField ($row['State']) ;
	listField ($row['ArticleNumber']) ;
	listField ((int)$row['StyleVersion']) ;
	listField ($row['ArticleDescription']) ;
	listField ($row['CompanyName']) ;
	listField (number_format ((float)$row['Quantity'], 0, ',', '.'), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["MaterialDate"])), 'align=right') ;
	listField (date ("Y-m-d", dbDateDecode($row["SubDeliveryDate"])), 'align=right') ;
	if ($row['Done'])
		listFieldRaw (formCheckbox (sprintf('DoneFlagSet[%d]', (int)$row['Id']), 1, '', DISABLED)) ;
	else
		listFieldRaw (formCheckbox (sprintf('DoneFlag[%d]', (int)$row['Id']), $row['Done'], '')) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No ProductionOrders', 'colspan=5') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
