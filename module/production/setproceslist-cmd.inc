<?php

//    if (!is_array($_POST['AllocFlag'])) return 'please select flag(s) to set' ;
    $PO = array () ;
    foreach ($_POST['DoneFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$PO[$id]['Done'] = 1 ;
	}

    foreach ($PO as $id => $row) {
		unset ($PO_update) ;
		if ($row['Done']) {
			$PO_update['Done'] = $row['Done'];
			$PO_update['DoneUserId'] = $User['Id'];
			$PO_update['DoneDate'] = dbDateEncode(time());
		}
		tableWrite ('production', $PO_update, (int)$id) ;
    }
    return 0 ;
?>
