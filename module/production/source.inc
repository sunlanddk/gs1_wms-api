<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
require_once 'lib/list.inc';
require_once 'lib/form.inc';
    
    // Header
    print productionHeader ($Record) ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    listStart () ;
    listRow () ;
    listHead ('No.', 25) ;
listHead('',3);
listHead('',3);
    listHead ('Location', 140) ;
    listHead ('From No', 60) ;
    listHead ('To No', 60) ;
    listHead ('Article Color', 175) ;
    
    listHead ('Article Size') ;

   
    for ($l = 1 ; $l <= 25 ; $l++) {
      $query = sprintf('SELECT * FROM OutSourcingPlan WHERE ProductionId=%d and Position = %d', $Id, $l);
      $res = dbQuery($query);
      $row = dbFetch($res);
      dbQueryFree($res);
      // check if outsourcing has taken place
      $disable = '';
      $query = sprintf('SELECT count(*) as cnt FROM (OutSourcing s, OutSourcingPlan p) WHERE p.Id=%d AND s.OutDate is not null AND p.LocationId=s.LocationId AND p.OperationFromNo= s.OperationFromNo AND p.OperationToNo=s.OperationToNo AND (p.ArticleColorId = s.ArticleColorId OR p.ArticleColorId is null) AND (p.ArticleSizeId = s.ArticleSizeId OR p.ArticleSizeId is null) AND p.ProductionId = s.ProductionId', $row['Id']);
      $res = dbQuery($query);
      $srow = dbFetch($res);
      dbQueryFree($res);
      if ($srow['cnt']) {$disable=' disabled ';}

	listRow () ;
	if (!$row['LocationId']){$row['LocationId'] = 0;}
	listField ($l) ;
	listFieldRaw(formHidden(sprintf( 'OutSourcingPlanId[%d]',$l), $row['Id']));
listFieldRaw(formHidden(sprintf( 'Locked[%d]',$l), $disable?1:0));
	listFieldRaw (formDBSelect (sprintf('LocationId[%d]', $l), 
				    $row['LocationId'],  'SELECT Id, Name AS Value FROM ProductionLocation WHERE Active=1 AND TypeSew=1 ORDER BY Name','', array(0 => '--none--'), $disable), '') ;

	listFieldRaw (formText (sprintf('OperationFromNo[%d]', $l), $row['OperationFromNo'], 4, 'text-align:right;width:46px;margin-right:0;', $disable), '') ;
listFieldRaw (formText (sprintf('OperationToNo[%d]', $l), $row['OperationToNo'], 4, 'text-align:right;width:46px;margin-right:0;', $disable), '') ;
listFieldRaw (formDBSelect (sprintf('ArticleColorId[%d]', $l), $row['ArticleColorId'],  sprintf('SELECT c.Id, CONCAT(o.Number ,"," , o.Description) AS Value FROM ArticleColor c INNER JOIN Color o ON c.ColorId=o.Id  WHERE ArticleId=(SELECT ArticleId FROM `Case` WHERE Id= %d) ORDER BY o.Number',$Record['CaseId']),'width:150px', array(0 => '--all--'), $disable), '') ;
listFieldRaw (formDBSelect (sprintf('ArticleSizeId[%d]', $l), $row['ArticleSizeId'],  sprintf('SELECT s.Id, s.Name AS Value FROM ArticleSize s WHERE ArticleId=(SELECT ArticleId FROM `Case` WHERE Id= %d) ORDER BY s.DisplayOrder',$Record['CaseId']),'', array(0 => '--all--'),$disable), '') ;


    } 
    listEnd () ;
    formEnd () ;
/*
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td><p>Location</p></td><td>%s</td></tr>\n", htmlDBSelect ("SourceLocationId style='width:250px'", $Record['SourceLocationId'], 'SELECT Id, Name AS Value FROM ProductionLocation WHERE Active=1 AND TypeSew=1 ORDER BY Name', array(0 => 'No OutSourcing'))) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>From</td><td><input type=text name=SourceFromNo size=3 maxlength=3 value=%d></td></tr>\n", $Record['SourceFromNo']) ;
    printf ("<tr><td class=itemlabel>To</td><td><input type=text name=SourceToNo size=3 maxlength=3 value=%d></td></tr>\n", $Record['SourceToNo']) ;
    print htmlItemSpace() ;
    print htmlFlag ($Record, 'SourceOut') ;
    print htmlFlag ($Record, 'SourceIn') ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
*/ 
    return 0 ;
?>
