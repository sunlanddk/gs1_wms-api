<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'module/style/include.inc' ;

    $fields = array (
	'Approved'		=> array ('type' => 'checkbox',	'check' => true),
	'ApprovedUserId'	=> array ('type' => 'set'),
	'ApprovedDate' 		=> array ('type' => 'set')
    ) ;
	
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Approved':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

		// Validate Allocated
		if (!$Record['Allocated']) return "the ProductionOrder must be Allocated before Approval" ;

		// Validate Ready
		if (!$Record['Ready']) return "the ProductionOrder must be Ready before Approval" ;
		
		// Validate style
	    // Added 2007-02-22: Link to new style or legacy style modules!
	    $HasStyle = tableGetField ('Article', 'HasStyle', $Record['ArticleId']) ;
		if (!$HasStyle) {
			// Old pdm style!!!
			if (!$Record['TypeSample'] and !$Record['StyleApproved']) {
			    // The Style has not yet been approved by sales
			    // Validate that Style has been approved by constructor
			    if (!$Record['StyleReady']) return 'the Style has not been approved by Constructor' ;
	
			    // Ensure that no other styles are approved
			    $query = sprintf ('SELECT Style.Id, Style.Version FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 AND Style.Approved=1', (int)$Record['ArticleId']) ;
			    $res = dbQuery ($query) ;
			    $row = dbFetch ($res) ;
			    dbQueryFree ($res) ;
			    if ((int)$row['Id'] > 0) return sprintf ('Style version %d has allready been approved for Sales', (int)$row['Version']) ;
	    
			    // Do Auto Approval of Style
			    $Style = array (
				'Approved'		=> 1,
				'ApprovedUserId'	=> (int)$User['Id'],
				'ApprovedDate'		=> dbDateEncode(time())
			    ) ;
			    tableWrite ('Style', $Style, (int)$Record['StyleId']) ;
	   		}
	   	} else {
			if (!$Record['TypeSample']) { 
		   	    if (isCurrentMaterial($Record['StyleId']))
		   	    if (!isCurrentMaterialClosed($Record['StyleId'])) return 'Open Material Lists';
		   	    if (!isCurrentSketchesClosed($Record['StyleId'])) return 'Open Sketches';
		   	    if (!isCurrentOperationsClosed($Record['StyleId'])) return 'Open Operation Lists';
		   	    if (!isCurrentDesignClosed($Record['StyleId'])) return 'Open Design';
			}
		}	
		
		// Set tracking information
		$fields['ApprovedUserId']['value'] = $User['Id'] ;
		$fields['ApprovedDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }

    if ($Record['Done']) return "no modifications to a Done Production Order" ;    
    if ($Record['TypeSample']) return 'no Approval for Sample ProductionOrders' ;

    return saveFields ('Production', $Id, $fields, true) ;
?>
