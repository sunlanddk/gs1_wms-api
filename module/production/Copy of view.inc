<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;

    // 1. Outher table
    printf ("<table class=item><tr><td width=280 style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
    
    // Header
    printf ("<table class=item>\n") ;
    print htmlItemHeader(NULL, NULL, 'Basic info') ;
    printf ("<tr><td class=itemlabel style='vertical-align:bottom;'>Production</td><td class=itemfield><div style='display:inline;vertical-align:bottom;'>%s</div><img class=list src='./image/toolbar/case.gif' %s></td></tr>\n", htmlentities($Record['Number']), navigationOnClickMark ('caseview', $Record['CaseId'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Location</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ProductionLocationName'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>State</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['State'])) ;
    print htmlItemSpace() ;
//    printf ("<tr><td class=itemlabel style='vertical-align:bottom;'>Article</td><td class=itemfield><div style='display:inline;vertical-align:bottom;'>%s</div><img class=list src='./image/toolbar/article.gif' %s></td></tr>\n", htmlentities($Record['ArticleNumber']), navigationOnClickMark ('articleview', $Record['ArticleId'])) ;
//    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ArticleDescription'])) ;
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
    itemField ('Description', $Record['ArticleDescription']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel style='vertical-align:bottom;'>Style version</td><td class=itemfield><div style='display:inline;vertical-align:bottom;'>%d</div><img class=list src='./image/toolbar/style.gif' %s></td></tr>\n", (int)$Record['StyleVersion'], navigationOnClickMark ('styleview', $Record['StyleId'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel style='vertical-align:bottom;'>Customer</td><td class=itemfield><div style='display:inline;vertical-align:bottom;'>%s</div><img class=list src='./image/toolbar/company.gif' %s></td></tr>\n", htmlentities($Record['CompanyNumber']), navigationOnClickMark ('companyview', $Record['CompanyId'])) ;
    printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['CompanyName'])) ;
    printf ("<tr><td class=itemlabel>Reference</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['CaseCustomerReference'])) ;
    itemField (
	'Certificate',
	((int)$Record['ArticleCertificateId'] <= 0) ? 'None' :
	tableGetFieldWhere (	
	    'ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId',
	    'CertificateType.Name',
	    sprintf ('ArticleCertificate.Id=%d', (int)$Record['ArticleCertificateId'])
	)
    ) ;
	
    itemSpace () ;
    itemFieldIcon ('Fabric', $Record['FabricArticleNumber'], 'article.gif', 'articleview', (int)$Record['FabricArticleId']) ;
    itemField ('Description', $Record['FabricArticleDescription']) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Quantity</td><td class=itemfield>%s</td></tr>\n", number_format((float)$Record['Quantity'], 0, ',', '.')) ;
    printf ("<tr><td class=itemlabel>Time</td><td class=itemfield>%s minutes, one piece</td></tr>\n", number_format((float)$Record['ProductionMinutes'], 2, ',', '.')) ;
    printf ("<tr><td class=itemlabel>Efficiency (%%)</td><td class=itemfield>%s</td></tr>\n", number_format((float)$Record['Efficiency'], 0, ',', '.')) ;
    print htmlItemSpace() ;
    if ($Record['Sample']) {
	printf ("<tr><td class=itemlabel>Sample</td><td class=itemfield>%s</td></tr>\n", date ("Y-m-d", dbDateDecode($Record['SampleDate']))) ;
    	printf ("<tr><td class=itemlabel>Text</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['SampleText'])) ;
    } else {
	printf ("<tr><td class=itemlabel>Sample</td><td class=itemfield>No</td></tr>\n") ;	
    }	    
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Plan</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['PlanText'])) ;
    printf ("<tr><td class=itemlabel>Missing</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['MissingText'])) ;
    printf ("<tr><td class=itemlabel>Bundle</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['BundleText'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    printf ("</td><td style=\"border-bottom: 1px solid #cdcabb;border-left: 1px solid #cdcabb;\">\n") ;
    
    // Sketch
    $file = fileName ('sketch', $Record['StyleId']) ;
    if (is_file($file)) {
	// Sketch exists
	printf ("<img src=index.php?nid=%d&id=%d>\n", navigationMark('sketch'), $Record['StyleId']) ;
    } else {
	// No Sketch
	printf ('<p style="padding-top:4px;">No sketch</p>') ;
    }
    
    // 1. Outher table END
    printf ("</td></tr></table>\n") ;


    // 2. Outher table
    printf ("<table class=item><tr><td width=50%%>\n") ;
    
    // Flags
    printf ("<table class=item>\n") ;
    print htmlItemHeader('Name', 'State', 'Flags') ;
    function htmlFlag (&$row, $field, $name=NULL) {
		if (is_null($name)) $name = $field ;
		printf ("<tr><td class=itemlabel>%s</td><td class=itemfield>%s</td></tr>", $name, ($row[$field]) ? date("Y-m-d H:i:s", dbDateDecode($row[$field.'Date'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $row[$field.'UserId']) :'no') ;
    }
    htmlFlag ($Record, 'Allocated') ;
    htmlFlag ($Record, 'Ready') ;
    htmlFlag ($Record, 'Fabrics') ;
    htmlFlag ($Record, 'Accessories') ;
    htmlFlag ($Record, 'MaterialSent', 'Fabric sent') ;
    htmlFlag ($Record, 'AccSent', 'Acc. sent') ;
    if (!$Record['TypeSample']) {
	htmlFlag ($Record, 'Approved') ;
	htmlFlag ($Record, 'Cut') ;
    }
    if ((int)$Record['SourceLocationId'] > 0 and (int)$Record['SourceFromNo'] > 0) {
	htmlFlag ($Record, 'SourceOut') ;
	htmlFlag ($Record, 'SourceIn') ;
    }
    htmlFlag ($Record, 'Packed') ;
    htmlFlag ($Record, 'Done') ;
    print htmlItemSpace() ;
    printf ("</table><br>\n") ;

    printf ("</td><td width=50%% style=\"border-left: 1px solid #cdcabb;\">\n") ;
 
    // Dates
printf ("<table class=item>\n") ;
//print htmlItemHeader('Type', 'Date', 'Schedule') ;
printf ("<tr><td class=\"itemheader super\" >%s</td></tr>\n", 'Schedule') ;
printf ("</table>\n") ;
printf ("<table class=item>\n") ;
	
	print '<tr><td class=itemheader width=80> Type </td><td class=itemheader> Baseline </td> <td class=itemheader> Actual</td></tr>'.htmlItemSpace() ;
  
  function htmlDate (&$row, $field) {
	printf ("<tr><td class=itemlabel>%s</td><td class=itemfield>%s</td><td class=itemfield>%s</td></tr>", $field, date ("Y-m-d", dbDateDecode($row[$field.'Date'])), is_null(dbDateDecode($row[$field.'Date_Act']))?'':date("Y-m-d",dbDateDecode($row[$field.'Date_Act']))) ;
    }
    htmlDate($Record, 'Material') ;
    htmlDate($Record, 'ProductionStart') ;
    htmlDate($Record, 'ProductionEnd') ;
    htmlDate($Record, 'SubDelivery') ;
    htmlDate($Record, 'Delivery') ;
    print htmlItemSpace() ;
    printf ("</table><br>\n") ;

    // 2. Outher table END
    printf ("</td></tr></table>\n") ;

    // Quantitys
    listStart () ;
    listHeader ('Quantities') ;
    listRow () ;
    listHead ('Color', 80) ;
    listHead ('Type', 55) ;
listHead('', 23);
listHead('', 55);
listHead('',55);
    foreach ($Size as $s) listHead ($s, 60, 'align=right') ;
    listHead ('', 15) ;
    listHead ('Sum', 45, 'align=right') ;
    listHead () ;

    $total = array() ;
    foreach ($Color as $cid => $c) {
      $orderlineref = dbRead(sprintf('SELECT l.Id, l.OrderId, l.No FROM OrderLine l WHERE ArticleColorId=%d AND ProductionId=%d', $c['Id'], $Record['Id']));
      //check if any items
      $sum = 0 ;
      foreach ($Size as $sid => $s) {
	$v = $Quantity[$cid][$sid]['Quantity'] ;
	$sum += $v ;
      }
      if( $sum==0) {continue;}
	// Items specified
	listRow () ;
	listFieldIcon ('color.gif', 'color', (int)$c['Id']) ;
	listField ('specified') ;
	if ($orderlineref[0]['Id']){
	  listFieldIcon('orderline.gif', 'orderline', $orderlineref[0]['Id']);
	  listField(sprintf("%d . %d", $orderlineref[0]['OrderId'],$orderlineref[0]['No']));
	  listField(tableGetField('OrderLine', 'InvoiceFooter', $orderlineref[0]['Id'])) ;
	} else {
	  listField('');listField('');listField('');
	}
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    $v = $Quantity[$cid][$sid]['Quantity'] ;
	    listField (number_format((float)$v, 0, ',', '.'), 'align=right') ;
	    $sum += $v ;
	}
	listField () ;
	listField (number_format((float)$sum, 0, ',', '.'), 'align=right') ;
	$total['Specified'] += $sum ;

	// Items bundled
	listRow () ;
	listField ($c['ColorNumber']) ;
	listField ('bundeled') ;
	listField();
	listField();
	listField();
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    $v = (int)$Quantity[$cid][$sid]['BundleQuantity'] ;
	    listField (number_format((float)$v, 0, ',', '.'), 'align=right') ;
	    $sum += $v ;
	}
	listField () ;
	listField (number_format((float)$sum, 0, ',', '.'), 'align=right') ;
	$total['Bundeled'] += $sum ;

	// Items produced, 1. sortation
	listRow () ;
	listFieldRaw (($c['ColorGroupId'] > 0) ? sprintf ('<div style="width:58px;height:16px;background:#%02X%02X%02X;"></div>', (int)$c['ValueRed'], (int)$c['ValueGreen'], (int)$c['ValueBlue']) : '', 'style="padding-left:6px"') ;
	listField ('1. Sort') ;
listField();
listField();
listField();
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    $v = $Quantity[$cid][$sid]['Sortation'][1] ;
	    listField (number_format((float)$v, 0, ',', '.'), 'align=right') ;
	    $sum += $v ;
	}
	listField () ;
	listField (number_format((float)$sum, 0, ',', '.'), 'align=right') ;
	$total['Stock'] += $sum ;

	// Items produced, 2. sortation
	listRow () ;
	listField ($c['ColorDescription']) ;
	listField ('2. Sort') ;
listField();
listField();
listField();
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    $v = $Quantity[$cid][$sid]['Sortation'][2] ;
	    listField (number_format((float)$v, 0, ',', '.'), 'align=right') ;
	    $sum += $v ;
	}
	listField () ;
	listField (number_format((float)$sum, 0, ',', '.'), 'align=right') ;
	$total['Stock'] += $sum ;

	// Items produced, 3. sortation
	listRow () ;
	listField ('', 'style="border-bottom: 1px solid #cdcabb;"') ;
	listField ('no Sort', 'style="border-bottom: 1px solid #cdcabb;"') ;
listField('', 'style="border-bottom: 1px solid #cdcabb;"');
listField('', 'style="border-bottom: 1px solid #cdcabb;"');
listField('', 'style="border-bottom: 1px solid #cdcabb;"');
	$sum = 0 ;
	foreach ($Size as $sid => $s) {
	    $v = $Quantity[$cid][$sid]['Sortation'][3] ;
	    listField (number_format((float)$v, 0, ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	    $sum += $v ;
	}
	listField ('', 'style="border-bottom: 1px solid #cdcabb;"') ;
	listField (number_format((float)$sum, 0, ',', '.'), 'align=right style="border-bottom: 1px solid #cdcabb;"') ;
	listField ('', 'style="border-bottom: 1px solid #cdcabb;"') ;
	$total['Stock'] += $sum ;
    }

    listRow () ;
    printf ("<td><p class=listhead>Total</p></td>") ;
    printf ("<td><p class=list>specified</p></td>") ;listField();listField();listField();
    foreach ($Size as $s) printf ("<td></td>") ;
    printf ("<td></td>") ;
    printf ("<td align=right class=list><p>%s</p></td>", number_format((float)$total['Specified'], 0, ',', '.')) ;
    listRow () ;
    printf ("<td></td>") ;
    printf ("<td><p class=list>bundeled</p></td>") ;listField();listField();listField();
    foreach ($Size as $s) printf ("<td></td>") ;
    printf ("<td></td>") ;
    printf ("<td align=right class=list><p>%s</p></td>", number_format((float)$total['Bundeled'], 0, ',', '.')) ;
    listRow () ;
    printf ("<td></td>") ;
    printf ("<td><p class=list>stock</p></td>") ;listField();listField();listField();
    foreach ($Size as $s) printf ("<td></td>") ;
    printf ("<td></td>") ;
    printf ("<td align=right class=list><p>%s</p></td>", number_format((float)$total['Stock'], 0, ',', '.')) ;
    listRow () ;
    listEnd () ;
    printf ("<br>\n") ; 

    // Comments
    printf ("<table class=item><tr><td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
    itemStart () ;
    itemHeader ('ProductionOrder') ;
    itemFieldText ('Comment', $Record['Comment']) ;
    itemInfo($Record) ;
    itemEnd () ;
    printf ("<br></td><td width=50%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
    itemStart () ;
    itemHeader ('Article') ;
    itemFieldText ('Comment', $Record['ArticleComment']) ;
    itemEnd () ;
    printf ("<br></td></tr></table>") ;

    return 0 ;
?>
