<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    print htmlFlag ($Record, 'Approved') ;
    print htmlItemInfo ($Record) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
    
    return 0 ;
?>
