<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    require_once 'lib/form.inc' ;

    // Header
    print productionHeader ($Record) ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
if ($Record['POType'] == 'devel') {
    printf ("<tr><td class=itemfield>Plan</td><td><input type=text name=PlanText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['PlanText'])) ;
    printf ("<tr><td class=itemfield>Missing</td><td><input type=text name=MissingText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['MissingText'])) ;
    print htmlItemSpace() ;
    print htmlFlag ($Record, 'Allocated') ;
    print htmlFlag ($Record, 'Ready', 'Shipped') ;
    print htmlFlag ($Record, 'Packed', 'Approved') ;
} else {
    printf ("<tr><td class=itemlabel>Start</td><td>%s</td></tr>\n", formDate ('ProductionStartDate', dbDateDecode($Record["ProductionStartDate"], null, 'tR'))) ;
    printf ("<tr><td class=itemlabel>End</td><td>%s</td></tr>\n", formDate ('ProductionEndDate', dbDateDecode($Record["ProductionEndDate"], null, 'tR'))) ;
//    printf ("<tr><td class=itemlabel>SubDelivery</td><td>%s</td></tr>\n", formDate ('SubDeliveryDate', dbDateDecode($Record["SubDeliveryDate"], null, 'tR'))) ;
    print htmlItemSpace() ;
//	printf ("<tr><td class=itemlabel>Baseline Start</td><td>%s</td></tr>\n", formDate ('ProductionStartDate_Act', dbDateDecode($Record["ProductionStartDate_Act"], null, 'tR'))) ;
//	printf ("<tr><td class=itemlabel>Baseline End</td><td>%s</td></tr>\n", formDate ('ProductionEndDate_Act', dbDateDecode($Record["ProductionEndDate_Act"], null, 'tR'))) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Plan</td><td><input type=text name=PlanText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['PlanText'])) ;
    printf ("<tr><td class=itemfield>Missing</td><td><input type=text name=MissingText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['MissingText'])) ;
    printf ("<tr><td class=itemfield>Bundle</td><td><input type=text name=BundleText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['BundleText'])) ;
    print htmlItemSpace() ;
    print htmlFlag ($Record, 'Allocated') ;
    if (!$Record['TypeSample']) {
	print htmlFlag ($Record, 'ProdDocRec', 'Spec received') ;
	print htmlFlag ($Record, 'Cut') ;
    }
    print htmlFlag ($Record, 'Packed') ;
}
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
