<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
	'Sortation'		=> array ('type' => 'integer',	'check' => true),

	'StockId'		=> array ('type' => 'integer',	'check' => true),
	'ContainerTypeId'	=> array ('type' => 'integer',	'check' => true),
	'GrossWeight'		=> array ('type' => 'decimal',	'format' => '12.3',	'mandatory' => true),

	'Description'		=> array ('type' => 'set'),
	'TaraWeight'		=> array ('type' => 'set'),
	'Volume'		=> array ('type' => 'set'),

	'ContainerId'		=> array ('type' => 'integer',	'check' => true),

    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $Sortation, $fields ;
	switch ($fieldname) {
	    case 'ContainerId' :
		if ($value == 0) return 'please specify Container' ;
		
		// Get Container
		$query = sprintf ('SELECT Container.*, Stock.Id AS StockId, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone FROM Container LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] == 0) return sprintf ('Container not found, id %d', $value) ;

		// New Items are only allowed on fixed stock locations
		if ($row['StockType'] != 'fixed') return 'produced Items must be put into a fixed Stock Locations' ;
		
	        // Is it allowed operate Items on this Stock
		if ($row['StockDone']) return sprintf ('now Stock Location is done') ;
		if ($row['StockReady'] and !$row['StockVerified']) return sprintf ('new Stock Location is locked') ;

		// Do nothine - we just need the container Id for making the Items
		return false ;

	    case 'StockId' :
		// Any Stock selected ?
		if ($value == 0) return 'please select Stock' ;
		
		// Get new Stock
		$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $value) ;

		// New Items are only allowed on fixed stock locations
		if ($row['Type'] != 'fixed') return 'produced Items must be put into a fixed Stock Locations' ;
		
		// Is it allowed to move containers into the new Stock Location
		if ($row['Done']) return sprintf ('%s(%d) new Stock is done, id %d', __FILE__, __LINE__, $value) ;
		if ($row['Ready'] and !$row['Verified']) return sprintf ('%s(%d) new Stock is locked, id %d', __FILE__, __LINE__, $value) ;

		// Set Description for new Container
		$fields['Description']['value'] = sprintf ('Production %s, sort %d', $Record['Number'], $fields['Sortation']['value']) ;
		return true ;

	    case 'ContainerTypeId' :
		// Any Container Type selected ?
		if ($value == 0) return 'please select Container type' ;
		
		// Get ContainerType
		$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] <= 0) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $value) ;
	    
		// Set defaults for new Containers
		$fields['TaraWeight']['value'] = $row['TaraWeight'] ;
		$fields['Volume']['value'] = $row['Volume'] ;

		return true ;

	    case 'Sortation' :
		// Validate
		if ($value < 1 or $value > 3) return 'invalid Sortation' ;

		// Save Sortation
		$Sortation = $value ;

		// Don't save in container !
		return false ;
	}
    }

    switch ($Navigation['Parameters']) {
	case 'stock' :
	    unset ($fields['ContainerId']) ;
	    break ;

	case 'container' :
	    unset ($fields['StockId']) ;
	    unset ($fields['GrossWeight']) ;
	    unset ($fields['ContainerTypeId']) ;
	    break ;

	default :
	    return sprintf ('%s(%d) invalid parameters, value "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
    }

    if (!$Record['Allocated']) return 'the ProductionOrder has not been Allocated' ;
    if (!$Record['Ready']) return 'the ProductionOrder has not been set Ready' ;
    if (!$Record['Approved'] and !$Record['TypeSample']) return 'the ProductionOrder has not been Approved' ;
    if ($Record['Packed']) return 'the ProductionOrder has allready been Packed' ;

    // Get and verify quantities
    $Quantity = array () ;
    foreach ($Color as $cid => $c) {
	    foreach ($Size as $sid => $s) {
		$value_string = trim($_POST['Quantity'][$cid][$sid]) ;
		if ($value_string == '') continue ;
		if ($value_string != '0') $value_string = ltrim($value_string, '0') ;
		$value = (int)$value_string ;
		if ($value_string != (string)$value or $value == 0) return sprintf ("invalid count in '%s/%s'",  $c['Number'], $s['Name']) ;
		$Quantity[] = array ('ArticleColorId' => $cid, 'ArticleSizeId' => $sid, 'Quantity' => $value) ;
	    }
    }

    // Any quantities ?
    if (count($Quantity) == 0) return 'please specify some Quantities' ;

    // Save record
    $res = saveFields ('Container', -1, $fields) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'stock' :
	    $ContainerId = dbInsertedId () ;
	    break ;

	case 'container' :
	    $ContainerId = $fields['ContainerId']['value'] ;
	    break ;

	default :
	    return sprintf ('%s(%d) invalid parameters, value "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
    }

    // Generate the new items
    foreach ($Quantity as $item) {
	$item['Active'] = 1 ;
	$item['ArticleId'] = $Record['ArticleId'] ;
	$item['ContainerId'] = $ContainerId ;
	$item['FromProductionId'] = $Record['Id'] ;
	$item['ArticleCertificateId'] = $Record['ArticleCertificateId'] ;
	$item['Sortation'] = $Sortation ;
	tableWrite ('Item', $item) ;
    }
    

    switch ($Navigation['Parameters']) {
	case 'stock' :
    	    // View new Container
	    return navigationCommandMark ('containerview', $ContainerId) ;
    }
    
    return 0 ;    
?>
