<?php

    require_once "lib/navigation.inc" ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Mark</p></td>") ;
    printf ("<td class=listhead width=300><p class=listhead>Value</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/'.$Navigation['Icon'], navigationOnClickLink ("edit", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Mark'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Value'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No parameters</p></td></tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
