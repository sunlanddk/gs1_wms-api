<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

//	$_upload_file_name =$upload_file_name ;
	function upload_my($upload_file_name, $target_name) {
		switch ($_FILES[$upload_file_name]['error']) {
			case 0: 
			if (!is_uploaded_file($_FILES[$upload_file_name]['tmp_name'])) {
				return 'photo upload failed, file not uploaded' ;
			}
			if ($_FILES[$upload_file_name]['size'] != filesize($_FILES[$upload_file_name]['tmp_name']))
				return 'invalid photo size' ;
			if ($_FILES[$upload_file_name]['size'] > 10000000)
				return 'photo too big, max 50 MB' ;

			// Set flag for photo upload	
			$photoUpload = true ;		    
			break ;
			
			case 4: 		// No file specified
			$photoUpload = false ;
			break ;
			
			default: return sprintf ('Doc1ument upload failed, code %d', $_FILES[$upload_file_name]['error']) ;
		}
		
		if ($photoUpload) {
			// Archive photo
			require_once 'lib/file.inc' ;
			$file =	'image/thumbnails/' . $target_name . '.jpg' ;
			if (!move_uploaded_file ($_FILES[$upload_file_name]['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive Doc1ument %s', __FILE__, __LINE__, $file) ;
			chmod ($file, 0777) ;
		}
		return 0 ;
	}
	
	$res=upload_my('Doc1', 'loginheader') ;
	if ($res!=0) return $res ;

	$res=upload_my('Doc2', 'loginmain') ;
	if ($res!=0) return $res ;

	$res=upload_my('Doc3', 'frontpage') ;
	if ($res!=0) return $res ;

	return 0 ;
?>
