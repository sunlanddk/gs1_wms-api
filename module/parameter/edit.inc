<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/perm.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    unset ($Record) ;

	    // Default values
	    $Record['Value'] = '0' ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    if (permAdminSystem()) {
        printf ("<tr><td class=itemlabel>Mark</td><td><input type=text name=Mark maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Mark'])) ;
	printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    } else {
        printf ("<tr><td class=itemlabel>Mark</td><td><p>%s</p></td></tr>\n", htmlentities($Record['Mark'])) ;
	printf ("<tr><td class=itemlabel>Description</td><td><p>%s</p></td></tr>\n", htmlentities($Record['Description'])) ;    
    }
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Value</td><td><input type=text name=Value maxlength=100 size=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Value'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
