<?php


class StyleParser {
	
	private $filename;
	private $skipErrors = false;
	private $error_count = 0;
	private $error_messages = array();

	function __construct($filename, $skipErrors = false){
		$this->filename = $filename;
		$this->skipErrors = $skipErrors;
	}
	
	public function parse(){
		$styles = array();
		
		$reader = new XMLReader();
		$reader->open($this->filename);
		
		$style = array();
		$error_raised = false;
		while($reader->read() && ($this->skipErrors || !$error_raised)) {
		    try {
		       switch($reader->nodeType) {
		          case (XMLREADER::ELEMENT):
		             if ($error_raised) break;
		             $node_name  = $reader->name;
		             	
		             $attrs = array(StyleParser::ID, StyleParser::STATE, StyleParser::NAME, StyleParser::NUMBER, StyleParser::DESCRIPTION, StyleParser::URL, StyleParser::PICT_URL, StyleParser::COMPANY);
		             	
		             if($node_name==StyleParser::STYLE){
		                $style = array();
		             } else if(in_array($node_name, $attrs)){
		                $node = $reader->expand();
		                $style[$node_name] = $node->nodeValue;
		                $reader->next();
		       
		             } else if($node_name==StyleParser::BRAND){
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_node($node, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::SUPPLIER) {
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_node($node, array(StyleParser::ID, StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::SEASON) {
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_node($node, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::PROJECT) {
		                $reader->next();
		       
		             } else if($node_name==StyleParser::GROUP) {
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_node($node, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::CATEGORIES) {
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_nodes($node, StyleParser::CATEGORY, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::STYLE_COLORS) {
		                $node = $reader->expand();
		                $style[$node_name] = $this->parse_nodes($node, StyleParser::STYLE_COLOR, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::SIZE_RANGE) {
		                $node = $reader->expand();
		                //$style[$node_name] = $this->parse_size_range($node);
		                $style[$node_name] = $this->parse_node_of_nodes($node, array(StyleParser::NAME), StyleParser::SIZES, StyleParser::SIZE, array(StyleParser::NAME));
		                $reader->next();
		       
		             } else if($node_name==StyleParser::ITEMS) {
		                // scip items section
		                $reader->next();		       
						
		             } else if($node_name==StyleParser::CUSTOMFIELDS) {
		                // scip items section
		                $reader->next();		       
		             
		             } else if($node_name==StyleParser::SAMPLEREQUESTS) {
		                // scip items section
		                $reader->next();		       
		             
		             } else if($node_name==StyleParser::PRICES) {
		                // scip items section
		                $reader->next();		       
		             }
		             	
		             //$reader->read();
		             break;
		          case (XMLREADER::END_ELEMENT):
		             $node_name  = $reader->name;
		             if($node_name==StyleParser::STYLE){
		                if(!$error_raised){
		                   array_push($styles, $style);
		                }else{
		                   $error_raised = false;
		                }
		             }
		             break;
		       }
		    } catch (Exception $e) {
		       $error_raised = true;
		       $this->error_count++;
		       array_push($this->error_messages, $e->getMessage());
		    }
		}
		
		return $styles;
	}
	
	public function get_error_count(){
	   return $this->error_count;
	}
	
	public function get_error_messages(){
	   return $this->error_messages;
	}
	
	/*private function parse_usage_in_style(DOMNode $node){
		$res = array();
		
		$style_colors = array();
		if($node->hasChildNodes()){
			$attrs = array(StyleParser::PLACEMENT_DESC, StyleParser::SIZE, StyleParser::QUANTITY);
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if(in_array($name, $attrs)){
					$res[$name] = $child->nodeValue;
				} else if($name == StyleParser::STYLE_COLOR){
					$children = array();
					if($child->hasChildNodes()){
						foreach($child->childNodes as $child_child) {
							$child_name = $child_child->nodeName;
							if($child_name == $subnode_name){
								$subsubnode = $this->parse_node_with_subnode($child_child, array(StyleParser::NAME), StyleParser::COLOR_CARD, array(StyleParser::NAME));
								array_push($children, $subsubnode);
							}
						}
					}
					$res[$name] = $children;
				}
			}
		}
		return $res;
	}*/
	
	private function parse_node_of_nodes_with_subnode(DOMNode $node, $attrs, $subnodes_name, $subnode_name, $subnode_attrs, $subsubnode_name, $subsubnode_attrs){
		$res = array();
		if($node->hasChildNodes()){
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if(in_array($name, $attrs)){
					$res[$name] = $child->nodeValue;
				} else if($name == $subnodes_name){
					$children = array();
					if($child->hasChildNodes()){
						foreach($child->childNodes as $child_child) {
							$child_name = $child_child->nodeName;
							if($child_name == $subnode_name){
								$subsubnode = $this->parse_node_with_subnode($child_child, $subnode_attrs, $subsubnode_name, $subsubnode_attrs);
								array_push($children, $subsubnode);
							}
						}
					}
					$res[$name] = $children;
				}
			}
		}
		return $res;
	}

	private function parse_node_with_subnode(DOMNode $node, $attrs, $subnode_name, $subnode_attrs){
		$res = array();
		if($node->hasChildNodes()){
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if(in_array($name, $attrs)){
					$res[$name] = $child->nodeValue;
				} else if($name == $subnode_name){
					$subnode = $this->parse_node($child, $subnode_attrs);
					$res[$name] = $subnode;
				}
			}
		}
		return $res;
	}
	
	private function parse_node_of_nodes(DOMNode $node, $attrs, $subnodes_name, $subnode_name, $subnode_attrs){
		$res = array();
		if($node->hasChildNodes()){
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if(in_array($name, $attrs)){
					$res[$name] = $child->nodeValue;
				} else if($name == $subnodes_name){
					$children = $this->parse_nodes($child, $subnode_name, $subnode_attrs);
					$res[$name] = $children;
				}
			}
		}
		return $res;
	}
	
	private function parse_nodes(DOMNode $node, $subnode_name, $subnode_attrs){
		$res = array();
		if($node->hasChildNodes()){
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if($name == $subnode_name){
					$item = $this->parse_node($child, $subnode_attrs);
					array_push($res, $item);
				}
			}
		}
		return $res;
	}

	private function parse_node(DOMNode $node, $attributes){
		$res = array();
		if($node->hasChildNodes()){
			foreach($node->childNodes as $child) {
				$name = $child->nodeName;
				if(in_array($name, $attributes)){
					$res[$name] = $child->nodeValue;
				}
			}
		}
		return $res;
	}


	const STYLE = 'style';
	const ID = 'id';
	const NAME = 'name';
	const STATE = 'state';
	const NUMBER = 'number';
	const DESCRIPTION = 'description';
	const URL = 'url';
	const PICT_URL = 'pictureUrl';
	const COMPANY = 'company';
	const BRAND = 'brand';
	const SUPPLIER = 'supplier';
	const SEASON = 'season';
	const PROJECT = 'project';
	const GROUP = 'group';
	
	const CATEGORIES = 'categories';
	const CATEGORY = 'category';
	
	const STYLE_COLORS = 'styleColors';
	const STYLE_COLOR = 'styleColor';
	
	const SIZE_RANGE = 'sizeRange';
	const SIZES = 'sizes';
	const SIZE = 'size';
	
	const ITEMS = 'items';
	const ITEM = 'item';
	const COMPOSITIONS = 'compositions';
	const COMPOSITION = 'composition';
	const USAGE_IN_STYLE = 'usageInStyle';
	const PLACEMENT_DESC = 'placementDescription';
	const QUANTITY = 'quantity';
	const COLOR_CARD = 'colorCard';

	const CUSTOMFIELDS = 'customFields';
	const SAMPLEREQUESTS = 'sampleRequests';
	const PRICES = 'prices';
	
}

return 0;
?>