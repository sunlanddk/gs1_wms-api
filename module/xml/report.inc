<?php

    require_once 'lib/http.inc' ;

    $query = '
	SELECT
	Order.Id AS OrderNumber,
	Company.Number AS CustomerNumber,
	Company.Name AS CustomerName,
	Order.Reference AS CustomerReference,
	OrderLine.No AS Pos,
	Article.Number AS ArticleNumber,
	Color.Number AS ColorNumber,
	OrderLine.DeliveryDate,
	OrderLine.PriceSale AS Price,
	OrderLine.Surplus,
	OrderLine.Discount,
	OrderLine.Quantity,
	Unit.Name AS Unit
	FROM `Order`
	INNER JOIN OrderLine ON OrderLine.OrderId=Order.Id AND OrderLine.Active=1
	LEFT JOIN Company ON Company.Id=Order.CompanyId
	LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
	LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
	LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
	LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
	LEFT JOIN Unit ON Unit.Id=Article.UnitId
	WHERE Order.Ready=1 AND Order.Done=0 AND Order.Active=1 
	ORDER BY Order.Id, OrderLine.No' ;
    $result = dbQuery ($query) ;

//  httpNoCache () ;
    header ('Content-Type: text/xml; charset=iso-8859-1') ;
    printf ("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n") ;

    printf ("<data>\n") ;
    while ($row = dbFetch ($result)) {
	printf (" <line>\n") ;
	foreach ($row as $field => $value) {
	    printf ("  <%s>%s</%s>\n", strtolower($field), htmlspecialchars($value), strtolower($field)) ;
	}
	printf (" </line>") ;
    }
    printf ("</data>") ;
    
    dbQueryFree ($result) ;

    return 0 ;
?>
