<?php

 
    function reportParameterString (&$row) {
	switch ($row['Type']) {
	    case 'text' :
		$value = '"' . $row['Value'] . '"' ;
		break ;
		
	    case 'comboid' :
		// Get the Value of the Id
		if ((int)$row['Value'] == 0) {
		    $value = '<blank>' ;
		    break ;
		}
		
		// Get Query and convert delimiters
		$query = $row['Query'] ;
		$query = str_replace ("\n", ' ', $query) ;
		$query = str_replace ("\r", ' ', $query) ;
		$query = str_replace ("\t", ' ', $query) ;
		$query = trim ($query) ;

		// Find Name of the first field
		$field = strstr ($query, ' ') ;				// Skip SELECT token
		$field = ltrim ($field) ;
		if (($endpos = strpos ($field, ',')) === false) {	// Get field until ","
		    return '<missing "," after first field in query>' ;
		}
		$field = substr ($field, 0, $endpos) ;
		if (($endpos = strpos ($field, ' ')) !== false) {	// Handle "<field> AS <name>"
		    $field = substr ($field, 0, $endpos) ;
		}
		
		// Use Combobox query and add the Id to WHERE-clause
		// Check existence of WHERE
		if (strpos($query, ' WHERE ') === false) return '<"WHERE" not found in query>' ;

		// Generate query and get value
		$query = str_replace (' WHERE ', sprintf (' WHERE %s=%d AND ', $field, (int)$row['Value']), $query) ; 
		$res = dbQuery ($query) ;
		$selected = dbFetch ($res) ;
		dbQueryFree ($res) ;

		$value = (string)$selected['Value'] ; 
//		$value = sprintf ("Id %d", (int)$row['Value']) ; 
		break ;
		
	    case 'integer' :
	    case 'boolean' :
		$value = (string)$row['Value'] ; 
		break ;

	    case 'date' :
		if ($row['Value'] == 0) {
		    $value = '<blank>' ;
		    break ;
		}

    	    case 'week' :
		$value = date ('Y-m-d', $row['Value']) ;
		break ;

	    default :
		$value = NULL ;
		break ;
	}
	return $value ;
    }

    function reportParameterValue (&$row) {
	switch ($row['Type']) {
	    case 'text' :
		$value = '"' . $row['Value'] . '"' ;
		break ;
		
	    case 'comboid' :
	    case 'integer' :
	    case 'boolean' :
		$value = (string)$row['Value'] ; 
		break ;

	    case 'date' :
    	    case 'week' :
		$value = '"' . date ('Y-m-d', $row['Value']) . '"' ;
		break ;

	    default :
		$value = NULL ;
		break ;
	}
	return $value ;
    }

    function reportGenerateQuery (&$Record, &$Table, &$Field, &$Parameter, &$error) {
	// Initialization
	$error = '' ;
	
	// Select
	$query = 'SELECT' ;

	// Fields to query
	$n = 0 ;
	foreach ($Field as $row) {
//	    if ($row['Type'] == 'none') continue ;
	    if ($n > 0) $query .= ',' ;
	    if ($row['Query'] != '') $query .= ' ' . $row['Query'] . ' AS' ;
	    $query .= ' `' . $row['Name'] . '`' ;
	    $n++ ;
	}
	if ($n == 0) {
	    $error = 'No fields to display' ;
	    return NULL ;
	}
	
	// Main tables
	$query .= ' FROM (' ;
	$n = 0 ;
	foreach ($Table as $row) {
	    if ($row['DoLeftJoin']) continue ;
	    if ($n > 0) $query .= ',' ;
	    if (strtoupper(substr(ltrim($row['TableName']),0,6)) == 'SELECT') {
	    	$query .= ' (' . $row['TableName'] . ')' ;
	    } else {
	    	$query .= ' `' . $row['TableName'] . '`' ;
	    }
	    if ($row['Name'] != $row['TableName']) $query .= ' AS `' . $row['Name'] . '`' ;
	    $n++ ;
    	}
	if ($n == 0) {
	    $error = 'No tables to display' ;
	    return NULL ;
	}
	$query .= ' )';

	// LEFT JOINs
	$n = 0 ;
	foreach ($Table as $row) {
	    if (!$row['DoLeftJoin']) continue ;
	    $query .= ' LEFT JOIN' ;
	    if (strtoupper(substr(ltrim($row['TableName']),0,6)) == 'SELECT') {
	    	$query .= ' (' . $row['TableName'] . ')' ;
	    } else {
	    	$query .= ' `' . $row['TableName'] . '`' ;
	    }
	    if ($row['Name'] != $row['TableName']) $query .= ' AS `' . $row['Name'] . '`' ;
	    if ($row['Query'] != '') $query .= ' ON ' . $row['Query'] ;
	    $n++ ;
    	}

	// WHERE
	$n = 0 ;
	foreach ($Table as $row) {
	    if ($row['DoLeftJoin']) continue ;
	    if ($row['Query'] == '') continue ;
	    $query .= ($n == 0) ? ' WHERE ' : ' AND ' ;
	    $query .= $row['Query'] ;
	    $n++ ;
    	}

	// Build level for grouping and sorting
	$grouplevel = array () ;
	$sortlevel = array () ;
	foreach ($Field as $i => $row) {
	    if ((int)$row['GroupLevel'] > 0) $grouplevel[$i] = (int)$row['GroupLevel'] ;
	    if ((int)$row['SortLevel'] > 0) $sortlevel[$i] = (int)$row['SortLevel'] ;
	}
	asort ($grouplevel) ;
	asort ($sortlevel) ;

	// GROUP BY
	$n = 0 ;
	foreach ($grouplevel as $i => $row) {
	    $query .= ($n == 0) ? ' GROUP BY ' : ', ' ;
	    $query .= '`' . $Field[$i]['Name'] . '`' ;
	    $n++ ;
    	}

	// ORDER BY
	$n = 0 ;
	foreach ($sortlevel as $i => $row) {
	    $query .= ($n == 0) ? ' ORDER BY ' : ', ' ;
	    $query .= '`' . $Field[$i]['Name'] . '`';
	    $n++ ;
    	}

	// LIMIT
	if ($Record['Limit'] > 0) {
	    $query .= ' LIMIT ' . (int)$Record['Limit'] ;
	}

	// Substitute parameters
	foreach ($Parameter as $row) {
	    $query = str_replace ('['.$row['Name'].']', reportParameterValue ($row), $query) ;
	}

	return $query ;
    }
?>
