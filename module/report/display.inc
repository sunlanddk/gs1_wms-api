<?php


    require_once 'lib/html.inc' ;
    require_once 'module/report/util.inc' ;
    require_once 'Spreadsheet/Excel/Writer.php';

    // Get and process parameters
    foreach ($Parameter as $name => $row) {
	$value = $_GET['Param'.$row['Name']] ;
	
	switch ($row['Type']) {
	    case 'text' :
		if (!$row['AllowBlank'] and $value == '') return sprintf ('please select %s', $row['Description']) ;
		break ;
		
	    case 'integer' :
		if (!isset($value) or $value == '' or $value === NULL) {
		    if (!$row['AllowBlank']) return sprintf ('please select %s', $row['Description']) ;
		    $value = NULL ;
		    break ;
		}
		$s = ltrim(trim($value), '0') ;
		if ($s == '') $s = '0' ;
		$value = (int)$value ;
		if ((string)$value !== $s) return sprintf ('parameter %s, invalid value', $row['Name']) ; 
		break ;

	    case 'boolean' :
		$value = ($value == 'on') ? 1 : 0 ; 
		break ;

	    case 'date' :
		require_once 'lib/uts.inc' ;
		if (!$row['AllowBlank'] and $value == '') return sprintf ('please select %s', $row['Description']) ;
		$value = utsDateString($value) ;
		break ;

	    case 'week' :
		// Get weeknumber
		$weekno = (int)$value ;
		if ($weekno < 1 or $weekno > 53) return sprintf ("parameter %s, invalid week-number (%s)", $row['Name'], htmlentities($value)) ;

		// Convert weekno into uts
		$thisweek = (int)date('W') ;
		$year = (int)date('Y') ;
		if (($thisweek - 26) > $weekno) $year += 1 ;
		if ($weekno > ($thisweek + 26)) $year -= 1 ;
		$value = mktime (0,0,0,1,1,$year);
		$adjust = array (1 => 0, 2 => -1, 3 => -2, 4 => -3, 5 => 3, 6 => 2, 0 => 1) ;
		$wday = (int)date('w',$value) ;
		$value += $adjust[$wday]*24*60*60 + ($weekno-1)*7*24*60*60 ;
		break ;

	    case 'comboid' :
		$value = (int)$value ;
		if ($value == 0 and !$row['AllowBlank']) return sprintf ('please select %s', $row['Description']) ;
		break ;

	    default :
		return sprintf ('%s(%d) unsupported parameter, type %s', __FILE__, __LINE__, $row['Type']) ;
		break ;
	}
	
	$Parameter[$name]['Value'] = $value ;
    }  

    if ($Record['ExecuteModule']) {
	// Execute external module for generation of the report
	return include './module/reportmodule/' . strtolower($Record['Name']) . '.inc' ;
    }

    // Generate SQL query and do request to database
    $error = '' ;
    $query = reportGenerateQuery ($Record, $Table, $Field, $Parameter, $error) ;
    if (is_null($query)) return $error ;
    $Result = dbQuery ($query) ;

    // Debug
    if (((int)$_GET['debug']) > 0) {
	printf ("dbNumRows: %d<br><br>\n", dbNumRows($Result)) ;
	printf ("Query: %s<br><br>\n", $query) ;
	return 0 ;
    }

    // Genrate XML view
    if (((int)$_GET['xml']) > 0) {
		print ('<?xml version="1.0" encoding="windows-1252" ?>' . "\n") ;
		print ('<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
');
		print ('<Styles>
				<Style ss:ID="s1">
			     <NumberFormat ss:Format="Short Date"/>
			    </Style>
				<Style ss:ID="s2">
			     <NumberFormat ss:Format="0.00"/>
			    </Style>
				<Style ss:ID="s3">
			     <NumberFormat ss:Format="0.000"/>
			    </Style>
				</Styles>
 				<Worksheet ss:Name="Ark1">');
		print ('<Table>'  . "\n");
		print ('<Row> ') ;
		foreach ($Field as $i => $column) {
			printf ('<Cell><Data ss:Type="String">%s</Data></Cell>', $column['Name']) ;
		}
		print ('</Row>' . "\n") ;

		while ($row = dbFetch($Result)) {
			print ('<Row>') ;
		    foreach ($Field as $i => $column) {
				// Make collumn formats
				switch ($column['Type']) {
				    case 'date' :
						$formatstring = 'ss:StyleID="s1"' ;
						$typestring = 'ss:Type="DateTime"' ;
						// convert to short format - eg strip timestamp
				    	$t = (int)strtotime($row[$column['Name']]) ;
				    	$row[$column['Name']] = date('Y-m-d', $t) ;
					break ;
			
				    case 'boolean' :
						$formatstring = '' ;
						$typestring = 'ss:Type="Number"' ;
					break ;
			
				    case 'integer' :
						if (!($row[$column['Name']] > 0)) $row[$column['Name']] = 0;
						$formatstring = '' ;
						$typestring = 'ss:Type="Number"' ;
					break ;
			
				    case 'decimal2':
				    case 'currency' :
						if (!($row[$column['Name']] > 0)) $row[$column['Name']] = 0;
						$formatstring = 'ss:StyleID="s2"' ;
						$typestring = 'ss:Type="Number"' ;
					break ;
					
				    case 'decimal3':
						if (!($row[$column['Name']] > 0)) $row[$column['Name']] = 0;
						$formatstring = 'ss:StyleID="s3"' ;
						$typestring = 'ss:Type="Number"' ;
					break ;
					
				    case 'text' :
				    case 'none' :
			   	    default :
						$formatstring = '' ;
						$typestring = 'ss:Type="String"' ;
					break;
				}
 				printf ('<Cell %s><Data %s>', $formatstring, $typestring) ;
				$strippedstring = str_replace ('&', ' ', $row[$column['Name']]) ;
				print ($strippedstring) ;
				print ('</Data></Cell>') ;
			}
			print ('</Row>' . "\n");
		}
		print ('</Table>
				</Worksheet>
				</Workbook>');
		file_put_contents ('prut.xml', ob_get_contents()) ;
		return 0 ;
    }

    // Genrate html table view
    if (((int)$_GET['htmltab']) > 0) {
		print ('<html>' . "\n") ;
		print ('<head>' . "\n") ;
		print ('<body>' . "\n") ;
		print('<table border="1" cellpadding="0" cellspacing="0">') ;
		print ('<tr> ') ;
	    foreach ($Field as $i => $row) {
				printf('<td WIDTH="64px"><B><FONT SIZE="-2">%s</FONT></B></td> ',$row['Description']);
	    }
		while ($row = dbFetch($Result)) {
			print ('<tr> ') ;
			foreach ($row as $fieldname => $fieldvalue) {
				printf('<td WIDTH="64px"><FONT SIZE="-2">%s</FONT></td> ',$fieldvalue);
			}
			print ('</tr>' . "\n");
		}
		print('</table>') ;
		print('</FONT>') ;
		print ('</html>' . "\n") ;
		print ('</head>' . "\n") ;
		print ('</body>' . "\n") ;
		return 0 ;
    }
    
    
    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;
    $book->send ('report.xls') ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ($Record['Name']) ;
    if ($Record['Landscape']) $sheet->setLandscape () ; else $sheet->setPortrait () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    if ($Record['FitWidth'] > 0 and $Record['FitHeight']) $sheet->fitToPages ((int)$Record['FitWidth'], (int)$Record['FitHeight']) ;			// ????????????????
    $line = 0 ;
    
    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString ($line++, 0, $Record['Description'], $format) ;

    // Parameters for report
    $s = '' ;
    foreach ($Parameter as $row) {
	if ($s != '') $s .= ', ' ;
	$s .= $row['Description'] . ': ' . reportParameterString($row) ;
    }
    if ($s != '') {
	$sheet->writeString ($line, 0, ' ' . $s) ;
	$line++ ;
    }

    // Report identity
    $sheet->writeString ($line++, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;
   
    // Blank line
    $line++ ;

    // Make formats
    define ('HEADLINECOLOR', 22) ;

    $formatHead =& $book->addFormat () ;
    $formatHead->setFgColor (HEADLINECOLOR) ;

    $formatHeadRight =& $book->addFormat () ;
    $formatHeadRight->setFgColor (HEADLINECOLOR) ;
    $formatHeadRight->setAlign ('right') ;

    $format =& $book->addFormat () ;
    $format->setAlign ('top') ;

    $formatRight =& $book->addFormat () ;
    $formatRight->setAlign ('right') ;
    $formatRight->setAlign ('top') ;

    $formatInteger =& $book->addFormat () ;
    $formatInteger->setNumFormat (3) ;		// Format: decimal #,##0
    $formatInteger->setAlign ('top') ;
    
    $formatDecimal2 =& $book->addFormat () ;
    $formatDecimal2->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatDecimal2->setAlign ('top') ;

    $formatCurrency =& $book->addFormat () ;
    $formatCurrency->setNumFormat (7) ;		// Format: currency
    $formatCurrency->setAlign ('top') ;
    
    $formatTotal =& $book->addFormat () ;
    $formatTotal->setTop (1) ;

    $formatTotalInteger =& $book->addFormat () ;
    $formatTotalInteger->setTop (1) ;
    $formatTotalInteger->setNumFormat (3) ;

    $formatTotalDecimal2 =& $book->addFormat () ;
    $formatTotalDecimal2->setTop (1) ;
    $formatTotalDecimal2->setNumFormat (4) ;

    $formatTotalCurrency =& $book->addFormat () ;
    $formatTotalCurrency->setTop (1) ;
    $formatTotalCurrency->setNumFormat (7) ;

    // Field Header and create formats
    $MakeTotal = false ;
    $collumn = 0 ;
    foreach ($Field as $i => $row) {
	// Make collumn formats
	switch ($row['Type']) {
	    case 'none' :
		continue 2 ;

	    case 'boolean' :
	    case 'date' :
		$Field[$i]['FormatLine'] = $formatRight ;
		$Field[$i]['FormatTotal'] = $formatTotal ;
		$f = $formatHeadRight ;
		break ;

	    case 'integer' :
		$Field[$i]['FormatLine'] = $formatInteger ;
		$Field[$i]['FormatTotal'] = $formatTotalInteger ;
		$f = $formatHeadRight ;
		if ($row['Total']) $MakeTotal = true ;
		break ;

	    case 'decimal2':
		$Field[$i]['FormatLine'] = $formatDecimal2 ;
		$Field[$i]['FormatTotal'] = $formatTotalDecimal2 ;
		$f = $formatHeadRight ;
		if ($row['Total']) $MakeTotal = true ;
		break ;
		
	    case 'currency' :
		$Field[$i]['FormatLine'] = $formatCurrency ;
		$Field[$i]['FormatTotal'] = $formatTotalCurrency ;
		$f = $formatHeadRight ;
		if ($row['Total']) $MakeTotal = true ;
		break ;

	    case 'text' :
    	    default :
		$Field[$i]['FormatLine'] = $format ;
		$Field[$i]['FormatTotal'] = $formatTotal ;
		$f = $formatHead ;
		break ;
	}	

	// Initialize total value
	$Field[$i]['TotalValue'] = 0 ;
	
	// Header field
	$sheet->setColumn($collumn, $collumn, (int)$row['Width']) ;
	$sheet->writeString ($line, $collumn, $row['Description'], $f) ;
	$collumn++ ;
    }
    $line++ ;

    // Freeze the pane
    $sheet->freezePanes (array($line, (int)$Record['FreezeColumn'])) ;

    // Loop all lines of data
    while ($data = dbFetch ($Result)) {
    	$collumn = 0 ;
	foreach ($Field as $i => $row) {
	    switch ($row['Type']) {
		case 'none' :
		    continue 2 ;

		case 'text' :
		    $sheet->writeString ($line, $collumn, str_replace("\r", "", $data[$row['Name']]), $row['FormatLine']) ;
		    break ;

		case 'date' :
		    $v = $data[$row['Name']] ;
		    if ($v == 0) break ;
		    $sheet->writeString ($line, $collumn, date($row['Format'], dbDateDecode($v)), $row['FormatLine']) ;
		    break ;

		case 'integer' :
		case 'decimal2' :
		case 'currency' :
		    $v = $data[$row['Name']] ;
		    $sheet->writeNumber ($line, $collumn, $v, $row['FormatLine']) ;
		    $Field[$i]['TotalValue'] += $v ;
		    break ;

		case 'boolean' :
		    $sheet->writeString ($line, $collumn, ($data[$row['Name']]) ? 'yes' : 'no', $row['FormatLine']) ;	
		    break ;
		    
    		default :
		    $sheet->writeString ($line, $collumn, sprintf ('unknown type %s', $row['Type']), $row['FormatLine']) ;
	    }
	    $collumn++ ;
	}
	$line++ ;
    }
    dbQueryFree ($Result) ;

    // Totals
    if ($MakeTotal) {
    	$collumn = 0 ;
	foreach ($Field as $i => $row) {
	    switch ($row['Type']) {
		case 'none' :
		    continue 2 ;

		case 'date' :
		case 'text' :
		case 'boolean' :
		default :
		    if ($collumn == 0) {
			$sheet->writeString ($line, $collumn, 'Total', $row['FormatTotal']) ;
			break ;
		    }
		    $sheet->writeBlank ($line, $collumn, $row['FormatTotal']) ;
		    break ;

		case 'integer' :
		case 'decimal2' :
		case 'currency' :
		    if ($row['Total']) {
			$sheet->writeNumber ($line, $collumn, $row['TotalValue'], $row['FormatTotal']) ;
		    } else {
		    	$sheet->writeBlank ($line, $collumn, $row['FormatTotal']) ;
		    }
		    break ;
    	    }
	    $collumn++ ;
	}	
	$line++ ;
    }

    // Let's send the file
    $book->close();
    
    return 0 ;    
?>
