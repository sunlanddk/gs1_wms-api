<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['Limit'] = 0 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 value=\"%s\" style=\"width:175px;\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td>%s</td></tr>\n", htmlDBSelect ('ReportGroupId style="width:250px;"', $Record['ReportGroupId'], 'SELECT Id, Name AS Value FROM ReportGroup WHERE Active=1 ORDER BY Name')) ;  
    if ($Navigation['Parameters'] != 'copy') {
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>ExecModule</td><td><input type=checkbox name=ExecuteModule %s></td></tr>\n", ($Record['ExecuteModule'])?'checked':'') ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Limit</td><td><input type=text name=Limit maxlength=6 size=6 value=%d> rows</td></tr>\n", $Record['Limit']) ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Landscape</td><td><input type=checkbox name=Landscape %s></td></tr>\n", ($Record['Landscape'])?'checked':'') ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Freeze</td><td><input type=text name=FreezeColumn maxlength=3 size=3 value=%d> column(s)</td></tr>\n", $Record['FreezeColumn']) ;
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>FitWidth</td><td><input type=text name=FitWidth maxlength=3 size=3 value=%d> page(s)</td></tr>\n", $Record['FitWidth']) ;
	printf ("<tr><td class=itemlabel>FitHeight</td><td><input type=text name=FitHeight maxlength=3 size=3 value=%d> page(s)</td></tr>\n", $Record['FitHeight']) ;
    }
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
