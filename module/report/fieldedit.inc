<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Get highest DisplayOrder number
	    $query = sprintf ('SELECT MAX(ReportField.DisplayOrder) as DisplayOrder FROM ReportField WHERE ReportField.ReportId=%d AND ReportField.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $row = dbFetch ($r) ;
	    dbQueryFree ($r) ;

	    // Default values
	    $Record['DisplayOrder'] = ($row) ? $row['DisplayOrder']+1 : 1 ;
	    $Record['SortLevel'] = 0 ;
	    $Record['GroupLevel'] = 0 ;
	    $Record['Width'] = 20 ;
	    $Record['Type'] = 'text' ;
	    break ;
    }

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=40 size=40 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=3 size=3 value=%d></td></tr>\n", htmlentities($Record['DisplayOrder'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>SortLevel</td><td><input type=text name=SortLevel maxlength=3 size=3 value=%d></td></tr>\n", $Record['SortLevel']) ;
    printf ("<tr><td class=itemlabel>GroupLevel</td><td><input type=text name=GroupLevel maxlength=3 size=3 value=%d></td></tr>\n", $Record['GroupLevel']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Type</td><td>%s</td></tr>\n", htmlSelect ('Type style="width:100px;"', $Record['Type'], tableEnumExplode(tableGetType('ReportField','Type')))) ;    
    printf ("<tr><td class=itemlabel>Format</td><td><input type=text name=Format maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Format'])) ;
    printf ("<tr><td class=itemlabel>Width</td><td><input type=text name=Width maxlength=3 size=3 value=%d> EXCEL collumn units</td></tr>\n", htmlentities($Record['Width'])) ;
    printf ("<tr><td class=itemlabel>Total</td><td><input type=checkbox name=Total %s></td></tr>\n", ($Record['Total'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Query</td><td><textarea name=Query style='width:100%%;height:150px;'>%s</textarea></td></tr>\n", $Record['Query']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
