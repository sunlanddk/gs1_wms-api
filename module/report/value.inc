<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/form.inc' ;

    // Header
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ReportGroupDescription'])) ;
    printf ("<tr><td class=itemlabel>Report</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    // Form
    printf ("<form method=GET name=appform>\n") ;
    printf ("<input type=hidden name=id>\n") ;
    printf ("<input type=hidden name=nid>\n") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    if (count($Parameter) > 0) {
	// Walk trough all parameters
	foreach ($Parameter as $row) {
	    if ($row['Width'] < 10) $row['Width'] = 10 ;
	    printf ("<tr><td class=itemlabel>%s</td><td>", $row['Description']) ;  
	    switch ($row['Type']) {
		case 'text' :
		case 'integer' :
		    if ($row['Width'] < 10) $row['Width'] = 10 ;
		    printf ('<input type=text name=Param%s maxlength=%d style="width:%dpx;">', $row['Name'], $row['Size'], $row['Width']) ;
		    break ;

		case 'boolean' :
		    printf ('<input type=checkbox name=Param%s>', $row['Name']) ;
		    break ;

		case 'date' :
		    print formDate ('Param'.$row['Name'], NULL, NULL, 'tR') ;
		    break ;

		case 'week' :
		    $thisweek = (int)date('W') ;
		    printf ("<select size=1 name=Param%s>\n", $row['Name']) ;
		    for ($n = 1 ; $n <= 53 ; $n++) {
			printf ("<option%s value=%d>%d\n", ($n == $thisweek)?" selected":"", $n, $n);
		    }
		    printf ("</select>\n") ;
		    break ;

		case 'comboid' :
		    print htmlDBSelect (sprintf ('Param%s style="width:%dpx;"', $row['Name'], $row['Width']), 0, $row['Query']) ;  
		    break ;

		default :
		    printf ('<p>unsupported parameter, type %s</p>', $row['Type']) ;
		    break ;
	    }
	    printf ("</td></tr>\n") ;
	    print htmlItemSpace() ;
	}  
    } else {
	// No parameters to display
	printf ("<tr><td class=itemlabel colspan=2>No parameters</td></tr>\n") ;
    }	
    printf ("</table>\n") ;
    printf ("</form>\n") ;
    
    return 0 ;
?>
