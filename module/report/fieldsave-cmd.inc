<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'DisplayOrder'		=> array ('type' => 'integer',	'check' => true),
	'Type'			=> array ('mandatory' => true),
	'SortLevel'		=> array ('type' => 'integer'),
	'GroupLevel'		=> array ('type' => 'integer'),
	'Width'			=> array ('type' => 'integer'),
	'Format'		=> array (),
	'Total'			=> array ('type' => 'checkbox'),
	'Query'			=> array (			'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['ReportId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ReportId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record ;
	switch ($fieldname) {
	    case 'Name':
		// Validate name
		if (!ereg('^[a-zA-Z0-9]{1,40}$', $value)) return 'invalid characters in name' ;
		
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ReportField WHERE ReportId=%d AND Active=1 AND Name="%s" AND Id<>%d', $Record['ReportId'], addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Parameter allready existing' ;
		return true ;

	    case 'DisplayOrder':
		if (!$changed) return false ;
		$query = sprintf ("SELECT Id FROM ReportField WHERE ReportId=%d AND Active=1 AND Id<>%d", $Record['ReportId'], $Record['Id']) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($value <= 0 or $value > ($count+1)) return "invalid DisplayOrder number" ;
		return true ;

	    case 'Query':
		// 'Password' not allowed
		if (strpos ($value, 'Password') !== false) return 'field "Password" not allowed in query' ;
		return true ;
	}
	return false ;
    }
     
    function saveValidate ($changed) {
	global $User, $Record, $Id, $fields ;

	if ($Record['Type'] == 'date' and $Record['Format'] == '') return 'format must be specified for the date type' ;

	return true ;
    }
    
    $r = saveFields ('ReportField', $Id, $fields, true) ;
    if ($r) return $r ;
    
    // Renumber other entries
    $query = sprintf ("SELECT Id, DisplayOrder FROM ReportField WHERE ReportId=%d AND Active=1 AND Id<>%d ORDER BY DisplayOrder", $Record['ReportId'], $Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == $Record['DisplayOrder']) $i += 1 ;       
	if ($i != $row['DisplayOrder']) {
	    $query = sprintf ("UPDATE ReportField SET DisplayOrder=%d WHERE Id=%d", $i, $row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0 ;
?>
