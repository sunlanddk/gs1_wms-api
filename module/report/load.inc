<?php

    require_once 'lib/perm.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // New entry to create
	    switch ($Navigation['Function']) {
		case 'baseedit' :
		case 'basesave-cmd' :
		    return 0 ;

		case 'paramedit' :
		case 'paramsave-cmd' :
		case 'fieldedit' :
		case 'fieldsave-cmd' :
		case 'tableedit' :
		case 'tablesave-cmd' :
		    // Query Report
		    $query = sprintf ('SELECT Report.Id, Report.Name FROM Report WHERE Report.Id=%d AND Report.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    return 0 ;
	    }
	    
	    return sprintf ('%s(%d) new paramater not supported for function %s', __FILE__, __LINE__, $Navigation['Function']) ;
    }


    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Reports
	    $query = 'SELECT Report.*, ReportGroup.Name AS ReportGroupName, ReportGroup.Description AS ReportGroupDescription FROM (Report, ReportGroup)' ;
	    if (!permAdminSystem()) $query .= ', ReportRole' ;
	    $query .= ' WHERE Report.Active=1 AND ReportGroup.Id=Report.ReportGroupId' ;
	    if (!permAdminSystem()) $query .= sprintf (' AND ReportRole.ReportGroupId=ReportGroup.Id AND ReportRole.RoleId=%d AND ReportRole.Active=1', $User['RoleId']) ;
	    $query .= ' ORDER BY ReportGroup.Name, Report.Name' ;
	    $Result = dbQuery ($query) ;
	    
	    return 0 ;
	    
	case 'showquery' :
	case 'display' :    
	case 'design' :
	case 'basesave-cmd' :
	    // Load Report Tables
	    $query = sprintf ('SELECT ReportTable.* FROM ReportTable WHERE ReportTable.ReportId=%d AND ReportTable.Active=1 ORDER BY ReportTable.DisplayOrder', $Id) ;
	    $res = dbQuery ($query) ;
	    $Table = array() ;
	    while ($row = dbFetch ($res)) {
		$Table[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($res) ;

	    // Load Report Fields
	    $query = sprintf ('SELECT ReportField.* FROM ReportField WHERE ReportField.ReportId=%d AND ReportField.Active=1 ORDER BY ReportField.DisplayOrder', $Id) ;
	    $res = dbQuery ($query) ;
	    $Field = array() ;
	    while ($row = dbFetch ($res)) {
		$Field[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($res) ;

	    // Fall trough
	    
	case 'value' :
	    // Load Report Parameters
	    $query = sprintf ('SELECT ReportParameter.* FROM ReportParameter WHERE ReportParameter.ReportId=%d AND ReportParameter.Active=1 ORDER BY ReportParameter.DisplayOrder', $Id) ;
	    $res = dbQuery ($query) ;
	    $Parameter = array() ;
	    while ($row = dbFetch ($res)) {
		$Parameter[$row['Name']] = $row ;
	    }
	    dbQueryFree ($res) ;
	    
	    // Fall trough

	case 'baseedit' :
	case 'basedelete-cmd' :
	    // Query Report
	    $query = sprintf ('SELECT Report.*, ReportGroup.Name AS ReportGroupName, ReportGroup.Description AS ReportGroupDescription FROM Report LEFT JOIN ReportGroup ON ReportGroup.Id=Report.ReportGroupId WHERE Report.Id=%d AND Report.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;

	case 'paramedit' :
	case 'paramsave-cmd' :
	case 'paramdelete-cmd' :
	    // Query Report Parameters
	    $query = sprintf ('SELECT ReportParameter.* FROM ReportParameter WHERE ReportParameter.Id=%d AND ReportParameter.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;

	case 'fieldedit' :
	case 'fieldsave-cmd' :
	case 'fielddelete-cmd' :
	    // Query Report Fields
	    $query = sprintf ('SELECT ReportField.* FROM ReportField WHERE ReportField.Id=%d AND ReportField.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;

	case 'tableedit' :
	case 'tablesave-cmd' :
	case 'tabledelete-cmd' :
	    // Query Report Tables
	    $query = sprintf ('SELECT ReportTable.* FROM ReportTable WHERE ReportTable.Id=%d AND ReportTable.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
