<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'ReportGroupId'		=> array ('type' => 'integer',	'mandatory' => true),
	'ExecuteModule'		=> array ('type' => 'checkbox'),
	'Limit'			=> array ('type' => 'integer'),
	'Landscape'		=> array ('type' => 'checkbox'),
	'FreezeColumn'		=> array ('type' => 'integer'),
	'FitWidth'		=> array ('type' => 'integer'),
	'FitHeight'		=> array ('type' => 'integer')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['ExecuteModule'] = array ('type' => 'set', 'value' => $Record['ExecuteModule']) ;

	    // Fall through
	    
	case 'copy' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Validate name
		if (!ereg('^[a-zA-Z0-9]{1,20}$', $value)) return 'invalid characters in name' ;

		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM Report WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Report allready existing' ;
		return true ;
	}
	return false ;
    }
     
    $res = saveFields ('Report', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'copy' :
	    require_once 'lib/table.inc' ;
	    
	    // Copy ReportParameters
	    foreach ($Parameter as $row) {
		$row['ReportId'] = $Record['Id'] ;
		tableWrite ('ReportParameter', $row) ;
	    }

    	    // Copy ReportTables
	    foreach ($Table as $row) {
		$row['ReportId'] = $Record['Id'] ;
		tableWrite ('ReportTable', $row) ;
	    }

    	    // Copy ReportFields
	    foreach ($Field as $row) {
		$row['ReportId'] = $Record['Id'] ;
		tableWrite ('ReportField', $row) ;
	    }
	    break ;
    }

    return 0 ;   
?>
