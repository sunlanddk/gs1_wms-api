<?php

    require_once 'lib/table.inc' ;
   
    tableDelete ('ReportField', $Id) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, DisplayOrder FROM ReportField WHERE ReportField.ReportId=%d AND ReportField.Active=1 ORDER BY DisplayOrder', (int)$Record['ReportId']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['DisplayOrder']) {
	    $query = sprintf ("UPDATE ReportField SET DisplayOrder=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
