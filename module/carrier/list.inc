<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 140) ;
    listHead ('Description') ;
    listHead ('Company') ;
    listHead ('Transport Border', 100) ;
    listHead ('Transport Local', 100) ;
    listHead ('Customs Location', 100) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['CompanyName']) ;
	listField ($row['TransportBorder']) ;
	listField ($row['TransportLocal']) ;
	listField ($row['CustomsLocation']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Carriers', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
