<?php

    require_once 'lib/html.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Login Name</td><td class=itemfield>%s</td></tr>", htmlentities($Record["Loginname"])) ;
    printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>", htmlentities($Record["FullName"])) ;
    printf ("<tr><td class=itemlabel>Company</td><td class=itemfield>%s</td></tr>", htmlentities($Record["CompanyName"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Logon</td><td class=itemfield>%s</td></tr>", date ("Y-m-d H:i:s", dbDateDecode($Record["LoginDate"]))) ;
    printf ("<tr><td class=itemlabel>Last activity</td><td class=itemfield>%s</td></tr>", date ("Y-m-d H:i:s", dbDateDecode($Record["AccessDate"]))) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Session ID</td><td class=itemfield>%s</td></tr>", htmlentities($Record["UID"])) ;
    printf ("<tr><td class=itemlabel>IP</td><td class=itemfield>%s</td></tr>", htmlentities($Record["IP"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Browser</td><td class=itemfield>%s</td></tr>", htmlentities($Record["Agent"])) ;
    printf ("<tr><td class=itemlabel>Language</td><td class=itemfield>%s</td></tr>", htmlentities($Record["Language"])) ;
    printf ("<tr><td class=itemlabel>Accept MIME</td><td class=itemfield>%s</td></tr>", htmlentities($Record["AcceptedMimeTypes"])) ;
    printf ("</table>") ;
    
    return 0 ;
?>
