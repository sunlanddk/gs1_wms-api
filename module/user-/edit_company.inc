<?php
   
    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/perm.inc' ;
    
    $my = true ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
	printf ("<tr><td><p>Company</p></td><td>%s</td></tr>\n", htmlDBSelect ("CompanyId style='width:250px'", $Record["CompanyId"], 
	              sprintf("SELECT Company.Id, Company.Name AS Value FROM (usercompanies, company) WHERE usercompanies.userid=%d and company.id=usercompanies.companyid AND Internal=1 And usercompanies.Active=1 And Company.Active=1 ORDER BY Name", $User['Id']))) ;  
    print htmlItemSpace() ;

    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
