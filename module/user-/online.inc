<?php

    require_once 'lib/navigation.inc' ;

    if (dbNumRows ($Result) == 0) return "No users online" ;
    
    printf ("<table width=100%% style='table-layout:fixed;'\n>") ;
    printf ("<tr>") ;
    printf ("<td class=listhead width=23></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Login Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Full Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Company</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Logon</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Last activity</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/user.gif', navigationOnClickLink ("detail", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Loginname"])) ;
	printf ("<td class=list><p class=list>%s</p></td>", htmlentities($row["FullName"])) ;
	printf ("<td class=list><p class=list>%s</p></td>", htmlentities($row["CompanyName"])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i:s", dbDateDecode($row["LoginDate"]))) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i:s", dbDateDecode($row["AccessDate"]))) ;
	printf ("</tr>\n") ;
    }
    printf ("</table>") ;
    
    dbQueryFree ($Result) ;
    
    return 0 ;
?>
