<?php

    require "lib/dropmenu.inc" ;

    if (dbNumRows($Result) == 0) return "No users" ;
    
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("%s", dropmenuTDHeader()) ;
    printf ("<td class=listhead width=90><p class=listhead>Login Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Role</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Title</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Full Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Company</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("%s", dropmenuTDList($row["Id"], 'image/toolbar/user.gif')) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Loginname"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["RoleName"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Title"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["FullName"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["CompanyName"])) ;
	printf ("</tr>\n") ;
    }
    printf ("</table>") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
