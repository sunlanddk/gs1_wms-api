<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;

    $MaterialCountryGroup = tableEnumExplode(tableGetType('CustomsPreference','MaterialCountryGroup')) ;
    $KnitCountryGroup = tableEnumExplode(tableGetType('CustomsPreference','KnitCountryGroup')) ;
    $WorkCountryGroup = tableEnumExplode(tableGetType('CustomsPreference','WorkCountryGroup')) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['MaterialCountryGroup'] = $MaterialCountryGroup[0] ;
	    $Record['KnitCountryGroup'] = $KnitCountryGroup[0] ;
	    $Record['WorkCountryGroup'] = $WorkCountryGroup[0] ;
	    break ;
    }
     
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Material', formSelect ('MaterialCountryGroup', $Record['MaterialCountryGroup'], $MaterialCountryGroup, 'width:100px;')) ;
    itemFieldRaw ('Drop', formCheckBox ('MaterialDrop', $Record['MaterialDrop'])) ;
    itemSpace () ;
    itemFieldRaw ('Knit', formSelect ('KnitCountryGroup', $Record['KnitCountryGroup'], $KnitCountryGroup, 'width:100px;')) ;
    itemFieldRaw ('Drop', formCheckBox ('KnitDrop', $Record['KnitDrop'])) ;
    itemSpace () ;
    itemFieldRaw ('Work', formSelect ('WorkCountryGroup', $Record['WorkCountryGroup'], $WorkCountryGroup, 'width:100px;')) ;
    itemFieldRaw ('Drop', formCheckBox ('WorkDrop', $Record['WorkDrop'])) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
