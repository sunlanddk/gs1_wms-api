<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT CustomsPreference.* FROM CustomsPreference WHERE CustomsPreference.Active=1 ORDER BY CustomsPreference.MaterialCountryGroup, CustomsPreference.KnitCountryGroup, CustomsPreference.WorkCountryGroup') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query CustomsPreference
	    $query = sprintf ('SELECT CustomsPreference.* FROM CustomsPreference WHERE CustomsPreference.Id=%d AND CustomsPreference.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
