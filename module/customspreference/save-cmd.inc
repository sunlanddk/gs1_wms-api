<?php

    require 'lib/save.inc' ;

    $fields = array (
	'MaterialCountryGroup'	=> array ('mandatory' => true),
	'MaterialDrop'		=> array ('type' => 'checkbox'),
	'KnitCountryGroup'	=> array ('mandatory' => true),
	'KnitDrop'		=> array ('type' => 'checkbox'),
	'WorkCountryGroup'	=> array ('mandatory' => true),
	'WorkDrop'		=> array ('type' => 'checkbox')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function saveValidate ($changed) {
	global $Id, $Record ;
	
	// Check Group combination does not allready exist
	$query = sprintf ('SELECT Id FROM CustomsPreference WHERE MaterialCountryGroup="%s" AND KnitCountryGroup="%s" AND WorkCountryGroup="%s" AND Active=1 AND Id<>%d', addslashes($Record['MaterialCountryGroup']), addslashes($Record['KnitCountryGroup']), addslashes($Record['WorkCountryGroup']), $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count > 0) return 'CustomsPreference entry allready existing' ;
	return true ;
    }
     
    return saveFields ('CustomsPreference', $Id, $fields, true) ;
?>
