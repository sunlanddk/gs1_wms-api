<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	// Variant and pick location generation.
	$Error =0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;

		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
	   			$index = 1;
   				$VariantCode = array (	
					'VariantCode'		=>  "",			// EAN code - calculated
					'VariantUnit'		=>  "pcs",			// 	 
					'VariantModelRef'	=>  "",			// Drappa STYLE 	- C3
					'VariantDescription' => "",			//		
					'VariantColorDesc'		=>  "",			
					'VariantColorCode'	=>  "",			
					'VariantSize'		=>  "",			
					'ArticleId'			=>	0,		// ArticleNumber C4
					'ArticleColorId'	=>	0,			// colornumber C6
					'ArticleSizeId'		=>	0,
					'Reference'			=>	0,			// Case C1
					'Type'				=>	"case"
				) ;
				$NoSizes = 0 ;
				$Street  = '' ;
				$Numbers = '' ;

   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('ss:Index');
					if ($ind != null) $index = $ind;
	  
					if ( $index == 1 ) $VariantCode['Reference']=$cell->nodeValue ;
					if ( $index == 3 ) {
						$VariantCode['VariantModelRef']=$cell->nodeValue ;
						$VariantModelRef = $cell->nodeValue ;
					}
					if ( $index == 4 ) { // Article number
			  			$query = sprintf("select * from Article where Number='%s' and active=1",$cell->nodeValue) ;
						$artres = dbQuery ($query) ;
						$art_row = dbFetch ($artres) ;
						dbQueryFree($artres) ;

						$ArticleId = (int)$art_row['Id'] ;
						if ($ArticleId == 0) return sprintf('Article %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
						$VariantCode['ArticleId']=$ArticleId ;
					}
					if ( $index == 6 )  { // Color
						if ($art_row['VariantColor']) {
							$Color = $cell->nodeValue;
							$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s' and active=1",$cell->nodeValue));
							if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $cell->nodeValue, $row_index) ;
							$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d and active=1",$ArticleId,$ColorId));
							if ($ArticleColorId == 0) return sprintf('ArticleColor %s, Articleid %d in line %d dosnt exist', $cell->nodeValue, $ArticleId, $row_index) ;
							$VariantCode['ArticleColorId']=$ArticleColorId ;
						} else {
							$ArticleColorId = 0;
						}
					}
					if ( $index == 7 ) $NoSizes = (int)$cell->nodeValue;
					if ( $index == 8 ) $Street = $cell->nodeValue ;
					if ( $index == 9 ) $Numbers = explode(',',strval($cell->nodeValue));
	 	 
  					$index += 1;
	  			}
	  			
	  			$query = sprintf("select * from articlesize
							where active=1 and articleid=%d	
 							order by displayorder", $ArticleId) ;	
				$res = dbQuery ($query) ;
				$NoFloorsPerNumber = 10 ;
				$display_val = '' ;
				$size_index=0;	
				while (($sizerow = dbFetch ($res)) or ($art_row['VariantSize']==0)) {
						if ($art_row['VariantSize']==0) {
							$sizerow['Id']=0;
							$art_row['VariantSize']=1;
						}
			  			$query = sprintf("select * from variantcode where ArticleId=%d and ArticleColorId=%d and ArticleSizeId=%d and active=1",$ArticleId,$ArticleColorId,$sizerow['Id']) ;
						$varres = dbQuery ($query) ;
						$old_row = dbFetch ($varres) ;

						$VariantCodeId=$old_row['Id'] ;
						// Calculate Position
						if ($Street=='' or sizeof($Numbers)==0) { // Other conditions to secure no overwrite of active containers?
							if ($VariantCodeId>0 and $old_row['PickContainerId']>0)
								$VariantCode['PickContainerId']=$old_row['PickContainerId'] ;
							else
								$VariantCode['PickContainerId']=0 ;
						} else {
							$displayorder=$size_index ;
							$seqno=(int)($displayorder/$NoFloorsPerNumber) ;
							if ($seqno > sizeof($Numbers)-1)
								$seqno= sizeof($Numbers)-1 ;
							$No=$Numbers[$seqno] ;
							$Floor = chr(ord('a')+($displayorder-$seqno*$NoFloorsPerNumber)) ;

//							$display_val = $display_val . sprintf('%d %d %d<br>', $displayorder,$seqno,($displayorder-$seqno*$NoFloorsPerNumber)) ;
//							$display_val = $display_val . $sizerow['Name'] . " " .  $sizerow['DisplayOrder'] . " " . $Street . $No . $Floor . '<br>';

							if ($VariantCodeId>0 and $old_row['PickContainerId']>0) {
								// Update position in Container
								$Container = array (
									'Position' => $Street . sprintf('%02d',$No) . $Floor ,
								) ;							
								$Container['Id'] = tableWrite ('Container', $Container, $old_row['PickContainerId'] ) ;
								$VariantCode['PickContainerId']=$old_row['PickContainerId'] ;
							} else {
								// Create new Container
								$Container = array (
									'StockId' => (int)9587, // NT Drappa picking 
									'ContainerTypeId' => 39,
									'Position' => $Street . sprintf('%02d',$No) . $Floor ,
									'TaraWeight' => 0,
									'GrossWeight' => 0,
									'Volume' => 0
								) ;							
								$Container['Id'] = tableWrite ('Container', $Container) ;
								$VariantCode['PickContainerId']=$Container['Id'] ;
							}
						}
						if ($VariantCodeId > 0) {
							$VariantCode['VariantCode']= $old_row['VariantCode'] ;
							$VariantCode['VariantModelRef']=$VariantModelRef . ',' . $old_row['VariantModelRef'] ;
						} else {
							$VariantCode['VariantCode']= sprintf('%13.0f',(10+(float)tableGetFieldWhere ('Variantcode', 'max(VariantCode)', '1=1'))) ;
						}
						$VariantCode['ArticleSizeId']=$sizerow['Id'] ;
						tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;
						dbQueryFree($varres) ;
						$size_index++ ;
				}
				dbQueryFree($res) ;
				$row_index++;
	  	    }			
	    }
    }

    return $Error ;
?>
