<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
   	require_once 'lib/reader.inc';

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	
	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);

		$SheetConfig = array (
			'ArticleInfoFromVariant' => 0,
			'ArticleColomn' => 3,
		) ;

		for ($k = 0; $k <= 2; $k++) {
		// New Collection
		if ($data->sheets[$k]['cells'][1][1] == '') continue ;
		
		$Collection['Name'] = $data->sheets[$k]['cells'][1][1] ;
		$Collection['SeasonId'] = 1 ;	// GC

		$Collection['OwnerId'] = 787 ;		// GC
		$Collection['CompanyId'] = 787 ;	// GC
		$CollectionId = tableWrite ('Collection', $Collection) ;
		
		// Create Collectionmembers
		for ($i = 0; $i <= $data->sheets[$k]['numRows']; $i++) {
			// Only proces lines with "active" in colomn A
			if ($data->sheets[$k]['cells'][$i][1] != 'active') continue ;

			$query = sprintf ("SELECT * from Article Where Number='%s' and Active=1", $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']]) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
    		dbQueryFree ($result) ;
			if ((int)$row['Id'] == 0)
				return sprintf ('Article %s in line %d not found', $data->sheets[$k]['cells'][$i][$SheetConfig['ArticleColomn']], $i) ;
			else
				$CollectionMember['ArticleId']=(int)$row['Id'] ;

			$query = sprintf ("select max(id) as Id from `case` c where c.articleid=%d group by c.articleid", $CollectionMember['ArticleId']) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
    		dbQueryFree ($result) ;   	
			$CollectionMember['CaseId']=(int)$row['Id'] ;

			$CollectionMember['CollectionId']=$CollectionId ;
			tableWrite ('CollectionMember', $CollectionMember) ;
		} // end for rows
		} // end for sheets
	} 

    return 0 ;    
?>
