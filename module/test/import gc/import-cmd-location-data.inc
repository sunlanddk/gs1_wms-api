<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$Error =0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
	   			$index = 1;
   			
   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('ss:Index');
					if ($ind != null) $index = $ind;
	  
					if ( $index == 1 ) $txt=$cell->nodeValue ;
					if ( $index == 2 ) {
						$ArticleId = tableGetFieldWhere('Article','Id',sprintf("Number='%s' and Active=1",$cell->nodeValue));
						if ($ArticleId == 0) return sprintf('Article %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
					}
					if ( $index == 4 )  {
						$Color = $cell->nodeValue;
						$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s'",$cell->nodeValue));
						if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $cell->nodeValue, $row_index) ;
						$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d",$ArticleId,$ColorId));
						if ($ArticleColorId == 0) return sprintf('ArticleColor %s, in line %d dosnt exist %d,%d', $cell->nodeValue, $row_index,$ArticleId,$ColorId ) ;
					}
					if ( $index == 8 ) $Street = $cell->nodeValue;
					if ( $index == 9 ) $No = $cell->nodeValue;
					if ( $index == 10) {
						$Floor = $cell->nodeValue;
					}
	 	 
  					$index += 1;
	  			}
	  			
	  			$query = sprintf("select * from articlesize where articleid=%d and active=1", $ArticleId) ;
	
				$res = dbQuery ($query) ;
	
				while ($sizerow = dbFetch ($res)) {
					$VariantCodeId=tableGetFieldWhere('VariantCode','Id',sprintf("ArticleId=%d and ArticleColorId=%d and ArticleSizeId=%d",$ArticleId, $ArticleColorId, $sizerow['Id'])) ;
					if ($VariantCodeId) {
						if (tableGetFieldWhere('VariantCode','PickContainerId',sprintf("Id=%d",$VariantCodeId))>0) {
							$Error=$Error . sprintf('Dupplicate found for art %d color %d, size %d txt (%s) at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $txt, $row_index) ;
						} else {
							// Create new Container
							$Container = array (
								'StockId' => (int)2059,
								'ContainerTypeId' => 39,
								'Position' => $Street . $No . chr(ord($Floor)+((int)$sizerow['DisplayOrder']-1)) ,
								'TaraWeight' => 0,
								'GrossWeight' => 0,
								'Volume' => 0
							) ;

							$Container['Id'] = tableWrite ('Container', $Container) ;
							$VariantCode['PickContainerId']=$Container['Id'] ;
							tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;

						} 
					} else $Error=$Error . sprintf('Variant not found for art %d color %d, size %d txt (%s) at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $txt, $row_index) ;
				}

				$row_index++;
	  	    }			
	    }
    }

    return $Error ;
?>
