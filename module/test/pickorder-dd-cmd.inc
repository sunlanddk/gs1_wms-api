<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$first=1 ;

	$query = sprintf(
"select o.id as OrderId, oq.id as OrderQuantityId, ol.Description as ArticleDescription, vc.id as VariantCodeId, vc.VariantCode as VariantCode, o.companyid as CompanyId, ol.deliverydate as DeliveryDate,
ol.articleid,  ol.articlecolorid, c.description as color, oq.articlesizeid, az.name as size, oq.quantity as quantity
from (`order` o, orderline ol, orderquantity oq, article a)
LEFT JOIN articlecolor ac on ol.articlecolorid=ac.id and ac.active=1
LEFT JOIN color c on ac.colorid=c.id 
LEFT JOIN articlesize az on oq.articlesizeid=az.id 
LEFT JOIN variantcode vc on ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid
WHERE ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and oq.active=1 and o.tocompanyid=1430
and o.done=0 and o.id=%d order by o.id", (int)$_POST['Order']);

	$res = dbQuery ($query) ;	
	while ($orderrow = dbFetch ($res)) {
		$rows[$orderrow['OrderQuantityId']] = $orderrow ;
	}
	dbQueryFree ($res) ;
	
	foreach ($rows as $row) { 
  		// Initialize
		$PickOrder = array (	
			'Type'				=>  "SalesOrder",	// Pick order from internal customer
			'Reference'			=>  0,			// Order number 
			'FromId'			=> 9587,		// NT Drappa Dot Picking stock
			'CompanyId'			=>  0,			// Shop Code	
			'OwnerCompanyId'	=>  1430,		// NW Drappa
			'HandlerCompanyId'	=>  2,		// Novotex
			'Packed'				=>  0,
			'DeliveryDate'		=>  ""			// Expected pick and pack complete
		) ;

		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		// Generate PickOrders
		$PickOrder['Reference'] = $row['OrderId'] ;
		$PickOrder['ReferenceId'] = $row['OrderId'] ;
		$PickOrder['CompanyId'] = $row['CompanyId'] ;
		$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
		
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $row['quantity'] ;
		$PickOrderLine['VariantDescription'] = $row['ArticleDescription'] ;
		$PickOrderLine['VariantColor'] = $row['color'] ;
		$PickOrderLine['VariantSize'] = $row['size'] ;

		// Update or insert into PickOrder table.
		if ($first) {
			$first=0 ;
			$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d and Active=1", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		}
			
		// Update or insert into PickOrderLine table.
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d and Active=1", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;
    }
    return 0 ;
?>
