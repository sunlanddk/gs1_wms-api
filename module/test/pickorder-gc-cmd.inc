<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$first=1 ;
	$query = sprintf(
"select o.id as OrderId, ol.Description as ArticleDescription, vc.id as VariantCodeId, vc.VariantCode as VariantCode, o.companyid as CompanyId, ol.deliverydate as DeliveryDate,
ol.articleid,  ol.articlecolorid, c.description as color, oq.articlesizeid, az.name as size, oq.quantity as quantity
from `order` o, orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az, variantcode vc
where ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and oq.active=1 and o.tocompanyid=787
and o.done=0
and ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid
and ol.articlecolorid=ac.id and ac.colorid=c.id and oq.articlesizeid=az.id and o.id=%s order by o.id", (int)$_POST['Order']);

	$res = dbQuery ($query) ;	
	while ($row = dbFetch ($res)) {
  		// Initialize
		$PickOrder = array (	
			'Type'				=>  "SalesOrder",	// Pick order from internal customer
			'Reference'			=>  0,			// Order number 
			'FromId'			=> 2059,		// GC ss09 Picking stock
			'CompanyId'			=>  0,			// Shop Code	
			'OwnerCompanyId'	=>  787,		// NW GC
			'HandlerCompanyId'	=>  2,		// Novotex
			'Packed'				=>  0,
			'DeliveryDate'		=>  ""			// Expected pick and pack complete
		) ;

		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		// Generate PickOrders
		$PickOrder['Reference'] = $row['OrderId'] ;
		$PickOrder['ReferenceId'] = $row['OrderId'] ;
		$PickOrder['CompanyId'] = $row['CompanyId'] ;
		$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
		
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $row['quantity'] ;
		$PickOrderLine['VariantDescription'] = $row['ArticleDescription'] ;
		$PickOrderLine['VariantColor'] = $row['color'] ;
		$PickOrderLine['VariantSize'] = $row['size'] ;

		// Update or insert into PickOrder table.
		if ($first) {
			$first=0 ;
			$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d and Active=1", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		}

		// Update or insert into PickOrderLine table.
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d and Active=1", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;
    }
    dbQueryFree ($res) ;
    return 0 ;
?>
