<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Season', TableGetField('Season','Name',$Id)) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

    if ($Navigation['Parameters'] == 'new') {
		$scr = 1 ;
	}
    itemSpace () ;
	echo '<table><tr>' ;
	
	$query = sprintf ("SELECT * FROM CollectionLot WHERE Active=1 AND SeasonId=%d", $Id) ;
	$res = dbQuery ($query) ;
	for ($i=1; $i<5; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		itemFieldRaw ('Name', formText ('Name['.$i.']', $row['Name'], 12, 'text-align:right;')) ;
		itemFieldRaw ('Date', formDate ('Date['.$i.']', $row['Name'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('PreSale', formCheckbox ('PreSale['.$i.']', $row['PreSale'], 12, 'text-align:right;')) ;	
		echo '</table></td>' ;
	}
	echo '</tr><tr>' ;
    itemSpace () ;
	for ($i=5; $i<9; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		itemFieldRaw ('Name', formText ('Name['.$i.']', $row['Name'], 12, 'text-align:right;')) ;
		itemFieldRaw ('Date', formDate ('Date['.$i.']', $row['Name'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('PreSale', formCheckbox ('PreSale['.$i.']', $row['PreSale'], 12, 'text-align:right;')) ;	
		echo '</table></td>' ;
	}
	echo '</tr></table>' ;
	dbQueryFree ($res) ;
    
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
