<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
//return 'scr ' . $_POST['WholeSalePrice']; 
	$_ShowZeroB2B = $_POST['ShowZeroB2B']=='on'?1:0 ;

	$_persize = 1 ;
	// With price per size.	
	if ($_persize) {
		$_WholeSalePrice = (float)0 ;
		$_RetailPrice    = (float)0 ;
		$_PurchasePrice  = (float)0 ;
		$_LogisticCost   = (float)0 ;
		if ($Navigation['Parameters'] == 'new') {
			if (isset($_POST['CurrencyId']) and (int)$_POST['CurrencyId']>0) {
				$CollectionMemberPrice['CurrencyId'] = (int)$_POST['CurrencyId'] ;
				$CollectionMemberPrice['CollectionMemberId'] = $Id ;
				$CollectionMemberPrice['ShowZeroB2B'] = $_ShowZeroB2B ;
				$Record['CollectionMemberId'] = $Id ;
				$CollectionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice) ;
$test = ' -1. CollectionMemberPriceId: ' . $CollectionMemberPriceId . ' cid ' . $CollectionMemberPrice['CollectionMemberId'] ;		
				$ColletionMemberPriceSizeId=0;
			} else {
				return 'Select Currency before create new' ;
			}
			$_tot = (float)0 ;
			foreach($_POST['WholeSalePrice'] as $key => $value) {
				if ($_WholeSalePrice>$_tot) $_tot = $_WholeSalePrice ;
				$_RetailPrice    = ((float)str_replace(',','.',$_POST['RetailPrice'][$key]) == 0) ? $_RetailPrice : (float)str_replace(',','.',$_POST['RetailPrice'][$key]) ;
				$_PurchasePrice  = ((float)str_replace(',','.',$_POST['PurchasePrice'][$key]) == 0) ? $_PurchasePrice : (float)str_replace(',','.',$_POST['PurchasePrice'][$key]) ;
				$_LogisticCost   = ((float)str_replace(',','.',$_POST['LogisticCost'][$key]) == 0) ? $_LogisticCost : (float)str_replace(',','.',$_POST['LogisticCost'][$key]) ;

				$CollectionmemberPriceSize['CollectionMemberPriceId'] = (int)$CollectionMemberPriceId;
				$CollectionmemberPriceSize['ArticleSizeId'] = (int)$key;
				$CollectionmemberPriceSize['WholeSalePrice'] = $_WholeSalePrice ;
				$CollectionmemberPriceSize['RetailPrice'] = $_RetailPrice ;
				$CollectionmemberPriceSize['PurchasePrice'] = $_PurchasePrice ;
				$CollectionmemberPriceSize['LogisticCost'] = $_LogisticCost  ;
				tableWrite('CollectionmemberPriceSize',$CollectionmemberPriceSize) ;
				$_WholeSalePrice = ((float)str_replace(',','.',$value) == 0) ? $_WholeSalePrice : (float)str_replace(',','.',$value);
			}
		} else {
			$CollectionMemberPriceId = $Id ; $_tot = (float)0 ;
			foreach($_POST['WholeSalePrice'] as $key => $value) {
//				if ($CollectionmemberPriceSize['WholeSalePrice']>$_tot) $_tot = $CollectionmemberPriceSize['WholeSalePrice'] ;

				$_WholeSalePrice = ((float)str_replace(',','.',$value) == 0) ? $_WholeSalePrice : (float)str_replace(',','.',$value);
				$_RetailPrice    = ((float)str_replace(',','.',$_POST['RetailPrice'][$key]) == 0) ? $_RetailPrice : (float)str_replace(',','.',$_POST['RetailPrice'][$key]) ;
				$_PurchasePrice  = ((float)str_replace(',','.',$_POST['PurchasePrice'][$key]) == 0) ? $_PurchasePrice : (float)str_replace(',','.',$_POST['PurchasePrice'][$key]) ;
				$_LogisticCost   = ((float)str_replace(',','.',$_POST['LogisticCost'][$key]) == 0) ? $_LogisticCost : (float)str_replace(',','.',$_POST['LogisticCost'][$key]) ;
				$CollectionMemberPriceSizeId = tableGetFieldWhere('collectionmemberpricesize', 'Id', sprintf('CollectionMemberPriceId=%d and ArticleSizeId=%d', $CollectionMemberPriceId, (int)$key)) ;
				$CollectionmemberPriceSize['CollectionMemberPriceId'] 	= (int)$CollectionMemberPriceId;
				$CollectionmemberPriceSize['ArticleSizeId'] 			= (int)$key;
				$CollectionmemberPriceSize['WholeSalePrice'] = $_WholeSalePrice ;
				$CollectionmemberPriceSize['RetailPrice'] = $_RetailPrice ;
				$CollectionmemberPriceSize['PurchasePrice'] = $_PurchasePrice ;
				$CollectionmemberPriceSize['LogisticCost'] = $_LogisticCost  ;
				tableWrite('CollectionmemberPriceSize',$CollectionmemberPriceSize, $CollectionMemberPriceSizeId) ;
				if ($_WholeSalePrice>$_tot) $_tot = $_WholeSalePrice ;
			}
		}
		$CollectionMemberPrice['ShowZeroB2B'] = $_ShowZeroB2B ;
		$CollectionMemberPrice['WholeSalePrice'] = (float)str_replace(',','.',$_tot); 
		$CollectionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice,$CollectionMemberPriceId) ;
$test .= ' 0. CollectionMemberPriceId: ' . $CollectionMemberPriceId . ' cid ' . $CollectionMemberPrice['CollectionMemberId'];		
		// set prices for other colors in collection
		if ($_POST['setall']=='on') {
			$query = sprintf('SELECT cm.collectionid as CollectionId, cm.articleid as ArticleId, cmp.currencyid as CurrencyId 
								FROM CollectionMember cm, CollectionMemberPrice cmp 
								WHERE cm.id=cmp.collectionmemberid and cmp.id=%d and cm.active=1 AND cmp.active=1 ', (int)$CollectionMemberPriceId) ;

			$res = dbQuery ($query) ;
			$cm_row = dbFetch($res) ;
			dbQueryFree($res) ;
			
			$query = sprintf('SELECT cm.articlecolorid as ArticleColorId, cm.id as CollectionMemberId, cmp.id as CollectionMemberPriceId 
								FROM CollectionMember cm
								LEFT JOIN CollectionMemberPrice cmp ON cmp.collectionmemberid=cm.id AND cmp.active=1 AND cmp.currencyid=%d
								WHERE cm.active=1 AND cm.CollectionId=%d AND cm.articleid=%d',
								(int)$cm_row['CurrencyId'],(int)$cm_row['CollectionId'],(int)$cm_row['ArticleId']) ;

			$res = dbQuery ($query) ;
			while ($row = dbFetch($res)) {
				$_WholeSalePrice = (float)0 ;
				$_RetailPrice    = (float)0 ;
				$_PurchasePrice  = (float)0 ;
				$_LogisticCost   = (float)0 ;
				if ($row['CollectionMemberPriceId']>0) {
					$CollectionMemberPriceId						= (int)$row['CollectionMemberPriceId'] ;
					$CollectionMemberPrice['CollectionMemberId'] 	= (int)$row['CollectionMemberId'] ;
				} else {
					$CollectionMemberPrice['CurrencyId'] 			= (int)$cm_row['CurrencyId'] ;
					$CollectionMemberPrice['CollectionMemberId'] 	= (int)$row['CollectionMemberId'] ;
					$CollectionMemberPrice['ShowZeroB2B'] 			= $_ShowZeroB2B ;
					$CollectionMemberPriceId						= tableWrite ('CollectionMemberPrice', $CollectionMemberPrice) ;
					$CollectionMemberPriceSizeId					= 0;
				}
$test .= ' 1. CollectionMemberPriceId: ' . $CollectionMemberPriceId . ' cid ' . (int)$row['CollectionMemberId'] ;		
				$_tot = (float)0 ;
				foreach($_POST['WholeSalePrice'] as $key => $value) {
					if ($row['CollectionMemberPriceId']>0) {
						$CollectionMemberPriceSizeId = tableGetFieldWhere('collectionmemberpricesize', 'Id', sprintf('CollectionMemberPriceId=%d and ArticleSizeId=%d AND Active=1', $CollectionMemberPriceId, (int)$key)) ;
					} else {
						$CollectionMemberPriceSizeId = 0 ;
					}
					$_WholeSalePrice = ((float)str_replace(',','.',$value) == 0) ? $_WholeSalePrice : (float)str_replace(',','.',$value);
					$_RetailPrice    = ((float)str_replace(',','.',$_POST['RetailPrice'][$key]) == 0) ? $_RetailPrice : (float)str_replace(',','.',$_POST['RetailPrice'][$key]) ;
					$_PurchasePrice  = ((float)str_replace(',','.',$_POST['PurchasePrice'][$key]) == 0) ? $_PurchasePrice : (float)str_replace(',','.',$_POST['PurchasePrice'][$key]) ;
					$_LogisticCost   = ((float)str_replace(',','.',$_POST['LogisticCost'][$key]) == 0) ? $_LogisticCost : (float)str_replace(',','.',$_POST['LogisticCost'][$key]) ;
		
					$CollectionmemberPriceSize['CollectionMemberPriceId'] 	= (int)$CollectionMemberPriceId;
					$CollectionmemberPriceSize['ArticleSizeId'] 			= (int)$key;
					$CollectionmemberPriceSize['WholeSalePrice'] = $_WholeSalePrice ;
					if ($CollectionmemberPriceSize['WholeSalePrice']>$_tot) 
						$_tot = $CollectionmemberPriceSize['WholeSalePrice'] ;
					$CollectionmemberPriceSize['RetailPrice'] = $_RetailPrice ;
					$CollectionmemberPriceSize['PurchasePrice'] = $_PurchasePrice ;
					$CollectionmemberPriceSize['LogisticCost'] = $_LogisticCost  ;
					tableWrite('CollectionmemberPriceSize',$CollectionmemberPriceSize, $CollectionMemberPriceSizeId) ;
				}
$test .= ' 2. CollectionMemberPriceId: ' . $CollectionMemberPriceId . ' cid ' . $CollectionMemberPrice['CollectionMemberId'];		
				$CollectionMemberPrice['WholeSalePrice'] = (float)str_replace(',','.',$_tot); 
				$CollectionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice,$CollectionMemberPriceId) ;
			}
			dbQueryFree($res) ;
		}
	// With one price for all sizes.	
	} else {
		if ($Navigation['Parameters'] == 'new') {
			if (isset($_POST['CurrencyId'])) {
				$CollectionMemberPrice['CurrencyId'] = (int)$_POST['CurrencyId'] ;
				$CollectionMemberPrice['CollectionMemberId'] = $Id ;
				$Record['CollectionMemberId'] = $Id ;
				$Id = 0 ;
			} else {
				return 'Select Currency before create new' ;
			}
		}

		$CollectionMemberPrice['WholesalePrice'] = (float)$_POST['WholeSalePrice'] ;
		$CollectionMemberPrice['RetailPrice'] = (float)$_POST['RetailPrice'] ;
		$CollectionMemberPrice['CampaignPrice'] = (float)$_POST['CampaignPrice'] ;
		$CollectionMemberPrice['CollectionPriceGroupId'] = (int)$_POST['PriceGroupId'];
		$CollectionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice, $Id) ;

		// View new Collection
	}
//return $test ;	
	return navigationCommandMark ('collmemberview', (int)$Record['CollectionMemberId']) ;

    return 0 ;    
?>
