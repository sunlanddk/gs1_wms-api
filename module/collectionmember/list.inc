<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

	
    itemStart () ;
    itemSpace () ;
    itemFieldIcon ('Season', $Record['Name'], 'season.png', 'seasonview', (int)$Record['Id']) ; 
    itemField ('Collection', $Record['CollectionName']) ; 
    itemField ('Brand', $Record['BrandName']) ; 
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemSpace () ;
    itemEnd () ;


    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon (40) ;
    listHead ('Article', 70)  ;
    listHeadIcon (10) ;
    listHead ('Description', 80) ;
    listHead ('DK', 60) ;
    listHead ('DE', 60) ;
    listHead ('UK', 60) ;
//    listHead ('Fabric', 100) ;
    listHead ('ColorNumber', 50) ;
    listHeadIcon (10) ;
    listHead ('ColorName', 70) ;
    listHead ('DK', 45) ;
    listHead ('DE', 45) ;
    listHead ('UK', 45) ;
		listHeadIcon () ;
		listHead ('FitsWith', 60) ;
//		listHead ('Campaign', 60) ;
//    listHead ('Program', 40) ;
    listHead ('Sex', 25) ;
    listHead ('LOT', 25) ;
    listHead ('#prices', 70) ;
    listHead ('#sizes', 25) ;
//    listHead ('Delivery', 80) ;
//    listHead ('Case', 70)  ;
    if ($ShowSeason)
    	listHead ('Collection', 150) ;
    if ($ShowCollection)
    	listHead ('Collection', 150) ;
    listHead ('Canceled', 70);
   listHead ('Focus', 30)  ;
   listHead ('Not on Web', 70)  ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
//		listFieldIcon ('component.gif', 'collmemberview', (int)$row['Id']) ;
		listStyleIcon ('collmemberview', $row) ;
    	listField ($row["Article"]) ;
 		listFieldIcon ('pen.gif', 'langedit', (int)$row['articleId']) ;
		listField ($row["Description"], '', 56) ;
		$description = tableGetFieldWhere('article_lang','WebDescription','LanguageId=4 and ArticleId='.(int)$row['articleId']) ;// explode ('#', $row["AltDesc"]) ; // 'concat(Description, ": ",WebDescription)'
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('article_lang','WebDescription','LanguageId=5 and ArticleId='.(int)$row['articleId']) ;
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('article_lang','WebDescription','LanguageId=6 and ArticleId='.(int)$row['articleId']) ;
   		listField ($description, '', 56) ;
//    	listField ($row["Fabric"]) ;
    	listField ($row["ColorNumber"]) ;
 		listFieldIcon ('pen.gif', 'coledit', (int)$row['ColorId']) ;
    	listField ($row["Color"], '', 56) ;
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=4 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=5 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=6 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
		listFieldIcon ('edit.gif', 'webgroupedit', (int)$row['Id']) ;
		listField ($row["WebFitGroup"]) ;
//    	listField ($row["WebCampaign"]) ;
//  		listField ($row["ProgramName"]) ;
    	listField ($row["SexName"]) ;
    	listField ($row["LOT"]) ;
		$query = sprintf ("SELECT group_concat(c.Name) as No FROM (CollectionMemberPrice cmp, Currency c) WHERE cmp.active=1 AND cmp.CollectionMemberId=%d AND cmp.CurrencyId>0 AND c.Id=cmp.CurrencyId GROUP BY CollectionMemberId", (int)$row['Id']) ;
	    $res = dbQuery ($query) ;
	    $row1 = dbFetch ($res) ;
    	listField ($row1["No"]) ;
	    dbQueryFree ($res) ;
		$query = sprintf ("SELECT count(*) as No FROM articlesize WHERE Active=1 AND articleId=%d", (int)$row['articleId']) ;
	    $res = dbQuery ($query) ;
	    $row1 = dbFetch ($res) ;
    	listField ($row1["No"] ) ;
	    dbQueryFree ($res) ;

//    	listField ($row["Planned"]) ;
//    	listField ($row["Case"]) ;
        if ($ShowSeason)
    		listField ($row["SeasonName"]) ;
        if ($ShowCollection)
    		listField ($row["Collection"]) ;
		if ($row["Cancel"])
			listField ($row["CancelDescription"]?$row["CancelDescription"]:'Canceled', '', 56) ;
		else
			listField ('') ;
		if ($row["Focus"])
			listField ('Focus') ;
		else
			listField ('') ;

	if ($row["notonwebshop"])
			listField ('Not on Webshop', '', 56) ;
		else
			listField ('') ;

    }

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
