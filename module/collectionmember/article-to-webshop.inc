<?php 

$collectionMemberId = $_GET['id'];

if(WEBSHOP_API_URL != ''){
	//// Update or add products from season to magento database.
	$potdata = json_encode(array('fromPot' => array('request' => 'create_article','data'    => $collectionMemberId)));
	$ch = curl_init(WEBSHOP_API_URL);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_TIMEOUT, 600);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	$result = json_decode($result);
	echo 'Collectionmember send to webshop and the importer has begun, you can now close the window.';
}
else{
	echo 'Remote endpoint not specified';	
}

return 0;


?>