<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;

	global $User, $Navigation, $Record, $Result2, $Size, $Id ;

//    if (dbNumRows($Result) == 0) return "No Collections assigned to this Season" ;
    printf ("<table><tr><td width=250>") ; 	
    itemStart () ;
    itemSpace () ;
    itemFieldIcon ('Season', $Record['SeasonName'], 'component.gif', 'seasonview', (int)$Record['SeasonId']) ; 
    itemFieldIcon ('Collection', $Record['CollectionName'],  'component.gif', 'collmemberlist', (int)$Record['CollectionId']) ; 
//    itemField ('Program', $Record['ProgramName']) ;
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemFieldIcon ('Company Coll.', ALL,  'component.gif', 'collcompanylst', (int)$Record['CompanyId']) ; 
    itemFieldIcon ('Article', tableGetField('Article','Number',$Record['ArticleId']) . ' ' . tableGetField('Article','Description',$Record['ArticleId']) , 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
    if ($Record['ArticleColorId'] > 0) {
	    $ColorId = tableGetField('ArticleColor', 'ColorId',$Record['ArticleColorId']) ;
	    itemField ('Color', tableGetField('Color','Number', $ColorId) . '  ' . tableGetField('Color','Description', $ColorId)) ;
	} else {
	    itemField ('Color', 'No Color') ;
	}
    if ($Record['Cancel']) {
	    itemSpace () ;
	    itemField ('Cancelled', $Record['CancelDescription']) ;
    } else
	    itemField ('LOT', $Record['LOT']) ;
	    itemField ('Focus style?', $Record['Focus']>0?'Yes':'No') ;
	    itemField ('On Web?', $Record['notonwebshop']>0?'No':'Yes') ;
    itemSpace () ;
	itemEnd () ;
	printf ("</td><td>") ;
//	class="item-img" src="image/thumbnails/
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '.jpg' ;
    printf ('<img width=250px src="' . $photo .'">') ;
	printf ("</td></tr><tr>") ;

$_persize=1 ;
    // List prices
if ($_persize==0) {
    printf("<td colspan=2><table><tr><td width=250>") ;
    listStart () ;
    listRow () ;

    listHead ('PriceGroup', 95) ;
    listHead ('Currency', 95) ;
    listHead ('Wholesale', 95) ;
    listHead ('Retail', 75) ;
    listHead ('Campaign', 75) ;
	listhead ('Edit',30) ;
	listhead ('Delete',45) ;
	
	$query = sprintf ("SELECT cmp.id as CollectionMemberPriceId, cpg.Name as PriceGroupName, c.Name as CurrencyName, cmp.WholeSalePrice, cmp.RetailPrice, cmp.CampaignPrice 
						from (CollectionMemberPrice cmp, Currency c )
						left join CollectionPriceGroup cpg ON cmp.collectionpricegroupid=cpg.id 
						Where cmp.CollectionMemberId=%d and cmp.CurrencyId=c.Id and cmp.Active=1 order by PriceGroupName, CurrencyName", $Id) ;
	$result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
    	listRow () ;
    	listField ($row["PriceGroupName"]) ;
    	listField ($row["CurrencyName"]) ;
    	listField (number_format ((float)$row["WholeSalePrice"], 2, ',', '.')) ;
    	listField (number_format ((float)$row["RetailPrice"], 2, ',', '.')) ;
    	listField ($row["CampaignPrice"]) ;
		listFieldIcon ('edit.gif', 'addcmprice', (int)$row['CollectionMemberPriceId']) ;
		listFieldIcon ('delete.gif', 'deleteprice', (int)$row['CollectionMemberPriceId']) ;
    }
   	listRow () ;
	listFieldIcon ('add.gif', 'addprice', (int)$Id) ;
	dbQueryFree ($result) ;
    listEnd () ;
} else {
    printf ("<td colspan=2><table><tr><td width=250>") ;
    listStart () ;
	listHeader ('Prices', 60) ;
    listRow () ;

	$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder, CollectionmemberPrice.Id as CollectionmemberPriceId, 
								CollectionmemberPriceSize.WholeSalePrice, CollectionmemberPriceSize.RetailPrice, 
								CollectionmemberPriceSize.PurchasePrice, CollectionmemberPriceSize.LogisticCost, 
								Currency.Name as Currency
						FROM ArticleSize
						INNER JOIN CollectionmemberPrice ON CollectionmemberPrice.CollectionMemberId=%d  AND CollectionmemberPrice.Active=1
						INNER JOIN Currency ON Currency.Id=CollectionmemberPrice.CurrencyId
						INNER JOIN CollectionmemberPriceSize ON CollectionmemberPriceSize.ArticleSizeId=ArticleSize.Id AND CollectionmemberPriceSize.CollectionMemberPriceId=CollectionMemberPrice.Id AND CollectionmemberPriceSize.Active=1
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY Currency.Id, ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	$Size = array() ;
	while ($row = dbFetch ($result)) {
		$Size[$row['Currency']][$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;
		if (count($Size) > 0) {
			listHead('Sizes', 60) ;
			listHead('', 60) ;
			foreach (current($Size) as $i => $s) 
				listHead (htmlentities($s['Name']), 110) ;
			listhead ('Edit',30) ;
			listhead ('Delete',45) ;
			foreach ($Size as $j => $sizes) {
				$tot = 0 ;
				foreach ($sizes as $i => $k) {
					$tot += (float)$k['WholeSalePrice'] ;
				}
				listRow () ;
				listField ($j) ;
				listField ('Sale') ;
				$tot = 0 ;
				foreach ($sizes as $i => $k) {
					listField (number_format((float)$k['WholeSalePrice'], (int)2, ',', '.') . ' / ' . number_format((float)$k['RetailPrice'], (int)2, ',', '.')) ;
					$tot += (float)$k['PurchasePrice'] ;
				}
				if ($tot>0) {
					listRow () ;
					listField ('') ;
					listField ('Purchase') ;
					foreach ($sizes as $i => $k) 
						listField (number_format((float)$k['PurchasePrice'], (int)2, ',', '.') . ' / ' . number_format((float)$k['LogisticCost'], (int)2, ',', '.')) ;
				}
//				listField ($k['CollectionmemberPriceId']) ;
				$tmp = (int)$k['CollectionmemberPriceId'] ;
				listFieldIcon ('edit.gif', 'addcmprice', $tmp) ;
				listFieldIcon ('delete.gif', 'deleteprice', $tmp) ;
			}
			listRow () ;
			
		}
		listFieldIcon ('add.gif', 'addprice', (int)$Id) ;
		listEnd () ;
		printf ("</td></tr></table><br><br>") ;
		printf ("</td></tr><tr>") ;
}
   
	// Assortments
	$query = sprintf ("SELECT ArticleSize.*, CollectionmemberAssortment.AssortmentId, CollectionmemberAssortment.Quantity FROM ArticleSize
						INNER JOIN CollectionmemberAssortment ON CollectionmemberAssortment.ArticleSizeId=ArticleSize.Id AND CollectionmemberAssortment.CollectionMemberId=%d AND CollectionmemberAssortment.Active=1
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY CollectionmemberAssortment.AssortmentId, ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;

	$Size = array() ;
	while ($row = dbFetch ($result)) {
		$Size[(int)$row['AssortmentId']][$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;

    printf ("<td colspan=2><table><tr><td width=750>") ;
	listStart () ;
	listRow () ;
	listHead ('Assortments', 60) ;
	if (count($Size) > 0) {
		foreach (current($Size) as $i => $s) 
			listHead (htmlentities($s['Name']), 60) ;
		listhead ('Variantcode',60) ;
		listhead ('Edit',20) ;
		listhead ('Delete',20) ;
		listhead ('PickQuantity',50) ;
		listhead ('Position',50) ;
		foreach ($Size as $j => $sizes) {
			listRow () ;
			listField ($j) ;
			foreach ($sizes as $i => $s) {
				listField (number_format ((int)$s['Quantity'], (int)0, ',', '.')) ;
			}
			$query = sprintf ("SELECT VariantCode.VariantCode, VariantCode.PickQuantity, Container.Position FROM VariantCode, Container 
								WHERE Container.Id=VariantCode.PickContainerId and Reference=%d and `Type`='assort' and VariantCode.Active=1", $j) ;
/*
			SELECT variantcode.Reference,variantcode.PickQuantity, variantcode.Type, article.number as ArticleNumber, article.Description as ArticleDescription 
						FROM variantcode, assortment, collectionmember, article 
						WHERE variaType`='assort' and variantcode.Active=1 AND PickContainerId=%d
                        and assortment.id=variantcode.reference and collectionmember.id=assortment.CollectionmemberId and article.id=collectionmember.articleid", $Id) ;
*/			
			$result = dbQuery ($query) ;
			$row = dbFetch ($result);
			listField ($row['VariantCode']) ;
			dbQueryFree ($result) ;
//			listField (tableGetFieldWhere('variantcode','VariantCode',sprintf("Reference=%d and `Type`='assort' and Active=1", $j))) ;
			listFieldIcon ('edit.gif', 'editassort', (int)$j) ;
			listFieldIcon ('delete.gif', 'deleteassort', (int)$j) ;
			listField ($row['PickQuantity']) ;
			listField ($row['Position']) ;
		}
	}
	listRow () ;
	listFieldIcon ('add.gif', 'addassort', (int)$Id) ;
	listEnd () ;
	printf ("</td></tr></table><br><br>") ;
	printf ("</td></tr></table>") ;
	
    // List sizes
    listStart () ;
    listRow () ;
    listHead ('Size',15) ;
    listHeadIcon () ;
    listHead ('Active',15) ;
    listHeadIcon () ;
    listHead ('Variant', 80)  ;
    listHeadIcon () ;
    listHead ('PickStock', 25) ;
    listHead ('Position', 25) ;
    listHead ('PickContainer',25) ;

    while ($row = dbFetch($Result2)) {
    	listRow () ;
		navigationSetParameter ('storesingle',sprintf('&Ean=%s&StockId=%d&CollMemberId=%d', $row["VariantCode"],(int)$Record['FromStockId'],$Id)) ;
    	listField ($row["VariantSize"]) ;
    	if ($row['Id']>0)
			listFieldIcon ('move.gif', 'storesingle', (int)$Record['PickStockId']) ;
		else
			listField ('') ;
		if ($row['VarDisabled']) {
			$form_var[$row['ArticleSizeId']] = 0 ;
		   	listField ('No') ;
			listFieldIcon ('add.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		} else {
			$form_var[$row['ArticleSizeId']] = 1 ;
		   	listField ('Yes') ;
			listFieldIcon ('delete.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		}
    	listField ($row["VariantCode"]) ;
    	if ($row['Id']>0)
			listFieldIcon ('stock.gif', 'variantinv', (int)$row['Id']) ;
		else
			listField ('') ;
    	listField ($row["StockName"]) ;
    	listField ($row["Position"]) ;
    	listField ($row["ContainerId"]) ;
    }
    listEnd () ;
	
	formStart () ;
	itemFieldRaw ('', formHidden('CollMemberId', $Id, 8,'','')) ;
	itemFieldRaw ('', formHidden('disable', 1, 8,'','')) ;
	if (is_array($form_var)) {
	foreach ($form_var as $varid => $value)
		itemFieldRaw ('', formHidden(sprintf('disable[%d]',(int)$varid), $value, 1,'','')) ;
	}
    formEnd () ;
    dbQueryFree ($Result2) ;

    return 0 ;

?>
