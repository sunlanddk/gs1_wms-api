<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	
    itemStart () ;
    itemSpace () ;
    itemField ('Style Group', TableGetField('Collection','Name',$Id)) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

    if ($Navigation['Parameters'] == 'new') {
		if (isset($_GET['ArticleId'])) {
			$article_id = (int)$_GET['ArticleId'] ;
		} else {
			$article_id = 0 ;
		}

		//		itemFieldRaw ('Article', formText ('ArticleNumber', '' , 15)) ;
		$query = 'SELECT Article.Id, concat(Article.Number, " - ", Article.Description) AS Value FROM Article WHERE Active=1 ORDER BY Value' ;
		$reload = sprintf('onchange="appLoad(%d,%d,\'ArticleId=\'+document.appform.ArticleId.options[document.appform.ArticleId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
		itemFieldRaw ('Article', formDBSelect ('ArticleId', $article_id, $query, 'width:250px;', NULL, $reload)) ; 
		//	    itemFieldRaw ('OrderLine', formDBSelect ('OrderLineId', (int)$OrderLine['Id'], $query, 'width:400px;', NULL, sprintf('onchange="appLoad(%d,%d,\'orderline=\'+document.appform.OrderLineId.options[document.appform.OrderLineId.selectedIndex].value);"', (int)$Navigation['Id'], $Id))) ; 

		//		itemFieldRaw ('Color', formText ('Color', '',15)) ;
		if ($article_id > 0) {
			$query = sprintf('SELECT ArticleColor.Id as Id, concat(Color.Number, " - ", Color.Description) AS Value 
								FROM (Color, ArticleColor) 
								LEFT JOIN collectionmember cm ON cm.articlecolorid=ArticleColor.Id and cm.articleid=ArticleColor.ArticleId
											and cm.collectionid=%d and cm. active=1
								WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND ArticleColor.ColorId=Color.Id AND Color.Active=1 
										AND isnull(cm.id)
								ORDER BY Value', $Id, $article_id) ;
								// printf($query) ;
			itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', 0, $query, 'width:250px;')) ;  

			$query = sprintf("SELECT cl.Id as Id, cl.Name AS Value FROM Collectionlot cl, collection c WHERE c.Id=%d and cl.SeasonId=c.SeasonId AND cl.Active=1 ORDER BY Date", $Id) ;
			$res = dbQuery ($query) ;
			$row = dbFetch($res) ;
			itemFieldRaw ('LOT', formDBSelect ('CollectionLOTId', (int)$row['Id'], $query, 'width:100px;')) ; 
			dbQueryFree ($res) ;
		}
	}
    itemSpace () ;
    itemSpace () ;
	echo '<table><tr>' ;
	
    if ($Navigation['Parameters'] == 'new') 
		$Id = 0 ;
$_perprice=1 ;	
if ($_perprice==0) {
	$query = sprintf ("SELECT * FROM CollectionMemberPrice WHERE Active=1 AND CollectionMemberId=%d AND CurrencyId>0", $Id) ;
	$res = dbQuery ($query) ;
	
    if (!($Navigation['Parameters'] == 'new')) {
		itemFieldRaw ('Set prices for all colors ', formCheckbox ('setall', 0) . ' (of same article in this collection)') ;
		itemSpace () ;
	} 

	for ($i=1; $i<5; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		if ($Navigation['Parameters'] == 'new' and $i==1)
			$row['CurrencyId'] =1 ;
		itemFieldRaw ('Currency', formDBSelect ('CurrencyId['.$i.']', (int)$row['CurrencyId'], 'SELECT Currency.Id, Currency.Name AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:100px')) ; 
		itemFieldRaw ('Wholesale Price', formText ('PriceWholeSale['.$i.']', $row['WholesalePrice'], 12, 'text-align:right;')) ;
		itemFieldRaw ('Retail Price', formText ('PriceRetail['.$i.']', $row['RetailPrice'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('Campaign Price', formText ('PriceCampaign['.$i.']', $row['CampaignPrice'], 12, 'text-align:right;')) ;	
//		itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', (int)0, "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
		echo '</table></td>' ;
	}
	echo '</tr><tr>' ;
    itemSpace () ;
	for ($i=5; $i<9; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		itemFieldRaw ('Currency', formDBSelect ('CurrencyId['.$i.']', (int)$row['CurrencyId'], 'SELECT Currency.Id, Currency.Name AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:100px')) ; 
		itemFieldRaw ('Wholesale Price', formText ('PriceWholeSale['.$i.']', $row['WholesalePrice'], 12, 'text-align:right;')) ;
		itemFieldRaw ('Retail Price', formText ('PriceRetail['.$i.']', $row['RetailPrice'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('Campaign Price', formText ('PriceCampaign['.$i.']', $row['CampaignPrice'], 12, 'text-align:right;')) ;	
//		itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', (int)0, "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
		echo '</table></td>' ;
	}
	echo '</tr></table>' ;
	dbQueryFree ($res) ;
 }   
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
