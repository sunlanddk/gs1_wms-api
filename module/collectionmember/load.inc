<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/list.inc' ;
    
	global $User, $Navigation, $Record, $Result2, $Size, $Id ;

    switch ($Navigation['Function']) {
	    
	case 'list' :
		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Article',			'db' => 'a.number'),
		array ('name' => 'Description',			'db' => 'a.description'),
		array ('name' => 'ColorName',			'db' => 'co.description'),
//		array ('name' => 'Program',			'db' => 'p.name'),
		array ('name' => 'WebFitGroup',			'db' => 'colm.WebFitGroup'),
		array ('name' => 'Sex',			'db' => 'sex.name'),
		array ('name' => 'LOT',			'db' => 'cl.name')
	    ) ;

	    require_once 'lib/list.inc' ;
	    switch ($Navigation['Parameters']) {
		case 'season' :
		    	$query = sprintf ("SELECT season.Id,season.Name, season.CompanyId,'ALL' as CollectionName,'ALL' as BrandName,  season.ExplicitMembers 
					FROM Season WHERE Season.Id=%d", $Id) ;
		        $res = dbQuery ($query) ;
		        $Record = dbFetch ($res) ;
		        dbQueryFree ($res) ;

			  if ($Record['ExplicitMembers']) {
				$queryClause = sprintf('s.id=%d and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
								and s.active=1 and c.active=1 and colm.active=1 and a.active=1
								group by colm.id', $Id) ;
			  } else {
				$queryClause = sprintf('o.seasonid=%d and s.id=o.seasonid and ol.orderid=o.id and ol.articleid=a.id
								and s.active=1 and o.active=1 and ol.active=1 and a.active=1
								group by ol.articleid, ol.articlecolorid', $Id) ;
			  }
			$ShowCollection = 1 ;
			navigationPasive('collmemberadd') ;
			navigationPasive('cmlwebshop') ;

		  	break ;
		case 'company' :
		    	$query = sprintf ("SELECT Company.Id,'ALL' AS Name, Company.Id as CompanyId,'ALL' as CollectionName,'ALL' as BrandName,  1 as ExplicitMembers 
					FROM Company WHERE Company.Id=%d", $Id) ;
		        $res = dbQuery ($query) ;
		        $Record = dbFetch ($res) ;
		        dbQueryFree ($res) ;

			  if ($Record['ExplicitMembers']) {
				$queryClause = sprintf('s.companyid=%d and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
								and s.active=1 and s.done=0 and c.active=1 and colm.active=1 and a.active=1
								group by colm.id', $Id) ;
			  } else {
				$queryClause = sprintf('o.companyid=%d and s.id=o.seasonid and ol.orderid=o.id and ol.articleid=a.id
								and s.active=1 and s.done=0 and o.active=1 and ol.active=1 and a.active=1
								group by ol.articleid, ol.articlecolorid', $Id) ;
			  }
			$ShowCollection = 1 ;
			$ShowSeason = 1 ;

		  	break ;

		case 'webshop' :
			$ShowWebshop = 1 ;
			// List field specification
			$fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Article',			'db' => 'a.number'),
				array ('name' => 'Description',			'db' => 'a.description'),
				array ('name' => 'ColorNumber',			'db' => 'co.number'),
				array ('name' => 'WebFitGroup',			'db' => 'colm.WebFitGroup'),
				array ('name' => 'WebCampaign',			'db' => 'colm.WebCampaign')
			) ;

		default:
			$queryClause = sprintf('c.id=%d and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
							and s.active=1 and c.active=1 and colm.active=1 and a.active=1
							group by colm.id', $Id) ;

		      $query = sprintf ("SELECT season.Id,season.Name, season.CompanyId,Collection.Name as CollectionName, Brand.Name as BrandName, season.ExplicitMembers 
						 FROM (Season, Collection) LEFT JOIN Brand on Brand.Id=Collection.BrandId WHERE Season.Id=Collection.SeasonId and Collection.Id=%d", $Id) ;
		      $res = dbQuery ($query) ;
		      $Record = dbFetch ($res) ;
		      dbQueryFree ($res) ;
			$ShowCollection = 0 ;
			break ;
	    }

		if ($Record['ExplicitMembers']) {
			$queryFields = "colm.id as Id, c.name as Collection, colm.caseid as `Case`, cl.Name as LOT, sex.Name as SexName, p.Name as ProgramName,
					cl.Date as Planned, colm.Cancel as Cancel, colm.notonwebshop as notonwebshop, colm.CancelDescription as CancelDescription,
					colm.WebFitGroup as WebFitGroup,colm.WebCampaign as WebCampaign,
					a.number as Article,a.number as ArticleNumber, a.description as Description, a.id as articleId, a.DeliveryComment as AltDesc, colm.Focus as Focus,
					co.Id as ColorId, co.description as Color, co.altdescription as AltColor, co.number as ColorNumber, s.name as SeasonName, s.Id as SeasonId, s.CompanyId as CompanyId,
					count(az.id) as NoSizes, sum(if(IsNull(ct.id) or ct.Position='',0,1)) as SizesWithPos, 
					min(if(IsNull(ct.id) or ct.Position='',null,ct.Position)) as MinPos";
	    
			$queryTables = '(season s, collection c, collectionmember colm, article a)
					left join CollectionLOT cl on colm.CollectionLOTId=cl.Id
					left join articlecolor ac on colm.ArticleColorId=ac.id and ac.active=1
					left join sex ON ac.sexid=sex.id
						LEFT JOIN collectionmemberprogram cmp ON cmp.collectionmemberid=colm.id and cmp.active=1
						LEFT JOIN `program` p ON p.id=cmp.programid
					left join color co on co.id=ac.colorid and co.active=1
					left join articlesize az on az.articleid=a.id and az.active=1
					left join variantcode vc on vc.articleid=a.id and vc.articlecolorid=ac.id and vc.articlesizeid=az.id and vc.active=1
					left join container ct on ct.id=vc.pickcontainerid and ct.active=1' ;
			
		} else {
			$queryFields =  "ol.id as Id, c.name as Collection, a.number as Article, a.description as Description, a.id as articleId, sex.Name as SexName,
					co.description as Color, co.number as ColorNumber, s.name as SeasonName, s.Id as SeasonId, s.CompanyId as CompanyId,
					count(az.id) as NoSizes, sum(if(IsNull(ct.id) or ct.Position='',0,1)) as SizesWithPos,
					min(if(IsNull(ct.id) or ct.Position='',null,ct.Position)) as MinPos";
	    
			$queryTables = '(`order` o, orderline ol, season s, article a)
					left join collection c on c.seasonid=s.id and c.active=1 
					left join articlecolor ac on ol.ArticleColorId=ac.id and ac.active=1
					left join sex ON ac.sexid=sex.id
					left join color co on co.id=ac.colorid and co.active=1
					left join articlesize az on az.articleid=a.id and az.active=1
					left join variantcode vc on vc.articleid=a.id and vc.articlecolorid=ac.id and vc.articlesizeid=az.id and vc.active=1
					left join container ct on ct.id=vc.pickcontainerid and ct.active=1' ;
		}

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ; //, NULL, NULL, "a.Number, co.description") ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'collmemberview') ;
	    
	case 'view' :
		if  (isset($_GET['CollMemberId'])) 
		     $Id = (int)$_GET['CollMemberId'] ;
	
	    $query = sprintf ("SELECT season.Id as SeasonId,season.Name as SeasonName, season.CompanyId, season.PickStockId as PickStockId, season.WebshopId as WebshopId,
							season.FromStockId as FromStockId,
							Collection.Name as CollectionName, Collection.Id as CollectionId, p.Name as ProgramName,
							CollectionMember.*,  Collectionlot.Name as LOT, Article.Number as ArticleNumber, co.Number as ColorNumber
						FROM (Season, Collection, CollectionMember, Article)
					left join articlecolor ac on CollectionMember.ArticleColorId=ac.id and ac.active=1
					left join color co on co.id=ac.colorid and co.active=1
						LEFT JOIN Collectionlot ON Collectionlot.id=CollectionLOTId
						LEFT JOIN collectionmemberprogram cmp ON cmp.collectionmemberid=collectionmember.id  and cmp.active=1
						LEFT JOIN `program` p ON p.id=cmp.programid
						WHERE Season.Id=Collection.SeasonId and Collection.Id=CollectionMember.CollectionId  AND CollectionMember.Id=%d AND Article.Id=CollectionMember.ArticleId", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		if ($Record['notonwebshop']==1 or $Record['WebshopId']==0) {
			navigationPasive ('UpdateCMToWebshop') ;
		}
	    // Variant list
	    $DescriptionField = 'if (VariantCode.VariantDescription="", Article.Description, VariantCode.VariantDescription)' ;
	    $ColorField = 'if (VariantCode.VariantColorDesc="", Color.Description,VariantCode.VariantColorDesc)' ;
	    $ColorCodeField = 'if (VariantCode.VariantColorCode="", Color.Number,VariantCode.VariantColorCode)' ;
	    $SizeField = 'if (VariantCode.VariantSize="" or VariantCode.VariantSize is null, ArticleSize.Name,VariantCode.VariantSize)' ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Variant',			'db' => 'VariantCode'),
		array ('name' => 'Position',		'db' => 'Position'),
//		array ('name' => 'Article',		'db' => 'Article.Number'),
//		array ('name' => 'Description',	'db' => $DescriptionField),
//		array ('name' => 'Color',		'db' => $ColorCodeField),
//		array ('name' => 'ColorDesc',		'db' => $ColorField),
		array ('name' => 'Size',		'db' => $SizeField),
		array ('name' => 'Stock',		'db' => 'Stock.Name')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
					VariantCode.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode, ArticleSize.Id as ArticleSizeId, ' .
					$DescriptionField . ' as VariantDescription,' .
					$ColorField . ' as VariantColor,' .
					$ColorCodeField . ' as VariantNumber,' .
					$SizeField . ' as VariantSize,
					if(articlesizecolldisable.id>0, 1, 0) as VarDisabled,
					Article.Number as ArticleNumber';
	    
		$queryTables = '(CollectionMember, VariantCode)
			 LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
			 LEFT JOIN ArticleSize ON ArticleSize.Id=VariantCode.ArticleSizeId and ArticleSize.Active=1 
			 LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
			 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
			 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
			 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1
			 LEFT JOIN articlesizecolldisable ON articlesizecolldisable.articlesizeId=ArticleSize.Id and articlesizecolldisable.CollectionMemberId=CollectionMember.Id' ;


		// Clause
		$queryClause = sprintf ('CollectionMember.Id=%d AND CollectionMember.Active=1 AND VariantCode.Active=1 
						AND CollectionMember.articleid=VariantCode.articleid 
						AND CollectionMember.articleColorid=VariantCode.articlecolorid ', $Id) ;

	    $query = 'SELECT ' . $queryFields . ' FROM ' . $queryTables . ' WHERE ' . $queryClause . ' ORDER BY ArticleSize.DisplayOrder' ;
	    $Result2 = dbQuery ($query) ;
	    
		navigationSetParameter ('varcolldis',sprintf('&CollMemberId=%d&disable=1',(int)$Id)) ;

		return 0 ;
 	case 'collbasic' :
	case 'collbasic-cmd' :
 	case 'move' :
	case 'move-cmd' :
	case 'colldelete-cmd' :
		$query = sprintf ("SELECT Collection.*, Season.name as SeasonName, Season.OwnerId as OwnerId FROM Collection, Season WHERE Season.Id=Collection.SeasonId and Collection.Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;

	case 'editprice' :
	case 'editprice-cmd' :
	    switch ($Navigation['Parameters']) {
		    case 'new' :
				$query = sprintf ("SELECT c.articleid as ArticleId, c.articleColorid as ArticleColorId, a.CostCurrencyId as CostCurrencyId 
									FROM (collectionmember c, article a) 
									WHERE c.id=%d AND a.id=c.articleid", $Id) ;
				$res = dbQuery ($query) ;
				$Record = dbFetch ($res) ;
				dbQueryFree ($res) ;
				break ;
			Default:
				$query = sprintf ("SELECT cmp.*, c.articleid as ArticleId, c.articleColorid as ArticleColorId, a.CostCurrencyId as CostCurrencyId FROM (CollectionMemberPrice cmp, collectionmember c, article a) 
									WHERE cmp.Active=1 AND cmp.Id=%d AND c.id=cmp.collectionmemberid AND a.id=c.articleid", $Id) ;
				$res = dbQuery ($query) ;
				$Record = dbFetch ($res) ;
				dbQueryFree ($res) ;
		}
		return 0 ;

 
		
 	case 'add' :
	case 'add-cmd' :
	case 'collmemberdelete-cmd' :
		$query = sprintf ("SELECT cm.*, c.SeasonId as SeasonId FROM CollectionMember cm, Collection c WHERE cm.Active=1 AND cm.Id=%d AND cm.collectionid=c.id", $Id) ;
	      $res = dbQuery ($query) ;
	      $Record = dbFetch ($res) ;
	      dbQueryFree ($res) ;
		return 0 ;

 	case 'basic' :
	case 'basic-cmd' :
		$query = sprintf ("SELECT cm.*, c.seasonid as SeasonId, a.number as ArticleNumber, a.Description as ArticleDescription, 
									co.Description as ColorDescription, co.Number as ColorNumber, p.Id as ProgramId, cmp.id as CollectionmemberProgramId
					FROM (CollectionMember cm, Collection c, article a, articlecolor ac, color co )
					    LEFT JOIN sex on ac.sexid=sex.id
						LEFT JOIN collectionmemberprogram cmp ON cmp.collectionmemberid=cm.id and cmp.active=1
						LEFT JOIN `program` p ON p.id=cmp.programid
						WHERE cm.Active=1 AND a.Active=1 AND ac.Active=1 AND co.Active=1 AND cm.collectionid=c.id AND
								cm.articleid=a.id AND cm.articlecolorid=ac.id AND ac.colorid=co.id AND cm.Id=%d", $Id) ;
        $res = dbQuery ($query) ;
        $Record = dbFetch ($res) ;
		dbQueryFree ($res) ;
		return 0 ;

	case 'rawmat' :
	case 'rawmatprod' :
	    $query = sprintf ("SELECT * FROM Season WHERE Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Article',			'db' => 'MaterialArticleNumber'),
		array ('name' => 'Description',			'db' => 'MaterialArticleDescription'),
		array ('name' => 'Color',		'db' => 'Color.Number'),
		array ('name' => 'ColorDesc',		'db' => 'Color.Description'),
//		array ('name' => 'SizeDim',		'db' => $SizeDimField),
//		array ('name' => 'TotalNeed',			'db' => 'TotalNeed')
//		array ('name' => 'Color',		'db' => $ColorCodeField),
//		array ('name' => 'ColorDesc',		'db' => $ColorField),
//		array ('name' => 'Size',		'db' => $SizeField),
//		array ('name' => 'Stock',		'db' => 'Stock.Name')
	    ) ;
	    require_once 'lib/list.inc' ;

	    $SizeDimField ="if(ProdQtyMatList.SizeDimensions = '-1', '', ProdQtyMatList.SizeDimensions)" ;
	    $ProductionField ="concat(CaseId, '/', ProdNumber)" ;

	    $queryFields = "MaterialArticleNumber, MaterialArticleDescription, FMaterialArticleId as ArticleId, if (FinalMaterialArticleColorid is null, 0, FinalMaterialArticleColorid) as FinalMaterialArticleColorid, UnitName, 
					MaterialArticleSizeId,MaterialArticleDimension,
					Color.Number as ColorNumber, Color.Description as ColorDescription, ArticleColor.Reference as ArticleColorReference," .
					$SizeDimField . " as MaterialSizeDimensions," .
					$ProductionField . " as Production, CaseId, ProdId, MaterialDate, supplier.Name as suppliercompany,
    					round(coalesce(Sum(Need),0)) as TotalNeed,
    					if (sum(SFCUsage)>0, '*', '') as AltBreakdownUsage" ;

	    $queryTables = sprintf('(Select Prod.StyleId, 
    if(smcv.AltMaterialArticleId is null Or smcv.AltMaterialArticleId = 0, MatArticle.Number, AltMaterialArticle.Number) as MaterialArticleNumber,
    if(smcv.AltMaterialArticleId is null Or smcv.AltMaterialArticleId = 0, MatArticle.Id, AltMaterialArticle.Id) as FMaterialArticleId,
    if(smcv.AltMaterialArticleId is null Or smcv.AltMaterialArticleId = 0, MatArticle.Description, AltMaterialArticle.Description) as MaterialArticleDescription,
    if(smcv.AltMaterialArticleId is null Or smcv.AltMaterialArticleId = 0, SM.articleid, smcv.AltMaterialArticleId) as MaterialArticleId,
    if(smcv.AltMaterialArticleColorId is null or smcv.AltMaterialArticleColorId = 0, SM.MaterialArticleColorid, smcv.AltMaterialArticleColorId) as FinalMaterialArticleColorId,
    Prod.CaseId as CaseId, Prod.Number as ProdNumber, Prod.Id as ProdId, Prod.MaterialDate as MaterialDate,
    ProdQty.quantity as Quantity,
    if(smsv.QuantityVariant is null, SM.usage, smsv.QuantityVariant) * SM.NumberOf as `Usage`,
    if((smsv.VariantArticleSizeId is null or smsv.VariantArticleSizeId <= 0)
    And (smsv.VariantArticleDimension is null or smsv.VariantArticleDimension <= 0),
    if(SizeDimenstionsArticleSize.Name is not null, SizeDimenstionsArticleSize.Name, SM.SizeDimentions),
    if((smsv.VariantArticleSizeId is null or smsv.VariantArticleSizeId = 0), smsv.VariantArticleDimension, ArticleSize.Name )) as SizeDimensions,
    if(MatArticle.VariantSize = 1, if(smsv.VariantArticleSizeId is null or smsv.VariantArticleSizeId <=0, SM.SizeDimentions, 
if(smcv.AltMaterialArticleId is null Or smcv.AltMaterialArticleId = 0,
    smsv.VariantArticleSizeId,
    (select id from ArticleSize AAA where AAA.Name=ArticleSize.Name and AAA.ArticleId=smcv.AltMaterialArticleId and AAA.Active=1))
), 0) as MaterialArticleSizeId,
    if(MatArticle.VariantDimension = 1, if((smsv.VariantArticleDimension is null or smsv.VariantArticleDimension <= 0), SM.SizeDimentions, smsv.VariantArticleDimension), 0) as MaterialArticleDimension,

    (if(smsv.QuantityVariant is null, if(SM.OrderNumber=1 and ProdQty.Consumption>0,ProdQty.Consumption,if(SFC.Usage>0,SFC.Usage,if(SM.usage>0,SM.usage,0))), smsv.QuantityVariant) * SM.NumberOf * ProdQty.quantity
	* (1+ (if(MatCostPrice.Wastage is not null, MatCostPrice.Wastage, MatArticleType.Wastage) /100))
	) as Need,
    if(SFC.Usage>0,SFC.Usage,0) as SFCUsage,
    ArticleSize.DisplayOrder,
    MatArticle.suppliercompanyid as suppliercompanyid,
    Unit.Name as UnitName
From (production Prod)
    Inner Join (select ol.productionid as Id from `Order` O, Orderline ol where ol.orderid=o.Id and ol.active=1 and o.active=1 and o.SeasonId=%d group by Id) ProdOrder on ProdOrder.Id=Prod.Id
    Inner Join ProductionQuantity ProdQty on ProdQty.productionId = Prod.id and ProdQty.active=1
    Inner Join Stylepageversion spv on Prod.StyleId = spv.styleid And spv.pageTypeid = 4 And spv.Current = 1
    Inner Join StyleMaterial SM on SM.StyleMaterialsVersionId = spv.PageId
    Left Join stylefabricconsumption SFC on SFC.ProductionId=Prod.Id And SM.OrderNumber = 1
    Left Outer Join StyleMaterialSizeVariation smsv on smsv.StyleMaterialId = SM.Id and ProdQty.ArticleSizeId = smsv.ArticleSizeId
    Left Outer Join StyleMaterialColorVariation smcv on smcv.StyleMaterialId = SM.Id and ProdQty.ArticleColorId = smcv.ArticleColorId and ProdQty.ArticleColorId = smcv.ArticleColorId
    Left Outer Join Article AltMaterialArticle on smcv.AltMaterialArticleId = AltMaterialArticle.Id
    Inner Join Article as MatArticle on SM.ArticleId = MatArticle.Id
    Left Outer Join ArticleCostPrice as MatCostPrice on MatCostPrice.ArticleId = SM.ArticleId And (SM.ArticleStyleId is null or SM.ArticleStyleId=0 or MatCostPrice.StyleId = SM.ArticleStyleId)
    Inner join ArticleType as MatArticleType on MatArticle.ArticleTypeId = MatArticleType.Id
    Inner join Style as ProdStyle on Prod.styleId = ProdStyle.Id
    Inner join Article as ProdArticle on ProdStyle.ArticleId = ProdArticle.Id
    Left Outer Join ArticleCostPrice as ProdCostPrice on ProdCostPrice.ArticleId = ProdStyle.ArticleId And ProdCostPrice.StyleId = Prod.StyleId
    Inner join ArticleType as ProdArticleType on ProdArticle.ArticleTypeId = ProdArticleType.Id
    Left Outer Join ArticleSize on smsv.VariantArticleSizeId is not null And smsv.VariantArticleSizeId != 0
			And ArticleSize.Id = smsv.VariantArticleSizeId
    Left Outer Join ArticleSize SizeDimenstionsArticleSize on MatArticle.VariantSize=1 And SM.SizeDimentions > 0 And SizeDimenstionsArticleSize.Id = SM.SizeDimentions
    Left Outer join Unit on Unit.id=MatArticle.unitid
) ProdQtyMatList     
Left Outer Join ArticleColor on ArticleColor.Id = ProdQtyMatList.FinalMaterialArticleColorId
Left Outer Join Color on Color.Id = ArticleColor.ColorId
Left Outer Join Company Supplier on Supplier.id=ProdQtyMatList.suppliercompanyid and Supplier.Active=1
', $Id) ;

	    // Clause
	    $queryClause = '1=1' ;

	    if ($Navigation['Parameters']=='prodgroup') {
		$queryGroup = 'MaterialArticleNumber, FinalMaterialArticleColorid, SizeDimensions, ProdId';
		$queryOrder = 'MaterialArticleNumber, ColorNumber, SizeDimensions, Production' ;
		$ShowProduction = 1 ;
	    } else {
		$queryGroup = 'MaterialArticleNumber, FinalMaterialArticleColorid, SizeDimensions';
		$queryOrder = '' ;
		$ShowProduction = 0 ;
	    }

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrder, $queryGroup) ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'articleview') ;
	    return 0 ;

    }

    return 0 ;
?>
