<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
//	'ArticleNumber'		=> array ('mandatory' => true,	'check' => true),
	'ArticleId'			=> array ('mandatory' => true,	'check' => true),
//	'Color'				=> array ('mandatory' => true,	'check' => true),
	'CollectionLOTId'	=> array ('mandatory' => false,	'check' => true),
	'ArticleColorId'	=> array ('mandatory' => true,	'check' => true),
	'CollectionId'		=> array ('type' => 'set')
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'CollectionLOTId' :
			return true ;
	    case 'ArticleId' :
	    case 'ArticleColorId' :
			if (!$changed) return false ;
			if ($value>0)
				return true ;
			else
				return false ;
	    case 'ArticleNumber' :
			$fields['ArticleId']['value']  = tableGetFieldWhere('Article','Id',sprintf('Number="%s" AND Active=1', $value));
			if ($fields['ArticleId']['value']>0) 
			  return false;
			else
			  return sprintf ('article not found, number "%s"', $value) ;
		case 'Color' :
			$query = sprintf ('SELECT ArticleColor.Id FROM Color, ArticleColor 
						WHERE Color.Id=ArticleColor.ColorId AND ArticleColor.ArticleId=%d AND Color.Number="%s" AND Color.Active=1 AND ArticleColor.Active=1', $fields['ArticleId']['value'] ,$value) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ($row['Id'] <= 0) { 
				if (tableGetField('Article','VariantColor',$fields['ArticleId']['value']))
					return sprintf ('Color not found, number "%s" for this article', $value) ;
				else
					$fields['ArticleColorId']['value']  = 0 ;
			} else
				$fields['ArticleColorId']['value']  = $row['Id']  ;
			return false ;
	}
	if (!$changed) return false ;
	return true ;	
    }
    
    if ($Navigation['Parameters'] == 'new') {
	
//	    if (!($fields['ArticleColorId']['value'] > 0)) return "Colour definition is mandatory" ;

	    $fields['CollectionId']['value']  = $Id ;
		$Id = -1 ;
		$res = saveFields ('CollectionMember', $Id, $fields, true) ;
		if ($res) return $res ;

		// always make price record with currcyid=0
		$CollectionMemberPrice['CollectionMemberId'] = (int)$Record['Id'] ;
		$CollectionMemberPrice['WholesalePrice'] = (float)0;
		$CollectionMemberPrice['RetailPrice'] = (float)0 ;
		$CollectionMemberPrice['CampaignPrice'] = (float)0;
		$CollectionMemberPrice['CurrencyId'] = (int)0;
		$CollectionMemberPrice['CollectionPriceGroupId'] = (int)$_POST['PriceGroupId'];
		$ColletionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice) ;

	} else
	    $Record['Id'] = $Id ;
	
		
	for ($i=1; $i<9; $i++)  {
		$PriceId = 0 ;
//return 'hallo ' . (int)$_POST['CurrencyId'][$i];
		if ((int)($_POST['CurrencyId'][$i]>0)) {
			if ($_POST['setall']=='on')
				$query = sprintf('SELECT * FROM CollectionMember Where active=1 AND CollectionId=%d AND articleid=%d',(int)$Record['CollectionId'],(int)$Record['ArticleId']) ;
			else
				$query = sprintf('SELECT * FROM CollectionMember Where active=1 AND Id=%d',(int)$Record['Id']) ;
			$res = dbQuery ($query) ;
			while ($row = dbFetch($res)) {
				$PriceId = tableGetFieldWhere('CollectionMemberPrice', 'Id', sprintf('CurrencyId=%d and CollectionMemberId=%d',(int)$_POST['CurrencyId'][$i],(int)$row['id'])) ;

				$CollectionMemberPrice['CollectionMemberId'] = (int)$row['id'] ;
				$CollectionMemberPrice['WholesalePrice'] = (float)$_POST['PriceWholeSale'][$i] ;
				$CollectionMemberPrice['RetailPrice'] = (float)$_POST['PriceRetail'][$i];
				$CollectionMemberPrice['CampaignPrice'] = (float)$_POST['PriceCampaign'][$i];
				$CollectionMemberPrice['CurrencyId'] = (int)$_POST['CurrencyId'][$i];
	//			$CollectionMemberPrice['CollectionPriceGroupId'] = (int)$_POST['PriceGroupId'];
// return 'test ' . $PriceId . ' ' . $row['id'] . ' ' . $query;
				$ColletionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice, $PriceId) ;
			}
		}
	}
    // View new Collection
    return navigationCommandMark ('collmemberview', (int)$Record['Id']) ;

    return 0 ;    
?>
