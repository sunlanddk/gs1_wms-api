<?php

    require_once 'lib/table.inc' ;

    // delete Collection member
    $query = sprintf ("UPDATE CollectionMember SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE CollectionId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
    dbQuery ($query) ;

    // Do delete 
    tableDelete ('Collection', $Id) ;

    return navigationCommandMark ('seasonview', (int)$Record['SeasonId']) ;
?>
