<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Collection', TableGetField('Collection','Name',$Id)) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

	echo '<table><tr>' ;
	// Update lines
	$First = 1 ;
    foreach ($_POST['SelectFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		
		// set default values from fisrt selected line
		if ($First==1) {
			$query = sprintf ("SELECT * FROM CollectionMemberPrice WHERE Active=1 AND CollectionMemberId=%d AND CurrencyId>0", $id) ;
			$res = dbQuery ($query) ;
			for ($i=1; $i<5; $i++)  {
				$row = dbFetch ($res) ;	
				echo '<td><table>' ;
				itemFieldRaw ('Currency', formDBSelect ('CurrencyId['.$i.']', (int)$row['CurrencyId'], 'SELECT Currency.Id, Currency.Name AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:100px')) ; 
				itemFieldRaw ('Wholesale Price', formText ('PriceWholeSale['.$i.']', $row['WholesalePrice'], 12, 'text-align:right;')) ;
				itemFieldRaw ('Retail Price', formText ('PriceRetail['.$i.']', $row['RetailPrice'], 12, 'text-align:right;')) ;	
				itemFieldRaw ('Campaign Price', formText ('PriceCampaign['.$i.']', $row['CampaignPrice'], 12, 'text-align:right;')) ;	
		//		itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', (int)0, "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
				echo '</table></td>' ;
			}
			echo '</tr><tr>' ;
			itemSpace () ;
			for ($i=5; $i<9; $i++)  {
				$row = dbFetch ($res) ;	
				echo '<td><table>' ;
				itemFieldRaw ('Currency', formDBSelect ('CurrencyId['.$i.']', (int)$row['CurrencyId'], 'SELECT Currency.Id, Currency.Name AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:100px')) ; 
				itemFieldRaw ('Wholesale Price', formText ('PriceWholeSale['.$i.']', $row['WholesalePrice'], 12, 'text-align:right;')) ;
				itemFieldRaw ('Retail Price', formText ('PriceRetail['.$i.']', $row['RetailPrice'], 12, 'text-align:right;')) ;	
				itemFieldRaw ('Campaign Price', formText ('PriceCampaign['.$i.']', $row['CampaignPrice'], 12, 'text-align:right;')) ;	
		//		itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', (int)0, "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
				echo '</table></td>' ;
			}
			echo '</tr></table>' ;
			dbQueryFree ($res) ;
		}
		$First=0 ;
		listFieldRaw (formHidden(sprintf('SelectFlag[%d]', (int)$id), $id)) ;

	}
	if ($First==1) return 'No lines selected' ;
    itemSpace () ;

    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
