<?php
    require_once 'lib/table.inc' ;

    // delete Collection member
	$CollectionMemberId = tableGetField('Assortment', 'CollectionMemberId',$Id) ;
	
    $query = sprintf ("UPDATE CollectionmemberAssortment SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE AssortmentId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
    dbQuery ($query) ;

    tableDelete ('Assortment', $Id) ;

    return navigationCommandMark ('collmemberview', (int)$CollectionMemberId) ;

?>
