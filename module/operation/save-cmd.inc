<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Number'		=> array ('mandatory' => true),
	'Description'		=> array (),
	'BundleText'		=> array (),
	'ProductionMinutes'	=> array ('mandatory' => true,	'type' => 'decimal',	'format' => '6.2')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['MachineGroupId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Id = -1 ;
	    break ;
    }
     
    return saveFields ('Operation', $Id, $fields) ;
?>
