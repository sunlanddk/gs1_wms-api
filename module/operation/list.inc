<?php

    require_once "lib/table.inc" ;
    require_once "lib/html.inc" ;
    require_once "lib/navigation.inc" ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>MachinGroup</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=57><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>BundleText</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Minutes</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/'.$Navigation['Icon'], navigationOnClickLink ("edit", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Number'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['BundleText'])) ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No operations</p></td></tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
