<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Certificate WHERE CertificateTypeId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the Cartificate Type has associated Certificates") ;

    // Do deleting
    tableDelete ('CertificateType', $Id) ;

    return 0
?>
