<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
		
	    // Default values
		$Record['FromCompanyId'] = $User['CompanyId'] ;
	    break ;
    }
     
    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 20)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%')) ;
    itemSpace () ;
    itemFieldRaw ('Departure', formDate ('DepartureDate', dbDateDecode($Record['DepartureDate']))) ;
    itemFieldRaw ('Arrival', formDate ('ArrivalDate', dbDateDecode($Record['ArrivalDate']))) ;
    itemFieldRaw ('Stock', formDate ('StockDate', dbDateDecode($Record['StockDate']))) ;
    itemSpace () ;
    itemFieldRaw ('Owner Company', formDBSelect ('FromCompanyId', (int)$Record['FromCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  


    if ($Navigation['Parameters'] != 'new') {
	itemSpace () ;
	flag ($Record, 'Ready') ;
	flag ($Record, 'Departed') ;
	flag ($Record, 'Arrived') ;
	flag ($Record, 'Verified') ;
	flag ($Record, 'Done') ; 
	itemInfo ($Record) ;
    }
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
