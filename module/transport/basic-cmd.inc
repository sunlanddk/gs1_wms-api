<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true),
	'Description'		=> array (),

	'DepartureDate'		=> array ('type' => 'date',	'mandatory' => true),
	'ArrivalDate'		=> array ('type' => 'date',	'mandatory' => true),
	'StockDate'		=> array ('type' => 'date',	'mandatory' => true),
	'FromCompanyId'		=> array ('type' => 'integer',				'check' => false),

	'Type'			=> array ('type' => 'set'),

	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),
	'Departed'		=> array ('type' => 'checkbox',	'check' => true),
	'DepartedUserId'	=> array ('type' => 'set'),
	'DepartedDate'		=> array ('type' => 'set'),
	'Arrived'		=> array ('type' => 'checkbox',	'check' => true),
	'ArrivedUserId'		=> array ('type' => 'set'),
	'ArrivedDate'		=> array ('type' => 'set'),
	'Verified'		=> array ('type' => 'checkbox',	'check' => true),
	'VerifiedUserId'	=> array ('type' => 'set'),
	'VerifiedDate'		=> array ('type' => 'set'),
	'Done'			=> array ('type' => 'checkbox',	'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set')
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Any Containers assigned to transport
		$query = sprintf ("SELECT Id FROM Container WHERE StockId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count == 0)  {
			// Any Shipments assigned to transport
			$query = sprintf ("SELECT Id FROM Stock WHERE BatchId=%d AND Active=1", $Id) ;
			$result = dbQuery ($query) ;
			$count = dbNumRows ($result) ;
			dbQueryFree ($result) ;
			if ($count == 0)  
				return 'the Transportation can not be set Ready when it has no Containers or shipments' ;
		}

		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Departed':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Ready
		if (!$Record['Ready']) return 'the Transportation can not Depart befor it is Ready' ;	

		// Set tracking information
		$fields['DepartedUserId']['value'] = $User['Id'] ;
		$fields['DepartedDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Arrived':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Departed']) return 'the Transportation can not be Arrive before it has Departed' ;	

		// Set tracking information
		$fields['ArrivedUserId']['value'] = $User['Id'] ;
		$fields['ArrivedDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Verified':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Arrived']) return 'the Transportation can not be Verified before it has Arrived' ;	

		// Set tracking information
		$fields['VerifiedUserId']['value'] = $User['Id'] ;
		$fields['VerifiedDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Done':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Verified']) return 'the Transportation can not be Done befor Verified' ;	

		// Check the all containers has been moved from the transport
		$query = sprintf ("SELECT Id FROM Container WHERE StockId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'the Transportation can not be Done when it still has Containers' ;

		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;
		return true ;

	}
	return false ;
    }

    function saveValidate ($changed) {
	global $User, $Record, $Id, $fields ;

	// Only validate if changed
	if (!$changed) return false ;

	// Validate dates
	if (dbDateDecode($Record['DepartureDate']) >= dbDateDecode ($Record['ArrivalDate'])) return 'Departure must be before Arrival' ;
	if (dbDateDecode($Record['ArrivalDateyDate']) >= dbDateDecode ($Record['StockDate'])) return 'Arrival must be befor expected date in Stock' ;
	
	return true ;
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Departed']) ;
	    unset ($fields['Arrived']) ;
	    unset ($fields['Verified']) ;
	    unset ($fields['Done']) ;
	    $fields['Type']['value'] = 'transport' ;
	    $Id = -1 ;
	    break ;

	default :
	    if ($Record['Done']) return 'Transportation is Done' ;
	    break ;
    }

     
    $res = saveFields ('Stock', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Production
//	    return navigationCommandMark ('transportview', $Record['Id']) ;
    }

    return 0 ;    
?>
