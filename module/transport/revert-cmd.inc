<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    
    $fields = array (
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'Departed'		=> array ('type' => 'checkbox',	'check' => true),
	'Arrived'		=> array ('type' => 'checkbox',	'check' => true),
	'Verified'		=> array ('type' => 'checkbox',	'check' => true),
    ) ;

    $flaglist = '' ;
    $flagcount = 0 ;
    
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	global $flagcount, $flaglist ;
	switch ($fieldname) {

	    default:
		if (!$changed or $value) return false ;
		$flaglist .= sprintf ("        %s\n", $fieldname) ;
		$flagcount++ ;
		return true ;
	}
	return false ;
    }
    
    $res = saveFields ('Stock', $Id, $fields, true) ;
    if ($res) return $res ;
    
    // Generate news
    if ($flagcount > 0) {
	unset ($row) ;
	$row['Header'] = substr(sprintf ("Transport %s (%s) reverted", $Record['Name'], $Record['Description']),0,79) ;
	$row['Text'] = sprintf ("The flag%s:\n\n%s\nhas been reverted by %s (%s) at %s", ($flagcount > 1) ? 's' : '', $flaglist, $User['FullName'], $User['Loginname'], date('Y-m-d H:i:s', dbDateDecode($Record['ModifyDate']))) ;
	tableWrite ('News', $row) ;
    }

    return 0 ;
	
?>
