<?php

    require_once 'lib/table.inc' ;

    // Verify state og transport
    if ($Record['Ready']) return 'the Transportation is Ready' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Container WHERE StockId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the Transportation has associated Containers") ;

    // Do delete  
    tableDelete ('Stock', $Id) ;

    return 0
?>
