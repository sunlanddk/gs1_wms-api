<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemField ($field, 'No') ;
	}
    }

    itemStart () ;
    itemHeader () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Description', $Record['Description']) ;
   itemField ('Owner', $Record['Owner']) ;

    itemSpace () ;
    itemField ('State', $Record['State']) ;

    itemSpace () ;
    itemField ('Containers', sprintf ('%d Pcs', (int)$Record['ContainerCount'])) ;

    itemSpace () ;
    itemField ('Departure', date ('Y-m-d', dbDateDecode($Record['DepartureDate']))) ;
    itemField ('Arrival', date ('Y-m-d', dbDateDecode($Record['ArrivalDate']))) ;
    itemField ('Stock', date ('Y-m-d', dbDateDecode($Record['StockDate']))) ;
    
    itemSpace () ;
    flag ($Record, 'Ready') ;
    flag ($Record, 'Departed') ;
    flag ($Record, 'Arrived') ;
    flag ($Record, 'Verified') ;
    flag ($Record, 'Done') ; 

    itemInfo ($Record) ;
    itemEnd () ;

    return 0 ;
?>
