<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    
    switch ($Navigation['Function']) {
	case 'view' :
	case 'delete-cmd' :
	    if ($Id <= 0) {
		navigationPasive('sheed') ;
		navigationPasive('print') ;
		navigationPasive('delete') ;
		return 0 ;
	    }

	    // Get Bundle information
	    $query = sprintf ("SELECT Bundle.*, ArticleSize.Name AS SizeName, 
CONCAT(Color.Description,' (',Color.Number,')') AS ColorNumber, 
CONCAT(Case.Id,'/',Production.Number) AS Number, 
Production.StyleId, Production.Description, Production.SourceLocationId, Production.SourceFromNo, 
Production.SourceToNo, Production.SourceIn, Production.SourceInDate, Article.Number as ArticleNumber, 
Article.Description AS ArticleDescription 
FROM (Bundle, ProductionQuantity, Production) 
LEFT JOIN ArticleSize ON ArticleSize.Id=ProductionQuantity.ArticleSizeId 
LEFT JOIN ArticleColor ON ArticleColor.Id=ProductionQuantity.ArticleColorId 
LEFT JOIN Color ON Color.Id=ArticleColor.ColorId 
LEFT JOIN `Case` ON Case.Id=Production.CaseId 
LEFT JOIN Article ON Article.Id=Case.ArticleId 
WHERE Bundle.Id=%d AND Bundle.Active=1 AND ProductionQuantity.Id=Bundle.ProductionQuantityId 
AND Production.Id=Bundle.ProductionId", $Id) ;
	    $result = dbQuery ($query) ;
	    if (count($result) > 1) return sprintf ('%s(%d) only one row expected, count %d', count($result)) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Get operation information
	    $Operation = array () ;
	    $query = sprintf ("SELECT CurrentStyleOperation.Id, CurrentStyleOperation.No, CurrentStyleOperation.Description, 
CurrentStyleOperation.ProductionMinutes, 
CONCAT(MachineGroup.Number,Operation.Number) AS Number, 
IFNULL(BundleQuantity.Quantity,Bundle.Quantity) AS Quantity,
OutSourcing.LocationId AS Location,
OutSourcing.InDate AS InDate 
FROM (Bundle, CurrentStyleOperation, Operation, MachineGroup) 
LEFT JOIN BundleQuantity ON BundleQuantity.BundleId=Bundle.Id 
AND BundleQuantity.Active=1 AND BundleQuantity.StyleOperationId=CurrentStyleOperation.Id
LEFT JOIN OutSourcing ON Bundle.Id = OutSourcing.BundleId
AND CurrentStyleOperation.No >= OutSourcing.OperationFromNo
AND CurrentStyleOperation.No <= OutSourcing.OperationToNo

WHERE Bundle.Id=%d AND CurrentStyleOperation.StyleId=%d 
AND CurrentStyleOperation.Active=1 AND CurrentStyleOperation.OperationId=Operation.Id 
AND CurrentStyleOperation.MachineGroupId=MachineGroup.Id
ORDER BY CurrentStyleOperation.No", $Id, $Record['StyleId']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['Id'] ; 
		$Operation[$i] = $row ;
		$Operation[$i]['work'] = array () ;
	    }
	    dbQueryFree ($result) ;

	    // Get work information
	    $BundleWorkCount = 0 ;
	    $query = sprintf ("SELECT BundleWork.*, CONCAT(User.FirstName,' ',User.LastName,' (',User.Loginname,')') AS UserName FROM BundleWork LEFT JOIN WorkSheet ON WorkSheet.Id=BundleWork.WorkSheetId LEFT JOIN User ON User.Id=WorkSheet.UserId WHERE BundleWork.BundleId=%d AND BundleWork.Active=1", $Id) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['StyleOperationId'] ;
		if (!isset($Operation[$i])) {
		    printf ("%s(%d) BundleId %d, BundleWorkId %d, CurrentStyleOperation (%d) not found<br>\n", __FILE__, __LINE__, $Id, $row['Id'], $i) ;
		    continue ;
		}
		$Operation[$i]['work'][(int)$row['Id']] = $row ;
		$BundleWorkCount++ ;
	    }
	    dbQueryFree ($result) ;

	    // Get Name of Source Location
	    if ($Record['SourceLocationId'] > 0) $Record['SourceLocationName'] = tableGetField ('ProductionLocation', 'Name', $Record['SourceLocationId']) ;

	    // Deleting Bundle is not allowed when work has been registrated
	    if ($BundleWorkCount > 0) navigationPasive('delete') ;

	    return 0 ;

	case 'sheet' :
	case 'print-cmd' :
	    // Load Bundle (for verification of existence)
	    $query = sprintf ("SELECT Id FROM Bundle WHERE Active=1 AND Id=%d", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;

	case 'adjust' :
	case 'adjust-cmd' :
	    // Extract ID's
	    $No = (int)($Id%100) ;
	    $BundleId = (int)($Id/100) ;
	    
	    // Get Quantity information for operation
	    $query = sprintf ("SELECT %d AS Id, Bundle.Id AS BundleId, Bundle.Quantity AS BundleQuantity, CurrentStyleOperation.No, Production.Id AS ProductionId, Production.StyleId, IFNULL(BundleQuantity.Quantity,Bundle.Quantity) AS Quantity FROM 
(Bundle, Production, CurrentStyleOperation) LEFT JOIN BundleQuantity ON BundleQuantity.BundleId=Bundle.Id AND BundleQuantity.Active=1 AND BundleQuantity.StyleOperationId=CurrentStyleOperation.Id WHERE Bundle.Id=%d AND Production.Id=Bundle.ProductionId AND CurrentStyleOperation.StyleId=Production.StyleId AND CurrentStyleOperation.No=%d AND CurrentStyleOperation.Active=1", $Id, $BundleId, $No) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;

	case 'work' :
	case 'work-cmd' :
	    // Get Work information
	    $query = sprintf ("SELECT BundleWork.*, CurrentStyleOperation.No, IFNULL(BundleQuantity.Quantity,Bundle.Quantity) AS BundleQuantity, CONCAT(User.FirstName,' ',User.LastName,' (',User.Loginname,')') AS UserName FROM (BundleWork, Bundle, CurrentStyleOperation) LEFT JOIN BundleQuantity ON BundleQuantity.BundleId=BundleWork.BundleId AND BundleQuantity.Active=1 AND BundleQuantity.StyleOperationId=BundleWork.StyleOperationId LEFT JOIN WorkSheet ON WorkSheet.Id=BundleWork.WorkSheetId LEFT JOIN User ON User.Id=WorkSheet.UserId WHERE BundleWork.Id=%d AND Bundle.Id=BundleWork.BundleId AND CurrentStyleOperation.Id=BundleWork.StyleOperationId", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;
    }

    return 0 ;
?>
