<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    
    // Get quantity
    $quantity_string = trim($_POST['Quantity']) ;
    if ($quantity_string != '0') $quantity_string = ltrim($quantity_string, '0') ;
    $quantity = (int)$quantity_string ;
    if ($quantity_string != (string)$quantity) return sprintf ("invalid quantity") ;

//printf ("BundleQuantity %d, new quantity %d, BundleId %d, ProductionId %d, StyleId %d<br>\n", $Record['BundleQuantity'], $quantity, $Record['BundleId'], $Record['ProductionId'], $Record['StyleId']) ;

    // Changed ?
    if ($quantity == (int)$Record['Quantity']) return 0 ;

    // If the new quantity is higher than the bundles quantity, all operations should be modified
    if ($quantity > (int)$Record['BundleQuantity']) $No = 1 ;
//printf ("No %d<br><br>\n", $No) ;

    // Check all BundleQuantities
    $query = sprintf ("SELECT CurrentStyleOperation.No, CurrentStyleOperation.Id AS StyleOperationId, BundleQuantity.Id, BundleQuantity.Quantity, SUM(BundleWork.Quantity) AS BundleWorkQuantity FROM CurrentStyleOperation 
    LEFT JOIN BundleQuantity ON BundleQuantity.BundleId=%d AND BundleQuantity.Active=1 AND BundleQuantity.StyleOperationId=CurrentStyleOperation.Id 
    LEFT JOIN BundleWork ON BundleWork.BundleId=%d AND BundleWork.Active=1 AND BundleWork.StyleOperationId=CurrentStyleOperation.Id 
    WHERE CurrentStyleOperation.StyleId=%d AND CurrentStyleOperation.Active=1 GROUP BY CurrentStyleOperation.Id", $Record['BundleId'], $Record['BundleId'], $Record['StyleId']) ;
    $result = dbQuery ($query) ;
    $Operation = array () ;
    while ($row = dbFetch ($result)) {
//printf ("No %d, Id %d, Quantity %d, WorkQuantity %d<br>\n", $row['No'], $row['Id'], $row['Quantity'], $row['BundleWorkQuantity'] ) ;

	if ($quantity > (int)$Record['Quantity']) {
	    // Raising the bundle qunatity
	    // When raising bundle quantity all operations with a lower quantity should be updated
	    // This means deleting the items for the system to use the quantity for the bundle
	    if ($row['Id'] > 0 and $row['Quantity'] <= $quantity) {
		// Delete quantity
		$Operation[(int)$row['No']] = array (
		    'Id'		=> $row['Id'],
		    'Active'		=> 0
		) ;
    	    }

	} else {
	    // Lowering the bundle quantity
	    // All operation from and including the selected operation should be updated, meaning deleting any specific
	    // BundleQuantity for the system to use the new bundle size. The pervious operations not allready set, must
	    // have a specific BundleQuantity equal to the previous bundle size	
	    if ($row['No'] >= $No) {
		// Validate quantity of registrated work
		if ($row['BundleWorkQuantity'] > $quantity) return sprintf ("Quantity for registrated work (%d) for Operation (line %d) exceeds the new bundle quantity (%d)", $row['BundleWorkQuantity'], $row['No'], $quantity) ;

		if ($row['Id'] > 0) {
		    // Delete quantity
		    $Operation[(int)$row['No']] = array (
			'Id'		=> $row['Id'],
			'Active'		=> 0
		    ) ;
		}

	    } else {
		if ($row['Id'] <= 0) {
		    // Make new BundleQuantity
		    $Operation[(int)$row['No']] = array (
			'BundleId'		=> $Record['BundleId'],
			'ProductionId'		=> $Record['ProductionLineId'],
			'StyleOperationId'	=> $row['StyleOperationId'],
			'Quantity'		=> $Record['Quantity'],
			'Active'		=> 1
		    ) ;
		}
	    }
	}
    }
    dbQueryFree ($result) ;
   
    // Update Bundle
    $fields = array (
	'Quantity'		=> array ('type' => 'set', 'value' => $quantity)
    ) ;
    $res = saveFields ('Bundle', $Record['BundleId'], $fields) ;
    if ($res) return $res ;

    // Do the specific opdates
    foreach ($Operation as $n => $op) {
	if ($op['Active']) {
//printf ("create, No %d, Quantity %d<br>\n", $n, $op['Quantity']) ;
	    tableWrite ('BundleQuantity', $op) ;
	} else {
//printf ("delete, No %d, Id %d<br>\n", $n, $op['Id']) ;
	    tableDelete ('BundleQuantity', $op['Id']) ; 
	}
    }

    return 0 ;
?>
