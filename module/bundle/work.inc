<?php

    require_once 'lib/html.inc' ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Bundle</td><td class=itemfield>%08d</td></tr>\n", $Record['BundleId']) ;
    printf ("<tr><td class=itemlabel>No</td><td class=itemfield>%d</td></tr>\n", $Record['No']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Operator</td><td class=itemfield>%s</td></tr>\n", $Record['UserName']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Quantity</td><td><input type=text name=Quantity maxlength=2 size=2' value=%d></td></tr>\n", $Record['Quantity']) ;
    printf ("<tr><td class=itemfield>Minutes</td><td><input type=text name=UsedMinutes maxlength=7 size=7 value=\"%s\"></td></tr>\n", htmlentities($Record['UsedMinutes'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
