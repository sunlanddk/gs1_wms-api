<?php

    require_once 'lib/parameter.inc' ;

    if ($Id <= 0) return "no bundle selected" ;
	
    $printer = trim(parameterGet('PrinterLabel')) ;
    if ($printer == '') return 'no Label Printer defined' ; 
    require_once 'module/makebundle/label.inc' ;
    $res = PrintLabel ($Id, $printer) ;
    if ($res) return $res ;
    
    // $printer = trim(parameterGet('PrinterBundle')) ;
    // if ($printer == '') return 'no Bundle Printer defined' ; 
    // require_once 'module/makebundle/sheet.inc' ;
    // $res = PrintSheet ($Id, $printer) ;
    // if ($res) return $res ;

    return 0 ;
?>
