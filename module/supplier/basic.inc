<?php

    switch ($Navigation["Parameters"]) {
	case "new":
	    unset ($Record) ;
	    break ;
   }

    require_once "lib/html.inc" ;
    $MyCompanyId = 787 ; 
	
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Record['Id']) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td><p>Number</p></td><td><input type=text name=Number maxlength=10 value=\"%s\" style=\"width:95px\"></td></tr>\n", htmlentities($Record["Number"])) ;
    printf ("<tr><td><p>Name</p></td><td><input type=text name=Name size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress1 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=%sAddress2 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=20 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    printf ("<tr><td class=itemlabel>Country</td><td>%s</td></tr>\n", htmlDBSelect ($prefix."CountryId style='width:280px'", $Record[$prefix."CountryId"], "SELECT Id, Description AS Value FROM Country WHERE Active=1 ORDER BY Description")) ;  
    print htmlItemSpace() ;
    printf ("<tr><td><p>Phone</p></td><td><input type=text name=PhoneMain size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneMain"])) ;
    printf ("<tr><td><p>Fax</p></td><td><input type=text name=PhoneFax size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneFax"])) ;
    printf ("<tr><td><p>Mail</p></td><td><input type=text name=Mail size=40 value=\"%s\"></td></tr>\n", htmlentities($Record["Mail"])) ;
    printf ("<tr><td><p>Reg.Number</p></td><td><input type=text name=RegNumber size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["RegNumber"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Purchase Ref</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseRefId style="width:250px"', $Record['PurchaseRefId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 AND User.Extern=0 ORDER BY Value',$MyCompanyId))) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Delivery Term</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseDeliveryTermId style="width:200px"', $Record['PurchaseDeliveryTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Carrier</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseCarrierId style="width:250px"', $Record['PurchaseCarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Currency</td><td>%s</td></tr>\n", htmlDBSelect ('PurchaseCurrencyId style="width:150px"', $Record['PurchaseCurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Payment Term</td><td>%s</td></tr>\n", htmlDBSelect ('PurchasePaymentTermId style="width:200px"', $Record['PurchasePaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Agent</td><td><input type=text name=PurchaseAgent size=20 maxlength=20 value=\"%s\"></td></tr>\n", htmlentities($Record['PurchaseAgent'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Hide from list</p></td><td><input type=checkbox name=ListHidden %s></td></tr>\n", ($Record["ListHidden"])?"checked":"") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>CompanyFooter</td><td><textarea name=CompanyFooter style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['CompanyFooter']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    print htmlItemSpace() ;
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
