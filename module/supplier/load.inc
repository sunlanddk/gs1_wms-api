<?php
//
    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Company.Number'),
		array ('name' => 'Name',		'db' => 'Company.Name'),
		array ('name' => 'City',		'db' => 'Company.City'),
		array ('name' => 'Country',		'db' => 'Country.Description'),
//		array ('name' => 'Customer',		'db' => 'Company.TypeCustomer'),
//		array ('name' => 'Supplier',		'db' => 'Company.TypeSupplier'),
//		array ('name' => 'Carrier',		'db' => 'Company.TypeCarrier')
	    ) ;
	    $queryFields = 'Company.*, CONCAT(Country.Description," (",Country.Name,")") AS CountryName' ;
	    $queryTables = 'Company LEFT JOIN Country ON Country.Id=Company.CountryId' ;
	    $queryClause = 'Company.TypeSupplier=1 AND Company.Active=1 AND not Company.Id=1' ;	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'companyview') ;

	case 'basic' :
	case 'basic-cmd':
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through
	case 'copy' :
	case 'copy-cmd' :		
	case 'view' :
	case 'delete-cmd' :
	case 'address' :
	case 'address-cmd' :
	case 'salesinfo' :
	case 'salesinfo-cmd' :	
	    // Get Company
	    $query = sprintf ("SELECT * FROM Company WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

//	    if (stripos($User['Title'],'multi')===false)
//		navigationPasive ('companycopy') ;
//	    else
//		navigationEnable ('companycopy') ;
			
	    // Navigation
	    if ($Record['TypeCustomer']) {
		navigationEnable ('typecustomer') ;
	    }
	    return 0 ;
    }
 
    return 0 ;
    
?>
