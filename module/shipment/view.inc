<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemField ($field, 'No') ;
	}
    }

    itemStart () ;
    itemHeader () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Description', $Record['Description']) ;

    itemSpace () ;
//    itemFieldIcon ('Company', empty($Record['AltCompanyName'])?$Record['CompanyName']:$Record['AltCompanyName'].' ('.$Record['CompanyName'].')', 'company.gif', 'companyview', $Record['CompanyId']) ;
//    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['CompanyId']) ;

    itemSpace () ;
    itemField ('State', $Record['State']) ;

    itemSpace () ;
    itemField ('Departure', date ('Y-m-d', dbDateDecode($Record['DepartureDate']))) ;
	
	itemSpace () ;
    itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['FromCompanyId']), 'company.gif', 'companyview', $Record['FromCompanyId']) ;
	if ($Record['Type']=='shipment')
		itemFieldIcon ('PickUp', tableGetField ('Company', 'Company.Name', (int)$Record['PickUpCompanyId']), 'company.gif', 'companyview', $Record['PickCompanyId']) ;

    itemSpace () ;
    itemField ('DeliveryTerm', $Record['DeliveryTermName']) ;

    itemSpace () ;
    itemField ('Carrier', $Record['CarrierName']) ;
    
    itemSpace () ;
    itemField ('Delivery', $Record['CompanyName']) ;  
    itemField ('', $Record['Address1']) ;  
    itemField ('', $Record['Address2']) ;  
    itemField ('', $Record['ZIP'] . ' ' . $Record['City']) ;  
    itemField ('', $Record['CountryName']) ;  

    itemSpace () ;
    flag ($Record, 'Ready') ;
    flag ($Record, 'Departed') ; 
    flag ($Record, 'Invoiced') ; 
    flag ($Record, 'Done') ; 

    itemInfo ($Record) ;
    itemEnd () ;

    return 0 ;
?>
