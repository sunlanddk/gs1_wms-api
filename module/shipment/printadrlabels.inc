<?php 
require_once 'lib/http.inc' ;
require_once('module/shipping/sscc.inc');

$query = sprintf ('
	SELECT c.Id as containerId, 
	c.Sscc, 
	i.Id as itemId, 
	SUM(i.Quantity*v.Weight/1000) AS ContainerWeight,
	a.Description, 
	v.VariantCode, 
	s.Id as StockId,
	s.Type,
	s.CompanyName,
	s.Address1,
	s.Address2,
	s.ZIP,
	s.City,
    s.CompanyId,s.FromCompanyId,
	s.Description as StockDescription,
	s.DepartureDate,
	co.Description as CountryName,
	c.Volume,
	c.GrossWeight
	FROM (container c, stock s)
	LEFT JOIN item i ON i.ContainerId=c.Id AND i.active=1
	LEFT JOIN article a ON a.Id=i.ArticleId
	LEFT JOIN variantcode v ON v.id=i.VariantCodeId
	LEFT JOIN country co ON co.Id=s.CountryId
	WHERE 
	c.StockId=%d AND c.active=1 
	 AND s.Id=%d GROUP by c.id', 
	$Id, 
	$Id
) ;
$res = dbQuery ($query) ;

$containers = array('containers' => array());

while ($row = dbFetch ($res)) {
	$query = sprintf ('SELECT *  FROM (variant_ai ai, ai a) WHERE ai.ItemId=%d AND a.Id=ai.AiId', (int)$row['itemId']) ;
	$ressult = dbQuery ($query) ;
	$ais = array();
	$ais['02'] = $row['VariantCode'];
	while ($ai = dbFetch ($ressult)) {
		$ais[$ai['Name']] = $ai['Value'];
		// $ais['10'] = 'XSBFSHJ1133';
		// $ais['15'] = '201005';
		// $ais['37'] = '300';
	}

	$containers['type'] = $row['Type'];

	$query = sprintf ('
		SELECT 
		c.Name,
		c.Address1,
		c.Address2,
		c.ZIP,
		c.City,
		co.Description as CountryName
		FROM (company c) 
		LEFT JOIN country co ON co.Id=c.Id
		WHERE
		c.Id=%d', 
        $row['FromCompanyId']
	) ;

	$ress = dbQuery ($query) ;
	$company = dbFetch ($ress);
	dbQueryFree($ress) ;

	$query = sprintf ('
		SELECT 
		c.Name,
		c.Address1,
		c.Address2,
		c.ZIP,
		c.City,
		co.Description as CountryName
		FROM (company c) 
		LEFT JOIN country co ON co.Id=c.Id
		WHERE
		c.Id=%d', 
		787
	) ;

	$ress = dbQuery ($query) ;
	$owner = dbFetch ($ress);
	dbQueryFree($ress) ;

	// From
	$containers['from']['name'] = $company['Name'];
	$containers['from']['street'] = $company['Address1'];
	$containers['from']['street2'] = $company['Address2'];
	$containers['from']['zip'] = $company['ZIP'];
	$containers['from']['city'] = $company['City'];
	$containers['from']['country'] = $company['CountryName'];

	// Owner
	$containers['owner']['name'] = $owner['Name'];
	$containers['owner']['street'] = $owner['Address1'];
	$containers['owner']['street2'] = $owner['Address2'];
	$containers['owner']['zip'] = $owner['ZIP'];
	$containers['owner']['city'] = $owner['City'];
	$containers['owner']['country'] = $owner['CountryName'];

	// To
	$containers['to']['name'] = $row['CompanyName'];
	$containers['to']['street'] = $row['Address1'];
	$containers['to']['street2'] = $row['Address2'];
	$containers['to']['zip'] = $row['ZIP'];
	$containers['to']['city'] = $row['City'];
	$containers['to']['country'] = $row['CountryName'];

	$container = array(
		'id' => $row['containerId'],
		'stockid' => $row['StockId'],
		'sscc' => $row['Sscc'],
		'ai' => $ais,
		'name' => $row['Description'],
		'reference' => $row['StockDescription'],
		'date' => $row['DepartureDate'],
		'brutto' => $row['ContainerWeight'],
		'volume' => $row['Volume'],
	);
	array_push($containers['containers'], $container);
}


$pdf = createAdrLabel($containers, $Id);

httpNoCache ('pdf') ;
httpContent ('application/pdf', sprintf('sscclabels.pdf', 1), strlen($pdf)) ;
print ($pdf) ; 



