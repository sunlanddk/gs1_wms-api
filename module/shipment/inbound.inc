<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;

			// Default values
			$Record['DepartureDate'] = dbDateEncode(time()) ;
			$Record['FromCompanyId'] = $User['CustomerCompanyId'] ;
			$Record['CompanyId'] = $User['CustomerCompanyId'] ;
			$Record['Name'] = 'Inbound w/o labels ' . date('Y-m-d h:m', time()) ;
			break ;
    
		default :
			itemStart () ;
			itemSpace () ;
			itemField ('Company', $Record['CompanyName']) ;
			itemSpace () ;
			itemEnd () ;
			break ;
    }

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;


    switch ($Navigation['Parameters']) {
	case 'new' :
	    itemFieldRaw ('Company', formDBSelect ('CompanyId', (int)$Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name', 'width:300px;')) ;  
	    itemSpace () ;
	    break ;
    }

    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 37)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%')) ;

    itemSpace () ;
//    if (tableGetFieldWhere('Company', 'Id', sprintf('TypeSupplier=1 And Internal=1 AND Active=1 And Id=%d', $Record['FromCompanyId']))==0 and $Record['FromCompanyId']>0)
//	 itemFieldRaw ('Owner Company', formText ('FromCompanyId', (int)$Record['FromCompanyId'], 8,'','disabled') . tableGetFieldWhere('Company', 'Name', sprintf ('Id=%d', (int)$Record['FromCompanyId']))) ;  
//    else
	 itemFieldRaw ('Owner Company', formDBSelect ('FromCompanyId', (int)$Record['FromCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE (Company.TypeCustomer or Company.Internal=1) AND Company.Active=1 ORDER BY Value'), 'width:300px;')) ;  

    itemSpace () ;
	itemFieldRaw ('VariantCode', formDBSelect ('VariantCodeId', (int)$Record['VariantCodeId'], sprintf('SELECT VariantCode.Id, concat(VariantCode.VariantCode," - ", VariantCode.VariantDescription) AS Value 
																										FROM (VariantCode, Article) 
																										WHERE VariantCode.Active=1 and Article.Id=VariantCode.ArticleId and Article.SsccLabelCreation=1 AND VariantCode.OwnerCompanyId=%d
																										ORDER BY Value', $User['CustomerCompanyId']), 'width:300px;')) ;  
    itemSpace () ;
	itemFieldRaw ('Batch', formText ('Batch', $Record['Batch'], 45, 'text-align:right;') . ' ') ;
    itemSpace () ;
	itemFieldRaw ('Quantity', formText ('Quantity', (int)$Record['Quantity'], 3, 'text-align:right;') . ' (Number of Containers without SSCC to be unloaded/received)') ;

    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
