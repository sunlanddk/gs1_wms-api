<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	    if ($Record[$field]) {
	        itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	    } else {
	        itemFieldRaw ($field, formCheckbox ($field, 1) . 'Close shipment') ;
	    }
    }

    formStart () ;   
    
    itemStart() ;
    itemSpace () ;
//    flag ($Record, 'Ready') ;
//    flag ($Record, 'Departed') ;
    flag ($Record, 'Done') ;
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Order(s) Completely done', formCheckbox ('CompletelyDone', 0)  . 'Closes all orderlines in all orders on this shipment') ;
    itemSpace () ;
    itemEnd() ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
 
    listHead ('Orderline', 50)  ;
    listHead ('OrderQty', 45, 'align=right') ;
    listHead ('TotalShipped', 50) ;
    listHead ('This Qty', 45) ;
    listHead ('Remaining', 45) ;
    listHead ('Unit', 20) ;
    listHead ('Done', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result)) and $row['OrderNo']) {
    	listRow () ;
    	listField (sprintf('O%d.%d',$row['OrderNo'],$row['OrderLineNo'])) ;
      listField(number_format ((float)$row['OQty'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
      listField(number_format ((float)$row['TotalShipQty'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
      listField(number_format ((float)$row['ShipQty'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
      listField(number_format (((float)$row['OQty']-(float)$row['TotalShipQty']), (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
      
    	listField (sprintf('%s',$row['UnitName'])) ;
      
    	listFieldRaw (formCheckbox (sprintf('OrderLineId[%d]', (int)$row['OrderLineId']), 1)) ;
    }
    if (dbNumRows($Result) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Orderlines', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;


    formEnd () ;

    return 0 ;
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
