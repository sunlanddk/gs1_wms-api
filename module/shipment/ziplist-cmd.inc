<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

class zipfile
{
var $datasec = array();
var $ctrl_dir = array();
var $eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00";
var $old_offset = 0; 

function add_dir($name) {
$name = str_replace("", "/", $name); 

$fr = "\x50\x4b\x03\x04";
$fr .= "\x0a\x00";
$fr .= "\x00\x00";
$fr .= "\x00\x00";
$fr .= "\x00\x00\x00\x00"; 

$fr .= pack("V",0);
$fr .= pack("V",0);
$fr .= pack("V",0);
$fr .= pack("v", strlen($name) );
$fr .= pack("v", 0 );
$fr .= $name;
$fr .= pack("V", 0);
$fr .= pack("V", 0);
$fr .= pack("V", 0); 

$this -> datasec[] = $fr;
$new_offset = strlen(implode('', $this->datasec)); 

$cdrec = "\x50\x4b\x01\x02";
$cdrec .= "\x00\x00";
$cdrec .= "\x0a\x00";
$cdrec .= "\x00\x00";
$cdrec .= "\x00\x00";
$cdrec .= "\x00\x00\x00\x00";
$cdrec .= pack("V",0);
$cdrec .= pack("V",0);
$cdrec .= pack("V",0);
$cdrec .= pack("v", strlen($name) );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$ext = "\x00\x00\x10\x00";
$ext = "\xff\xff\xff\xff";
$cdrec .= pack("V", 16 );
$cdrec .= pack("V", $this -> old_offset );
$cdrec .= $name; 

$this -> ctrl_dir[] = $cdrec;
$this -> old_offset = $new_offset;
return;
} 

function add_file($data, $name) {
$fp = fopen($data,"r");
$data = fread($fp,filesize($data));
fclose($fp);
$name = str_replace("", "/", $name);
$unc_len = strlen($data);
$crc = crc32($data);
$zdata = gzcompress($data);
$zdata = substr ($zdata, 2, -4);
$c_len = strlen($zdata);
$fr = "\x50\x4b\x03\x04";
$fr .= "\x14\x00";
$fr .= "\x00\x00";
$fr .= "\x08\x00";
$fr .= "\x00\x00\x00\x00" ;

$fr .= pack("V",$crc) ;
$fr .= pack("V",$c_len);
$fr .= pack("V",$unc_len);
$fr .= pack("v", strlen($name) );
$fr .= pack("v", 0 );
$fr .= $name;
$fr .= $zdata;
$fr .= pack("V",$crc);
$fr .= pack("V",$c_len);
$fr .= pack("V",$unc_len); 

$this -> datasec[] = $fr;
$new_offset = strlen(implode("", $this->datasec)); 

$cdrec = "\x50\x4b\x01\x02";
$cdrec .="\x00\x00";
$cdrec .="\x14\x00";
$cdrec .="\x00\x00";
$cdrec .="\x08\x00";
$cdrec .="\x00\x00\x00\x00";
$cdrec .= pack("V",$crc);
$cdrec .= pack("V",$c_len);
$cdrec .= pack("V",$unc_len);
$cdrec .= pack("v", strlen($name) );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$cdrec .= pack("v", 0 );
$cdrec .= pack("V", 32 );
$cdrec .= pack("V", $this -> old_offset ); 

$this -> old_offset = $new_offset; 

$cdrec .= $name;
$this -> ctrl_dir[] = $cdrec;
} 

function file() {
$data = implode("", $this -> datasec);
$ctrldir = implode("", $this -> ctrl_dir); 

return 
$data .
$ctrldir .
$this -> eof_ctrl_dir .
pack("v", sizeof($this -> ctrl_dir)) .
pack("v", sizeof($this -> ctrl_dir)) .
pack("V", strlen($ctrldir)) .
pack("V", strlen($data)) . '\x00\x00' ;
}
}

	if (!is_array($_POST['Depart'])) return 'please select shipments to ZIP' ;

	// Initialize ZIP
	$zipTest = new zipfile();

	foreach ($_POST['Depart'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;

	    $query = sprintf ("SELECT * FROM Stock WHERE Active=1 AND Id=%d", $id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		// add to ZIP
		$file = fileName ('packinglist', $id) ;
		if (is_file($file))
			$zipTest->add_file($file, sprintf('%s.pdf', $Record['Name']));
	}
    // return zip to browser.
    Header("Content-type: application/octet-stream");
	Header ("Content-disposition: attachment; filename=zipTest.zip");
	echo $zipTest->file();

    return 0 ;
?>
