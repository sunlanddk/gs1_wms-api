<?php 
require_once 'lib/http.inc' ;
require_once('module/shipping/sscc.inc');

$query = sprintf ('
	SELECT c.Id as containerId, 
	c.Sscc, 
	i.Id as itemId, 
	a.Description, 
	v.VariantCode, 
	s.Type,
	s.CompanyName,
	s.Address1,
	s.Address2,
	s.ZIP,
	s.City,
	s.CompanyId,
	co.Description as CountryName
	FROM (container c, item i, article a, variantcode v, stock s, country co) 
	WHERE 
	c.StockId=%d AND 
	i.ContainerId=c.Id AND 
	a.Id=i.ArticleId AND 
	i.VariantCodeId=v.Id AND 
	co.Id=s.CountryId AND
	s.Id=%d', 
	$Id, 
	$Id
) ;
$res = dbQuery ($query) ;

$containers = array('containers' => array());

while ($row = dbFetch ($res)) {
	$query = sprintf ('SELECT *  FROM (variant_ai ai, ai a) WHERE ai.ItemId=%d AND a.Id=ai.AiId', $row['itemId']) ;
	$ressult = dbQuery ($query) ;
	$ais = array();
	$ais['02'] = $row['VariantCode'];
	while ($ai = dbFetch ($ressult)) {
		$ais[$ai['Name']] = $ai['Value'];
		// $ais['10'] = 'XSBFSHJ1133';
		// $ais['15'] = '201005';
		// $ais['37'] = '300';
	}

	$containers['type'] = $row['Type'];

	$query = sprintf ('
		SELECT 
		c.Name,
		c.Address1,
		c.Address2,
		c.ZIP,
		c.City,
		co.Description as CountryName
		FROM (company c, country co) 
		WHERE
		co.Id=c.CountryId AND
		c.Id=%d', 
		$row['CompanyId']
	) ;

	$ress = dbQuery ($query) ;
	$company = dbFetch ($ress);

	// From
	$containers['from']['name'] = $company['Name'];
	$containers['from']['street'] = $company['Address1'];
	$containers['from']['street2'] = $company['Address2'];
	$containers['from']['zip'] = $company['ZIp'];
	$containers['from']['city'] = $company['City'];
	$containers['from']['country'] = $company['CountryName'];

	// To
	$containers['to']['name'] = $row['CompanyName'];
	$containers['to']['street'] = $row['Address1'];
	$containers['to']['street2'] = $row['Address2'];
	$containers['to']['zip'] = $row['ZIp'];
	$containers['to']['city'] = $row['City'];
	$containers['to']['country'] = $row['CountryName'];

	$container = array(
		'id' => $row['containerId'],
		'sscc' => $row['Sscc'],
		'ai' => $ais,
		'name' => $row['Description'],
	);
	array_push($containers['containers'], $container);
}


$pdf = createSsccLabelAndAi($containers, $Id);

httpNoCache ('pdf') ;
httpContent ('application/pdf', sprintf('sscclabels.pdf', 1), strlen($pdf)) ;
print ($pdf) ; 



