<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,				'check' => true),
	'Description'		=> array (						'check' => true),

	'DepartureDate'		=> array ('type' => 'date',	'mandatory' => true,	'check'	=> true),
    
	'CompanyId'		=> array ('type' => 'integer',				'check' => true),
	'FromCompanyId'		=> array ('type' => 'integer',				'check' => false),
	
	'Type'			=> array ('type' => 'set'),

	'DeliveryComment'		=> array (),

	'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'CompanyName'	=> array (						'check' => true),
	'Address1'		=> array (						'check' => true),
	'Address2'		=> array (						'check' => true),
	'ZIP'			=> array (						'check' => true),
	'City'			=> array (						'check' => true),
	'CountryId'		=> array ('type' => 'integer',				'check' => true),
	
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),

	'Invoiced'		=> array ('type' => 'checkbox',	'check' => true),
	'InvoicedUserId'	=> array ('type' => 'set'),
	'InvoicedDate'		=> array ('type' => 'set'),

	'Departed'		=> array ('type' => 'checkbox',	'check' => true),
	'DepartedUserId'	=> array ('type' => 'set'),
	'DepartedDate'		=> array ('type' => 'set'),

	'Done'			=> array ('type' => 'set'),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set')
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Any Containers assigned to transport
		$query = sprintf ("SELECT Id FROM Container WHERE StockId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
//		if ($count == 0) return 'the Shipment can not be set Ready when it has no Containers' ;

		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Invoiced':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Ready']) return 'the Shipment can not be Invoiced before it has been set Ready' ;	

		// Set tracking information
		$fields['InvoicedUserId']['value'] = $User['Id'] ;
		$fields['InvoicedDate']['value'] = dbDateEncode(time()) ;

		// Auto Done when Departed and Invoiced
		if ($Record['Departed']) {
		    $fields['Done']['value'] = 1 ;
		    $fields['DoneUserId']['value'] = $User['Id'] ;
		    $fields['DoneDate']['value'] = dbDateEncode(time()) ;
		}
		return true ;

	    case 'Departed':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Ready']) return 'the Shipment can not Depart before it has been set Ready' ;	

		// Set tracking information
		$fields['DepartedUserId']['value'] = $User['Id'] ;
		$fields['DepartedDate']['value'] = dbDateEncode(time()) ;

		// Auto Done when Departed and Invoiced
//		if ($Record['Invoiced']) {
		    $fields['Done']['value'] = 1 ;
		    $fields['DoneUserId']['value'] = $User['Id'] ;
		    $fields['DoneDate']['value'] = dbDateEncode(time()) ;
//		}
		dbQuery('update pickorder set packed=1 where toid='.$Id) ;
		return true ;

	    case 'CompanyId' :
		if ($value == 0) return 'please select Company' ;
		if (!$changed) return false ;
		
		// Get company
		$query = sprintf ('SELECT * FROM Company WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] == 0) return sprintf ('%s(%d) Company not found, id %d', __FILE__, __LINE__, $value) ;

		if ($Id <= 0) {
		    // Get default values for new Shipment
		    $fields['DeliveryTermId'] = array ('type' => 'set', 'value' => (int)$row['DeliveryTermId']) ;
		    $fields['CarrierId'] = array ('type' => 'set', 'value' => (int)$row['CarrierId']) ;

		    if ((int)$row['DeliveryCountryId'] > 0) {
			// Get delivery address
			$fields['Address1'] = array ('type' => 'set', 'value' => $row['DeliveryAddress1']) ;
			$fields['Address2'] = array ('type' => 'set', 'value' => $row['DeliveryAddress2']) ;
			$fields['ZIP'] = array ('type' => 'set', 'value' => $row['DeliveryZIP']) ;
			$fields['City'] = array ('type' => 'set', 'value' => $row['DeliveryCity']) ;
			$fields['CountryId'] = array ('type' => 'set', 'value' => (int)$row['DeliveryCountryId']) ;
			$fields['DeliveryComment'] = array ('type' => 'set', 'value' => '') ;
		    } else {
			// Get company address
			$fields['Address1'] = array ('type' => 'set', 'value' => $row['Address1']) ;
			$fields['Address2'] = array ('type' => 'set', 'value' => $row['Address2']) ;
			$fields['ZIP'] = array ('type' => 'set', 'value' => $row['ZIP']) ;
			$fields['City'] = array ('type' => 'set', 'value' => $row['City']) ;
			$fields['CountryId'] = array ('type' => 'set', 'value' => (int)$row['CountryId']) ;
		    }
		}

		break ;
	}

	if (!$changed) return false ;
	if ($Record['Ready']) return sprintf ('the Shipment can not be modified when Ready, field %s', $fieldname) ;
	return true ;	
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Invoiced']) ;
	    unset ($fields['Done']) ;
	    $fields['Type']['value'] = 'shipment' ;
		$fields['DeliveryComment'] = array ('type' => 'set', 'value' => '!') ;
	    $Id = -1 ;
	    break ;

	case 'neworder' :
	    // Shipment created from Order
	    // Validate state of shipment
	    if ($Record['Done']) return 'no new Shipment for a Done Order' ;
	    if (!$Record['Ready']) return 'the Order has to be Ready before making Shipment' ;
	    unset ($Record['Ready']) ;
	    unset ($Record['Done']) ;
	    
	    // Unused fields from form
	    unset ($fields['Ready']) ;
	    unset ($fields['Invoiced']) ;
	    unset ($fields['Done']) ;

	    $fields['CompanyId'] = array ('type' => 'set', 'value' => (int)$Record['CompanyId']) ;
	    $fields['FromCompanyId'] = array ('type' => 'set', 'value' => (int)$Record['ToCompanyId']) ;
	    $fields['Type']['value'] = 'shipment' ;
	    $Id = -1 ;
	    break ;

	default :
	    //if ($Record['Done']) return 'the Shipment can not be modified when Done' ;
	    unset ($fields['CompanyId']) ;
	    break ;
    }
     
    $res = saveFields ('Stock', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Shipment
	    return navigationCommandMark ('shipmentview', (int)$Record['Id']) ;
    }

    return 0 ;    
?>
