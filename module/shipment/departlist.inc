<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
	global $User, $Navigation, $Record, $Size, $Id ;

    if (dbNumRows($Result) == 0) return "No shipments to select" ;

    formStart () ;

    // Header fields
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Transport', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Type="Transport" And Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', 0), 'width:250px;')) ;
    itemFieldRaw ('Set departed', formCheckbox ('Departed', 0)) ;
    itemFieldRaw ('Copy Items', formCheckbox ('Copy', 0)) ;
    itemSpace () ;
    itemEnd () ;
    
    // List
    listClear () ;
    listStart () ;
    listRow () ;
//    listHead ('', 23) ;
    listHead ('Name', 140) ;
    listHead ('Description') ;
    if (!$HideCompany) {
	listHead ('Company') ;
    }
    listHead ('DeliveryTerms', 90) ;
    listHead ('Carrier', 80) ;
    listHead ('State', 55) ;
//    listHead ('Invoice', 40) ;

    while ($row = dbFetch($Result)) {
		if (($row['State'] == 'Ready') or ($row['State'] == 'Invoiced')) {
			listRow () ;
	//		listFieldIcon ($Navigation['Icon'], 'shipmentview', (int)$row['Id']) ;
			listField ($row['Name']) ;
			listField ($row['Description']) ;
			if (!$HideCompany) {
				listField ($row['CompanyName']) ;
			}
			listField ($row['DeliveryTermName']) ;
			listField ($row['CarrierName']) ;
			listField ($row['State']) ;
//			listField ($row['InvoiceMade']) ;
	    		ListFieldRaw (formCheckbox (sprintf('Depart[%d]', (int)$row['Id']), 0)) ;
		}
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;

?>
