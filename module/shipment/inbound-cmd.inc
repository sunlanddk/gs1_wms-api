<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;

    $fields = array (
		'Name'			=> array ('mandatory' => true,		'check' => true),
		'Description'		=> array (						'check' => true),

		'DepartureDate'		=> array ('type' => 'date'),
		'ArrivalDate'		=> array ('type' => 'set'),
		
		'CompanyId'			=> array ('type' => 'integer',				'check' => true),
		'FromCompanyId'		=> array ('type' => 'integer',				'check' => false),
		
		'Type'				=> array ('type' => 'set'),

		'DeliveryComment'	=> array (),

//		'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

//		'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

		'CompanyName'	=> array (						'check' => true),
		'Address1'		=> array (						'check' => true),
		'Address2'		=> array (						'check' => true),
		'ZIP'			=> array (						'check' => true),
		'City'			=> array (						'check' => true),
		'CountryId'		=> array ('type' => 'integer',				'check' => true),
		
		'Ready'			=> array ('type' => 'checkbox',	'check' => false),
		'ReadyUserId'		=> array ('type' => 'set'),
		'ReadyDate'		=> array ('type' => 'set'),

		'Invoiced'		=> array ('type' => 'checkbox',	'check' => false),
		'InvoicedUserId'	=> array ('type' => 'set'),
		'InvoicedDate'		=> array ('type' => 'set'),

		'Departed'		=> array ('type' => 'checkbox',	'check' => false),
		'DepartedUserId'	=> array ('type' => 'set'),
		'DepartedDate'		=> array ('type' => 'set'),

		'Done'			=> array ('type' => 'set'),
		'DoneUserId'		=> array ('type' => 'set'),
		'DoneDate' 		=> array ('type' => 'set')
   ) ;


    function GetCheckDigitSscc($barcode){
		//Compute the check digit
		$sum=0;
		$barcodeArray = str_split($barcode);
		$i = 0;
		foreach ($barcodeArray as $key => $value) {
			$i ++;
			# code...
			if($i%2 == 0){
				$sum += ( 1 * $value);
				continue;
			}

			$sum += ( 3 * $value);
			continue;
		}
		$r=$sum%10;
		if($r>0)
			$r=10-$r;
		return $r;
	}

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'CompanyId' :
				if ($value == 0) return 'please select Company' ;
				if (!$changed) return false ;
				
				// Get company
				$query = sprintf ('SELECT * FROM Company WHERE Id=%d AND Active=1', $value) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
				dbQueryFree ($result) ;
				if ((int)$row['Id'] == 0) return sprintf ('%s(%d) Company not found, id %d', __FILE__, __LINE__, $value) ;

				if ($Id <= 0) {
					// Get default values for new Shipment
					$fields['DeliveryTermId'] = array ('type' => 'set', 'value' => (int)$row['DeliveryTermId']) ;
					$fields['CarrierId'] = array ('type' => 'set', 'value' => (int)$row['CarrierId']) ;

					if ((int)$row['DeliveryCountryId'] > 0) {
					// Get delivery address
					$fields['Address1'] = array ('type' => 'set', 'value' => $row['DeliveryAddress1']) ;
					$fields['Address2'] = array ('type' => 'set', 'value' => $row['DeliveryAddress2']) ;
					$fields['ZIP'] = array ('type' => 'set', 'value' => $row['DeliveryZIP']) ;
					$fields['City'] = array ('type' => 'set', 'value' => $row['DeliveryCity']) ;
					$fields['CountryId'] = array ('type' => 'set', 'value' => (int)$row['DeliveryCountryId']) ;
					$fields['DeliveryComment'] = array ('type' => 'set', 'value' => '') ;
					} else {
					// Get company address
					$fields['Address1'] = array ('type' => 'set', 'value' => $row['Address1']) ;
					$fields['Address2'] = array ('type' => 'set', 'value' => $row['Address2']) ;
					$fields['ZIP'] = array ('type' => 'set', 'value' => $row['ZIP']) ;
					$fields['City'] = array ('type' => 'set', 'value' => $row['City']) ;
					$fields['CountryId'] = array ('type' => 'set', 'value' => (int)$row['CountryId']) ;
					}
				}
				break ;
			default:
				break ;
		}

		if (!$changed) return false ;
		if ($Record['Ready']) return sprintf ('the Shipment can not be modified when Ready, field %s', $fieldname) ;
		return true ;	
    }
	
    $qty = (int)$_POST['Quantity'] ;
	if ($qty==0)
		return 'Please enter quantity (minimum 1) before pressing save' ;

	$variantcodeid = (int)$_POST['VariantCodeId'] ;
	if ($variantcodeid==0)
		return 'Please select variantcode before pressing save' ;
	
    switch ($Navigation['Parameters']) {
		case 'new' :
			$fields['Type']['value'] = 'Inbound' ;
			$fields['Ready'] = array ('type' => 'set', 'value' => 1) ;
//			$fields['Invoiced'] = array ('type' => 'set', 'value' => 1) ;
//			$fields['Departed'] = array ('type' => 'set', 'value' => 1) ;
			$fields['Arrived'] = array ('type' => 'set', 'value' => 1) ;
//			$fields['Done'] = array ('type' => 'set', 'value' => 1) ;
			$fields['DeliveryComment'] = array ('type' => 'set', 'value' => '!') ;
			$fields['DepartureDate'] = array ('type' => 'set', 'value' => date('Y-m-d',time())) ;
			$fields['ArrivalDate']['value'] = date('Y-m-d',time()) ;
			$Id = -1 ;
			break ;

		default :
			break ;
    }
     
    $res = saveFields ('Stock', $Id, $fields, true) ;
    if ($res) return $res ;
	
	$res = dbQuery('Select * from containertype where id=33') ;
	$containertype = dbFetch($res) ;
	dbQueryFree($res) ;
	
	$res = dbQuery('Select * from variantcode where id='.$variantcodeid) ;
	$variantcode = dbFetch($res) ;
	dbQueryFree($res) ;

	$i = 0 ;
	While ($i<$qty) {
		$container = array ('StockId' 			=> $Record['Id'],
							'ContainerTypeId'	=> $containertype['Id'],
							'Description'		=> 'Autogenerated SSCC label - variant ' . $variantcode['VariantCode'], 
							'InboundStockId' 	=> $Record['Id'],
							'GrossWeight'		=> $containertype['TaraWeight'],
							'TaraWeight'		=> $containertype['TaraWeight'],
							'Volume'			=> $containertype['Volume'],
						) ;
		$containerid = tableWrite('Container', $container) ;

		$totalprefix = parameterGet('ssccprefix');
		$containerNumberWZero = sprintf('%09d', $containerid);
		$checkDigit = GetCheckDigitSscc((string) $totalprefix.$containerNumberWZero);
		$ssccCode = (string) $totalprefix.$containerNumberWZero.$checkDigit;
		$container = array('Sscc' => $ssccCode) ;
		$containerid = tableWrite('Container', $container, $containerid) ;
		
		$item = array ('ContainerId' 		=> $containerid,
						'VariantCodeId'		=> $variantcode['Id'],
						'ArticleId'			=> $variantcode['ArticleId'],
						'BatchNumber'		=> $_POST['Batch'],
						'Quantity'			=> ((int)$variantcode['Quantity']>0) ? (int)$variantcode['Quantity'] : 1
					) ;
		$itemId = tableWrite('Item', $item) ;

		$variantai = array ('AiId'			=> 2,
							'ItemId'		=> $itemId,
							'Value'			=> $variantcode['VariantCode'],
					);
		tableWriteSimple('variant_ai', $variantai) ;		
		$variantai = array ('AiId'			=> 3,
							'ItemId'		=> $itemId,
							'Value'			=> (int)$item['Quantity'],
					);
		tableWriteSimple('variant_ai', $variantai) ;		
		$variantai = array ('AiId'			=> 6,
							'ItemId'		=> $itemId,
							'Value'			=> $_POST['Batch'],
					);
		tableWriteSimple('variant_ai', $variantai) ;		
		$variantai = array ('AiId'			=> 7,
							'ItemId'		=> $itemId,
							'Value'			=> dbDateEncode(time()),
					);
		tableWriteSimple('variant_ai', $variantai) ;		

		$i++ ;
	}

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Shipment
	    return navigationCommandMark ('shipmentcontainers', (int)$Record['Id']) ;
    }

    return 0 ;    
?>
