<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/save.inc' ;

    if (!is_array($_POST['Depart'])) return 'please select some shipments to be send with transport' ;

    // Get Fields
    $fields = array (
		'StockId'		=> array ('type' => 'integer'),
	'Departed'			=> array ('type' => 'checkbox'),
	'Copy'			=> array ('type' => 'checkbox')
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;

	if ($fields['StockId']['value'] == 0) return 'Please select transport to copy containers and items to' ;
	
	foreach ($_POST['Depart'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;

		// Update shipment
		if ($fields['Departed']['value'] == 0) {
			$shipment = array (
				'BatchId'		=> $fields['StockId']['value']
			) ;
			tableWrite('Stock', $shipment, $id) ;
		} else {
			$shipment = array (
				'Departed'			=> 1,
				'DepartedUserId'	=> $User['Id'],
				'DepartedDate'		=> dbDateEncode(time()),
				'BatchId'		=> $fields['StockId']['value']
			) ;
			$Invoiced = tableGetField ('Stock', 'Invoiced', $id) ;
			if ($Invoiced) {
				$shipment['Done'] =  1;
				$shipment['DoneUserId'] =  $User['Id'];
				$shipment['DoneDate'] =  dbDateEncode(time());
			}
			tableWrite('Stock', $shipment, $id) ;
		}

		// Copy containers and items?
		if ($fields['Copy']['value'] > 0) {
		    $query = sprintf ("select * from Container where active=1 and StockId=%d", $id) ;
		    $res = dbQuery ($query) ;
		    while ($row = dbFetch($res)) {
			    // Create new Container
			    $row['PreviousStockId'] = $row['StockId'] ;
			    $row['StockId'] = $fields['StockId']['value'] ;
			    $ContainerId = tableWrite ('Container', $row) ;
			
			    // Copy items in container to new Container
			    $query = sprintf ("select * from Item where active=1 and ContainerId=%d", $row['Id']) ;
			    $result = dbQuery ($query) ;
			    while ($item_row = dbFetch($result)) {
					$item_row['PreviousId'] = $item_row['Id'] ;
					$item_row['PreviousContainerId'] = $item_row['ContainerId'] ;
					$item_row['OrderLineId'] = 0 ;
					$item_row['ContainerId'] = $ContainerId ;
					$ItemId = tableWrite ('Item', $item_row) ;
				}
				dbQueryFree ($result) ;

				// reset FromProductionId to avoid items to counted twice on production orders.
				$query = sprintf ("update Item set FromProductionId=0 where active=1 and ContainerId=%d", $row['Id']) ;
				$result = dbQuery ($query) ;
				dbQueryFree ($result) ;
		    }
		    dbQueryFree ($res) ;
	      }
	}
    return navigationCommandMark ('transportview', $fields['StockId']['value']) ;

    return 0 ;
?>
