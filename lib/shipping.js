
const url = weburl; 
const appinbound = new Vue({
	

	el: "#appinbound",
	data: {
		items: [],
		tempitems: [],
		str: '',
		onlyOneDataBarcode: false,
		url: url
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		vm.str = '';
	        		console.log('Send data to server');
	        		vm.createInbound();
	        		return;
	        	}
	        	if(vm.str.substring(0,2) == '02'){

	        	}
	            if(vm.str.substring(0,2) == '00'){
	            	// if(vm.tempitems.length > 0){
	            		var found = true;
	            		vm.items.forEach(function(el, i) {
	            			if(el.sscc == vm.str){
	            				vm.items[i].items = vm.tempitems;
	            				found = false;
	            			}
	            		});
	            		if(found == true){
		            		vm.items.push({'sscc':vm.str, 'items': vm.tempitems});
		            	}
		            	vm.tempitems = [];
		            	vm.str = '';
		            // }
	            	return;
	            }
	            if(vm.onlyOneDataBarcode == true){
	            	vm.tempitems = [];
	        	}
	        	let notfound = true;
	        	vm.tempitems.forEach(function(el){
	        		if(el == vm.str){
	        			notfound = false;	
	        		}
	        	});
	        	if(notfound == true){
	            	vm.tempitems.push(vm.str);
	            }
	            vm.str = '';
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		resetAll(){
			this.items = [];
			this.tempitems = [];
		},
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.tempitems.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    createInbound(){
	    	var vm = this;
	    	if(vm.items.length > 0){
	    		let ownercompanyid = $('.ownercompanyid').val();
	    		vm.postData(url+'shipping/inbound', {items: vm.items, ownercompanyid: ownercompanyid}, vm.createInboundResponse);
	    	}
	    	else{
	    		alert('There are no scanned items yet.');
	    	}
	    },
	    createInboundResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		vm.items = [];
	    		alert('Inbound shipment has been created, and are ready to be positioned.');
	    	}
	    	else{
	    		alert('An error occured, please try again. Error code: '+response.errorcode);
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
	 		if(data.items.length > 0){
				$.ajax({
				    url : url,
				    type: "POST",
				    data : JSON.stringify(data),
				    success: function(data, textStatus, jqXHR)
				    {
				    	callback(data);
				    },
				    error: function (jqXHR, textStatus, errorThrown)
				    {
				    	console.log('Error: ');
				 		console.log(jqXHR);
				 		console.log(textStatus);
				 		console.log(errorThrown);
				    }
				});
			}

			return;
		},
	    getData(url){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			     	return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});
