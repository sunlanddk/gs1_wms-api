
const url = weburl; 
const appinbound = new Vue({
	

	el: "#appinbound",
	data: {
		sscc: '',
		ais: [],
		newais: [],
		ailist: [],
		str: '',
		url: url
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str == ''){
	        		return;
	        	}
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		vm.str = '';
	        		console.log('Send data to server');
	        		// vm.createInbound();
	        		return;
	        	}

	            if(vm.str.substring(0,2) == '00'){
	            	vm.sscc = vm.str;
	            	vm.str = '';
	            	vm.ais = [];
	            	vm.newais = [];
	            	vm.getAiInfo();
	            	return;
	            }
	            
	        	let notfound = true;
	        	let allcodesinstr = vm.convertAiCodes(vm.str);

				allcodesinstr.forEach(function(e){
					notfound = true;
					vm.newais.forEach(function(el){
		        		if(el.ai == e.substring(0,2)){
		        			notfound = false;	
		        			el.value = e.substring(2, e.length);
		        		}
		        	});

		        	if(notfound == true){
		        		let tmpai = e.substring(0,2);
		        		let tmpvalue = e.substring(2, e.length);
		        		let tmdescription = '';
		        		vm.ailist.forEach(function(el){
		        			if(e.substring(0,2) == el.ai){
		        				tmdescription = el.description;
		        			}
		        		});

		            	vm.newais.push({'ai' : tmpai, 'value': tmpvalue, 'description': tmdescription});
		            }
				});        	

	            vm.str = '';
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
    	vm.sscc = $('.sscc').val();
    	vm.getAiInfo();
	},
	methods: {
		resetAll(){
			this.sscc = '';
			this.ais = [];
			this.newais = [];
		},
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.newais.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    getAiInfo(){
	    	var vm = this;
	    	var form = { sscc : vm.sscc };
	    	if(vm.sscc.length > 0){
	    		vm.postData(url+'shipping/get/sscc/ai/info', form, vm.getAiInfoResponse);
	    	}
	    	else{
	    		alert('No SSCC has been scanned');
	    	}
	    },
	    convertAiCodes(str){
	    	let vm = this;
	    	let codes = [];
	    	let pointer = 0;
	    	let donestr = str;
            while (donestr !== '') {                    
                firstAiStr = donestr.substring(0, 2);
                if(vm.getAiLength(firstAiStr) == 0){
                	codes.push(donestr);	
                	donestr = '';	
                }

                value = donestr.substring(0, (vm.getAiLength(firstAiStr) + 2) );
                codes.push(value);
                donestr = donestr.replace(donestr.substring(0, (vm.getAiLength(firstAiStr) + 2) ), '');
            }

            return codes;
	    },
	    getAiLength(ai){
	    	let vm = this;
	    	let ailength = 0;

	    	vm.ailist.forEach(function(el){
	    		if(ai == el.ai){
	    			ailength = parseInt(el.ailength);
	    		}
	    	});

	    	return ailength;
	    },
	    getAiInfoResponse(response){
	    	var vm = this;
	    	// console.log(response);
	    	if(response.error == false){
	    		vm.ais = [];
	    		vm.ais = response.data.ais;
	    		vm.ailist = response.data.ailist;

	    		// var ailist = response.data.ais;
	    		// if(ailist != false){
		    	// 	ailist.forEach(function(el, i) {

		    	// 	});
		    	// }
	    	}
	    	else{
	    		alert('An error occured, please try again. Error code: '+response.errorcode);
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
	 		if(Object.keys(data).length > 0){
				$.ajax({
				    url : url,
				    type: "POST",
				    data : JSON.stringify(data),
				    success: function(data, textStatus, jqXHR)
				    {
				    	callback(data);
				    },
				    error: function (jqXHR, textStatus, errorThrown)
				    {
				    	console.log('Error: ');
				 		console.log(jqXHR);
				 		console.log(textStatus);
				 		console.log(errorThrown);
				    }
				});
			}

			return;
		},
	    getData(url){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			     	return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});
