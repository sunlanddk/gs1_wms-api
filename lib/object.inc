<?php

    $Objects = array () ;
    $ObjectTypeFields = 'ObjectType.Mark AS ObjectTypeMark, ObjectType.Name AS ObjectTypeName, ObjectType.Icon, ObjectType.NavigationMark, ObjectType.ValidDone, ObjectType.ValidReady' ;

    function objTypeId ($mark) {
	$query = sprintf ("SELECT Id FROM ObjectType WHERE Mark='%s'", $mark) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	return $row["Id"] ;
    }   

    function objPath ($id, $level=0) {
	global $Objects, $Project, $ObjectTypeFields ;
	$id = (int)$id ;
	if ($level > 10) return "Error: too many levels" ;
	if ($id == 0) return "" ;

	if ($Object[$id]["Id"] != $id) {
	    $query = sprintf ("SELECT Object.*, %s FROM Object, ObjectType WHERE Object.Active=1 AND Object.ProjectId=%d AND Object.ObjectTypeId=ObjectType.Id AND Object.Id=%d", $ObjectTypeFields, $Project["Id"], $id) ;
	    $result = dbQuery ($query) ;  
	    $Objects[$id] = dbFetch($result) ;
	    dbQueryFree ($result) ;
	}
//	return objPath ($Objects[$id]["ParentId"], $level+1) . htmlentities($Objects[$id]["Name"]) . (($level > 0) ? htmlentities(" -> ") : "") ;
	if ($Objects[$id]['ParentId'] == 0) return $Objects[$id]['ObjectTypeName'] ;
	return objPath ($Objects[$id]['ParentId'], $level+1) . htmlentities(' -> ' . $Objects[$id]['Name']) ;
    }

    function objIcon (&$row, $extra=null) {
	global $Project ;
        $icon = "image/object/".$row["Icon"] ; 
	if ($row["ValidDone"]) $icon .= ($row["Done"]) ? "_done" : ((time() > (dbDateDecode($row["EndDate"])+60*60*24) and $Project['TemplateProjectId'] > 0) ? "_over" :  "_open") ;
	if (!is_null($extra)) $icon .= $extra ;
       	if (!$row["External"]) $icon .= "_int" ;
	$icon .= ".gif" ;
	return $icon ;
    }

    // Walk the nodes to build the permission structure
    function calcperm ($id) {
	global $Permission, $PermFields, $Objects ;
	$row = &$Objects[$id] ;
	if ($row['Valid']) return ;
	if ($row['ParentId'] > 0) {
	    // Object has parent
	    $parentrow = &$Objects[$row['ParentId']] ;
	    calcperm ($row['ParentId']) ;
	    $row["Level"] = $parentrow["Level"]+1 ;
	    foreach ($PermFields AS $perm) {
		switch ($row[$perm]) {
		    case -1 :
			// Access fobidden
			break ;

		    case 0 :
			// Inherit permission from parent
			$row[$perm] = $parentrow[$perm] ;
			break ;

		    case 1 :
			// Access requested
			// Allowed if function has permission
			if ($Permission[$perm] == 0) $row[$perm] = -1 ;
			break ;
		}
	    }

	    // Update flag for external information
	    if ($parentrow["External"] == 0) $row["External"] = 0 ;
	} else {
	    // root object
	    $row["Level"] = 0 ;
	    
	    // inherits global permissions
	    foreach ($PermFields AS $perm) {
		$row[$perm] = ($Permission[$perm]) ? 1 : -1 ;
	    }

	    // Root is external
	    $row["External"] = 1 ;
	}
	$row["Valid"] = true ;
    }

    function objLoad ($group) {
	global $PermFields, $Permission, $Project, $Objects, $ObjectTypeFields ;

	// Read selected object tree
	$query = "SELECT Object.*, " ;
	$query .= $ObjectTypeFields ;
	foreach ($PermFields AS $perm) $query .= ", ObjectPermission.".$perm ;
	$query .= sprintf (" FROM Object, ObjectType LEFT JOIN ObjectPermission ON ObjectPermission.ObjectId=Object.Id AND ObjectPermission.RoleId=%d WHERE Object.Active=1 AND Object.ProjectId=%d AND Object.ObjectTypeId=ObjectType.Id AND ObjectType.GroupMark='%s' ORDER BY ObjectType.DisplayOrder, Object.DisplayOrder, Object.EndDate, Object.Name", $Permission["RoleId"], $Project["Id"], $group) ;
	$result = dbQuery ($query) ;  
	while ($row = dbFetch($result)) {
	    $id = (int)$row["Id"] ;
	    switch ($row['ObjectTypeMark']) {
		case 'phase' :
		    if (!$phasefound and !$row["Done"] and time() < dbDateDecode($row["EndDate"])) {
			$phasefound = true ;
			$row['CurrentPhase'] = true ;
		    }
		    break ;
	    }
	    $Objects[$id] = $row ;
	    if ($row["ParentId"] == 0) $rootid = $id ;
	}
	dbQueryFree ($result) ;
    
	// Walk the nodes to build the permission structure
	foreach ($Objects as $id => $row) {
	    calcperm ($id) ;
	}

	$Objects['RootId'] = $rootid ;
	return $rootid ;
    }
?>
