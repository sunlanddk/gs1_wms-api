<?php

    $navigationMarkCache = null ;

    function navigationEnable($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Enable"] = true;
    }

    function navigationDisable($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Enable"] = false;
    }

    function navigationPasive($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Pasive"] = true;
    }

    function navigationSetParameter($mark, $value) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Parameter"] = $value ;
    }

    function navigationLink ($mark) {
    	global $Navigation, $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark or $navigationMarkCache["ParentId"] != $Navigation["Id"]) {
	    // Get links
	    $navigationMarkCache = null ;
	    $link = array () ;
	    navigationItems ($link, $Navigation["Id"], "l", $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	if (is_null($navigationMarkCache)) return 0 ;
	return (int)$navigationMarkCache['Id'] ;
    }

    function navigationIDsByMark($mark) {

       $link = array () ;
       navigationItems ($link, null, null, $mark) ;
       $result = array_keys($link);

       return implode(',', $result);
    }

    function navigationMark ($mark) {
    	global $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark) {
	    // Get links
	    $navigationMarkCache = null ;
	    $link = array () ;
	    navigationItems ($link, null, null, $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	return (int)$navigationMarkCache['Id'] ;
    }

    function navigationOnClickLink ($mark, $id=0, $param=null) {
    	global $Navigation, $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark or $navigationMarkCache["ParentId"] != $Navigation["Id"]) {
	    // Get links
	    $navigationMarkCache = null ;
	    $link = array () ;
	    navigationItems ($link, $Navigation["Id"], "l", $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	if (is_null($navigationMarkCache)) return "style='cursor:default;'" ;
	$t = htmlOnClick ("appClick", $navigationMarkCache, $id, $param) ;
	if ($t == '') return "style='cursor:default;'" ;
	return $t . " style='cursor:pointer;'" ;
    }

        function navigationOnClickMark ($mark=null, $id=0, $param=null) {
	global $nid ;
  //added by ray ----
  global $navigationMarkCache;
  navigationMark($mark);
  if ($navigationMarkCache['Legacy'] || (is_null($mark))) {
    $html = "onclick='location.href=\"index.php?nid=" . ((is_null($mark)) ? $nid : navigationMark($mark)) ;
    }
  else {
    $html = "onclick='location.href=\"\\\itworx.passon.web\\\index.aspx?nid=" . ((is_null($mark)) ? $nid : navigationMark($mark)) ;
  }
  //------------------
  if ($id > 0) $html .= '&id='.$id ;
  if (!is_null($param)) $html .= '&'.$param ;
	$html .= "\"' style='cursor:pointer;'" ;
	return $html ;
    }

    function navigationCommandPopupClose () {
	global $Navigation, $navigationMarkCache ;

	// Close window
	printf ("<script type='text/javascript'>\n") ;
    printf ("\twindow.close();\n") ;
    printf ("\twindow.opener.test();\n") ;
	printf ("</script>\n") ;

	return -1 ;
    }


    function navigationCommandMark ($mark, $id=0, $Param='', $Bypass=0) {
	global $Navigation, $navigationMarkCache ;

	// Any unexpected application messages
	if (headers_sent()) return 0 ;

	// Validate type og navigation
	if ($Bypass=0)
	if ($Navigation['Type'] != 'command') return sprintf ('navigationCommandViev invalid type "%s"', $Navigation['Type']) ;

	// Construct URL
	$nid = navigationMark ($mark) ;
	if ($nid == 0) return sprintf ('%s(%d) navigation ot found, mark \'%s\'', __FILE__, __LINE__, $mark) ;
	if ($navigationMarkCache['Legacy'])
		$href = 'index.php?nid=' . $nid ;
	else
		$href = '\\\itworx.passon.web\\\index.aspx?nid='  . $nid ;
	if ($id > 0) $href .= '&id=' . $id ;
	if ($Param > '') $href .= $Param ;


	// Redirect to new page
	printf ("<script type='text/javascript'>\n") ;
	if (!$navigationMarkCache['LayoutPopup']) {
	    // The new page is not a popup, so close current window if it is a popup
	    printf ("if (window.name.substring(0,5)=='popup') {\n") ;
	    printf ("\tif (!window.opener.closed) {\n") ;
	    printf ("\t\twindow.opener.location.href='%s';\n", $href) ;
	    printf ("\t\twindow.opener.focus();\n") ;
	    printf ("\t}\n") ;
	    printf ("\twindow.close();\n") ;
	    printf ("} else \n") ;
	}
	printf ("\twindow.location.href='%s' ;\n", $href) ;
	printf ("</script>\n") ;

	return -1 ;
    }
    function NewnavigationCommandMark ($mark, $id=0) {
	global $Navigation, $navigationMarkCache ;

	// Any unexpected application messages
	if (headers_sent()) return 0 ;

	// Validate type og navigation
	if ($Navigation['Type'] != 'command') return sprintf ('navigationCommandViev invalid type "%s"', $Navigation['Type']) ;

	// Construct URL
	$nid = navigationMark ($mark) ;
	if ($nid == 0) return sprintf ('%s(%d) navigation ot found, mark \'%s\'', __FILE__, __LINE__, $mark) ;
	$href = 'index.php?nid=' . $nid ;
	if ($id > 0) $href .= '&id=' . $id ;

	// Redirect to new page
	printf ("<script type='text/javascript'>\n") ;
	if (!$navigationMarkCache['LayoutPopup']) {
	    // The new page is not a popup, so close current window if it is a popup
	    printf ("if (window.name.substring(0,5)=='popup') {\n") ;
	    printf ("\tif (!window.opener.closed) {\n") ;
	    printf ("\t\twindow.opener.location.href='%s';\n", $href) ;
	    printf ("\t\twindow.opener.focus();\n") ;
	    printf ("\t}\n") ;
	    printf ("\twindow.close();\n") ;
	    printf ("} else \n") ;
	}
	printf ("\twindow.location.href='%s' ;\n", $href) ;
	printf ("</script>\n") ;

	return -1 ;
    }

    function navigationItems (&$menuarray, $parentid, $type, $mark=null) {
       global $Permission, $User, $Project, $NavigationParam, $PermFields, $PermMap ;

       $query = "SELECT Navigation.*, ".
          "NavigationType.Mark AS Type, NavigationType.LayoutPopup, NavigationType.LayoutRaw, ".
          "Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole";

       foreach ($PermFields AS $perm) {
          $query .= ", Permission.".$perm ;
       }
       $query .= " FROM (Navigation, NavigationType, Function)" ;
       $query .= sprintf (" LEFT JOIN Permission ON Navigation.FunctionId=Permission.FunctionId AND Permission.Active=1 AND ((Permission.RoleId=%d AND (NOT Function.PermProjectRole)) OR (Permission.RoleId=%d AND Function.PermProjectRole)) AND Permission.Active=1", $User['RoleId'], $Project['RoleId']) ;
       $query .= " WHERE" ;
       if ($parentid > 0) {
          $query .= " Navigation.ParentId=".$parentid." AND"  ;
       }
       if (!is_null($mark)) {
          $query .= " Navigation.Mark='".$mark."' AND" ;
       }
       if (!is_null($type)) {
          $query .= " Navigation.RefType='".$type."' AND" ;
       }
       $query .= " Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id AND Navigation.FunctionId=Function.Id AND Function.Active=1" ;
       $query .= " ORDER BY Navigation.DisplayOrder" ;

       $result = dbQuery ($query) ;
       while ($row = dbFetch ($result)) {
          if ($row["Disable"] && !$NavigationParam[$row["Mark"]]["Enable"] && is_null($mark))
             continue ;
          if (!$User["AdminSystem"]) {
             if ($row["PermAdminSystem"])
                continue ;
             if (!$User["AdminProject"]) {
                if ($row["PermAdminProject"])
                   continue ;
                if (!$row["PermPublic"]) {
                   if ($row["PermRecord"] and $row["RefType"] != "m") {
                      if (!$Permission[$PermMap[$row["Operation"]]])
                         continue ;
                   } else {
                      if (!$row[$PermMap[$row["Operation"]]])
                         continue ;
                   }
                }
             }
          }
          $menuarray[((int)$row["Id"])] = $row ;
       }
       dbQueryFree ($result);
    }

    function navigationParameters (&$row, $id=0, $param=null) {
       global $Navigation, $NavigationParam, $Application, $nid ;

       if (isset($NavigationParam[$row["Mark"]]["Parameter"])) {
          if (is_null($param)) {
             $param = $NavigationParam[$row["Mark"]]["Parameter"] ;
          } else {
             $param .= '&' . $NavigationParam[$row["Mark"]]["Parameter"] ;
          }
       }

       switch ($row["Type"]) {
          case 'back':
             $func = sprintf ("appBack(%d)", $row["Back"]) ;
             break ;

          case 'submit':
             if ($row['Function'] != '') {
                $query = sprintf ("SELECT Id FROM Navigation WHERE Mark='%s'", $row['Function']) ;
                $result = dbQuery ($query) ;
                $r = dbFetch ($result) ;
                dbQueryFree ($result) ;
                $row['Id'] = $r['Id'] ;
             } else {
                $row['Id'] = $row['ParentId'] ;
             }
             // Fall through
          case 'command':
             $func = sprintf("appSubmit(%d,%s,%s)", $row["Id"], ($row["RefIdInclude"])?"id":"null", ($row["RefConfirm"]!="") ? "'".$row["RefConfirm"]."'" : "null") ;
             break ;
          case 'ajax' :
             $func = sprintf("appLoadAjaxBody(%d,%s,%s,null,null,null,%s,%s)",
                $row["Id"],
                $row["RefIdInclude"]        ? "id"                          : "null",
                is_null($param)             ? "null"                        : "'".$param."'",
                $row["RefParameters"] != "" ? $row["RefParameters"]         : "null",
                $row["RefConfirm"]    != "" ? "'".$row["RefConfirm"]."'"    : "null"
             ) ;
             break;
          case 'main':
          case 'raw':
          case 'explore':
             $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
             break ;

          case 'parent':
             $func = sprintf("appLoad(%d,%s%s)", $Navigation["ParentId"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
             $id = $Application->ParentId ;	// Some what very dirty !!
             break ;

          case 'reload':
             $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $nid, "id", (is_null($param)) ? "" : (",'".$param."'")) ;
             break ;

          case 'popup':
          case 'rawpopup':
             $func = sprintf("appLoadLegacy(%s,%d,%s,%s,%d,%d)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "null" : ("'".$param."'"), $row["Width"], $row["Height"]) ;
             break ;

          case 'view':
             $func = sprintf("window.open('index.php?nid=%d&id='+id%s,'','')", $row["Id"], (is_null($param)) ? "" : "+'&".$param."'") ;
             break ;

          case 'link':
             $r = array () ;
             navigationItems ($r, NULL, NULL, $row['Function']) ;
             $n = current($r) ;
             $n['Id'] = $row['Id'] ;
             $n['Parameter'] = $row['Parameter'] ;
             $n['RefIdInclude'] = $row['RefIdInclude'] ;
             $n['Legacy'] = $row['Legacy'];
             return navigationParameters ($n, $id, $param) ;

          case 'script':
             $func = $row['Function'] ;
             break ;

          case 'none':
          default:
             $func = '' ;
       }

       return sprintf ("'%s',%s", addslashes($func), ($id > 0) ? $id : "null") ;
    }

    function htmlOnClick ($func, &$row, $id=0, $param=null) {
       global $NavigationParam ;

       if ($NavigationParam[$row["Mark"]]["Pasive"]) {
          return "" ;
       }
       return sprintf ("onclick=\"%s(event,%s)\"", $func, navigationParameters ($row, $id, $param)) ;
    }

    function navigationOnClickLinkByMark ($mark, $id=0, $param=null) {
       return navigationOnClickByMark($mark, $id, $param, "l");
    }

    function navigationOnClickByMark ($mark, $id=0, $param=null, $type=null) {
       $link = array () ;
       navigationItems ($link, null, null, $mark) ;

       $t = htmlOnClick ("appClick", current($link), $id, $param) ;
       if ($t == '') return "style='cursor:default;'" ;
       return $t . " style='cursor:pointer;'" ;
    }

    function prepareMainMenu ($parentid, $level) {
       global $menu, $child ;

       $_menu = '';
       if ($level++ > 5) {
          return ;
       }
       $n = 0 ;
       if ($child[$parentid]) {
          foreach ($child[$parentid] as $id) {
             $_menu .= sprintf ("%s\n%s[%s,'%s',%s,%s", ($level>1 or $n++>0)?",":"", str_repeat("\t", $level), ($menu[$id]["Icon"] != "") ? "'".$menu[$id]["Icon"]."'" : "null", $menu[$id]["Name"], "null", ($menu[$id]["Module"] != "")?"[".navigationParameters($menu[$id], null)."]":"null") ;
             $_menu .= prepareMainMenu ($id, $level) ;
             $_menu .= sprintf ("\n%s]", str_repeat("\t", $level)) ;
          }
       }

       return $_menu;
    }

    function prepareMainMenuNew ($parentid, $level) {
       global $menu, $child ;

       $_menu = '';
       if ($level++ > 5) {
          return ;
       }
       $n = 0 ;
       if ($child[$parentid]) {
          foreach ($child[$parentid] as $id) {
             $_menu .= sprintf ("%s\n%s['%s',%d,%d,%d,%d,%d,%s",
                ($level>1 or $n++>0) ? "," : "",
                str_repeat("\t", $level),
                $menu[$id]["Name"],
                $id, $menu[$id]["ParentId"],
                $menu[$id]["ShowCart"],
                $menu[$id]["isDefault"],
                $menu[$id]["LayoutPopup"],
                ($menu[$id]["Module"] != "") ? "[".navigationParameters($menu[$id], null)."]" : "null")
             ;
             $_menu .= prepareMainMenuNew ($id, $level) ;
             $_menu .= sprintf ("\n%s]", str_repeat("\t", $level)) ;
          }
       }

       return $_menu;
    }

    function prepareToolMenu() {
       global $Navigation, $NavigationParam, $Id;

       $_menu = '';
       $_rows = array () ;
       $buttoncount = 0 ;
       navigationItems ($_rows, $Navigation["Id"], "t") ;

       foreach ($_rows AS $row) {
          if ($buttoncount > 0) {
             $_menu .= sprintf (",\n") ;
          }
          $_menu .= sprintf ("\t[%s,'%s',%s,%s",
             ($row["Icon"] != "") ? "'".$row["Icon"]."'" : "null",
             $row["Name"],
             (trim($row['AccessKey']) == '') ? 'null' : "'".strtoupper($row['AccessKey'])."'",
             ($row['Type'] != 'none' and !$NavigationParam[$row["Mark"]]["Pasive"])?"[".navigationParameters($row, $Id)."]":"null"
          ) ;
          if ($row['Type'] == 'none') {
             // Button drop down menu
             unset ($subbuttons) ;
             navigationItems ($subbuttons, $row["Id"], "t") ;
             if (isset($subbuttons)) {
                foreach ($subbuttons as $button) {
                   $_menu .= sprintf (",\n\t\t[%s,'%s',null,%s]",
                      ($button["Icon"] != "") ? "'".$button["Icon"]."'" : "null",
                      $button["Name"],
                      (!$NavigationParam[$button["Mark"]]["Pasive"])?"[".navigationParameters($button, $Id)."]":"null"
                   ) ;
                }
             }
          }

          $_menu .= sprintf  ("]") ;
          $buttoncount++ ;
       }
       unset ($_rows) ;
       unset ($_subbuttons) ;
       return $_menu;
    }

    function prepareToolMenuNew() {
       global $Navigation, $NavigationParam, $Id;

       $_menu = '';
       $_rows = array () ;
       $buttoncount = 0 ;
       navigationItems ($_rows, $Navigation["Id"], "t") ;

       foreach ($_rows AS $row) {
          if ($buttoncount > 0) {
             $_menu .= sprintf (",\n") ;
          }
          $_menu .= sprintf ("\t['%s',%d,%d,%d,%d,%d,%s",
                $row["Name"],
                $row['Id'],
                $row['ParentId'],
                $row['ShowCart'],
                $row['isDefault'],
                $row['LayoutPopup'],
                ($row['Type'] != 'none' and !$NavigationParam[$row["Mark"]]["Pasive"])?"[".navigationParameters($row, $Id)."]":"null"
          ) ;
          if ($row['Type'] == 'none') {
             // Button drop down menu
             unset ($subbuttons) ;
             navigationItems ($subbuttons, $row["Id"], "t") ;
             if (isset($subbuttons)) {
                foreach ($subbuttons as $button) {
                   $_menu .= sprintf (",\n\t\t['%s',%d,%d,%d,%d,%d,%s]",
                         $button["Name"],
                         $button["Id"],
                         $button["ParentId"],
                         $button["ShowCart"],
                         $button["isDefault"],
                         $button["LayoutPopup"],
                         (!$NavigationParam[$button["Mark"]]["Pasive"])?"[".navigationParameters($button, $Id)."]":"null"
                   ) ;
                }
             }
          }

          $_menu .= sprintf  ("]") ;
          $buttoncount++ ;
       }
       unset ($_rows) ;
       unset ($_subbuttons) ;
       return $_menu;
    }

    function searchDefaultToolbar($id, $default = 0) {
       $_rows       = array () ;
       $_result     = null;
       $_hasDefault = false;
       navigationItems ($_rows, $id, "t") ;
       foreach ($_rows AS $row) {
          $_isDefault = $default === 0 ? $row['isDefault'] : $row['Id'] == (int)$default;

          if ($_isDefault) {
             $_hasDefault = true;
             if (($row['Type'] == 'none') && strlen($row['Module']) == 0) {
                $_result = searchDefaultToolbar($row['Id']);
             } else {
                $_result = $row;
             }
             break;
          }
       }

       if (!$_hasDefault && count($_rows) > 0) {
          $_value = array_values($_rows);
          $_result = $_value[0];
       }

       return $_result;
    }
?>
