<?php
/**
 * Created by JetBrains PhpStorm.
 * User: sbabych
 * Date: 13.06.13
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */

   function isLocalPath($path) {
      return preg_match('#^[a-zA-Z]+:\\\#i', $path);
   }

   function isURL($path) {
      return preg_match('#(https|http|ftp)://#i', $path) || preg_match('#www.#i', $path);
   }

   function joinPaths() {
      $paths = array();

      foreach (func_get_args() as $arg) {
         if ($arg !== '') {
            $paths[] = $arg;
         }
      }

      return preg_replace('~(?<!https:|http:|ftp:)[/\\\\]+~', '/', join('/', $paths));
   }

   function urlExists($url) {
      $file_headers = @get_headers($url);
      return strpos($file_headers[0], '200') !== false;

   }
?>