var _kmCurrentNumContainer;
var _kmPictureSti = 'image/toolbar/';
var _kmMenuPrefix = 'ThemeIE';
var deferfocus = false;

var keyMenu =
{

  	mainFolderLeft: '',
  	mainFolderRight: '<img src="' + _kmPictureSti + 'arrowdown3.gif" width="8" height="12" class="arrowdown" />',
	mainItemLeft: '',
	mainItemRight: '',

	folderLeft: '<img alt="" src="' + _kmPictureSti + 'spacer2.gif" width="20" height="20" />',
	folderRight: '<img alt="" src="' + _kmPictureSti + 'arrow2.gif" width="19" height="16" />',
	itemLeft: '',
	itemRight: '<img alt="" src="' + _kmPictureSti + 'blank.gif" width="1" height="1" />',

	mainSpacing: 0,
	subSpacing: 0,
	delay: 500
};

// Globals
var _haxSuffix = 'hax';
var _kmSuffixKeyNavWidget = '_keynav';
var _mouseOverDebug = 0;
var _menuItemName = "Item";
var _shortcuts = ['1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u'];
var _shortcutsLookup = new Array(_shortcuts.length);
for (var i = 0; i < _shortcuts.length; i++) {
	_shortcutsLookup[_shortcuts[i]] = i;
};

var _kmIDCount = 0;
var _kmIDName = '_Menu';		// for creating submenu id
var _menuItemCounter = 0;

var _firedByKeyboard = false;

var _kmTimeOut = null;
var _kmCurrentItem = null;

var _kmItemList = new Array();
//Added by Karim El Kommmos Dec 26, 2006
//An array that stores the client IDs of the menu bar buttons
var _kmItemIDList = new Array();

var _kmNodeProperties =
{
  	mainFolderLeft: '',
  	mainFolderRight: '',
	mainItemLeft: '',
	mainItemRight: '',

	folderLeft: '',
	folderRight: '',
	itemLeft: '',
	itemRight: '',
	mainSpacing: 0,
	subSpacing: 0,
	delay: 500,
	clickOpen: 3
};




function kmNewID ()
{
	return _kmIDName + (++_kmIDCount);
}


//Modified by Karim El Kommmos Dec 26, 2006
//Added the itemclientid parameter to store the client IDs of the buttons in an array
function kmActionItem(idThis, item, itemclientid, isMain, idSub, orient, nodeProperties, lvlOffset, mainMenuSystemID)
{
	var inactive = false;
	if(item[3] == null && item.length < 5) {inactive = true}

	var clickOpen = _kmNodeProperties.clickOpen;
	if (nodeProperties.clickOpen)
	{
		clickOpen = nodeProperties.clickOpen;
	};
	
	_kmItemList[_kmItemList.length] = item;
	//Added by Karim El Kommmos Dec 26, 2006
	//Store the client ID of each menu bar button in the _kmItemIDList to be able to disable them if needed
	_kmItemIDList[_kmItemIDList.length] = itemclientid;
	var index = _kmItemList.length - 1;
	idSub = (!idSub) ? 'null' : ('\'' + idSub + '\'');
	orient = '\'' + orient + '\'';
	idThis = '\'' + idThis + '\'';

	var returnStr;
	
	returnStr = ' onmouseover="kmItemMouseOver(this,' + isMain + ',' + idSub + ',' + index + ',' + idThis + ',' + lvlOffset + ',\'' + mainMenuSystemID + '\');"';
	returnStr += ' onmousedown="kmItemMouseDownOpenSub(' + idThis	 + ', this,' + index + ',' + orient + ',' + idSub + ')"';
	returnStr += ' onmouseout="kmItemMouseOut(this,' + nodeProperties.delay + ')"';
	returnStr += ' onmouseup="kmItemMouseUp(this,' + index + ')"';
	if (inactive) {
		returnStr += ' style="color:#999" ';
	}
	return returnStr;
}


function kmDrawSubMenu(subMenu, id, orient, nodeProperties, itemIDPrefix, lvl, mainMenuSystemID)
{
	var str = '<div class="' + _kmMenuPrefix + 'SubMenu" id="' + id + '"><div id="' + id + _haxSuffix + '" class="ItemContainer"><table summary="sub menu" cellspacing="' + nodeProperties.subSpacing + '" class="' + _kmMenuPrefix + 'SubMenuTable">';
	var strSub = '';

	var item;
	var idSub;
	var hasChild;
	var theID = id;

	var i;
	var j = 0;

	var classStr;

	for (i = 4; i < subMenu.length; ++i)
	{
		item = subMenu[i];
		if (!item)
		{
			continue;
		};

			
		var thisNameID = itemIDPrefix + _shortcuts[j];
		
		hasChild = (item.length >= 5);
		idSub = hasChild ? thisNameID + _kmIDName : null;

		
		//Modified by Karim El Kommmos Dec 26, 2006
		//Added the client ID "thisNameID" of the button to the call of the kmActionItem function
		str += '<tr id="' + thisNameID + '" class="' + _kmMenuPrefix + 'MenuItem"' + kmActionItem(id, item, thisNameID, 0, idSub, orient, nodeProperties, lvl, mainMenuSystemID) + '>';

		classStr = _kmMenuPrefix + 'Menu';
		classStr += hasChild ? 'Folder' : 'Item';

		str += '<td class="' + classStr + 'Left">';

		if (item[0] != null) {
			//str += item[0];
			str += kmDrawSubMenuMakeIcon(item[0]);
		};
			str += hasChild ? nodeProperties.folderLeft : nodeProperties.itemLeft;


		str += '</td><td><span class="menuNumber">' + _shortcuts[j] + '</span>&nbsp;</td><td class="' + classStr + 'Text"> ' + item[1];
		str += '</td><td class="' + classStr + 'Right">';

		if (hasChild)
		{
			str += nodeProperties.folderRight;
			var thisNameID;
			thisNameID = itemIDPrefix + _shortcuts[j];

			strSub += kmDrawSubMenu (item, idSub, orient, nodeProperties, thisNameID, lvl + 1, mainMenuSystemID);											// Rekursionen
		}
		else
		{
			str += nodeProperties.itemRight;
		}
		str += '</td></tr>';
		j++;
	}

	str += '</table></div></div>' + strSub;
	return str;
}



function kmDrawSubMenuMakeIcon(iconName)
{
	return '<img src="' + _kmPictureSti + iconName + '" alt="' + iconName + '" style="vertical-align: bottom;margin-bottom:2px" />';
}



function kmDraw(id, menu, activateKey)
{
	var lvlOffset = 1;
	var orient = 'hbr';
	var mainMenuObject = kmGetObject(id);

	var nodeProperties = keyMenu;
	
	var str = '<table summary="main menu" class="' + _kmMenuPrefix + 'Menu" cellspacing="' + nodeProperties.mainSpacing + '">';
	var strSub = '';

	var orientStr = String(orient);
	var orientSub;

	orientSub = 'v' + orientStr.substr (1, 2);
	str += '<tr id="' + id + _haxSuffix + '" class="ItemContainer">';

	var i;
	var j = 0;
	var item;
	var idSub;
	var hasChild;


	var classStr;

	for (i = 0; i < menu.length; ++i)
	{

		item = menu[i];

		if (!item)
			continue;

		var thisNameID;
		thisNameID = id + _menuItemName + _shortcuts[j];
		
		
		str += '<td id="' + thisNameID +  '" class="' + _kmMenuPrefix + 'MainItem"';

		hasChild = (item.length >= 5);
		idSub = hasChild ? thisNameID + _kmIDName : null;
		
		//Modified by Karim El Kommmos Dec 26, 2006
		//Added the client ID "thisNameID" of the button to the call of the kmActionItem function
		str += kmActionItem(id, item, thisNameID, 1, idSub, orient, nodeProperties, lvlOffset, id) + '><span class="menuNumber">' + _shortcuts[i] + '</span> &nbsp;';

		classStr = _kmMenuPrefix + 'Main' + (hasChild ? 'Folder' : 'Item');

		str += '<span class="' + classStr + 'Left">';

		str += (item[0] == null) ? (hasChild ? nodeProperties.mainFolderLeft : nodeProperties.mainItemLeft) : kmDrawSubMenuMakeIcon(item[0]) + '&nbsp;&nbsp;</span>';

		var name = item[1] ;
		if ((item[2] != null) && (item[3] != null)) {
			// Underline AccessKey letter
			var re = new RegExp ('('+item[2]+')', 'i') ;
			name = name.replace (re, '<U>$1</U>') ;
		 }
		str += '<span class="' + classStr + 'Text">' + name + '</span><span class="' + classStr + 'Right">';
/*
		str += hasChild ? ('<span id="' + thisNameID + 'arrow">' + nodeProperties.mainFolderRight + '</span>') : nodeProperties.mainItemRight;
*/
	str += hasChild ? nodeProperties.mainFolderRight : nodeProperties.mainItemRight;
		str += '</span></td>';

if ((item[2] != null) && (item[3] != null)) {
str += '<td style="width:0;"><button tabindex="-1" accesskey="' + item[2] + '" style="width:0;position:absolute; top:-1000px" onclick="kmActivateItem(\'' + thisNameID + '\')"></button></td>' ; 
}
		if (hasChild) {
			strSub += kmDrawSubMenu(item, idSub, orientSub, nodeProperties, thisNameID, lvlOffset + 1, id);
		};
		j++;
	}

	
	str += '</tr>';
	str += '</table>' + strSub;
	str += (typeof(addKeyboardNavigation) != 'undefined' && typeof(addKeyboardNavigation)=='function') ? addKeyboardNavigation(id, activateKey) : ''; 
	mainMenuObject.innerHTML = str;


}

function kmActivateItem(itemname) {
	kmGetObject(itemname).onmouseup();
}

function countNumsInContainingMenu(obj) {
	return obj.parentNode.children.length;
}


function kmItemMouseOver(obj, isMain, idSub, index, idMenu, lvlOffset, mainMenuSystemID)
{
	clearTimeout(_kmTimeOut);

	if (_kmCurrentItem) {
		if(_kmCurrentItem == obj) return;
	}
	
	if (obj.kmMenuCount == null)													//Making sure the object properties are only applied once
	{
		obj.kmMainMenu = mainMenuSystemID;												//The name of the menusystem this item is a part of
		
		obj.kmIsMain = isMain;														//boolean, is this element an element in the main menu?
		obj.kmHasSubs = !(!idSub);
		
		obj.kmMenuCount = obj.parentNode.childNodes.length;							//numeric, number of siblings + 1 (object itself)
		obj.kmNumberInMenu = obj.id.charAt(obj.id.length - 1);						//alphanumeric, number that this element has
		obj.kmIDPrefix = obj.id.substring(0, obj.id.length - 1);					//everything but the last number of the id
		obj.kmNumberInMenuNumeric = _shortcutsLookup[obj.kmNumberInMenu];			//numeric, from 0
		
		
		obj.kmLvl = lvlOffset;
		
		obj.kmLastElement = _shortcuts[obj.kmMenuCount - 1];						//alphanumeric, number of last element in the container
		obj.kmIsFirstInMenu = (obj.kmNumberInMenu == _shortcuts[0]); 				//boolean, is this element the top element?
		obj.kmIsLastInMenu = (obj.kmNumberInMenu == obj.kmLastElement);				//boolean, is this element the bottom element?
		obj.kmNumContainer = isMain?kmGetObject(obj.kmIDPrefix.substring(0,obj.kmIDPrefix.length-(_menuItemName.length))+_haxSuffix):kmGetObject(obj.kmIDPrefix + _kmIDName+_haxSuffix);	//The object numcontainer that contains this object
	}

	

	showNums(obj);
	
	var thisMenu = kmGetThisMenu(obj);

	if (!thisMenu.kmItems)
		thisMenu.kmItems = new Array();
	var i;
	for (i = 0; i < thisMenu.kmItems.length; ++i)
	{
		if (thisMenu.kmItems[i] == obj)
			break;
	}
	if (i == thisMenu.kmItems.length)
	{
		thisMenu.kmItems[i] = obj;
	}

	
	
	if (_kmCurrentItem)
	{
		if (_kmCurrentItem == obj || _kmCurrentItem == thisMenu)
		{
			var item = _kmItemList[index];
			return;
		}

		var thatMenu = kmGetThisMenu(_kmCurrentItem);

		if (thatMenu != thisMenu.kmParentMenu)
		{
			if (_kmCurrentItem.kmIsMain) {
				_kmCurrentItem.className = _kmMenuPrefix + 'MainItem';
			}
			else {
				_kmCurrentItem.className = _kmMenuPrefix + 'MenuItem';
			};

			if (thatMenu.id != idSub || _firedByKeyboard) {
				kmHideMenu(thatMenu, thisMenu);
				_firedByKeyboard = false;
			};
		}
	}
	

	_kmCurrentItem = obj;
	
	

	kmResetMenu(thisMenu);

	var item = _kmItemList[index];
	if (isMain) {
		obj.className = _kmMenuPrefix + 'MainItemHover';
	}
	else {
		obj.className = _kmMenuPrefix + 'MenuItemHover';
	};
}

function showNums(obj) {
	//var numcontainer;
	if (_kmCurrentNumContainer != null) {
		_kmCurrentNumContainer.className = 'ItemContainer';
	};
		_kmCurrentNumContainer = obj.kmNumContainer;
		_kmCurrentNumContainer.className = 'ItemContainerShowNums';
}


function kmItemMouseOut(obj, delayTime)
{	
	if (!delayTime)
		delayTime = _kmNodeProperties.delay;
	_kmTimeOut = window.setTimeout('kmHideMenuTime()', delayTime);
	window.defaultStatus = '';
}

function kmItemMouseDown(obj, index)
{
	if (obj.kmIsMain) {
		obj.className = _kmMenuPrefix + 'MainItemActive';
	}
	else {
		obj.className = _kmMenuPrefix + 'MenuItemActive';
	}
}

function kmItemMouseDownOpenSub(thisID, obj, index, orient, idSub)
{
	kmItemMouseDown(obj, index);

	if (idSub)
	{
		var subMenu = kmGetObject(idSub);
		kmShowSubMenu(obj, subMenu, orient);

	}
}

function kmShowSubMenu(obj, subMenu, orient)																						// Kaldes n�r en submenu
{																																			// �bnes, kun 1 gang
	if (!subMenu.kmParentMenu)
	{
		var thisMenu = kmGetThisMenu(obj);
		subMenu.kmParentMenu = thisMenu;
		if (!thisMenu.kmSubMenu)
		{
			thisMenu.kmSubMenu = new Array();
		};
		thisMenu.kmSubMenu[thisMenu.kmSubMenu.length] = subMenu;
	}
	kmMoveSubMenu(obj, subMenu, orient);
	subMenu.style.visibility = 'visible';
	


	if (document.all)
	{
		if (!subMenu.kmOverlap)
			subMenu.kmOverlap = new Array ();
/*@cc_on @*/
/*@if (@_jscript_version >= 5.5)
@else @*/
		kmHideControl ("IFRAME", subMenu);
/*@end @*/
		kmHideControl ("SELECT", subMenu);
		kmHideControl ("OBJECT", subMenu);
	}
}


function kmHideSubMenu (thisMenu)
{
	if (thisMenu.style.visibility == 'hidden')
		return;
	if (thisMenu.kmSubMenu)
	{
		var i;
		for (i = 0; i < thisMenu.kmSubMenu.length; ++i)
		{
			kmHideSubMenu(thisMenu.kmSubMenu[i]);																					// Rekursion
		}
	}
	kmResetMenu(thisMenu);
	thisMenu.style.visibility = 'hidden';
	kmShowControl(thisMenu);
}


// Til n�r et helt tr� blir fjernet, enten timeout eller andet valg p� main menu
//
function kmHideMenu(thisMenu, currentMenu)
{
	var str = _kmMenuPrefix + 'SubMenu';

	if (thisMenu.kmSubMenu)
	{
		var i;
		for (i = 0; i < thisMenu.kmSubMenu.length; ++i)
		{
			kmHideSubMenu (thisMenu.kmSubMenu[i]);
		}
	}

	while (thisMenu && thisMenu != currentMenu)
	{
		kmResetMenu(thisMenu);
		if (thisMenu.className == str)
		{
			thisMenu.style.visibility = 'hidden';
			kmShowControl (thisMenu);
		}
		else
		{
			break;
		};
		thisMenu = kmGetThisMenu (thisMenu.kmParentMenu);
	}
}

function kmGetThisMenu (obj)
{
	var str1 = _kmMenuPrefix + 'SubMenu';
	var str2 = _kmMenuPrefix + 'Menu';
	while (obj)
	{
		if (obj.className == str1 || obj.className == str2)
			return obj;
		obj = obj.parentNode;
	}
	return null;
}

function kmGetObject (id) {
	if (document.all)
		return document.all[id];
	return document.getElementById (id);
}

function addKeyboardNavigation(id, activateKey) {
	//document.body.onkeydown = KeyCheck;
	document.onkeydown = KeyCheck;
	var str = '';
	//str += '<form>';
	
	str += '<input type="button" tabindex="-1" accesskey="' + activateKey + '" onclick="return baat(\'' + id + '\');" id="' + id + _kmSuffixKeyNavWidget + '" name="' + id + '_keynav" style="position: absolute; left: -100px; top: -100px;" />';
	//str += '</form>'; 	
	return str;
}

function baat(menuID) {
	if(window.event) {
	window.event.cancelBubble = true;
	}
	if (!keyNav()) {
		enterKeyboardMode(menuID);
	}
	else {
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		var oldMenu = _kmCurrentItem.kmMainMenu;
		kmHideMenuTime();
		
		if (menuID != oldMenu) {
			enterKeyboardMode(menuID);
		};
	};
	return false;
}

function enterKeyboardMode(menuID) {

	
	selectFirstMenuItem(menuID + _menuItemName);
	
	//deferclick = false;
	return false;
}

function selectFirstMenuItem(menuID)
{
	var firstItemObject = kmGetObject(menuID + _shortcuts[0]);
	if (!firstItemObject) {return false;}
	firstItemObject.onmouseover();
	//firstItemObject.fireEvent("onmouseover");
	return false;
}

function keyNav() {
	return (_kmCurrentItem != null);
}

function exitKeyboardMode() {
	if (!deferfocus) {
		_kmCurrentItem = null;
		appLoaded();
		//document.body.focus();
	}
	//debug("");
	return false;
}

var deferclick = false;

function bodyClick() {


 if (keyNav() && !deferclick) {
escapePress();}
deferclick=false;
}

var testCounter = 0;

function KeyCheck(e) {
	if (keyNav()) {
		var KeyID = (window.event) ? event.keyCode : e.keyCode;
		//var keyboardDebug = kmGetObject('keyboarddebug');
		switch(KeyID) {
			case 13:
				enterPress();
				break;
			case 27:
			case 9:
				escapePress();
				break;
			case 37:
				leftPress();
				//keyboardDebug.value = "Arrow Left" + testCounter++;
				break;
			case 38:
				upPress();
				//keyboardDebug.value = "Arrow Up" + testCounter++;
				break;
			case 39:
				rightPress();
				//keyboardDebug.value = "Arrow Right" + testCounter++;
				break;
			case 40:
				downPress();
				//keyboardDebug.value = "Arrow Down" + testCounter++;
				break;
			default: 
				
				otherPress(KeyID);
				//keyboardDebug.value = ", KeyID: " + KeyID + ", Key: " + theKey;
		};
		return false;
	}
	//else {
		//return true;
	//};
}

function escapePress() {
	_kmCurrentItem.onmouseout();
	//_kmCurrentItem.fireEvent("onmouseout");
	kmHideMenuTime();
}

function enterPress() {
	if (_kmCurrentItem.kmHasSubs) {
		enterMenuSelFirstItem(_kmCurrentItem);
	}
	else {
		_kmCurrentItem.onmouseup();
		//_kmCurrentItem.fireEvent("onmouseup");
	};
}

function otherPress(KeyID) {
	var theKey;
	if (96 <= KeyID && KeyID <= 105) {
		theKey = KeyID - 96;
	}
	else {
		theKey = String.fromCharCode(KeyID).toLowerCase();
	};
	var numberInList = _shortcutsLookup[theKey];
	if (numberInList != null && numberInList < _kmCurrentItem.kmMenuCount) {
		var nextElem = kmGetObject(_kmCurrentItem.kmIDPrefix + _shortcuts[numberInList]);
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		nextElem.onmouseover();
		//nextElem.fireEvent("onmouseover");
		if (nextElem.kmHasSubs) {
			enterMenuSelFirstItem(nextElem);
		}
		else {
			nextElem.onmouseup();
			//nextElem.fireEvent("onmouseup");
		};
	}
	//if (_kmCurrentItem.kmMenuCount)
}

function leftPress() {
	if (_kmCurrentItem.kmIsMain) {
		var prevElem = getPrevElem(_kmCurrentItem);
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		prevElem.onmouseover();
		//prevElem.fireEvent("onmouseover");
	}
	else {
		if (_kmCurrentItem.kmLvl > 2) {
			var parentItem = kmGetObject(_kmCurrentItem.kmIDPrefix);
			_firedByKeyboard = true;
			_kmCurrentItem.onmouseout();
			//_kmCurrentItem.fireEvent("onmouseout");
			parentItem.onmouseover();
			//parentItem.fireEvent("onmouseover");
		}
		else {
			var prevElem = getPrevElem(getTopItemObject(_kmCurrentItem));
			_kmCurrentItem.onmouseout();
			//_kmCurrentItem.fireEvent("onmouseout");
			prevElem.onmouseover();
			//prevElem.fireEvent("onmouseover");
			if (prevElem.kmHasSubs) {
				enterMenuSelFirstItem(prevElem);
			};
		};
	};
}

function upPress() {
	if (!_kmCurrentItem.kmIsMain) {
		var prevElem = getPrevElem(_kmCurrentItem);
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		prevElem.onmouseover();
		//prevElem.fireEvent("onmouseover");
	}
}

function rightPress() {
	if (_kmCurrentItem.kmIsMain) {
		var nextElem = getNextElem(_kmCurrentItem);
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		nextElem.onmouseover();
		//nextElem.fireEvent("onmouseover");
	}
	else {
		if (_kmCurrentItem.kmHasSubs) {
			enterMenuSelFirstItem(_kmCurrentItem);
		}
		else {
			var nextElem = getNextElem(getTopItemObject(_kmCurrentItem));
			_kmCurrentItem.onmouseout();
			//_kmCurrentItem.fireEvent("onmouseout");
			nextElem.onmouseover();
			//nextElem.fireEvent("onmouseover");
			if (nextElem.kmHasSubs) {
				enterMenuSelFirstItem(nextElem);
			};
		};
	}
}

function getTopItemObject(source) {
	return kmGetObject(source.id.substring(0, source.id.length - source.kmLvl + 1));
}


function downPress() {
	if (_kmCurrentItem.kmIsMain) {
		if (_kmCurrentItem.kmHasSubs) {
			enterMenuSelFirstItem(_kmCurrentItem);
		};
	}
	else {
		var nextElem = getNextElem(_kmCurrentItem);
		_kmCurrentItem.onmouseout();
		//_kmCurrentItem.fireEvent("onmouseout");
		nextElem.onmouseover();
		//nextElem.fireEvent("onmouseover");
	};
}

function enterMenuSelFirstItem(source) {
	source.onmousedown();
	//source.fireEvent("onmousedown");
	var firstItemInMenu = kmGetObject(source.id + _shortcuts[0]);
	source.onmouseout();
	//source.fireEvent("onmouseout");
	//_kmCurrentItem.fireEvent("onmouseup");
	firstItemInMenu.onmouseover();
	//firstItemInMenu.fireEvent("onmouseover");
}


function getLastElem(source) {
	return kmGetObject(source.kmIDPrefix + _shortcuts[source.kmMenuCount - 1]);
}

function getFirstElem(source) {
	return kmGetObject(source.kmIDPrefix + _shortcuts[0]);
}

function getNextElem(source) {
	return source.kmIsLastInMenu ? getFirstElem(source) : kmGetObject(source.kmIDPrefix + _shortcuts[source.kmNumberInMenuNumeric + 1]);
}

function getPrevElem(source) {
	return source.kmIsFirstInMenu ? getLastElem(source) : kmGetObject(source.kmIDPrefix + _shortcuts[source.kmNumberInMenuNumeric - 1]);
}

function debug(string) {
	var debugBox = kmGetObject('debug');
	debugBox.value = string;
}

function dumpProps(obj, parent) {
	var msg;
	for (var i in obj) {
		if (parent) {
			var msg = parent + "." + i + "\n" + obj[i];
		}
		else {
			if (i.substring(0, 2) == 'km') {
				msg += i + " = " + obj[i] + "\n";
			};
		}
	}
	var theArea = kmGetObject('propdump');
	theArea.innerText = msg;
   
}



function kmSetStatus (item)
{
	var descript = '';
	if (item.length > 4)
		descript = (item[4] != null) ? item[4] : (item[2] ? item[2] : descript);
	else if (item.length > 2)
		descript = (item[2] ? item[2] : descript);

	window.defaultStatus = descript;
}

function kmGetWidth (obj)
{
	var width = obj.offsetWidth;
	if (width > 0 || !kmIsTRNode (obj))
		return width;
	if (!obj.firstChild)
		return 0;
	return obj.lastChild.offsetLeft - obj.firstChild.offsetLeft + kmGetWidth (obj.lastChild);
}

function kmGetHeight (obj)
{
	var height = obj.offsetHeight;
	if (height > 0 || !kmIsTRNode (obj)) {
		return height;
	};
	if (!obj.firstChild) {
		return 0;
	};
	return obj.firstChild.offsetHeight;
}

function kmGetX (obj)
{
	var x = 0;

	do
	{
		x += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	while (obj);
	return x;
}

function kmGetXAt (obj, elm)
{
	var x = 0;

	while (obj && obj != elm)
	{
		x += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	if (obj == elm)
		return x;
	return x - kmGetX (elm);
}

function kmGetY (obj)
{
	var y = 0;
	do
	{
		y += obj.offsetTop;
		obj = obj.offsetParent;
	}
	while (obj);
	return y;
}

function kmIsTRNode (obj)
{
	var tagName = obj.tagName;
	return tagName == "TR" || tagName == "tr" || tagName == "Tr" || tagName == "tR";
}

function kmGetYAt (obj, elm)
{
	var y = 0;

	if (!obj.offsetHeight && kmIsTRNode (obj))
	{
		var firstTR = obj.parentNode.firstChild;
		obj = obj.firstChild;
		y -= firstTR.firstChild.offsetTop;
	};

	while (obj && obj != elm)
	{
		y += obj.offsetTop;
		obj = obj.offsetParent;
	};

	if (obj == elm) {
		return y;
	};
	return y - kmGetY (elm);
}

function kmHideControl (tagName, subMenu)
{
	var x = kmGetX (subMenu);
	var y = kmGetY (subMenu);
	var w = subMenu.offsetWidth;
	var h = subMenu.offsetHeight;

	var i;
	for (i = 0; i < document.all.tags(tagName).length; ++i)
	{
		var obj = document.all.tags(tagName)[i];
		if (!obj || !obj.offsetParent) {
			continue;
		};

		var ox = kmGetX (obj);
		var oy = kmGetY (obj);
		var ow = obj.offsetWidth;
		var oh = obj.offsetHeight;

		if (ox > (x + w) || (ox + ow) < x) {
			continue;
		};
		if (oy > (y + h) || (oy + oh) < y) {
			continue;
		};
		if(obj.style.visibility == "hidden") {
			continue;
		};
		subMenu.kmOverlap[subMenu.kmOverlap.length] = obj;
		obj.style.visibility = "hidden";
	}
}


function kmShowControl (subMenu) {
	if (subMenu.kmOverlap) {
		var i;
		for (i = 0; i < subMenu.kmOverlap.length; ++i)
			subMenu.kmOverlap[i].style.visibility = "";
	}
	subMenu.kmOverlap = null;
}

function kmHideMenuTime() {
	if (_kmCurrentNumContainer != null) {
		_kmCurrentNumContainer.className = 'ItemContainer';
		_kmCurrentNumContainer = null;
	};
	if (_kmCurrentItem != null) {
		kmHideMenu(kmGetThisMenu (_kmCurrentItem), null);
		_kmCurrentItem = null;
		exitKeyboardMode();
		//var theArea = kmGetObject('propdump');
		//theArea.innerText = "";
	};
}

function kmResetMenu(thisMenu)
{
	if (thisMenu.kmItems)
	{
		var i;
		var str;
		var items = thisMenu.kmItems;
		for (i = 0; i < items.length; ++i)
		{
			if (items[i].kmIsMain) {
				str = _kmMenuPrefix + 'MainItem';
			}
			else {
				str = _kmMenuPrefix + 'MenuItem';
			};
			if (items[i].className != str) {
				items[i].className = str;
			};
		}
	}
}

function kmGetHorizontalAlign (obj, mode, p, subMenuWidth)
{
	var horiz = mode.charAt (2);
	if (!(document.body))
		return horiz;
	var body = document.body;
	var browserLeft;
	var browserRight;
	if (window.innerWidth)
	{
		browserLeft = window.pageXOffset;
		browserRight = window.innerWidth + browserLeft;
	}
	else if (body.clientWidth)
	{
		browserLeft = body.clientLeft;
		browserRight = body.clientWidth + browserLeft;
	}
	else
		return horiz;
	if (mode.charAt (0) == 'h')
	{
		if (horiz == 'r' && (kmGetXAt (obj) + subMenuWidth) > browserRight)
			horiz = 'l';
		if (horiz == 'l' && (kmGetXAt (obj) + kmGetWidth (obj) - subMenuWidth) < browserLeft)
			horiz = 'r';
		return horiz;
	}
	else
	{
		if (horiz == 'r' && (kmGetXAt (obj, p) + kmGetWidth (obj) + subMenuWidth) > browserRight)
			horiz = 'l';
		if (horiz == 'l' && (kmGetXAt (obj, p) - subMenuWidth) < browserLeft)
			horiz = 'r';
		return horiz;
	}
}

function kmMoveSubMenu (obj, subMenu, orient)
{
	var mode = String (orient);
	var p = subMenu.offsetParent;
	var subMenuWidth = kmGetWidth (subMenu);
	var horiz = kmGetHorizontalAlign (obj, mode, p, subMenuWidth);
	if (mode.charAt (0) == 'h')
	{
		if (mode.charAt (1) == 'b')
			subMenu.style.top = (kmGetYAt (obj, p) + kmGetHeight (obj)) + 'px';
		else
			subMenu.style.top = (kmGetYAt (obj, p) - kmGetHeight (subMenu)) + 'px';
		if (horiz == 'r')
			subMenu.style.left = (kmGetXAt (obj, p)) + 'px';
		else
			subMenu.style.left = (kmGetXAt (obj, p) + kmGetWidth (obj) - subMenuWidth) + 'px';
	}
	else
	{
		if (horiz == 'r')
			subMenu.style.left = (kmGetXAt (obj, p) + kmGetWidth (obj)) + 'px';
		else
			subMenu.style.left = (kmGetXAt (obj, p) - subMenuWidth) + 'px';
		if (mode.charAt (1) == 'b')
			subMenu.style.top = (kmGetYAt (obj, p)) + 'px';
		else
			subMenu.style.top = (kmGetYAt (obj, p) + kmGetHeight (obj) - kmGetHeight (subMenu)) + 'px';
	}
}

function kmItemMouseUp(obj, index)
{
	var item = _kmItemList[index];
	deferclick = true;

	var thisMenu = kmGetThisMenu (obj);

	var p = item[3];
	if (p != null) {
		deferfocus = true;
		var result = appClick(null,p[0],p[1]);

		//Added by Karim El Kommmos Dec 26, 2006
		//Disable all other submit buttons untill the page is refreshed
		//Loop on all buttons in the _kmItemList array
		if(p[0].indexOf("Submit") >= 0 && result == true)
		{
			for(i=0; i < _kmItemList.length; i++)
			{
				var tmpItem = _kmItemList[i];
				//Get the action associated with the button
				var tmpP = tmpItem[3];
				//Check if there is already no action for the button
				if(tmpP != null)
				{
					//Check if the action of the button is a Submit action and that it is not a logout
					if(tmpP[0].indexOf("Submit") >=0 && tmpP[0].indexOf("401") == -1)
					{
						//Clear the button action
						tmpItem[3] = null;
						//Get the client ID of the button from _kmItemIDList array
						tmpClientId = _kmItemIDList[i];
						//Change the color of the button to give a disabled look
						document.getElementById(tmpClientId).style.color = "#999";
					}
				}
			}
		}
	};

	var hasChild = (item.length >= 5);
	if (!hasChild)
	{
		if (obj.kmIsMain) {
			obj.className = _kmMenuPrefix + 'MainItem';
		}
		else {
			obj.className = _kmMenuPrefix + 'MenuItem';
		};
		kmHideMenu(thisMenu, null);
	}
	else
	{
		if (obj.kmIsMain) {
			obj.className = _kmMenuPrefix + 'MainItemHover';
		}
		else {
			obj.className = _kmMenuPrefix + 'MenuItemHover';
		};
	}
}

