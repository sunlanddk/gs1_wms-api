<?php

    function strInteger ($s) {
	$s = trim ($s) ;
	if ($s == '') return NULL ;
	$s = ltrim($s, '0') ;
	if ($s == '') $s = '0' ;
	$value = (int)$s ;
	if ($s != (string)$value) return false ;
	return $value ;
    }

    function strFloat ($s, $decimals=0) {
	$s = trim ($s) ;
	if ($s == '') return NULL ;
	$s = str_replace (',', '.',ltrim($s, '0')) ;
	if ($s == '' or $s == '.') $s = '0' ;
	$format = sprintf ('(^[0-9]{1,10}$)|(^[0-9]{0,10}\.[0-9]{0,%d}$)', $decimals) ;
	if (!ereg($format,$s)) return false ;
	return (float)$s ;
    }

    function strCheckBox ($s) {
	if (is_null($s)) return NULL ;
	return ($s == 'on') ;
    }

    function startsWith($haystack, $needle) {
       return substr($haystack, 0, strlen($needle)) === $needle;
    }

    function endsWith($haystack,$needle,$case=true) {
       $expectedPosition = strlen($haystack) - strlen($needle) - 1;

       if($case)
          return strrpos($haystack, $needle, 0) === $expectedPosition;

       return strripos($haystack, $needle, 0) === $expectedPosition;
    }
?>
