<?php

	$PcsPerVariantUnit = array (
		'pcs'	=>	1,
		'PACK2'	=>	2,
		'PACK3'	=>	3,
		'PACK4'	=>	4,
		'Pcs'	=>	1
	) ;
	
	function GetCheckDigit($barcode)
	{
		//Compute the check digit
		$sum=0;
		for($i=1;$i<=11;$i+=2)
			$sum+=3*$barcode{$i};
		for($i=0;$i<=10;$i+=2)
			$sum+=$barcode{$i};
		$r=$sum%10;
		if($r>0)
			$r=10-$r;
		return $r;
	}

?>
