var appFocusField = null ;
var baseUrl = "";
String.prototype.endsWith = function(suffix) {
   return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function appFocus (name) {
    appFocusField = name ;
}

function appBack(back) {
   if ((back==0) && (window.name.substring(0,5) == 'popup')) {
      if (!window.opener.closed) {
         window.opener.focus() ;
      }
      window.close() ;
   } else {
      if (back==0) {
         back=1;
      }
      history.go(-back) ;
   }
}

function appCommandEnd(back) {
    if ((back<=0) && (window.name.substring(0,5)=='popup')) {
       if (!window.opener.closed) {
          if (back==0) {
             // reload current page without reloading all referanced items
             href=window.opener.location.href ;
             if (href.endsWith("#")) {
                href = href.substring(0, href.length - 1);
             } 
             if (href.indexOf("#/") != -1) {
                window.opener.history.go(0);
             } else {
                window.opener.location.replace(href) ;
             }
          } else {
             window.opener.history.go(back);
          }
          window.opener.focus();
       }
       window.close();
    } else {
       history.go(-back);
    }
}

function appSubmit(nid,id,confirm) {
    var form = document.appform ;
//alert ('Pressed') ;
    if (confirm != null) {
       var confirmFunc = window[confirm];
       if (typeof(confirmFunc) == typeof(Function)) {
          var result = confirmFunc();
          if (!result) {
             return false;
          }
       } else if (!window.confirm(confirm)) 
          return false ;
    }



    if (!form) {
	form = document.createElement("FORM");
	form.method = "POST" ;
	form.name = "appform" ;
	document.body.appendChild(form) ;

	var field = document.createElement("INPUT") ;
	field.name = "nid" ;
	field.type = "hidden" ;
	field.value = nid ;
	form.appendChild(field) ;

	if (id > 0) {
	    var field = document.createElement("INPUT") ;
	    field.name = "id" ;
	    field.type = "hidden" ;
	    field.value = id ;
	    form.appendChild(field) ;
	}
    } else {
	form.nid.value = nid ;
	if (id > 0) form.id.value = id ;
    }

	var _clicked = document.appform.clicked ;
    if (!_clicked) {
	var cfield = document.createElement("INPUT") ;
	cfield.name = "clicked" ;
	cfield.type = "hidden" ;
	cfield.value = 1 ;
	form.appendChild(cfield) ;

    document.body.style.cursor = "wait";
    window.status="Saving and processing data ...." ;
	myWindow=window.open('','','width=200,height=100');
	myWindow.document.write("Saving data and processing ....'");
	 myWindow.focus() ;

	 var cookie = "field=" ;
    cookie += escape(window.location.search) + ','
    for (j = 0 ; j < form.elements.length ; j++) {
	var element = form.elements[j] ;
	if (j > 0) cookie += ","
	switch (element.type) {
	    case 'text' :
	    case 'file' :
	    case 'hidden' :
	    case 'textarea' :
		cookie += escape(element.value) ;
		break ;

	    case 'checkbox' :
		cookie += (element.checked) ? 1 : 0 ;
		break ;

	    case 'select-one' :
		cookie += parseInt(element.value) ;
		break ;

	    default :
		cookie += 'x' ;
		break ;
	}
    }
    cookie += '; path=/' ;
    cookie += '; domain=' + window.location.hostname ;
    var expires = new Date() ;
    expires.setTime(expires.getTime() + 10*60*1000) ;
    cookie += '; expires=' + expires.toGMTString() ;
    document.cookie = cookie ;
//alert ('Pressed 2') ;
    document.body.style.cursor = "auto";
    window.status="";
	myWindow.close() ;
    form.submit() ;
//alert ('Pressed 3') ;
    return true;
	} else {
		document.body.style.cursor = "wait";
		window.status="Saving and processing data ...." ;
		myWindow=window.open('','','width=300,height=150');
		myWindow.document.write("Only click once on buttons<br><br>Command is already executing<br><br>Close this window to continue");
		myWindow.focus() ;
	}
}


function appLoad (nid,id,param,width,height) {
    var href = "index.php?nid="+nid ;
    if (id > 0) href += "&id="+id ;
    if (param != null) href += "&"+param ;
    var now = new Date () ;
    href += '&inst=' + now.getTime() ;

    if (width > 0 && height > 0) {
       var LeftPosition=(screen.width)?(screen.width-width)/2:100;
       var TopPosition=(screen.height)?(screen.height-height)/2:100;
       var settings='width='+width+',height='+height+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes,dependent=yes';
       var win=window.open(href,'popup_win'+nid,settings);
    } else {       
       jQuery.ui.Mask.show("Loading ..."); 
       location.href=href ;
    }
}

function appAjaxPaging(offset) {
   $.address.history(false);
   appLoadAjaxBody(null, null, null, null, {offset : offset});
   $.address.history(true);
}

function appLoadAjaxBody(nid,id,param,selector,urlParams,method,callback,confirm) {
   var doInit = false;
   var additionalData = urlParams || {};
   
   if (confirm != null) {
      var confirmFunc = window[confirm];
      if (typeof(confirmFunc) == typeof(Function)) {
         var result = confirmFunc(additionalData);
         if (!result) {
            return false;
         }
      } else if (!window.confirm(confirm)) 
         return false ;
   }
   if (additionalData.selector != null) {
      selector  = additionalData.selector;
   }
   if (additionalData.method != null) {
      method  = additionalData.method;
   }
   
	if (!selector) {
		selector = ".main";
		doInit   = true;
	}
	
	if(!method) {
	   method   = "GET";
	}
	var now = new Date ();
	var data = {
	   nid  : nid || $('input[name="nid"]').val(),
	   inst : now.getTime() 
   };

	if (id != null) {
	   data.Id = id ;
	} 

	if (param != null) {
      data.param = param ;
   }
	
	//saving ajax deep linking history
   var history = $.address.history();
   if (history) {
      $.address.parameter('ajaxNID', data.nid);
      
      if (id != null) {
         $.address.parameter('ajaxID', id, true);
      }
      if (param != null) {
         $.address.parameter('ajaxParam', param, true);
      }      
   }   
   
	//merges data and urlParams (notice that data has a higher priority)
	data = $.extend({}, additionalData, data);
	
	jQuery.ui.Mask.show("Loading ...");
	
	$.ajax({url: baseUrl + 'index.php',type:method,data:data}).done(function(data, textStatus, jqXHR) {
   		jQuery.ui.Mask.hide();
		var ct = jqXHR.getResponseHeader("content-type") || "";
      if (callback) {
         callback((ct.indexOf('json') > -1) ? data : null);
      }
    	$(selector).html(data).show();
    	
    	if(doInit) {
    	   tableManager.init();
    	} else {
    	   $('html, body').animate({ scrollTop: 0 }, 'fast');
    	}
   });
}

function ajaxResponce(data) {
  
}

function appLoadLegacy (legacy,nid,id,param,width,height) {
    var href ="";
    if (legacy) {
        href = baseUrl + "index.php?nid="+nid ;
    } else {
        href="/itworx.passon.web/index.aspx?nid="+nid;
    }
    if (id > 0) href += "&id="+id ;
    if (param != null) href += "&"+param ;
    var now = new Date () ;
    href += '&inst=' + now.getTime() ;

    if (width > 0 && height > 0) {
       var LeftPosition=(screen.width)?(screen.width-width)/2:100;
       var TopPosition=(screen.height)?(screen.height-height)/2:100;
       var settings='width='+width+',height='+height+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes,dependent=yes';
       var win=window.open(href,'popup_win'+nid,settings);
       window.hasPopup = true;
    } else {      
       jQuery.ui.Mask.show("Loading ..."); 
       location.href=href ;
    }
}
function appClick(event,func,id,param) {
	var result;
    eval("result = " + func);
    return result;
}

function appLoaded () {
    window.focus() ;

    if (appFocusField) {
	var collection = document.getElementsByName (appFocusField) ;
	var element = collection[0] ;
	if (element) {
	    element.focus () ;
	    if (element.select) element.select () ;
	}
    }
}

function appRestore() {
    // Get field cookie
    var cookie = 'field=' ;
    var posStart = document.cookie.indexOf(cookie) ;
    if (posStart != -1) {
	posStart += cookie.length ;
	var posEnd = document.cookie.indexOf(';', posStart) ;
	if (posEnd == -1) posEnd = document.cookie.length ;
	var fields = document.cookie.substring(posStart, posEnd).split(',') ;
	
	if (fields.length > 1) {
	    if (unescape(fields.shift()) == window.location.search) {
		var form = document.appform ;
		if (form) {
		    if (fields.length == form.length) {
			for (j = 0 ; j < form.elements.length ; j++) {
			    var element = form.elements[j] ;

			    switch (element.name) {
				case 'id' :
				case 'nid' :
				    continue ;
			    }

			    switch (element.type) {
				case 'text' :
				case 'file' :
				case 'textarea' :
				    element.value = unescape(fields[j]) ;
				    break ;

				case 'checkbox' :
				    element.checked = (fields[j] == '1') ;
				    break ;

				case 'select-one' :
				    element.value = parseInt(fields[j]) ;
				    break ;
			    }
			}
		    }
		}
	    }
	}

	// Delete cookie
	var cookie = 'field=expired' ;
	cookie += '; path=/' ;
	cookie += '; domain=' + window.location.hostname ;
	var expires = new Date() ;
	expires.setTime(expires.getTime() - 24*60*60*1000) ;
	cookie += '; expires=' + expires.toGMTString() ;
	document.cookie = cookie ;
    }
}

function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}