
const url = weburl; 
const appstore = new Vue({
	el: "#appStore",
	data: {
		sscc: '',
		position: '',
		str: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		nextposition: '',
		oldposition: '',
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		vm.str = '';
	        		if(vm.sscc != '' && vm.position != ''){
	        			vm.positionContainer();
	        		}
	        		else{
	        			vm.message = 'Please scan a container and thereafter a position.';
	        		}	        
	        		return;
	        	}
	            if(vm.str.substring(0,2) == '00'){
	            	vm.sscc = vm.str;
	            	vm.str = '';
	            	vm.getAvailablePositionByArticle();
	            	return;
	            }
	            else{
	            	if(vm.sscc == ''){
	            		vm.str = '';
	            		vm.message = 'This is not a container. please start by scanning a container.';
	            		
	            		return;
	            	}
	            	else{
	            		vm.position = vm.str;
	            		vm.str = '';
	            		if(vm.sscc != '' && vm.position != ''){
	        				vm.positionContainer();
	        			}
	            		return;
	            	}
	            }
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    positionContainer(){
	    	var vm = this;
	    	if(vm.sscc != '' && vm.position != ''){
	    		var form = {sscc: vm.sscc, position: vm.position};
	    		vm.postData(url+'shipping/store/container', form, vm.createStoreResponse);
	    	}
	    	else{
	    		vm.message = 'Please scan a container and thereafter a position.';
	    	}
	    },
	    getAvailablePosition(){
	    	var vm = this;
	    	vm.getData(url+'shipping/stocklayout/available/position/no', vm.createAvailableResponse);
	    },
	    getAvailablePositionByArticle(){
	    	var vm = this;
	    	var form = {sscc: vm.sscc};
	    	vm.nextposition = '';
	    	vm.postData(url+'shipping/stocklayout/available/position/by/article', form, vm.createAvailableResponse);
	    },
	    createAvailableResponse(response){
	    	var vm = this;
	    	// console.log(response);
	    	if(response.error == true){
	    		vm.message = 'An error occured, try by scanning again. Error message: ' +response.errorcode;
	    		return
	    	}
	    	vm.nextposition = response.data.next;
	    	vm.oldposition = response.data.old;
	    },
	    createStoreResponse(response){
	    	var vm = this;
	    	// console.log(response);
	    	if(response.error == false){
	    		vm.done = 'SSCC: '+vm.sscc+' has been placed at';
	    		vm.donesecond = 'position: '+vm.position;
	    		vm.sscc = '';
	    		vm.position = '';
	    		vm.nextposition = '';
	    		// vm.getAvailablePosition();
	    	}
	    	else{
	    		vm.message = 'An error occured, try by scanning again. Error message: ' +response.errorcode;
	    		vm.sscc = '';
	    		vm.position = '';
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			     	// return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});






// function getData(url){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "GET",
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }

// function postData(url, data){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "POST",
// 	    data : data,
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }


// $(document).ready(function(){

// 	var str = '';
// 	$(document).keypress(function(e){
// 		var key = e.which;
// 		str += String.fromCharCode(key);
//         if (key==13) {
//             console.log(str);
//             str = '';
//         }
// 	});
// });