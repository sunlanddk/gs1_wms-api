<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	global $User, $Navigation, $Record, $Size, $Id, $csv_data_list ;

	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	
	function CreateCSV($query, $ftp_file) {
		global $User, $Navigation, $Record, $Size, $Id, $csv_data_list, $Config ;
		
		$result = dbQuery ($query) ;
//		print ($query) ; 
		$local_file = $Config['fileStore'] .'/export/' . $ftp_file ; //'varlist.txt';
		$file = fopen($local_file,"w");
		$n = 0 ;
		while ($row = dbFetch ($result)) {
				$csv_data = array_2_csv($row);
//				$csv_data .= "\r\n" ;
				fwrite($file,$csv_data.PHP_EOL);
				$csv_data_list .= $csv_data .'<br>' ;
//				print_r($csv_data.'<br>');
				$n++ ;  
		}
		dbQueryFree ($result) ;
		fclose($file) ;
//		return 0 ;	
		
		if ($Config['Instance'] == 'Test') return 0 ; // dont transfer file if test instance.

		// FTP access parameters
		$ftp_path = '/prod/inbox/' . $ftp_file ;
		$host = 'alpiftp.alpi.dk';
		$usr = 'fub';
		$pwd = 'duJS72CD';
		 
		// connect to FTP server (port 21)
		$conn_id = ftp_connect($host, 21) or die ("Cannot connect to host");
		 
		// send access parameters
		ftp_login($conn_id, $usr, $pwd) or die("Cannot login");
		 
		// turn on passive mode transfers (some servers need this)
		ftp_pasv ($conn_id, true);
		 
		// perform file upload
		$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_ASCII);
		 
		// check upload status:
		if (!$upload) print('Cannot upload ' . ftp_path) ;
//		print (!$upload) ? 'Cannot upload: ' . $upload : 'Upload complete';
//		print "\n";
		 
		/*
		** Chmod the file (just as example)
		*/
		 
		// If you are using PHP4 then you need to use this code:
		// (because the "ftp_chmod" command is just available in PHP5+)
		if (!function_exists('ftp_chmod')) {
		   function ftp_chmod($ftp_stream, $mode, $filename){
				return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
		   }
		}
		 
		// try to chmod the new file to 666 (writeable)
		if (ftp_chmod($conn_id, 0666, $ftp_path) !== false) {
			$_prut = 1 ;
//			print $ftp_path . " chmod successfully to 666<br>";
		} else {
			print "could not chmod $file\n";
		}
		 
		// close the FTP stream
		ftp_close($conn_id);
		
//		print ('Order csv file succesfully created<br>') ;
		return 0 ;	
	}


?>