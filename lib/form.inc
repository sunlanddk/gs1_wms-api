<?php

    $formCalendarLoaded = false ;
    
    function formStart ($name=NULL, $encode=NULL) {
	global $Id ;
	if (is_null($name)) $name = 'appform' ;
	$html = '<form method=POST name="' . $name . '"' ;
	if (!is_null($encode)) $html .= ' enctype="' . $encode . '"' ;
	$html .= sprintf (">\n<input type=hidden name=id value=%d>\n<input type=hidden name=nid>\n", $Id) ;
	print $html ;
    }

    function formCalendar () {
	global $formCalendarLoaded ;
        if ($formCalendarLoaded) return ;
	printf ("<link rel='stylesheet' type='text/css' media='all' href='layout/default/popcal.css'>\n") ;
	printf ("<script type='text/javascript' src='lib/popcal.js'></script>\n") ;
	$formCalendarLoaded = true ;
    }
   
    function formText ($name, $value, $size=0, $style='', $param='') {
	$html = '<input type=text name=' . $name ;
	if ($size > 0) $html .= ' maxlength=' . $size . ' size=' . $size ;
	if ($param != '') $html .= ' ' . $param ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	$html .= ' value="' . htmlentities($value) . '">' ;
	return $html ;
    }

    function formTextArea ($name, $value, $style='') {
	$html = '<textarea name=' . $name ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	$html .= '>' . $value . '</textarea>' ;
	return $html ;
    }

    function formCheckbox ($name, $value=0, $style='', $param='') {
	$html = '<input type=checkbox name=' . $name ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	if ($value) $html .= ' checked' ;
	if ($param != '') $html .= ' ' . $param ;
	$html .= '>' ;
	return $html ;
    }

    function formDate ($name, $value, $style='', $pos='bR') {
	global $formCalendarLoaded ;
	$html = "" ;
	
 	if (!$formCalendarLoaded) {
	    // Load calendar support code
	    $html .= "<link rel='stylesheet' type='text/css' media='all' href='layout/default/popcal.css'>" ;
	    $html .= "<script type='text/javascript' src='lib/popcal.js'></script>" ;
	    $formCalendarLoaded = true ;
	}
	
	$html .= '<input type=text id=Id' . $name . ' name=' . $name . ' size=10 maxlength=10 style="width:68px;' ;
	if ($style != '') $html .= $style ;
	$html .= '"' ;
	if ($value > 0) $html .= ' value="' . date('Y-m-d', $value) . '"' ;
	$html .= '>' ;
	$html .= '<img src="image/date.gif" id=Id' . $name . 'Button style="vertical-align:bottom;margin-left:0px;cursor: pointer;">' ;

	// Activate JavaScript calendar
	$html .= '<script type="text/javascript">' ;
	$html .= 'Calendar.setup ( {' ;
	$html .=	'inputField: "Id' . $name . '",' ;
	$html .= 	'button: "Id' . $name . 'Button",' ;
	$html .=	'align: "' . $pos . '"' ;
	$html .= '} ) ;' ;
	$html .= '</script>' ;

	return $html ;
    }
    
    function formHidden ($name, $value) {
	$html = '<input type=hidden name=' . $name . ' value="' . htmlentities($value) . '">' ;
	return $html ;
    }

    function formDBSelect ($name, $value, $query, $style='', $extrafields=NULL, $param='') {
	$selected = false ;
	$html = '<select size=1 name=' . $name ;
	if ($param != '') $html .= ' ' . $param ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	$html .= '>' ;
	if (!is_null($extrafields)) {
	    foreach ($extrafields as $id => $text) {
		if ($id == $value) $selected = true ;
		$html .= sprintf ("<option%s value=%d>%s\n", ($id == $value)?" selected":"", $id, htmlentities($text));
	    }
	}
	$result = dbQuery ($query) ;
	while ($row = dbFetch($result)) {
	    if ($row["Id"] == $value) $selected = true ;
	    $html .= sprintf ("<option%s value=%d>%s</option>\n", ($row["Id"] == $value)?" selected":"", $row["Id"], htmlentities($row["Value"]));
	}
	dbQueryFree ($result) ;
	if (!$selected) $html .= sprintf ("<option selected value=0>- select -\n") ;
	$html .= sprintf ("</select>\n") ;
	return $html ;
    }

    function formSelect ($name, $value, $option, $style='', $extrafields=NULL, $param='') {
	// Parameter "extrafields" not yet implemented !
	$selected = false ;
	$html = '<select size=1 name=' . $name ;
	if ($param != '') $html .= ' ' . $param ;
	if ($style != '') $html .= ' style="' . $style . '"' ;
	$html .= '>' ;
	foreach ($option as $text) {
	    if ($value == $text) $selected = true ;
	    $html .= sprintf ("<option%s>%s\n", ($value == $text) ? " selected" : "", htmlentities($text));
	}
	if (!$selected) $html .= sprintf ("<option selected value=''>- select -\n") ;
	$html .= sprintf ("</select>\n") ;
	return $html ;
    }

    function formEnd () {
        printf ("</form>\n") ;
    }
?>
