<?php

require_once $Config['legacyLib']."lib/log.inc" ;

require_once $Config['legacyLib']."PEAR/Mail.php" ;
require_once $Config['legacyLib']."PEAR/Mail/mime.php" ;

function userinfo ($id) {
    $query = sprintf ("SELECT User.Loginname, User.Email, User.FirstName, User.LastName, User.Title, Company.Name AS CompanyName FROM User LEFT JOIN Company ON User.CompanyId=Company.Id WHERE User.Id=%d", $id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch($result) ;
    dbQueryFree ($result) ;
    foreach ($row as $key => $value) $row[$key] = htmlentities($value) ;
    return $row ;
}

function saveUserMail ($id, $email) {
   $query = sprintf ("UPDATE User SET Email='%s' WHERE Id=%d", $email, $id) ;
   dbQuery ($query);
}

function sendmail ($mark, $toId, $fromId, $param, $origintime=null, $include_default_html = true, $toEmail = null, $updateEmail = false, $attachments = null, $subjectadd = null) {
    global $Config, $Project ;

    // Get mail layout
    $query = "SELECT MailLayout.* FROM MailLayout WHERE MailLayout.Active=1 AND MailLayout.Mark='$mark'" ;
    $result = dbQuery ($query) ;
    $layout = dbFetch($result) ;
    dbQueryFree ($result) ;
    if (!$layout) {
	   logPrintf (logERROR, "sendmail: layout not found, mark '$mark'") ;
	   return sprintf ("layout not found<br>", $mail_object->message) ;
    }

    // Get destination user

    // HTML header and styles
    if ($include_default_html) {
       $body  = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" ;
       $body .= "<HTML>\n<HEAD>\n" ;
       $body .= "<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" ;
       $body .= "<STYLE>\n" ;
       $body .= "body,table,tr,td,p{margin:0;padding:0;border:0;border-spacing:0;border-width:0;border-style:none;border-collapse:collapse;font:13px Arial, Helvetica, sans-serif;caption-side:left;vertical-align:top;}\n" ;
       $body .= "p{margin-left:8px;margin-right:8px;margin-top:14px}\n" ;
       $body .= "td.header{background:#0000b0;color:#ffffff;vertical-align:middle;font:20px Arial;padding-left:9px;padding-right:9px;height=40px;}\n" ;
       $body .= "</STYLE>\n</HEAD><BODY>\n" ;
   } else {
      $body  = "";
   }

    eval ('$header="'.$layout['Header'].'";') ;

    // Check if this is a 'resend' of the E-mail
    if (!is_null($origintime)) {
        // Get mail layout
		$query = "SELECT MailLayout.* FROM MailLayout WHERE MailLayout.Active=1 AND MailLayout.Mark='resend'" ;
		$result = dbQuery ($query) ;
		$resend = dbFetch($result) ;
		dbQueryFree ($result) ;
		if (!$resend) {
			logPrintf (logERROR, "sendmail: layout not found, mark 'resend'") ;
			return sprintf ("layout not found<br>", $mail_object->message) ;
		}

		// Layout parameters
		$origindate = date ("Y-m-d", $origintime) ;

		// Format 'resend' note
        eval ('$body.="'.$resend['Body'].'";') ;
    }

    // Format E-mail body
    eval ('$body.="'.$layout['Body'].'";') ;
    if ($include_default_html) {
       // HTML prolog
       $body .= "</BODY></HTML>\n" ;
    }

    // MIME encode
    $headers = array();
    $subject = $layout["Subject"] . ' ' . $subjectadd;

    $mime = new Mail_mime();
    $mime->setFrom (($from) ? $from["Email"] : $Config["SMTPFrom"]);
    $mime->addTo ($toEmail);
//    $mime->addCc ('kc@novotex.dk');

//  $mime->setSubject (html_entity_decode($subject)) ;
    $mime->setSubject ($subject) ;
    $mime->setTXTBody("Use HTML-mode to view this E-mail");
    $mime->setHTMLBody($body);

   //add attachments
   if (!empty($attachments)) {
      for ($i = 0; $i < count($attachments); $i++) {
         if (!empty($attachments[$i]['path'])) {
            $mime->addAttachment($attachments[$i]['path'], $attachments[$i]['mimeType'], $attachments[$i]['name']);
         }
      }
   }

    $body = $mime->get();
    $headers = $mime->headers($headers);

    // Send E-mail
    $params = array();
    switch ($Config["mailDriver"]) {
       case "smtp":
          $params['host']     = $Config["SMTPHost"];
          $params["port"]     = $Config['SMTPPort'];
          $params["auth"]     = $Config['SMTPAuth'];
          $params["username"] = $Config['SMTPFrom'];
          $params["password"] = $Config['SMTPPasswd'];
          $params["localhost"]= $Config["SMTPHost"];
          break;
    }

    $mail =& Mail::factory($Config["mailDriver"], $params);

    if (PEAR::isError($mail)) {
		logPrintf (logERROR, "Mail::factory, message %s", $res->message) ;
		return sprintf ("SMTP-factory: %s<br>", $mail_object->message) ;
    }

    $res = $mail->send($toEmail, $headers, $body);
    if (PEAR::isError($res)) {
		logPrintf (logERROR, "mail->send, host %s, to %s, subject %s, message %s", $Config["SMTPHost"], $toEmail, $subject, $res->message) ;
		return sprintf ("sending E-mail: %s<br>", $res->message) ;
    }
	$Project['Id'] = $toId ; // Insert CompanyId for customer in log to allow customer mail log.
    logPrintf (logMAIL, "Send to: %s, Subject: %s, Executed: %s %s", $toEmail, $subject, ($res[0]>0)?$res[0]:'Succesfully', $res[1]) ;

    return 0 ;
}
function sendmail_org ($mark, $toId, $fromId, $param, $origintime=null, $include_default_html = true, $toEmail = null, $updateEmail = false, $attachments = null) {
    global $Config, $Project ;

    // Get mail layout
    $query = "SELECT MailLayout.* FROM MailLayout WHERE MailLayout.Active=1 AND MailLayout.Mark='$mark'" ;
    $result = dbQuery ($query) ;
    $layout = dbFetch($result) ;
    dbQueryFree ($result) ;
    if (!$layout) {
	   logPrintf (logERROR, "sendmail: layout not found, mark '$mark'") ;
	   return sprintf ("layout not found<br>", $mail_object->message) ;
    }

    // Get destination user
    $to = userinfo ($toId) ;
    if (!$to) {
	   logPrintf (logERROR, "sendmail: user not found, mark '$mark', id $toId") ;
	   return sprintf ("user not found<br>", $mail_object->message) ;
    }
    if ($updateEmail && $toEmail != null) {
       saveUserMail ($toId, $toEmail) ;
    }
    // Get origin user
    if ($fromId > 0) $from = userinfo ($fromId) ;

    // Layout parameters
    $projectname = htmlentities($Project['Name']) ;

   // $projectlink = sprintf ('http://%s/legacy/index.php?id=%d&user=%s', $Config['Server'], $Project['Id'], $to['Loginname']) ;

    $projectlink = sprintf ('http://%s/index.php?id=%d&user=%s', $Config['Server'], $Project['Id'], $to['Loginname']) ;

    // HTML header and styles
    if ($include_default_html) {
       $body  = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" ;
       $body .= "<HTML>\n<HEAD>\n" ;
       $body .= "<META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" ;
       $body .= "<STYLE>\n" ;
       $body .= "body,table,tr,td,p{margin:0;padding:0;border:0;border-spacing:0;border-width:0;border-style:none;border-collapse:collapse;font:13px Arial, Helvetica, sans-serif;caption-side:left;vertical-align:top;}\n" ;
       $body .= "p{margin-left:8px;margin-right:8px;margin-top:14px}\n" ;
       $body .= "td.header{background:#0000b0;color:#ffffff;vertical-align:middle;font:20px Arial;padding-left:9px;padding-right:9px;height=40px;}\n" ;
       $body .= "</STYLE>\n</HEAD><BODY>\n" ;
   } else {
      $body  = "";
   }

    // proWide header
    eval ('$header="'.$layout['Header'].'";') ;
//    $body .= sprintf ($Config['HTMLHeader'], $header) ;

    // Check if this is a 'resend' of the E-mail
    if (!is_null($origintime)) {
        // Get mail layout
	$query = "SELECT MailLayout.* FROM MailLayout WHERE MailLayout.Active=1 AND MailLayout.Mark='resend'" ;
	$result = dbQuery ($query) ;
	$resend = dbFetch($result) ;
	dbQueryFree ($result) ;
	if (!$resend) {
	    logPrintf (logERROR, "sendmail: layout not found, mark 'resend'") ;
	    return sprintf ("layout not found<br>", $mail_object->message) ;
	}

	// Layout parameters
	$origindate = date ("Y-m-d", $origintime) ;

	// Format 'resend' note
        eval ('$body.="'.$resend['Body'].'";') ;
    }

    // Format E-mail body
    eval ('$body.="'.$layout['Body'].'";') ;
    if ($include_default_html) {
       // HTML prolog
       $body .= "</BODY></HTML>\n" ;
    }

    // MIME encode
    $headers = array();//['To'] = $to["Email"] ;
    $subject = $layout["Subject"];

    $mime = new Mail_mime();
    $mime->setFrom (($from) ? $from["Email"] : $Config["SMTPFrom"]);

//  $mime->setSubject (html_entity_decode($subject)) ;
    $mime->setSubject ($subject) ;
    $mime->setTXTBody("Use HTML-mode to view this E-mail");
    $mime->setHTMLBody($body);

   //add attachments
   if (!empty($attachments)) {
      for ($i = 0; $i < count($attachments); $i++) {
         if (!empty($attachments[$i]['path'])) {
            $mime->addAttachment($attachments[$i]['path'], $attachments[$i]['mimeType'], $attachments[$i]['name']);
         }
      }
   }

    $body = $mime->get();
    $headers = $mime->headers($headers);

    // Send E-mail
    $params = array();
    switch ($Config["mailDriver"]) {
       case "smtp":
          $params['host']     = $Config["SMTPHost"];
          $params["port"]     = $Config['SMTPPort'];
          $params["auth"]     = $Config['SMTPAuth'];
          $params["username"] = $Config['SMTPFrom'];
          $params["password"] = $Config['SMTPPasswd'];
          $params["localhost"]= $Config["SMTPHost"];
          break;
    }

    $mail =& Mail::factory($Config["mailDriver"], $params);

    if (PEAR::isError($mail)) {
	logPrintf (logERROR, "Mail::factory, message %s", $res->message) ;
	return sprintf ("SMTP-factory: %s<br>", $mail_object->message) ;
    }

    $res = $mail->send(($toEmail != null ? $toEmail : $to["Email"]), $headers, $body);
    if (PEAR::isError($res)) {
	logPrintf (logERROR, "mail->send, host %s, to %s, from %s, subject %s, message %s", $Config["SMTPHost"], $to["Email"], $from["Email"], $subject1, $res->message) ;
	return sprintf ("sending E-mail: %s<br>", $res->message) ;
    }

    logPrintf (logMAIL, "send, host %s, to %s, from %s, subject %s, responce %d %s", $Config["SMTPHost"], $to["Email"], $from["Email"], $subject1, $res[0], $res[1]) ;

    return 0 ;
}

?>
