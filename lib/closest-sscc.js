
const url = weburl; 
const appstore = new Vue({
	el: "#appStore",
	data: {
		sscc: '',
		item: '',
		position: '',
		nextposition: '',
		str: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		nextpositions: [],
		preloader: false
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		vm.str = '';
	        		if(vm.item != ''){
	        			//vm.getAvailableSSCC();
	        			return;
	        		}
	        		if(vm.position != ''){
	        			//vm.getAvailableSSCC();
	        			return;
	        		}
	        		vm.message = 'Please scan a position';
	        		return;
	        	}
	        	
	        	if(vm.str == ''){
	        		return;
	        	}

	        	if(vm.position == ''){
	        		vm.position = vm.str;
	        		vm.str = '';
	        		vm.getVariantsFromPosition();
	        		return;
	        	}

	        	if(vm.sscc == ''){
	        		if(vm.str.substring(0,2) == '00'){
		        		vm.sscc = vm.str;
		            	vm.str = '';
	    	        	return;
	    	        }
	    	        vm.message = 'This is not an sscc, please scan again.';
	    	        vm.str = '';
	    	        return;
	            }

	            if(vm.position != '' && vm.sscc != ''){
					vm.nextposition = vm.str;
					vm.str = '';

					if(vm.nextposition == vm.position){
						vm.replenishVariant();
						return;
					}	
					vm.nextposition = '';
					vm.message = 'Replenish position should be the same as first scanned.';
					return;            	
	            }
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    getAvailableSSCC(){
	    	var vm = this;
	    	var form = {position: vm.position};
	    	return;

	    	vm.postData(url+'shipping/get/sscc/available', form, vm.getAvaiableResponse);
	    },
	    reset(){
	    	var vm = this;
	    	vm.position = '';
	    	vm.str = '';
	    	vm.nextposition = '';
	    	vm.nextpositions = [];
	    	vm.message = '';
			vm.done = '';
			vm.donesecond = '';
			vm.sscc = '';
			return;
	    },
	    getVariantsFromPosition(){
	    	var vm = this;
	    	var form = {position: vm.position};
	    	vm.preloader = true;
	    	vm.postData(url+'shipping/get/sscc/available', form, vm.getVariantsFromPositionResponse);
	    },
	    getVariantsFromPositionResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		vm.nextpositions = response.data;
	    		vm.preloader = false;
	    		return;
	    	}

	    	alert('An error occured, try by scanning again. Error message: ' +JSON.stringify(response.errorcode));
	    	vm.reset();
	    	vm.preloader = false;
	    },
	    replenishVariant(){
	    	var vm = this;
	    	if(vm.nextposition == '' || vm.position == '' || vm.sscc == ''){
	    		vm.message = 'Not all information has been scanned.';
				return;
	    	}

	    	var form = {position: vm.nextposition, sscc: vm.sscc};
	    	vm.preloader = true;
	    	vm.postData(url+'shipping/replenish/variant', form, vm.replenishVariantResponse);
	    	return;
	    },
	    replenishVariantResponse(response){
	    	var vm = this;
	    	vm.preloader = false;

	    	if(response.error == false){
	    		vm.reset();
	    		vm.done = 'Position has been replenished';
	    		return;
	    	}

	    	vm.reset();

	    	vm.message = 'An error occured. Errorcode: '+JSON.stringify(response.errorcode);
	    },
	    getAvaiableResponse(response){
	    	var vm = this;
	    	// console.log(response);
	    	if(response.error == false){
	    		vm.item = '';
	    		vm.position = '';
	    		vm.nextposition = response.data.position;
	    		vm.sscc = response.data.sscc;
	    		vm.done = 'Scanned:'+vm.position+ vm.item+' success, scan next.';
	    		// vm.getAvailablePosition();
	    	}
	    	else{
	    		vm.message = 'An error occured, try by scanning again. Error message: ' +JSON.stringify(response.errorcode);
	    		// vm.reset();
	    		vm.item = '';
	    		vm.done = '';
	    		vm.position = '';
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {

			    	vm.preloader = false;
			    	vm.reset();
			    	vm.message = 'An error occured, try by scanning again. Code:'+textStatus;
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			     	// return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});






// function getData(url){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "GET",
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }

// function postData(url, data){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "POST",
// 	    data : data,
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }


// $(document).ready(function(){

// 	var str = '';
// 	$(document).keypress(function(e){
// 		var key = e.which;
// 		str += String.fromCharCode(key);
//         if (key==13) {
//             console.log(str);
//             str = '';
//         }
// 	});
// });