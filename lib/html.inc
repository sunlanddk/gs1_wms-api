<?php

    function htmlDBSelect ($fieldname, $value, $query, $extrafields=NULL) {
	$selected = false ;
	$html = sprintf ("<select size=1 name=%s>\n", $fieldname) ;
	if (!is_null($extrafields)) {
	    foreach ($extrafields as $id => $text) {
		if ($id == $value) $selected = true ;
		$html .= sprintf ("<option%s value=%d>%s\n", ($id == $value)?" selected":"", $id, htmlentities($text));
	    }
	}
	$result = dbQuery ($query) ;
	while ($row = dbFetch($result)) {
	    if ($row["Id"] == $value) $selected = true ;
	    $html .= sprintf ("<option%s value=%d>%s\n", ($row["Id"] == $value)?" selected":"", $row["Id"], htmlentities($row["Value"]));
	}
	dbQueryFree ($result) ;
	if (!$selected) $html .= sprintf ("<option selected value=0>- select -\n") ;
	$html .= sprintf ("</select>\n") ;
	return $html ;
    }

    function htmlSelect ($fieldname, $value, $option) {
	$selected = false ;
	$html = sprintf ("<select size=1 name=%s>\n", $fieldname) ;
	foreach ($option as $text) {
	    if ($value == $text) $selected = true ;
	    $html .= sprintf ("<option%s>%s\n", ($value == $text) ? " selected" : "", htmlentities($text));
	}
	if (!$selected) $html .= sprintf ("<option selected value=''>- select -\n") ;
	$html .= sprintf ("</select>\n") ;
	return $html ;
    }

    function htmlOptionButton ($fieldname, $parameter) {
	global $Id, $Navigation ;
	return "<button name='".$fieldname."' class=midbutton onClick='location.href=\"index.php?nid=".$Navigation["Id"].(($Id>0)?"&id=".$Id:"").(($parameter) ? "&".$parameter : "")."\"'>".$fieldname."</button>\n" ;
    }

    function htmlItemHeader ($t1=NULL, $t2=NULL, $h1=NULL) {
	if (!$t1) $t1 = 'Field' ;
	if (!$t2) $t2 = 'Content' ;
	if ($h1) {
	    printf ("<tr><td class=\"itemheader super\" >%s</td></tr>\n", $h1) ;
	    printf ("</table>\n") ;
	    printf ("<table class=item>\n") ;
	}
	return '<tr><td class=itemheader width=80>'.$t1.'</td><td class=itemheader>'.$t2.'</td></tr>'.htmlItemSpace() ;
    }

    function htmlItemSpace () {
	return "<tr><td class=itemspace></td></tr>\n" ;
    }

    function htmlItemInfo ($row) {
	
	if ($row['Id'] <= 0) return '' ;

	$html = htmlItemSpace() ;

	if ($row['CreateUserId'] == 0) {
	    $html .= sprintf ("<tr><td class=itemlabel>Created</td><td class=itemfield>Initial</td></tr>\n") ;
	} else {
	    $query = sprintf ("SELECT CONCAT(FirstName, ' ', LastName) AS UserName, Loginname FROM User WHERE Id=%d", $row["CreateUserId"]) ;
	    $result = dbQuery ($query) ;
	    $user = dbFetch ($result) ;
	    dbQueryFree ($result) ;	
	    $html .= sprintf ("<tr><td class=itemlabel>Created</td><td class=itemfield>%s, %s (%s)</td></tr>\n", date ("Y-m-d H:i:s", dbDateDecode($row["CreateDate"])), htmlentities($user["UserName"]), htmlentities($user["Loginname"])) ;
	}
	
	if (($row["CreateDate"] != $row["ModifyDate"]) or ($row["CreateUserId"] != $row["ModifyUserId"])) {
	    $query = sprintf ("SELECT CONCAT(FirstName, ' ', LastName) AS UserName, Loginname FROM User WHERE Id=%d", $row["ModifyUserId"]) ;
	    $result = dbQuery ($query) ;
	    $user = dbFetch ($result) ;
	    dbQueryFree ($result) ;	
		if ($row["Modifydate"]>'2001-01-01') $row["ModifyDate"] = $row["Modifydate"] ;
	    $html .= sprintf ("<tr><td class=itemlabel>Modified</td><td class=itemfield>%s, %s (%s)</td></tr>\n", date ("Y-m-d H:i:s", dbDateDecode($row["ModifyDate"])), htmlentities($user["UserName"]), htmlentities($user["Loginname"])) ;
	}
	return $html ;
    }
    
    function htmlItemAppInfo ($app) {
	if ($app->Id <= 0) return '' ;

	$html = htmlItemSpace() ;

	if ($App->CreateUserId == 0) {
	    $html .= sprintf ("<tr><td class=itemlabel>Created</td><td class=itemfield>Initial</td></tr>\n") ;
	} else {
	    $query = sprintf ("SELECT CONCAT(FirstName, ' ', LastName) AS UserName, Loginname FROM User WHERE Id=%d", $app->CreateUserId) ;
	    $result = dbQuery ($query) ;
	    $user = dbFetch ($result) ;
	    dbQueryFree ($result) ;	
	    $html .= sprintf ("<tr><td class=itemlabel>Created</td><td class=itemfield>%s, %s (%s)</td></tr>\n", date ("Y-m-d H:i:s", dbDateDecode($app->CreateDate)), htmlentities($user["UserName"]), htmlentities($user["Loginname"])) ;
	}
	
	if (($app->CreateDate != $app->ModifyDate) or ($app->CreateUserId != $app->ModifyUserId)) {
	    $query = sprintf ("SELECT CONCAT(FirstName, ' ', LastName) AS UserName, Loginname FROM User WHERE Id=%d", $app->ModifyUserId) ;
	    $result = dbQuery ($query) ;
	    $user = dbFetch ($result) ;
	    dbQueryFree ($result) ;	
	    $html .= sprintf ("<tr><td class=itemlabel>Modified</td><td class=itemfield>%s, %s (%s)</td></tr>\n", date ("Y-m-d H:i:s", dbDateDecode($app->ModifyDate)), htmlentities($user["UserName"]), htmlentities($user["Loginname"])) ;
	}
	return $html ;
    }
?>
