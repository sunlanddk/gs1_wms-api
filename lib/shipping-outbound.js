
const url = weburl; 
const appoutbound = new Vue({
	

	el: "#appoutbound",
	data: {
		items: [],
		variants: [],
		notscanable: [],
		totalpending: 0,
		totalnotscanable: 0,
		totalscan: 0,
		inTotalnotscanable: 0,
		inTotal: 0,
		totalPackedAndScanned: 0,
		str: '',
		total: 0,
		cansave: 1,
		id: '',
		url: url
	},
	beforeMount(){
		var vm = this;
		vm.getPickorderlines($('.idhidden').val());
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		if($('.idhidden').val() == 0){
	        			alert('Pickorder is done, no further commands can be made.');
	        			return;
	        		}
	        		vm.str = '';
	        		vm.createOutbound();
	        		return;
	        	}
	            if(vm.str.substring(0,2) == '00'){
            		var found = true;
            		vm.items.forEach(function(el, i) {
            			if(el.sscc == vm.str){
            				found = false;
            			}
            		});
            		if(found == true){
	            		vm.items.push({'sscc':vm.str, 'description': '', 'ssccshort':'', 'position':'', 'description':'', 'variant':'', 'alreadysaved' : false})
	            		vm.getContainerData(vm.str);
	            	}
	            	vm.str = '';
	            	return;
	            }
	            vm.str = '';
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		reset(){
			var vm = this;
			vm.items = [];
			vm.variants = [];
			vm.notscanable = [];
			vm.totalpending = 0;
			vm.totalnotscanable = 0;
			vm.totalscan = 0;
			vm.inTotalnotscanable = 0;
			vm.inTotal = 0;
			vm.totalPackedAndScanned = 0;
			vm.str = '';
			vm.total = 0;
			vm.cansave = 1;

			vm.getPickorderlines($('.idhidden').val());

		},
		removeitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    getPickorderlines(id){
	    	var vm = this;
	    	if(id > 0){
	    		var form = {id : id};
	    		vm.postData(url+'shipping/pickorder/lines', form, vm.createPickorderlinesResponse);
	    	}
	    	else{
	    		// alert('There are no scanned items yet.');
	    	}
	    },
	    createPickorderlinesResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		var lineData = response.data.lines;
	    		var notscanableData = response.data.notscanable;
	    		if(lineData != false){
		    		lineData.forEach(function(el, i) {
		    			vm.totalpending += (parseInt(el.qty) - parseInt(el.sqty));
		    			vm.total += parseInt(el.sqty);
		    			vm.inTotal += parseInt(el.qty);
		    			vm.totalPackedAndScanned += parseInt(el.sqty);

		    			vm.variants.push({'lineid': el.pickorderid ,'qty': parseInt(el.qty), 'pqty':(parseInt(el.qty) - parseInt(el.sqty)), 'variant': el.variant, 'description': el.description, 'sqty' : parseInt(el.sqty)});
	        		});
	    		}

        		if(notscanableData != false){
	        		notscanableData.forEach(function(el, i) {
	        			vm.totalnotscanable += (parseInt(el.qty) - parseInt(el.sqty));
	        			vm.totalscan += parseInt(el.sqty);
	        			vm.inTotalnotscanable += parseInt(el.qty);
	        			vm.totalPackedAndScanned += parseInt(el.sqty);
		    			vm.notscanable.push({'alreadysaved': el.alreadysaved,'lineid': el.pickorderid ,'qty': parseInt(el.qty), 'pqty':(parseInt(el.qty) - parseInt(el.sqty)), 'variant': el.variant, 'description': el.description, 'sqty' : parseInt(el.sqty)});
	        		});
	        	}

        		// console.log(response.data);
        		if(response.data.items != false){
        			response.data.items.forEach(function(el, i) {
	    				vm.items.push({'lineid': 0 ,'description': el.Description, 'variant': el.VariantCode, 'position': el.Position, 'alreadysaved': true, 'sscc': el.Sscc});
        			});
        		}
	    	}
	    	else{
	    		alert('An error occured, please try again');
	    	}
	    },
	    getContainerData(ssccstr){
	    	var vm = this;
	    	if(ssccstr != ''){
	    		var form = {sscc : ssccstr};
	    		vm.postData(url+'shipping/container/data', form, vm.createContainerResponse);
	    	}
	    	else{
	    		alert('There are no scanned items yet.');
	    	}
	    },
	    saveToLoading(){
	    	var vm = this;
	    	if(vm.items.length > 0 || vm.notscanable.length > 0){
	    		var form = {
	    			items : vm.items, 
	    			id: $('.idhidden').val(), 
	    			stockid: $('.stockid').val(), 
	    			totalpending: vm.totalpending,
	    			notscanable: vm.notscanable
	    		};
	    		vm.postData(url+'shipping/outbound/save', form, vm.saveToLoadingResponse);
	    	}
	    	else{
	    		alert('There are no scanned items yet.');
	    	}
	    },
	    saveToLoadingResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		vm.items = [];
	    		window.location = window.location;
	    		return;
	    		alert('Outbound shipment has been created, and are ready to depart.')
	    	}
	    	else{
	    		alert('An error occured, please try again');
	    	}
	    	
	    },
	    createOutbound(){
	    	var vm = this;
	    	if(vm.items.length > 0 || vm.notscanable.length > 0){
	    		var form = {
	    			items : vm.items, 
	    			notscanable: vm.notscanable, 
	    			totalpending: vm.totalpending, 
	    			id: $('.idhidden').val(), 
	    			stockid: $('.stockid').val()
	    		};
	    		vm.postData(url+'shipping/outbound/new', form, vm.createOutboundResponse);
	    	}
	    	else{
	    		alert('There are no scanned items yet.');
	    	}
	    },
	    createContainerResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		var cotainerData = response.data;
	    		var found = false;
	    		if(cotainerData.Cansave == false){
	    			vm.cansave = 0;
	    		}
	    		vm.items.forEach(function(el, i) {
	    			if(el.sscc == cotainerData.Sscc){
	    				found = true;
	    			}
	    			if(el.alreadysaved !== true){
	        			if(el.sscc == cotainerData.Ssccfull){
	        				if(found == true){
	        					vm.items.splice(i, 1);
	        					return;
	        				}
	        				// console.log(cotainerData);
	        				vm.items[i].description = cotainerData.Description;
	        				vm.items[i].cansave = cotainerData.Cansave;
	        				vm.items[i].variant = cotainerData.VariantCode;
	        				vm.items[i].position = cotainerData.Position;
	        				vm.items[i].ssccshort = cotainerData.Sscc;
	        				vm.items[i].sscc = cotainerData.Sscc;
	        				vm.items[i].lineid = 0;
	        				var variantfound = true;
	        				vm.variants.forEach(function(v, ind){
	        					if(v.variant == cotainerData.VariantCode){
	        						vm.items[i].lineid = v.lineid;
	        						vm.variants[ind].sqty += 1;
	        						vm.variants[ind].pqty -= 1;
	        						if(vm.variants[ind].pqty >= 0){
	        							vm.totalpending -= 1;
	        						}
	        						vm.total += 1;
	        						variantfound = false;
	        					}
	        				});
	        				if(variantfound == true){
	        					vm.variants.push({'lineid': 0, 'qty': 0, 'variant': cotainerData.VariantCode, 'description': cotainerData.Description, 'sqty' : 1, 'pqty': 0});
	        				}
	        			}
	        		}
        		});
	    		// vm.items = [];
	    		// alert('Outbound shipment has been created, and are ready to depart.')
	    	}
	    	else{
	    		alert('An error occured, please try again');
	    	}
	    },
	    createOutboundResponse(response){
	    	var vm = this;
	    	if(response.error == false){
	    		vm.items = [];
	    		window.location = '/wms/index.php?nid=1240&id='+response.data;
	    		// window.location = window.location;
	    		return;
	    		alert('Outbound shipment has been created, and are ready to depart.')
	    	}
	    	else{
	    		alert('An error occured, please try again');
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			     	return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});
