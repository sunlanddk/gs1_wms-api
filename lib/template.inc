<?php

    require_once 'lib/table.inc' ;

    // Copy objects from template project
    function templateCopyObject (&$project, $t, &$row, $parentid) {
	// project	where to copy object
	// t		timestamp used
	// row		the template object
	// parentid	the parrent of the object copied
	global $User ;
	unset ($row['TemplateCreate']) ;
	unset ($row['TemplateSelect']) ;
	unset ($row['Ready']) ;
	unset ($row['ReadyDate']) ;
	unset ($row['ReadyUserId']) ;
	unset ($row['Done']) ;
	unset ($row['DoneDate']) ;
	unset ($row['DoneUserId']) ;
	unset ($row['RefId']) ;
	$row['ProjectId'] = $project['Id'] ;
	$row['TemplateObjectId'] = $row['Id'] ;
	$row['ParentId'] = $parentid ;
	$row['UserId'] = $User['Id'] ;
	$row['StartDate'] = dbDateEncode ($t+60*60*24*14) ;
	$row['EndDate'] = dbDateEncode ($t+60*60*24*30) ;
	$newid = tableCreate ('Object', $row) ;

	// Create permissions
	$query = sprintf ("SELECT ObjectPermission.*, Role.Id AS NewRoleId FROM ObjectPermission, Role WHERE ObjectPermission.Active=1 AND ObjectPermission.ObjectId=%d AND ObjectPermission.RoleId=Role.TemplateRoleId", $row['Id']) ;
	$permresult = dbQuery ($query) ;
	while ($permrow = dbFetch($permresult)) {
	    $permrow['ObjectId'] = $newid ;
	    $permrow['RoleId'] = $permrow['NewRoleId'] ;
	    unset ($permrow['NewRoleId']) ;
	    tableCreate ('ObjectPermission', $permrow) ;
	}
	dbQueryFree ($permresult) ;

	return $newid ;
    }

    // Create objects flaged for creation
    function templateCopySubTree (&$project, $t, $templateid=0, $id=0, $level=0) {
	if ($level > 10) return sprintf ("%s(%d), object create depth exceded, level %d, id %d", __FILE__, __LINE__, $level, $templateid) ;
	$query = sprintf ("SELECT Object.* FROM Object, ObjectType WHERE Object.Active=1 AND Object.ProjectId=%d AND Object.ParentId=%d AND (Object.TemplateCreate=1 OR Object.ParentId=0) AND Object.ObjectTypeId=ObjectType.Id AND ObjectType.TemplateCreate=1", $project['TemplateProjectId'], $templateid) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch($result)) {
	    // Copy the object
	    $newid = templateCopyObject ($project, $t, $row, $id) ;
	     
	    // Create sub-objects
	    templateCopySubTree ($project, $t, $row['Id'], $newid, $level+1) ;
	}
	dbQueryFree ($result) ;
    }
?>
