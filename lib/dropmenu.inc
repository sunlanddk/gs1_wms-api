<?php

    printf ("<script type='text/javascript' src='lib/dropmenu.js'></script>\n") ;

    $dropmenuLine = array () ;
    navigationItems ($dropmenuLine, $Navigation["Id"], "d") ;
    if (count($dropmenuLine) > 1) {
	printf ("<div id='dropmenu' class='dropskin' onMouseover='dropmenuHighlight(event,1)' onMouseout='dropmenuHighlight(event,0);dropmenuHide(event)'>\n") ;
        foreach ($dropmenuLine AS $row) {
	    printf ("<div class='dropitem' %s><table><tr><td class=dropimg>%s</td><td class=dropfield>%s</td></tr></table></div>\n", htmlOnClick("dropmenuClick", $row), ($row["Icon"] != "") ? "<img src='image/toolbar/".$row["Icon"]."'>" : "", $row["Name"]) ;
	}
	printf ("</div>") ;
	unset ($row) ;
    }

    function dropmenuTDHeader () {
	global $dropmenuLine ;
	return (count($dropmenuLine) > 0) ? "<td class=listhead width=23>&nbsp;</td>" : "" ;
    }
    
    function dropmenuTDList ($id = 0, $icon = null) {
	global $dropmenuLine ;
	if (count($dropmenuLine) == 0) return "" ;
       	if ($id == 0) return "<td>&nbsp</td>" ;
	    
	$icon = (is_null($icon)) ? "src='image/arow.gif' width=12 height=12" : "src='".$icon."'" ;

	if (count($dropmenuLine) == 1) {
	    return sprintf ("<td class=list><img class=list %s %s style='cursor:pointer;'></td>", $icon, htmlOnClick ("appClick", current($dropmenuLine), $id)) ;
	}

	return sprintf ("<td class=list><img class=list %s onMousedown='dropmenuShow(event, %d)' onMouseout='dropmenuHideDelayed(event)'></td>", $icon, $id) ;
    }
    
?>
