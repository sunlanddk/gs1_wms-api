<?php
   function price_to_float($price) {
      $price_fl_point = preg_replace("/,/", ".", $price);
      return floatval(preg_replace("/^[^0-9\.]/", "", $price_fl_point));
   }

   function float_to_currency($price) {
      return number_format($price, 2, ',', ' ');
   }

?>