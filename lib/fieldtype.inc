<?php
final class FieldType {
	private function __construct(){}
	
	const STRING = 0;
	const DATE = 1;
	const INTEGER = 2;
	const FLOAT = 3;
	const BOOLEAN = 4;
	const SELECT = 5;
}
?>