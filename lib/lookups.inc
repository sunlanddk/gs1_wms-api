<?php
   function getOrderTypes() {
      return getLookupValuesArray('
         SELECT Id, CONCAT(Description," (",Name,")") AS Value
         FROM OrderType WHERE Active=1
         ORDER BY Value
      ');
   }

   function getSalesRef() {
      return getLookupValuesArray('
         SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value
         FROM Company, User
         WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND (User.Login=1 OR User.CompanyId=1)
         ORDER BY Value
      ');
   }

   function getOwnerCompanies() {
      return getLookupValuesArray('
         SELECT Company.Id, Company.Name AS Value
         FROM Company
         WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1
         ORDER BY Value
      ');
   }

   function getUserCompanies($userId, $companyId, $isSalesRef) {
      Global $User;

/*
      return getLookupValuesArray(sprintf('
         SELECT Company.Id, Company.Name AS Value
         FROM (usercompanies, company)
         WHERE usercompanies.userid=%d
            AND company.id=usercompanies.companyid
            AND Internal=1
            And Company.Active=1
         ORDER BY Name
      ', $userId));
*/
      $query = sprintf('
         SELECT  Company.Id, Company.Name AS Value
         FROM Company
         WHERE
	        Company.TypeCustomer = 1
	        AND Company.ListHidden = 0
	        AND Company.PaymentStatusId < 4
	        AND Company.Active = 1
	        AND Company.ToCompanyId=%d
      ', $companyId);

/* Temp solution untill select from all possibility added */
      if ($isSalesRef) {
		if ($User['AgentManager']) {
			$query .= sprintf(" AND Company.agentid=%d", $User['AgentCompanyId']);
		} else {
			$query .= sprintf(" AND Company.salesuserid=%d", $userId);
		}
  	  } 

      $query .= " ORDER BY Value";
      return getLookupValuesArray($query);
   }

   function getCurrencies() {
      return getLookupValuesArray('
         SELECT Id, CONCAT(Description," (",Name,")") AS Value
         FROM Currency
         WHERE Active=1
         ORDER BY Value
      ');
   }
   function getPaymentStates() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM PaymentStatus
         ORDER BY Id
      ');
   }
   function getCustomerCategory() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM CustomerCategory
         ORDER BY Id
      ');
   }
   function getCollectionType() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM CollectionType
         ORDER BY Id
      ');
   }

   function getCollectionCurrencies($ids) {
      $_ids = count($ids) > 0 ? implode(",", $ids) : '0';
      return getLookupValuesArray(sprintf('
         SELECT DISTINCT c.Id, CONCAT(c.Description," (",c.Name,")") AS Value
         FROM collectionmemberprice cm
         LEFT JOIN currency c on c.Id = cm.CurrencyId
         WHERE c.Active = 1 AND cm.collectionmemberid in (%s) 
         ORDER BY Value
      ', $_ids));
//         WHERE c.Active = 1 AND cm.collectionmemberid in (%s) AND cm.WholesalePrice > 0
   }

   function getSeasonCollectionCurrencies($company_id) {
      $_ids = count($ids) > 0 ? implode(",", $ids) : '0';
      return getLookupValuesArray(sprintf('
         SELECT DISTINCT cur.Id, CONCAT(cur.Description," (",cur.Name,")") AS Value
         FROM season s
		 left join `collection` c on c.SeasonId = s.Id
		 left join `collectionmember` cm on cm.CollectionId = c.Id
		 left join `collectionmemberprice` cmp on cmp.CollectionMemberId=cm.Id
         LEFT JOIN currency cur on cur.Id = cmp.CurrencyId

         WHERE cur.Active = 1 AND cur.NoSale=0 AND s.Active=1 and s.Done=0 and s.OwnerId=%d AND cm.Active=1 AND cm.Cancel=0 
         ORDER BY Value
      ', $company_id));
   }
   
   function getCompaniesWithCurrency($userId, $companyId, $isSalesRef){
	   	$query = sprintf('SELECT Id, CurrencyId, Name FROM Company WHERE TypeCustomer=1 AND ListHidden=0 AND Active=1 AND ToCompanyId=%d', $companyId);
	   	if ($isSalesRef) {
	   		$query .= sprintf(" AND SalesUserId=%d", $userId);
	   	}
	   	$query .= " ORDER BY Name";
	   	$values = array();
	   	$res  = dbQuery ($query);
	   	$num_rows = mysql_num_rows($res);
	   	if($num_rows>0){
	   		array_push($values, array('value' => '', 'text' => '- select -', 'currency' => ''));
	   		while ($row = dbFetch($res)) {
	   			array_push($values, array('value' => $row['Id'], 'text' => $row['Name'], 'currency' => $row['CurrencyId']));
	   		}
	   	} else {
	   		array_push($values, array('value' => '', 'text' => '- no choise -', 'currency' => ''));
	   	}
	   	dbQueryFree ($res) ;
	   	return $values;
   }

   function getCurrenciesArray() {
      $query = '
         SELECT *
         FROM Currency
         WHERE Active=1
         ORDER BY Name
      ';
      $result = array();
      $res    = dbQuery ($query);
      $num_rows = mysql_num_rows($res);
      if($num_rows>0){
         while ($row = dbFetch($res)) {
            $result[$row['Id']] = $row;
         }
      }

      return $result;
   }

   function getInvoices($companyId) {
      return getLookupValuesArray(sprintf('
         SELECT Id, CONCAT(Number, " - ", Description) AS Value
         FROM Invoice WHERE Active=1 AND Credit=0 AND Done=1 and CompanyId=%d
         ORDER BY DoneDate DESC
         LIMIT 0, 10
      ', $companyId));
   }

   function getPaymentTerms() {
      return getLookupValuesArray('
         SELECT Id, CONCAT(Description," (",Name,")") AS Value
         FROM PaymentTerm WHERE Active=1
         ORDER BY Value
      ');
   }

   function getDeliveryLOTs($seasonID, $maxDate) {
      return getLookupValuesArray(sprintf('
         SELECT Id, CONCAT(Name," (",Date,")") AS Value
         FROM CollectionLOT WHERE Active=1 AND SeasonID = %d AND Date >= \'%s\'
         ORDER BY Date
      ', $seasonID, $maxDate));
   }

   function getDeliveryLOTsArray($seasonID) {
      $query = sprintf('
         SELECT Id, Name, DATE(`Date`) as Date
         FROM CollectionLOT WHERE Active=1 AND SeasonID = %d AND Date >= CURDATE() 
         ORDER BY Date
      ', $seasonID);

      $result = array();
      $res    = dbQuery ($query);
      $num_rows = mysql_num_rows($res);
      if($num_rows>0){
         while ($row = dbFetch($res)) {
            $result[$row['Id']] = $row;
         }
      }

      return $result;
   }

   function getDeliveryTerms() {
      return getLookupValuesArray('
         SELECT Id, CONCAT(Name," (",Description,")") AS Value
         FROM DeliveryTerm WHERE Active=1
         ORDER BY Name
      ');
   }

   function getCarriers() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM Carrier WHERE Active=1
         ORDER BY Name
      ');
   }

   function getCountries($_agentcompanyid) {
	   if ($_agentcompanyid>0) {
		  return getLookupValuesArray('
			 SELECT Id, CONCAT(Name," - ",Description) AS Value
			 FROM Country WHERE Active=1 and INSTR(Name, "+")=0  and Id in (select countryid from company where active=1 and agentid=' . $_agentcompanyid . ' group by agentid,countryid) 
			 ORDER BY Value
		  ');
	   } else {
		  return getLookupValuesArray('
			 SELECT Id, CONCAT(Name," - ",Description) AS Value
			 FROM Country WHERE Active=1
			 ORDER BY Value
		  ');
	   }
   }

   function getClaimTypes() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM ClaimType
         ORDER BY Name
      ');
   }

   function getClaimStates() {
      return getLookupValuesArray('
         SELECT Id, Name AS Value
         FROM ClaimState
         ORDER BY Name
      ');
   }

   function getArticleCategories($company_id){
//logPrintTime ('point 2 sex') ;
		$query = sprintf("		
			select acg.Id as Id, acg.name as Value from season s
			left join `collection` c on c.SeasonId = s.Id
			left join `collectionmember` m on m.CollectionId = c.Id
			left join `article` a on a.Id = m.ArticleId
			left join `articlecategory` cat on cat.Category=substring(a.Number,2,3) and cat.articletypeid=a.articletypeid
            left join articlecategorygroup acg on acg.id=cat.ArticleCategoryGroupId
			where
			a.Active=1 and s.Active=1 and s.Done=0 and c.Active=1 AND m.Active=1 and m.Cancel=0 and s.OwnerId=%d 
			group by ArticleCategoryGroupId
			order by DisplayOrder", $company_id) ;
//			left join `collectionmemberprice` cmp on cmp.CollectionMemberId=m.Id
//		$query = "select Id as Id, Name as Value from `articlecategorygroup` order by DisplayOrder";
		
		return getLookupValuesArray($query);
	}

	function getArticleCategory($id) {
	   $query = sprintf("select id, name, ArticleTypeId, ArticleCategoryGroupId, TopBottom from `articlecategory` where id=%d", $id);
	   $res    = dbQuery ($query);
	   $_cat = dbFetch($res);
	   dbQueryFree ($res);
	   return $_cat;
	}

	function getCategoriesGroupArray() {
	   $query = "select Id as Id, Name as Value from `articlecategorygroup` order by DisplayOrder";
	   $result = array();
	   $res    = dbQuery ($query);
	   $num_rows = mysql_num_rows($res);
	   if($num_rows>0){
	      while ($row = dbFetch($res)) {
	         $result[$row['Id']] = array(
               'Name' => $row['Value'],
               'pcs'  => 0,
               'qty'  => 0
             );
	      }
	   }

	   return $result;
	}

	function getArticleSex(){
		$query = "select Id as Id, Name as Value from `sex` order by Name";
		return getLookupValuesArray($query);
	}

	function getOwnerSeasons($company_id){
	    global $User;
		if ($User['Extern']) {
			$company_id = TableGetField ('Company','ToCompanyId', $company_id) ;
		}
		$query = sprintf("select Id as Id, Name as Value from `season` where Active=1 and Done=0 and OwnerId=%d order by Name", $company_id);
		return getLookupValuesArray($query);
	}
	
	function getFilterOwnerSeasons($company_id, $allowuserid=0){
	    global $User;
		if ($User['Extern'] ) {
			$_agentid = (int)tableGetField('Company','AgentId',(int)$User['CompanyId']) ;
			if ($_agentid>0) {
				$_salesuserid = (int)tableGetField('Company','SalesUserId',(int)$User['CompanyId']) ;
				if ($_salesuserid>0) {
					$allowuserid=$_salesuserid;
				}
			}
		}
		$query = sprintf("select `season`.Id as Id, concat(`season`.Name) as Value 
							from `season` 
							left join `user` u on u.id=%d and u.Active=1
							left join `seasonallowuser` sau on sau.seasonid=`season`.id and sau.userid=u.id and sau.active=1
							where `season`.Active=1 and `season`.Done=0 and `season`.OwnerId=%d and `season`.ShowOnB2B=1", (int)$allowuserid, (int)$company_id); 
							
		if ($User['Extern']) {
			$query .= ' and (SELECT count(id) - sum(NoRetail) as SomeRetail from collectionlot where active=1 and seasonid=season.id group by seasonid)>0'; 
//			$query .= ' and (`season`.RestrictAccess=0 or (`season`.RestrictAccess=1 and sau.id>0))'; 
			$query .= ' and (u.agentcompanyid=0 or (`season`.RestrictAccess=0 or (`season`.RestrictAccess=1 and sau.id>0)))'; 
		} else {
			$query .= ' and (u.agentcompanyid=0 or (`season`.RestrictAccess=0 or (`season`.RestrictAccess=1 and sau.id>0)))'; 
		}
		$query .= ' order by `season`.Name' ;
//echo $query ;							
							
		return getLookupValuesArray($query);
	}

	function getSeasons($company_id){
		$query = sprintf("select Id as Id, Name as Value from `season` where Active=1 and Done=0 order by Name");
		return getLookupValuesArray($query);
	}

	function getBrands(){
		$query = "select Id as Id, Name as Value from `brand` where Active=1 order by Name";
		return getLookupValuesArray($query);
	}

	function getPrograms($seasonId){
	   $_season = null;
	   if (isset($seasonId) && $seasonId > 0) {
	       $_season = sprintf("p.SeasonId = %d", $seasonId);
	   }
	   $query = sprintf("select p.Id as Id, concat(p.Name,' (',s.Name,')') as Value from `program` p, season s where p.Active=1 and s.id=p.seasonid %s ORDER BY p.Name", empty($_season) ? "" : " AND ".$_season);
		return getLookupValuesArray($query);
	}
	function getProgramNames($seasonId) {
//logPrintTime ('point 2 getArticleCategories') ;
	   $_season = null;
	   if (isset($seasonId) && $seasonId > 0) {
	       $_season = sprintf("p.SeasonId = %d", $seasonId);
	   }
	   $query = sprintf("select p.Name as Id, p.Name as Value from `program` p, season s where p.Active=1 and s.id=p.seasonid and s.done=0 and s.ShowOnB2B=1 %s group by p.Name ORDER BY p.Name ", empty($_season) ? "" : " AND ".$_season);
		return getLookupValuesArray($query);
	}
	function getCOllectionNames($seasonId, $company_id) {
//logPrintTime ('point 2 getCollections') ;
	   $_season = null;
	   if (isset($seasonId) && $seasonId > 0) {
	       $_season = sprintf("c.SeasonId = %d", $seasonId);
	   } else {
			$_season = "c.SeasonId=0" ;
		}
	   $query = sprintf("select c.Name as Id, c.Name as Value from `collection` c, season s 
				where c.Active=1 and s.id=c.seasonid  and s.OwnerId=%d and s.done=0 and s.active=1 and s.ShowOnB2B=1 %s 
				group by c.Name ORDER BY c.Name ", $company_id, empty($_season) ? "" : " AND ".$_season);
		return getLookupValuesArray($query);
	}

   function getLookupValuesArray($query='') {
      $values = array(
         "" => '- select -'
      );
      $res  = dbQuery ($query);
      $num_rows = mysql_num_rows($res);
      if($num_rows>0){
	      while ($row = dbFetch($res)) {
	         $values[$row['Id']] = $row['Value'];
	      }
      } else {
      	$values = array("" => '- no choise -');
      }

      dbQueryFree ($res) ;

      return $values;
   }
   function getLookupValuesArrayNone($query='') {
      $values[0] = '- none -' ;
      $res  = dbQuery ($query);
      $num_rows = mysql_num_rows($res);
      if($num_rows>0){
	      while ($row = dbFetch($res)) {
	         $values[$row['Id']] = $row['Value'];
	      }
      } else {
      	$values = array("" => '- no choise -');
      }

      dbQueryFree ($res) ;

      return $values;
   }

   function getSalesResponsible($companyId=0, $onlyActive=true) {
            Global $User;
   		$query = sprintf(
   		   "SELECT u.Id, CONCAT(u.FirstName, ' ', u.LastName) AS Value
   		    FROM `User` u
   			WHERE
   		       (u.CompanyId=%d OR (u.Id IN
   		          (SELECT UserId FROM UserCompanies WHERE CompanyId=%d AND Active=1)
		       )) %s %s
   		    ORDER BY Value", $companyId, $companyId, ($onlyActive ? "AND u.Active=1" : ""), ($User['AgentManager']>0 ? sprintf('and u.AgentCompanyId=%d ', $User['AgentCompanyId']) : ''));
   		return getLookupValuesArray($query);
   }

?>