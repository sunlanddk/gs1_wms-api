import Main from './components/Main.vue'
import Test from './components/Test.vue'
import User from './components/user/User.vue'
import ScanProducts from './components/inventory/ScanProducts.vue'
import Items from './components/statistics/Items.vue'
import { store } from './store/store';
import * as mylib from './translations.js'

let lang = store.state.lang.name;
let translations = mylib.translateStrings(lang);


export const routes = {
	mode: 'history',
	scrollBehavior() {
    	return { x: 0, y: 0 };
  	},
	routes: [
	 //   { 
	 //    	path: mylib.translatePath('home'), 
	 //    	component: Main,
	 //    	name: 'Main',
	 //    	meta: {
  //     			title: 'Main'
  //     		}
		// },
		// { 
	 //    	path: mylib.translatePath('test'), 
	 //    	component: Test ,
	 //    	name: 'Test',
	 //    	meta: {
  //     			title: 'Test'
  //     		}
		// },
		// { 
	 //    	path: mylib.translatePath('user'), 
	 //    	component: User ,
	 //    	name: 'User',
	 //    	meta: {
  //     			title: 'User'
  //     		}
		// },
		{ 
	    	path: mylib.translatePath('scanproducts'), 
	    	component: ScanProducts ,
	    	name: 'Scan products',
	    	meta: {
      			title: 'Scan products'
      		}
		},
		{ 
	    	path: mylib.translatePath('items'), 
	    	component: Items ,
	    	name: 'Items',
	    	meta: {
      			title: 'Items'
      		}
		},
		{ 
	    	path: '*', 
	    	component: ScanProducts,
	    	name: 'Scan products',
	    	meta: {
      			title: 'Scan products'
      		}
		},
	  ]
};
