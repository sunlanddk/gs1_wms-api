import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueRouterOdd from 'vue-router'
import { routes } from './routes'
import VueMeta from 'vue-meta';
import { store } from './store/store';
import * as mylib from './translations.js'
import * as routesfile from './routes.js';

Vue.use(VueRouter);

Vue.mixin({
  methods: {
    globalHelper: function () {
      	let vm = this;
		vm.$store.dispatch('addProducts', []);
		vm.products = [];
    },
    reloadPage: function () {
      	location.reload();
    },
  },
});

const router = new VueRouter(routesfile.routes);

export const EventBus = new Vue(); // added line

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
