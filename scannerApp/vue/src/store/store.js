import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import { getField, updateField } from 'vuex-map-fields';

Vue.use(Vuex);


export const store = new Vuex.Store({
	plugins: [createPersistedState()],
	strict: true,
	state: {
		// apiurl: 'https://shop.gs1.dk/api/public/', // Live server
		// apiurl: 'http://185.21.41.236/wms/s4/public/', // test server
		apiurl: 'http://wms7.gs1.dk/public/', // local server
		lang: {lang:'en-us', 'name': 'en'}, 
		preloader: 0,
		opacity: 0,
		modalshow: 0,
		modalmessage: '',
		modalhtml: '',
		token: '',
		routername: '',
		activeuser: {
			name: 'Anonymous',
			id: 0,
		},
		cart: {
			time: '',
			items: [],
			previousitems: []
		}
	},
	getters: {
		getField,
		getToken: state => {
			return state.token;
		},
		getPrealoder: state => {
			return state.preloader;
		},
		getRoutername: state => {
			return state.routername;
		},
	},
	mutations: {
		updateField,
		setUrl: (state, url ) => {
			state.apiurl = url;	
		},
		setToken: (state, token ) => {
			state.token = token;	
		},
		setPreloader: (state, value ) => {
			state.preloader = value;	
		},
		setModal: (state, value ) => {
			state.modalshow = value.show;	
			state.modalmessage = value.message;
			state.modalhtml = value.html;
		},
		setUser: (state, value) => {
			state.activeuser.name = value.name;	
			state.activeuser.id = value.id;
		},
		setMutationRouterName: (state, name ) => {
			state.routername = name;
		},
		addProducts: (state, value) => {
			state.cart.items = value;	
		},
	    addPreviousProduct: (state, value) => {
	      	state.cart.previousitems.push({
	        	gtin 		: value.gtin,
				id 			: value.id,
				new 		: 0,
				attributes 	: value.attributes,
				position 	: value.position,
				error 		: value.error
	      	});
	    },
	    resetPreviousProducts: (state, value) => {
	    	let itemLength = state.cart.previousitems.length;
	    	state.cart.previousitems.splice(0, itemLength);
	    },
	    addProduct: (state, value) => {
	      	state.cart.items.push({
	        	gtin 		: value.gtin,
				id 			: value.id,
				new 		: value.new,
				attributes 	: value.attributes,
				position 	: value.position,
				error 		: value.error
	      	});
	    },
	    updateProduct: (state, value) => {
	    	state.cart.items.forEach(function(e, i){
	    		if(e.gtin == value.gtin){
	    			state.cart.items[i].id = value.id;
	    			state.cart.items[i].new = value.new;
	    			state.cart.items[i].attributes = value.attributes;
	    			state.cart.items[i].position = value.position;
	    			state.cart.items[i].error = value.error;
	    			return;
	    		}
			});
	    },
	    resetProducts: (state, value) => {
	    	let itemLength = state.cart.items.length;
	    	state.cart.items.splice(0, itemLength);
	    	
	    	// state.cart.items = [];
	    },
	    removeProduct: (state, value) => {
	    	state.cart.items.forEach(function(e, i){
	    		if(e.gtin == value){
	    			state.cart.items.splice(i, 1);	
	    			return;
	    		}
			});
	    },
		// setMutationCompany: (state, company ) => {
		// 	if (Object.prototype.hasOwnProperty.call(company, 'id')){
		// 		state.company.id = company.id;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'admin')){
		// 		state.company.admin = company.admin;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'biname')){
		// 		state.company.biname = company.biname;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'contactid')){
		// 		state.company.contactid = company.contactid;
		// 	}

		// 	if (Object.prototype.hasOwnProperty.call(company, 'cvr')){
		// 		state.company.cvr = company.cvr;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'cvrcountry')){
		// 		state.company.cvrcountry = company.cvrcountry;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'name')){
		// 		state.company.name = company.name;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'address1')){
		// 		state.company.address1 = company.address1;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'address2')){
		// 		state.company.address2 = company.address2;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'zip')){
		// 		state.company.zip = company.zip;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'city')){
		// 		state.company.city = company.city;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'turnover')){
		// 		state.company.turnover = company.turnover;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'b1')){
		// 		state.company.b1 = company.b1;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'b2')){
		// 		state.company.b2 = company.b2;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'b3')){
		// 		state.company.b3 = company.b3;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'industry')){
		// 		state.company.b3 = company.b3;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'electronicinvoice')){
		// 		state.company.electronicinvoice = company.electronicinvoice;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'electronicemail')){
		// 		state.company.electronicemail = company.electronicemail;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'gln')){
		// 		state.company.gln = company.gln;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'cvrchecked')){
		// 		state.company.cvrchecked = company.cvrchecked;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'lockall')){
		// 		state.company.lockall = company.lockall;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'locksome')){
		// 		state.company.locksome = company.locksome;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'phone')){
		// 		state.company.phone = company.phone;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'contacts')){
		// 		state.company.contacts = company.contacts;
		// 	}
		// 	if (Object.prototype.hasOwnProperty.call(company, 'fda')){
		// 		state.company.fda.active = company.fda.active;
		// 		state.company.fda.name = company.fda.name;
		// 		state.company.fda.email = company.fda.email;
		// 	}

		// },
		// addMore(state) {
	 //      state.newcompany.contacts.push({
	 //        email: "",
	 //        name: ""
	 //      });
	 //    },

	},
	actions: {
		setRouteName({ commit }, name) {
		  commit("setMutationRouterName", name);
		},
		setUrl({ commit }, url) {
		  commit("setUrl", url);
		},
		setToken({ commit }, token) {
		  commit("setToken", token);
		},
		setPreloader({ commit }, value) {
		  commit("setPreloader", value);
		},
		setModal({ commit }, value) {
		  commit("setModal", value);
		},
		setUser({ commit }, value) {
		  commit("setUser", value);
		},
		addProducts({ commit }, value) {
		  commit("addProducts", value);
		},
		addPreviousProducts({ commit }, value) {
		  commit("addPreviousProducts", value);
		}
	}
});