
function showArticle(articleId, colorId, seasonId, nid, vnid, reload){
	var urlParams       = {
	   articleId : articleId,
	   colorId   : colorId,
	   seasonId  : seasonId,
	   listnvid  : nid,
	   reload    : reload != null ? reload : false
	};
	
	$.address.history(false);
	appLoadAjaxBody(vnid, null, null, ".item-panel", urlParams);
	$.address.history(true);
}