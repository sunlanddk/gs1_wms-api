$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function() {
	tableManager.init();
});

var tableManager = {
   //After initialization this property is set with current 'nid' value
   nid: 0,  
   
	//initializing listeners, setting 'nid' value 
	init : function() {
	   tableManager.nid = $('input[name="nid"]').val();
	   
		tableManager.initFilterBarListeners();
		
		//Set listeners to table headers (for sorting purposes)
		$('table.orderTable th').click(function(event){
			var sortDir = $(this).attr('sort'),
			    sortParam = $(this).attr('sortId');
			
			if(sortDir == 'ASC') {
				sortParam = - sortParam;
			}
			$.address.history(false);
			appLoadAjaxBody(tableManager.nid, null, null, null, {sort: sortParam});
			$.address.history(true);
		});
	},
	
	initFilterBarListeners : function() {
		$('#filterform').submit(function() {
			$('#filterform input:text').each(function() {
				if ($(this).val() === "") {
					$(this).attr('name', '');
				}
			});
		});
		$('#filterform').keypress(function(event) {
			if(event.which==13) {
				event.preventDefault();
	         if ($('#submit').length > 0) {
	            $('#submit').click();
	         } else {
	            $('#filterform').submit();
	         }
			}
		});

		$('.datepicker').datepicker( {
			dateFormat : 'yy-mm-dd'
		});

		$('#filterform input.integer').each(function() {
			maskElement(this, isInteger);
		});

		$('#filterform input.float').each(function() {
			maskElement(this, isFloat);
		});

		$('#reset').click(function() {
			$('#filterform input:text').val('');
			$('#filterform select').prop('selectedIndex', 0);
			$('#filterform #clearFilter').val("1");
			if ($('#submit').length > 0) {
			   $('#submit').click();
			} else {
			   $('#filterform').submit();
			}
		});
		
		//Handling of pressing "Submit button".
		$('#submit').click(function() {
			appLoadAjaxBody(
		      tableManager.nid, 
		      null,
		      null, 
		      null, 
		      {
		         filter : $("#filterform").serializeObject()
	         }, 
		      $('#filterform').attr('method')
	      );
		});
	}
};
