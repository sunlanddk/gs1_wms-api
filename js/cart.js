 //temp
  var articles = [
     {
        ArticleId : 20380,  ArticleColorId : 507, Price : 20.00,
 	     Desc : 'Heavy Single, cotton/pes (knittted on Isoli machine)', Number : '2200000200', 
    	  SeasonId : 391, SeasonName : 'Fall 2013 Kids',
    	  //CollectionId : 1, CollectionName : '', CollectionMemberId : 2,
        ColorNumber : '018094000', ColorDesc : 'Golden Brown',
        Sizes: {
            23 : {qty:1, name:''},
            24 : {qty:3, name:''}
        }
     }, 
     {
        ArticleId : 20381, ArticleColorId : 507, Price : 15.50,
        Desc : 'T-shirt with print 6793 and 6794', Number : '2200000200', 
        SeasonId : 2, SeasonName : 'Season #2',
        //CollectionId : 1, CollectionName : '', CollectionMemberId : 2,
        ColorNumber : '018094000', ColorDesc : 'Golden Brown',
        Sizes: {
             23 : {qty:2, name:''},
             24 : {qty:3, name:''}
        }
      },      
      {
         ArticleId : 20382, ArticleColorId : 508,  Price : 4.00,
         Desc : 'Nigth wear dress.Decorated with lace. GCD-Kira style.', Number : '2200000200',
         SeasonId : 391, SeasonName : 'Fall 2013 Kids',
         //CollectionId : 1, CollectionName : '', CollectionMemberId : 2,
         ColorNumber : '018093700', ColorDesc : 'Bronze Brown',
         Sizes: {
              23 : {qty:10, name:''},
              24 : {qty:3, name:''}
         }
       },      
       {
          ArticleId : 20383, ArticleColorId : 517, Price : 5.30,
          Desc : 'Short sleeve top. GCD-Benja style.', Number : '2200000200', 
          SeasonId : 2, SeasonName : 'Season #2',
          //CollectionId : 1, CollectionName : '', CollectionMemberId : 2,
          ColorNumber : '013094100', ColorDesc : 'Banana Cream',
          Sizes: {
               23 : {qty:10, name:''},
               24 : {qty:3, name:''},
               25 : {qty:7, name:''}
          }
        }     
  ];

  function changeCurrency() {
     $.address.history(false);
     appLoadAjaxBody(
         $("#cartCurr").attr("changeCurrency"),
         null,
         null,
         ".item-panel",
         null,
         "POST"
     );
     $.address.history(true);
  }
  
  function  getCartInfo () {
     jQuery.ui.Mask.show("Updating cart info ...");
     var post = $.post(
        baseUrl + "index.php",
        {
           nid    : $("div.shoppingCartPlace").attr("nid")
        },
        function(data, textStatus, jqXHR) {
        },
        'json'
     )
     .done(function(data){
        $("#cartStyles").text(data.styles);
        $("#cartTops").text(data.tops_perc);
        $("#cartBottoms").text(data.bottoms_perc);
        $("#cartSeasons").text(data.seasons);
        $("#cartPcs").text(data.pcs);
        $("#cartSumm").text(data.summ);
        $("#cartCurr").text(data.currency);
     })
     .always(function(){
       jQuery.ui.Mask.hide();   
     });
  }
  
  
$(document).ready(function() {
   $("#addtocart").click(function( event ) {      
      var i = Math.floor(Math.random() * articles.length);
      
      jQuery.ui.Mask.show("Adding to cart ...");
      var post = $.post(
         baseUrl + "index.php",
         {
            nid            : $(this).attr("nid"),
            articleInfo    : $.toJSON( articles[i])  
         },
         function(data, textStatus, jqXHR) {
         },
         'json'
 	   )
 	   .done(function(data){
 	      $("#cartStyles").text(data.styles);
 	      $("#cartSeasons").text(data.seasons);
 	      $("#cartPcs").text(data.pcs);
 	      $("#cartSumm").text(data.summ);
 	   })
 	   .always(function(){
 	     jQuery.ui.Mask.hide();   
 	   });
      
   });   
});