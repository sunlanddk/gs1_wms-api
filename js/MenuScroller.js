// MenuScroller has dependency to jQuery

var MenuScroller = function ($container) {
    this.leftControl = $container.find('.scroller-control-left')[0];
    this.rightControl = $container.find('.scroller-control-right')[0];
    this.$controls = $container.find('.scroller-control-left, .scroller-control-right');
    this.list = $container.find('.scroller-list ul:visible')[0];
    
    this.menuShiftAtTheEnd = this._getMenuShiftAtTheEnd();
    
    this._init();
};

MenuScroller.constants = {
	MENU_SHIFT : 200,
	MENU_STATE_AT_THE_START : 1,
	MENU_STATE_IN_THE_MIDDLE : 2,
	MENU_STATE_AT_THE_END : 3
};

MenuScroller.prototype._init = function() {
	if (this._hasOverflow()) {
		this._initControlListeners();
		this._refreshControlsState();
	} else {
		this._hideControls();
	}
};

MenuScroller.prototype.scrollMenuToElement = function($element) {
	var elementLeftOffset = $element.offset().left,
		parentLeftOffset = $element.parent().offset().left,
		elementRelativeOffset = elementLeftOffset - parentLeftOffset;
	
	if (this.menuShiftAtTheEnd < elementRelativeOffset) {
		this._scrollMenu(this.menuShiftAtTheEnd)
	} else {
		this._scrollMenu(elementRelativeOffset);
	}
};

MenuScroller.prototype._scrollMenu = function(menuShift) {
	var me = this;
	
	MenuScroller.utils.disableControl(this.leftControl);
	MenuScroller.utils.disableControl(this.rightControl);
	
	var animationConfig = {
	    left : -1 * menuShift
	};
	
	$(me.list).animate(animationConfig, 500, function() {
		me._refreshControlsState();
	});
};

MenuScroller.prototype._getMenuShiftAtTheEnd = function() {
	var menuScrollWidth = MenuScroller.utils.getCrossbrowserScrollWidth(this.list);
	return menuScrollWidth - this.list.parentNode.clientWidth;
};

MenuScroller.prototype._hasOverflow = function() {
	
	return this.menuShiftAtTheEnd > 0;
};

MenuScroller.prototype._hideControls = function() {
	this.$controls.hide();
};

MenuScroller.prototype._initControlListeners = function() {
	var me = this;
	
	me.$controls.click(function() {
		if (this.isDisabled) {
			return;
		}
		
		var nextMenuShift = me._calculateNewMenuShift(this);
		me._scrollMenu(nextMenuShift);
	});
};

MenuScroller.prototype._calculateNewMenuShift = function(scroller) {
	var currentMenuShift = this._getCurrentMenuShift(),
		nextMenuShift;
	
	if (scroller === this.leftControl) {
		nextMenuShift = currentMenuShift - MenuScroller.constants.MENU_SHIFT;
	} else {
		nextMenuShift = currentMenuShift + MenuScroller.constants.MENU_SHIFT
	}
	
	if (nextMenuShift < 0) {
		nextMenuShift = 0;
	} else if (nextMenuShift > this.menuShiftAtTheEnd) {
		nextMenuShift = this.menuShiftAtTheEnd;
	}
	
	return nextMenuShift;
};

MenuScroller.prototype._refreshControlsState = function() {
	MenuScroller.utils.enableControl(this.leftControl);
	MenuScroller.utils.enableControl(this.rightControl);
	
	var menuState = this._getCurrentMenuState();
	
	if (menuState == MenuScroller.constants.MENU_STATE_AT_THE_START) {
		MenuScroller.utils.disableControl(this.leftControl);
	} else if (menuState == MenuScroller.constants.MENU_STATE_AT_THE_END) {
		MenuScroller.utils.disableControl(this.rightControl);
	}
};

MenuScroller.prototype._getCurrentMenuState = function() {
	var currentMenuShift = this._getCurrentMenuShift(),
	    currentMenuState;
	
	if (currentMenuShift <= 0) {
		currentMenuState = MenuScroller.constants.MENU_STATE_AT_THE_START;
	} else if (currentMenuShift >= this.menuShiftAtTheEnd) {
		currentMenuState = MenuScroller.constants.MENU_STATE_AT_THE_END;
	} else {
		currentMenuState = MenuScroller.constants.MENU_STATE_IN_THE_MIDDLE;
	}
	
	return currentMenuState;
};

MenuScroller.prototype._getCurrentMenuShift = function() {
	var negativeMenuShift = parseInt(this.list.style.left) || 0;
	return -1 * negativeMenuShift;
};


MenuScroller.utils = {
	disableControl : function(control) {
		control.isDisabled = true;
		$(control).addClass('scroller-control-disabled');
	},
	
	enableControl : function(control) {
		control.isDisabled = false;
		$(control).removeClass('scroller-control-disabled');
	},
	
	// http://stackoverflow.com/questions/14666931/why-scrollwidth-doesnt-work-in-this-case-in-firefox
	getCrossbrowserScrollWidth : function(element) {
		var scrollWidth = $(element).css("overflow", "hidden")[0].scrollWidth;
		$(element).css("overflow", "visible");
		
		return scrollWidth;
	}
};