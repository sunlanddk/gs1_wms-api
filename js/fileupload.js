function uploadFile(){
	if($('#file').val()=="") return;
	if(typeof window.FormData === "undefined"){
		var iframe = $("#upload_iframe");
		if (iframe.length==0){
			iframe = $(document.createElement("IFRAME"));
			iframe.css({"width": "0px", "height": "0px", "border": "0"});
			iframe.prop("id", 'upload_iframe');
			iframe.prop("name", 'upload_iframe');
			$('body').append(iframe);
			iframe[0].onload = function(){
				$('#uploadresult').html(iframe[0].contentWindow.document.body.innerHTML);
			};
			$('#uploadform').prop("target", 'upload_iframe');
		}
		$('#uploadform').submit();
	}else{
		var formData = new FormData($('#uploadform')[0]);
		$.ajax({
	        url: 'index.php',
	        type: 'POST',
	        success: function(data){
	        	$('#uploadresult').html(data);
	        },
	        data: formData,
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	}
}

function printResult(){
	if($('#uploadresult').html() != ''){
		var win = window.open();
		win.document.write(
			"<!DOCTYPE html>" + 
			"<html>" +
				"<head>" +
					"<title>XML Processing Results</title>" + 
					"<link href='layout/new/style.css' rel='stylesheet' type='text/css'>" +
					"<style type='text/css'>" +
						"@media print {" +
							"input {" +
								"display: none;" +
							"}" +
						"}" +
					"</style>" +
				"</head>" +
				"<body>" +
					"<input type='button' value='Print' onclick='window.print();'/>" +
					"<div>" +
						document.getElementById('uploadresult').innerHTML +
					"</div>" +
				"</body>" +
			"</html>"
		);
		win.document.close();
		win.focus();
	}
}