 <!DOCTYPE html>
  <html>
      <head>
         <title>{block name=title}PassOnTex - {$Navigation["Header"]} [{$Config['Instance']}]{/block}</title>
         <meta http-equiv='Content-Type' content="text/html;charset=ISO-8859-1" />
         <link href='{_LEGACY_URI}/layout/default/styles.css' rel='stylesheet' type='text/css'>
         <link rel="stylesheet" type="text/css" media="print" href="{_LEGACY_URI}/layout/default/print.css">
         <link rel='stylesheet' type='text/css' href='{_LEGACY_URI}/layout/default/list.css'>
         <link rel="stylesheet" type="text/css" href="{_LEGACY_URI}/layout/jquery/css/jquery-ui-1.10.2.min.css">
         <link href='{_LEGACY_URI}/layout/new/style.css' rel='stylesheet' type='text/css'>
         {if $Navigation["LayoutPopup"]}
            <link href='{_LEGACY_URI}/layout/new/popup.css' rel='stylesheet' type='text/css'>
         {/if}
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery-1.9.1.min.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery-ui-1.10.2.min.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery.cookie.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery.ui.Mask.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery.json-2.4.min.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/jquery/jquery.address-1.6.min.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/validation.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/lib/app.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/menuManager.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/toolBar.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/table.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/cart.js'></script>
         <script type='text/javascript' src='{_LEGACY_URI}/js/style.js'></script>
         {literal}
            <script type='text/javascript'>
              var MainMenu    = [{/literal}{$menu}{literal}];
              var ToolMenu    = [{/literal}{$toolmenu}{literal}];
              var defaultNID  = {/literal}{$Navigation["DefaultNID"]|default: {0}}{literal};
              var activeID    = {/literal}{$Navigation["DefaultNID"]|default: {$nid}}{literal};

               $(document).ready(function() {
                   baseUrl = '{/literal}{$Config['base_url']}{literal}';
                   $.cookie.json = true;
            	   menuRenderer.render(MainMenu, "mainNav", {/literal}{if $Navigation["LayoutPopup"]}false{else}true{/if}{literal});
            	   menuRenderer.render(ToolMenu, "subNavigation", false);

            	   $.address.externalChange(function(event) {
            	      var ajaxNID = $.address.parameter('ajaxNID');
            	      if ((ajaxNID != null) && isInteger(ajaxNID)) {
            	         var ajaxID    = $.address.parameter('ajaxID');
            	         var ajaxParam = $.address.parameter('ajaxParam');
            	         //appLoadAjaxBody(ajaxNID, ajaxID, ajaxParam);
            	         menuRenderer.setActive(ajaxNID);
            	      } else {
                  	     if ((defaultNID != null) && isInteger(defaultNID) && defaultNID > 0) {
                  	        $.address.history(false);
                  	        $.address.parameter('ajaxNID', defaultNID);
                  	        $.address.history(true);
                  	     }
            	         menuRenderer.setActive(activeID);
               	      }
            	   });
               });
            </script>
         {/literal}
         <script type='text/javascript' src='{SFMODULE_PATH}/js/pager.js'></script>
         <script type='text/javascript' src='{SFMODULE_PATH}/js/table.js'></script>
         <script type='text/javascript' src='{SFMODULE_PATH}/js/validate.js'></script>

      </head>
   <body onload='appLoaded()'>
   <!--
      id: {$Id}, nid: {$nid}
      NavId: {$Navigation['Id']}
   -->
      <div class="header">
         <div class="mainNav">
            <div class="level-1">
               <ul>
                  <li id="home" {navigation_on_click_link_by_mark mark='homenew'}><a href="#"><img id="logo" height=35px src="{_LEGACY_URI}/layout/new/images/Logo{$User['CustomerCompanyId']}.jpg" alt="Passon logo" /></a></li>
               </ul>
			</div>
			<div class="level-2-bg">
			</div>
	 		{if !$Navigation["LayoutPopup"]}
			<div class="loginLayer" >
			   <div id="loginUser">
					<div class="loggedUserName">{$User["FirstName"]}</div>
					<div class="loggedUserCompany"><a {navigation_on_click_link_by_mark mark='mycomp'}>{$User['CustomerName']}</a></div>
					<div class="SignOut"><a href="#" {navigation_on_click_link_by_mark mark='Logout-cmd'}>Logout</a></div>
			   </div>
		   </div>
		   {/if}
         </div>
      </div>
      <div>
         <div class="content">
            <h1 class="leftCaption">{$htmlHeader}</h1>
            {if !$Navigation["LayoutPopup"] && $Navigation["ShowCart"]}
			<div class="shoppingCartPlace" {navigation_on_click_link_by_mark mark="sales.`$cartMark`.view"} nid='{navigation_mark mark="sales.`$cartMark`.info"}'>
      			<ul>
      				<li>
      					<div class="style"><span id="cartStyles">{$cartInfo['styles']}</span> styles (<span id="cartTops">{$cartInfo['tops_perc']}</span>% / <span id="cartBottoms">{$cartInfo['bottoms_perc']}</span>%)</div>
      					<div class="season">in <span id="cartSeasons">{$cartInfo['seasons']}</span> seasons</div>
      				</li>

      				<li class="cartSum">
      					<div class="style"><span id="cartPcs">{$cartInfo['pcs']}</span> pcs.</div>
      					<div class="season"><span id="cartSumm">{$cartInfo['summ']}</span> <span id="cartCurr" changeCurrency='{navigation_mark mark="sales.ordercart.changecurrency"}'>{$cartInfo['currency']}</span></div>
      				</li>
      			</ul>
			</div>
            <!--  div class="addToCart">
               <input type="button" value="Add To Cart" id="addtocart" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.ordercart.add'}">
            </div-->
            <div style="clear: both;"></div>
            {/if}
		</div>
      </div>
	  <div class="subNavigation">
         <div class="level-1">
            <ul>
               {if ($Error or !$hasToolMenu)}
                  {if $Navigation["LayoutPopup"]}
      			     <li handler='appBack({$Navigation["Back"]})'>
                        <a href="#">Close</a>
                     </li>
      		      {/if}
      			  <li handler='appBack(1)'>
                        <a href="#">Back</a>
                  </li>
               {/if}
   		     </ul>
         </div>
         <div class="level-2-bg" style="display:none;">
         </div>
      </div>

      <div class="main">
        {block name=main}{/block}
