<div class="groupingByForm" style='margin:5px;'>
    {assign "groupingActive" ""}
    {foreach $listField as $key => $item}
        {if !empty($item['groupByLink'])}
            {assign "groupingActive" true}
            {break}
        {/if}
    {/foreach}

    {if $groupingActive}
        <div><b>Group by:</b></div>
        <div>
            <select onchange='javascript:document.location.href=this.value' style="border:1px solid lightgrey;">
                <option value='{str_replace('groupby','ungroup',$smarty.server.REQUEST_URI)}'> -ungroup- </option>
                {foreach $listField as $key => $item}
                    {if !empty($item['groupByLink'])}
                        {$item['groupByLink']}
                    {/if}
                {/foreach}
            </select>
        </div>
    {/if}
</div>