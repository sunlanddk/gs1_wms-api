{extends file="base.html.smarty.tpl"}

{block name=main}

    {include file="reports/xmlview.html.smarty.tpl"}

<script type='text/javascript'>
    $(document).ready(function() {
        var headerText = $(".leftCaption").text();
        menuRenderer.setActive({$activeNid});
        $(".leftCaption").text(headerText);  
    });
</script>

</body>
</html>
{/block}
