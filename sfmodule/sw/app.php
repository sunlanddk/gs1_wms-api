<?php

// list of ips allowed to use POT api
$allowedIpApi = array(
	'127.0.0.1', 
	'::1', 
	'localhost', // self
	'194.255.246.185', // Sunland office
 ); 


$url = $_SERVER['REQUEST_URI'];

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

//if (strpos($url, '/api/')){var_dump($ip);exit;}
if (strpos($url, '/api/') === false OR strpos($url, '/shipping/') === false) {
	include 'legacyCheck.php';
}
else{
	if (!in_array($ip, $allowedIpApi)) {
		echo 'You do not have permission to this route ';
		die();
	}
}



use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../app/bootstrap.php.cache';



$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
