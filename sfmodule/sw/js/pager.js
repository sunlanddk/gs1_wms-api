

function pagerAjaxPaging(offset) {
   $.address.history(false);
   pagerLoadAjaxBody(null, null, null, ".main", {offset : offset});
   $.address.history(true);
}

function pagerLoadAjaxBody(nid,id,param,selector,urlParams,method,callback,confirm) {
   var doInit = false;
   var additionalData = urlParams || {};

   if (confirm != null) {
      var confirmFunc = window[confirm];
      if (typeof(confirmFunc) == typeof(Function)) {
         var result = confirmFunc(additionalData);
         if (!result) {
            return false;
         }
      } else if (!window.confirm(confirm)) 
         return false ;
   }
   if (additionalData.selector != null) {
      selector  = additionalData.selector;
   }
   if (additionalData.method != null) {
      method  = additionalData.method;
   }
   
	if(!method) {
	   method   = "GET";
	}
	var now = new Date ();
	var data = {
	   nid  : nid || $('input[name="nid"]').val(),
	   inst : now.getTime() 
   };

	if (id != null) {
	   data.Id = id ;
	}else{
        var qs = location.search.substring(1).split("&");
        for (var i=0;i<qs.length;i++) {
            var pair = qs[i].split("=");
            if(pair[0]=='id'){
                data.id = pair[1];
            }
            if(pair[0]=='groupby'){
                data.groupby = pair[1];
            }
        }
    }

	if (param != null) {
      data.param = param ;
   }

	//saving ajax deep linking history
   var history = $.address.history();
   if (history) {
      $.address.parameter('ajaxNID', data.nid);
      
      if (id != null) {
         $.address.parameter('ajaxID', id, true);
      }
      if (param != null) {
         $.address.parameter('ajaxParam', param, true);
      }      
   }   
   
	//merges data and urlParams (notice that data has a higher priority)
	data = $.extend({}, additionalData, data);

	jQuery.ui.Mask.show("Loading ! ...");
    var curUrl = location.protocol + '//' + location.host + location.pathname;
	$.ajax({url: curUrl ,type:method,data:data}).done(function(data, textStatus, jqXHR) {
   		jQuery.ui.Mask.hide();
		var ct = jqXHR.getResponseHeader("content-type") || "";
      if (callback) {
         callback((ct.indexOf('json') > -1) ? data : null);
      }
    	$(selector).html(data).show();
    	
    	if(doInit) {
    	   tableManager.init();
    	} else {
    	   $('html, body').animate({ scrollTop: 0 }, 'fast');
    	}
   });
}

