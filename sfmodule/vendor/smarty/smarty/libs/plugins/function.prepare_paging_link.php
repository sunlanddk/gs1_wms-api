<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {prepare_paging_link} plugin
 *
 * Type:     function
 * Name:     prepare_paging_link
 * Purpose:  prepares onclick attribute string with pagerLoadAjaxBody function call
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string                  link to pager next/prev page
 */

function smarty_function_prepare_paging_link($params, $template) {
    $rethtml = sprintf("onclick=\"pagerAjaxPaging(%d);return false;\"", $params['offset']);
    if (!empty($params['assign'])) {
        $template->assign($params['assign'], $rethtml);
    }else{
        return $rethtml;
    }
}
