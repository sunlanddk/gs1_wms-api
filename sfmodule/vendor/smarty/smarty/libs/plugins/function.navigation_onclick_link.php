<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_onclick_link} plugin
 *
 * Type:     function
 * Name:     navigation_onclick_link
 * Purpose:  outputs result of navigationOnClickLink call and temporary sets $Navigation['Id'] as $Navigation['ParentId'] 
 *           if $params['switch_id'] is set to true
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */
function smarty_function_navigation_onclick_link($params, $template) {
    $navHelper = new \Legacy\Lib\NavigationHelper();

    if($params['switch_id'] === true) {
        global $Navigation;
        $nid = $Navigation['Id'];
        $Navigation['Id'] = $Navigation['ParentId'];
        $link = $navHelper->navigationOnClickLink($params['mark'], $params['id'], $params['param']);
        $Navigation['Id'] = $nid;
        $rethtml = $link;
    } else {
        $rethtml = $navHelper->navigationOnClickLink($params['mark'], $params['id'], $params['param']);
    }

    if (!empty($params['assign'])) {
        $template->assign($params['assign'], $rethtml);
    }else{
        return $rethtml;
    }
}
