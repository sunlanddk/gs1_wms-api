<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_on_click_mark} plugin
 *
 * Type:     function
 * Name:     navigation_on_click_mark
 * Purpose:  outputs result of navigationOnClickMark function call
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */
function smarty_function_navigation_on_click_mark($params, $template) {
    $navHelper = new \Legacy\Lib\NavigationHelper();

    $rethtml = $navHelper->navigationOnClickMark($params['mark'], $params['id'], $params['param']);
    if (!empty($params['assign'])) {
        $template->assign($params['assign'], $rethtml);
    }else{
        return $rethtml;
    }
}
