<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_on_click_link_by_mark} plugin
 *
 * Type:     function
 * Name:     navigation_on_click_link_by_mark
 * Purpose:  outputs result of navigationOnClickLinkByMark function call
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */
function smarty_function_navigation_on_click_link_by_mark($params, $template) {
    $navHelper = new \Legacy\Lib\NavigationHelper();

    $rethtml = $navHelper->navigationOnClickByMark($params['mark']);
    if (!empty($params['assign'])) {
        $template->assign($params['assign'], $rethtml);
    }else{
        return $rethtml;
    }
}
