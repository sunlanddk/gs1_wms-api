<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_mark} plugin
 *
 * Type:     function
 * Name:     navigation_mark
 * Purpose:  outputs result of navigationMark function call
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 */
function smarty_function_navigation_mark($params, $template) {
    $navHelper = new \Legacy\Lib\NavigationHelper();

    $rethtml = $navHelper->navigationMark($params['mark']);
    if (!empty($params['assign'])) {
        $template->assign($params['assign'], $rethtml);
    }else{
        return $rethtml;
    }
}
