<?php

namespace Legacy\Lib;

class ListHelper
{

    public function listStart () {
           return "<table class=list>" ;
    }

    public  function listEnd () {
	return "</tr>\n</table>\n" ;
    }

    public function listRow ($listRowNo) {
        $retstr = ($listRowNo > 0) ? "</tr>" : "" ;
        
        if (($listRowNo % 2) == 1) {
           return  $retstr.'<tr bgcolor=#EEEEEE>' ;
        } else {
           return $retstr.'<tr bgcolor=#FFFFFF>' ;
        }
     }

     public  function listHead ($column='', $width=0, $param='') {
         $html = '<td class=listhead' ;
         if ($width > 0) {
             $html .= ' width='.$width ;
         }
         if ($param != '') {
             $html .= ' '.$param ;
         }
         $html .= '>' ;
         $html .= '<p class=listhead' ;
         $html .= '>' ;
         $html .= $column ;
         $html .= '</p>' ;
         $html .= '</td>' ;

         return $html ;
    }

    public function listField ($value='', $param='') {
        $html = '<td class=list' ;
        if ($param != '') {
            $html .= ' '.$param ;
        }
        $html .= '>' ;
        $html .= '<p class=list>'.htmlentities($value).'</p>' ;
        $html .= '</td>' ;

        return $html ;
    }

    public function listFieldIcon ($icon, $mark, $id=0, $param=null) {
        $navHelper = new NavigationHelper();

        return  sprintf ('<td class=list %s><img class=list src="'._LEGACY_URI.'/image/toolbar/%s"></td>', $navHelper->navigationOnClickLink ($mark, $id, $param), $icon);
    }

    public function listFilterBar ($listField, $listFilter = array()) {
        global $listSort;

       // Find number for filter fields
        if (!isset($listField)){
           return ;
        }
        
        $n = 0 ;
        $filterValues = array();
        $fid = array_keys($listFilter) ;
        foreach ($listField as $i => $field) {
            if (!$field['nofilter']) {
                $n++ ;
                if (array_search($field['db'],$fid)!==false) {
                   $filterValues[$i] = $listFilter[$field['db']];
                }
             }
        }
        
        if ($n == 0){
           return ;
        }

        $retVal = array(
            'fid'         => $fid,
            'listSort'    => $listSort,
            'n'           => $n,
            'listField'   => $listField,
            'filterValues'=> $filterValues
        );

        return $retVal;
       
    }

   public function arrayGetIndex($array, $key, $value) {
      $_res = 0;

      for($i = 0; $i < count($array); $i++) {
          if (
            !is_null($array[$i])
            && $array[$i]['type'] != 6 //img type
            && array_key_exists($key, $array[$i])
            && (strtolower($array[$i][$key]) == strtolower($value)
               || (str_replace(" ", "_", strtolower($array[$i][$key]))) == strtolower($value))
         ) {
            $_res = $i;
            break;
         }
      }
      
      return $_res;
   }

}
