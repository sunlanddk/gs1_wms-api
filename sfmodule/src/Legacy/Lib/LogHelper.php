<?php

namespace Legacy\Lib;

use AppBundle\Entity\Log;
use Symfony\Component\HttpFoundation\Response;


class LogHelper
{
    const logFATAL = 0;
    const logERROR = 1;
    const logWARNING = 2;
    const logINFO = 3;
    const logTRANSACTION = 4;
    const logMAIL = 5;
    const logDUMP = 6;

    protected function _logLevelText ($level)
    {
        $leveltext = array (
            self::logFATAL                 => "FATAL", 
            self::logERROR                => "ERROR", 
            self::logWARNING           => "WARNING", 
            self::logINFO                   => "INFO", 
            self::logTRANSACTION     => "TRANSACTION", 
            self::logMAIL                   => "MAIL", 
            self::logDUMP                  => 'DUMP'
        ) ;
        return $leveltext[$level] ;
    }

    public function logPrintf ($level, $text) 
    {
        global $Session,$User,$Project;

        $query = sprintf ("INSERT INTO Log SET Date='%s', Level=%d, Text='%s'", dbDateEncode(time()), $level, addslashes($text)) ;
        if ($Session != 0) {
            $query .= sprintf (", SessionId=%d", $Session["Id"]) ;
        }
        if ($User != 0) {
            $query .= sprintf (", UserId=%d", $User["Id"]) ;
        }
        if ($Project != 0) {
            $query .= sprintf (", ProjectId=%d", $Project["Id"]) ;
        }

        logSave ($query) ;
        
        if ($level <= self::logFATAL) {
            echo ("<b>" . $this->_logLevelText($level) .":</b>". $text . "<br>");
            exit;
        }
    }

    public function logSave ($query) {
        global $logQuerys ;
        
        $logQuerys[] = $query ;
        foreach ($logQuerys as $i => $query) {
            $r = dbRawQuery ($query) ;
            if ($r !== true) break ;
            unset ($logQuerys[$i]) ;
        }
    }



}
