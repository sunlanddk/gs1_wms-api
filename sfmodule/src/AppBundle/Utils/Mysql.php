<?php

/* 
 * Utils to help operate on mysql queries
 */

namespace AppBundle\Utils;

class Mysql
{
    /*
    * Check and create right comparison form from any text given
    * Ex.: '>1' => '> 1'; '1<>' => '<> 1'; 'asdfk' => '';
    *
    * @param string $val
    * @return string
    */
    public function sanitizeTextComp($val)
    {
            $strOperator = trim(preg_replace('/[0-9,.,-]+/', '', $val));
            $strNumberic = filter_var($val, FILTER_SANITIZE_NUMBER_FLOAT,
                                            FILTER_FLAG_ALLOW_FRACTION);

            if(!$strOperator || !in_array($strOperator, array('>','<','<=','>=','!=','<>'))){
                $strOperator = "= ";
            }
            if(empty($strNumberic) && $strNumberic != '0'){
                $strOperator = "";
            }

            return $strOperator . $strNumberic;
    }
}