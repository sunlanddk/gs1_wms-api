<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Attribute as AttributeModel;

class Attribute extends Service
{
    /**
     * @var AttributeModel $attribute
     */
    private $attribute;

    /**
     * Language constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->attribute = new AttributeModel();
    }

    /**
     * Return data of colors
     *
     * @param array $ids List of colors
     * @param bool  $all If false - label is a id color | true - label is in format: [description_color]_[hash_color]
     *
     * @return array
     */
    public function getColorsByIds($ids, $all = false)
    {
        $statement = $this->em->getConnection()->prepare($this->attribute->getColorsByIds($ids, $this->options['language_ids']));
        $statement->execute();

        $c = array();
        $colors = $statement->fetchAll();

        foreach ($colors as $key => $color) {
            $lang = $color['lang'] != null ? $color['lang'] : 'default';
            $hash =  '#' . sprintf('%02x', $color['RED']) . sprintf('%02x', $color['GREEN']) . sprintf('%02x', $color['BLUE']);
            $name = $color['langDescription'] != null ? $color['langDescription'] . '_' . $hash : $color['Description'] . '_' . $hash;
            $defaultLabel = $all == false ? $color['Description'] . '_' . $hash : $color['id'];

            if (isset($c[$defaultLabel])) {
                $c[$defaultLabel][$lang . '_label'] = $name;
            } else {
                $newC = array(
                    'id' => $color['id'],
                    $lang . '_label' => $name,
                    'color' => $hash
                );

                $c[$defaultLabel] = $newC;
            }

            if (false === isset($c[$color['id']]['default_label'])) {
                $c[$defaultLabel]['default_label'] = $color['Description'] . '_' . $hash;
            }
        }

        return $c;
    }

    /**
     * Get all data of all gender form POT db
     *
     * @return array
     */
    public function getSex()
    {
        $statement = $this->em->getConnection()->prepare($this->attribute->getSex($this->options['language_ids']));
        $statement->execute();

        $g = array();
        $gender = $statement->fetchAll();

        foreach ($gender as $key => $item) {
            $name = $item['langName'] != null ? $item['langName'] : $item['Name'];
            $lang = $item['lang'] != null ? $item['lang'] : 'default';

            if (isset($g[$item['id']])) {
                $g[$item['id']][$lang . '_label'] = $name;
            } else {
                $newG = array(
                    'id' => $item['id'],
                    $lang . '_label' => $name
                );

                $g[$item['id']] = $newG;
            }

            if (false === isset($g[$item['id']]['default_label'])) {
                $g[$item['id']]['default_label'] = $item['Name'];
            }
        }

        return $g;
    }

    /**
     * Get brands info from POT db
     *
     * @param array $ids list of brands ids
     *
     * @return array
     */
    public function getBrands($ids)
    {
        $query = $this->attribute->getBrandsById($ids, $this->options['language_ids']);
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        $brands = $statement->fetchAll();
        $b = array();

        foreach ($brands as $key => $item) {
            $name = $item['brand_lang_name'] != null ? $item['brand_lang_name'] : $item['brand_name'];
            $lang = $item['lang'] != null ? $item['lang'] : 'default';

            if (isset($b[$item['brand_id']])) {
                $b[$item['brand_id']][$lang . '_label'] = $name;
            } else {
                $newB = array(
                    'id' => $item['brand_id'],
                    $lang . '_label' => $name
                );

                $b[$item['brand_id']] = $newB;
            }

            if (false === isset($b[$item['brand_id']]['default_label'])) {
                $b[$item['brand_id']]['default_label'] = $item['brand_name'];
            }
        }

        return $b;
    }

    /**
     * Return one map sql query for size mapper
     *
     * @param int $idPot     id size attr in POT
     * @param int $idMagento id size attr in Magento
     *
     * @return string
     */
    public function addSizeMap($idPot, $idMagento)
    {
        return $this->attribute->mapSize($idPot, $idMagento);
    }

    /**
     * Return one map sql query for brand mapper
     *
     * @param int $idPot     id brand in POT
     * @param int $idMagento id brand in Magento
     *
     * @return string
     */
    public function addBrandMap($idPot, $idMagento)
    {
        return $this->attribute->mapBrand($idPot, $idMagento);
    }

    /**
     * Return one map sql query for color mapper
     *
     * @param int $idPot     id color in POT
     * @param int $idMagento id color in Magento
     *
     * @return string
     */
    public function addColorMap($idPot, $idMagento)
    {
        return $this->attribute->mapColor($idPot, $idMagento);
    }

    /**
     * Return one map sql query for sex attr mapper
     *
     * @param int $idPot     id sex in POT
     * @param int $idMagento id sex in Magento
     *
     * @return string
     */
    public function addSexMap($idPot, $idMagento)
    {
        return $this->attribute->mapSex($idPot, $idMagento);
    }

    /**
     * Save map sizes attr query in db
     *
     * @return void
     */
    public function saveSizesMapDB($query)
    {
        $statement1 = $this->em->getConnection()->prepare($this->attribute->clearSizeMap());
        $statement1->execute();

        $statement2 = $this->em->getConnection()->prepare($query);
        $statement2->execute();
    }

    /**
     * Save map brands query in db
     *
     * @return void
     */
    public function saveBrandMapDB($query)
    {
        $statement1 = $this->em->getConnection()->prepare($this->attribute->clearBrandMap());
        $statement1->execute();

        $statement2 = $this->em->getConnection()->prepare($query);
        $statement2->execute();
    }

    /**
     * Save map colors attr query in db
     *
     * @return void
     */
    public function saveColorMapDB($query)
    {
        $statement1 = $this->em->getConnection()->prepare($this->attribute->clearColorMap());
        $statement1->execute();

        $statement2 = $this->em->getConnection()->prepare($query);
        $statement2->execute();
    }

    /**
     * Save map sex attr query in db
     *
     * @return void
     */
    public function saveSexMapDB($query)
    {
        $statement1 = $this->em->getConnection()->prepare($this->attribute->clearSexMap());
        $statement1->execute();

        $statement2 = $this->em->getConnection()->prepare($query);
        $statement2->execute();
    }
}
