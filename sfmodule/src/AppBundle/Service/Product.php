<?php

namespace AppBundle\Service;

use AppBundle\Module\Export\Collection\ProductCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Product as ProductModel;
use AppBundle\Model\Season;

/**
 * Class Product
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Product extends Service
{
    /**
     * @var ProductModel $productModel
     */
    private $productModel;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->productModel = new ProductModel();
    }

    /**
     * Get web products from POT system.
     *
     * @param null|int $season season id
     *
     * @return ProductCollection
     */
    public function getWebProducts($season = null)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getWebProductsQuery($this->options['language_ids'], $this->options['currency_ids'], $this->options['webshop_id'], $season));
        $statement->execute();

        return new ProductCollection($statement->fetchAll(), $this->container);
    }

    public function getAllProducts($season = null)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getAllProductsQuery($this->options['language_ids'], $this->options['currency_ids'], $this->options['webshop_id'], $season));
        $statement->execute();

        return $statement->fetchAll() ;
    }



    public function getCategoryName($articleNumber)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getCategoryNameQuery($this->options['language_ids'], $articleNumber, $this->options['category_procedure']));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getColorName($variantCode)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getColorNameQuery($this->options['language_ids'], $variantCode));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getColorRGB($collectionmemberid)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getColorRGBQuery($collectionmemberid));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getGenderName($genderId)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getGenderNameQuery($this->options['language_ids'], $genderId));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getSingleProducts($season = null, $articlenumber)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getSingleProductsQuery($this->options['language_ids'], $this->options['currency_ids'], $this->options['webshop_id'], $season, $articlenumber));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getCrosssellProducts($season = null, $crosssell, $collectionmemberid)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getCrosssellProductsQuery($this->options['language_ids'], $this->options['currency_ids'], $this->options['webshop_id'], $season, $crosssell, $collectionmemberid));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getProductsBySeason($seasonid)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getProductsBySeasonQuery($this->options['language_ids'],$seasonid));
        $statement->execute();
        //$stat = $this->productModel->getProductsBySeasonQuery($seasonid);
        return $statement->fetchAll();
    }



    public function getPriceSize($collectionMemberId ,$articleSizeId)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getPriceSizeQuery($this->options['currency_ids'], $collectionMemberId, $articleSizeId));
        $statement->execute();
        return $statement->fetchAll() ;
    }

    public function getOrderQuantity($article, $color, $size)
    {
        $query = $this->productModel->getProductOrderQuantityQuery($article, $color, $size);
        $quantity = $this->em->getConnection()->prepare($query);
        $quantity->execute();

        return $quantity->fetchAll();
    }

    /**
     * Return total quantity of specific variant (article, color, size).
     *
     * @param int $article article id from POT DB
     * @param int $color   color id from POT DB
     * @param int $size    size id from POT DB
     *
     * @return array
     */
    public function getProductQuantity($article, $color, $size)
    {
        $seasonModel = new Season();
        $stocks = $this->em->getConnection()->prepare($seasonModel->getWebshopStocksQuery($this->options['webshop_id']));
        $stocks->execute();
		$query = $this->productModel->getProductInventoryQuantityQuery($article, $color, $size, $this->options['webshop_id'], $stocks->fetchAll());
		if($query == ''){
			return;
		}
		else{
			$quantity = $this->em->getConnection()->prepare($query);
			$quantity->execute();
			return $quantity->fetchAll();
		}
        
    }

    /**
     * Return list of shops where specific product is available for buy
     *
     * @param int $article article id from POT db
     * @param int $color   article id from POT db
     *
     * @return array
     */
    public function getShopUrls($article, $color)
    {
        $query = $this->productModel->getAvailableUrlShops($article, $color, $this->options['webshop_id']);
        $urls = $this->em->getConnection()->prepare($query);
        $urls->execute();

        return $urls->fetchAll();
    }

    /**
     * Return all data for variants
     *
     * @param array    $variants simple array with variant_codes only
     * @param null|int $season Season id
     *
     * @return array
     */
    public function getVariants($variants, $season = null)
    {
        $seasonModel = new Season();
        $stocks = $this->em->getConnection()->prepare($seasonModel->getWebshopStocksQuery($this->options['webshop_id']));
        $stocks->execute();

        $query = $this->productModel->getVariantsQuery($variants, $this->options['webshop_id'], $stocks->fetchAll());
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();

        return $variants->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
    }

    /**
     * Method return from POT every order for each variant passed in param
     *
     * @param array $variants all variants collection for getting orders of each
     *
     * @return array
     */
    public function getVariantsOrder($variants)
    {
        $query = $this->productModel->getVariantOrdersQuery($variants);
        $orderVariants = $this->em->getConnection()->prepare($query);
        $orderVariants->execute();

        return $orderVariants->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
    }
}
