<?php

namespace AppBundle\Service;

use AppBundle\Module\Mapper\VariantCodeMapper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Module\Transport\Transporter as TransporterModule;

/**
 * Class Transporter
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Transporter extends Service
{
    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
    }

    /**
     * Creating and returning new instance of transporter
     *
     * @return TransporterModule
     */
    public function getProductTransporter()
    {
        $auth = $this->options['api_auth'];
        return new TransporterModule($this->options['magento_api'], $auth['user'], $auth['pass']);
    }
}
