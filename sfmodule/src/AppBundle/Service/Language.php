<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Language as LanguageModel;

/**
 * Class Language
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Language extends Service
{
    /**
     * @var LanguageModel $productModel
     */
    private $languageModel;

    /**
     * Language constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->languageModel = new LanguageModel();
    }

    /**
     * Get all languages info about genders attr from POT db
     *
     * @param $id
     * @return array
     */
    public function getSex($id)
    {
        $statement = $this->em->getConnection()->prepare($this->languageModel->getSexLangsQuery($id));
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * Get supported languages data from POT db
     *
     * @return array
     */
    public function getLanguages()
    {
        $statement = $this->em->getConnection()->prepare($this->languageModel->getLanguages($this->options['language_ids']));
        $statement->execute();

        return $statement->fetchAll();
    }
}
