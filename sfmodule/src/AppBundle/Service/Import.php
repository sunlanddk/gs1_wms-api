<?php

namespace AppBundle\Service;


use AppBundle\Entity\Invoice;
use AppBundle\Entity\Invoiceline;
use AppBundle\Entity\Invoicequantity;
use AppBundle\Entity\Pickorder;
use AppBundle\Entity\Requisition;
use AppBundle\Entity\Pickorderline;
use AppBundle\Entity\Order;
use AppBundle\Entity\Orderline;
use AppBundle\Entity\Stock;
use AppBundle\Entity\Item;
use AppBundle\Entity\Containers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AppBundle\Exception\ApiException;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Import as ImportModel;

/**
 * Class Import
 *
 * @author Alex roland <aro@turndigital.dk>
 * @package AppBundle\Service
 */
class Import extends Service
{
    const PickStockId       =  14321;
    /**
     * @var ImportModel $importModel
     */
    private $importModel;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->importModel = new ImportModel();
    }




    public function getOrderIdFromPo($_poId){
        $query = 'SELECT OrderId FROM requisition
                  WHERE  Id = '.$_poId.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch['OrderId'];
        }
    }

    public function getOrderCompany($_Id){
        $query = 'SELECT CompanyId FROM `order`
                  WHERE  Id = '.$_Id.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch['CompanyId'];
        }
    }

    public function getCompanyInfo($_Id){
        $query = 'SELECT * FROM `order`
                  WHERE  Id = '.$_Id.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getCompany($_Id){
        $query = 'SELECT * FROM `company`
                  WHERE  Id = '.$_Id.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }

    
    public function getOrderlines($_Id){
        $orderlines = array();
        $query = 'SELECT orderline.ArticleId, orderline.ArticleColorId, orderquantity.ArticleSizeId, orderquantity.Quantity, variantcode.Id as variantcodeId, orderline.Description, variantcode.VariantCode, `order`.PartDelivery, `order`.PartDeliveryLine FROM `order`
        		LEFT JOIN orderline ON orderline.OrderId = `order`.Id AND orderline.Active=1
                LEFT JOIN orderquantity ON orderquantity.OrderLineId = orderline.Id AND orderquantity.Active=1
                LEFT JOIN variantcode ON variantcode.ArticleId = orderline.ArticleId AND variantcode.ArticleColorId = orderline.ArticleColorId AND variantcode.ArticleSizeId = orderquantity.ArticleSizeId AND variantcode.Active =1
                  WHERE  `order`.ConsolidatedId = '.$_Id.' AND `order`.Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetchAll();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getOrderlinesById($_Id){
        $orderlines = array();
        $query = 'SELECT orderline.ArticleId, orderline.ArticleColorId, orderquantity.ArticleSizeId, orderquantity.Quantity, variantcode.Id as variantcodeId, orderline.Description, variantcode.VariantCode FROM orderline
                LEFT JOIN orderquantity ON orderquantity.OrderLineId = orderline.Id AND orderquantity.Active=1
                LEFT JOIN variantcode ON variantcode.ArticleId = orderline.ArticleId AND variantcode.ArticleColorId = orderline.ArticleColorId AND variantcode.ArticleSizeId = orderquantity.ArticleSizeId AND variantcode.Active =1
                  WHERE  orderline.OrderId = '.$_Id.' AND orderline.Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetchAll();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }


    public function getPickOrderLines($_Id){
        $orderlines = array();
        $query = 'SELECT * FROM pickorderline WHERE PickorderId='.$_Id.' AND pickorderline.Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetchAll();
        if (empty($fetch)) {
            die('no polines found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getVariantColor($colorId) {

        $query = 'SELECT color.Description FROM articlecolor
                  LEFT JOIN color ON color.id=articlecolor.ColorId 
                  WHERE articlecolor.id = '.$colorId . ' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();

        $fetch  =$variants->fetch();
        if (!empty($fetch)) {
            return $fetch['Description'];
        } else {
            return '';
        }
    }

    public function getVariantSize($articlesizeid) {

        $query = 'SELECT Name FROM articlesize 
                  WHERE id='.$articlesizeid.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (!empty($fetch)) {
            return $fetch['Name'];
        } else {
            return '';
        }
    }

    public function getPickorderId($id) {
        $query = 'SELECT Id FROM pickorder 
                  WHERE ConsolidatedId='.$id;
        $pickorder = $this->em->getConnection()->prepare($query);
        $pickorder->execute();
        $fetch  = $pickorder->fetchAll();
        if (!empty($fetch)) {
            return $fetch;
        } else {
            return '';
        }
    }

    public function getStock($_Id){
        $query = 'SELECT * FROM Stock WHERE Id='.$_Id.' AND Active=1 LIMIT 1';
        $stock = $this->em->getConnection()->prepare($query);
        $stock->execute();
        $fetch  = $stock->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getContainerType($_Id){
        $query = 'SELECT * FROM ContainerType WHERE Id='.$_Id.' AND Active=1';
        $stock = $this->em->getConnection()->prepare($query);
        $stock->execute();
        $fetch  = $stock->fetch();
        if (empty($fetch)) {
            die('no vat found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getPickorderStockInfo($_Id){
        $query = 'SELECT PickOrder.*,  o.CarrierId as CarrierId FROM (PickOrder, `order` o) WHERE PickOrder.Active=1 and PickOrder.referenceid=o.id AND PickOrder.Id='.$_Id;
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no pickorder stock found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getVariantInformation($_code){
        $query = 'SELECT VariantCode.*, Container.StockId as PickStockId
                                FROM VariantCode 
                                LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
                                WHERE VariantCode.VariantCode="'.$_code.'" AND VariantCode.Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no variant found');
            return false;
        } else {
            return $fetch;
        }
    }

    public function getItems($VariantCodeInfo, $_fromId){
        $query = 'SELECT Item.* FROM Item, Container WHERE Item.ContainerId=Container.Id And Container.StockId='.$_fromId.' And ArticleId='.$VariantCodeInfo['ArticleId'].' AND ArticleColorId='.$VariantCodeInfo['ArticleColorId'].' AND ArticleSizeId='.$VariantCodeInfo['ArticleSizeId'].' AND Item.Active=1 order by createdate';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetchAll();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch;
        }
    }

    public function getOrderLineIdByQlId($_qlid){
        $query = 'SELECT * FROM orderquantity
                    WHERE   Id='.$_qlid.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no orderquantity found');
            return false;
        } else {
            return $fetch['OrderLineId'];
        }
    }

    public function getOrderlinePrice($orderlineid){
        $query = 'SELECT OrderLine.PriceSale as Price FROM OrderLine
                    WHERE   OrderLine.Id='.$orderlineid.' LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no orderlinesprice found');
            return false;
        } else {
            return $fetch['Price'];
        }
    }
    public function getOrderlineId($Record, $VariantCodeInfo){
        $query = 'SELECT OrderQuantity.OrderLineId as Id FROM OrderLine
                    LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.Active=1 
                    WHERE   OrderQuantity.ArticleSizeId='.$VariantCodeInfo['ArticleSizeId'].' AND OrderLine.OrderId='.$Record['ReferenceId'].' And OrderLine.ArticleId='.$VariantCodeInfo['ArticleId'].' AND 
                            OrderLine.ArticleColorId='.$VariantCodeInfo['ArticleColorId'].' AND OrderLine.Active=1  AND OrderLine.Done=0 LIMIT 1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die($query);
            die('no orderlines found');
            return false;
        } else {
            return $fetch['Id'];
        }
    }

    public function getConIdFromPickorder($_pickorderId){
        $query = 'SELECT * FROM pickorder WHERE Id='.$_pickorderId;
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no pickorder found');
            return false;
        } else {
            return $fetch['ConsolidatedId'];
        }
    }

    public function updateOrder($_orderId){
        $query = 'UPDATE `order` SET `Ready`=1 WHERE Id='.$_orderId;
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
    }

    public function updateQuantityOnItem($_qty, $_itemId){
        $query = 'UPDATE `item` SET `Quantity`='.$_qty.' WHERE Id='.$_itemId;
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
    }

    public function deleteItem($_itemId){
        $query = 'UPDATE `item` SET `Active`=0 WHERE Id='.$_itemId;
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
    }

    public function getshipmentDataById($Id){
        $query = 'SELECT * FROM Stock WHERE Id='.$Id.' AND Active=1';
        $shipment = $this->em->getConnection()->prepare($query);
        $shipment->execute();
        $fetch  = $shipment->fetch();
        if (empty($fetch)) {
            die('no shipment found');
            return false;
        } else {
            return $fetch;
        }
    }
    public function getCompanyId($_poId){
        $orderId = $this->getOrderIdFromPo($_poId);

        $query = 'SELECT * FROM `order` WHERE Id='.$orderId.' AND Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();
        if (empty($fetch)) {
            die('no company found');
            return false;
        } else {
            return $fetch['CompanyId'];
        }
    }
    

    public function getUserData(){
        $query = 'SELECT * FROM user WHERE Id=1 AND Active=1';
        $user = $this->em->getConnection()->prepare($query);
        $user->execute();
        $fetch  = $user->fetch();
        if (empty($fetch)) {
            die('no user found');
            return false;
        } else {
            return $fetch;
        }
    }

    /**
     * get user
     *
     * @param null|int $id row id
     *
     * @return array
     */
    public function getUser()
    {
        $data = $this->getUserData();
        return $data;
    }

    public function setInvoiceNumber($stockId){
        $query = 'SELECT Id FROM Invoice WHERE StockId='.$stockId.' AND Active=1';
        $stock = $this->em->getConnection()->prepare($query);
        $stock->execute();
        $fetch  = $stock->fetch();
        $Id = $fetch['Id'];
        $newNumber = $this->getNextInvoiceNumber();

        $invoice = $this->em->getRepository('AppBundle\Entity\Invoice')->find($Id);        
        $invoice->setNumber($newNumber)
                ->setInvoicedate(new \DateTime())
                ->setReady(1)
                ->setReadyuserid(1)
                ->setReadydate(new \DateTime())
                ->setDone(1)
                ->setDoneuserid(1)
                ->setDonedate(new \DateTime())
                ->SetCurrencyrate(100.00)
                
        ;
        $this->em->persist($invoice);
        $this->em->flush();

        return $invoice->getId();

    }


    public function getInvoiceData($_invoiceId){
        $query = 'SELECT Invoice.*, Currency.Symbol AS CurrencySymbol, Currency.Rate AS CurrRate, Stock.Id AS StockId2, Stock.Name AS StockName2, Stock.Description AS StockDescription2, Stock.CarrierId, Stock.DeliveryTermId, Stock.Address1, Stock.Address2, Stock.ZIP, Stock.City, Stock.CountryId, Company.Number AS CompanyNumber, Company.Name AS CompanyName, Company.InvoiceCountryId as InvoiceCountryId, Company.CountryId as CompanyCountryId, If(Company.InvoicePerEmail>0,"Yes","") as InvoicePerEmail, Company.MailInvoice as MailInvoice, FromCompany.Name AS FromCompanyName, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName, Stock.Name AS StockName, Stock.Description AS StockDescription, IF(Invoice.Done,"Done",IF(NOT Invoice.Ready,"Defined","Ready")) AS State,IF(Invoice.Credit,"Credit","Invoice") AS Type FROM Invoice LEFT JOIN Currency ON Currency.Id=Invoice.CurrencyId LEFT JOIN Company ON Company.Id=Invoice.CompanyId LEFT JOIN Company FromCompany On FromCompany.id =Invoice.FromCompanyId LEFT JOIN User ON User.Id=Invoice.SalesUserId LEFT JOIN Stock ON Stock.Id=Invoice.StockId WHERE Invoice.Id='.$_invoiceId.' AND Invoice.Active=1';

        $invoicenote = $this->em->getConnection()->prepare($query);
        $invoicenote->execute();
        
        return $Record  = $invoicenote->fetchAll();
    }

    public function getLineSizeData($i, $articleId, $articlecolorId){
        $query = 'SELECT ArticleSize.*, InvoiceQuantity.Id AS InvoiceQuantityId, InvoiceQuantity.Quantity, vc.VariantCode as VariantCode 
                                FROM ArticleSize 
                                LEFT JOIN InvoiceQuantity ON InvoiceQuantity.InvoiceLineId='.$i.' AND InvoiceQuantity.Active=1 AND InvoiceQuantity.ArticleSizeId=ArticleSize.Id 
                                LEFT JOIN VariantCode vc ON vc.articleid='.$articleId.' AND vc.articlecolorid='.$articlecolorId.' AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
                                WHERE ArticleSize.ArticleId='.$articleId.' AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name';

        $linesize = $this->em->getConnection()->prepare($query);
        $linesize->execute();
        
        return $Record  = $linesize->fetchAll();
    }

    public function checkPickorder($_poId){
        $query = 'SELECT `pickorder`.Packed FROM requisition LEFT JOIN `order` ON `order`.Id = requisition.OrderId AND `order`.Active=1 LEFT JOIN `pickorder` on `order`.Id = `pickorder`.ReferenceId AND `pickorder`.Active=1 WHERE requisition.Id='.$_poId.' AND requisition.Active=1';
        $user = $this->em->getConnection()->prepare($query);
        $user->execute();
        $fetch  = $user->fetch();

        if((int)$fetch['Packed'] == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function checkForOrder($_poId){
        $query = 'SELECT OrderId FROM requisition WHERE requisition.Id='.$_poId.' AND requisition.Active=1';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetch();

        if((int)$fetch['OrderId'] > 0){
            return false;
        }
        else{
            return true;
        }
    }

    public function getInvoiceNoteData($_invoiceId, $Record){
        $query = 'SELECT
            InvoiceLine.*,
            Order.Id AS OrderId, Order.Reference AS OrderReference, Order.SeasonId as SeasonId, Order.InvoiceHeader AS OrderInvoiceHeader, 
            Order.Phone as OrderPhone, Order.LanguageId as LanguageId,
            Order.InvoiceFooter AS OrderInvoiceFooter, Order.VariantCodes as VariantCodes, OrderLine.AltColorName as AltColorName, 
            InvoiceLine.Description as ArtDescription,
            Case.Id AS CaseId, Case.CustomerReference AS OrderLineReference, Case.ArticleCertificateId,
            Article.Id AS ArticleId, Article.Number AS ArticleNumber, 
            Article.Description AS ArticleDescription, Article.DeliveryComment as DeliveryComment,
            Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
            Color.Description AS ColorDescription, Color.AltDescription AS ColorAltDescription, 
            Color.Number AS ColorNumber, Color.Id as ColorId, 
            ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
            CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Name AS CustomsPositionName,CustomsPosition.Description AS CustomsPositionDesc,
            Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
            MaterialCountry.Description AS MaterialCountryDescription,
            KnitCountry.Description AS KnitCountryDescription,
            WorkCountry.Id AS WorkCountryId, WorkCountry.Description AS WorkCountryDescription, WorkCountry.Name AS WorkCountryName,
            CustomsPreference.Id AS CustomsPreferenceId, COUNT(CustomsPreference.Id) AS CustomsPreferenceCount, CustomsPreference.MaterialDrop, 
            CustomsPreference.KnitDrop, CustomsPreference.WorkDrop  , Invoice.Number as InvoiceNumber, Invoice.Id as InvoiceId, Invoice.StockId as InvoiceStockId       
            FROM InvoiceLine
            LEFT JOIN Invoice ON Invoice.Id=InvoiceLine.InvoiceId
            LEFT JOIN OrderLine ON OrderLine.Id=InvoiceLine.OrderLineId
            LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
            LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
            LEFT JOIN Article ON Article.Id=InvoiceLine.ArticleId
            LEFT JOIN ArticleColor ON ArticleColor.Id=InvoiceLine.ArticleColorId
            LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
            LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
            LEFT JOIN CustomsPosition ON CustomsPosition.Id=Article.CustomsPositionId
            LEFT JOIN Unit ON Unit.Id=Article.UnitId
            LEFT JOIN Country AS MaterialCountry ON MaterialCountry.Id=Article.MaterialCountryId
            LEFT JOIN Country AS KnitCountry ON KnitCountry.Id=Article.KnitCountryId
            LEFT JOIN Country AS WorkCountry ON WorkCountry.Id=Article.WorkCountryId
            LEFT JOIN CustomsPreference ON CustomsPreference.MaterialCountryGroup=IF(Article.MaterialCountryId>0,MaterialCountry.CountryGroup,"none") AND CustomsPreference.KnitCountryGroup=KnitCountry.CountryGroup AND CustomsPreference.WorkCountryGroup=WorkCountry.CountryGroup AND CustomsPreference.Active=1
            WHERE InvoiceLine.InvoiceId='.$_invoiceId.' AND InvoiceLine.Active=1
            GROUP BY InvoiceLine.Id
            ORDER BY InvoiceLine.No';

        $invoicenote = $this->em->getConnection()->prepare($query);
        $invoicenote->execute();
        $data = $invoicenote->fetchAll();
        $Line = array();
        $SizeCount = 0;
        foreach ($data as $key => $value) {
            $Line[(int)$value['Id']] = $value;
            if(isset($Record['LanguageId']) === true){
                if (!$Record['LanguageId']>0) $Record['LanguageId'] = $value['LanguageId'];
            }
            else{
                $Record['LanguageId'] = $value['LanguageId'];
            }
        }


        // Get size specific quantities for each Line with VariantSize
        foreach ($Line as $i => $l) {
            if (!$l['VariantSize']) continue ;

            $Line[$i]['Size'] = array () ;

            $result = $this->getLineSizeData($i, (int)$l['ArticleId'], (int)$l['ArticleColorId']);
            $n = 0 ;
            foreach ($result as $row) {
                $Line[$i]['Size'][(int)$row['Id']] = $row ;
                $n++ ;
            }
            // Update max size count
            if ($n > $SizeCount) $SizeCount = $n ;
        }



        return array('record' => $Record, 'line' => $Line, 'n' => $n, 'sizecount' => $SizeCount);
    }

    /**
     * get shipmentdata
     *
     * @param null|int $id row id
     *
     * @return array
     */
    public function getShipmentData($Id)
    {
        $data = $this->getshipmentDataById($Id);
        return $data;
    }   


    public function getOrdersFromInvoice($Id){
        $query = 'select orderid from invoiceline il, orderline ol
                                         where il.invoiceid='.$Id.' and il.orderlineid=ol.id and il.active=1 and ol.active=1
                                         group by ol.orderid';
        $order = $this->em->getConnection()->prepare($query);
        $order->execute();
        $fetch  = $order->fetchAll();
        if (empty($fetch)) {
            die('no orders found');
            return false;
        } else {
            return $fetch;
        }
    }

    // public function getOrderlinesFromInvoice($Id){
    //     $query = 'select orderline.* from orderline join
    //                                     (select ol.orderid as OLorderid from invoiceline il, orderline ol
    //                                      where il.invoiceid='.$Id.' and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
    //                                      group by ol.orderid) Orders
    //                         where orderline.orderid=Orders.OLorderid and orderline.active=1';
    //     $order = $this->em->getConnection()->prepare($query);
    //     $order->execute();
    //     $fetch  = $order->fetchAll();
    //     if (empty($fetch)) {
    //         die('no orderlines found');
    //         return false;
    //     } else {
    //         return $fetch;
    //     }
    // }


    /**
     * complete order by invoice id
     *
     * @param null|int $id row id
     *
     * @return statement
     */
    public function completeOrderByInvoice($_invoiceId)
    {

        $orders = $this->getOrdersFromInvoice($_invoiceId);

        foreach ($orders as $orderdata) {
            $OrderId = $orderdata['orderid'];
            $order = $this->em->getRepository('AppBundle\Entity\Order')->find($OrderId);
            $order->setDone(1)
	                ->setDoneuserid(1)
	                ->setDonedate(new \DateTime())
	                ->setOrderdoneid(5)
            ;
            $this->em->persist($order);
            $this->em->flush();

        }

        $orderlines = $this->getAllOrderlinesFromInvoice($_invoiceId);
        foreach ($orderlines as $orderlinedata) {
            $orderline = $this->em->getRepository('AppBundle\Entity\Orderline')->find($orderlinedata['Id']);
            $orderline->setDone(1)
                    ->setDoneuserid(1)
                    ->setDonedate(new \DateTime())
            ;
            $this->em->persist($orderline);
            $this->em->flush();
        }

        return true;

    }
    /**
     * Add pickorder to order by PO id
     *
     * @param null|int $id row id
     *
     * @return statement
     */
    public function readyOrderFromPoId($_poId)
    {
        $orderId = $this->getOrderIdFromPo($_poId);
        if((int)$orderId == 0){
            return;
        }

        $this->updateOrder($orderId);

        $companyId = $this->getOrderCompany($orderId);
        $companyInfo = $this->getCompanyInfo($orderId);
        $orderLines = $this->getOrderlinesById($orderId);

         //Create pickorder
        $oNullDate      = \DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');
        $pOrder = new Pickorder();
        $pOrder->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setType("SalesOrder")
                ->setReference($orderId) 
                ->setReferenceid((int)$orderId)
                ->setFromid(self::PickStockId)
                ->setOwnercompanyid(787)
                ->setHandlercompanyid(787)
                ->setInstructions("")
                ->setPacked(0)
                ->setPicked(0)
                ->setPrinted(0)
                ->setUpdated(0)
                ->setDeliverydate(new \DateTime())
                ->setExpecteddate(new \DateTime())
                ->setPickeddate($oNullDate)
                ->setPackeddate($oNullDate)
                ->setCompanyid($companyId)
                ->setUpdated(0)
        ;

        $this->em->persist($pOrder);
        $this->em->flush(); 


        foreach ($orderLines as $value) {
            // PickOrderLine
            $pOrderline = new Pickorderline();
            $pOrderline->setCreatedate(new \DateTime())
                    ->setCreateuserid(1)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(1)
                    ->setActive(true)
                    ->setVariantcodeid($value['variantcodeId']) // VariantCodeId
                    ->setVariantcode($value['VariantCode'])
                    ->setOrderedquantity($value['Quantity'])
                    ->setPickedquantity(0)
                    ->setPackedquantity(0)
                    ->setPickorderid($pOrder->getId())
                    ->setVariantdescription($value['Description'])
                    ->setVariantcolor($this->getVariantColor( $value['ArticleColorId'] ))
                    ->setVariantsize($this->getVariantSize($value['ArticleSizeId']))
                    ->setDone(0)
                ;

            $this->em->persist($pOrderline);
            $this->em->flush();
        }

        return 'success';
    }

	/**
	* create ITEM
	*
	*
	* @return string
	*/
    public function createItem($_variantInfo, $_qty, $_orderlineId, $_price, $_containerid){
    	$item = new Item();
        $item->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setArticleid((int)$_variantInfo['ArticleId'])
                ->setContainerid($_containerid)
                ->setQuantity($_qty)
                ->setArticlecolorid((int)$_variantInfo['ArticleColorId'])
                ->setArticlesizeid((int)$_variantInfo['ArticleSizeId'])
                ->setPrice($_price)
                ->setOrderlineid($_orderlineId)

                ->setArticlecertificateid(0)
               	->setPreviouscontainerid(0)
               	->setStyleid(0)
               	->setPurchaseid(0)
               	->setCaseid(0)
               	->setFromproductionid(0)
               	->setConsumeproductionid(0)
               	->setParentId(1)
               	->setTransportid(0)
               	->setComment('')
               	->setDimension(0)
               	->setSortation(1)
               	->setTestitemid(0)
               	->setTestdone(0)
               	->setTestdoneuserid(0)
               	->setTestdonedate(new \DateTime())
               	->setWidthusable(0)
               	->setWidthfull(0)
               	->setM2weight(0)
               	->setShrinkagewashlength(0)
               	->setShrinkagewashwidth(0)
               	->setShrinkageworklength(0)
               	->setShrinkageworkwidth(0)
               	->setWashinginstruction(0)
               	->setColorfastnesswater(0)
               	->setColorfastnesswash(0)
               	->setColorfastnesslight(0)
               	->setColorfastnessperspiration(0)
               	->setColorfastnesschlor(0)
               	->setColorfastnesssea(0)
               	->setColorfastnessdurability(0)
               	->setRubbingdry(0)
               	->setRubbingwet(0)
               	->setBatchnumber('PICK')
               	->setReceivalnumber(0)
               	->setItemoperationid(0)
               	->setPreviousid(0)
               	->setRequisitionid(0)
               	->setRequisitionlineid(0)
               	->setProductionid(0)
               	->setReservationid(0)
               	->setOwnerid(0)
               	->setInventorypct(0)
               	->setReservationdate(new \DateTime())
               	->setReservationuserid(0)
               	->setPurchasedforid(0)
               	->setAltstockid(0)
               	->setTwisting(0)

        ;

        try {
        	$this->em->persist($item);
	    	$this->em->flush();	
        } catch (\Exception $e) {
        	throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);
        }
        

        return $item->getId();

	} // pickAndInvoice

	/**
	* get data for invoice 
	*
	* @return array
	*/
    public function getItemInvoiceData($_shipmentId){
    	 $query =   '(SELECT
			Order.Id AS OrderId, Order.CurrencyId, Order.PaymentTermId, Order.SalesUserId,  Order.Email, consolidatedorder.InvoiceDraft, consolidatedorder.ConsolidatedInvoice,
			OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
			Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
			Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity, SUM(Item.Price*Item.Quantity) AS ItemCost
			FROM Container
			INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
			INNER JOIN Article ON Article.Id=Item.ArticleId
			LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
			LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId
            LEFT JOIN consolidatedorder ON consolidatedorder.Id=`order`.ConsolidatedId' ;
	    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$_shipmentId) ;
	    $query .= ' GROUP BY OrderLine.Id, Item.ArticleId, Item.ArticleColorId*Article.VariantColor, Item.ArticleSizeId*Article.VariantSize, Item.ArticleCertificateId*Article.VariantCertificate' ;

	    $query .=   sprintf ( ') union (SELECT
			`Order`.Id AS OrderId, `Order`.CurrencyId, `Order`.PaymentTermId, `Order`.SalesUserId,  Order.Email, clo.InvoiceDraft, clo.ConsolidatedInvoice,
			OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
			Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
			0 as ArticleColorId, 0 as ArticleSizeId, 0 as ArticleCertificateId, OrderLine.Quantity AS Quantity, SUM((OrderLine.Quantity*OrderLine.PriceSale)*Currency.Rate/100) AS ItemCost
		FROM `Order`
        LEFT JOIN Consolidatedorder clo on clo.Id=`order`.ConsolidatedId and clo.Active=1
	    JOIN (select ol.orderid as orderid from item i, container c, orderline ol where i.containerid=c.id and c.stockid=%d and i.orderlineid=ol.id group by ol.orderid) Orders ON `Order`.Id=Orders.OrderId
		JOIN Orderline On `Order`.Id=OrderLine.OrderId
		JOIN Article ON Article.Id=Orderline.ArticleId
		JOIN Currency ON Currency.Id=Order.CurrencyId
	    WHERE article.articletypeid=6 and orderline.done=0 and orderline.active=1 and OrderLine.Quantity>0
	     GROUP BY order.id, Orderline.id) ORDER BY OrderId, OrderLineNo, ArticleNumber', (int)$_shipmentId) ;
        $item = $this->em->getConnection()->prepare($query);
        $item->execute();
        $fetch  = $item->fetchAll();
        if (empty($fetch)) {
            die('no item data for invoice found');
            return false;
        } else {
            return $fetch;
        }
    }

    private function getCurrencyRate($Id){
    	$query =   'SELECT Rate FROM currency WHERE Id = '.$Id;
        $currency = $this->em->getConnection()->prepare($query);
        $currency->execute();
        $fetch  = $currency->fetch();
        if (empty($fetch)) {
            die('no currrency with that id');
            return false;
        } else {
            return $fetch['Rate'];
        }
    }

    public function updateInvoiceLineQty($qty, $id){
		$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $qty, $id) ;
		$update = $this->em->getConnection()->prepare($query);
        $update->execute();
	}
    	
    public function insertInvoiceQuantityInDb($_quantityData){
    	$invoicequantity = new Invoicequantity();
    	$invoicequantity->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setQuantity($_quantityData['Quantity'])
                ->setInvoicelineid($_quantityData['InvoiceLineId'])
                ->setArticlesizeid($_quantityData['ArticleSizeId'])
        ;

        try {
        	$this->em->persist($invoicequantity);
	    	$this->em->flush();	
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

	    return $invoicequantity->getId();

    }
    public function insertInvoiceLineInDb($_lineData){
    	$invoiceLine = new Invoiceline();
    	$invoiceLine->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setInvoiceid($_lineData['InvoiceId'])
                ->setOrderlineid($_lineData['OrderLineId'])
                ->setNo($_lineData['No'])
                ->setInvoicefooter($_lineData['InvoiceFooter'])
                ->setDescription($_lineData['Description'])
                ->setCustartref($_lineData['CustArtRef'])
                ->setArticleid($_lineData['ArticleId'])
                ->setQuantity($_lineData['Quantity'])
                ->setDiscount($_lineData['Discount'])
                ->setPricecost($_lineData['PriceCost'])
                ->setPricesale($_lineData['PriceSale'])
                ->setArticlecolorid($_lineData['ArticleColorId'])
                ->setArticlecertificateid($_lineData['ArticleCertificateId'])
        ;

        try {
        	$this->em->persist($invoiceLine);
	    	$this->em->flush();	
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

	    return $invoiceLine->getId();

    }

    public function getNextInvoiceNumber(){
        $query = 'SELECT MAX(Invoice.Number) AS Number FROM Invoice, Company
                                    WHERE Invoice.Ready=1 AND Invoice.Active=1 AND Invoice.FromCompanyID=787 AND
                                          Company.id=Invoice.CompanyId';
        $number = $this->em->getConnection()->prepare($query);
        $number->execute();
        $fetch  = $number->fetch();
        $Number = $fetch['Number'];

        return $Number + 1;
    } 

    public function insertInvoiceInDb($_invoiceData){
    	$invoice = new Invoice();
        $invoice->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setInvoicedate(new \DateTime())
                ->setDuedate(new \DateTime())
                ->setDuebasedate(new \DateTime())
                ->setCredit(0)
                ->setCompanyid($_invoiceData['CompanyId'])
                ->setFromcompanyid($_invoiceData['FromCompanyId'])
                ->setStockid($_invoiceData['StockId'])
                ->setDescription($_invoiceData['Description'])
                ->setCurrencyid($_invoiceData['CurrencyId'])
                ->setCurrencyrate($_invoiceData['CurrencyRate'])
                ->setPaymenttermid($_invoiceData['PaymentTermId'])
                ->setSalesuserid($_invoiceData['SalesUserId'])
                ->setCompanyfooter($_invoiceData['CompanyFooter'])
                ->setEmail($_invoiceData['Email'])
                ->setReference('')
                ->setDiscount(0)
                ->setAccountingno(0)
                ->setInvoiceheader('')
                ->setInvoicefooter('')
                ->setPaidincash(0.00)
                ->setPaidincard(0.00)
                ->setInvoicesummaryid(0)
        ;
		if ($_invoiceData['Ready']) {
			$invoice->setReady(1)
					->setReadyuserid(1)
					->setReadydate(new \DateTime())
					->setNumber($this->getNextInvoiceNumber())
					->setDone(1)
					->setDoneuserid(1)
					->setDonedate(new \DateTime())
			;
		} else {
			$invoice->setReady(0)
					->setReadyuserid(0)
					->setReadydate(new \DateTime())
					->setNumber(0)
					->setDone(0)
					->setDoneuserid(0)
					->setDonedate(new \DateTime())
			;
		}

        try {
        	$this->em->persist($invoice);
	    	$this->em->flush();	
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

	    return $invoice->getId();
    }

	/**
	* create invoice from shipmentId
	*
	* @return array
	*/
    public function createInvoice($_shipmentId){
    	$Record = $this->getshipmentDataById($_shipmentId);
    	$Company = $this->getCompany($Record['CompanyId']);

    	// Valiables
	    $Part = 0 ;
	    $OrderId = 0 ;
	    $Invoice = array () ;
	    $InvoiceLine = array () ;
	    $InvoiceQuantity = array () ;
	    $Quantity = (float)0 ;   
	    $Price_to_be_invoiced = 1 ;
	    $No;
	    $in = array();

	    $itemInvoiceData = $this->getItemInvoiceData($_shipmentId);
	    $Invoice['Id'] = 0;
	    foreach ($itemInvoiceData as $key => $row) {

	    	if ((int)$row['OrderId'] == 0) {
			    // No Order
			    // Fill in default information from Company
			    $row['CurrencyId'] = $Company['CurrencyId'] ;
			    $row['PaymentTermId'] = $Company['PaymentTermId'] ;
			    $row['SalesUserId'] = $Company['SalesUserId'] ;
			    $row['CompanyFooter'] = $Company['CompanyFooter'] ;
			    $row['Description'] = $row['ArticleDescription'] ;
			}   

			// New Invoice ?
			if ((int)$Invoice['Id'] == 0 or
				    ($row['ConsolidatedInvoice'] and ((int)$row['OrderId'] != $OrderId)) or
				    $Invoice['CurrencyId'] != (int)$row['CurrencyId'] or
				    $Invoice['PaymentTermId'] != (int)$row['PaymentTermId'] or
				    $Invoice['SalesUserId'] != (int)$row['SalesUserId']				) {
			    // Generate the Invoice
			    $Invoice = array (
					'Active'		=> 1,
					'CompanyId'		=> (int)$Company['Id'],
					'FromCompanyId'	=> (int)$Record['FromCompanyId'],
					'StockId'		=> (int)$Record['Id'],
					'Credit'		=> 0,
					'Ready'			=> (int)(($row['InvoiceDraft']>0)?0:1),
					'Description'		=> sprintf ('Generated from Shipment, order %d - part %d', (int)$row['OrderId'], ++$Part),
					'CurrencyId'		=> (int)$row['CurrencyId'],
					'PaymentTermId'		=> (int)$row['PaymentTermId'],
					'SalesUserId'		=> (int)$row['SalesUserId'],
					'Email'				=> sprintf ("%s", $row['Email']),
					'CompanyFooter'		=> sprintf ("%s", $Company['CompanyFooter']),
					'CurrencyRate'		=> $this->getCurrencyRate((int)$row['CurrencyId']),
			    ) ;
				$Invoice['Id'] = $this->insertInvoiceInDb($Invoice);
			    $No = 0 ;
			    $OrderId = (int)$row['OrderId'] ;
			}

			// New Invoice Line ?
			if ($InvoiceLine['Id'] == 0 or
			    $InvoiceLine['InvoiceId'] != $Invoice['Id'] or
			    $InvoiceLine['OrderLineId'] != (int)$row['OrderLineId'] or
			    $InvoiceLine['ArticleId'] != (int)$row['ArticleId'] or
			    !$row['VariantSize'] or
			    ($row['VariantColor'] and ($InvoiceLine['ArticleColorId'] != (int)$row['ArticleColorId'])) or
				    ($row['VariantCertificate'] and ($InvoiceLine['ArticleCertificateId'] != (int)$row['ArticleCertificateId']))  ) {

			    // Update Invoice with correct Quantity
			    // Generate the InvoiceLine
			    $InvoiceLine = array (
				'Active'		=> 1,
				'InvoiceId'		=> $Invoice['Id'],
				'OrderLineId'		=> (int)$row['OrderLineId'],
				'No'			=> ++$No,
				'Description'		=> $row['ArticleDescription'],
				'InvoiceFooter'		=> $row['InvoiceFooter'],
				'CustArtRef'		=> $row['CustArtRef'],
				'ArticleId'		=> (int)$row['ArticleId'],
				'Quantity'		=> $row['Quantity'],
				'Discount'		=> (int)$row['Discount'],
				'PriceCost'		=> (float)$row['ItemCost']/(float)$row['Quantity'],
				'PriceSale'		=> (float)$row['PriceSale'],
				'ArticleColorId' => 0,
				'ArticleCertificateId' => 0
			    ) ;
			    if ($row['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$row['ArticleColorId'] ;
			    if ($row['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$row['ArticleCertificateId'] ;

			    $InvoiceLine['Id'] = $this->insertInvoiceLineInDb($InvoiceLine);
			    $Quantity = (float)0 ;  
			    $in[$Invoice['Id']][] = [$InvoiceLine['Id']];
			}

			// Size Variants
			if ($row['VariantSize']) {
			    $InvoiceQuantity = array (
					'Active'		=> 1,
					'InvoiceLineId'		=> $InvoiceLine['Id'],
					'ArticleSizeId'		=> (int)$row['ArticleSizeId'],
					'Quantity'		=> $row['Quantity']
			    ) ;
				
				$invoicequantityId = $this->insertInvoiceQuantityInDb($InvoiceQuantity);
			    $Quantity += (float)$row['Quantity'] ;
			}

			if ($InvoiceLine['Id'] > 0 and $Quantity > (float)$InvoiceLine['Quantity']) {
				$this->updateInvoiceLineQty($Quantity, $InvoiceLine['Id']);
    		}

		}

    	return $in;
    }

    /**
	* pick and invoice by XML data
	*
	* @param object $_pickedXml
	*
	* @return array
	*/
    public function pickAndInvoice($_pickedXml){
    	// $shipmentIds = array(39032, 39031);
    	$shipmentIds = array();
    	$invoiceIds = array();

        $pickorders = array();
        $numberAndKey = array();
        $errors = array();
        $count = 0;
    	foreach ($_pickedXml->Order as $key => $order) {
            if(isset($numberAndKey[(string)$order->{'Delivery-address'}->OrderNumber]) === false){
                $temp = (string)$order->{'Delivery-address'}->OrderNumber;
                $numberAndKey[$temp] = $count;
        		$orderId = $order->{'Delivery-address'}->OrderNumber;
//print_r($order); print_r('test ' . $orderId); die();

        		$shipmentId = $this->setPickorderForReqParentOrder($orderId, $order);
                if($shipmentId === false){
                    array_push($errors, $temp);
                }
                else{
                    array_push($shipmentIds, $shipmentId);
                    $pickorders[$shipmentId] = (int)$orderId;
                }
            }
            else{
                continue;
            }
    	}

        $shipments = array();
        if(empty($shipmentIds) === true){
            return false;
        }

    	foreach ($shipmentIds as $key => $shipId) {
    		$invoiceId = $this->createInvoice($shipId);
    		array_push($invoiceIds, $invoiceId);
            $shipments[$pickorders[$shipId]] = $invoiceId;
    	}

		return array('invoiceIds' => $invoiceIds, 'shipmentInvoices' => $shipments, 'errors' => $errors);

	} // pickAndInvoice

	public function getOrderIdsFromInvoice($Id){
		$query = sprintf ('select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1
										 group by ol.orderid', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            die('no orderid on lines');
            return false;
        } else {
            return $fetch;
        }	
	}

	public function getAllOrderLinesFromInvoice($Id){
		$query = sprintf ('select orderline.* from orderline join
										(select ol.orderid as OLorderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.OLorderid and orderline.active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            die('no orderlines on invoice 1: '. $query);
            return false;
        } else {
            return $fetch;
        }	
	}

	public function getAllOrderLinesFromInvoiceDone($Id){
		$query = sprintf ('select orderline.* from orderline join
										(select ol.orderid as OLorderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.OLorderid and orderline.active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            die('no orderlines on invoice 2');
            return false;
        } else {
            return $fetch;
        }	
	}

	public function getOrderIdByConId($Id){
		$query = sprintf ('SELECT Id FROM `order` WHERE `order`.Active=1 AND `order`.ConsolidatedId=%d  LIMIT 1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            die('no orderlines on invoice 3');
            return false;
        } else {
            return $fetch['Id'];
        }	
	}



	// public function getOrderLinesFromInvoice($Id){
	// 	$query = sprintf ('select orderline.* from orderline join
	// 									(select ol.orderid as OLorderid from invoiceline il, orderline ol
	// 									 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
	// 									 group by ol.orderid) Orders
	// 						where orderline.orderid=Orders.OLorderid and orderline.active=1', $Id) ;
 //        $ids = $this->em->getConnection()->prepare($query);
 //        $ids->execute();
 //        $fetch  = $ids->fetchall();
 //        if (empty($fetch)) {
 //            die('no orderlines on invoice');
 //            return false;
 //        } else {
 //            return $fetch;
 //        }	
	// }

	public function getOrderLinesFromInvoice($Id){
		$query = sprintf ('SELECT orderline.*, invoiceline.Quantity as inQty FROM invoiceline 
						LEFT JOIN orderline ON orderline.Id=invoiceline.OrderLineId AND orderline.Active=1
						WHERE InvoiceId =%d AND invoiceline.Active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            die('no orderlines from invoice');
            return false;
        } else {
            return $fetch;
        }	
	}

    public function getOrdersPartlyDone($Id){
        $query = sprintf ('SELECT consolidatedorder.* FROM invoiceline 
                        LEFT JOIN orderline ON orderline.Id=invoiceline.OrderLineId AND orderline.Active=1
                        LEFT JOIN `order` ON orderline.OrderId=`order`.Id AND `order`.Active=1
                        LEFT JOIN consolidatedorder ON `order`.ConsolidatedId = consolidatedorder.Id
                        WHERE InvoiceId =%d AND invoiceline.Active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            die('no orderlines from invoice');
            return false;
        } else {
            return $fetch[0];
        }   
    }

    public function checkOrderLines($Id){
        $query = sprintf ('SELECT * FROM orderline WHERE OrderId=%d AND Done=0', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchall();
        if (empty($fetch)) {
            return false;
        } else {
            return true;
        }   
    }

    public function getConsolidatedOrderData($Id){
        $query = sprintf ('SELECT * FROM consolidatedorder WHERE Id=%d AND Active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            die('no consolidated order with that id ' . $Id);
            return false;
        } else {
            return $fetch;
        }   
    }

    

	/**
	* set order and orderlines completely done
	*
	* @return array
	*/
	public function orderCompletelyDone($invoiceId){

		$orderIds = $this->getOrderIdsFromInvoice($invoiceId);
		foreach ($orderIds as $key => $row) {
			$order = $this->em->getRepository('AppBundle\Entity\Order')->find($row['orderid']);
			$order->setDone(1)
	                ->setDoneuserid(1)
	                ->setOrderdoneid(5)
	                ->setDonedate(new \DateTime())
	        ;
	        $this->em->persist($order);
	        $this->em->flush();
		}

		$orderlines = $this->getAllOrderLinesFromInvoice($invoiceId);
		foreach ($orderlines as $key => $row) {
			$orderline = $this->em->getRepository('AppBundle\Entity\Orderline')->find($row['Id']);
			$orderline->setDone(1)
	                ->setDoneuserid(1)
	                ->setDonedate(new \DateTime())
	        ;
	        $this->em->persist($orderline);
	        $this->em->flush();
		}

	}

	/**
	* set orderlines done
	*
	* @return array
	*/
	public function orderPartlyDone($invoiceId, $_doneAll){

		$orderlines = $this->getOrderLinesFromInvoice($invoiceId);
		foreach ($orderlines as $key => $row) {
			if((int)$row['inQty'] >= (int)$row['Quantity']){
				$orderline = $this->em->getRepository('AppBundle\Entity\Orderline')->find($row['Id']);
				$orderline->setDone(1)
		                ->setDoneuserid(1)
		                ->setDonedate(new \DateTime())
		        ;
		        $this->em->persist($orderline);
		        $this->em->flush();
		    }
            else{
                if($_doneAll == true){
                    $orderline = $this->em->getRepository('AppBundle\Entity\Orderline')->find($row['Id']);
                    $orderline->setDone(1)
                            ->setDoneuserid(1)
                            ->setDonedate(new \DateTime())
                    ;
                    $this->em->persist($orderline);
                    $this->em->flush();
                }
            }
		}


        $orderlines = $this->getAllOrderLinesFromInvoiceDone($invoiceId);
        $OrderIds = array();
        foreach ($orderlines as $key => $row) {
            $OrderIds[$row['OrderId']] = $row['OrderId'];
        }

        foreach ($OrderIds as $orderId => $value) {
                $isCompletelyDone = $this->checkOrderLines($orderId);

                if($isCompletelyDone === false){
                    $order = $this->em->getRepository('AppBundle\Entity\Order')->find($orderId);
                    $order->setDone(1)
                            ->setOrderdoneid(5)
                            ->setDoneuserid(1)
                            ->setDonedate(new \DateTime())
                    ;
                    $this->em->persist($order);
                    $this->em->flush();
                }
        }

	}


	/**
	* set order and orderlines as done
	*
	* @return array
	*/
	public function setOrderDone($invoiceId, $done){
        $done = $this->getOrdersPartlyDone($invoiceId);
		if($done['PartDelivery'] == 0){
			$this->orderCompletelyDone($invoiceId);
			return $done['InvoiceDraft'];
		}

        if($done['PartDelivery'] == 1 && $done['PartDeliveryLine'] == 0){
            $this->orderPartlyDone($invoiceId, true);
            return $done['InvoiceDraft'];
        }

		$this->orderPartlyDone($invoiceId, false);
		return $done['InvoiceDraft'];

	} //setOrderDone

    /**
     * Add pickorder to order by PO id
     *
     * @param null|int $id row id
     *
     * @return statement
     */
    public function setPickorderForReqParentOrder($_pickorderId, $_json)
    {
        $consolidatedOrderId = $this->getConIdFromPickorder($_pickorderId);
//print_r('test ' . $_pickorderId . ' - ' . $consolidatedOrderId) ;
        $consolidatedOrderData = $this->getConsolidatedOrderData($consolidatedOrderId);
        $orderId = $this->getOrderIdByConId($consolidatedOrderId);
        $companyId = $this->getOrderCompany($orderId);
        $companyInfo = $this->getCompanyInfo($orderId);
        $orderLines = $this->getOrderlines($consolidatedOrderId);
        $pickorderIds = $this->getPickorderId($consolidatedOrderId);


        //foreach ($pickorderIds as $pIdkey => $pId) {
            $pId['Id'] = $_pickorderId;
        	$pickorderlines = $this->getPickOrderLines($pId['Id']);

        	$pOrder = $this->em->getRepository('AppBundle\Entity\Pickorder')->find($pId['Id']);
        	if($pOrder->getPacked() == 1){
        		return false;
        	}
	        $pOrder->setPacked(1)
	                ->setPicked(1)
	                ->setPrinted(1)
	                ->setPackeduserid(1)
	                ->setPickuserid(1)
	                ->setPickeddate(new \DateTime())
	                ->setPackeddate(new \DateTime())
	        ;
	        try {
	        	$this->em->persist($pOrder);
	        	$this->em->flush();
	        } catch (Exception $e) {
				throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
	        }
	       
	        foreach ($pickorderlines as $value) {
	            $qty = 0;

	            // PickOrderLine
	            $pOrderline = $this->em->getRepository('AppBundle\Entity\Pickorderline')->find($value['Id']);
// Er det PackedQty?
// Jeg forstår ikke det efterfølgende kode?
                $qty = (int) $pOrderline->getPackedquantity();
                $orderedQty = ((int)$pOrderline->getOrderedquantity() - (int)$qty);

                //foreach ($_json->Products->Product as $receivedItems) {
				foreach ($_json->Products->{0} as $receivedItems) {
//                    if($receivedItems->SKU == $value['VariantCode'] ){
                    if($receivedItems->plId == $value['Id'] ){
                        if((int)$receivedItems->Quantity > (int)$orderedQty){
                            $qty += (int)$orderedQty;
                            $receivedItems->Quantity = ($receivedItems->Quantity - $orderedQty);
                        }
                        else{
                            $qty += (int)$receivedItems->Quantity;
                            $receivedItems->Quantity = ( $orderedQty - $receivedItems->Quantity );
                        }
                    }
                }

	            $done = 0;

	            if((int)$value['OrderedQuantity'] <= (int)$qty){
	                $done = 1;
	            }  
                if($consolidatedOrderData['PartDelivery'] == 0){
                    $alwaysSetDone = 1;    
                }
                if($consolidatedOrderData['PartDelivery'] == 1 && $consolidatedOrderData['PartDeliveryLine'] == 0){
	               $alwaysSetDone = 1;
                }
	            if($alwaysSetDone === 1){
	            	$done = 1;
	            }

	            $pOrderline->setModifydate(new \DateTime())
	                    ->setModifyuserid(1)
	                    ->setPickedquantity($qty)
	                    ->setPackedquantity($qty)
	                    ->setDone($done)
	                ;
	            try {
		        	$this->em->persist($pOrderline);
	            	$this->em->flush();
		        } catch (Exception $e) {
					throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
		        }
	            

	        }

        //} // go trought pickorders

        $oNullDate      = \DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');

        $field = array();
        $stockFrom = 14321;
        // $stockFrom = 32805;
        
        $fields['ContainerTypeId']['value'] = 22;
        $fields['Position']['value'] = '';
        $fields['GrossWeight']['value'] = 1;

        $carrierUrl = $_json->{'Delivery-address'}->{'TrackAndTrace'};
        $carrierId = 'gls';
        
        switch (true){
            case stristr($carrierUrl,'ups'):
                $carrierId = 25;
                break;
            case stristr($carrierUrl,'gls'):
                $carrierId = 37;
                break;
            case stristr($carrierUrl,'bring'):
                $carrierId = 66;
                break;
        }


        // create shipment
        $shipmentStock = new Stock();
        $shipmentStock->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setType("shipment")
                ->setName('Autoship from '.$consolidatedOrderId) 
                ->setDescription('')
                ->setDeparturedate(new \DateTime())
                ->setArrivaldate(new \DateTime())
                ->setStockdate(new \DateTime())
                ->setAccountid(0)
                ->setBatchid(0)
                ->setPartship(0)
                ->setRangeassign(0)
                ->setOnstockonly(0)
                ->setInventorymode(0)
                ->setPickstock(0)
                ->setInventorypct(100)
                ->setReady(1)
                ->setReadydate(new \DateTime())
                ->setReadyuserid(1)
                ->setInvoiced(1)
                ->setInvoiceddate(new \DateTime())
                ->setInvoiceduserid(1)
                ->setDone(1)
                ->setDonedate(new \DateTime())
                ->setDoneuserid(1)
                ->setArrived(0)
                ->setArriveduserid(1)
                ->setArriveddate(new \DateTime())
                ->setVerified(1)
                ->setVerifieduserid(1)
                ->setVerifieddate(new \DateTime())
                ->setDeparted(1)
                ->setDeparteduserid(1)
                ->setDeparteddate(new \DateTime())
                ->setCompanyid($companyId)
                ->setFromcompanyid(787)
                ->setAltcompanyname($companyInfo['Address1'])
                ->setDeliverytermid($companyInfo['DeliveryTermId'])
                ->setCarrierid($carrierId)
                ->setAddress1($companyInfo['Address1'])
                ->setAddress2($companyInfo['Address2'].' ')
                ->setZip($companyInfo['ZIP'])
                ->setCity($companyInfo['City'])
                ->setCountryid($companyInfo['CountryId'])
                ->setCarrierurl($carrierUrl);
        ;

        try {
        	$this->em->persist($shipmentStock);
        	$this->em->flush();
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

        $fields['StockId']['value'] = $shipmentStock->getId();

        // Get Stock
        $Stock = $this->getStock($shipmentStock->getId());

        // Get ContainerType
        $ContainerType = $this->getContainerType(22);

        $container = new Containers();
        $container->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setStockid($shipmentStock->getId())
                ->setContainertypeid(22)
                ->setPosition('')
                ->setPrevioustransportid(0)
                ->setPreviousstockid(0)
                ->setTaraweight($ContainerType['TaraWeight'])
                ->setGrossweight(1)
                ->setDescription('Autoship SO'.$consolidatedOrderId)
                ->setVolume($ContainerType['Volume'])
        ;

        try {
        	$this->em->persist($container);
        	$this->em->flush();
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

        //foreach ($pickorderIds as $pIdkey => $pId) {
	        $Record = $this->getPickorderStockInfo($pId['Id']);
	        // Move items to shipment.
	        $pickorderlines = $this->getPickOrderLines($pId['Id']);
	        foreach ($pickorderlines as $poLines) {
	            if((int)$poLines['PickedQuantity'] == 0){
	                continue;
	            }

                $IOrderLineId = $this->getOrderLineIdByQlId($poLines['QlId']);

	            // Get Variant Information
	            $VariantCodeInfo = $this->getVariantInformation($poLines['VariantCode']);

	            if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0)){
	                $VariantCodeInfo['PickContainerId'] = 0;    // Future opt: Use containerid from pickcontainer table (variantid, container)  
	            }

	            $items = $this->getItems($VariantCodeInfo, $Record['FromId']);
	            if($items === false){

	            	$price = 0;//$this->getOrderlinePrice($IOrderLineId);
	            	$this->createItem($VariantCodeInfo, $poLines['PickedQuantity'], $IOrderLineId, $price, $container->getId());
	     
	            }
	            else{

		            $PickedQuantity = 0;                        
		            $RemScannedQty = $poLines['PickedQuantity'] ;
		            $ReqQty=$RemScannedQty ;

		            //$IOrderLineId = $this->getOrderlineId($Record, $VariantCodeInfo); 

		            // Find items on stock to fulfil registered qty.
		            foreach ($items as $Item) {
                        $tempQty = (int) $RemScannedQty;
                        if($tempQty == 0){
                            continue;
                        }
		                if (($Item['Quantity'] > $RemScannedQty) or ($Item['Quantity'] == $RemScannedQty)) {
		                    $PickedQuantity += (int)$RemScannedQty ; 

		                    $objectItem = $this->em->find('AppBundle\Entity\Item',$Item['Id']);
		                    $clonedItem = clone $objectItem;
                            $Clone_Item = clone $objectItem;
		                    

		                    // Split Item and create new item with complete qty.
		                    $Item['Quantity'] -= (int)$RemScannedQty ;
		                    $this->updateQuantityOnItem($Item['Quantity'], $Item['Id']);
		                    if ((int)$Item['Quantity'] == 0) {
		                        $this->deleteItem($Item['Id']);
		                    }

		                    $clonedItem->setId(null);
		                    $clonedItem->setPreviouscontainerid($Item['ContainerId']);
		                    $clonedItem->setContainerid($container->getId());
		                    $clonedItem->setParentid($Item['Id']);
		                    $clonedItem->setQuantity($RemScannedQty );
		                    $clonedItem->setOrderlineid($IOrderLineId);
                            $clonedItem->setModifydate(new \DateTime());
                            $clonedItem->setModifyuserid(1);


                            $RemScannedQty = 0;
		                    try {
					        	$this->em->persist($clonedItem);
		                    	$this->em->flush();
					        } catch (Exception $e) {
								throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
					        }
		                    
		                } else {
		                    $RemScannedQty -= $Item['Quantity'] ;
		                    $PickedQuantity += $Item['Quantity'] ;

		                    $objectItem = $this->em->find('AppBundle\Entity\Item',$Item['Id']);
		                    $clonedItem = clone $objectItem;
                            $Clone_Item = clone $objectItem;

		                    // delete Item on stock and create new item with item qty.
		                    $ItemQty = $Item['Quantity'];
		                    $this->updateQuantityOnItem(0, $Item['Id']);
		                    $this->deleteItem($Item['Id']);

		                    $clonedItem->setId(null);
		                    $clonedItem->setPreviouscontainerid($Item['ContainerId']);
		                    $clonedItem->setContainerid($container->getId());
		                    $clonedItem->setParentid($Item['Id']);
		                    $clonedItem->setQuantity($ItemQty);
		                    $clonedItem->setOrderlineid($IOrderLineId);
                            $clonedItem->setModifydate(new \DateTime());
                            $clonedItem->setModifyuserid(1);

		                    try {
					        	$this->em->persist($clonedItem);
		                    	$this->em->flush();
					        } catch (Exception $e) {
								throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
					        }
		                }

                        $lastItemId = $Item['Id'];
                        $lastContianer = $Item['ContainerId'];

		                //$Clone_Item = $Item ;                
		            }   

                    // test her om der stadig mangler nogle qty på ordren.
                    if((int)$RemScannedQty > 0){
                        $Clone_Item->setId(null);
                        $Clone_Item->setPreviouscontainerid($lastContianer);
                        $Clone_Item->setContainerid($container->getId());
                        $Clone_Item->setParentid($lastItemId);
                        $Clone_Item->setQuantity($RemScannedQty);
                        $Clone_Item->setOrderlineid($IOrderLineId);
                        $Clone_Item->setModifydate(new \DateTime());
                        $Clone_Item->setModifyuserid(1);

                        try {
                            $this->em->persist($Clone_Item);
                            $this->em->flush();
                        } catch (Exception $e) {
                            throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);        
                        }
                    }
                    //

	        	}
	        }

	        $pOrder = $this->em->find('AppBundle\Entity\Pickorder',$pId['Id']);
            $pOrder->setLastcontainerid($container->getId())
                ->setToid($shipmentStock->getId())

            ;
            
            try {
	        	$this->em->persist($pOrder);
            	$this->em->flush();
	        } catch (Exception $e) {
				throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
	        }
	    //}

        
        return $shipmentStock->getId();
    }

}

