<?php

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\Container;

use AppBundle\Model\Warehouse as WarehouseModel;

/**
 * Class Warehouse
 *
 * @author Alex Roland <aro@turndigital.dk>
 * @package AppBundle\Service
 */
class Warehouse extends Service {

	  const PickStockId       =  14321;
    const uploadFilePath    = '/develstore/warehouse/upload/';
    const tempFilePath      = '/develstore/warehouse/download/';
    const parsedFilePath    = '/develstore/warehouse/download/parsed/';
    const server            = 'ftp://alpiftp.alpi.dk/';
    const password          = 'duJS72CD';
    const username          = 'fub';

    /**
     * @var Warehouse $warehouseModel
     */
    private $warehouseModel;



    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     * @param PDFMerger $pdfmerger
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->warehouseModel = new WarehouseModel();
    }

    public function fileName($type, $id) {
        $options = $this->container->getParameter('pot_export');
        $libpath = $options['libpath'].'develstore';

        $idstring = sprintf('%08d', $id);
        $file     = $libpath . '/' . $type;

        // Verify directory existance
        while ($idstring != '') {
           $file .= '/' . substr($idstring, 0, 2);
           $idstring = substr($idstring, 2);
        }
        return $file;
     }

    public function saveCopyFile($_filepath, $_content){        
        if($myfile = fopen($_filepath, "w")){
            fwrite($myfile, $_content);
            fclose($myfile);
            return true;
        }
        else{
            return "Unable to open file! ( ".$_filepath.")";
        }
    } // saveXmlCopyFile


  public function downloadSpecificFileToServer($_type, $_folder,$_deleteFile = false, $_extension){
    $options = $this->container->getParameter('pot_export');
    $libpath = $options['libpath'];

    ### DOWNLOAD USING CURL ###
    $username = self::username;
    $password = self::password;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, self::server.$_folder.'/'); // this is for tradebyte
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password"); // this is for tradebyte
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);

    // Check if directory is empty
    if($result === false){
      return 'no files';
    }

    // get only _type files
    $strings = explode(' ', $result);
    $files = array();
	$_nf=0;
    foreach ($strings as $key => $value) {
      if (strpos($value, $_type) !== false) {
		  $_nf++ ;
		  if ($_nf > 7) continue ;
          $fileName = substr($value, 0, strpos($value, ".".$_extension)).'.'.$_extension;
          $fileName = str_replace(array("\r", "\n"), '', $fileName);
          $files[] = $fileName;
      }
    }

    $downloadedFiles = array();
    foreach ($files as $key => $value) {
        $deleteFile = false;
        $parsed = false;
        $pickorderContent = "";
        // $fileName = substr($value, 0, strpos($value, ".xml")).'.xml';
        // $fileName = str_replace(array("\r", "\n"), '', $fileName);
        $fileName = $value;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::server.$_folder.'/'.$fileName); // 
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password"); // this is for tradebyte
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $pickorderContent = curl_exec($curl);
        curl_close($curl);


        if($pickorderContent === false){
            return 'error when saving file.'.$fileName; 
            // something went wrong, try next file
            continue;
        }



        $tempPath = $libpath.self::tempFilePath.$fileName;
        if($this->saveCopyFile($tempPath, $pickorderContent) !== true){
            return 'error when saving file.'.$tempPath;
        }

        $parsed = true;
        $deleteFile = true;

        if($parsed === true){
            //Delete file from ftp server. 
            if($_deleteFile === true && $deleteFile === true){
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, self::server.$_folder.'/'); 
                curl_setopt($curl, CURLOPT_USERPWD, "$username:$password"); 
                curl_setopt($curl, CURLOPT_QUOTE, array("dele /".$_folder."/".$fileName)); 
                curl_exec($curl);
                if($errno = curl_errno($curl)) {
                    $error_message = curl_strerror($errno);
                    return "cURL error ({$errno}):\n {$error_message}";
                }
            }
        }
        array_push($downloadedFiles, $value);
    }

    return $downloadedFiles;
  } // downloadSpecificFileToServer

  public function uploadFileToServer($_filePath, $_extension, $_folder){
        $username = self::username;
        $password = self::password;
        ### UPLOAD USING CURL ###
        # CREATE FILE
        $localfile = basename($_filePath);
        # WRITE TO AND CLOSE FILE
        $fp = fopen($_filePath, 'r');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::server.$_folder.'/'.$localfile); 
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password"); 
        curl_setopt($curl, CURLOPT_UPLOAD, 1);
        curl_setopt($curl, CURLOPT_INFILESIZE, filesize($_filePath));
        curl_setopt($curl, CURLOPT_INFILE, $fp);
        $result = curl_exec($curl);
        curl_close ($curl);
        fclose($fp);

        if($result === false){
            return false;
        }
        return true;
    } // uploadFileToServer

    private function array_to_xml( $data, &$xml_data, $usekey = false ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                //$key = 'item'.$key; //dealing with <0/>..<n/> issues
                $key = key($value);
            }
            if( is_array($value) ) {
                if($usekey === true ){
                    $subnode = $xml_data->addChild("$key");
                    $this->array_to_xml($value[key($value)], $subnode);
                }
                else{
                    $subnode = $xml_data->addChild($key);
                    $this->array_to_xml($value, $subnode, true);
                }
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
         //return $xml_data;
    } // array_to_xml

    private function array_to_xml1($data, $object)
    {   
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $new_object = $object->addChild($key);
                $this->array_to_xml1( $value, $new_object);
            } else {
                // if the key is an integer, it needs text with it to actually work.
                if ($key == (int) $key) {
                    $key = "$key"; // "key_$key"
                }

                $object->addChild($key, $value);
            }   
        }   
    }  
	public function getSKUInventoryQuantity($_stock, $_variantId) {
//		$articleId, $articleColorId, $articleSizeId
		$query = sprintf ("SELECT articleId, articleColorId, articleSizeId FROM VariantCode WHERE id=%d", $_variantId);
		$res=dbQuery($query) ;
		$_variant=dbFetch($res) ;
		dbQueryFree($res) ;
		$query = sprintf ("SELECT  i.articleid, i.articlecolorid, i.articlesizeid, sum(quantity) as quantity
						FROM stock s, container c, item i 
						WHERE s.id=%d and c.stockid=s.id and i.containerid=c.id
							and i.articleid=%d and i.articlecolorid=%d and i.articlesizeid=%d and i.active=1
						GROUP BY i.articleid, i.articlecolorid, i.articlesizeid
						 ", $_stock, $_variant['articleId'],  $_variant['articleColorId'], $_variant['articleSizeId']);
	//print_r($query) ; die() ;
		$res=dbQuery($query) ;
		$row=dbFetch($res) ;
		dbQueryFree($res) ;
		return $row['quantity'];
	}	
   public function processCsv($fileName){
    /*
        Alpi file CSV file format: pickorderlineid; OrderId; PackedQty; VariantCodeId
    */
        $options = $this->container->getParameter('pot_export');
        $libpath = $options['libpath'];
        $tempPath = $libpath.self::tempFilePath.$fileName;
        $csv = str_getcsv(file_get_contents($tempPath), "\r");

        $Products = array();
        $_pickorderid = 0 ;
		$_n = 0 ;
        foreach ($csv as $key => $value) {
			$_n++ ;
            $csvline = explode(';', $value);
			$_cnt=count($csvline) ;
			if (!($_cnt==4)) {
//				die ('Invalid lineformat in line ' . $_n . ' - only 4 coloms allowed: ' . $value . ' (' . $_cnt . ')') ;
				return 'Invalid lineformat in file - only 4 coloms allowed: ' . $value  . ' (' . $_cnt . ')';
			} 
            if ($_pickorderid==0) {
                $_pickorderid = $this->tableGetField('pickorderline', 'PickOrderId', (int)$csvline[0]) ;
            }
			unset($rows) ;
			if ($csvline[3]=='100') {
				$_boxnumber = $this->tableGetField('pickorderline', 'BoxNumber', $csvline[0]) ;
				$_query = 'Select * from pickorderline where pickorderid=' . (int)$_pickorderid . ' and boxnumber=' . $_boxnumber . ' and active=1' ;
				$_res=dbQuery($_query) ;
				while ($row=dbFetch($_res)){
					$rows[$row['Id']][0] = $row['Id'] ;
					$rows[$row['Id']][1] = $row['ReferenceId'] ;
					$rows[$row['Id']][2] = $row['PrePackedQuantity'] ;
					$rows[$row['Id']][3] = $row['VariantCodeId'] ;
				}
			} else {
				$rows[$row['Id']] = $csvline;
			}
			foreach ($rows as $key => $line) {
				$_sku = $this->tableGetField('variantcode', 'VariantCode', $line[3]) ;
				
				// Check if picked matches requested
				$_picklineordered = (int)$this->tableGetField('pickorderline', 'OrderedQuantity', (int)$line[0]) ;
				if ((int)$line[2] > $_picklineordered) { // Surplus not allowed
					$_errTxt = 'Issue in importfile with sku ' . $_sku . ' in fileline ' . $_n . ': Picked ' . (int)$line[2] . ' but only ordered ' . $_picklineordered ;
				}
				$_qtyOnPickStock = (int)$this->getSKUInventoryQuantity(14321, (int)$line[3]) ;
				if ((int)$line[2] > $_qtyOnPickStock ) { // Picked more than is on stock not allowed.
					$_errTxt =  'Issue in importfile with sku ' . $_sku . ' in fileline ' . $_n . ': Ordered and picked ' . $_picklineordered . ' but only ' . $_qtyOnPickStock . ' on Stock';
				}
				if ((int)$line[2] < $_picklineordered) { // Picked less than ordered
					if ((int)$line[2] < $_qtyOnPickStock) // Picking less than what is on stock not allowed.
						$_errTxt =  'Issue in importfile with sku ' . $_sku . ' in fileline ' . $_n . ': Picked ' . (int)$line[2] . ' but ordered ' . $_picklineordered . ' and ' . $_qtyOnPickStock .  ' was on Stock';
					else
						$_errTxt =  'Issue in importfile with sku ' . $_sku . ' in fileline ' . $_n . ': Picked ' . (int)$line[2] . ' and ' . $_qtyOnPickStock .  ' was on Stock but ordered ' . $_picklineordered ; // This shouldn't be needed and will disturb B2B
				}
				$_pickOrderLine = array('ImportError' 		=> $_errTxt,
										'PickedQuantity' 	=> (int)$line[2]
										) ;
				tableWrite('PickOrderLine', $_pickOrderLine,  (int)$line[0]) ;
				if (isset($_errTxt)) {
					if (isset($_pickErrTxt)) {
						$_pickErrTxt .= ','.$_n;
					} else {
						$_pickErrTxt = 'Error in importfile line '.$_n;
					}
					unset($_errTxt) ;
				}
/*			
				if ($_picklineordered==(int)$line[2]) { // Picked and ordered qty matches.
					$_ok = 1 ;
				} else { 
					return 'Issue with sku ' . $_sku . ' in line ' . $_n . ': OrderedQuantity ' . $_picklineordered . ' PickedQuatity ' . $line[2] . ' OnStockQuantity ' . $_qtyOnPickStock;
				} 
*/
				//print_r($line); print_r('test ' . $_pickorderid); die();
				$Products[] = array(
										'SKU'       => $_sku,
										'Quantity'  =>  $line[2],
										'plId'       => $line[0]
									);
			}
        }

         $_consolidatedId = $this->tableGetField('pickorder', 'ConsolidatedId', $_pickorderid) ;
         $_invoiceDraft = $this->tableGetField('consolidatedorder', 'InvoiceDraft', $_consolidatedId) ;
		 if ($_invoiceDraft){
			 $_pickErrTxt = 'Manuel shipping and Invoicing set for this order ' .  $_pickErrTxt ;
		 }
		if (isset($_pickErrTxt)) {
			$_pickOrder = array('ImportError' => $_pickErrTxt) ;
			tableWrite('PickOrder', $_pickOrder,  $_pickorderid) ;
			return $_pickErrTxt ;
		}
		
         $arraydata = array('Order' => array(
                          'Supplier-shop' => array(
                            'Name' => 'NotUsed',
                            'VATRegistrationNumber' => '123123123',
                          ),
                          'Delivery-address' => array(
                            'OrderNumber' => $_pickorderid,
                            'TrackAndTrace' => 'NotUsed',
                          ),
                          'Products' => $Products,
                         ));
            /*$xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><Orders></Orders>');
            $xml = $this->array_to_xml($arraydata,$xml_data);
            return $xml_data;*/

            $xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><Orders></Orders>');
            $this->array_to_xml1($arraydata, $xml_data);
//    print_r($xml_data->asXML());die();
            return $xml_data;
    }

    public function tableGetField($_table, $_coloumn, $_id){
        $query = 'select ' . $_coloumn . ' from ' . $_table . ' where id=' . $_id ;

        $res = $this->em->getConnection()->prepare($query);
        $res->execute();
        $fetch  = $res->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch[$_coloumn];
        }
    }

}