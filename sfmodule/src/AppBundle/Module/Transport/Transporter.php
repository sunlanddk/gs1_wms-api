<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-09-07
 */

namespace AppBundle\Module\Transport;

use AppBundle\Module\Export\Collection\ProductCollection;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Transporter
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Transport
 */
class Transporter extends TransporterAbstract
{
    private $hostApi;
    private $userApi;
    private $passwordApi;

    /**
     * Transporter constructor.
     *
     * @param string $apiMain url main API
     * @param string $user    user with access to API in Magento
     * @param string $pass    password
     */
    public function __construct($apiMain, $user, $pass)
    {
        $this->hostApi = $apiMain;
        $this->userApi = $user;
        $this->passwordApi = $pass;
    }

    /**
     * Send all passed colors into Magento asynchronously
     *
     * @param string $token  Auth token
     * @param array  $colors List of colors
     */
    public function sendColors($token, $colors)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        $requestPromises = array();

        foreach ($colors as $color) {
            unset($color['id']);
            $s['attribute'] =  $color;

            $requestPromises[] = $client->requestAsync('POST', $this->hostApi . self::COLOR_URL_API, array(
                'body' => json_encode($s)
            ));
        }

        \GuzzleHttp\Promise\settle($requestPromises)->wait();
    }

    /**
     * Send all passed sizes into Magento asynchronously
     *
     * @param string $token Auth token
     * @param array $sizes  List of sizes
     */
    public function sendSizes($token, $sizes)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        $requestPromises = array();

        foreach ($sizes as $size) {
            unset($size['id']);
            $s = array(
                'option' => array(
                    'attribute_code' => 'size',
                    'default_label' => $size['name']
                )
            );

            $requestPromises[] = $client->requestAsync('POST', $this->hostApi . self::CUSTOM_ATTR_URL_API, array(
                'body' => json_encode($s)
            ));
        }

        \GuzzleHttp\Promise\settle($requestPromises)->wait();
    }

    /**
     * Send one gender to Magento and return this gender from Magento
     *
     * @param string $token Auth token
     * @param array $gender Gender data
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function sendSex($token, $gender)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        unset($gender['id']);
        $gender['attribute_code'] = 'gender';
        $s['option'] = $gender;

        $response = $client->request('POST', $this->hostApi . self::CUSTOM_ATTR_URL_API, array(
            'body' => json_encode($s)
        ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * Save brand in Magneto and get return this data from Magento
     *
     * @param string $token Auth token
     * @param array  $brand Brand data
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function sendBrand($token, $brand)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        $brand['attribute_code'] = 'brand';
        $s['option'] = $brand;

        $response = $client->request('POST', $this->hostApi . self::CUSTOM_ATTR_URL_API, array(
            'body' => json_encode($s)
        ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    public function sendCampaign($token, $campaign)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        $campaign['attribute_code'] = 'campaigns';
        $s['option'] = $campaign;

        $response = $client->request('POST', $this->hostApi . self::CUSTOM_ATTR_URL_API, array(
            'body' => json_encode($s)
        ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * Return category from Magento after push
     *
     * @param string $token    Auth token
     * @param array  $category Category data
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function sendCategory($token, $category)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        unset($category['id']);
        if (isset($category['magento_id']) && is_int($category['magento_id'])) {
            $category['id'] = $category['magento_id'];
            unset($category['magento_id']);
        }
        $category['parent_id'] = '2';
        $category['is_active'] = true;
        $category['include_in_menu'] = false;
        $cat['category'] = $category;

        $response = $client->request('POST', $this->hostApi . self::CATEGORY_URL_API, array(
            'body' => json_encode($cat)
        ));
        file_put_contents(__DIR__ . '/categories.log', json_encode($cat) . PHP_EOL, FILE_APPEND);

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody()->getContents(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * API Auth
     *
     * @return mixed Token return here
     *
     * @throws TransporterException
     */
    public function authenticate()
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json'
            )
        ));

        $response = $client->request('POST', $this->hostApi . self::AUTH_URL_API, array(
            'body' => json_encode(array('username' => $this->userApi, 'password' => $this->passwordApi))
        ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * Return sizes from Magento
     *
     * @param string $token       Auth token
     * @param bool   $onlyOptions If true return only sizes objects, false - return all magento data size attributes
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function getSizes($token, $onlyOptions = true)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        $response = $client->request('GET', $this->hostApi . self::GET_SIZE_URL_API);

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        if ($onlyOptions) {
            return json_decode($response->getBody())->options;
        }

        return json_decode($response->getBody());
    }

    /**
     * Return all colors from Magento default api
     *
     * @param string $token       Auth token
     * @param bool   $onlyOptions If true return only color objects, false - return all magento data color attributes
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function getColorsDef($token, $onlyOptions = true)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        $response = $client->request('GET', $this->hostApi . self::GET_COLOR_URL_API);

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        if ($onlyOptions) {
            return json_decode($response->getBody())->options;
        }

        return json_decode($response->getBody());
    }


    /**
     * Return all colors from Magento custom api
     *
     * @param string $token       Auth token
     * @param bool   $onlyOptions If true return only color objects, false - return all magento data color attributes
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function getColors($token, $onlyOptions = true)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        $response = $client->request('POST', $this->hostApi . self::POST_COLOR_URL_API, array(
                'body' => json_encode(array('store_code' => 'admin')) // admin for 'default_label'
            ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        if ($onlyOptions) {
            return json_decode($response->getBody())->options;
        }

        return json_decode($response->getBody());
    }

    /**
     * Return all gender info from Magento
     *
     * @param string $token Auth token
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function getSex($token)
    {
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        $response = $client->request('GET', $this->hostApi . self::GET_SEX_URL_API);

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }

        return json_decode($response->getBody())->options;
    }

    /**
     * Preparing new magento API structure where everything is a product. After that send all in one json.
     *
     * @param string $token              Auth token
     * @param array  $productsDivCollection Array with separated collections products. Structure of array ['collections' => 'all_products' => 'each products has a variants key']
     * @param array  $langs              List of supported langs in export
     * @param int    $attribute_color_id Id color attribute from magento
     * @param int    $attribute_size_id  Id size attribute from magento
     * @param string $imageDomain        Url of physical images path where images should get from
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function sendProduct($token, $productsDivCollection, $langs, $attribute_color_id, $attribute_size_id, $imageDomain)
    {
        $productsCollection = array();
        $variantCollection = array();
        $variantStepCollection = array();
        $productsStepCollection = array();
        $simpleProductsVisibility = array();
        $promisesJson = array();
        $promises = array();
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            )
        ));

        foreach ($langs as $lang) {
            $simpleProductsVisibility[$lang['Culture']] = ['val' => '1'];
        }

        foreach ($productsDivCollection as $step => $products) {
            $promisesCollection = array();

            foreach ($products as $product) {
                $usedColors = array();
                $configColors = array();
                $usedSizes = array();
                $configSizes = array();
                $allQuantity = 0;
                $brand = $gender = null;
                $firstImage = null;
                $vShops = '';

                foreach ($product['variants'] as $variant) {
                    if (!isset($variantStepCollection[$step][$variant['variant_code']])) {
                        if (!in_array($variant['magento_color'], $usedColors)) {
                            $usedColors[] = $variant['magento_color'];
                            $configColors[] = array('value_index' => $variant['magento_color']);
                        }

                        if (!in_array($variant['magento_size'], $usedSizes)) {
                            $usedSizes[] = $variant['magento_size'];
                            $configSizes[] = array('value_index' => $variant['magento_size']);
                        }

                        $allQuantity += $variant['quantity'];
                        $brand = $variant['magento_brand'];
                        $gender = $variant['magento_sex'];
                        $image = $imageDomain . $product['product_number'] . '_' . $variant['color_number'] . '.jpg';

                        if ($firstImage == null) {
                            $firstImage = $image;
                        }

                        $v = array(
                            'names' => $product['title_langs'],
                            'prices' => $product['prices']['langs'],
                            'special_prices' => $product['special_prices']['langs'],
                            'sku' => $variant['variant_code'],
                            'attribute_set_id' => "17",
                            'status' => "1",
                            'descriptions' => $product['description_langs'],
                            'visibilities' => $simpleProductsVisibility,
                            'images' => [0 => ['val' => $image]],
                            'type_id' => 'simple',
                            'weight' => "1",
                            'parent_id' => $product['article_id'],
                            'custom_attributes' => array(
                                array('attribute_code' => 'category_ids', 'value' => [$variant['category_id']]),
                                array('attribute_code' => 'short_description', 'value' => ''),
                                array('attribute_code' => 'web_frontpage', 'value' => $product['WebFrontpage'] ? 1 : 0),
                                array('attribute_code' => 'brand', 'value' => $variant['magento_brand']),
                                array('attribute_code' => 'color', 'value' => $variant['magento_color']),
                                array('attribute_code' => 'size', 'value' => $variant['magento_size']),
                                array('attribute_code' => 'article_id', 'value' => $product['article_id']),
                                array('attribute_code' => 'gender', 'value' => $variant['magento_sex']),
                                array('attribute_code' => 'season_id', 'value' => $product['season_id']),
                            ),
                            'extensionAttributes' => array(
                                'stockItem' => array(
                                    'qty' => $variant['quantity'],
                                    'is_in_stock' => $variant['quantity'] > 0 ? true : false,
                                )
                            ),

                        );

                        if($variant['shops']){
                            $vShops = serialize($variant['shops']);
                        }
                        if ($product['WebCampaign'] && $product['WebCampaign'] != '') {
                            //$match = array();
                            //preg_match("/^#$lang:.*/m", $product['WebCampaign'], $match);

                            //if (isset($match[0])) {
                            //  $campaign = str_replace(array("#$lang:", ' ', "\r", "\n", "\t"), '', $match[0]);

                            //$v['custom_attributes'][] = array('attribute_code' => 'campaigns', 'value' => $campaign);
                            //}
                            $v['custom_attributes'][] = array('attribute_code' => 'campaigns', 'value' => $product['WebCampaign']);
                        }

                        if ($product['WebFitGroup']) {
                            $v['custom_attributes'][] = array('attribute_code' => 'web_fit_group', 'value' => $product['WebFitGroup']);
                        }

                        $variantCollection[$variant['variant_code']] = $v;
                        $variantStepCollection[$step][$variant['variant_code']] = $v;
                        $promisesCollection[$variant['variant_code']] = $v;
                    }
                }

                if (!isset($productsStepCollection[$step][$product['article_id']])) {
                    $configurableProduct = array(
                        'names' => $product['title_langs'],
                        'sku' => $product['product_number'],
                        'attribute_set_id' => "17",
                        'status' => "1",
                        'descriptions' => $product['description_langs'],
                        'visibilities' => $product['visibilities']['langs'],
                        'type_id' => 'configurable',
                        'weight' => "1",
                        'parent_id' => '-1',
                        'custom_attributes' => array(
                            //array('attribute_code' => 'url_key', 'value' => $title . '-' . $product['product_id']),
                            array('attribute_code' => 'category_ids', 'value' => [$product['category_id']]),
                            array('attribute_code' => 'web_frontpage', 'value' => $product['WebFrontpage'] ? 1 : 0),
                            array('attribute_code' => 'brand', 'value' => $brand),
                            array('attribute_code' => 'article_id', 'value' => $product['article_id']),
                            array('attribute_code' => 'gender', 'value' => $gender),
                            array('attribute_code' => 'season_id', 'value' => $product['season_id']),
                        ),
                        'images' => [0 => ['val' => $firstImage]],
                        'extensionAttributes' => array(
                            'stockItem' => array(
                                'is_in_stock' => true,
                                //'manage_stock' => true
                            ),
                            "configurable_product_options" => array(
                                array("attribute_id" => $attribute_color_id, "label" => "color", "position" => "0", "values" => $configColors),
                                array("attribute_id" => $attribute_size_id, "label" => "size", "position" => "0", "values" => $configSizes),
                            ),
                        ),

                    );

                    if($vShops) {
                        $configurableProduct['custom_attributes'][] = array('attribute_code' => 'store_partner', 'value' => $vShops);
                        $vShops = '';
                    }

                    if ($product['WebCampaign'] && $product['WebCampaign'] != '') {
                        //$match = array();
                        //preg_match("/^#$lang:.*/m", $product['WebCampaign'], $match);

                        //if (isset($match[0])) {
                        //  $campaign = str_replace(array("#$lang:", ' ', "\r", "\n", "\t"), '', $match[0]);

                        //$configurableProduct['product']['custom_attributes'][] = array('attribute_code' => 'campaigns', 'value' => $campaign);
                        //}

                        $configurableProduct['custom_attributes'][] = array('attribute_code' => 'campaigns', 'value' => $product['WebCampaign']);

                    }

                    if ($product['WebFitGroup'] && is_numeric($product['WebFitGroup'])) {
                        $configurableProduct['custom_attributes'][] = array('attribute_code' => 'web_fit_group', 'value' => $product['WebFitGroup']);
                    }

                    $productsCollection[$product['article_id']] = $configurableProduct;
                    $productsStepCollection[$step][$product['article_id']] = $configurableProduct;
                    $promisesCollection[$product['article_id']] = $configurableProduct;
                }
            }

            $json = json_encode(['products' => $promisesCollection]);

            $promises[] = $client->requestAsync('POST', $this->hostApi . self::CUSTOM_PRODUCT_URL_API, array(
                'body' => $json
            ));
        }

        /*$promisesJson[] = $json;
		foreach ($promisesJson as $key => $j) {
            try {
                $resp = $client->request('POST', $this->hostApi . self::CUSTOM_PRODUCT_URL_API, array(
                    'body' => $j
                ));

                if ($resp->getStatusCode() != 200) {
                    throw new TransporterException('Not 200 - Problem with json in part ' . $key, $resp->getStatusCode());
                }

            } catch (\Exception $e) {
                throw new TransporterException($e->getMessage() . ' Exception catch: Problem with json in part ' . $key);
            }

            return json_decode($resp->getBody());

        }*/

        $responses = \GuzzleHttp\Promise\settle($promises)->wait();
        $lastResponse = end($responses);
        /** @var ResponseInterface $resp */
        $resp =isset($lastResponse['state']) ? $lastResponse['state'] : null;

        if ($resp == 'fulfilled') {
            return json_encode(['success' => true, 'message' => 'Product data sent success.']);
        }

        return json_encode(['success' => false, 'message' => 'Something got wrong']);
    }

    /**
     * @deprecated
     *
     * Send image to Magento
     *
     * @param string $token    Auth token
     * @param string $imageUrl Full image url
     * @param string $sku      Product SKU (variant_code)
     * @param bool   $async    if true - method return prepared promise | false - image is send immediately
     *
     * @return mixed
     *
     * @throws TransporterException
     */
    public function sendImage($token, $imageUrl, $sku, $async = false)
    {
        $url = str_replace('{sku}', $sku, $this->hostApi . self::IMG_URL_API);
        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));

        $image = array(
            'entry' => array(
                "mediaType" => "image",
                "label" => "Image #1",
                "position" => 0,
                "disabled" => false,
                "types" => array("image", 'small_image', 'thumbnail'),
                "content" => array(
                "base64EncodedData" =>  base64_encode(file_get_contents($imageUrl)),
                    "type" => "image/jpeg",
                    "name" => "Image label #1"
                ),
                "extensionAttributes" => array(
                    "videoContent" => array(
                        "mediaType" => "string",
                        "videoProvider" => "string",
                        "videoUrl" => "string",
                        "videoTitle" => "string",
                        "videoDescription" => "string",
                        "videoMetadata" => "string"
                    )
                )
            )
        );

        if ($async == true) {
            return $client->requestAsync('POST', $url, array(
                'body' => json_encode($image)
            ))->wait();
        }

        $response = $client->request('POST', $url, array(
            'body' => json_encode($image)
        ));

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody()->getContents(), $response->getStatusCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * Clear all data in Magento
     *
     * @param string $token Auth token
     *
     * @param $seasonId
     * @throws TransporterException
     */
    public function cleanProduct($token, $seasonId = null)
    {
        $url = $this->hostApi . self::CLEAN_PRODUCT_URL_API;

        $options = ['options' => []];
        if ($seasonId != null) {
            $options['options']['season_id'] = $seasonId;
        }

        $client = new \GuzzleHttp\Client(array(
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$token
            )
        ));
        
        $response = $client->request('POST', $url, ['body' => json_encode($options)]);

        if ($response->getStatusCode() != 200) {
            throw new TransporterException($response->getBody(), $response->getStatusCode());
        }
        
        return json_decode($response->getBody());
    }
}