<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-09-07
 */

namespace AppBundle\Module\Transport;

/**
 * Class TransporterAbstract
 *
 * @package AppBundle\Module\Transport
 */
class TransporterAbstract
{
    const CATEGORY_URL_API = 'V1/custom_category/import';
    const AUTH_URL_API = 'V1/integration/admin/token';
    const CUSTOM_ATTR_URL_API = 'V1/custom_attribute/option';
    const GET_SIZE_URL_API = 'V1/products/attributes/size';
    const GET_COLOR_URL_API = 'V1/products/attributes/color';
    const POST_COLOR_URL_API = 'V1/color/getall';
    const GET_SEX_URL_API = 'V1/products/attributes/gender';
    const COLOR_URL_API = 'V1/color/import';
    const PRODUCT_URL_API = '{lang}/V1/products';
    const CUSTOM_PRODUCT_URL_API = 'V1/custom_api/products_import';
    const IMG_URL_API = 'V1/products/{sku}/media';
    const CLEAN_PRODUCT_URL_API = 'V1/custom_api/products_clean';
}