<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-09-07
 */

namespace AppBundle\Module\Transport;

/**
 * Class TransporterException
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Transport
 */
class TransporterException extends \Exception
{
}