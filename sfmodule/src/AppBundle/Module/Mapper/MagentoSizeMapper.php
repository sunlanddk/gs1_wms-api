<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class MagentoSizeMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class MagentoSizeMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * MagentoSizeMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:MagentoSize');
    }

    /**
     * Return size attribute id from magento
     *
     * @param string $potSizeId
     *
     * @return integer|null
     */
    public function map($potSizeId)
    {
        $size = $this->repository->findOneBy(['idPot' => $potSizeId]);

        if (null === $size) {
            return null;
        }

        return $size->getIdMagento();
    }
}