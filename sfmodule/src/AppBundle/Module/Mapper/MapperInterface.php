<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;


interface MapperInterface
{
    /**
     * Map passed value to mapper value
     *
     * @param  string $key Key for get mapper value
     *
     * @return mixed
     */
    public function map($key);
}