<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class MagentoCategoryMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class MagentoCategoryMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * MagentoCategoryMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:MagentoCategory');
    }

    /**
     * Return a category id from magento
     *
     * @param string $potCategoryId
     *
     * @return integer|null
     */
    public function map($potCategoryId)
    {
        $category = $this->repository->findOneBy(['idPot' => $potCategoryId]);

        if (null === $category) {
            return null;
        }

        return $category->getIdMagento();
    }

    /**
     * {@inheritdoc}
     *
     * @return \AppBundle\Entity\MagentoCategory[]|array
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }
}