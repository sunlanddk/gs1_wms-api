<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class MagentoSexMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class MagentoSexMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * MagentoSexMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:MagentoSex');
    }

    /**
     * Return a gender attribute id from magento
     *
     * @param string $potSexId
     *
     * @return integer|null
     */
    public function map($potSexId)
    {
        $sex = $this->repository->findOneBy(['idPot' => $potSexId]);

        if (null === $sex) {
            return null;
        }

        return $sex->getIdMagento();
    }
}