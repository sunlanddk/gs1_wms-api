<?php

namespace AppBundle\Events;

use AppBundle\Exception\ApiException;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        $oException = $event->getException();

        if ($oException instanceof NotFoundHttpException) {
            $event->setResponse(new RedirectResponse('/'));
        }

        if($oException instanceof ApiException) {
            $event->setResponse(new JsonResponse(['result' => 'error', 'message' => $oException->getMessage(), 'code' => $oException->getCode()], 500));
        }
    }
    
}
