<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collectionmemberprice
 */
class Collectionmemberprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $collectionmemberid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var float
     */
    private $wholesaleprice;

    /**
     * @var float
     */
    private $retailprice;

    /**
     * @var float
     */
    private $campaignprice;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $collectionpricegroupid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectionmemberid
     *
     * @param integer $collectionmemberid
     * @return Collectionmemberprice
     */
    public function setCollectionmemberid($collectionmemberid)
    {
        $this->collectionmemberid = $collectionmemberid;

        return $this;
    }

    /**
     * Get collectionmemberid
     *
     * @return integer 
     */
    public function getCollectionmemberid()
    {
        return $this->collectionmemberid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Collectionmemberprice
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set wholesaleprice
     *
     * @param float $wholesaleprice
     * @return Collectionmemberprice
     */
    public function setWholesaleprice($wholesaleprice)
    {
        $this->wholesaleprice = $wholesaleprice;

        return $this;
    }

    /**
     * Get wholesaleprice
     *
     * @return float 
     */
    public function getWholesaleprice()
    {
        return $this->wholesaleprice;
    }

    /**
     * Set retailprice
     *
     * @param float $retailprice
     * @return Collectionmemberprice
     */
    public function setRetailprice($retailprice)
    {
        $this->retailprice = $retailprice;

        return $this;
    }

    /**
     * Get retailprice
     *
     * @return float 
     */
    public function getRetailprice()
    {
        return $this->retailprice;
    }

    /**
     * Set campaignprice
     *
     * @param float $campaignprice
     * @return Collectionmemberprice
     */
    public function setCampaignprice($campaignprice)
    {
        $this->campaignprice = $campaignprice;

        return $this;
    }

    /**
     * Get campaignprice
     *
     * @return float 
     */
    public function getCampaignprice()
    {
        return $this->campaignprice;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Collectionmemberprice
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Collectionmemberprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Collectionmemberprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Collectionmemberprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Collectionmemberprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set collectionpricegroupid
     *
     * @param integer $collectionpricegroupid
     * @return Collectionmemberprice
     */
    public function setCollectionpricegroupid($collectionpricegroupid)
    {
        $this->collectionpricegroupid = $collectionpricegroupid;

        return $this;
    }

    /**
     * Get collectionpricegroupid
     *
     * @return integer 
     */
    public function getCollectionpricegroupid()
    {
        return $this->collectionpricegroupid;
    }
}
