<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bundlework
 */
class Bundlework
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $bundleid;

    /**
     * @var integer
     */
    private $styleoperationid;

    /**
     * @var integer
     */
    private $worksheetid;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $starttime;

    /**
     * @var string
     */
    private $usedminutes;

    /**
     * @var integer
     */
    private $productionid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Bundlework
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Bundlework
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Bundlework
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Bundlework
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Bundlework
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set bundleid
     *
     * @param integer $bundleid
     * @return Bundlework
     */
    public function setBundleid($bundleid)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return integer 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set styleoperationid
     *
     * @param integer $styleoperationid
     * @return Bundlework
     */
    public function setStyleoperationid($styleoperationid)
    {
        $this->styleoperationid = $styleoperationid;

        return $this;
    }

    /**
     * Get styleoperationid
     *
     * @return integer 
     */
    public function getStyleoperationid()
    {
        return $this->styleoperationid;
    }

    /**
     * Set worksheetid
     *
     * @param integer $worksheetid
     * @return Bundlework
     */
    public function setWorksheetid($worksheetid)
    {
        $this->worksheetid = $worksheetid;

        return $this;
    }

    /**
     * Get worksheetid
     *
     * @return integer 
     */
    public function getWorksheetid()
    {
        return $this->worksheetid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Bundlework
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set starttime
     *
     * @param \DateTime $starttime
     * @return Bundlework
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return \DateTime 
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set usedminutes
     *
     * @param string $usedminutes
     * @return Bundlework
     */
    public function setUsedminutes($usedminutes)
    {
        $this->usedminutes = $usedminutes;

        return $this;
    }

    /**
     * Get usedminutes
     *
     * @return string 
     */
    public function getUsedminutes()
    {
        return $this->usedminutes;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Bundlework
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }
}
