<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Containers
 */
class Containers
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $stockid;

    /**
     * @var integer
     */
    private $containertypeid;

    /**
     * @var integer
     */
    private $previoustransportid;

    /**
     * @var integer
     */
    private $previousstockid;

    /**
     * @var string
     */
    private $position;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $taraweight;

    /**
     * @var string
     */
    private $grossweight;

    /**
     * @var string
     */
    private $volume;

    /**
     * @var string
     */
    private $sscc;

    /**
     * @var integer
     */
    private $inboundstockid;


    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Container
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Container
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Container
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Container
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Container
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Container
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }

    /**
     * Set containertypeid
     *
     * @param integer $containertypeid
     * @return Container
     */
    public function setContainertypeid($containertypeid)
    {
        $this->containertypeid = $containertypeid;

        return $this;
    }

    /**
     * Get containertypeid
     *
     * @return integer 
     */
    public function getContainertypeid()
    {
        return $this->containertypeid;
    }

    /**
     * Set previoustransportid
     *
     * @param integer $previoustransportid
     * @return Container
     */
    public function setPrevioustransportid($previoustransportid)
    {
        $this->previoustransportid = $previoustransportid;

        return $this;
    }

    /**
     * Get previoustransportid
     *
     * @return integer 
     */
    public function getPrevioustransportid()
    {
        return $this->previoustransportid;
    }

    /**
     * Set previousstockid
     *
     * @param integer $previousstockid
     * @return Container
     */
    public function setPreviousstockid($previousstockid)
    {
        $this->previousstockid = $previousstockid;

        return $this;
    }

    /**
     * Get previousstockid
     *
     * @return integer 
     */
    public function getPreviousstockid()
    {
        return $this->previousstockid;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Container
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Container
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set taraweight
     *
     * @param string $taraweight
     * @return Container
     */
    public function setTaraweight($taraweight)
    {
        $this->taraweight = $taraweight;

        return $this;
    }

    /**
     * Get taraweight
     *
     * @return string 
     */
    public function getTaraweight()
    {
        return $this->taraweight;
    }

    /**
     * Set grossweight
     *
     * @param string $grossweight
     * @return Container
     */
    public function setGrossweight($grossweight)
    {
        $this->grossweight = $grossweight;

        return $this;
    }

    /**
     * Get grossweight
     *
     * @return string 
     */
    public function getGrossweight()
    {
        return $this->grossweight;
    }

    /**
     * Set volume
     *
     * @param string $volume
     * @return Container
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return string 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set sscc
     *
     * @param string $sscc
     * @return Container
     */
    public function setSscc($sscc)
    {
        $this->sscc = $sscc;

        return $this;
    }

    /**
     * Get sscc
     *
     * @return string 
     */
    public function getSscc()
    {
        return $this->sscc;
    }

    /**
     * Set inboundstockid
     *
     * @param integer $inboundstockid
     * @return Container
     */
    public function setInboundstockid($inboundstockid)
    {
        $this->inboundstockid = $inboundstockid;

        return $this;
    }

    /**
     * Get inboundstockid
     *
     * @return integer 
     */
    public function getInboundstockid()
    {
        return $this->inboundstockid;
    }
}
