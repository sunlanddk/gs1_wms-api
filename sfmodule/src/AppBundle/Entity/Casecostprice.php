<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Casecostprice
 */
class Casecostprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var integer
     */
    private $priceversion;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $operationscosttotal;

    /**
     * @var string
     */
    private $materialscosttotal;

    /**
     * @var string
     */
    private $logisticscost;

    /**
     * @var string
     */
    private $netcost;

    /**
     * @var string
     */
    private $grosscost;

    /**
     * @var integer
     */
    private $materialspageid;

    /**
     * @var integer
     */
    private $operationspageid;

    /**
     * @var boolean
     */
    private $approve;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var string
     */
    private $actualmaterialssurplus;

    /**
     * @var string
     */
    private $actualoperationssurplus;

    /**
     * @var string
     */
    private $logisticcostpricecomment;

    /**
     * @var string
     */
    private $productionwastage;

    /**
     * @var string
     */
    private $othermaterialcost;

    /**
     * @var string
     */
    private $othermaterialcomment;

    /**
     * @var string
     */
    private $servicecost;

    /**
     * @var string
     */
    private $servicecostcomment;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $totalservicecost;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Casecostprice
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Casecostprice
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Casecostprice
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set priceversion
     *
     * @param integer $priceversion
     * @return Casecostprice
     */
    public function setPriceversion($priceversion)
    {
        $this->priceversion = $priceversion;

        return $this;
    }

    /**
     * Get priceversion
     *
     * @return integer 
     */
    public function getPriceversion()
    {
        return $this->priceversion;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Casecostprice
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set operationscosttotal
     *
     * @param string $operationscosttotal
     * @return Casecostprice
     */
    public function setOperationscosttotal($operationscosttotal)
    {
        $this->operationscosttotal = $operationscosttotal;

        return $this;
    }

    /**
     * Get operationscosttotal
     *
     * @return string 
     */
    public function getOperationscosttotal()
    {
        return $this->operationscosttotal;
    }

    /**
     * Set materialscosttotal
     *
     * @param string $materialscosttotal
     * @return Casecostprice
     */
    public function setMaterialscosttotal($materialscosttotal)
    {
        $this->materialscosttotal = $materialscosttotal;

        return $this;
    }

    /**
     * Get materialscosttotal
     *
     * @return string 
     */
    public function getMaterialscosttotal()
    {
        return $this->materialscosttotal;
    }

    /**
     * Set logisticscost
     *
     * @param string $logisticscost
     * @return Casecostprice
     */
    public function setLogisticscost($logisticscost)
    {
        $this->logisticscost = $logisticscost;

        return $this;
    }

    /**
     * Get logisticscost
     *
     * @return string 
     */
    public function getLogisticscost()
    {
        return $this->logisticscost;
    }

    /**
     * Set netcost
     *
     * @param string $netcost
     * @return Casecostprice
     */
    public function setNetcost($netcost)
    {
        $this->netcost = $netcost;

        return $this;
    }

    /**
     * Get netcost
     *
     * @return string 
     */
    public function getNetcost()
    {
        return $this->netcost;
    }

    /**
     * Set grosscost
     *
     * @param string $grosscost
     * @return Casecostprice
     */
    public function setGrosscost($grosscost)
    {
        $this->grosscost = $grosscost;

        return $this;
    }

    /**
     * Get grosscost
     *
     * @return string 
     */
    public function getGrosscost()
    {
        return $this->grosscost;
    }

    /**
     * Set materialspageid
     *
     * @param integer $materialspageid
     * @return Casecostprice
     */
    public function setMaterialspageid($materialspageid)
    {
        $this->materialspageid = $materialspageid;

        return $this;
    }

    /**
     * Get materialspageid
     *
     * @return integer 
     */
    public function getMaterialspageid()
    {
        return $this->materialspageid;
    }

    /**
     * Set operationspageid
     *
     * @param integer $operationspageid
     * @return Casecostprice
     */
    public function setOperationspageid($operationspageid)
    {
        $this->operationspageid = $operationspageid;

        return $this;
    }

    /**
     * Get operationspageid
     *
     * @return integer 
     */
    public function getOperationspageid()
    {
        return $this->operationspageid;
    }

    /**
     * Set approve
     *
     * @param boolean $approve
     * @return Casecostprice
     */
    public function setApprove($approve)
    {
        $this->approve = $approve;

        return $this;
    }

    /**
     * Get approve
     *
     * @return boolean 
     */
    public function getApprove()
    {
        return $this->approve;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Casecostprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Casecostprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Casecostprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Casecostprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set actualmaterialssurplus
     *
     * @param string $actualmaterialssurplus
     * @return Casecostprice
     */
    public function setActualmaterialssurplus($actualmaterialssurplus)
    {
        $this->actualmaterialssurplus = $actualmaterialssurplus;

        return $this;
    }

    /**
     * Get actualmaterialssurplus
     *
     * @return string 
     */
    public function getActualmaterialssurplus()
    {
        return $this->actualmaterialssurplus;
    }

    /**
     * Set actualoperationssurplus
     *
     * @param string $actualoperationssurplus
     * @return Casecostprice
     */
    public function setActualoperationssurplus($actualoperationssurplus)
    {
        $this->actualoperationssurplus = $actualoperationssurplus;

        return $this;
    }

    /**
     * Get actualoperationssurplus
     *
     * @return string 
     */
    public function getActualoperationssurplus()
    {
        return $this->actualoperationssurplus;
    }

    /**
     * Set logisticcostpricecomment
     *
     * @param string $logisticcostpricecomment
     * @return Casecostprice
     */
    public function setLogisticcostpricecomment($logisticcostpricecomment)
    {
        $this->logisticcostpricecomment = $logisticcostpricecomment;

        return $this;
    }

    /**
     * Get logisticcostpricecomment
     *
     * @return string 
     */
    public function getLogisticcostpricecomment()
    {
        return $this->logisticcostpricecomment;
    }

    /**
     * Set productionwastage
     *
     * @param string $productionwastage
     * @return Casecostprice
     */
    public function setProductionwastage($productionwastage)
    {
        $this->productionwastage = $productionwastage;

        return $this;
    }

    /**
     * Get productionwastage
     *
     * @return string 
     */
    public function getProductionwastage()
    {
        return $this->productionwastage;
    }

    /**
     * Set othermaterialcost
     *
     * @param string $othermaterialcost
     * @return Casecostprice
     */
    public function setOthermaterialcost($othermaterialcost)
    {
        $this->othermaterialcost = $othermaterialcost;

        return $this;
    }

    /**
     * Get othermaterialcost
     *
     * @return string 
     */
    public function getOthermaterialcost()
    {
        return $this->othermaterialcost;
    }

    /**
     * Set othermaterialcomment
     *
     * @param string $othermaterialcomment
     * @return Casecostprice
     */
    public function setOthermaterialcomment($othermaterialcomment)
    {
        $this->othermaterialcomment = $othermaterialcomment;

        return $this;
    }

    /**
     * Get othermaterialcomment
     *
     * @return string 
     */
    public function getOthermaterialcomment()
    {
        return $this->othermaterialcomment;
    }

    /**
     * Set servicecost
     *
     * @param string $servicecost
     * @return Casecostprice
     */
    public function setServicecost($servicecost)
    {
        $this->servicecost = $servicecost;

        return $this;
    }

    /**
     * Get servicecost
     *
     * @return string 
     */
    public function getServicecost()
    {
        return $this->servicecost;
    }

    /**
     * Set servicecostcomment
     *
     * @param string $servicecostcomment
     * @return Casecostprice
     */
    public function setServicecostcomment($servicecostcomment)
    {
        $this->servicecostcomment = $servicecostcomment;

        return $this;
    }

    /**
     * Get servicecostcomment
     *
     * @return string 
     */
    public function getServicecostcomment()
    {
        return $this->servicecostcomment;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Casecostprice
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set totalservicecost
     *
     * @param string $totalservicecost
     * @return Casecostprice
     */
    public function setTotalservicecost($totalservicecost)
    {
        $this->totalservicecost = $totalservicecost;

        return $this;
    }

    /**
     * Get totalservicecost
     *
     * @return string 
     */
    public function getTotalservicecost()
    {
        return $this->totalservicecost;
    }
}
