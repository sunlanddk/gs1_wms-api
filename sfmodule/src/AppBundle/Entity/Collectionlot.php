<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collectionlot
 */
class Collectionlot
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $generatedpick;

    /**
     * @var \DateTime
     */
    private $generateddate;

    /**
     * @var integer
     */
    private $presale;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Collectionlot
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Collectionlot
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Collectionlot
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Collectionlot
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Collectionlot
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Collectionlot
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Collectionlot
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Collectionlot
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set generatedpick
     *
     * @param integer $generatedpick
     * @return Collectionlot
     */
    public function setGeneratedpick($generatedpick)
    {
        $this->generatedpick = $generatedpick;

        return $this;
    }

    /**
     * Get generatedpick
     *
     * @return integer 
     */
    public function getGeneratedpick()
    {
        return $this->generatedpick;
    }

    /**
     * Set generateddate
     *
     * @param \DateTime $generateddate
     * @return Collectionlot
     */
    public function setGenerateddate($generateddate)
    {
        $this->generateddate = $generateddate;

        return $this;
    }

    /**
     * Get generateddate
     *
     * @return \DateTime 
     */
    public function getGenerateddate()
    {
        return $this->generateddate;
    }

    /**
     * Set presale
     *
     * @param integer $presale
     * @return Collectionlot
     */
    public function setPresale($presale)
    {
        $this->presale = $presale;

        return $this;
    }

    /**
     * Get presale
     *
     * @return integer 
     */
    public function getPresale()
    {
        return $this->presale;
    }
}
