<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 */
class Permission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $functionid;

    /**
     * @var integer
     */
    private $roleid;

    /**
     * @var boolean
     */
    private $permread;

    /**
     * @var boolean
     */
    private $permwrite;

    /**
     * @var boolean
     */
    private $permcreate;

    /**
     * @var boolean
     */
    private $permreadother;

    /**
     * @var boolean
     */
    private $permwriteother;

    /**
     * @var boolean
     */
    private $permdelete;

    /**
     * @var boolean
     */
    private $permdeleteother;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Permission
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Permission
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Permission
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set functionid
     *
     * @param integer $functionid
     * @return Permission
     */
    public function setFunctionid($functionid)
    {
        $this->functionid = $functionid;

        return $this;
    }

    /**
     * Get functionid
     *
     * @return integer 
     */
    public function getFunctionid()
    {
        return $this->functionid;
    }

    /**
     * Set roleid
     *
     * @param integer $roleid
     * @return Permission
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;

        return $this;
    }

    /**
     * Get roleid
     *
     * @return integer 
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * Set permread
     *
     * @param boolean $permread
     * @return Permission
     */
    public function setPermread($permread)
    {
        $this->permread = $permread;

        return $this;
    }

    /**
     * Get permread
     *
     * @return boolean 
     */
    public function getPermread()
    {
        return $this->permread;
    }

    /**
     * Set permwrite
     *
     * @param boolean $permwrite
     * @return Permission
     */
    public function setPermwrite($permwrite)
    {
        $this->permwrite = $permwrite;

        return $this;
    }

    /**
     * Get permwrite
     *
     * @return boolean 
     */
    public function getPermwrite()
    {
        return $this->permwrite;
    }

    /**
     * Set permcreate
     *
     * @param boolean $permcreate
     * @return Permission
     */
    public function setPermcreate($permcreate)
    {
        $this->permcreate = $permcreate;

        return $this;
    }

    /**
     * Get permcreate
     *
     * @return boolean 
     */
    public function getPermcreate()
    {
        return $this->permcreate;
    }

    /**
     * Set permreadother
     *
     * @param boolean $permreadother
     * @return Permission
     */
    public function setPermreadother($permreadother)
    {
        $this->permreadother = $permreadother;

        return $this;
    }

    /**
     * Get permreadother
     *
     * @return boolean 
     */
    public function getPermreadother()
    {
        return $this->permreadother;
    }

    /**
     * Set permwriteother
     *
     * @param boolean $permwriteother
     * @return Permission
     */
    public function setPermwriteother($permwriteother)
    {
        $this->permwriteother = $permwriteother;

        return $this;
    }

    /**
     * Get permwriteother
     *
     * @return boolean 
     */
    public function getPermwriteother()
    {
        return $this->permwriteother;
    }

    /**
     * Set permdelete
     *
     * @param boolean $permdelete
     * @return Permission
     */
    public function setPermdelete($permdelete)
    {
        $this->permdelete = $permdelete;

        return $this;
    }

    /**
     * Get permdelete
     *
     * @return boolean 
     */
    public function getPermdelete()
    {
        return $this->permdelete;
    }

    /**
     * Set permdeleteother
     *
     * @param boolean $permdeleteother
     * @return Permission
     */
    public function setPermdeleteother($permdeleteother)
    {
        $this->permdeleteother = $permdeleteother;

        return $this;
    }

    /**
     * Get permdeleteother
     *
     * @return boolean 
     */
    public function getPermdeleteother()
    {
        return $this->permdeleteother;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Permission
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Permission
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
