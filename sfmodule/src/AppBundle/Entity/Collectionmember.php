<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collectionmember
 */
class Collectionmember
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $collectionid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $cancel;

    /**
     * @var string
     */
    private $canceldescription;

    /**
     * @var integer
     */
    private $collectionlotid;

    /**
     * @var integer
     */
    private $notonwebshop;

    /**
     * @var boolean
     */
    private $focus;

    /**
     * @var boolean
     */
    private $webfrontpage;

    /**
     * @var boolean
     */
    private $showonb2b;

    /**
     * @var boolean
     */
    private $webfrontpagede;

    /**
     * @var string
     */
    private $webfitgroup;

    /**
     * @var string
     */
    private $webcampaign;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set collectionid
     *
     * @param integer $collectionid
     * @return Collectionmember
     */
    public function setCollectionid($collectionid)
    {
        $this->collectionid = $collectionid;

        return $this;
    }

    /**
     * Get collectionid
     *
     * @return integer 
     */
    public function getCollectionid()
    {
        return $this->collectionid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Collectionmember
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Collectionmember
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Collectionmember
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Collectionmember
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Collectionmember
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Collectionmember
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Collectionmember
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Collectionmember
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set cancel
     *
     * @param integer $cancel
     * @return Collectionmember
     */
    public function setCancel($cancel)
    {
        $this->cancel = $cancel;

        return $this;
    }

    /**
     * Get cancel
     *
     * @return integer 
     */
    public function getCancel()
    {
        return $this->cancel;
    }

    /**
     * Set canceldescription
     *
     * @param string $canceldescription
     * @return Collectionmember
     */
    public function setCanceldescription($canceldescription)
    {
        $this->canceldescription = $canceldescription;

        return $this;
    }

    /**
     * Get canceldescription
     *
     * @return string 
     */
    public function getCanceldescription()
    {
        return $this->canceldescription;
    }

    /**
     * Set collectionlotid
     *
     * @param integer $collectionlotid
     * @return Collectionmember
     */
    public function setCollectionlotid($collectionlotid)
    {
        $this->collectionlotid = $collectionlotid;

        return $this;
    }

    /**
     * Get collectionlotid
     *
     * @return integer 
     */
    public function getCollectionlotid()
    {
        return $this->collectionlotid;
    }

    /**
     * Set notonwebshop
     *
     * @param integer $notonwebshop
     * @return Collectionmember
     */
    public function setNotonwebshop($notonwebshop)
    {
        $this->notonwebshop = $notonwebshop;

        return $this;
    }

    /**
     * Get notonwebshop
     *
     * @return integer 
     */
    public function getNotonwebshop()
    {
        return $this->notonwebshop;
    }

    /**
     * Set focus
     *
     * @param boolean $focus
     * @return Collectionmember
     */
    public function setFocus($focus)
    {
        $this->focus = $focus;

        return $this;
    }

    /**
     * Get focus
     *
     * @return boolean 
     */
    public function getFocus()
    {
        return $this->focus;
    }

    /**
     * Set webfrontpage
     *
     * @param boolean $webfrontpage
     * @return Collectionmember
     */
    public function setWebfrontpage($webfrontpage)
    {
        $this->webfrontpage = $webfrontpage;

        return $this;
    }

    /**
     * Get webfrontpage
     *
     * @return boolean 
     */
    public function getWebfrontpage()
    {
        return $this->webfrontpage;
    }

    /**
     * Set showonb2b
     *
     * @param boolean $showonb2b
     * @return Collectionmember
     */
    public function setShowonb2b($showonb2b)
    {
        $this->showonb2b = $showonb2b;

        return $this;
    }

    /**
     * Get showonb2b
     *
     * @return boolean 
     */
    public function getShowonb2b()
    {
        return $this->showonb2b;
    }

    /**
     * Set webfrontpagede
     *
     * @param boolean $webfrontpagede
     * @return Collectionmember
     */
    public function setWebfrontpagede($webfrontpagede)
    {
        $this->webfrontpagede = $webfrontpagede;

        return $this;
    }

    /**
     * Get webfrontpagede
     *
     * @return boolean 
     */
    public function getWebfrontpagede()
    {
        return $this->webfrontpagede;
    }

    /**
     * Set webfitgroup
     *
     * @param string $webfitgroup
     * @return Collectionmember
     */
    public function setWebfitgroup($webfitgroup)
    {
        $this->webfitgroup = $webfitgroup;

        return $this;
    }

    /**
     * Get webfitgroup
     *
     * @return string 
     */
    public function getWebfitgroup()
    {
        return $this->webfitgroup;
    }

    /**
     * Set webcampaign
     *
     * @param string $webcampaign
     * @return Collectionmember
     */
    public function setWebcampaign($webcampaign)
    {
        $this->webcampaign = $webcampaign;

        return $this;
    }

    /**
     * Get webcampaign
     *
     * @return string 
     */
    public function getWebcampaign()
    {
        return $this->webcampaign;
    }
}
