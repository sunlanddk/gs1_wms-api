<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisitionline
 */
class Requisitionline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $requisitionid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $no;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $articlecertificateid;

    /**
     * @var string
     */
    private $surplus;

    /**
     * @var string
     */
    private $purchasepriceconsumption;

    /**
     * @var string
     */
    private $actualpriceconsumption;

    /**
     * @var string
     */
    private $logisticcostconsumption;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var \DateTime
     */
    private $requesteddate;

    /**
     * @var \DateTime
     */
    private $confirmeddate;

    /**
     * @var \DateTime
     */
    private $expecteddate;

    /**
     * @var \DateTime
     */
    private $availabledate;

    /**
     * @var string
     */
    private $linefooter;

    /**
     * @var string
     */
    private $articlecomment;

    /**
     * @var boolean
     */
    private $group;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var string
     */
    private $purchaseprice;

    /**
     * @var string
     */
    private $actualprice;

    /**
     * @var string
     */
    private $logisticcost;

    /**
     * @var integer
     */
    private $articlepurchaseunitid;

    /**
     * @var string
     */
    private $articlepuratio;

    /**
     * @var string
     */
    private $articlepuname;

    /**
     * @var integer
     */
    private $articlepudecimals;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $reservationid;

    /**
     * @var integer
     */
    private $delayreasonid;

    /**
     * @var integer
     */
    private $defectreasonid;

    /**
     * @var \DateTime
     */
    private $baselinedate;


    /**
     * Set id
     *
     * @param integer $id
     * @return Requisitionline
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requisitionid
     *
     * @param integer $requisitionid
     * @return Requisitionline
     */
    public function setRequisitionid($requisitionid)
    {
        $this->requisitionid = $requisitionid;

        return $this;
    }

    /**
     * Get requisitionid
     *
     * @return integer 
     */
    public function getRequisitionid()
    {
        return $this->requisitionid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Requisitionline
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set no
     *
     * @param integer $no
     * @return Requisitionline
     */
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get no
     *
     * @return integer 
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Requisitionline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Requisitionline
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set articlecertificateid
     *
     * @param integer $articlecertificateid
     * @return Requisitionline
     */
    public function setArticlecertificateid($articlecertificateid)
    {
        $this->articlecertificateid = $articlecertificateid;

        return $this;
    }

    /**
     * Get articlecertificateid
     *
     * @return integer 
     */
    public function getArticlecertificateid()
    {
        return $this->articlecertificateid;
    }

    /**
     * Set surplus
     *
     * @param string $surplus
     * @return Requisitionline
     */
    public function setSurplus($surplus)
    {
        $this->surplus = $surplus;

        return $this;
    }

    /**
     * Get surplus
     *
     * @return string 
     */
    public function getSurplus()
    {
        return $this->surplus;
    }

    /**
     * Set purchasepriceconsumption
     *
     * @param string $purchasepriceconsumption
     * @return Requisitionline
     */
    public function setPurchasepriceconsumption($purchasepriceconsumption)
    {
        $this->purchasepriceconsumption = $purchasepriceconsumption;

        return $this;
    }

    /**
     * Get purchasepriceconsumption
     *
     * @return string 
     */
    public function getPurchasepriceconsumption()
    {
        return $this->purchasepriceconsumption;
    }

    /**
     * Set actualpriceconsumption
     *
     * @param string $actualpriceconsumption
     * @return Requisitionline
     */
    public function setActualpriceconsumption($actualpriceconsumption)
    {
        $this->actualpriceconsumption = $actualpriceconsumption;

        return $this;
    }

    /**
     * Get actualpriceconsumption
     *
     * @return string 
     */
    public function getActualpriceconsumption()
    {
        return $this->actualpriceconsumption;
    }

    /**
     * Set logisticcostconsumption
     *
     * @param string $logisticcostconsumption
     * @return Requisitionline
     */
    public function setLogisticcostconsumption($logisticcostconsumption)
    {
        $this->logisticcostconsumption = $logisticcostconsumption;

        return $this;
    }

    /**
     * Get logisticcostconsumption
     *
     * @return string 
     */
    public function getLogisticcostconsumption()
    {
        return $this->logisticcostconsumption;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Requisitionline
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set requesteddate
     *
     * @param \DateTime $requesteddate
     * @return Requisitionline
     */
    public function setRequesteddate($requesteddate)
    {
        $this->requesteddate = $requesteddate;

        return $this;
    }

    /**
     * Get requesteddate
     *
     * @return \DateTime 
     */
    public function getRequesteddate()
    {
        return $this->requesteddate;
    }

    /**
     * Set confirmeddate
     *
     * @param \DateTime $confirmeddate
     * @return Requisitionline
     */
    public function setConfirmeddate($confirmeddate)
    {
        $this->confirmeddate = $confirmeddate;

        return $this;
    }

    /**
     * Get confirmeddate
     *
     * @return \DateTime 
     */
    public function getConfirmeddate()
    {
        return $this->confirmeddate;
    }

    /**
     * Set expecteddate
     *
     * @param \DateTime $expecteddate
     * @return Requisitionline
     */
    public function setExpecteddate($expecteddate)
    {
        $this->expecteddate = $expecteddate;

        return $this;
    }

    /**
     * Get expecteddate
     *
     * @return \DateTime 
     */
    public function getExpecteddate()
    {
        return $this->expecteddate;
    }

    /**
     * Set availabledate
     *
     * @param \DateTime $availabledate
     * @return Requisitionline
     */
    public function setAvailabledate($availabledate)
    {
        $this->availabledate = $availabledate;

        return $this;
    }

    /**
     * Get availabledate
     *
     * @return \DateTime 
     */
    public function getAvailabledate()
    {
        return $this->availabledate;
    }

    /**
     * Set linefooter
     *
     * @param string $linefooter
     * @return Requisitionline
     */
    public function setLinefooter($linefooter)
    {
        $this->linefooter = $linefooter;

        return $this;
    }

    /**
     * Get linefooter
     *
     * @return string 
     */
    public function getLinefooter()
    {
        return $this->linefooter;
    }

    /**
     * Set articlecomment
     *
     * @param string $articlecomment
     * @return Requisitionline
     */
    public function setArticlecomment($articlecomment)
    {
        $this->articlecomment = $articlecomment;

        return $this;
    }

    /**
     * Get articlecomment
     *
     * @return string 
     */
    public function getArticlecomment()
    {
        return $this->articlecomment;
    }

    /**
     * Set group
     *
     * @param boolean $group
     * @return Requisitionline
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return boolean 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Requisitionline
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Requisitionline
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Requisitionline
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Requisitionline
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Requisitionline
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Requisitionline
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Requisitionline
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Requisitionline
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set purchaseprice
     *
     * @param string $purchaseprice
     * @return Requisitionline
     */
    public function setPurchaseprice($purchaseprice)
    {
        $this->purchaseprice = $purchaseprice;

        return $this;
    }

    /**
     * Get purchaseprice
     *
     * @return string 
     */
    public function getPurchaseprice()
    {
        return $this->purchaseprice;
    }

    /**
     * Set actualprice
     *
     * @param string $actualprice
     * @return Requisitionline
     */
    public function setActualprice($actualprice)
    {
        $this->actualprice = $actualprice;

        return $this;
    }

    /**
     * Get actualprice
     *
     * @return string 
     */
    public function getActualprice()
    {
        return $this->actualprice;
    }

    /**
     * Set logisticcost
     *
     * @param string $logisticcost
     * @return Requisitionline
     */
    public function setLogisticcost($logisticcost)
    {
        $this->logisticcost = $logisticcost;

        return $this;
    }

    /**
     * Get logisticcost
     *
     * @return string 
     */
    public function getLogisticcost()
    {
        return $this->logisticcost;
    }

    /**
     * Set articlepurchaseunitid
     *
     * @param integer $articlepurchaseunitid
     * @return Requisitionline
     */
    public function setArticlepurchaseunitid($articlepurchaseunitid)
    {
        $this->articlepurchaseunitid = $articlepurchaseunitid;

        return $this;
    }

    /**
     * Get articlepurchaseunitid
     *
     * @return integer 
     */
    public function getArticlepurchaseunitid()
    {
        return $this->articlepurchaseunitid;
    }

    /**
     * Set articlepuratio
     *
     * @param string $articlepuratio
     * @return Requisitionline
     */
    public function setArticlepuratio($articlepuratio)
    {
        $this->articlepuratio = $articlepuratio;

        return $this;
    }

    /**
     * Get articlepuratio
     *
     * @return string 
     */
    public function getArticlepuratio()
    {
        return $this->articlepuratio;
    }

    /**
     * Set articlepuname
     *
     * @param string $articlepuname
     * @return Requisitionline
     */
    public function setArticlepuname($articlepuname)
    {
        $this->articlepuname = $articlepuname;

        return $this;
    }

    /**
     * Get articlepuname
     *
     * @return string 
     */
    public function getArticlepuname()
    {
        return $this->articlepuname;
    }

    /**
     * Set articlepudecimals
     *
     * @param integer $articlepudecimals
     * @return Requisitionline
     */
    public function setArticlepudecimals($articlepudecimals)
    {
        $this->articlepudecimals = $articlepudecimals;

        return $this;
    }

    /**
     * Get articlepudecimals
     *
     * @return integer 
     */
    public function getArticlepudecimals()
    {
        return $this->articlepudecimals;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Requisitionline
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set reservationid
     *
     * @param integer $reservationid
     * @return Requisitionline
     */
    public function setReservationid($reservationid)
    {
        $this->reservationid = $reservationid;

        return $this;
    }

    /**
     * Get reservationid
     *
     * @return integer 
     */
    public function getReservationid()
    {
        return $this->reservationid;
    }

    /**
     * Set delayreasonid
     *
     * @param integer $delayreasonid
     * @return Requisitionline
     */
    public function setDelayreasonid($delayreasonid)
    {
        $this->delayreasonid = $delayreasonid;

        return $this;
    }

    /**
     * Get delayreasonid
     *
     * @return integer 
     */
    public function getDelayreasonid()
    {
        return $this->delayreasonid;
    }

    /**
     * Set defectreasonid
     *
     * @param integer $defectreasonid
     * @return Requisitionline
     */
    public function setDefectreasonid($defectreasonid)
    {
        $this->defectreasonid = $defectreasonid;

        return $this;
    }

    /**
     * Get defectreasonid
     *
     * @return integer 
     */
    public function getDefectreasonid()
    {
        return $this->defectreasonid;
    }

    /**
     * Set baselinedate
     *
     * @param \DateTime $baselinedate
     * @return Requisitionline
     */
    public function setBaselinedate($baselinedate)
    {
        $this->baselinedate = $baselinedate;

        return $this;
    }

    /**
     * Get baselinedate
     *
     * @return \DateTime 
     */
    public function getBaselinedate()
    {
        return $this->baselinedate;
    }
}
