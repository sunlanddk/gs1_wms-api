<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paymentinformation
 */
class Paymentinformation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $tocompanyid;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var integer
     */
    private $companyid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Paymentinformation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tocompanyid
     *
     * @param integer $tocompanyid
     * @return Paymentinformation
     */
    public function setTocompanyid($tocompanyid)
    {
        $this->tocompanyid = $tocompanyid;

        return $this;
    }

    /**
     * Get tocompanyid
     *
     * @return integer 
     */
    public function getTocompanyid()
    {
        return $this->tocompanyid;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Paymentinformation
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Paymentinformation
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }
}
