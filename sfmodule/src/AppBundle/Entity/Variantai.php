<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variantai
 */
class Variantai
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $itemid;

    /**
     * @var string
     */
    private $value;

    /**
     * @var integer
     */
    private $aiid;    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Variantai
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     * @return Variantai
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer 
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set aiid
     *
     * @param integer $aiid
     * @return Variantai
     */
    public function setAiid($aiid)
    {
        $this->aiid = $aiid;

        return $this;
    }

    /**
     * Get aiid
     *
     * @return integer 
     */
    public function getAiid()
    {
        return $this->aiid;
    }

   
}
