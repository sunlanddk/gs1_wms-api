<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 */
class Article
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $defaultstockid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $articletypeid;

    /**
     * @var integer
     */
    private $suppliercompanyid;

    /**
     * @var string
     */
    private $suppliernumber;

    /**
     * @var integer
     */
    private $customspositionid;

    /**
     * @var integer
     */
    private $unitid;

    /**
     * @var integer
     */
    private $materialcountryid;

    /**
     * @var integer
     */
    private $workcountryid;

    /**
     * @var integer
     */
    private $knitcountryid;

    /**
     * @var boolean
     */
    private $variantcolor;

    /**
     * @var boolean
     */
    private $variantdimension;

    /**
     * @var boolean
     */
    private $variantsize;

    /**
     * @var boolean
     */
    private $variantsortation;

    /**
     * @var boolean
     */
    private $variantcertificate;

    /**
     * @var boolean
     */
    private $pricedimensionadjust;

    /**
     * @var string
     */
    private $pricestock;

    /**
     * @var string
     */
    private $sketchtype;

    /**
     * @var integer
     */
    private $sketchuserid;

    /**
     * @var \DateTime
     */
    private $sketchdate;

    /**
     * @var string
     */
    private $printnumber;

    /**
     * @var integer
     */
    private $widthusable;

    /**
     * @var integer
     */
    private $widthfull;

    /**
     * @var integer
     */
    private $m2weight;

    /**
     * @var string
     */
    private $shrinkagewashlength;

    /**
     * @var string
     */
    private $shrinkagewashwidth;

    /**
     * @var string
     */
    private $shrinkageworklength;

    /**
     * @var string
     */
    private $shrinkageworkwidth;

    /**
     * @var string
     */
    private $washinginstruction;

    /**
     * @var string
     */
    private $colorfastnesswater;

    /**
     * @var string
     */
    private $colorfastnesswash;

    /**
     * @var string
     */
    private $colorfastnesslight;

    /**
     * @var string
     */
    private $colorfastnessperspiration;

    /**
     * @var string
     */
    private $colorfastnesschlor;

    /**
     * @var string
     */
    private $colorfastnesssea;

    /**
     * @var string
     */
    private $colorfastnessdurability;

    /**
     * @var string
     */
    private $rubbingdry;

    /**
     * @var string
     */
    private $rubbingwet;

    /**
     * @var integer
     */
    private $fabricarticleid;

    /**
     * @var string
     */
    private $deliverycomment;

    /**
     * @var boolean
     */
    private $public;

    /**
     * @var string
     */
    private $salesprice;

    /**
     * @var integer
     */
    private $listhidden;

    /**
     * @var \DateTime
     */
    private $receiveddate;

    /**
     * @var \DateTime
     */
    private $ordereddate;

    /**
     * @var \DateTime
     */
    private $rejecteddate;

    /**
     * @var \DateTime
     */
    private $approveddate;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var boolean
     */
    private $usepdmstyle;

    /**
     * @var boolean
     */
    private $hasstyle;

    /**
     * @var string
     */
    private $requisitiontext;

    /**
     * @var integer
     */
    private $costcurrencyid;

    /**
     * @var integer
     */
    private $customercompanyid;

    /**
     * @var integer
     */
    private $ownercompanyid;

    /**
     * @var integer
     */
    private $importidref;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Article
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Article
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Article
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Article
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set defaultstockid
     *
     * @param boolean $defaultstockid
     * @return Article
     */
    public function setDefaultstockid($defaultstockid)
    {
        $this->defaultstockid = $defaultstockid;

        return $this;
    }

    /**
     * Get defaultstockid
     *
     * @return boolean 
     */
    public function getDefaultstockid()
    {
        return $this->defaultstockid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Article
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Article
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Article
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set articletypeid
     *
     * @param integer $articletypeid
     * @return Article
     */
    public function setArticletypeid($articletypeid)
    {
        $this->articletypeid = $articletypeid;

        return $this;
    }

    /**
     * Get articletypeid
     *
     * @return integer 
     */
    public function getArticletypeid()
    {
        return $this->articletypeid;
    }

    /**
     * Set suppliercompanyid
     *
     * @param integer $suppliercompanyid
     * @return Article
     */
    public function setSuppliercompanyid($suppliercompanyid)
    {
        $this->suppliercompanyid = $suppliercompanyid;

        return $this;
    }

    /**
     * Get suppliercompanyid
     *
     * @return integer 
     */
    public function getSuppliercompanyid()
    {
        return $this->suppliercompanyid;
    }

    /**
     * Set suppliernumber
     *
     * @param string $suppliernumber
     * @return Article
     */
    public function setSuppliernumber($suppliernumber)
    {
        $this->suppliernumber = $suppliernumber;

        return $this;
    }

    /**
     * Get suppliernumber
     *
     * @return string 
     */
    public function getSuppliernumber()
    {
        return $this->suppliernumber;
    }

    /**
     * Set customspositionid
     *
     * @param integer $customspositionid
     * @return Article
     */
    public function setCustomspositionid($customspositionid)
    {
        $this->customspositionid = $customspositionid;

        return $this;
    }

    /**
     * Get customspositionid
     *
     * @return integer 
     */
    public function getCustomspositionid()
    {
        return $this->customspositionid;
    }

    /**
     * Set unitid
     *
     * @param integer $unitid
     * @return Article
     */
    public function setUnitid($unitid)
    {
        $this->unitid = $unitid;

        return $this;
    }

    /**
     * Get unitid
     *
     * @return integer 
     */
    public function getUnitid()
    {
        return $this->unitid;
    }

    /**
     * Set materialcountryid
     *
     * @param integer $materialcountryid
     * @return Article
     */
    public function setMaterialcountryid($materialcountryid)
    {
        $this->materialcountryid = $materialcountryid;

        return $this;
    }

    /**
     * Get materialcountryid
     *
     * @return integer 
     */
    public function getMaterialcountryid()
    {
        return $this->materialcountryid;
    }

    /**
     * Set workcountryid
     *
     * @param integer $workcountryid
     * @return Article
     */
    public function setWorkcountryid($workcountryid)
    {
        $this->workcountryid = $workcountryid;

        return $this;
    }

    /**
     * Get workcountryid
     *
     * @return integer 
     */
    public function getWorkcountryid()
    {
        return $this->workcountryid;
    }

    /**
     * Set knitcountryid
     *
     * @param integer $knitcountryid
     * @return Article
     */
    public function setKnitcountryid($knitcountryid)
    {
        $this->knitcountryid = $knitcountryid;

        return $this;
    }

    /**
     * Get knitcountryid
     *
     * @return integer 
     */
    public function getKnitcountryid()
    {
        return $this->knitcountryid;
    }

    /**
     * Set variantcolor
     *
     * @param boolean $variantcolor
     * @return Article
     */
    public function setVariantcolor($variantcolor)
    {
        $this->variantcolor = $variantcolor;

        return $this;
    }

    /**
     * Get variantcolor
     *
     * @return boolean 
     */
    public function getVariantcolor()
    {
        return $this->variantcolor;
    }

    /**
     * Set variantdimension
     *
     * @param boolean $variantdimension
     * @return Article
     */
    public function setVariantdimension($variantdimension)
    {
        $this->variantdimension = $variantdimension;

        return $this;
    }

    /**
     * Get variantdimension
     *
     * @return boolean 
     */
    public function getVariantdimension()
    {
        return $this->variantdimension;
    }

    /**
     * Set variantsize
     *
     * @param boolean $variantsize
     * @return Article
     */
    public function setVariantsize($variantsize)
    {
        $this->variantsize = $variantsize;

        return $this;
    }

    /**
     * Get variantsize
     *
     * @return boolean 
     */
    public function getVariantsize()
    {
        return $this->variantsize;
    }

    /**
     * Set variantsortation
     *
     * @param boolean $variantsortation
     * @return Article
     */
    public function setVariantsortation($variantsortation)
    {
        $this->variantsortation = $variantsortation;

        return $this;
    }

    /**
     * Get variantsortation
     *
     * @return boolean 
     */
    public function getVariantsortation()
    {
        return $this->variantsortation;
    }

    /**
     * Set variantcertificate
     *
     * @param boolean $variantcertificate
     * @return Article
     */
    public function setVariantcertificate($variantcertificate)
    {
        $this->variantcertificate = $variantcertificate;

        return $this;
    }

    /**
     * Get variantcertificate
     *
     * @return boolean 
     */
    public function getVariantcertificate()
    {
        return $this->variantcertificate;
    }

    /**
     * Set pricedimensionadjust
     *
     * @param boolean $pricedimensionadjust
     * @return Article
     */
    public function setPricedimensionadjust($pricedimensionadjust)
    {
        $this->pricedimensionadjust = $pricedimensionadjust;

        return $this;
    }

    /**
     * Get pricedimensionadjust
     *
     * @return boolean 
     */
    public function getPricedimensionadjust()
    {
        return $this->pricedimensionadjust;
    }

    /**
     * Set pricestock
     *
     * @param string $pricestock
     * @return Article
     */
    public function setPricestock($pricestock)
    {
        $this->pricestock = $pricestock;

        return $this;
    }

    /**
     * Get pricestock
     *
     * @return string 
     */
    public function getPricestock()
    {
        return $this->pricestock;
    }

    /**
     * Set sketchtype
     *
     * @param string $sketchtype
     * @return Article
     */
    public function setSketchtype($sketchtype)
    {
        $this->sketchtype = $sketchtype;

        return $this;
    }

    /**
     * Get sketchtype
     *
     * @return string 
     */
    public function getSketchtype()
    {
        return $this->sketchtype;
    }

    /**
     * Set sketchuserid
     *
     * @param integer $sketchuserid
     * @return Article
     */
    public function setSketchuserid($sketchuserid)
    {
        $this->sketchuserid = $sketchuserid;

        return $this;
    }

    /**
     * Get sketchuserid
     *
     * @return integer 
     */
    public function getSketchuserid()
    {
        return $this->sketchuserid;
    }

    /**
     * Set sketchdate
     *
     * @param \DateTime $sketchdate
     * @return Article
     */
    public function setSketchdate($sketchdate)
    {
        $this->sketchdate = $sketchdate;

        return $this;
    }

    /**
     * Get sketchdate
     *
     * @return \DateTime 
     */
    public function getSketchdate()
    {
        return $this->sketchdate;
    }

    /**
     * Set printnumber
     *
     * @param string $printnumber
     * @return Article
     */
    public function setPrintnumber($printnumber)
    {
        $this->printnumber = $printnumber;

        return $this;
    }

    /**
     * Get printnumber
     *
     * @return string 
     */
    public function getPrintnumber()
    {
        return $this->printnumber;
    }

    /**
     * Set widthusable
     *
     * @param integer $widthusable
     * @return Article
     */
    public function setWidthusable($widthusable)
    {
        $this->widthusable = $widthusable;

        return $this;
    }

    /**
     * Get widthusable
     *
     * @return integer 
     */
    public function getWidthusable()
    {
        return $this->widthusable;
    }

    /**
     * Set widthfull
     *
     * @param integer $widthfull
     * @return Article
     */
    public function setWidthfull($widthfull)
    {
        $this->widthfull = $widthfull;

        return $this;
    }

    /**
     * Get widthfull
     *
     * @return integer 
     */
    public function getWidthfull()
    {
        return $this->widthfull;
    }

    /**
     * Set m2weight
     *
     * @param integer $m2weight
     * @return Article
     */
    public function setM2weight($m2weight)
    {
        $this->m2weight = $m2weight;

        return $this;
    }

    /**
     * Get m2weight
     *
     * @return integer 
     */
    public function getM2weight()
    {
        return $this->m2weight;
    }

    /**
     * Set shrinkagewashlength
     *
     * @param string $shrinkagewashlength
     * @return Article
     */
    public function setShrinkagewashlength($shrinkagewashlength)
    {
        $this->shrinkagewashlength = $shrinkagewashlength;

        return $this;
    }

    /**
     * Get shrinkagewashlength
     *
     * @return string 
     */
    public function getShrinkagewashlength()
    {
        return $this->shrinkagewashlength;
    }

    /**
     * Set shrinkagewashwidth
     *
     * @param string $shrinkagewashwidth
     * @return Article
     */
    public function setShrinkagewashwidth($shrinkagewashwidth)
    {
        $this->shrinkagewashwidth = $shrinkagewashwidth;

        return $this;
    }

    /**
     * Get shrinkagewashwidth
     *
     * @return string 
     */
    public function getShrinkagewashwidth()
    {
        return $this->shrinkagewashwidth;
    }

    /**
     * Set shrinkageworklength
     *
     * @param string $shrinkageworklength
     * @return Article
     */
    public function setShrinkageworklength($shrinkageworklength)
    {
        $this->shrinkageworklength = $shrinkageworklength;

        return $this;
    }

    /**
     * Get shrinkageworklength
     *
     * @return string 
     */
    public function getShrinkageworklength()
    {
        return $this->shrinkageworklength;
    }

    /**
     * Set shrinkageworkwidth
     *
     * @param string $shrinkageworkwidth
     * @return Article
     */
    public function setShrinkageworkwidth($shrinkageworkwidth)
    {
        $this->shrinkageworkwidth = $shrinkageworkwidth;

        return $this;
    }

    /**
     * Get shrinkageworkwidth
     *
     * @return string 
     */
    public function getShrinkageworkwidth()
    {
        return $this->shrinkageworkwidth;
    }

    /**
     * Set washinginstruction
     *
     * @param string $washinginstruction
     * @return Article
     */
    public function setWashinginstruction($washinginstruction)
    {
        $this->washinginstruction = $washinginstruction;

        return $this;
    }

    /**
     * Get washinginstruction
     *
     * @return string 
     */
    public function getWashinginstruction()
    {
        return $this->washinginstruction;
    }

    /**
     * Set colorfastnesswater
     *
     * @param string $colorfastnesswater
     * @return Article
     */
    public function setColorfastnesswater($colorfastnesswater)
    {
        $this->colorfastnesswater = $colorfastnesswater;

        return $this;
    }

    /**
     * Get colorfastnesswater
     *
     * @return string 
     */
    public function getColorfastnesswater()
    {
        return $this->colorfastnesswater;
    }

    /**
     * Set colorfastnesswash
     *
     * @param string $colorfastnesswash
     * @return Article
     */
    public function setColorfastnesswash($colorfastnesswash)
    {
        $this->colorfastnesswash = $colorfastnesswash;

        return $this;
    }

    /**
     * Get colorfastnesswash
     *
     * @return string 
     */
    public function getColorfastnesswash()
    {
        return $this->colorfastnesswash;
    }

    /**
     * Set colorfastnesslight
     *
     * @param string $colorfastnesslight
     * @return Article
     */
    public function setColorfastnesslight($colorfastnesslight)
    {
        $this->colorfastnesslight = $colorfastnesslight;

        return $this;
    }

    /**
     * Get colorfastnesslight
     *
     * @return string 
     */
    public function getColorfastnesslight()
    {
        return $this->colorfastnesslight;
    }

    /**
     * Set colorfastnessperspiration
     *
     * @param string $colorfastnessperspiration
     * @return Article
     */
    public function setColorfastnessperspiration($colorfastnessperspiration)
    {
        $this->colorfastnessperspiration = $colorfastnessperspiration;

        return $this;
    }

    /**
     * Get colorfastnessperspiration
     *
     * @return string 
     */
    public function getColorfastnessperspiration()
    {
        return $this->colorfastnessperspiration;
    }

    /**
     * Set colorfastnesschlor
     *
     * @param string $colorfastnesschlor
     * @return Article
     */
    public function setColorfastnesschlor($colorfastnesschlor)
    {
        $this->colorfastnesschlor = $colorfastnesschlor;

        return $this;
    }

    /**
     * Get colorfastnesschlor
     *
     * @return string 
     */
    public function getColorfastnesschlor()
    {
        return $this->colorfastnesschlor;
    }

    /**
     * Set colorfastnesssea
     *
     * @param string $colorfastnesssea
     * @return Article
     */
    public function setColorfastnesssea($colorfastnesssea)
    {
        $this->colorfastnesssea = $colorfastnesssea;

        return $this;
    }

    /**
     * Get colorfastnesssea
     *
     * @return string 
     */
    public function getColorfastnesssea()
    {
        return $this->colorfastnesssea;
    }

    /**
     * Set colorfastnessdurability
     *
     * @param string $colorfastnessdurability
     * @return Article
     */
    public function setColorfastnessdurability($colorfastnessdurability)
    {
        $this->colorfastnessdurability = $colorfastnessdurability;

        return $this;
    }

    /**
     * Get colorfastnessdurability
     *
     * @return string 
     */
    public function getColorfastnessdurability()
    {
        return $this->colorfastnessdurability;
    }

    /**
     * Set rubbingdry
     *
     * @param string $rubbingdry
     * @return Article
     */
    public function setRubbingdry($rubbingdry)
    {
        $this->rubbingdry = $rubbingdry;

        return $this;
    }

    /**
     * Get rubbingdry
     *
     * @return string 
     */
    public function getRubbingdry()
    {
        return $this->rubbingdry;
    }

    /**
     * Set rubbingwet
     *
     * @param string $rubbingwet
     * @return Article
     */
    public function setRubbingwet($rubbingwet)
    {
        $this->rubbingwet = $rubbingwet;

        return $this;
    }

    /**
     * Get rubbingwet
     *
     * @return string 
     */
    public function getRubbingwet()
    {
        return $this->rubbingwet;
    }

    /**
     * Set fabricarticleid
     *
     * @param integer $fabricarticleid
     * @return Article
     */
    public function setFabricarticleid($fabricarticleid)
    {
        $this->fabricarticleid = $fabricarticleid;

        return $this;
    }

    /**
     * Get fabricarticleid
     *
     * @return integer 
     */
    public function getFabricarticleid()
    {
        return $this->fabricarticleid;
    }

    /**
     * Set deliverycomment
     *
     * @param string $deliverycomment
     * @return Article
     */
    public function setDeliverycomment($deliverycomment)
    {
        $this->deliverycomment = $deliverycomment;

        return $this;
    }

    /**
     * Get deliverycomment
     *
     * @return string 
     */
    public function getDeliverycomment()
    {
        return $this->deliverycomment;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return Article
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set salesprice
     *
     * @param string $salesprice
     * @return Article
     */
    public function setSalesprice($salesprice)
    {
        $this->salesprice = $salesprice;

        return $this;
    }

    /**
     * Get salesprice
     *
     * @return string 
     */
    public function getSalesprice()
    {
        return $this->salesprice;
    }

    /**
     * Set listhidden
     *
     * @param integer $listhidden
     * @return Article
     */
    public function setListhidden($listhidden)
    {
        $this->listhidden = $listhidden;

        return $this;
    }

    /**
     * Get listhidden
     *
     * @return integer 
     */
    public function getListhidden()
    {
        return $this->listhidden;
    }

    /**
     * Set receiveddate
     *
     * @param \DateTime $receiveddate
     * @return Article
     */
    public function setReceiveddate($receiveddate)
    {
        $this->receiveddate = $receiveddate;

        return $this;
    }

    /**
     * Get receiveddate
     *
     * @return \DateTime 
     */
    public function getReceiveddate()
    {
        return $this->receiveddate;
    }

    /**
     * Set ordereddate
     *
     * @param \DateTime $ordereddate
     * @return Article
     */
    public function setOrdereddate($ordereddate)
    {
        $this->ordereddate = $ordereddate;

        return $this;
    }

    /**
     * Get ordereddate
     *
     * @return \DateTime 
     */
    public function getOrdereddate()
    {
        return $this->ordereddate;
    }

    /**
     * Set rejecteddate
     *
     * @param \DateTime $rejecteddate
     * @return Article
     */
    public function setRejecteddate($rejecteddate)
    {
        $this->rejecteddate = $rejecteddate;

        return $this;
    }

    /**
     * Get rejecteddate
     *
     * @return \DateTime 
     */
    public function getRejecteddate()
    {
        return $this->rejecteddate;
    }

    /**
     * Set approveddate
     *
     * @param \DateTime $approveddate
     * @return Article
     */
    public function setApproveddate($approveddate)
    {
        $this->approveddate = $approveddate;

        return $this;
    }

    /**
     * Get approveddate
     *
     * @return \DateTime 
     */
    public function getApproveddate()
    {
        return $this->approveddate;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Article
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer 
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set usepdmstyle
     *
     * @param boolean $usepdmstyle
     * @return Article
     */
    public function setUsepdmstyle($usepdmstyle)
    {
        $this->usepdmstyle = $usepdmstyle;

        return $this;
    }

    /**
     * Get usepdmstyle
     *
     * @return boolean 
     */
    public function getUsepdmstyle()
    {
        return $this->usepdmstyle;
    }

    /**
     * Set hasstyle
     *
     * @param boolean $hasstyle
     * @return Article
     */
    public function setHasstyle($hasstyle)
    {
        $this->hasstyle = $hasstyle;

        return $this;
    }

    /**
     * Get hasstyle
     *
     * @return boolean 
     */
    public function getHasstyle()
    {
        return $this->hasstyle;
    }

    /**
     * Set requisitiontext
     *
     * @param string $requisitiontext
     * @return Article
     */
    public function setRequisitiontext($requisitiontext)
    {
        $this->requisitiontext = $requisitiontext;

        return $this;
    }

    /**
     * Get requisitiontext
     *
     * @return string 
     */
    public function getRequisitiontext()
    {
        return $this->requisitiontext;
    }

    /**
     * Set costcurrencyid
     *
     * @param integer $costcurrencyid
     * @return Article
     */
    public function setCostcurrencyid($costcurrencyid)
    {
        $this->costcurrencyid = $costcurrencyid;

        return $this;
    }

    /**
     * Get costcurrencyid
     *
     * @return integer 
     */
    public function getCostcurrencyid()
    {
        return $this->costcurrencyid;
    }

    /**
     * Set customercompanyid
     *
     * @param integer $customercompanyid
     * @return Article
     */
    public function setCustomercompanyid($customercompanyid)
    {
        $this->customercompanyid = $customercompanyid;

        return $this;
    }

    /**
     * Get customercompanyid
     *
     * @return integer 
     */
    public function getCustomercompanyid()
    {
        return $this->customercompanyid;
    }

    /**
     * Set importidref
     *
     * @param integer $importidref
     * @return Article
     */
    public function setImportidref($importidref)
    {
        $this->importidref = $importidref;

        return $this;
    }

    /**
     * Get importidref
     *
     * @return integer 
     */
    public function getImportidref()
    {
        return $this->importidref;
    }

    /**
     * Set ownercompanyid
     *
     * @param integer $ownercompanyid
     * @return Article
     */
    public function setOwnercompanyid($ownercompanyid)
    {
        $this->ownercompanyid = $ownercompanyid;

        return $this;
    }

    /**
     * Get importidref
     *
     * @return integer 
     */
    public function getOwnercompanyid()
    {
        return $this->ownercompanyid;
    }
}
