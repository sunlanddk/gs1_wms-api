<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class Order
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $consolidatedid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $carrierid;

    /**
     * @var integer
     */
    private $deliverytermid;

    /**
     * @var integer
     */
    private $paymenttermid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $exchangerate;

    /**
     * @var string
     */
    private $invoiceheader;

    /**
     * @var string
     */
    private $invoicefooter;

    /**
     * @var string
     */
    private $altcompanyname;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var integer
     */
    private $orderdoneid;

    /**
     * @var boolean
     */
    private $draft;

    /**
     * @var integer
     */
    private $tocompanyid;

    /**
     * @var string
     */
    private $instructions;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $shippingshop;

     /**
     * @var integer
     */
    private $ordertypeid;

    /**
     * @var \DateTime
     */
    private $receiveddate;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * @var boolean
     */
    private $variantcodes;

    /**
     * @var integer
     */
    private $purchaseorderid;

    /**
     * @var integer
     */
    private $topurchaseorderid;

    /**
     * @var integer
     */
    private $webshopno;

    /**
     * @var boolean
     */
    private $proposal;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var integer
     */
    private $languageid;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consolidatedid
     *
     * @param integer $consolidatedid
     * @return Order
     */
    public function setConsolidatedid($consolidatedid)
    {
        $this->consolidatedid = $consolidatedid;

        return $this;
    }

    /**
     * Get consolidatedid
     *
     * @return integer
     */
    public function getConsolidatedid()
    {
        return $this->consolidatedid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Order
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Order
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Order
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Order
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Order
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Order
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Order
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set carrierid
     *
     * @param integer $carrierid
     * @return Order
     */
    public function setCarrierid($carrierid)
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    /**
     * Get carrierid
     *
     * @return integer
     */
    public function getCarrierid()
    {
        return $this->carrierid;
    }

    /**
     * Set deliverytermid
     *
     * @param integer $deliverytermid
     * @return Order
     */
    public function setDeliverytermid($deliverytermid)
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    /**
     * Get deliverytermid
     *
     * @return integer
     */
    public function getDeliverytermid()
    {
        return $this->deliverytermid;
    }

    /**
     * Set paymenttermid
     *
     * @param integer $paymenttermid
     * @return Order
     */
    public function setPaymenttermid($paymenttermid)
    {
        $this->paymenttermid = $paymenttermid;

        return $this;
    }

    /**
     * Get paymenttermid
     *
     * @return integer
     */
    public function getPaymenttermid()
    {
        return $this->paymenttermid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Order
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Order
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Order
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Order
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set exchangerate
     *
     * @param string $exchangerate
     * @return Order
     */
    public function setExchangerate($exchangerate)
    {
        $this->exchangerate = $exchangerate;

        return $this;
    }

    /**
     * Get exchangerate
     *
     * @return string
     */
    public function getExchangerate()
    {
        return $this->exchangerate;
    }

    /**
     * Set invoiceheader
     *
     * @param string $invoiceheader
     * @return Order
     */
    public function setInvoiceheader($invoiceheader)
    {
        $this->invoiceheader = $invoiceheader;

        return $this;
    }

    /**
     * Get invoiceheader
     *
     * @return string
     */
    public function getInvoiceheader()
    {
        return $this->invoiceheader;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Order
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

    /**
     * Set altcompanyname
     *
     * @param string $altcompanyname
     * @return Order
     */
    public function setAltcompanyname($altcompanyname)
    {
        $this->altcompanyname = $altcompanyname;

        return $this;
    }

    /**
     * Get altcompanyname
     *
     * @return string
     */
    public function getAltcompanyname()
    {
        return $this->altcompanyname;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Order
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Order
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Order
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Order
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Order
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Order
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Order
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Order
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Order
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Order
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Order
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set orderdoneid
     *
     * @param integer $orderdoneid
     * @return Order
     */
    public function setOrderdoneid($orderdoneid)
    {
        $this->orderdoneid = $orderdoneid;

        return $this;
    }

    /**
     * Get orderdoneid
     *
     * @return integer
     */
    public function getOrderdoneid()
    {
        return $this->orderdoneid;
    }

    /**
     * Set draft
     *
     * @param boolean $draft
     * @return Order
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * Get draft
     *
     * @return boolean
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * Set tocompanyid
     *
     * @param integer $tocompanyid
     * @return Order
     */
    public function setTocompanyid($tocompanyid)
    {
        $this->tocompanyid = $tocompanyid;

        return $this;
    }

    /**
     * Get tocompanyid
     *
     * @return integer
     */
    public function getTocompanyid()
    {
        return $this->tocompanyid;
    }

    /**
     * Set instructions
     *
     * @param string $instructions
     * @return Order
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Get instructions
     *
     * @return string
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return String
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set shippingshop
     *
     * @param string $shippingshop
     * @return String
     */
    public function setShippingShop($shippingshop)
    {
        $this->shippingshop = $shippingshop;

        return $this;
    }

    /**
     * Get shippingshop
     *
     * @return string 
     */
    public function getShippingShop()
    {
        return $this->shippingshop;
    }

    /**
     * Set ordertypeid
     *
     * @param integer $ordertypeid
     * @return Order
     */
    public function setOrdertypeid($ordertypeid)
    {
        $this->ordertypeid = $ordertypeid;

        return $this;
    }

    /**
     * Get ordertypeid
     *
     * @return integer
     */
    public function getOrdertypeid()
    {
        return $this->ordertypeid;
    }

    /**
     * Set receiveddate
     *
     * @param \DateTime $receiveddate
     * @return Order
     */
    public function setReceiveddate($receiveddate)
    {
        $this->receiveddate = $receiveddate;

        return $this;
    }

    /**
     * Get receiveddate
     *
     * @return \DateTime
     */
    public function getReceiveddate()
    {
        return $this->receiveddate;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Order
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }

    /**
     * Set variantcodes
     *
     * @param boolean $variantcodes
     * @return Order
     */
    public function setVariantcodes($variantcodes)
    {
        $this->variantcodes = $variantcodes;

        return $this;
    }

    /**
     * Get variantcodes
     *
     * @return boolean
     */
    public function getVariantcodes()
    {
        return $this->variantcodes;
    }

    /**
     * Set purchaseorderid
     *
     * @param integer $purchaseorderid
     * @return Order
     */
    public function setPurchaseorderid($purchaseorderid)
    {
        $this->purchaseorderid = $purchaseorderid;

        return $this;
    }

    /**
     * Get purchaseorderid
     *
     * @return integer
     */
    public function getPurchaseorderid()
    {
        return $this->purchaseorderid;
    }

    /**
     * Set topurchaseorderid
     *
     * @param integer $topurchaseorderid
     * @return Order
     */
    public function setTopurchaseorderid($topurchaseorderid)
    {
        $this->topurchaseorderid = $topurchaseorderid;

        return $this;
    }

    /**
     * Get topurchaseorderid
     *
     * @return integer
     */
    public function getTopurchaseorderid()
    {
        return $this->topurchaseorderid;
    }

    /**
     * Set webshopno
     *
     * @param integer $webshopno
     * @return Order
     */
    public function setWebshopno($webshopno)
    {
        $this->webshopno = $webshopno;

        return $this;
    }

    /**
     * Get webshopno
     *
     * @return integer
     */
    public function getWebshopno()
    {
        return $this->webshopno;
    }

    /**
     * Set proposal
     *
     * @param boolean $proposal
     * @return Order
     */
    public function setProposal($proposal)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return boolean
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return Order
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }
}
