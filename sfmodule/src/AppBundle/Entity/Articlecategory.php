<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlecategory
 */
class Articlecategory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articletypeid;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $articlecategorygroupid;

    /**
     * @var string
     */
    private $topbottom;

    /**
     * @var integer
     */
    private $customspositionid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articletypeid
     *
     * @param integer $articletypeid
     * @return Articlecategory
     */
    public function setArticletypeid($articletypeid)
    {
        $this->articletypeid = $articletypeid;

        return $this;
    }

    /**
     * Get articletypeid
     *
     * @return integer 
     */
    public function getArticletypeid()
    {
        return $this->articletypeid;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Articlecategory
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Articlecategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set articlecategorygroupid
     *
     * @param integer $articlecategorygroupid
     * @return Articlecategory
     */
    public function setArticlecategorygroupid($articlecategorygroupid)
    {
        $this->articlecategorygroupid = $articlecategorygroupid;

        return $this;
    }

    /**
     * Get articlecategorygroupid
     *
     * @return integer 
     */
    public function getArticlecategorygroupid()
    {
        return $this->articlecategorygroupid;
    }

    /**
     * Set topbottom
     *
     * @param string $topbottom
     * @return Articlecategory
     */
    public function setTopbottom($topbottom)
    {
        $this->topbottom = $topbottom;

        return $this;
    }

    /**
     * Get topbottom
     *
     * @return string 
     */
    public function getTopbottom()
    {
        return $this->topbottom;
    }

    /**
     * Set customspositionid
     *
     * @param integer $customspositionid
     * @return Articlecategory
     */
    public function setCustomspositionid($customspositionid)
    {
        $this->customspositionid = $customspositionid;

        return $this;
    }

    /**
     * Get customspositionid
     *
     * @return integer 
     */
    public function getCustomspositionid()
    {
        return $this->customspositionid;
    }
}
