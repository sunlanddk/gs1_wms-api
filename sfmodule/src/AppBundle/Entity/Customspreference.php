<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customspreference
 */
class Customspreference
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $materialcountrygroup;

    /**
     * @var boolean
     */
    private $materialdrop;

    /**
     * @var string
     */
    private $knitcountrygroup;

    /**
     * @var boolean
     */
    private $knitdrop;

    /**
     * @var string
     */
    private $workcountrygroup;

    /**
     * @var boolean
     */
    private $workdrop;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Customspreference
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Customspreference
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Customspreference
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Customspreference
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Customspreference
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set materialcountrygroup
     *
     * @param string $materialcountrygroup
     * @return Customspreference
     */
    public function setMaterialcountrygroup($materialcountrygroup)
    {
        $this->materialcountrygroup = $materialcountrygroup;

        return $this;
    }

    /**
     * Get materialcountrygroup
     *
     * @return string 
     */
    public function getMaterialcountrygroup()
    {
        return $this->materialcountrygroup;
    }

    /**
     * Set materialdrop
     *
     * @param boolean $materialdrop
     * @return Customspreference
     */
    public function setMaterialdrop($materialdrop)
    {
        $this->materialdrop = $materialdrop;

        return $this;
    }

    /**
     * Get materialdrop
     *
     * @return boolean 
     */
    public function getMaterialdrop()
    {
        return $this->materialdrop;
    }

    /**
     * Set knitcountrygroup
     *
     * @param string $knitcountrygroup
     * @return Customspreference
     */
    public function setKnitcountrygroup($knitcountrygroup)
    {
        $this->knitcountrygroup = $knitcountrygroup;

        return $this;
    }

    /**
     * Get knitcountrygroup
     *
     * @return string 
     */
    public function getKnitcountrygroup()
    {
        return $this->knitcountrygroup;
    }

    /**
     * Set knitdrop
     *
     * @param boolean $knitdrop
     * @return Customspreference
     */
    public function setKnitdrop($knitdrop)
    {
        $this->knitdrop = $knitdrop;

        return $this;
    }

    /**
     * Get knitdrop
     *
     * @return boolean 
     */
    public function getKnitdrop()
    {
        return $this->knitdrop;
    }

    /**
     * Set workcountrygroup
     *
     * @param string $workcountrygroup
     * @return Customspreference
     */
    public function setWorkcountrygroup($workcountrygroup)
    {
        $this->workcountrygroup = $workcountrygroup;

        return $this;
    }

    /**
     * Get workcountrygroup
     *
     * @return string 
     */
    public function getWorkcountrygroup()
    {
        return $this->workcountrygroup;
    }

    /**
     * Set workdrop
     *
     * @param boolean $workdrop
     * @return Customspreference
     */
    public function setWorkdrop($workdrop)
    {
        $this->workdrop = $workdrop;

        return $this;
    }

    /**
     * Get workdrop
     *
     * @return boolean 
     */
    public function getWorkdrop()
    {
        return $this->workdrop;
    }
}
