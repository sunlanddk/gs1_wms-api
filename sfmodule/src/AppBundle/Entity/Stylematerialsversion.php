<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylematerialsversion
 */
class Stylematerialsversion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $constructiondone;

    /**
     * @var boolean
     */
    private $fabricconsumptiondone;

    /**
     * @var boolean
     */
    private $threadconsumptiondone;

    /**
     * @var boolean
     */
    private $qualitycontroldone;

    /**
     * @var \DateTime
     */
    private $constructiondonedate;

    /**
     * @var integer
     */
    private $constructiondoneuserid;

    /**
     * @var \DateTime
     */
    private $fabricconsumptiondonedate;

    /**
     * @var integer
     */
    private $fabricconsumptiondoneuserid;

    /**
     * @var \DateTime
     */
    private $threadconsumptiondonedate;

    /**
     * @var integer
     */
    private $threadconsumptiondoneuserid;

    /**
     * @var \DateTime
     */
    private $qualitycontroldonedate;

    /**
     * @var integer
     */
    private $qualitycontroldoneuserid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set constructiondone
     *
     * @param boolean $constructiondone
     * @return Stylematerialsversion
     */
    public function setConstructiondone($constructiondone)
    {
        $this->constructiondone = $constructiondone;

        return $this;
    }

    /**
     * Get constructiondone
     *
     * @return boolean 
     */
    public function getConstructiondone()
    {
        return $this->constructiondone;
    }

    /**
     * Set fabricconsumptiondone
     *
     * @param boolean $fabricconsumptiondone
     * @return Stylematerialsversion
     */
    public function setFabricconsumptiondone($fabricconsumptiondone)
    {
        $this->fabricconsumptiondone = $fabricconsumptiondone;

        return $this;
    }

    /**
     * Get fabricconsumptiondone
     *
     * @return boolean 
     */
    public function getFabricconsumptiondone()
    {
        return $this->fabricconsumptiondone;
    }

    /**
     * Set threadconsumptiondone
     *
     * @param boolean $threadconsumptiondone
     * @return Stylematerialsversion
     */
    public function setThreadconsumptiondone($threadconsumptiondone)
    {
        $this->threadconsumptiondone = $threadconsumptiondone;

        return $this;
    }

    /**
     * Get threadconsumptiondone
     *
     * @return boolean 
     */
    public function getThreadconsumptiondone()
    {
        return $this->threadconsumptiondone;
    }

    /**
     * Set qualitycontroldone
     *
     * @param boolean $qualitycontroldone
     * @return Stylematerialsversion
     */
    public function setQualitycontroldone($qualitycontroldone)
    {
        $this->qualitycontroldone = $qualitycontroldone;

        return $this;
    }

    /**
     * Get qualitycontroldone
     *
     * @return boolean 
     */
    public function getQualitycontroldone()
    {
        return $this->qualitycontroldone;
    }

    /**
     * Set constructiondonedate
     *
     * @param \DateTime $constructiondonedate
     * @return Stylematerialsversion
     */
    public function setConstructiondonedate($constructiondonedate)
    {
        $this->constructiondonedate = $constructiondonedate;

        return $this;
    }

    /**
     * Get constructiondonedate
     *
     * @return \DateTime 
     */
    public function getConstructiondonedate()
    {
        return $this->constructiondonedate;
    }

    /**
     * Set constructiondoneuserid
     *
     * @param integer $constructiondoneuserid
     * @return Stylematerialsversion
     */
    public function setConstructiondoneuserid($constructiondoneuserid)
    {
        $this->constructiondoneuserid = $constructiondoneuserid;

        return $this;
    }

    /**
     * Get constructiondoneuserid
     *
     * @return integer 
     */
    public function getConstructiondoneuserid()
    {
        return $this->constructiondoneuserid;
    }

    /**
     * Set fabricconsumptiondonedate
     *
     * @param \DateTime $fabricconsumptiondonedate
     * @return Stylematerialsversion
     */
    public function setFabricconsumptiondonedate($fabricconsumptiondonedate)
    {
        $this->fabricconsumptiondonedate = $fabricconsumptiondonedate;

        return $this;
    }

    /**
     * Get fabricconsumptiondonedate
     *
     * @return \DateTime 
     */
    public function getFabricconsumptiondonedate()
    {
        return $this->fabricconsumptiondonedate;
    }

    /**
     * Set fabricconsumptiondoneuserid
     *
     * @param integer $fabricconsumptiondoneuserid
     * @return Stylematerialsversion
     */
    public function setFabricconsumptiondoneuserid($fabricconsumptiondoneuserid)
    {
        $this->fabricconsumptiondoneuserid = $fabricconsumptiondoneuserid;

        return $this;
    }

    /**
     * Get fabricconsumptiondoneuserid
     *
     * @return integer 
     */
    public function getFabricconsumptiondoneuserid()
    {
        return $this->fabricconsumptiondoneuserid;
    }

    /**
     * Set threadconsumptiondonedate
     *
     * @param \DateTime $threadconsumptiondonedate
     * @return Stylematerialsversion
     */
    public function setThreadconsumptiondonedate($threadconsumptiondonedate)
    {
        $this->threadconsumptiondonedate = $threadconsumptiondonedate;

        return $this;
    }

    /**
     * Get threadconsumptiondonedate
     *
     * @return \DateTime 
     */
    public function getThreadconsumptiondonedate()
    {
        return $this->threadconsumptiondonedate;
    }

    /**
     * Set threadconsumptiondoneuserid
     *
     * @param integer $threadconsumptiondoneuserid
     * @return Stylematerialsversion
     */
    public function setThreadconsumptiondoneuserid($threadconsumptiondoneuserid)
    {
        $this->threadconsumptiondoneuserid = $threadconsumptiondoneuserid;

        return $this;
    }

    /**
     * Get threadconsumptiondoneuserid
     *
     * @return integer 
     */
    public function getThreadconsumptiondoneuserid()
    {
        return $this->threadconsumptiondoneuserid;
    }

    /**
     * Set qualitycontroldonedate
     *
     * @param \DateTime $qualitycontroldonedate
     * @return Stylematerialsversion
     */
    public function setQualitycontroldonedate($qualitycontroldonedate)
    {
        $this->qualitycontroldonedate = $qualitycontroldonedate;

        return $this;
    }

    /**
     * Get qualitycontroldonedate
     *
     * @return \DateTime 
     */
    public function getQualitycontroldonedate()
    {
        return $this->qualitycontroldonedate;
    }

    /**
     * Set qualitycontroldoneuserid
     *
     * @param integer $qualitycontroldoneuserid
     * @return Stylematerialsversion
     */
    public function setQualitycontroldoneuserid($qualitycontroldoneuserid)
    {
        $this->qualitycontroldoneuserid = $qualitycontroldoneuserid;

        return $this;
    }

    /**
     * Get qualitycontroldoneuserid
     *
     * @return integer 
     */
    public function getQualitycontroldoneuserid()
    {
        return $this->qualitycontroldoneuserid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylematerialsversion
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylematerialsversion
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylematerialsversion
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylematerialsversion
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
