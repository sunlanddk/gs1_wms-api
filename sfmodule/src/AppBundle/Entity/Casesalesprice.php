<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Casesalesprice
 */
class Casesalesprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $casecostpriceid;

    /**
     * @var integer
     */
    private $priceversion;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $salesprice;

    /**
     * @var string
     */
    private $exchangerate;

    /**
     * @var string
     */
    private $cashdiscount;

    /**
     * @var string
     */
    private $agentfee;

    /**
     * @var string
     */
    private $bonus;

    /**
     * @var string
     */
    private $freightorhandling;

    /**
     * @var string
     */
    private $stockcost;

    /**
     * @var boolean
     */
    private $approve;

    /**
     * @var \DateTime
     */
    private $approvedate;

    /**
     * @var integer
     */
    private $approveuserid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $quantity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set casecostpriceid
     *
     * @param integer $casecostpriceid
     * @return Casesalesprice
     */
    public function setCasecostpriceid($casecostpriceid)
    {
        $this->casecostpriceid = $casecostpriceid;

        return $this;
    }

    /**
     * Get casecostpriceid
     *
     * @return integer 
     */
    public function getCasecostpriceid()
    {
        return $this->casecostpriceid;
    }

    /**
     * Set priceversion
     *
     * @param integer $priceversion
     * @return Casesalesprice
     */
    public function setPriceversion($priceversion)
    {
        $this->priceversion = $priceversion;

        return $this;
    }

    /**
     * Get priceversion
     *
     * @return integer 
     */
    public function getPriceversion()
    {
        return $this->priceversion;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Casesalesprice
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set salesprice
     *
     * @param string $salesprice
     * @return Casesalesprice
     */
    public function setSalesprice($salesprice)
    {
        $this->salesprice = $salesprice;

        return $this;
    }

    /**
     * Get salesprice
     *
     * @return string 
     */
    public function getSalesprice()
    {
        return $this->salesprice;
    }

    /**
     * Set exchangerate
     *
     * @param string $exchangerate
     * @return Casesalesprice
     */
    public function setExchangerate($exchangerate)
    {
        $this->exchangerate = $exchangerate;

        return $this;
    }

    /**
     * Get exchangerate
     *
     * @return string 
     */
    public function getExchangerate()
    {
        return $this->exchangerate;
    }

    /**
     * Set cashdiscount
     *
     * @param string $cashdiscount
     * @return Casesalesprice
     */
    public function setCashdiscount($cashdiscount)
    {
        $this->cashdiscount = $cashdiscount;

        return $this;
    }

    /**
     * Get cashdiscount
     *
     * @return string 
     */
    public function getCashdiscount()
    {
        return $this->cashdiscount;
    }

    /**
     * Set agentfee
     *
     * @param string $agentfee
     * @return Casesalesprice
     */
    public function setAgentfee($agentfee)
    {
        $this->agentfee = $agentfee;

        return $this;
    }

    /**
     * Get agentfee
     *
     * @return string 
     */
    public function getAgentfee()
    {
        return $this->agentfee;
    }

    /**
     * Set bonus
     *
     * @param string $bonus
     * @return Casesalesprice
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return string 
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set freightorhandling
     *
     * @param string $freightorhandling
     * @return Casesalesprice
     */
    public function setFreightorhandling($freightorhandling)
    {
        $this->freightorhandling = $freightorhandling;

        return $this;
    }

    /**
     * Get freightorhandling
     *
     * @return string 
     */
    public function getFreightorhandling()
    {
        return $this->freightorhandling;
    }

    /**
     * Set stockcost
     *
     * @param string $stockcost
     * @return Casesalesprice
     */
    public function setStockcost($stockcost)
    {
        $this->stockcost = $stockcost;

        return $this;
    }

    /**
     * Get stockcost
     *
     * @return string 
     */
    public function getStockcost()
    {
        return $this->stockcost;
    }

    /**
     * Set approve
     *
     * @param boolean $approve
     * @return Casesalesprice
     */
    public function setApprove($approve)
    {
        $this->approve = $approve;

        return $this;
    }

    /**
     * Get approve
     *
     * @return boolean 
     */
    public function getApprove()
    {
        return $this->approve;
    }

    /**
     * Set approvedate
     *
     * @param \DateTime $approvedate
     * @return Casesalesprice
     */
    public function setApprovedate($approvedate)
    {
        $this->approvedate = $approvedate;

        return $this;
    }

    /**
     * Get approvedate
     *
     * @return \DateTime 
     */
    public function getApprovedate()
    {
        return $this->approvedate;
    }

    /**
     * Set approveuserid
     *
     * @param integer $approveuserid
     * @return Casesalesprice
     */
    public function setApproveuserid($approveuserid)
    {
        $this->approveuserid = $approveuserid;

        return $this;
    }

    /**
     * Get approveuserid
     *
     * @return integer 
     */
    public function getApproveuserid()
    {
        return $this->approveuserid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Casesalesprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Casesalesprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Casesalesprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Casesalesprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Casesalesprice
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Casesalesprice
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
