<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Styleoperationcostprice
 */
class Styleoperationcostprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $casecostpriceid;

    /**
     * @var integer
     */
    private $styleoperationid;

    /**
     * @var string
     */
    private $surplus;

    /**
     * @var string
     */
    private $actualunitprice;

    /**
     * @var string
     */
    private $wastage;

    /**
     * @var string
     */
    private $actualusage;

    /**
     * @var string
     */
    private $totalcostprice;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set casecostpriceid
     *
     * @param integer $casecostpriceid
     * @return Styleoperationcostprice
     */
    public function setCasecostpriceid($casecostpriceid)
    {
        $this->casecostpriceid = $casecostpriceid;

        return $this;
    }

    /**
     * Get casecostpriceid
     *
     * @return integer 
     */
    public function getCasecostpriceid()
    {
        return $this->casecostpriceid;
    }

    /**
     * Set styleoperationid
     *
     * @param integer $styleoperationid
     * @return Styleoperationcostprice
     */
    public function setStyleoperationid($styleoperationid)
    {
        $this->styleoperationid = $styleoperationid;

        return $this;
    }

    /**
     * Get styleoperationid
     *
     * @return integer 
     */
    public function getStyleoperationid()
    {
        return $this->styleoperationid;
    }

    /**
     * Set surplus
     *
     * @param string $surplus
     * @return Styleoperationcostprice
     */
    public function setSurplus($surplus)
    {
        $this->surplus = $surplus;

        return $this;
    }

    /**
     * Get surplus
     *
     * @return string 
     */
    public function getSurplus()
    {
        return $this->surplus;
    }

    /**
     * Set actualunitprice
     *
     * @param string $actualunitprice
     * @return Styleoperationcostprice
     */
    public function setActualunitprice($actualunitprice)
    {
        $this->actualunitprice = $actualunitprice;

        return $this;
    }

    /**
     * Get actualunitprice
     *
     * @return string 
     */
    public function getActualunitprice()
    {
        return $this->actualunitprice;
    }

    /**
     * Set wastage
     *
     * @param string $wastage
     * @return Styleoperationcostprice
     */
    public function setWastage($wastage)
    {
        $this->wastage = $wastage;

        return $this;
    }

    /**
     * Get wastage
     *
     * @return string 
     */
    public function getWastage()
    {
        return $this->wastage;
    }

    /**
     * Set actualusage
     *
     * @param string $actualusage
     * @return Styleoperationcostprice
     */
    public function setActualusage($actualusage)
    {
        $this->actualusage = $actualusage;

        return $this;
    }

    /**
     * Get actualusage
     *
     * @return string 
     */
    public function getActualusage()
    {
        return $this->actualusage;
    }

    /**
     * Set totalcostprice
     *
     * @param string $totalcostprice
     * @return Styleoperationcostprice
     */
    public function setTotalcostprice($totalcostprice)
    {
        $this->totalcostprice = $totalcostprice;

        return $this;
    }

    /**
     * Get totalcostprice
     *
     * @return string 
     */
    public function getTotalcostprice()
    {
        return $this->totalcostprice;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Styleoperationcostprice
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Styleoperationcostprice
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Styleoperationcostprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Styleoperationcostprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Styleoperationcostprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Styleoperationcostprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
