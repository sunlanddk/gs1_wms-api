<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Season
 */
class Season
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $ownerid;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $pickstockid;

    /**
     * @var integer
     */
    private $collmembersready;

    /**
     * @var integer
     */
    private $varcodesready;

    /**
     * @var integer
     */
    private $presalepickgenerated;

    /**
     * @var integer
     */
    private $pickposgenerated;

    /**
     * @var integer
     */
    private $done;

    /**
     * @var string
     */
    private $basevarcode;

    /**
     * @var integer
     */
    private $explicitmembers;

    /**
     * @var integer
     */
    private $fromstockid;

    /**
     * @var integer
     */
    private $webshopid;

    /**
     * @var integer
     */
    private $seasondisplayorder;

    /**
     * @var integer
     */
    private $showonbudget;

    /**
     * @var integer
     */
    private $showonb2b;

    /**
     * @var integer
     */
    private $restrictaccess;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Season
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Season
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ownerid
     *
     * @param integer $ownerid
     * @return Season
     */
    public function setOwnerid($ownerid)
    {
        $this->ownerid = $ownerid;

        return $this;
    }

    /**
     * Get ownerid
     *
     * @return integer 
     */
    public function getOwnerid()
    {
        return $this->ownerid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Season
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Season
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Season
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Season
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Season
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Season
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pickstockid
     *
     * @param integer $pickstockid
     * @return Season
     */
    public function setPickstockid($pickstockid)
    {
        $this->pickstockid = $pickstockid;

        return $this;
    }

    /**
     * Get pickstockid
     *
     * @return integer 
     */
    public function getPickstockid()
    {
        return $this->pickstockid;
    }

    /**
     * Set collmembersready
     *
     * @param integer $collmembersready
     * @return Season
     */
    public function setCollmembersready($collmembersready)
    {
        $this->collmembersready = $collmembersready;

        return $this;
    }

    /**
     * Get collmembersready
     *
     * @return integer 
     */
    public function getCollmembersready()
    {
        return $this->collmembersready;
    }

    /**
     * Set varcodesready
     *
     * @param integer $varcodesready
     * @return Season
     */
    public function setVarcodesready($varcodesready)
    {
        $this->varcodesready = $varcodesready;

        return $this;
    }

    /**
     * Get varcodesready
     *
     * @return integer 
     */
    public function getVarcodesready()
    {
        return $this->varcodesready;
    }

    /**
     * Set presalepickgenerated
     *
     * @param integer $presalepickgenerated
     * @return Season
     */
    public function setPresalepickgenerated($presalepickgenerated)
    {
        $this->presalepickgenerated = $presalepickgenerated;

        return $this;
    }

    /**
     * Get presalepickgenerated
     *
     * @return integer 
     */
    public function getPresalepickgenerated()
    {
        return $this->presalepickgenerated;
    }

    /**
     * Set pickposgenerated
     *
     * @param integer $pickposgenerated
     * @return Season
     */
    public function setPickposgenerated($pickposgenerated)
    {
        $this->pickposgenerated = $pickposgenerated;

        return $this;
    }

    /**
     * Get pickposgenerated
     *
     * @return integer 
     */
    public function getPickposgenerated()
    {
        return $this->pickposgenerated;
    }

    /**
     * Set done
     *
     * @param integer $done
     * @return Season
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return integer 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set basevarcode
     *
     * @param string $basevarcode
     * @return Season
     */
    public function setBasevarcode($basevarcode)
    {
        $this->basevarcode = $basevarcode;

        return $this;
    }

    /**
     * Get basevarcode
     *
     * @return string 
     */
    public function getBasevarcode()
    {
        return $this->basevarcode;
    }

    /**
     * Set explicitmembers
     *
     * @param integer $explicitmembers
     * @return Season
     */
    public function setExplicitmembers($explicitmembers)
    {
        $this->explicitmembers = $explicitmembers;

        return $this;
    }

    /**
     * Get explicitmembers
     *
     * @return integer 
     */
    public function getExplicitmembers()
    {
        return $this->explicitmembers;
    }

    /**
     * Set fromstockid
     *
     * @param integer $fromstockid
     * @return Season
     */
    public function setFromstockid($fromstockid)
    {
        $this->fromstockid = $fromstockid;

        return $this;
    }

    /**
     * Get fromstockid
     *
     * @return integer 
     */
    public function getFromstockid()
    {
        return $this->fromstockid;
    }

    /**
     * Set webshopid
     *
     * @param integer $webshopid
     * @return Season
     */
    public function setWebshopid($webshopid)
    {
        $this->webshopid = $webshopid;

        return $this;
    }

    /**
     * Get webshopid
     *
     * @return integer 
     */
    public function getWebshopid()
    {
        return $this->webshopid;
    }

    /**
     * Set seasondisplayorder
     *
     * @param integer $seasondisplayorder
     * @return Season
     */
    public function setSeasondisplayorder($seasondisplayorder)
    {
        $this->seasondisplayorder = $seasondisplayorder;

        return $this;
    }

    /**
     * Get seasondisplayorder
     *
     * @return integer 
     */
    public function getSeasondisplayorder()
    {
        return $this->seasondisplayorder;
    }

    /**
     * Set showonbudget
     *
     * @param integer $showonbudget
     * @return Season
     */
    public function setShowonbudget($showonbudget)
    {
        $this->showonbudget = $showonbudget;

        return $this;
    }

    /**
     * Get showonbudget
     *
     * @return integer 
     */
    public function getShowonbudget()
    {
        return $this->showonbudget;
    }

    /**
     * Set showonb2b
     *
     * @param integer $showonb2b
     * @return Season
     */
    public function setShowonb2b($showonb2b)
    {
        $this->showonb2b = $showonb2b;

        return $this;
    }

    /**
     * Get showonb2b
     *
     * @return integer 
     */
    public function getShowonb2b()
    {
        return $this->showonb2b;
    }

    /**
     * Set restrictaccess
     *
     * @param integer $restrictaccess
     * @return Season
     */
    public function setRestrictaccess($restrictaccess)
    {
        $this->restrictaccess = $restrictaccess;

        return $this;
    }

    /**
     * Get restrictaccess
     *
     * @return integer 
     */
    public function getRestrictaccess()
    {
        return $this->restrictaccess;
    }
}
