<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticlecategorygroupLang
 */
class ArticlecategorygroupLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articlecategorygroupid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articlecategorygroupid
     *
     * @param integer $articlecategorygroupid
     * @return ArticlecategorygroupLang
     */
    public function setArticlecategorygroupid($articlecategorygroupid)
    {
        $this->articlecategorygroupid = $articlecategorygroupid;

        return $this;
    }

    /**
     * Get articlecategorygroupid
     *
     * @return integer 
     */
    public function getArticlecategorygroupid()
    {
        return $this->articlecategorygroupid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ArticlecategorygroupLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ArticlecategorygroupLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ArticlecategorygroupLang
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
