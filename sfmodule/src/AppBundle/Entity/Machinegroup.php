<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Machinegroup
 */
class Machinegroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $productionminutes;

    /**
     * @var integer
     */
    private $machines;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $workgroupid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Machinegroup
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Machinegroup
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Machinegroup
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Machinegroup
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Machinegroup
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Machinegroup
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set productionminutes
     *
     * @param string $productionminutes
     * @return Machinegroup
     */
    public function setProductionminutes($productionminutes)
    {
        $this->productionminutes = $productionminutes;

        return $this;
    }

    /**
     * Get productionminutes
     *
     * @return string 
     */
    public function getProductionminutes()
    {
        return $this->productionminutes;
    }

    /**
     * Set machines
     *
     * @param integer $machines
     * @return Machinegroup
     */
    public function setMachines($machines)
    {
        $this->machines = $machines;

        return $this;
    }

    /**
     * Get machines
     *
     * @return integer 
     */
    public function getMachines()
    {
        return $this->machines;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Machinegroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set workgroupid
     *
     * @param integer $workgroupid
     * @return Machinegroup
     */
    public function setWorkgroupid($workgroupid)
    {
        $this->workgroupid = $workgroupid;

        return $this;
    }

    /**
     * Get workgroupid
     *
     * @return integer 
     */
    public function getWorkgroupid()
    {
        return $this->workgroupid;
    }
}
