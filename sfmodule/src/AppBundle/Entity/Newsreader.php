<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsreader
 */
class Newsreader
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $readerid;

    /**
     * @var integer
     */
    private $newsid;

    /**
     * @var boolean
     */
    private $isread;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set readerid
     *
     * @param integer $readerid
     * @return Newsreader
     */
    public function setReaderid($readerid)
    {
        $this->readerid = $readerid;

        return $this;
    }

    /**
     * Get readerid
     *
     * @return integer 
     */
    public function getReaderid()
    {
        return $this->readerid;
    }

    /**
     * Set newsid
     *
     * @param integer $newsid
     * @return Newsreader
     */
    public function setNewsid($newsid)
    {
        $this->newsid = $newsid;

        return $this;
    }

    /**
     * Get newsid
     *
     * @return integer 
     */
    public function getNewsid()
    {
        return $this->newsid;
    }

    /**
     * Set isread
     *
     * @param boolean $isread
     * @return Newsreader
     */
    public function setIsread($isread)
    {
        $this->isread = $isread;

        return $this;
    }

    /**
     * Get isread
     *
     * @return boolean 
     */
    public function getIsread()
    {
        return $this->isread;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Newsreader
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Newsreader
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Newsreader
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Newsreader
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
