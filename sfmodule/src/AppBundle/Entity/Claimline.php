<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Claimline
 */
class Claimline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $no;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $claimid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var integer
     */
    private $surplus;

    /**
     * @var string
     */
    private $pricesale;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var \DateTime
     */
    private $deliverydate;

    /**
     * @var integer
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Claimline
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Claimline
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Claimline
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Claimline
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Claimline
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set no
     *
     * @param integer $no
     * @return Claimline
     */
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get no
     *
     * @return integer 
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Claimline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set claimid
     *
     * @param integer $claimid
     * @return Claimline
     */
    public function setClaimid($claimid)
    {
        $this->claimid = $claimid;

        return $this;
    }

    /**
     * Get claimid
     *
     * @return integer 
     */
    public function getClaimid()
    {
        return $this->claimid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Claimline
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Claimline
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Claimline
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Claimline
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set surplus
     *
     * @param integer $surplus
     * @return Claimline
     */
    public function setSurplus($surplus)
    {
        $this->surplus = $surplus;

        return $this;
    }

    /**
     * Get surplus
     *
     * @return integer 
     */
    public function getSurplus()
    {
        return $this->surplus;
    }

    /**
     * Set pricesale
     *
     * @param string $pricesale
     * @return Claimline
     */
    public function setPricesale($pricesale)
    {
        $this->pricesale = $pricesale;

        return $this;
    }

    /**
     * Get pricesale
     *
     * @return string 
     */
    public function getPricesale()
    {
        return $this->pricesale;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Claimline
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set deliverydate
     *
     * @param \DateTime $deliverydate
     * @return Claimline
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return \DateTime 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }

    /**
     * Set done
     *
     * @param integer $done
     * @return Claimline
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return integer 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Claimline
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Claimline
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }
}
