<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timezone
 */
class Timezone
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $zone1;

    /**
     * @var string
     */
    private $zone2;

    /**
     * @var integer
     */
    private $projectid;

    /**
     * @var boolean
     */
    private $defaultrecord;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Timezone
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Timezone
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Timezone
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Timezone
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Timezone
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Timezone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zone1
     *
     * @param string $zone1
     * @return Timezone
     */
    public function setZone1($zone1)
    {
        $this->zone1 = $zone1;

        return $this;
    }

    /**
     * Get zone1
     *
     * @return string 
     */
    public function getZone1()
    {
        return $this->zone1;
    }

    /**
     * Set zone2
     *
     * @param string $zone2
     * @return Timezone
     */
    public function setZone2($zone2)
    {
        $this->zone2 = $zone2;

        return $this;
    }

    /**
     * Get zone2
     *
     * @return string 
     */
    public function getZone2()
    {
        return $this->zone2;
    }

    /**
     * Set projectid
     *
     * @param integer $projectid
     * @return Timezone
     */
    public function setProjectid($projectid)
    {
        $this->projectid = $projectid;

        return $this;
    }

    /**
     * Get projectid
     *
     * @return integer 
     */
    public function getProjectid()
    {
        return $this->projectid;
    }

    /**
     * Set defaultrecord
     *
     * @param boolean $defaultrecord
     * @return Timezone
     */
    public function setDefaultrecord($defaultrecord)
    {
        $this->defaultrecord = $defaultrecord;

        return $this;
    }

    /**
     * Get defaultrecord
     *
     * @return boolean 
     */
    public function getDefaultrecord()
    {
        return $this->defaultrecord;
    }
}
