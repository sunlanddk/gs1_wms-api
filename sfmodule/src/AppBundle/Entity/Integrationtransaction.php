<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Integrationtransaction
 */
class Integrationtransaction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $integrationtransactiontypeid;

    /**
     * @var \DateTime
     */
    private $executiondate;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set integrationtransactiontypeid
     *
     * @param integer $integrationtransactiontypeid
     * @return Integrationtransaction
     */
    public function setIntegrationtransactiontypeid($integrationtransactiontypeid)
    {
        $this->integrationtransactiontypeid = $integrationtransactiontypeid;

        return $this;
    }

    /**
     * Get integrationtransactiontypeid
     *
     * @return integer 
     */
    public function getIntegrationtransactiontypeid()
    {
        return $this->integrationtransactiontypeid;
    }

    /**
     * Set executiondate
     *
     * @param \DateTime $executiondate
     * @return Integrationtransaction
     */
    public function setExecutiondate($executiondate)
    {
        $this->executiondate = $executiondate;

        return $this;
    }

    /**
     * Get executiondate
     *
     * @return \DateTime 
     */
    public function getExecutiondate()
    {
        return $this->executiondate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Integrationtransaction
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Integrationtransaction
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Integrationtransaction
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Integrationtransaction
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
