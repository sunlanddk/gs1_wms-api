<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColorgroupLang
 */
class ColorgroupLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $colorgroupid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorgroupid
     *
     * @param integer $colorgroupid
     * @return ColorgroupLang
     */
    public function setColorgroupid($colorgroupid)
    {
        $this->colorgroupid = $colorgroupid;

        return $this;
    }

    /**
     * Get colorgroupid
     *
     * @return integer 
     */
    public function getColorgroupid()
    {
        return $this->colorgroupid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ColorgroupLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ColorgroupLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ColorgroupLang
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
