<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bundlequantity
 */
class Bundlequantity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $bundleid;

    /**
     * @var integer
     */
    private $styleoperationid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var integer
     */
    private $quantity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Bundlequantity
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Bundlequantity
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Bundlequantity
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Bundlequantity
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Bundlequantity
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set bundleid
     *
     * @param integer $bundleid
     * @return Bundlequantity
     */
    public function setBundleid($bundleid)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return integer 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set styleoperationid
     *
     * @param integer $styleoperationid
     * @return Bundlequantity
     */
    public function setStyleoperationid($styleoperationid)
    {
        $this->styleoperationid = $styleoperationid;

        return $this;
    }

    /**
     * Get styleoperationid
     *
     * @return integer 
     */
    public function getStyleoperationid()
    {
        return $this->styleoperationid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Bundlequantity
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Bundlequantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
