<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylematerialcolorvariation
 */
class Stylematerialcolorvariation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stylematerialid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $altmaterialarticlecolorid;

    /**
     * @var integer
     */
    private $altmaterialarticleid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stylematerialid
     *
     * @param integer $stylematerialid
     * @return Stylematerialcolorvariation
     */
    public function setStylematerialid($stylematerialid)
    {
        $this->stylematerialid = $stylematerialid;

        return $this;
    }

    /**
     * Get stylematerialid
     *
     * @return integer 
     */
    public function getStylematerialid()
    {
        return $this->stylematerialid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Stylematerialcolorvariation
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set altmaterialarticlecolorid
     *
     * @param integer $altmaterialarticlecolorid
     * @return Stylematerialcolorvariation
     */
    public function setAltmaterialarticlecolorid($altmaterialarticlecolorid)
    {
        $this->altmaterialarticlecolorid = $altmaterialarticlecolorid;

        return $this;
    }

    /**
     * Get altmaterialarticlecolorid
     *
     * @return integer 
     */
    public function getAltmaterialarticlecolorid()
    {
        return $this->altmaterialarticlecolorid;
    }

    /**
     * Set altmaterialarticleid
     *
     * @param integer $altmaterialarticleid
     * @return Stylematerialcolorvariation
     */
    public function setAltmaterialarticleid($altmaterialarticleid)
    {
        $this->altmaterialarticleid = $altmaterialarticleid;

        return $this;
    }

    /**
     * Get altmaterialarticleid
     *
     * @return integer 
     */
    public function getAltmaterialarticleid()
    {
        return $this->altmaterialarticleid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylematerialcolorvariation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylematerialcolorvariation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylematerialcolorvariation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylematerialcolorvariation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
