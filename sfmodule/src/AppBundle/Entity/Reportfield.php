<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reportfield
 */
class Reportfield
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $reportid;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $displayorder;

    /**
     * @var integer
     */
    private $sortlevel;

    /**
     * @var integer
     */
    private $width;

    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $format;

    /**
     * @var boolean
     */
    private $total;

    /**
     * @var integer
     */
    private $grouplevel;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Reportfield
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Reportfield
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Reportfield
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Reportfield
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Reportfield
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Reportfield
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Reportfield
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set reportid
     *
     * @param integer $reportid
     * @return Reportfield
     */
    public function setReportid($reportid)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get reportid
     *
     * @return integer 
     */
    public function getReportid()
    {
        return $this->reportid;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Reportfield
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set displayorder
     *
     * @param integer $displayorder
     * @return Reportfield
     */
    public function setDisplayorder($displayorder)
    {
        $this->displayorder = $displayorder;

        return $this;
    }

    /**
     * Get displayorder
     *
     * @return integer 
     */
    public function getDisplayorder()
    {
        return $this->displayorder;
    }

    /**
     * Set sortlevel
     *
     * @param integer $sortlevel
     * @return Reportfield
     */
    public function setSortlevel($sortlevel)
    {
        $this->sortlevel = $sortlevel;

        return $this;
    }

    /**
     * Get sortlevel
     *
     * @return integer 
     */
    public function getSortlevel()
    {
        return $this->sortlevel;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Reportfield
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set query
     *
     * @param string $query
     * @return Reportfield
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return string 
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return Reportfield
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set total
     *
     * @param boolean $total
     * @return Reportfield
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return boolean 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set grouplevel
     *
     * @param integer $grouplevel
     * @return Reportfield
     */
    public function setGrouplevel($grouplevel)
    {
        $this->grouplevel = $grouplevel;

        return $this;
    }

    /**
     * Get grouplevel
     *
     * @return integer 
     */
    public function getGrouplevel()
    {
        return $this->grouplevel;
    }
}
