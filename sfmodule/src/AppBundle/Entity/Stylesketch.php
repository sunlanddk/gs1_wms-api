<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylesketch
 */
class Stylesketch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stylesketchcategoryid;

    /**
     * @var integer
     */
    private $stylesketchversionid;

    /**
     * @var integer
     */
    private $documentid;

    /**
     * @var string
     */
    private $constructiondone;

    /**
     * @var \DateTime
     */
    private $constructiondonedate;

    /**
     * @var integer
     */
    private $constructiondoneuserid;

    /**
     * @var boolean
     */
    private $technologydone;

    /**
     * @var \DateTime
     */
    private $technologydonedate;

    /**
     * @var integer
     */
    private $technologydoneuserid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stylesketchcategoryid
     *
     * @param integer $stylesketchcategoryid
     * @return Stylesketch
     */
    public function setStylesketchcategoryid($stylesketchcategoryid)
    {
        $this->stylesketchcategoryid = $stylesketchcategoryid;

        return $this;
    }

    /**
     * Get stylesketchcategoryid
     *
     * @return integer 
     */
    public function getStylesketchcategoryid()
    {
        return $this->stylesketchcategoryid;
    }

    /**
     * Set stylesketchversionid
     *
     * @param integer $stylesketchversionid
     * @return Stylesketch
     */
    public function setStylesketchversionid($stylesketchversionid)
    {
        $this->stylesketchversionid = $stylesketchversionid;

        return $this;
    }

    /**
     * Get stylesketchversionid
     *
     * @return integer 
     */
    public function getStylesketchversionid()
    {
        return $this->stylesketchversionid;
    }

    /**
     * Set documentid
     *
     * @param integer $documentid
     * @return Stylesketch
     */
    public function setDocumentid($documentid)
    {
        $this->documentid = $documentid;

        return $this;
    }

    /**
     * Get documentid
     *
     * @return integer 
     */
    public function getDocumentid()
    {
        return $this->documentid;
    }

    /**
     * Set constructiondone
     *
     * @param string $constructiondone
     * @return Stylesketch
     */
    public function setConstructiondone($constructiondone)
    {
        $this->constructiondone = $constructiondone;

        return $this;
    }

    /**
     * Get constructiondone
     *
     * @return string 
     */
    public function getConstructiondone()
    {
        return $this->constructiondone;
    }

    /**
     * Set constructiondonedate
     *
     * @param \DateTime $constructiondonedate
     * @return Stylesketch
     */
    public function setConstructiondonedate($constructiondonedate)
    {
        $this->constructiondonedate = $constructiondonedate;

        return $this;
    }

    /**
     * Get constructiondonedate
     *
     * @return \DateTime 
     */
    public function getConstructiondonedate()
    {
        return $this->constructiondonedate;
    }

    /**
     * Set constructiondoneuserid
     *
     * @param integer $constructiondoneuserid
     * @return Stylesketch
     */
    public function setConstructiondoneuserid($constructiondoneuserid)
    {
        $this->constructiondoneuserid = $constructiondoneuserid;

        return $this;
    }

    /**
     * Get constructiondoneuserid
     *
     * @return integer 
     */
    public function getConstructiondoneuserid()
    {
        return $this->constructiondoneuserid;
    }

    /**
     * Set technologydone
     *
     * @param boolean $technologydone
     * @return Stylesketch
     */
    public function setTechnologydone($technologydone)
    {
        $this->technologydone = $technologydone;

        return $this;
    }

    /**
     * Get technologydone
     *
     * @return boolean 
     */
    public function getTechnologydone()
    {
        return $this->technologydone;
    }

    /**
     * Set technologydonedate
     *
     * @param \DateTime $technologydonedate
     * @return Stylesketch
     */
    public function setTechnologydonedate($technologydonedate)
    {
        $this->technologydonedate = $technologydonedate;

        return $this;
    }

    /**
     * Get technologydonedate
     *
     * @return \DateTime 
     */
    public function getTechnologydonedate()
    {
        return $this->technologydonedate;
    }

    /**
     * Set technologydoneuserid
     *
     * @param integer $technologydoneuserid
     * @return Stylesketch
     */
    public function setTechnologydoneuserid($technologydoneuserid)
    {
        $this->technologydoneuserid = $technologydoneuserid;

        return $this;
    }

    /**
     * Get technologydoneuserid
     *
     * @return integer 
     */
    public function getTechnologydoneuserid()
    {
        return $this->technologydoneuserid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylesketch
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylesketch
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylesketch
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylesketch
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
