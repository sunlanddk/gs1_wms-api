<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylematerial
 */
class Stylematerial
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stylematerialsversionid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlestyleid;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $materialarticlecolorid;

    /**
     * @var integer
     */
    private $sizedimentions;

    /**
     * @var string
     */
    private $usagepercentage;

    /**
     * @var string
     */
    private $usage;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $ordernumber;

    /**
     * @var integer
     */
    private $numberof;

    /**
     * @var boolean
     */
    private $print;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stylematerialsversionid
     *
     * @param integer $stylematerialsversionid
     * @return Stylematerial
     */
    public function setStylematerialsversionid($stylematerialsversionid)
    {
        $this->stylematerialsversionid = $stylematerialsversionid;

        return $this;
    }

    /**
     * Get stylematerialsversionid
     *
     * @return integer 
     */
    public function getStylematerialsversionid()
    {
        return $this->stylematerialsversionid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Stylematerial
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlestyleid
     *
     * @param integer $articlestyleid
     * @return Stylematerial
     */
    public function setArticlestyleid($articlestyleid)
    {
        $this->articlestyleid = $articlestyleid;

        return $this;
    }

    /**
     * Get articlestyleid
     *
     * @return integer 
     */
    public function getArticlestyleid()
    {
        return $this->articlestyleid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Stylematerial
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set materialarticlecolorid
     *
     * @param integer $materialarticlecolorid
     * @return Stylematerial
     */
    public function setMaterialarticlecolorid($materialarticlecolorid)
    {
        $this->materialarticlecolorid = $materialarticlecolorid;

        return $this;
    }

    /**
     * Get materialarticlecolorid
     *
     * @return integer 
     */
    public function getMaterialarticlecolorid()
    {
        return $this->materialarticlecolorid;
    }

    /**
     * Set sizedimentions
     *
     * @param integer $sizedimentions
     * @return Stylematerial
     */
    public function setSizedimentions($sizedimentions)
    {
        $this->sizedimentions = $sizedimentions;

        return $this;
    }

    /**
     * Get sizedimentions
     *
     * @return integer 
     */
    public function getSizedimentions()
    {
        return $this->sizedimentions;
    }

    /**
     * Set usagepercentage
     *
     * @param string $usagepercentage
     * @return Stylematerial
     */
    public function setUsagepercentage($usagepercentage)
    {
        $this->usagepercentage = $usagepercentage;

        return $this;
    }

    /**
     * Get usagepercentage
     *
     * @return string 
     */
    public function getUsagepercentage()
    {
        return $this->usagepercentage;
    }

    /**
     * Set usage
     *
     * @param string $usage
     * @return Stylematerial
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;

        return $this;
    }

    /**
     * Get usage
     *
     * @return string 
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylematerial
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylematerial
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylematerial
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylematerial
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set ordernumber
     *
     * @param integer $ordernumber
     * @return Stylematerial
     */
    public function setOrdernumber($ordernumber)
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }

    /**
     * Get ordernumber
     *
     * @return integer 
     */
    public function getOrdernumber()
    {
        return $this->ordernumber;
    }

    /**
     * Set numberof
     *
     * @param integer $numberof
     * @return Stylematerial
     */
    public function setNumberof($numberof)
    {
        $this->numberof = $numberof;

        return $this;
    }

    /**
     * Get numberof
     *
     * @return integer 
     */
    public function getNumberof()
    {
        return $this->numberof;
    }

    /**
     * Set print
     *
     * @param boolean $print
     * @return Stylematerial
     */
    public function setPrint($print)
    {
        $this->print = $print;

        return $this;
    }

    /**
     * Get print
     *
     * @return boolean 
     */
    public function getPrint()
    {
        return $this->print;
    }
}
