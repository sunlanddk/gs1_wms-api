<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orderline
 */
class Orderline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $no;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $invoicefooter;

    /**
     * @var integer
     */
    private $orderid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var integer
     */
    private $surplus;

    /**
     * @var string
     */
    private $pricesale;

    /**
     * @var string
     */
    private $pricecost;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var \DateTime
     */
    private $deliverydate;

    /**
     * @var integer
     */
    private $articlecertificateid;

    /**
     * @var \DateTime
     */
    private $prefdeliverydate;

    /**
     * @var string
     */
    private $requestcomment;

    /**
     * @var string
     */
    private $custartref;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var integer
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var \DateTime
     */
    private $confdeliverydate;

    /**
     * @var string
     */
    private $linefooter;

    /**
     * @var string
     */
    private $altcolorname;


    /**
     * @var integer
     */
    private $collectionmemberid;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Orderline
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Orderline
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Orderline
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Orderline
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Orderline
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set no
     *
     * @param integer $no
     * @return Orderline
     */
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get no
     *
     * @return integer 
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Orderline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Orderline
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string 
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

    /**
     * Set orderid
     *
     * @param integer $orderid
     * @return Orderline
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer 
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Orderline
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Orderline
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Orderline
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Orderline
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set surplus
     *
     * @param integer $surplus
     * @return Orderline
     */
    public function setSurplus($surplus)
    {
        $this->surplus = $surplus;

        return $this;
    }

    /**
     * Get surplus
     *
     * @return integer 
     */
    public function getSurplus()
    {
        return $this->surplus;
    }

    /**
     * Set pricesale
     *
     * @param string $pricesale
     * @return Orderline
     */
    public function setPricesale($pricesale)
    {
        $this->pricesale = $pricesale;

        return $this;
    }

    /**
     * Get pricesale
     *
     * @return string 
     */
    public function getPricesale()
    {
        return $this->pricesale;
    }

    /**
     * Set pricecost
     *
     * @param string $pricecost
     * @return Orderline
     */
    public function setPricecost($pricecost)
    {
        $this->pricecost = $pricecost;

        return $this;
    }

    /**
     * Get pricecost
     *
     * @return string 
     */
    public function getPricecost()
    {
        return $this->pricecost;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Orderline
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set deliverydate
     *
     * @param \DateTime $deliverydate
     * @return Orderline
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return \DateTime 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }

    /**
     * Set articlecertificateid
     *
     * @param integer $articlecertificateid
     * @return Orderline
     */
    public function setArticlecertificateid($articlecertificateid)
    {
        $this->articlecertificateid = $articlecertificateid;

        return $this;
    }

    /**
     * Get articlecertificateid
     *
     * @return integer 
     */
    public function getArticlecertificateid()
    {
        return $this->articlecertificateid;
    }

    /**
     * Set prefdeliverydate
     *
     * @param \DateTime $prefdeliverydate
     * @return Orderline
     */
    public function setPrefdeliverydate($prefdeliverydate)
    {
        $this->prefdeliverydate = $prefdeliverydate;

        return $this;
    }

    /**
     * Get prefdeliverydate
     *
     * @return \DateTime 
     */
    public function getPrefdeliverydate()
    {
        return $this->prefdeliverydate;
    }

    /**
     * Set requestcomment
     *
     * @param string $requestcomment
     * @return Orderline
     */
    public function setRequestcomment($requestcomment)
    {
        $this->requestcomment = $requestcomment;

        return $this;
    }

    /**
     * Get requestcomment
     *
     * @return string 
     */
    public function getRequestcomment()
    {
        return $this->requestcomment;
    }

    /**
     * Set custartref
     *
     * @param string $custartref
     * @return Orderline
     */
    public function setCustartref($custartref)
    {
        $this->custartref = $custartref;

        return $this;
    }

    /**
     * Get custartref
     *
     * @return string 
     */
    public function getCustartref()
    {
        return $this->custartref;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Orderline
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set done
     *
     * @param integer $done
     * @return Orderline
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return integer 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Orderline
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Orderline
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set confdeliverydate
     *
     * @param \DateTime $confdeliverydate
     * @return Orderline
     */
    public function setConfdeliverydate($confdeliverydate)
    {
        $this->confdeliverydate = $confdeliverydate;

        return $this;
    }

    /**
     * Get confdeliverydate
     *
     * @return \DateTime 
     */
    public function getConfdeliverydate()
    {
        return $this->confdeliverydate;
    }

    /**
     * Set linefooter
     *
     * @param string $linefooter
     * @return Orderline
     */
    public function setLinefooter($linefooter)
    {
        $this->linefooter = $linefooter;

        return $this;
    }

    /**
     * Get linefooter
     *
     * @return string 
     */
    public function getLinefooter()
    {
        return $this->linefooter;
    }

    /**
     * Set altcolorname
     *
     * @param string $altcolorname
     * @return Orderline
     */
    public function setAltcolorname($altcolorname)
    {
        $this->altcolorname = $altcolorname;

        return $this;
    }

    /**
     * Get altcolorname
     *
     * @return string 
     */
    public function getAltcolorname()
    {
        return $this->altcolorname;
    }
	
    /**
     * Set collectionmemberid
     *
     * @param integer $collectionmemberid
     * @return Orderline
     */
    public function setCollectionmemberid($collectionmemberid)
    {
        $this->collectionmemberid = $collectionmemberid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer 
     */
    public function getCollectionmemberid()
    {
        return $this->collectionmemberid;
    }
	
    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Orderline
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return integer 
     */
    public function geSeasonid()
    {
        return $this->seasonid;
    }
	
}
