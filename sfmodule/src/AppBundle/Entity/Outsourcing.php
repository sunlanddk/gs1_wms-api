<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Outsourcing
 */
class Outsourcing
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bundleid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var \DateTime
     */
    private $outdate;

    /**
     * @var \DateTime
     */
    private $indate;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $operationfromno;

    /**
     * @var integer
     */
    private $operationtono;

    /**
     * @var integer
     */
    private $locationid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bundleid
     *
     * @param integer $bundleid
     * @return Outsourcing
     */
    public function setBundleid($bundleid)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return integer 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Outsourcing
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set outdate
     *
     * @param \DateTime $outdate
     * @return Outsourcing
     */
    public function setOutdate($outdate)
    {
        $this->outdate = $outdate;

        return $this;
    }

    /**
     * Get outdate
     *
     * @return \DateTime 
     */
    public function getOutdate()
    {
        return $this->outdate;
    }

    /**
     * Set indate
     *
     * @param \DateTime $indate
     * @return Outsourcing
     */
    public function setIndate($indate)
    {
        $this->indate = $indate;

        return $this;
    }

    /**
     * Get indate
     *
     * @return \DateTime 
     */
    public function getIndate()
    {
        return $this->indate;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Outsourcing
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Outsourcing
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set operationfromno
     *
     * @param integer $operationfromno
     * @return Outsourcing
     */
    public function setOperationfromno($operationfromno)
    {
        $this->operationfromno = $operationfromno;

        return $this;
    }

    /**
     * Get operationfromno
     *
     * @return integer 
     */
    public function getOperationfromno()
    {
        return $this->operationfromno;
    }

    /**
     * Set operationtono
     *
     * @param integer $operationtono
     * @return Outsourcing
     */
    public function setOperationtono($operationtono)
    {
        $this->operationtono = $operationtono;

        return $this;
    }

    /**
     * Get operationtono
     *
     * @return integer 
     */
    public function getOperationtono()
    {
        return $this->operationtono;
    }

    /**
     * Set locationid
     *
     * @param integer $locationid
     * @return Outsourcing
     */
    public function setLocationid($locationid)
    {
        $this->locationid = $locationid;

        return $this;
    }

    /**
     * Get locationid
     *
     * @return integer 
     */
    public function getLocationid()
    {
        return $this->locationid;
    }
}
