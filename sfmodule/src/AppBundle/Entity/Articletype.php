<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articletype
 */
class Articletype
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $leaddigit;

    /**
     * @var boolean
     */
    private $variantcolor;

    /**
     * @var boolean
     */
    private $variantdimension;

    /**
     * @var boolean
     */
    private $variantsize;

    /**
     * @var boolean
     */
    private $variantsortation;

    /**
     * @var boolean
     */
    private $variantcertificate;

    /**
     * @var boolean
     */
    private $product;

    /**
     * @var boolean
     */
    private $material;

    /**
     * @var boolean
     */
    private $test;

    /**
     * @var boolean
     */
    private $fabric;

    /**
     * @var string
     */
    private $surplusmaterials;

    /**
     * @var string
     */
    private $surplusoperations;

    /**
     * @var string
     */
    private $wastage;

    /**
     * @var string
     */
    private $logisticcost;

    /**
     * @var string
     */
    private $othermaterialcost;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Articletype
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Articletype
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Articletype
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Articletype
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Articletype
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Articletype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Articletype
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set leaddigit
     *
     * @param string $leaddigit
     * @return Articletype
     */
    public function setLeaddigit($leaddigit)
    {
        $this->leaddigit = $leaddigit;

        return $this;
    }

    /**
     * Get leaddigit
     *
     * @return string 
     */
    public function getLeaddigit()
    {
        return $this->leaddigit;
    }

    /**
     * Set variantcolor
     *
     * @param boolean $variantcolor
     * @return Articletype
     */
    public function setVariantcolor($variantcolor)
    {
        $this->variantcolor = $variantcolor;

        return $this;
    }

    /**
     * Get variantcolor
     *
     * @return boolean 
     */
    public function getVariantcolor()
    {
        return $this->variantcolor;
    }

    /**
     * Set variantdimension
     *
     * @param boolean $variantdimension
     * @return Articletype
     */
    public function setVariantdimension($variantdimension)
    {
        $this->variantdimension = $variantdimension;

        return $this;
    }

    /**
     * Get variantdimension
     *
     * @return boolean 
     */
    public function getVariantdimension()
    {
        return $this->variantdimension;
    }

    /**
     * Set variantsize
     *
     * @param boolean $variantsize
     * @return Articletype
     */
    public function setVariantsize($variantsize)
    {
        $this->variantsize = $variantsize;

        return $this;
    }

    /**
     * Get variantsize
     *
     * @return boolean 
     */
    public function getVariantsize()
    {
        return $this->variantsize;
    }

    /**
     * Set variantsortation
     *
     * @param boolean $variantsortation
     * @return Articletype
     */
    public function setVariantsortation($variantsortation)
    {
        $this->variantsortation = $variantsortation;

        return $this;
    }

    /**
     * Get variantsortation
     *
     * @return boolean 
     */
    public function getVariantsortation()
    {
        return $this->variantsortation;
    }

    /**
     * Set variantcertificate
     *
     * @param boolean $variantcertificate
     * @return Articletype
     */
    public function setVariantcertificate($variantcertificate)
    {
        $this->variantcertificate = $variantcertificate;

        return $this;
    }

    /**
     * Get variantcertificate
     *
     * @return boolean 
     */
    public function getVariantcertificate()
    {
        return $this->variantcertificate;
    }

    /**
     * Set product
     *
     * @param boolean $product
     * @return Articletype
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return boolean 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set material
     *
     * @param boolean $material
     * @return Articletype
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return boolean 
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set test
     *
     * @param boolean $test
     * @return Articletype
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return boolean 
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set fabric
     *
     * @param boolean $fabric
     * @return Articletype
     */
    public function setFabric($fabric)
    {
        $this->fabric = $fabric;

        return $this;
    }

    /**
     * Get fabric
     *
     * @return boolean 
     */
    public function getFabric()
    {
        return $this->fabric;
    }

    /**
     * Set surplusmaterials
     *
     * @param string $surplusmaterials
     * @return Articletype
     */
    public function setSurplusmaterials($surplusmaterials)
    {
        $this->surplusmaterials = $surplusmaterials;

        return $this;
    }

    /**
     * Get surplusmaterials
     *
     * @return string 
     */
    public function getSurplusmaterials()
    {
        return $this->surplusmaterials;
    }

    /**
     * Set surplusoperations
     *
     * @param string $surplusoperations
     * @return Articletype
     */
    public function setSurplusoperations($surplusoperations)
    {
        $this->surplusoperations = $surplusoperations;

        return $this;
    }

    /**
     * Get surplusoperations
     *
     * @return string 
     */
    public function getSurplusoperations()
    {
        return $this->surplusoperations;
    }

    /**
     * Set wastage
     *
     * @param string $wastage
     * @return Articletype
     */
    public function setWastage($wastage)
    {
        $this->wastage = $wastage;

        return $this;
    }

    /**
     * Get wastage
     *
     * @return string 
     */
    public function getWastage()
    {
        return $this->wastage;
    }

    /**
     * Set logisticcost
     *
     * @param string $logisticcost
     * @return Articletype
     */
    public function setLogisticcost($logisticcost)
    {
        $this->logisticcost = $logisticcost;

        return $this;
    }

    /**
     * Get logisticcost
     *
     * @return string 
     */
    public function getLogisticcost()
    {
        return $this->logisticcost;
    }

    /**
     * Set othermaterialcost
     *
     * @param string $othermaterialcost
     * @return Articletype
     */
    public function setOthermaterialcost($othermaterialcost)
    {
        $this->othermaterialcost = $othermaterialcost;

        return $this;
    }

    /**
     * Get othermaterialcost
     *
     * @return string 
     */
    public function getOthermaterialcost()
    {
        return $this->othermaterialcost;
    }
}
