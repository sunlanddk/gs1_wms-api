<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stock
 */
class Stock
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $stockno;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $altcompanyname;

    /**
     * @var string
     */
    private $companyname;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $deliverytermid;

    /**
     * @var integer
     */
    private $carrierid;

    /**
     * @var \DateTime
     */
    private $departuredate;

    /**
     * @var \DateTime
     */
    private $arrivaldate;

    /**
     * @var \DateTime
     */
    private $stockdate;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var boolean
     */
    private $departed;

    /**
     * @var integer
     */
    private $departeduserid;

    /**
     * @var \DateTime
     */
    private $departeddate;

    /**
     * @var boolean
     */
    private $arrived;

    /**
     * @var integer
     */
    private $arriveduserid;

    /**
     * @var \DateTime
     */
    private $arriveddate;

    /**
     * @var boolean
     */
    private $verified;

    /**
     * @var integer
     */
    private $verifieduserid;

    /**
     * @var \DateTime
     */
    private $verifieddate;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var boolean
     */
    private $invoiced;

    /**
     * @var integer
     */
    private $invoiceduserid;

    /**
     * @var \DateTime
     */
    private $invoiceddate;

    /**
     * @var boolean
     */
    private $public;

    /**
     * @var integer
     */
    private $accountid;

    /**
     * @var integer
     */
    private $fromcompanyid;

    /**
     * @var integer
     */
    private $batchid;

    /**
     * @var integer
     */
    private $partship;

    /**
     * @var boolean
     */
    private $pickstock;

    /**
     * @var boolean
     */
    private $rangeassign;

    /**
     * @var boolean
     */
    private $onstockonly;

    /**
     * @var boolean
     */
    private $inventorymode;

    /**
     * @var integer
     */
    private $inventorypct;

    /**
     * @var string
     */
    private $deliverycomment;

    /**
     * @var string
     */
    private $carrierurl;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set stockno
     *
     * @param string $stockno
     * @return Stock
     */
    public function setStockno($stockno)
    {
        $this->stockno = $stockno;

        return $this;
    }

    /**
     * Get stockno
     *
     * @return string 
     */
    public function getStockno()
    {
        return $this->stockno;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stock
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stock
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stock
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stock
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Stock
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Stock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Stock
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set altcompanyname
     *
     * @param string $altcompanyname
     * @return Stock
     */
    public function setAltcompanyname($altcompanyname)
    {
        $this->altcompanyname = $altcompanyname;

        return $this;
    }

    /**
     * Get altcompanyname
     *
     * @return string 
     */
    public function getAltcompanyname()
    {
        return $this->altcompanyname;
    }

    /**
     * Set companyname
     *
     * @param string $companyname
     * @return Stock
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Get companyname
     *
     * @return string 
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Stock
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Stock
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Stock
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Stock
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Stock
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Stock
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Stock
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set deliverytermid
     *
     * @param integer $deliverytermid
     * @return Stock
     */
    public function setDeliverytermid($deliverytermid)
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    /**
     * Get deliverytermid
     *
     * @return integer 
     */
    public function getDeliverytermid()
    {
        return $this->deliverytermid;
    }

    /**
     * Set carrierid
     *
     * @param integer $carrierid
     * @return Stock
     */
    public function setCarrierid($carrierid)
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    /**
     * Get carrierid
     *
     * @return integer 
     */
    public function getCarrierid()
    {
        return $this->carrierid;
    }

    /**
     * Set departuredate
     *
     * @param \DateTime $departuredate
     * @return Stock
     */
    public function setDeparturedate($departuredate)
    {
        $this->departuredate = $departuredate;

        return $this;
    }

    /**
     * Get departuredate
     *
     * @return \DateTime 
     */
    public function getDeparturedate()
    {
        return $this->departuredate;
    }

    /**
     * Set arrivaldate
     *
     * @param \DateTime $arrivaldate
     * @return Stock
     */
    public function setArrivaldate($arrivaldate)
    {
        $this->arrivaldate = $arrivaldate;

        return $this;
    }

    /**
     * Get arrivaldate
     *
     * @return \DateTime 
     */
    public function getArrivaldate()
    {
        return $this->arrivaldate;
    }

    /**
     * Set stockdate
     *
     * @param \DateTime $stockdate
     * @return Stock
     */
    public function setStockdate($stockdate)
    {
        $this->stockdate = $stockdate;

        return $this;
    }

    /**
     * Get stockdate
     *
     * @return \DateTime 
     */
    public function getStockdate()
    {
        return $this->stockdate;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Stock
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean 
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Stock
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer 
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Stock
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime 
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set departed
     *
     * @param boolean $departed
     * @return Stock
     */
    public function setDeparted($departed)
    {
        $this->departed = $departed;

        return $this;
    }

    /**
     * Get departed
     *
     * @return boolean 
     */
    public function getDeparted()
    {
        return $this->departed;
    }

    /**
     * Set departeduserid
     *
     * @param integer $departeduserid
     * @return Stock
     */
    public function setDeparteduserid($departeduserid)
    {
        $this->departeduserid = $departeduserid;

        return $this;
    }

    /**
     * Get departeduserid
     *
     * @return integer 
     */
    public function getDeparteduserid()
    {
        return $this->departeduserid;
    }

    /**
     * Set departeddate
     *
     * @param \DateTime $departeddate
     * @return Stock
     */
    public function setDeparteddate($departeddate)
    {
        $this->departeddate = $departeddate;

        return $this;
    }

    /**
     * Get departeddate
     *
     * @return \DateTime 
     */
    public function getDeparteddate()
    {
        return $this->departeddate;
    }

    /**
     * Set arrived
     *
     * @param boolean $arrived
     * @return Stock
     */
    public function setArrived($arrived)
    {
        $this->arrived = $arrived;

        return $this;
    }

    /**
     * Get arrived
     *
     * @return boolean 
     */
    public function getArrived()
    {
        return $this->arrived;
    }

    /**
     * Set arriveduserid
     *
     * @param integer $arriveduserid
     * @return Stock
     */
    public function setArriveduserid($arriveduserid)
    {
        $this->arriveduserid = $arriveduserid;

        return $this;
    }

    /**
     * Get arriveduserid
     *
     * @return integer 
     */
    public function getArriveduserid()
    {
        return $this->arriveduserid;
    }

    /**
     * Set arriveddate
     *
     * @param \DateTime $arriveddate
     * @return Stock
     */
    public function setArriveddate($arriveddate)
    {
        $this->arriveddate = $arriveddate;

        return $this;
    }

    /**
     * Get arriveddate
     *
     * @return \DateTime 
     */
    public function getArriveddate()
    {
        return $this->arriveddate;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     * @return Stock
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set verifieduserid
     *
     * @param integer $verifieduserid
     * @return Stock
     */
    public function setVerifieduserid($verifieduserid)
    {
        $this->verifieduserid = $verifieduserid;

        return $this;
    }

    /**
     * Get verifieduserid
     *
     * @return integer 
     */
    public function getVerifieduserid()
    {
        return $this->verifieduserid;
    }

    /**
     * Set verifieddate
     *
     * @param \DateTime $verifieddate
     * @return Stock
     */
    public function setVerifieddate($verifieddate)
    {
        $this->verifieddate = $verifieddate;

        return $this;
    }

    /**
     * Get verifieddate
     *
     * @return \DateTime 
     */
    public function getVerifieddate()
    {
        return $this->verifieddate;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Stock
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Stock
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Stock
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set invoiced
     *
     * @param boolean $invoiced
     * @return Stock
     */
    public function setInvoiced($invoiced)
    {
        $this->invoiced = $invoiced;

        return $this;
    }

    /**
     * Get invoiced
     *
     * @return boolean 
     */
    public function getInvoiced()
    {
        return $this->invoiced;
    }

    /**
     * Set invoiceduserid
     *
     * @param integer $invoiceduserid
     * @return Stock
     */
    public function setInvoiceduserid($invoiceduserid)
    {
        $this->invoiceduserid = $invoiceduserid;

        return $this;
    }

    /**
     * Get invoiceduserid
     *
     * @return integer 
     */
    public function getInvoiceduserid()
    {
        return $this->invoiceduserid;
    }

    /**
     * Set invoiceddate
     *
     * @param \DateTime $invoiceddate
     * @return Stock
     */
    public function setInvoiceddate($invoiceddate)
    {
        $this->invoiceddate = $invoiceddate;

        return $this;
    }

    /**
     * Get invoiceddate
     *
     * @return \DateTime 
     */
    public function getInvoiceddate()
    {
        return $this->invoiceddate;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return Stock
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set accountid
     *
     * @param integer $accountid
     * @return Stock
     */
    public function setAccountid($accountid)
    {
        $this->accountid = $accountid;

        return $this;
    }

    /**
     * Get accountid
     *
     * @return integer 
     */
    public function getAccountid()
    {
        return $this->accountid;
    }

    /**
     * Set fromcompanyid
     *
     * @param integer $fromcompanyid
     * @return Stock
     */
    public function setFromcompanyid($fromcompanyid)
    {
        $this->fromcompanyid = $fromcompanyid;

        return $this;
    }

    /**
     * Get fromcompanyid
     *
     * @return integer 
     */
    public function getFromcompanyid()
    {
        return $this->fromcompanyid;
    }

    /**
     * Set batchid
     *
     * @param integer $batchid
     * @return Stock
     */
    public function setBatchid($batchid)
    {
        $this->batchid = $batchid;

        return $this;
    }

    /**
     * Get batchid
     *
     * @return integer 
     */
    public function getBatchid()
    {
        return $this->batchid;
    }

    /**
     * Set partship
     *
     * @param integer $partship
     * @return Stock
     */
    public function setPartship($partship)
    {
        $this->partship = $partship;

        return $this;
    }

    /**
     * Get partship
     *
     * @return integer 
     */
    public function getPartship()
    {
        return $this->partship;
    }

    /**
     * Set pickstock
     *
     * @param boolean $pickstock
     * @return Stock
     */
    public function setPickstock($pickstock)
    {
        $this->pickstock = $pickstock;

        return $this;
    }

    /**
     * Get pickstock
     *
     * @return boolean 
     */
    public function getPickstock()
    {
        return $this->pickstock;
    }

    /**
     * Set rangeassign
     *
     * @param boolean $rangeassign
     * @return Stock
     */
    public function setRangeassign($rangeassign)
    {
        $this->rangeassign = $rangeassign;

        return $this;
    }

    /**
     * Get rangeassign
     *
     * @return boolean 
     */
    public function getRangeassign()
    {
        return $this->rangeassign;
    }

    /**
     * Set onstockonly
     *
     * @param boolean $onstockonly
     * @return Stock
     */
    public function setOnstockonly($onstockonly)
    {
        $this->onstockonly = $onstockonly;

        return $this;
    }

    /**
     * Get onstockonly
     *
     * @return boolean 
     */
    public function getOnstockonly()
    {
        return $this->onstockonly;
    }

    /**
     * Set inventorymode
     *
     * @param boolean $inventorymode
     * @return Stock
     */
    public function setInventorymode($inventorymode)
    {
        $this->inventorymode = $inventorymode;

        return $this;
    }

    /**
     * Get inventorymode
     *
     * @return boolean 
     */
    public function getInventorymode()
    {
        return $this->inventorymode;
    }

    /**
     * Set inventorypct
     *
     * @param integer $inventorypct
     * @return Stock
     */
    public function setInventorypct($inventorypct)
    {
        $this->inventorypct = $inventorypct;

        return $this;
    }

    /**
     * Get inventorypct
     *
     * @return integer 
     */
    public function getInventorypct()
    {
        return $this->inventorypct;
    }

    /**
     * Set deliverycomment
     *
     * @param string $deliverycomment
     * @return Stock
     */
    public function setDeliverycomment($deliverycomment)
    {
        $this->deliverycomment = $deliverycomment;

        return $this;
    }

    /**
     * Get deliverycomment
     *
     * @return string 
     */
    public function getDeliverycomment()
    {
        return $this->deliverycomment;
    }

    /**
     * Set carrierurl
     *
     * @param string $carrierurl
     * @return Stock
     */
    public function setCarrierurl($carrierurl)
    {
        $this->carrierurl = $carrierurl;

        return $this;
    }

    /**
     * Get carrierurl
     *
     * @return string 
     */
    public function getCarrierurl()
    {
        return $this->carrierurl;
    }

}
