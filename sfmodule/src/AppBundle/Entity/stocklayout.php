<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stocklayout
 */
class Stocklayout
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stockid;

    /**
     * @var string
     */
    private $position;

    /**
     * @var integer
     */
    private $occupied;

    /**
     * @var integer
     */
    private $max;

    /**
     * @var string
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Stocklayout
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Stocklayout
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set occupied
     *
     * @param integer $occupied
     * @return Stocklayout
     */
    public function setOccupied($occupied)
    {
        $this->occupied = $occupied;

        return $this;
    }

    /**
     * Get occupied
     *
     * @return integer 
     */
    public function getOccupied()
    {
        return $this->occupied;
    }

    /**
     * Set max
     *
     * @param integer $max
     * @return Stocklayout
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return integer 
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Stocklayout
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

}
