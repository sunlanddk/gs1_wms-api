<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stockconfiguration
 */
class Stockconfiguration
{
    /**
     * @var integer
     */
    private $stockid;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $containerid;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var integer
     */
    private $customercompanyid;

    /**
     * @var integer
     */
    private $customerstockid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var integer
     */
    private $collectionpricegroupid;

    /**
     * @var boolean
     */
    private $editprice;

    /**
     * @var boolean
     */
    private $addorder;

    /**
     * @var boolean
     */
    private $oskkeyboard;

    /**
     * @var boolean
     */
    private $osksoft;

    /**
     * @var boolean
     */
    private $usewholesaleprice;


    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Stockconfiguration
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Stockconfiguration
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set containerid
     *
     * @param integer $containerid
     * @return Stockconfiguration
     */
    public function setContainerid($containerid)
    {
        $this->containerid = $containerid;

        return $this;
    }

    /**
     * Get containerid
     *
     * @return integer 
     */
    public function getContainerid()
    {
        return $this->containerid;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Stockconfiguration
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set customercompanyid
     *
     * @param integer $customercompanyid
     * @return Stockconfiguration
     */
    public function setCustomercompanyid($customercompanyid)
    {
        $this->customercompanyid = $customercompanyid;

        return $this;
    }

    /**
     * Get customercompanyid
     *
     * @return integer 
     */
    public function getCustomercompanyid()
    {
        return $this->customercompanyid;
    }

    /**
     * Set customerstockid
     *
     * @param integer $customerstockid
     * @return Stockconfiguration
     */
    public function setCustomerstockid($customerstockid)
    {
        $this->customerstockid = $customerstockid;

        return $this;
    }

    /**
     * Get customerstockid
     *
     * @return integer 
     */
    public function getCustomerstockid()
    {
        return $this->customerstockid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Stockconfiguration
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set collectionpricegroupid
     *
     * @param integer $collectionpricegroupid
     * @return Stockconfiguration
     */
    public function setCollectionpricegroupid($collectionpricegroupid)
    {
        $this->collectionpricegroupid = $collectionpricegroupid;

        return $this;
    }

    /**
     * Get collectionpricegroupid
     *
     * @return integer 
     */
    public function getCollectionpricegroupid()
    {
        return $this->collectionpricegroupid;
    }

    /**
     * Set editprice
     *
     * @param boolean $editprice
     * @return Stockconfiguration
     */
    public function setEditprice($editprice)
    {
        $this->editprice = $editprice;

        return $this;
    }

    /**
     * Get editprice
     *
     * @return boolean 
     */
    public function getEditprice()
    {
        return $this->editprice;
    }

    /**
     * Set addorder
     *
     * @param boolean $addorder
     * @return Stockconfiguration
     */
    public function setAddorder($addorder)
    {
        $this->addorder = $addorder;

        return $this;
    }

    /**
     * Get addorder
     *
     * @return boolean 
     */
    public function getAddorder()
    {
        return $this->addorder;
    }

    /**
     * Set oskkeyboard
     *
     * @param boolean $oskkeyboard
     * @return Stockconfiguration
     */
    public function setOskkeyboard($oskkeyboard)
    {
        $this->oskkeyboard = $oskkeyboard;

        return $this;
    }

    /**
     * Get oskkeyboard
     *
     * @return boolean 
     */
    public function getOskkeyboard()
    {
        return $this->oskkeyboard;
    }

    /**
     * Set osksoft
     *
     * @param boolean $osksoft
     * @return Stockconfiguration
     */
    public function setOsksoft($osksoft)
    {
        $this->osksoft = $osksoft;

        return $this;
    }

    /**
     * Get osksoft
     *
     * @return boolean 
     */
    public function getOsksoft()
    {
        return $this->osksoft;
    }

    /**
     * Set usewholesaleprice
     *
     * @param boolean $usewholesaleprice
     * @return Stockconfiguration
     */
    public function setUsewholesaleprice($usewholesaleprice)
    {
        $this->usewholesaleprice = $usewholesaleprice;

        return $this;
    }

    /**
     * Get usewholesaleprice
     *
     * @return boolean 
     */
    public function getUsewholesaleprice()
    {
        return $this->usewholesaleprice;
    }
}
