<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paymentterm
 */
class Paymentterm
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $paydays;

    /**
     * @var integer
     */
    private $paymonths;

    /**
     * @var boolean
     */
    private $runningmonth;

    /**
     * @var integer
     */
    private $discountdays;

    /**
     * @var integer
     */
    private $discountpct;

    /**
     * @var string
     */
    private $gpkey;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Paymentterm
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Paymentterm
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Paymentterm
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Paymentterm
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Paymentterm
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Paymentterm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Paymentterm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set paydays
     *
     * @param integer $paydays
     * @return Paymentterm
     */
    public function setPaydays($paydays)
    {
        $this->paydays = $paydays;

        return $this;
    }

    /**
     * Get paydays
     *
     * @return integer 
     */
    public function getPaydays()
    {
        return $this->paydays;
    }

    /**
     * Set paymonths
     *
     * @param integer $paymonths
     * @return Paymentterm
     */
    public function setPaymonths($paymonths)
    {
        $this->paymonths = $paymonths;

        return $this;
    }

    /**
     * Get paymonths
     *
     * @return integer 
     */
    public function getPaymonths()
    {
        return $this->paymonths;
    }

    /**
     * Set runningmonth
     *
     * @param boolean $runningmonth
     * @return Paymentterm
     */
    public function setRunningmonth($runningmonth)
    {
        $this->runningmonth = $runningmonth;

        return $this;
    }

    /**
     * Get runningmonth
     *
     * @return boolean 
     */
    public function getRunningmonth()
    {
        return $this->runningmonth;
    }

    /**
     * Set discountdays
     *
     * @param integer $discountdays
     * @return Paymentterm
     */
    public function setDiscountdays($discountdays)
    {
        $this->discountdays = $discountdays;

        return $this;
    }

    /**
     * Get discountdays
     *
     * @return integer 
     */
    public function getDiscountdays()
    {
        return $this->discountdays;
    }

    /**
     * Set discountpct
     *
     * @param integer $discountpct
     * @return Paymentterm
     */
    public function setDiscountpct($discountpct)
    {
        $this->discountpct = $discountpct;

        return $this;
    }

    /**
     * Get discountpct
     *
     * @return integer 
     */
    public function getDiscountpct()
    {
        return $this->discountpct;
    }

    /**
     * Set gpkey
     *
     * @param string $gpkey
     * @return Paymentterm
     */
    public function setGpkey($gpkey)
    {
        $this->gpkey = $gpkey;

        return $this;
    }

    /**
     * Get gpkey
     *
     * @return string 
     */
    public function getGpkey()
    {
        return $this->gpkey;
    }
}
