<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Certificate
 */
class Certificate
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $certificatetypeid;

    /**
     * @var integer
     */
    private $holdercompanyid;

    /**
     * @var integer
     */
    private $suppliercompanyid;

    /**
     * @var \DateTime
     */
    private $validuntil;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Certificate
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Certificate
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Certificate
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Certificate
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Certificate
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Certificate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Certificate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set certificatetypeid
     *
     * @param integer $certificatetypeid
     * @return Certificate
     */
    public function setCertificatetypeid($certificatetypeid)
    {
        $this->certificatetypeid = $certificatetypeid;

        return $this;
    }

    /**
     * Get certificatetypeid
     *
     * @return integer 
     */
    public function getCertificatetypeid()
    {
        return $this->certificatetypeid;
    }

    /**
     * Set holdercompanyid
     *
     * @param integer $holdercompanyid
     * @return Certificate
     */
    public function setHoldercompanyid($holdercompanyid)
    {
        $this->holdercompanyid = $holdercompanyid;

        return $this;
    }

    /**
     * Get holdercompanyid
     *
     * @return integer 
     */
    public function getHoldercompanyid()
    {
        return $this->holdercompanyid;
    }

    /**
     * Set suppliercompanyid
     *
     * @param integer $suppliercompanyid
     * @return Certificate
     */
    public function setSuppliercompanyid($suppliercompanyid)
    {
        $this->suppliercompanyid = $suppliercompanyid;

        return $this;
    }

    /**
     * Get suppliercompanyid
     *
     * @return integer 
     */
    public function getSuppliercompanyid()
    {
        return $this->suppliercompanyid;
    }

    /**
     * Set validuntil
     *
     * @param \DateTime $validuntil
     * @return Certificate
     */
    public function setValiduntil($validuntil)
    {
        $this->validuntil = $validuntil;

        return $this;
    }

    /**
     * Get validuntil
     *
     * @return \DateTime 
     */
    public function getValiduntil()
    {
        return $this->validuntil;
    }
}
