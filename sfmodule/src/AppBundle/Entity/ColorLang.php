<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColorLang
 */
class ColorLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $colorid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colorid
     *
     * @param integer $colorid
     * @return ColorLang
     */
    public function setColorid($colorid)
    {
        $this->colorid = $colorid;

        return $this;
    }

    /**
     * Get colorid
     *
     * @return integer 
     */
    public function getColorid()
    {
        return $this->colorid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ColorLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ColorLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ColorLang
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
