<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Navigation
 */
class Navigation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $parentid;

    /**
     * @var integer
     */
    private $displayorder;

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $parameters;

    /**
     * @var string
     */
    private $function;

    /**
     * @var string
     */
    private $reftype;

    /**
     * @var string
     */
    private $header;

    /**
     * @var integer
     */
    private $functionid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $focus;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $navigationtypeid;

    /**
     * @var string
     */
    private $refparameters;

    /**
     * @var integer
     */
    private $width;

    /**
     * @var integer
     */
    private $height;

    /**
     * @var integer
     */
    private $back;

    /**
     * @var boolean
     */
    private $permowner;

    /**
     * @var boolean
     */
    private $perminternal;

    /**
     * @var string
     */
    private $mark;

    /**
     * @var boolean
     */
    private $disable;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $refconfirm;

    /**
     * @var boolean
     */
    private $refidinclude;

    /**
     * @var string
     */
    private $reftable;

    /**
     * @var boolean
     */
    private $projectselect;

    /**
     * @var boolean
     */
    private $mustload;

    /**
     * @var string
     */
    private $accesskey;

    /**
     * @var boolean
     */
    private $legacy;

    /**
     * @var boolean
     */
    private $showcart;

    /**
     * @var boolean
     */
    private $isdefault;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Navigation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return Navigation
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer 
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set displayorder
     *
     * @param integer $displayorder
     * @return Navigation
     */
    public function setDisplayorder($displayorder)
    {
        $this->displayorder = $displayorder;

        return $this;
    }

    /**
     * Get displayorder
     *
     * @return integer 
     */
    public function getDisplayorder()
    {
        return $this->displayorder;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Navigation
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set parameters
     *
     * @param string $parameters
     * @return Navigation
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get parameters
     *
     * @return string 
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set function
     *
     * @param string $function
     * @return Navigation
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get function
     *
     * @return string 
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set reftype
     *
     * @param string $reftype
     * @return Navigation
     */
    public function setReftype($reftype)
    {
        $this->reftype = $reftype;

        return $this;
    }

    /**
     * Get reftype
     *
     * @return string 
     */
    public function getReftype()
    {
        return $this->reftype;
    }

    /**
     * Set header
     *
     * @param string $header
     * @return Navigation
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return string 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set functionid
     *
     * @param integer $functionid
     * @return Navigation
     */
    public function setFunctionid($functionid)
    {
        $this->functionid = $functionid;

        return $this;
    }

    /**
     * Get functionid
     *
     * @return integer 
     */
    public function getFunctionid()
    {
        return $this->functionid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Navigation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Navigation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Navigation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set operation
     *
     * @param string $operation
     * @return Navigation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation
     *
     * @return string 
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set focus
     *
     * @param string $focus
     * @return Navigation
     */
    public function setFocus($focus)
    {
        $this->focus = $focus;

        return $this;
    }

    /**
     * Get focus
     *
     * @return string 
     */
    public function getFocus()
    {
        return $this->focus;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Navigation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Navigation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set navigationtypeid
     *
     * @param integer $navigationtypeid
     * @return Navigation
     */
    public function setNavigationtypeid($navigationtypeid)
    {
        $this->navigationtypeid = $navigationtypeid;

        return $this;
    }

    /**
     * Get navigationtypeid
     *
     * @return integer 
     */
    public function getNavigationtypeid()
    {
        return $this->navigationtypeid;
    }

    /**
     * Set refparameters
     *
     * @param string $refparameters
     * @return Navigation
     */
    public function setRefparameters($refparameters)
    {
        $this->refparameters = $refparameters;

        return $this;
    }

    /**
     * Get refparameters
     *
     * @return string 
     */
    public function getRefparameters()
    {
        return $this->refparameters;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Navigation
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return Navigation
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set back
     *
     * @param integer $back
     * @return Navigation
     */
    public function setBack($back)
    {
        $this->back = $back;

        return $this;
    }

    /**
     * Get back
     *
     * @return integer 
     */
    public function getBack()
    {
        return $this->back;
    }

    /**
     * Set permowner
     *
     * @param boolean $permowner
     * @return Navigation
     */
    public function setPermowner($permowner)
    {
        $this->permowner = $permowner;

        return $this;
    }

    /**
     * Get permowner
     *
     * @return boolean 
     */
    public function getPermowner()
    {
        return $this->permowner;
    }

    /**
     * Set perminternal
     *
     * @param boolean $perminternal
     * @return Navigation
     */
    public function setPerminternal($perminternal)
    {
        $this->perminternal = $perminternal;

        return $this;
    }

    /**
     * Get perminternal
     *
     * @return boolean 
     */
    public function getPerminternal()
    {
        return $this->perminternal;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Navigation
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set disable
     *
     * @param boolean $disable
     * @return Navigation
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;

        return $this;
    }

    /**
     * Get disable
     *
     * @return boolean 
     */
    public function getDisable()
    {
        return $this->disable;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Navigation
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set refconfirm
     *
     * @param string $refconfirm
     * @return Navigation
     */
    public function setRefconfirm($refconfirm)
    {
        $this->refconfirm = $refconfirm;

        return $this;
    }

    /**
     * Get refconfirm
     *
     * @return string 
     */
    public function getRefconfirm()
    {
        return $this->refconfirm;
    }

    /**
     * Set refidinclude
     *
     * @param boolean $refidinclude
     * @return Navigation
     */
    public function setRefidinclude($refidinclude)
    {
        $this->refidinclude = $refidinclude;

        return $this;
    }

    /**
     * Get refidinclude
     *
     * @return boolean 
     */
    public function getRefidinclude()
    {
        return $this->refidinclude;
    }

    /**
     * Set reftable
     *
     * @param string $reftable
     * @return Navigation
     */
    public function setReftable($reftable)
    {
        $this->reftable = $reftable;

        return $this;
    }

    /**
     * Get reftable
     *
     * @return string 
     */
    public function getReftable()
    {
        return $this->reftable;
    }

    /**
     * Set projectselect
     *
     * @param boolean $projectselect
     * @return Navigation
     */
    public function setProjectselect($projectselect)
    {
        $this->projectselect = $projectselect;

        return $this;
    }

    /**
     * Get projectselect
     *
     * @return boolean 
     */
    public function getProjectselect()
    {
        return $this->projectselect;
    }

    /**
     * Set mustload
     *
     * @param boolean $mustload
     * @return Navigation
     */
    public function setMustload($mustload)
    {
        $this->mustload = $mustload;

        return $this;
    }

    /**
     * Get mustload
     *
     * @return boolean 
     */
    public function getMustload()
    {
        return $this->mustload;
    }

    /**
     * Set accesskey
     *
     * @param string $accesskey
     * @return Navigation
     */
    public function setAccesskey($accesskey)
    {
        $this->accesskey = $accesskey;

        return $this;
    }

    /**
     * Get accesskey
     *
     * @return string 
     */
    public function getAccesskey()
    {
        return $this->accesskey;
    }

    /**
     * Set legacy
     *
     * @param boolean $legacy
     * @return Navigation
     */
    public function setLegacy($legacy)
    {
        $this->legacy = $legacy;

        return $this;
    }

    /**
     * Get legacy
     *
     * @return boolean 
     */
    public function getLegacy()
    {
        return $this->legacy;
    }

    /**
     * Set showcart
     *
     * @param boolean $showcart
     * @return Navigation
     */
    public function setShowcart($showcart)
    {
        $this->showcart = $showcart;

        return $this;
    }

    /**
     * Get showcart
     *
     * @return boolean 
     */
    public function getShowcart()
    {
        return $this->showcart;
    }

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     * @return Navigation
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return boolean 
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }
}
