<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Integrationtransactionlogs
 */
class Integrationtransactionlogs
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var string
     */
    private $exception;

    /**
     * @var integer
     */
    private $integrationtransactiontypeid;

    /**
     * @var integer
     */
    private $integrationtransactionentryid;

    /**
     * @var boolean
     */
    private $reexecute;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Integrationtransactionlogs
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Integrationtransactionlogs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set exception
     *
     * @param string $exception
     * @return Integrationtransactionlogs
     */
    public function setException($exception)
    {
        $this->exception = $exception;

        return $this;
    }

    /**
     * Get exception
     *
     * @return string 
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Set integrationtransactiontypeid
     *
     * @param integer $integrationtransactiontypeid
     * @return Integrationtransactionlogs
     */
    public function setIntegrationtransactiontypeid($integrationtransactiontypeid)
    {
        $this->integrationtransactiontypeid = $integrationtransactiontypeid;

        return $this;
    }

    /**
     * Get integrationtransactiontypeid
     *
     * @return integer 
     */
    public function getIntegrationtransactiontypeid()
    {
        return $this->integrationtransactiontypeid;
    }

    /**
     * Set integrationtransactionentryid
     *
     * @param integer $integrationtransactionentryid
     * @return Integrationtransactionlogs
     */
    public function setIntegrationtransactionentryid($integrationtransactionentryid)
    {
        $this->integrationtransactionentryid = $integrationtransactionentryid;

        return $this;
    }

    /**
     * Get integrationtransactionentryid
     *
     * @return integer 
     */
    public function getIntegrationtransactionentryid()
    {
        return $this->integrationtransactionentryid;
    }

    /**
     * Set reexecute
     *
     * @param boolean $reexecute
     * @return Integrationtransactionlogs
     */
    public function setReexecute($reexecute)
    {
        $this->reexecute = $reexecute;

        return $this;
    }

    /**
     * Get reexecute
     *
     * @return boolean 
     */
    public function getReexecute()
    {
        return $this->reexecute;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Integrationtransactionlogs
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Integrationtransactionlogs
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Integrationtransactionlogs
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Integrationtransactionlogs
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
