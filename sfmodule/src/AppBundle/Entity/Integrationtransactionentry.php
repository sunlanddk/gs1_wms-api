<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Integrationtransactionentry
 */
class Integrationtransactionentry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $integrationtransactionid;

    /**
     * @var \DateTime
     */
    private $sourcemodificationdate;

    /**
     * @var integer
     */
    private $sourceid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set integrationtransactionid
     *
     * @param integer $integrationtransactionid
     * @return Integrationtransactionentry
     */
    public function setIntegrationtransactionid($integrationtransactionid)
    {
        $this->integrationtransactionid = $integrationtransactionid;

        return $this;
    }

    /**
     * Get integrationtransactionid
     *
     * @return integer 
     */
    public function getIntegrationtransactionid()
    {
        return $this->integrationtransactionid;
    }

    /**
     * Set sourcemodificationdate
     *
     * @param \DateTime $sourcemodificationdate
     * @return Integrationtransactionentry
     */
    public function setSourcemodificationdate($sourcemodificationdate)
    {
        $this->sourcemodificationdate = $sourcemodificationdate;

        return $this;
    }

    /**
     * Get sourcemodificationdate
     *
     * @return \DateTime 
     */
    public function getSourcemodificationdate()
    {
        return $this->sourcemodificationdate;
    }

    /**
     * Set sourceid
     *
     * @param integer $sourceid
     * @return Integrationtransactionentry
     */
    public function setSourceid($sourceid)
    {
        $this->sourceid = $sourceid;

        return $this;
    }

    /**
     * Get sourceid
     *
     * @return integer 
     */
    public function getSourceid()
    {
        return $this->sourceid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Integrationtransactionentry
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Integrationtransactionentry
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Integrationtransactionentry
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Integrationtransactionentry
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
