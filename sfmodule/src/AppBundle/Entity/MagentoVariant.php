<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MagentoVariant
 *
 * @ORM\Table(name="magento_variant", indexes={@ORM\Index(name="id_pot", columns={"variant_code", "id_magento"})})
 * @ORM\Entity
 */
class MagentoVariant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="variant_code", type="integer", nullable=false)
     */
    private $variantCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_magento", type="integer", nullable=false)
     */
    private $idMagento;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variantCode
     *
     * @param integer $variantCode
     *
     * @return MagentoVariant
     */
    public function setVariantCode($variantCode)
    {
        $this->variantCode = $variantCode;

        return $this;
    }

    /**
     * Get variantCode
     *
     * @return integer
     */
    public function getVariantCode()
    {
        return $this->variantCode;
    }

    /**
     * Set idMagento
     *
     * @param integer $idMagento
     *
     * @return MagentoVariant
     */
    public function setIdMagento($idMagento)
    {
        $this->idMagento = $idMagento;

        return $this;
    }

    /**
     * Get idMagento
     *
     * @return integer
     */
    public function getIdMagento()
    {
        return $this->idMagento;
    }
}
