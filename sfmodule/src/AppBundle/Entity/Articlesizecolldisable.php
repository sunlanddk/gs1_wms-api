<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlesizecolldisable
 */
class Articlesizecolldisable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $collectionmemberid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Articlesizecolldisable
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set collectionmemberid
     *
     * @param integer $collectionmemberid
     * @return Articlesizecolldisable
     */
    public function setCollectionmemberid($collectionmemberid)
    {
        $this->collectionmemberid = $collectionmemberid;

        return $this;
    }

    /**
     * Get collectionmemberid
     *
     * @return integer 
     */
    public function getCollectionmemberid()
    {
        return $this->collectionmemberid;
    }
}
