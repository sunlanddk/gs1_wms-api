<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Outsourcingplan
 */
class Outsourcingplan
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $operationfromno;

    /**
     * @var integer
     */
    private $operationtono;

    /**
     * @var integer
     */
    private $locationid;

    /**
     * @var integer
     */
    private $position;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Outsourcingplan
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Outsourcingplan
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Outsourcingplan
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set operationfromno
     *
     * @param integer $operationfromno
     * @return Outsourcingplan
     */
    public function setOperationfromno($operationfromno)
    {
        $this->operationfromno = $operationfromno;

        return $this;
    }

    /**
     * Get operationfromno
     *
     * @return integer 
     */
    public function getOperationfromno()
    {
        return $this->operationfromno;
    }

    /**
     * Set operationtono
     *
     * @param integer $operationtono
     * @return Outsourcingplan
     */
    public function setOperationtono($operationtono)
    {
        $this->operationtono = $operationtono;

        return $this;
    }

    /**
     * Get operationtono
     *
     * @return integer 
     */
    public function getOperationtono()
    {
        return $this->operationtono;
    }

    /**
     * Set locationid
     *
     * @param integer $locationid
     * @return Outsourcingplan
     */
    public function setLocationid($locationid)
    {
        $this->locationid = $locationid;

        return $this;
    }

    /**
     * Get locationid
     *
     * @return integer 
     */
    public function getLocationid()
    {
        return $this->locationid;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Outsourcingplan
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}
