<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Styledocument
 */
class Styledocument
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $documentid;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var boolean
     */
    private $productionprint;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var \DateTime
     */
    private $productionprintdate;

    /**
     * @var integer
     */
    private $productionprintuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentid
     *
     * @param integer $documentid
     * @return Styledocument
     */
    public function setDocumentid($documentid)
    {
        $this->documentid = $documentid;

        return $this;
    }

    /**
     * Get documentid
     *
     * @return integer 
     */
    public function getDocumentid()
    {
        return $this->documentid;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Styledocument
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set productionprint
     *
     * @param boolean $productionprint
     * @return Styledocument
     */
    public function setProductionprint($productionprint)
    {
        $this->productionprint = $productionprint;

        return $this;
    }

    /**
     * Get productionprint
     *
     * @return boolean 
     */
    public function getProductionprint()
    {
        return $this->productionprint;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Styledocument
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Styledocument
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Styledocument
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Styledocument
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set productionprintdate
     *
     * @param \DateTime $productionprintdate
     * @return Styledocument
     */
    public function setProductionprintdate($productionprintdate)
    {
        $this->productionprintdate = $productionprintdate;

        return $this;
    }

    /**
     * Get productionprintdate
     *
     * @return \DateTime 
     */
    public function getProductionprintdate()
    {
        return $this->productionprintdate;
    }

    /**
     * Set productionprintuserid
     *
     * @param integer $productionprintuserid
     * @return Styledocument
     */
    public function setProductionprintuserid($productionprintuserid)
    {
        $this->productionprintuserid = $productionprintuserid;

        return $this;
    }

    /**
     * Get productionprintuserid
     *
     * @return integer 
     */
    public function getProductionprintuserid()
    {
        return $this->productionprintuserid;
    }
}
