<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Companyclass
 */
class Companyclass
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $gpkey;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $issupplier;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gpkey
     *
     * @param string $gpkey
     * @return Companyclass
     */
    public function setGpkey($gpkey)
    {
        $this->gpkey = $gpkey;

        return $this;
    }

    /**
     * Get gpkey
     *
     * @return string 
     */
    public function getGpkey()
    {
        return $this->gpkey;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Companyclass
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set issupplier
     *
     * @param boolean $issupplier
     * @return Companyclass
     */
    public function setIssupplier($issupplier)
    {
        $this->issupplier = $issupplier;

        return $this;
    }

    /**
     * Get issupplier
     *
     * @return boolean 
     */
    public function getIssupplier()
    {
        return $this->issupplier;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Companyclass
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Companyclass
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Companyclass
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Companyclass
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
