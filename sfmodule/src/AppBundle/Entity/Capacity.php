<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Capacity
 */
class Capacity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $actualdate;

    /**
     * @var integer
     */
    private $workgroupid;

    /**
     * @var integer
     */
    private $operators;

    /**
     * @var string
     */
    private $productionminutes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Capacity
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Capacity
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Capacity
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Capacity
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Capacity
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set actualdate
     *
     * @param \DateTime $actualdate
     * @return Capacity
     */
    public function setActualdate($actualdate)
    {
        $this->actualdate = $actualdate;

        return $this;
    }

    /**
     * Get actualdate
     *
     * @return \DateTime 
     */
    public function getActualdate()
    {
        return $this->actualdate;
    }

    /**
     * Set workgroupid
     *
     * @param integer $workgroupid
     * @return Capacity
     */
    public function setWorkgroupid($workgroupid)
    {
        $this->workgroupid = $workgroupid;

        return $this;
    }

    /**
     * Get workgroupid
     *
     * @return integer 
     */
    public function getWorkgroupid()
    {
        return $this->workgroupid;
    }

    /**
     * Set operators
     *
     * @param integer $operators
     * @return Capacity
     */
    public function setOperators($operators)
    {
        $this->operators = $operators;

        return $this;
    }

    /**
     * Get operators
     *
     * @return integer 
     */
    public function getOperators()
    {
        return $this->operators;
    }

    /**
     * Set productionminutes
     *
     * @param string $productionminutes
     * @return Capacity
     */
    public function setProductionminutes($productionminutes)
    {
        $this->productionminutes = $productionminutes;

        return $this;
    }

    /**
     * Get productionminutes
     *
     * @return string 
     */
    public function getProductionminutes()
    {
        return $this->productionminutes;
    }
}
