<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisitioninvoices
 */
class Requisitioninvoices
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $requisitionid;

    /**
     * @var string
     */
    private $receivalnumber;

    /**
     * @var string
     */
    private $invoicenumber;

    /**
     * @var string
     */
    private $internalref;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requisitionid
     *
     * @param integer $requisitionid
     * @return Requisitioninvoices
     */
    public function setRequisitionid($requisitionid)
    {
        $this->requisitionid = $requisitionid;

        return $this;
    }

    /**
     * Get requisitionid
     *
     * @return integer 
     */
    public function getRequisitionid()
    {
        return $this->requisitionid;
    }

    /**
     * Set receivalnumber
     *
     * @param string $receivalnumber
     * @return Requisitioninvoices
     */
    public function setReceivalnumber($receivalnumber)
    {
        $this->receivalnumber = $receivalnumber;

        return $this;
    }

    /**
     * Get receivalnumber
     *
     * @return string 
     */
    public function getReceivalnumber()
    {
        return $this->receivalnumber;
    }

    /**
     * Set invoicenumber
     *
     * @param string $invoicenumber
     * @return Requisitioninvoices
     */
    public function setInvoicenumber($invoicenumber)
    {
        $this->invoicenumber = $invoicenumber;

        return $this;
    }

    /**
     * Get invoicenumber
     *
     * @return string 
     */
    public function getInvoicenumber()
    {
        return $this->invoicenumber;
    }

    /**
     * Set internalref
     *
     * @param string $internalref
     * @return Requisitioninvoices
     */
    public function setInternalref($internalref)
    {
        $this->internalref = $internalref;

        return $this;
    }

    /**
     * Get internalref
     *
     * @return string 
     */
    public function getInternalref()
    {
        return $this->internalref;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Requisitioninvoices
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Requisitioninvoices
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Requisitioninvoices
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Requisitioninvoices
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
