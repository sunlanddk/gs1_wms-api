<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Claim
 */
class Claim
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $claimstateid;

    /**
     * @var integer
     */
    private $claimtypeid;

    /**
     * @var integer
     */
    private $invoiceid;

    /**
     * @var integer
     */
    private $creditnoteid;

    /**
     * @var integer
     */
    private $pickorderid;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $fromcompanyid;

    /**
     * @var integer
     */
    private $tocompanyid;

    /**
     * @var string
     */
    private $invoiceheader;

    /**
     * @var string
     */
    private $invoicefooter;

    /**
     * @var string
     */
    private $internalcomment;

    /**
     * @var string
     */
    private $externalcomment;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * @var integer
     */
    private $currencyid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Claim
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Claim
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Claim
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Claim
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Claim
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Claim
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Claim
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set claimstateid
     *
     * @param integer $claimstateid
     * @return Claim
     */
    public function setClaimstateid($claimstateid)
    {
        $this->claimstateid = $claimstateid;

        return $this;
    }

    /**
     * Get claimstateid
     *
     * @return integer 
     */
    public function getClaimstateid()
    {
        return $this->claimstateid;
    }

    /**
     * Set claimtypeid
     *
     * @param integer $claimtypeid
     * @return Claim
     */
    public function setClaimtypeid($claimtypeid)
    {
        $this->claimtypeid = $claimtypeid;

        return $this;
    }

    /**
     * Get claimtypeid
     *
     * @return integer 
     */
    public function getClaimtypeid()
    {
        return $this->claimtypeid;
    }

    /**
     * Set invoiceid
     *
     * @param integer $invoiceid
     * @return Claim
     */
    public function setInvoiceid($invoiceid)
    {
        $this->invoiceid = $invoiceid;

        return $this;
    }

    /**
     * Get invoiceid
     *
     * @return integer 
     */
    public function getInvoiceid()
    {
        return $this->invoiceid;
    }

    /**
     * Set creditnoteid
     *
     * @param integer $creditnoteid
     * @return Claim
     */
    public function setCreditnoteid($creditnoteid)
    {
        $this->creditnoteid = $creditnoteid;

        return $this;
    }

    /**
     * Get creditnoteid
     *
     * @return integer 
     */
    public function getCreditnoteid()
    {
        return $this->creditnoteid;
    }

    /**
     * Set pickorderid
     *
     * @param integer $pickorderid
     * @return Claim
     */
    public function setPickorderid($pickorderid)
    {
        $this->pickorderid = $pickorderid;

        return $this;
    }

    /**
     * Get pickorderid
     *
     * @return integer 
     */
    public function getPickorderid()
    {
        return $this->pickorderid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Claim
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set fromcompanyid
     *
     * @param integer $fromcompanyid
     * @return Claim
     */
    public function setFromcompanyid($fromcompanyid)
    {
        $this->fromcompanyid = $fromcompanyid;

        return $this;
    }

    /**
     * Get fromcompanyid
     *
     * @return integer 
     */
    public function getFromcompanyid()
    {
        return $this->fromcompanyid;
    }

    /**
     * Set tocompanyid
     *
     * @param integer $tocompanyid
     * @return Claim
     */
    public function setTocompanyid($tocompanyid)
    {
        $this->tocompanyid = $tocompanyid;

        return $this;
    }

    /**
     * Get tocompanyid
     *
     * @return integer 
     */
    public function getTocompanyid()
    {
        return $this->tocompanyid;
    }

    /**
     * Set invoiceheader
     *
     * @param string $invoiceheader
     * @return Claim
     */
    public function setInvoiceheader($invoiceheader)
    {
        $this->invoiceheader = $invoiceheader;

        return $this;
    }

    /**
     * Get invoiceheader
     *
     * @return string 
     */
    public function getInvoiceheader()
    {
        return $this->invoiceheader;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Claim
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string 
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

    /**
     * Set internalcomment
     *
     * @param string $internalcomment
     * @return Claim
     */
    public function setInternalcomment($internalcomment)
    {
        $this->internalcomment = $internalcomment;

        return $this;
    }

    /**
     * Get internalcomment
     *
     * @return string 
     */
    public function getInternalcomment()
    {
        return $this->internalcomment;
    }

    /**
     * Set externalcomment
     *
     * @param string $externalcomment
     * @return Claim
     */
    public function setExternalcomment($externalcomment)
    {
        $this->externalcomment = $externalcomment;

        return $this;
    }

    /**
     * Get externalcomment
     *
     * @return string 
     */
    public function getExternalcomment()
    {
        return $this->externalcomment;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Claim
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer 
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Claim
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Claim
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Claim
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }
}
