<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Login
 */
class Login
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $sessionid;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var \DateTime
     */
    private $logindate;

    /**
     * @var \DateTime
     */
    private $logoutdate;

    /**
     * @var \DateTime
     */
    private $accessdate;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $selecttype;

    /**
     * @var integer
     */
    private $selectid;

    /**
     * @var string
     */
    private $acceptedmimetypes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionid
     *
     * @param integer $sessionid
     * @return Login
     */
    public function setSessionid($sessionid)
    {
        $this->sessionid = $sessionid;

        return $this;
    }

    /**
     * Get sessionid
     *
     * @return integer 
     */
    public function getSessionid()
    {
        return $this->sessionid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Login
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set logindate
     *
     * @param \DateTime $logindate
     * @return Login
     */
    public function setLogindate($logindate)
    {
        $this->logindate = $logindate;

        return $this;
    }

    /**
     * Get logindate
     *
     * @return \DateTime 
     */
    public function getLogindate()
    {
        return $this->logindate;
    }

    /**
     * Set logoutdate
     *
     * @param \DateTime $logoutdate
     * @return Login
     */
    public function setLogoutdate($logoutdate)
    {
        $this->logoutdate = $logoutdate;

        return $this;
    }

    /**
     * Get logoutdate
     *
     * @return \DateTime 
     */
    public function getLogoutdate()
    {
        return $this->logoutdate;
    }

    /**
     * Set accessdate
     *
     * @param \DateTime $accessdate
     * @return Login
     */
    public function setAccessdate($accessdate)
    {
        $this->accessdate = $accessdate;

        return $this;
    }

    /**
     * Get accessdate
     *
     * @return \DateTime 
     */
    public function getAccessdate()
    {
        return $this->accessdate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Login
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set selecttype
     *
     * @param string $selecttype
     * @return Login
     */
    public function setSelecttype($selecttype)
    {
        $this->selecttype = $selecttype;

        return $this;
    }

    /**
     * Get selecttype
     *
     * @return string 
     */
    public function getSelecttype()
    {
        return $this->selecttype;
    }

    /**
     * Set selectid
     *
     * @param integer $selectid
     * @return Login
     */
    public function setSelectid($selectid)
    {
        $this->selectid = $selectid;

        return $this;
    }

    /**
     * Get selectid
     *
     * @return integer 
     */
    public function getSelectid()
    {
        return $this->selectid;
    }

    /**
     * Set acceptedmimetypes
     *
     * @param string $acceptedmimetypes
     * @return Login
     */
    public function setAcceptedmimetypes($acceptedmimetypes)
    {
        $this->acceptedmimetypes = $acceptedmimetypes;

        return $this;
    }

    /**
     * Get acceptedmimetypes
     *
     * @return string 
     */
    public function getAcceptedmimetypes()
    {
        return $this->acceptedmimetypes;
    }
}
