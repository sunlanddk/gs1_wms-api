<?php
// src/AppBundle/Controller/ReportsController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Legacy\Lib\ListHelper;
use Legacy\Lib\ArrayHelper;
use Legacy\Lib\LogHelper;
use Legacy\Lib\FormHelper;


class ReportsController extends Controller
{
    //const XML_CONFIG_PATH = __DIR__.'/../../../reportsconf/';
	const XML_CONFIG_PATH = REPORTXML_PATH;
    
    const LINES = 20;
    const START_DATE = "-1 month";
    const DISPLAY_ERRORS = false;

    //field types
    const STRING = 0;
    const DATE = 1;
    const INTEGER = 2;
    const FLOAT = 3;
    const BOOLEAN = 4;
    const SELECT = 5;
    const IMGLINK = 6;

    private $_sqlQuery = '';
    private $_session = null;
    private $_totalRecords = 0;

    protected function _getFieldType($type)
    {

        switch (strtolower($type)) {
            case 'number':
            case 'integer':
                $fieldtype = self::INTEGER;
                break;
            case 'datetime':
                $fieldtype = self::DATE;
                break;
            case 'decimal':
                $fieldtype = self::FLOAT;
                break;
            case 'img':
                $fieldtype = self::IMGLINK;
                break;

            default:
                $fieldtype = self::STRING;
                break;
        }

        return $fieldtype;
    }
    
    protected function _getSortedList($ord)
    {
        $groupSorted = array();
        $entitySorted = array();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Reportgroup');
        $fileList = glob(self::XML_CONFIG_PATH.'{*.xml}', GLOB_BRACE);
        if($ord=='id'){
           natsort($fileList);
        }
        
        foreach($fileList as $file) {
            $row = @simplexml_load_file($file);
            if($row){
                if((bool)$row->HideFromReportList || (bool)$row->HidefromReportList){
                    continue;
                }
                $gid = (int)$row->ReportGroupId;
                $gname = $repository->findOneById($gid)->getDescription();
                $groupSorted[substr($gname,0,3)] = $gname;
                if(in_array($ord,array('dir','id'))){
                    $entitySorted[substr($gname,0,3)][]=$row;
                }else if($ord=='name'){
                    $entitySorted[substr($gname,0,3)][str_replace(' ','-',$row->Name).'('.$row->Id.')']=$row;
                }else{
                    $entitySorted[substr($gname,0,3)][str_replace(' ','-',$row->Description).'('.$row->Id.')']=$row;
                }
            }
        }
        
        if(!in_array($ord,array('id','dir'))){
            ksort($groupSorted);
            foreach($groupSorted as $gidx => $gname){
                ksort($entitySorted[$gidx]);
            }
        }
        
        return array($groupSorted, $entitySorted);
    }

    protected function _setFilterAndSortParameters($filters, $ftypes, $sql, $sort)
    {
        if(count($filters)){
            $sqlQuery = "select * from (".$sql.") as wynik where ";
        }else{
            $sqlQuery = "select * from (".$sql.") as wynik ";
        }

        foreach($filters as $key => $val){
            if($key == 'nid' || $key == 'clearFilter'){
                continue;
            }
            switch ($ftypes[$key]) {
                case self::STRING:
                case self::SELECT:
                case self::DATE:
                    $sqlQuery .= " wynik.". $key . " like '%".$val."%'";
                    break;
                case self::BOOLEAN:
                    $query = (bool)$val ? ' is true' : ' is false';
                    $sqlQuery .= " wynik.". $key . $query;
                    break;

                default:
                    $sqlQuery .= " wynik.". $key ." ". $this->get('app.utils.mysql')->sanitizeTextComp($val);
                    break;
            }

            if((count($filters)>1) && ($key != end(array_keys($filters)))){
                $sqlQuery .= " and ";
            }
        }

        if(!reset($sort)){
            if(!$sort[key($sort)] = key($sort)){
                return $sqlQuery;
            }
        }

        // add sorting
        $order = (key($sort) >= 0) ? " ASC" : " DESC";
        if(!strstr(reset($sort),',')){
            $sortCols =  (array)reset($sort);
        }else{
            $sortArr = explode(',',reset($sort));
            foreach($sortArr as $col){
                $sortCols[] = trim($col);
            }
        }

        $sqlQuery = $sqlQuery . " ORDER BY ";
        foreach($sortCols as $key => $col){
            if(strstr($col,' ')){
                $pieces = explode(' ',$col);
                $lastWord = array_pop($pieces);
                if(in_array(strtolower($lastWord), array('asc','desc'))){
                    $sqlQuery = $sqlQuery." wynik.". $col . ',';
                }
            }else{
                $sqlQuery = $sqlQuery." wynik.". $col . $order . ',';
            }

            if((count($sortCols)) && ($key == end(array_keys($sortCols)))){
                $sqlQuery = rtrim($sqlQuery, ',');
            }
        }

        return $sqlQuery;
    }
    
    protected function _getFilterParams($post)
    {
        // filter params
        $filters = array_filter($post);

        // if sql params have changed then clear filters
        if(isset($filters['paramSqlForm']) || (isset($filters['nid']) && $filters['nid']==2331)){
            $this->_session->set('filters',array());
            return array();
        }

        if(isset($filters['nid'])){
            unset($filters['nid']);
            unset($filters['id']);
        }else{
            $sessionFilters = $this->_session->get('filters');
        }
        if(isset($filters['clearFilter'])) {
            unset($filters['clearFilter']);
        }
        // clear report action params
        foreach($filters as $key => $val){
            if(is_numeric($key)){
                unset($filters[$key]);
            }
        }

        // page change don't change filters
        $isPager = filter_input(INPUT_GET,'offset');
        // sort column don't change filters
        $isSort = filter_input(INPUT_GET,'sort');
        if(strlen($isPager) || strlen($isSort)){
            return $sessionFilters;
        }else {
            $sessionFilters = $this->_setSessionParam('filters',$filters);
        }

        return $sessionFilters;
    }
    
    protected function _setSqlQueryExtraIncludes($sql,$fields)
    {
        //check if pattern "[PlaceExtraIncludeCol {ColumnIndex}]" exists
        preg_match_all("/\[PlaceExtraIncludeCol\s+\d+\]/i", $sql, $matches);

        if(!count($matches[0])){
            return $sql;
        }

        foreach($matches[0] as $key => $mExtField){
            $colIdx = preg_split('/\s+/',$mExtField);
            $colIdx = str_replace(']','',$colIdx[1]);

            $sql = str_replace($mExtField, " and ".$fields[$colIdx+1]['extIncl'], $sql);

        }
        
        return $sql;
    }

    protected function _setSqlQueryParameters($sql,$params)
    {
        $sessionParam = $this->_session->get('paramSet');
        $paramSet = count($sessionParam) ? $sessionParam : array();

        foreach($params as $replaceStr => $param) {
            //sql statement parameters replacement
            if(isset($sessionParam[$replaceStr]) && isset($param['results'])){
                foreach($param['results'] as $result){
                    $resultID = isset($result['Id'])?$result['Id']:$result['ID'];
                    if($resultID == $sessionParam[$replaceStr]){
                        $paramStr = $resultID;
                    }
                }
            }else if(isset($sessionParam[$replaceStr])){
                $paramStr = $sessionParam[$replaceStr];
            }else{ //first assigning
                switch(strtolower($param['type'])){
                    case 'combo':
                        $paramStr = isset($param['results'][0]['Id'])?$param['results'][0]['Id']:$param['results'][0]['ID'];
                        $paramSet[$replaceStr] = $paramStr;
                        break;
                    case 'date':
                        $paramStr = $param['value'];
                        $paramSet[$replaceStr] = $param['value'];
                        break;

                    default:
                        $paramStr = "";
                        $paramSet[$replaceStr] = "";
                        break;
                }
            }

            $paramStr = str_replace("'","\\'",$paramStr);
            $sql = str_replace('[' . $replaceStr . ']', ' "' .$paramStr. '" ', $sql);
            $paramStr = "";
        }

        // save to session if needed
        $paramSet = $this->_setSessionParam('paramSet',$paramSet);

        // check if all parameters have been changed
        preg_match_all("/\[[a-z]+\]/i", $sql, $matches);
        if(count($matches[0])){
            $logHelper = new LogHelper();
            global $User;
            $logHelper->logPrintf(logFATAL, sprintf("unmatched parameters in XML file %s <SqlStatment>: %s, session params: %s, IP %s, uid %s",
                'id:'.trim($this->_session->get('reportId')),
                join(', ',$matches[0]),
                print_r($paramSet, true),
                $_SERVER["REMOTE_ADDR"],
                $User['Id']));
        }

        return $sql;
    }

    protected function  _setSessionParam($name,$new)
    {
        $old = $this->_session->get($name);
        if(!$old || (isset($old) && $this->get('app.utils.arrays')->arrayDiff($old, $new)) ){
            $this->_session->set($name, $new);
            // if filters or params changed then clear totals row
            $this->_session->remove('totalsRow');

            return $this->_session->get($name);
        }

        return $old;
    }

    protected function _getReportItemsTotals($fields)
    {
        if(!empty($this->_session->get('totalsRow'))){
            return $this->_session->get('totalsRow');
        }
        
        $sqlQ = $this->_sqlQuery;
        $totals = array();
        if($sqlQ){
            $sql = 'select ';
            foreach($fields as $key => $data){
                if($data['isTotal']){
                    $sql .= 'sum('.$data['db'].') as total'.$data['db'];
                    $sql .= ',';
                }
            }
            // if there are no totals
            if(!strcmp($sql,'select ')){
                return $totals;
            }
            $sql = rtrim($sql, ",");
            $sql .= ' from ('.$sqlQ.') as wynik';
           
            $connection = $this->getDoctrine()->getManager()->getConnection();
            $query = $connection->prepare($sql);
            try{
               $query->execute();
            }catch(\Exception  $e){
               $this->addFlash('error', sprintf('<b>Error</b>: %s', $e->getMessage()) );
            }

            $totalValue = $query->fetch();

            foreach($fields as $key => $data){
                if($data['isTotal']){
                   $totals[$data['db']] = $totalValue['total'.$data['db']];
                }
            }
       }
        $this->_session->set('totalsRow',$totals);

        return $totals;
    }

    protected function _setSqlQueryGrouped($sql,$fields)
    {
        $groupBy = $fields[$this->_session->get('groupBy')]['groupBy'];
        $colName = $fields[$this->_session->get('groupBy')]['db'];
        $colType = $fields[$this->_session->get('groupBy')]['type'];
        $groupByExp = $groupBy['GroupExpression'];
        $groupByAgg = $groupBy['Aggregate'];
        if($colType == self::FLOAT){
            $colNameValue = 'round('.$colName.',2)';
        }else{
            $colNameValue = $colName;
        }

        $sqlQueryGroup = "select concat(wynikgr.".$groupByExp.", ' (', ".$groupByAgg."(".$colNameValue."),')') as Aggregated".$colName." ,wynikgr.* from (";
        $sqlQueryGroup .= $sql;
        $sqlQueryGroup .= ") as wynikgr group by ".$groupByExp;

        return $sqlQueryGroup;
    }

    protected function _getReportItems($fields, $params, $filters, $nopage = false)
    {
         // pager param
         $lines_offset = filter_input(INPUT_GET,'offset');
         $sessionOffset = isset($lines_offset) ? $lines_offset : $this->_session->get('offset');
         $this->_session->set('offset', $sessionOffset);
         $sessionSort = $this->_session->get('sort');

         $sqlQuery = $this->_sqlQuery;
         if($sqlQuery){
             // set sql statement parameters
             if(count($params)){
                 $sqlQuery = $this->_setSqlQueryParameters($sqlQuery,$params);
             }
             //add extra includes params from fields
             $sqlQuery = $this->_setSqlQueryExtraIncludes($sqlQuery,$fields);
             //add filter and sorting parameters

             if(count($filters) || $sessionSort){
                 //if filterBy exists then override sortExp
                 //if(isset($fields[abs($sessionSort)]['filterBy']) && $fields[abs($sessionSort)]['filterBy']){
                 //    $sortExp = $fields[abs($sessionSort)]['filterBy'];
                 //}else{
                     $sortExp = $fields[abs($sessionSort)]['sortExp'];
                 //}

                 if($fields[abs($sessionSort)]['sortExp']){
                     $sortField = array($sessionSort => $sortExp);
                 }else{
                     $sortField = array($sessionSort => $fields[abs($sessionSort)]['db']);
                 }

                 $filterTypes = array();
                 foreach($fields as $key => $field){
                     if(isset($field['db']) && isset($filters[$field['db']])){
                         $filterTypes[$field['db']] = $fields[$key]['type'];
                     }
                 }
                 $sqlQuery = $this->_setFilterAndSortParameters($filters, $filterTypes, $sqlQuery, $sortField);

                 //if aggregated option was chosen
                 if($this->_session->get('groupBy')){
                     $sqlQuery = $this->_setSqlQueryGrouped($sqlQuery,$fields);
                 }
             }
             $this->_sqlQuery = $sqlQuery;

             // user sql statement params have advantage
             $limit = (!(stripos($sqlQuery, 'limit') !== false)) ? self::LINES : null;
             $offset = (!(stripos($sqlQuery, 'offset') !== false) && ($sessionOffset > 0)) ? $sessionOffset : 0;

             $connection = $this->getDoctrine()->getManager()->getConnection();
             $stmt = $connection->prepare($sqlQuery);
             try{
                 $stmt->execute();
                 $items = $stmt->fetchAll();
             }catch(\Exception  $e){
                 $this->addFlash('error', sprintf('<b>Error</b>: %s', $e->getMessage()) );
            }
            if(!isset($items)){
               $items = array();
           }
           
            $this->_totalRecords = count($items);
            while($this->_totalRecords < $offset){
                $offset -= self::LINES;
            }

            if($nopage){
                return $items;
            }else{
                return array_slice($items, $offset, $limit);
            }             
         }

         return null;
    }

    protected function _getParamFieldChoices($list)
    {
        foreach($list as $value){
            $idx = reset($value);
            $choices[$idx] = $value['Value'];
        }
        
        return $choices;
    }

    protected function _getReportParamsForm($params)
    {
        $formHelper = new FormHelper();

        $isLoaded = false;
        $form = $formHelper->formStart($this->_session->get('reportId'),"paramFilters");

        foreach($params as $name => $param){
            if($param['hideDisplay']){
                continue;
            }
            $style = "width: ".$param['width']."px; margin:5px;";
            $paramSet = $this->_session->get('paramSet');
            $class = ($param['require']) ? " class=required" : '';
            switch (strtolower($param['type'])){
                case 'combo':
                    $choices = $this->_getParamFieldChoices($param['results']);
                    $form .= '<div class="input-container">';
                    $form.= $formHelper->formLabel($param['desc'],$param['require']);
                    $style .= " border:1px solid lightgrey;";
                    $form .= $formHelper->formSelect($name, $paramSet[$name], $choices, $style, $class);
                    $form .= '</div>';
                    $isLoaded = true;
                    break;
                case 'checkbox':
                    $form .= '<div class="input-container">';
                    $form.= $formHelper->formLabel($param['desc'],$param['require']);
                    $style .= " border:1px solid lightgrey;";
                    $form .= $formHelper->formCheckbox($name, $paramSet[$name], $style, $class);
                    $form .= '</div>';
                    $isLoaded = true;
                    break;
                case 'date':
                    $form .= '<div class="input-container">';
                    $form.= $formHelper->formLabel($param['desc'],$param['require']);
                    $form .= $formHelper->formCalendar();
                    $style .= " border:1px solid lightgrey;";
                    $form .= $formHelper->formDate($name, $paramSet[$name], $style, $class);
                    $form .= '</div>';
                    $isLoaded = true;
                    break;

                default:
                    $form .= '<div class="input-container">';
                    $form.= $formHelper->formLabel($param['desc'],$param['require']);
                    $style .= " border:1px solid lightgrey;";
                    $form .= $formHelper->formText($name, $paramSet[$name], $param['maxlen'], $param['width'], $style, $class);
                    $form .= '</div>';
                    $isLoaded = true;
                    break;
            }
        }
        $form .= $formHelper->formHidden('paramSqlForm', '1'); 

        if($isLoaded) {
            $form .= $formHelper->formSubmit();
        }
        $form .= $formHelper->formEnd();

        return $form;
    }

    protected function _getReportAdditionalHeader($xmlAddHeader)
    {
        $xmlHeaderFields = (array)$xmlAddHeader['AdditionalHeaderFields'];
        $xmlHeaderFields = $xmlHeaderFields['AdditionalHeaderField'];
        if(!is_array($xmlHeaderFields)){
            $ptmp = array();
            $ptmp[0] = $xmlHeaderFields;
            $xmlHeaderFields = $ptmp;
        }

        $addHeaderFields = array();
        foreach($xmlHeaderFields as $k => $ahfield){
            $addHeaderFields[$k]['DataSource'] = trim($ahfield->DataSource);
            $addHeaderFields[$k]['DisplayName'] = trim($ahfield->DisplayName);
        }

        return $addHeaderFields;
    }

    protected function _getReportAction($xmlAction,$items,$param)
    {
        global $User;

        $action = array();
        if(count($xmlAction)){
            // action statement for each selected row, 2 placeholders 
            // [UserId] currently logged in user id and 
            // [identifier] value from 'identifier' column
            $actionSqlStatement = $xmlAction['ActionStatement'];
            $actionSqlStatement = str_replace('[userId]',$User['Id'],$actionSqlStatement);

            $actionIdentifier = $xmlAction['ActionIdentifierColumn'];
           if(!empty($param) && is_array($param)){
               $results = array();
               $connection = $this->getDoctrine()->getManager()->getConnection();
               foreach($param as $key => $value){
                   if(!is_numeric($key)){
                       continue;
                   }
                   $actionStatement = str_replace('[identifier]',$value,$actionSqlStatement);
                   $actionQuery = $connection->prepare($actionStatement);
                   try{
                        $result = ($actionQuery->execute()) ? 'correct' : 'incorrect';
                    }catch(\Exception  $e){
                        $this->addFlash('error', sprintf('<b>Error</b>: %s', $e->getMessage()) );
                    }
                   $results[$key] = $value.":". $result; 
               }
               if(count($results)){
                    $this->addFlash('notice', sprintf('<b>Action results</b>: %s', implode('; ',$results)) );
               }
           }
           $action['index'] = $actionIdentifier;
           // prepare action column view
           foreach($items as $values){
               $action['values'][$values[$actionIdentifier]] = true;
           }
        }

        return $action;
    }

    protected function _getReportSqlParams($parameters, $sqlParam)
    {
        // sql param
        if(isset($sqlParam['paramSqlForm'])){
            if(isset($sqlParam['nid'])){
                unset($sqlParam['nid']);
            }
            unset($sqlParam['paramSqlForm']);
            $sessionParam = $this->_setSessionParam('paramSet',$sqlParam);
        }else{
            $sessionParam = $this->_session->get('paramSet');
        }

        // diffrence in reading xml where 1 or more parameters
        if(!is_array($parameters) && $parameters->Name){
            $ptmp = array();
            $ptmp[0] = $parameters;
            $parameters = $ptmp;
        }

        $params = array();
        foreach($parameters as $param){
            $paramName = trim($param->Name);
            $params[$paramName]['sql'] = array();
            $params[$paramName]['sql'] = trim($param->ParameterQuery);
            $params[$paramName]['desc'] = trim($param->Description);
            $params[$paramName]['width'] = trim($param->Width);
            $params[$paramName]['type'] = trim($param->Type);
            $params[$paramName]['maxlen'] = trim($param->MaxLength);
            $hideFromDisplay = trim($param->HideFromDisplay)=='true' ? 1 : 0;
            $params[$paramName]['hide'] = (bool) $hideFromDisplay;
            $hideFromParamFilters = trim($param->HideFromParamFilters)=='true' ? 1 : 0;
            $params[$paramName]['hideDisplay'] = (bool) $hideFromParamFilters;
            $isMandatory = trim($param->IsMandatory)=='false' ? 0 : 1;
            $params[$paramName]['require'] = (bool) $isMandatory;

            if($params[$paramName]['sql']){
                $connection = $this->getDoctrine()->getManager()->getConnection();
                $stmt = $connection->prepare($params[$paramName]['sql']);
                try{
                    $stmt->execute();
                 }catch(\Exception  $e){
                    $this->addFlash('error', sprintf('<b>Error</b>: %s', $e->getMessage()) );
                 }
                $params[$paramName]['results'] = $stmt->fetchAll();
            }else if(strtolower($params[$paramName]['type']) == 'date'){
                if(in_array(strtolower($paramName) ,array('from', 'fromdate'))){
                    $fromdate = filter_input(INPUT_GET,'fromdate');
                    $startDate = (!empty($fromdate)) ? $fromdate : date("Y-m-d",strtotime(self::START_DATE));
                    $params[$paramName]['value'] = (isset($sessionParam[$paramName])) ? $sessionParam[$paramName] : $startDate;
                }else{
                    $params[$paramName]['value'] = (isset($sessionParam[$paramName])) ? $sessionParam[$paramName] :  date("Y-m-d",time());
                }
            }else{
                $params[$paramName]['value'] =
                        (isset($sessionParam[$paramName])) ? $sessionParam[$paramName] : "";
            }
        }

        return $params;
    }

    /**
     * @param $xmlFile
     * @return array
     */
    protected function _getReportConfigFields($xmlFile)
    {
        //first reserved column
        $fields = array(null);
        $reportFields = $xmlFile->Columns;
        foreach($reportFields->ReportField as $field){
            $arrFld = array();
            $arrFld['db'] = trim($field->FieldDataSource);
            $arrFld['name'] = trim($field->FieldHeader);
            if(!isset($arrFld['name']) || !$arrFld['name']){
                $arrFld['name'] = $arrFld['db'];
            }
            $arrFld['headKey'] = trim($field->FieldHeaderKey);
            $arrFld['width'] = trim($field->FieldWidth);
            if(!isset($arrFld['width']) || !$arrFld['width']){
                $arrFld['width'] = '80';
            }
            $arrFld['type'] = $this->_getFieldType(trim($field->FieldType));
            if($arrFld['type'] == self::IMGLINK){
                $arrFld['navMark'] = trim($field->NavigationMark);
                $arrFld['name'] = '';
            }
            $arrFld['format'] = trim($field->DisplayFormat);
            // default format for float type
            if(($arrFld['type'] == self::FLOAT) && !strlen($arrFld['format'])){
                $arrFld['format'] = '0.00';
            }
            // default format for integer type
            if(($arrFld['type'] == self::INTEGER) && !strlen($arrFld['format'])){
                $arrFld['format'] = '%d';
            }
            // default format for datetime
            if(($arrFld['type'] == self::DATE) && !strlen($arrFld['format'])){
                $arrFld['format'] = 'Y-m-d';
            }
            $displayInFilter = trim($field->DisplayInFilter)=='false' ? 0 : 1;
            $arrFld['nofilter'] = !(bool)$displayInFilter;
            $arrFld['filterBy'] = trim($field->FieldFilterBy);
            $arrFld['sortExp'] = trim($field->FieldSortExpression);
            $arrFld['hideIdent'] = trim($field->HideIdentical)=='true' ? 1 : 0;
            $arrFld['extIncl'] = trim($field->ExtraInclude);
            $arrFld['groupBy'] = (array)$field->GroupBy;
            if(count($arrFld['groupBy'])){
                global $nid;
                $groupByDesc = $arrFld['groupBy']['GroupExpression'] .":". $arrFld['groupBy']['Aggregate'] . "(".$arrFld['name'].") </a>";
                $groupBySel = ( !empty($this->_session->get('groupBy')) && ($this->_session->get('groupBy') == count($fields)) ) ? 'selected selected="selected"' : '';
                $arrFld['groupByLink'] =
                        ' <option '.$groupBySel.' value="'.SFMODULE_PATH.$this->get('request')->getPathInfo()
                        .'?nid='.$nid.'&id='.$this->_session->get('reportId').'&inst='.number_format(microtime(true)*1000,0,'.','')
                        .'&groupby='.count($fields).'">'.$groupByDesc.'</option>';

            }
            $arrFld['isTotal'] = trim($field->IsTotal)=='true' ? 1 : 0;

            $fields[] = $arrFld;
        }

        return $fields;
    }
    
    /**
     * @Route("/reports/listxml")
     */
    public function listXmlAction(Request $request)
    {

        global $Id,$nid,$Config,$Navigation,$User,$Error,$_menu,$_toolmenu;
 
        $listHelper = new ListHelper();
        
        $xmlFiles = $listHelper->listStart();
        $xmlFiles .= $listHelper->listRow(0);
        $xmlFiles .= $listHelper->listHead('Group', 175);
        $xmlFiles .= $listHelper->listHead('', 23);
        $xmlFiles .= $listHelper->listHead('Description (order: '
                . '<a href="'.SFMODULE_PATH.$request->getPathInfo().'?nid='.$nid.'&inst='.number_format(microtime(true)*1000,0,'.','').'&ord=dir">dir</a>, '
                . '<a href="'.SFMODULE_PATH.$request->getPathInfo().'?nid='.$nid.'&inst='.number_format(microtime(true)*1000,0,'.','').'&ord=id">id</a>, '
                . '<a href="'.SFMODULE_PATH.$request->getPathInfo().'?nid='.$nid.'&inst='.number_format(microtime(true)*1000,0,'.','').'&ord=name">name</a>, '
                . '<a href="'.SFMODULE_PATH.$request->getPathInfo().'?nid='.$nid.'&inst='.number_format(microtime(true)*1000,0,'.','').'">desc.</a>)');
        $xmlFiles .= $listHelper->listHead('Name', 175);
        
        $listRowNr = 0;
        
        // sorting 'ord' param included in array('dir','id',null)
        $listSortOrd = filter_input(INPUT_GET,'ord');
        list($groupSorted, $entitySorted) = $this->_getSortedList($listSortOrd);

        $xmlFiles .= $listHelper->listRow($listRowNr++);
        foreach($groupSorted as $gidx => $xmlGroup){

            $xmlFiles .= $listHelper->listField('', 'height=6 colspan=4');
            $xmlFiles .= $listHelper->listRow($listRowNr++);
            $xmlFiles .= $listHelper->listField($xmlGroup) ;
  
            $nameRow = true;
            foreach($entitySorted[$gidx] as $xmlEnt){
                if(!$nameRow){
                    $xmlFiles .= $listHelper->listField();
                }
                $xmlFiles .= $listHelper->listFieldIcon ('report.gif', 'report', $xmlEnt->Id) ;
                $xmlFiles .= $listHelper->listField($xmlEnt->Description.' ('.$xmlEnt->Id.')') ;
                $xmlFiles .= $listHelper->listField($xmlEnt->Name) ;
                $xmlFiles .= $listHelper->listRow($listRowNr++);
                $nameRow = false;
            }
  
        }
        
        if (!count($entitySorted)) {
            $xmlFiles .= $listHelper->listRow($listRowNr++);
            $xmlFiles .= $listHelper->listField('No Reports', 'colspan=3');
        }
        $xmlFiles .= $listHelper->listEnd();

        
        return $this->render('::reports/listxml.html.smarty.tpl', array(
            'list_xml'    => $xmlFiles,
            'Id'          => $Id,
            'nid'         => $nid,
            'Config'      => $Config,
            'Navigation'  => $Navigation,
            'User'        => $User,
            'Error'       => $Error,
            'htmlHeader'  => sprintf($Config["HTMLHeader"], $Navigation["Header"], $User["Loginname"], $User['CompanyName']),
            'menu'        => $_menu,
            'toolmenu'    => $_toolmenu,
            'hasToolMenu' => strlen($_toolmenu) > 0
         ));
        
    }

    /**
     * @Route("/reports/xmlvalue")
     */
    public function xmlValueAction(Request $request)
    {
        global $Id,$nid,$Config,$Navigation,$User,$Error,$_menu,$_toolmenu;

        $this->_session = $request->getSession();
        $reportId = $request->get('id');
        $sessionRepId = $this->_session->get('reportId');
        if(!$sessionRepId || ($sessionRepId != $reportId) ){
            $this->_session->set('reportId', $reportId);
            $sessionRepId = $this->_session->get('reportId');
            // clear for new report id
            $this->_session->remove('offset');
            $this->_session->remove('sort');
            $this->_session->remove('sortView');
            $this->_session->remove('filters');
            $this->_session->remove('groupBy');
            $this->_session->remove('totalsRow');
            if(!$this->_session->get('navId')){
                $this->_session->remove('paramSet');
            }
        }
        $this->_session->remove('navId');

        //grouping parameter
        $sessionGroupBy = $request->get('groupby');
        if($sessionGroupBy){
            $this->_session->set('groupBy', $sessionGroupBy);
        }else{
            $this->_session->remove('groupBy');
        }

        // menu option highlight
        $repository = $this->getDoctrine()->getRepository('AppBundle:Navigation');
        if(!$nid || $nid==2244){
            $navItem = $repository->findOneByFunction('symfony-xml-value');
            if(!empty($navItem)){
                    $nid = $navItem->getId();
            }
        }
        $activeNid = $repository->find($nid)->getParentId();

        // read xml report configuration file
        $xmlFile = null;
        $fileList = glob(self::XML_CONFIG_PATH.'{*.xml}', GLOB_BRACE);
        foreach($fileList as $file) {
            $row = @simplexml_load_file($file);
            if($row){
                 $repId = (int)$row->Id;
                 if($repId==$sessionRepId){
                     $xmlFile = $row;
                     break;
                 }
            }
         }
         
         if(!$xmlFile){
            $logHelper = new LogHelper();
            global $User;
            $logHelper->logPrintf(logFATAL, sprintf("couldn't find XML file with %s or XML parser error, IP %s, uid %s",
                'id:'.trim($this->_session->get('reportId')),
                $_SERVER["REMOTE_ADDR"],
                $User['Id']));
        }

        $header = $xmlFile->Name;
        $this->_sqlQuery = trim($xmlFile->SqlStatment);
		
        $this->_sqlQuery = str_replace('[userId]',$User['Id'],$this->_sqlQuery);

        
        // split form filterparams and filterform
        $post =(array)filter_input_array(INPUT_POST);
        $sqlParamsPost = array();
        if(isset($post['filterParams'])){
            $sqlParamsPost = (array)json_decode($post['filterParams']);
            unset($post['filterParams']);
        }
        $filters = $this->_getFilterParams($post);

        // get configuration file fields parameters
        $fields = $this->_getReportConfigFields($xmlFile);
        $xmlParameters = (array)$xmlFile->Parameters;
        $params = array();
        if(count($xmlParameters) && !empty($xmlParameters['ReportParameter'])){
            $params = $this->_getReportSqlParams($xmlParameters['ReportParameter'], $sqlParamsPost);
        }

        // set sorting column
        // sorting param
        $listSort = filter_input(INPUT_GET,'sort');
        $sessionSort = isset($listSort) ? $listSort : $this->_session->get('sort');
        $this->_session->set('sort', $sessionSort);

       $listHelper = new ListHelper();
       $listField = $listHelper->listFilterBar($fields,$filters);

       if(!$sessionSort = $this->_session->get('sort')){
           $sortCol = trim($xmlFile->InitialSortField);
           $this->_session->set('sort',$sortCol);
           if(strstr($sortCol,',')){
               $sortArr = explode(',',$sortCol);
               $sortCol = $sortArr[0];
               if(strstr($sortCol,' ')){
                   $pieces = explode(' ',$sortCol);
                   $sortCol = reset($pieces);
               }
           }
           $sessionSort = $listHelper->arrayGetIndex($listField['listField'], 'db', $sortCol);
       }
       if(strstr($sessionSort,',') || !is_numeric($sessionSort)){
           $pieces = explode(',',$sessionSort);
           $sortCol = reset($pieces);
           if(strstr($sortCol,' ')){
               $pieces = explode(' ',$sessionSort);
               $sortCol = reset($pieces);
           }
           $dbs = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'db','name');
           $imgCols = array_filter($this->_ifImgType($fields));
           $sortKey = array_search($sortCol,array_flip($imgCols));
           $sortKey = array_search($sortKey,$dbs);
           $sessionSort = substr($sortKey, strpos($sortKey, "_") + 1);
       }
       $this->_session->set('sortView', $sessionSort);

       // generate sql and get report items
        $items = $this->_getReportItems($fields, $params, $filters);
        $totals = array();
        //if grouping is on then don't calculate totals
        if(count($items) && empty($this->_session->get('groupBy'))){
            $totals = $this->_getReportItemsTotals($fields);
        }

        // get report action
        $action = $this->_getReportAction((array) $xmlFile->ReportAction,$items,$post);
        $flashMsg = $this->get('session')->getFlashBag()->get('notice');
		$flashErrors = array();
        if(self::DISPLAY_ERRORS){
			$flashErrors = $this->get('session')->getFlashBag()->get('error');
		}

       $form = $this->_getReportParamsForm($params);
       $sessionSort = $this->_session->get('sortView');

       //styles
       $styles = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'name','width');
       //format
       $format = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'db','format');
       //column img type
       $imgFields = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'db','navMark');
       //column hide identical
       $hideIdents = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'db','hideIdent');
       //headers for view
       $headers = $this->get('app.utils.arrays')->arrayKeyValues($fields,'name');
       //headers for data
       $headersDb = $this->get('app.utils.arrays')->arrayKeyValues($fields,'db');

       //pager
       $emptyInfo = (!$this->_totalRecords) ? trim($xmlFile->NoRowsLabel) : '';
       $total_pages  = ceil($this->_totalRecords / self::LINES);
       $current_page = 1;
       if ($this->_session->get('offset') > 0) {
          $current_page = ($this->_session->get('offset') + self::LINES) / self::LINES;
       }
       if($current_page > $total_pages){
           if(!$total_pages){
               $total_pages = 1;
           }
           $current_page = $total_pages;
       }
       
        // get additional header
        $xmlAddHeader = (array)$xmlFile->AdditionalHeader;
        $additionalHeader = array();
        if(count($xmlAddHeader)){
            $sqlHeaderQuery = $this->_setSqlQueryParameters($xmlAddHeader['Query'],$params);
            $connection = $this->getDoctrine()->getManager()->getConnection();
            $addHeader = $connection->prepare($sqlHeaderQuery);
            $addHeader->execute();
            $addHeaderResult = reset($addHeader->fetchAll());

            $addHeadersFields = $this->_getReportAdditionalHeader($xmlAddHeader);
            $paramAdHeder = array();
            foreach($params as $paramArr){
                $paramAdHeder[$paramArr['desc']] = $paramArr['hide'];
            }

            foreach($addHeadersFields as $field){
                //if value of the param is not empty and corresponding param filter is not
                if(/* !empty($addHeaderResult[$field['DataSource']]) && */ !$paramAdHeder[$field['DisplayName']]){
                    $additionalHeader[$field['DisplayName']] = $addHeaderResult[$field['DataSource']];
                }
            }
        }

        if($request->isXmlHttpRequest()){
            $smartyTpl = '::reports/xmlvalajax.html.smarty.tpl';
        }else{
            $smartyTpl = '::reports/xmlvalues.html.smarty.tpl';
        }

       //Assigning values to template engine and preparing HTML output
       return $this->render($smartyTpl, array(
            'Id'          => $Id,
            'nid'         => $nid,
            'activeNid'   => $activeNid,
            'Config'      => $Config,
            'Navigation'  => $Navigation,
            'User'        => $User,
            'Error'       => $Error,
            'htmlHeader'  => $header,
            'menu'        => $_menu,
            'toolmenu'    => $_toolmenu,
            'hasToolMenu' => strlen($_toolmenu) > 0,

            //params fo filter
            'listField'      => $listField['listField'],
            'filterValues'   => $listField['filterValues'],
            'addHeader'      => $additionalHeader,
            'fid'            => $listField['fid'],
            'n'              => $listField['n'],

            //params for list
            'listItems'      => $items,
            'totals'         => $totals,
            'headers'        => $headers,
            'headersDb'      => $headersDb,
            'imgFields'      => $imgFields,
            'hideIdents'     => $hideIdents,
            'styles'         => $styles,
            'format'         => $format,
            'sort'           => $sessionSort,
            'emptyInfo'      => $emptyInfo,
            'form'           => $form,

           //action params
            'action'        => $action,
            'notices'       => $flashMsg,
            'errors'        => $flashErrors,

             //params for paging
             'overall_pages'  => $total_pages,
             'current_page'   => $current_page,
             'lines_per_page' => self::LINES
           
        ));

       
    }
    
    protected function _ifImgType($fields)
    {
        $retr = array();
        foreach($fields as $field){
            if($field['type'] != self::IMGLINK){
                $retr[$field['db']] = $field['name'];
            }
        }

        return $retr;
    }

    protected function _ifFloatType($fields)
    {
        $retr = array();
        foreach($fields as $field){
            if($field['type'] == self::FLOAT){
                $retr[$field['name']] = $field['format'];
            }
        }

        return $retr;
    }

    /**
     * @Route("/reports/xmlexcel")
     */
    public function xmlExcelAction(Request $request)
    {
        global $User;

        $this->_session = $request->getSession();
        $sessionRepId = $this->_session->get('reportId');
      
         //grouping parameter
        $sessionGroupBy = $request->get('groupby');
        if($sessionGroupBy){
            $this->_session->set('groupBy', $sessionGroupBy);
        }else{
            $this->_session->remove('groupBy');
        }
        // read xml report configuration file
        $xmlFile = null;
        $fileList = glob(self::XML_CONFIG_PATH.'{*.xml}', GLOB_BRACE);
        foreach($fileList as $file) {
            $row = @simplexml_load_file($file);
            if($row){
                 $repId = (int)$row->Id;
                 if($repId==$sessionRepId){
                     $xmlFile = $row;
                     break;
                 }
            }
         }
         
         if(!$xmlFile){
            $logHelper = new LogHelper();
            $logHelper->logPrintf(logFATAL, sprintf("couldn't find XML file with %s or XML parser error, IP %s, uid %s",
                'id:'.trim($this->_session->get('reportId')),
                $_SERVER["REMOTE_ADDR"],
                $User['Id']));
        }

        $header = $xmlFile->Name;
        $this->_sqlQuery = trim($xmlFile->SqlStatment);
        $this->_sqlQuery = str_replace('[userId]',$User['Id'],$this->_sqlQuery);
        $filters = $this->_session->get('filters');

        // get configuration file fields parameters
        $fields = $this->_getReportConfigFields($xmlFile);
        $xmlParameters = (array)$xmlFile->Parameters;
        $params = array();
        if(count($xmlParameters) && !empty($xmlParameters['ReportParameter'])){
            $sqlParams = $this->_session->get('paramSet');
            $params = $this->_getReportSqlParams($xmlParameters['ReportParameter'],$sqlParams);
        }

        $items = $this->_getReportItems($fields, $params, $filters, true);
        $headers = array_values(array_filter($this->get('app.utils.arrays')->arrayKeyValues($fields,'name')));
        $styles = $this->get('app.utils.arrays')->arrayAssocKeyValues($fields,'name','width');
        $imgCols = array_filter($this->_ifImgType($fields));
        $floatCols = $this->_ifFloatType($fields);

		// ask the service for a Excel5
       $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
       $creator = $User['FirstName']." ".$User['LastName']." (".$User['Loginname'].")";
       $phpExcelObject->getProperties()->setCreator($creator)
           ->setTitle("Office 2005 XLSX ".$header)
           ->setSubject("Office 2005 XLSX ".$header)
           ->setDescription("Office 2005 XLSX raport xml export")
           ->setKeywords("office 2005 openxml php");
       $phpExcelObject->setActiveSheetIndex(0);
       $sheet = $phpExcelObject->getActiveSheet();

        // add report title
        $sheet->setCellValue('A1', $header);
        $sheet->getStyle("A1:A1")->getFont()->setSize(24);
        $sheet->setTitle((string)$header);
       
       // set columns width
       $idx = 0;
       foreach($styles as $key => $value){
           if($key[0]=='_') continue;
           $cell = chr(ord('A') + $idx++);
           $sheet->getColumnDimension($cell)->setWidth($value*0.19);
           if(isset($floatCols[substr($key,0,strpos($key,'_'))])){
                $sheet->getStyle($cell)->getNumberFormat()->setFormatCode('#,##'.$floatCols[substr($key,0,strpos($key,'_'))]);
           }
        }

        // add parameters with 'hide from display' == false
        $paramSet = $this->_session->get('paramSet');
        foreach($paramSet as $key => $param){
            if((isset($params[$key]) && $params[$key]['hide'] === true) || ($key == 'id')){
                continue;
            }
            $cell = 'A' . ($sheet->getHighestRow() + 1);
            $value = $params[$key]['desc'].': '.$param;
            $sheet->setCellValue($cell, $value);
        }
       
        // add additional header
        $xmlAddHeader = (array)$xmlFile->AdditionalHeader;
        if(count($xmlAddHeader)){
            $sqlHeaderQuery = $this->_setSqlQueryParameters($xmlAddHeader['Query'],$params);
            $connection = $this->getDoctrine()->getManager()->getConnection();
            $addHeader = $connection->prepare($sqlHeaderQuery);
            try{
               $addHeader->execute();
            }catch(\Exception  $e){
                echo $e->getMessage();
                exit;
            }
            $addHeaderResult = reset($addHeader->fetchAll());
            $addHeadersFields = $this->_getReportAdditionalHeader($xmlAddHeader);

             for($i=0;$i<count($addHeadersFields);$i++){
                $cell = 'A' . ($sheet->getHighestRow() + 1);
                $value = $addHeadersFields[$i]['DisplayName'].': '.$addHeaderResult[$addHeadersFields[$i]['DataSource']];
                $sheet->setCellValue($cell, $value);
            }
        }

        // generated at...
        $cell = 'A' . ($sheet->getHighestRow() + 1);
        $value = "Generated at: ".date("Y-m-d H:i:s",time()).' by: '.$creator;
        $sheet->setCellValue($cell, $value);

       // add header line
       $cell = 'A' . ($sheet->getHighestRow() + 2);
       $sheet->fromArray($headers, NULL, $cell);
       $cellsToFill = $cell.':'.chr(ord('A') + count($headers)-1).$sheet->getHighestRow();
       $sheet->getStyle($cellsToFill)->getFill()->applyFromArray(array(
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('rgb' => 'CCCCCC')
        ));
       $sheet->freezePane('A'.($sheet->getHighestRow() + 1));
    
       // add data lines
       $rows = array();
       foreach($items as $key => $row){
           $row = $this->get('app.utils.arrays')->arrayValuesByKey($row,array_keys($imgCols));
           $rows[$key] = str_replace(' 00:00:00', '', $row);
        }
	   $cell = 'A' . ($sheet->getHighestRow() + 1);
	   $sheet->fromArray($rows, NULL, $cell);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'report.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;     

    }

}