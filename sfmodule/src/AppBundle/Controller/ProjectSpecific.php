<?php

	private function productallDetailsBySeasonActionPostProcessor(&$productArray)
	{
		// Restructuring product information array
		foreach ($productArray as $key => $prod) {
			unset($rp) ; 

			$rp['productNumber'] = $key ;	
			foreach ($prod['name'] as $nkey => $name) {
				$rp['productDescriptions'][] = array(
					'productLanguage' 		=> $nkey,
					'shortDesc' 	=> $name,
					'longDesc'	 	=> $prod['description'][$nkey]
				);
			}
			unset($prod['categories']) ; 		// move after foreach loop if categories wanted. in API
			unset($prod['categoriesdesc']) ; 	// move after foreach loop if categories wanted. in API
			foreach ($prod['categories'] as $cankey => $caname) {
				$rp['categories'][] = array(
					'categoryLanguage' 		=> $cankey,
					'categoryNames' 		=> $caname,
					'categoryDescriptions'	=> isset($prod['categoriesdesc'][$cankey])? $prod['categoriesdesc'][$cankey] : ''
				);
			}
			foreach ($prod['colors'] as $ckey => $color) {
				unset($rc) ;unset($rcs) ;
				$rc['colorNumber'] = $ckey ;	
				foreach ( $color['colorName'] as $cnkey => $cname) {
					$rc['colorDescriptions'][] = array(
						'colorLanguage' 	=> $cnkey,
						'colorName' 		=> $cname,
						'gender'			=> isset($color['gender'][$cnkey])? $color['gender'][$cnkey] : 'None',
						'OnWebShop'			=> $color['webshop'][$cnkey] 
					);
				}
				unset($color['colorName']) ;
				unset($color['gender']) ;

				//				$rc['webshop'][] =  ;
				unset($color['webshop']) ;
				foreach ($color['sizes'] as $skey => $size) {
					unset($rs) ; 
					$rs['sizeName'] = $skey ;	
					$rcs['sizes'][] = array_merge($rs, $size) ;
				}
				unset($color['sizes']) ;
				$rp['colors'][] = array_merge($rc, $color, $rcs) ; // $rc ;
			}
			$NewproductArray['products'][] = $rp ;
		}
		$productArray = $NewproductArray ;
	}
?>
