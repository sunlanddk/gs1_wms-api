<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Order;
use AppBundle\Exception\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Range;
use AppBundle\Controller\ApiControllerProjectSpecific;

/**
 * Order controller class
 * @author Mariusz Angielski <mariusz.angielski@jcommerce.pl>
 * 
 * @Route("/api")
 */
class ApiController extends Controller
{   

    /**
     * 
     *
     * @Route("/pickorder/response/download", name="download_pickorder_response")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function pickorderResponseDownload(Request $request)
    {
		global $Config, $Id, $Record, $Navigation, $User, $test;
		
		set_time_limit(0);
		
        $postData = json_decode($request->getContent());
        $warehouse = $this->get('app.warehouse.handler');
        $import = $this->get('app.import.handler');
//        $User = $import->getUser();
        $options = $this->container->getParameter('pot_export');
        $libpath = $options['libpath'];
        
        require_once $libpath.'lib/config.inc';
        if(isset($postData->file)){        
            $files[] = $postData->file;
        }
        else{
            // $_type = hvilken fil den skal lede efter
            // $_folder = hvilken folder skal der kigges i
            // $_deleteFile = Skal filen slettes på serveren? Hvis den ikke bliver slettet vil den prøve at importere igen.
            // $_extension = hvilken fil extension er der tale om
            // downloadSpecificFileToServer($_type, $_folder,$_deleteFile, $_extension)
            $files = $warehouse->downloadSpecificFileToServer('INT__', 'prod/outbox', true, 'TXT');
        }



        if(is_array($files) === false){
           return new JsonResponse(['error' => 0, 'response' => 'NO files on server', 'result' => $files]);
        }
		require_once __DIR__.'\..\..\..\..\lib\config.inc' ;
		require_once __DIR__.'\..\..\..\..\lib\db.inc' ;
		require_once __DIR__.'\..\..\..\..\lib\file.inc' ;
		require_once __DIR__.'\..\..\..\..\lib\table.inc' ;
        // Her kan du processere filerne som er downloaded.
        // De ligger i develstore/download/
        foreach ($files as $fkey => $filename) {

            $xmlObject = $warehouse->processCsv($filename);
				//return new JsonResponse(['error' => 0, 'response' => 'An error occured, when reading the xml file', 'result' => $array]); 
				//print_r($xmlObject) ; die(); 
           if(is_object($xmlObject) !== true ){
				// move parsed file to warehouse/download/parsed
				$filePath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
				$fileContents = file_get_contents($filePath);
				$parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/parsedError/'.$filename;
				if($warehouse->saveCopyFile($parsedPath, $fileContents) !== true){
					return 'error when saving file.'.$parsedPath . ' from ' . $filename;
				}
				//remove temporary file from warehouse/download
				$parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
				unlink($parsedPath);
				$files[$fkey] .= ': ' . $xmlObject ;
				continue ; // Log error differently than above?
                return new JsonResponse(['error' => 0, 'response' => 'An error occured, when reading the xml file', 'result' => $xmlObject]);            
            }

			
            $invoiceIds = $import->pickAndInvoice($xmlObject);
            // test
            //$invoiceIds = array('invoiceIds' => array('6444' => '6444', '6443' => '6443'), 'shipmentInvoices' => array('5552' => array(6443,6444)));

            
            $pdfs = array();
            if(empty($invoiceIds['invoiceIds']) === true){
				// move parsed file to warehouse/download/parsed
				$filePath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
				$fileContents = file_get_contents($filePath);
				$parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/parsedError/'.$filename;
				if($warehouse->saveCopyFile($parsedPath, $fileContents) !== true){
					return 'error when saving file.'.$parsedPath . ' from ' . $filename;
				}
				//remove temporary file from warehouse/download
				$parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
				unlink($parsedPath);
				
				continue ; // Log error?
                return new JsonResponse(['error' => 1, 'response' => 'no invoices', 'errors' => $invoiceIds['errors']]);
            }
            foreach ($invoiceIds['invoiceIds'] as $invoice) {
                foreach ($invoice as $key => $id) {
                    //$completelyDone = $import->getCompletelyDone($key);
                    $completelyDone['PartDelivery'] = 0;
                    $_invoicedraft=$import->setOrderDone($key, $completelyDone);        
					if ($_invoicedraft) continue ;
                    
                    // Create invoice pdf
					$_pdffile=$this->generatePdfFile($key) ; // From here on global $Record is set according invoice/loadFunction
					
					if (!($Config['Instance'] == 'Test')) {
						require_once __DIR__.'\..\..\..\..\module\invoice\invoiceExportToWeb.php' ;
						sendInvoiceDataToWebshop($key);
					}

					$_returnpdf = $Config['photoLib'] . 'returnnote.pdf' ;

					//preparing pdf attachment for the mail
					if ($Record['OrderTypeId']==10) {
					   $_attachmentArray = array(
							array(
								'path'     => $_pdffile,
								'mimeType' => 'application/pdf',
								'name'     => sprintf('Invoice #%s.pdf', $Record['Number'])
							),
							array(
								'path'     => $_returnpdf,
								'mimeType' => 'application/pdf',
								'name'     => sprintf('ReturnNote.pdf', $Record['Number'])
							)
						);
					} else {
					   $_attachmentArray = array(
							array(
								'path'     => $_pdffile,
								'mimeType' => 'application/pdf',
								'name'     => sprintf('Invoice #%s.pdf', $Record['Number'])
							)
						);
					}
					// Create and send email
					require_once __DIR__.'\..\..\..\..\lib\mail.inc' ;
					if ($Record['OrderTypeId']==10) {
						$_mark="webinvoicecreated" ;
						$_email= $Record['Email'] ;
					} else {
						$_mark="b2binvoicecreated" ;
						$_email= $Record['Mail'] ;
					}
					$_bodytxt = tableGetFieldWhere('maillayout','Body_default',sprintf('Mark="%s"', $_mark))  ;
					$_param['body'] = nl2br($_bodytxt) . '<br>' ;
					
					if ($Config['Instance'] == 'Test')
						sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false,  'kc@passon-solutions.com', false, $_attachmentArray, sprintf(' to %s Invoice #%s', $Record['CompanyName'], $Record['Number']));
					else
						sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false, $_email . ', sales@fub.dk, kc@passon-solutions.com', false, $_attachmentArray, sprintf(' to %s Invoice #%s', $Record['CompanyName'], $Record['Number']));

               }
            }

            // move parsed file to warehouse/download/parsed
            $filePath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
            $fileContents = file_get_contents($filePath);
            $parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/parsed/'.$filename;
            if($warehouse->saveCopyFile($parsedPath, $fileContents) !== true){
                return 'error when saving file.'.$parsedPath . ' from ' . $filename;
            }

            //remove temporary file from warehouse/download
            $parsedPath = _LEGACY_LIB.'/develstore/warehouse/download/'.$filename;
            unlink($parsedPath);
         }
        return new JsonResponse(['success' => 1, 'response' => $files]);
    }


     protected function generatePdfFile($_invoiceid) {
        global $User, $Id, $Config, $Record, $ProformaInvoice, $ProformaCustoms, $_l_margin, $top_margin, $bottom_margin, $Txt;

//    global $Id, $User, $Record, $Company, $CompanyMy, $Now, $CompanyDelivery, $Line, $Now, $ProformaInvoice, $ProformaCustoms, $LOT, $_l_margin, $top_margin, $bottom_margin ;
//    global $CompanyBank, $Shipment, $Page, $Total, $Lang, $FormFont, $Txt ;
		
		
        $Id   = $_invoiceid;
		require_once __DIR__.'\..\..\..\..\module\invoice\loadFunction.inc' ;
		$error_msg = printinvoiceload() ;
		if ($error_msg) return 'Load Message: ' . $error_msg ;
		
        $Config['savePdfToFile'] = true;
		$ProformaInvoice=0; $ProformaCustoms=0;$Record['FromCompanyId']=787; $User['CompanyId']=787 ;
		$Record['Ready'] = 1 ;
//		require_once __DIR__.'\..\..\..\..\module\invoice\printinvoice.inc' ;
		$Lang = 'en' ; // default language 
		$ShowRetailPrice = true ;
		require_once __DIR__.'\..\..\..\..\module\invoice\printinvoiceWH.inc' ;
		$error_msg = printinvoice() ;		
		if ($error_msg) return 'Message: ' . $error_msg ;

        $Config['savePdfToFile'] = false;

         return fileName('invoice', (int)$Id);
    }


    /**
     * 
     *
     * @Route("/upload/file", name="upload_file_server")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadFileToServer(Request $request)
    {
        $warehouse = $this->get('app.warehouse.handler');
        $aData = json_decode($request->getContent());
        
        $filePath = $aData->file; 
        $extension = $aData->extension;//'TXT';
        $folder = $aData->folder; //'prod/inbox';
        $upload = $warehouse->uploadFileToServer($filePath, $extension, $folder);
        if($upload === false){
            return new JsonResponse(['error' => 1, 'response' => $upload]);    
        }

        return new JsonResponse(['success' => 1, 'response' => $upload]);
    }

    /**
     * Saving new order incoming from Magento webshop to POT system
     *
     * @Route("/orders/new", name="magento_new_order")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function newOrderAction(Request $request)
    {
//        $aData = json_decode($request->request->all());
        //$aData = $request->request->all();

		$aData = (array)json_decode($request->getContent());
        foreach ($aData['Articles'] as $key => $value) {
            $aData['Articles'][$key] = (array)$value;
        }
        $oOrderHandler = $this->get('app.order.handler');
        $iOrderId = $oOrderHandler->newOrder($aData);

        return new JsonResponse(['orderId' => $iOrderId]);
    }

    /**
     * Return order details by magento order id
     *
     * @Route("/orders/get/{id}", name="magento_get_order")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getOrderAction(Request $request, $id)
    {
        $oOrderHandler = $this->get('app.order.handler');
        $aOrder = $oOrderHandler->getOrderByMagentoId($id);

        return new JsonResponse(['result' => 'success', 'data' => $aOrder]);
    }

    /**
     * Return a list of country / currency / carrier
     *
     * @Route("/list/{type}", name="get_pot_list")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request, $type)
    {
        $aList = $this->get('app.list.handler')->getList($type);
//        return new JsonResponse($aList);
        $serializer = $this->container->get('jms_serializer');
        $response = $serializer->serialize($aList, 'json');

        return new Response($response, Response::HTTP_OK, array('content-type' => 'application/json'));
    }

    /**
     * Return a product quantity
     *
     * @Route("/product/stock/{variant}", name="get_product_quantity")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function productQuantityAction(Request $request, $variant)
    {
        $manager = $this->get('app.product.manager');
        $mapper = $this->get('app.mapper.factory')->createVariantCodeMapper();

        $productMapped = $mapper->map($variant);
	
		
        // Turn Digital added this
        /*if ($productMapped === null) {
            if($variant == 'all'){
                $productArray = $manager->getAllProducts();
                foreach ($productArray as $key => $product) {


                    // Find available quantity: "on stock" - "in order"
                    $quantityArray = $manager->getProductQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);
                    if (false === isset($quantityArray[0])) {
                        $quantityArray[0]['quantity'] = 0;
                    }
                    if ($quantityArray[0]['quantity'] > 0) {
                        $order = $manager->getOrderQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);

                        if (true === isset($order[0]['quantity']) && $order[0]['quantity'] > 0) {
                            $quantityArray[0]['quantity'] -= $order[0]['quantity'];
                        }
                    } 

                    $response[$product['ean']] =  array('qty' => $quantityArray[0]['quantity']);
                    
                }

                // return new JsonResponse('test');
                return new JsonResponse($response);
            }
            else{
                return new JsonResponse(array('success' => false, 'message' => 'Product variant not found.'));
            }
        }*/
        // Turn Digital //
		
        if ($productMapped === null) {
             return new JsonResponse(array('success' => false, 'message' => 'Product variant not found.'));
        }
	

        $productMapped = $productMapped->toArray();
		
        $quantityArray = $manager->getProductQuantity($productMapped['article_id'], $productMapped['color_id'], $productMapped['size_id']);
		
		
		 if ($quantityArray == '') {
             return new JsonResponse(array('success' => false, 'message' => 'Product variant not found.'));
        }
		
        if (false === isset($quantityArray[0])) {
            $quantityArray[0]['quantity'] = 0;
            $quantityArray[0]['article_id'] = $productMapped['article_id'];
            $quantityArray[0]['color_id'] = $productMapped['color_id'];
            $quantityArray[0]['size_id'] = $productMapped['size_id'];
        }

        $quantityArray[0]['shops'] = null;

        if ($quantityArray[0]['quantity'] > 0) {
            $order = $manager->getOrderQuantity($productMapped['article_id'], $productMapped['color_id'], $productMapped['size_id']);

            if (true === isset($order[0]['quantity']) && $order[0]['quantity'] > 0) {
                $quantityArray[0]['quantity'] -= $order[0]['quantity'];
            }
        } else {
            $urls = $manager->getShopUrls($productMapped['article_id'], $productMapped['color_id']);

            if (isset($urls[0])) {
                $shops = array();

                foreach ($urls as $key => $shop) {
                    //$s['name'] = $shop['Name'];
                    $s['url'] = $shop['web'];
                    //$s['country'] = $shop['Country'];

                    array_push($shops, $s);
                }

                $quantityArray[0]['shops'] = $shops;
            }
        }

        $response = array(
//            'orderquantity' => $order[0]['quantity'],
            'quantity' => (int)$quantityArray[0]['quantity'],
            'shops' => $quantityArray[0]['shops']
        );

        return new JsonResponse($response);
    }

    /**
     * Return a product list 
     *
     * @Route("/product/list/{collectionmemberid}", name="get_product_all_quantity")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    // public function productallDetailsAction(Request $request, $variant)
    public function productallQuantityAction(Request $request, $collectionmemberid)
    {
        $manager = $this->get('app.product.manager');
        $mapper = $this->get('app.mapper.factory')->createVariantCodeMapper();

        if($collectionmemberid == 'all'){
            return new JsonResponse(array('success' => false, 'message' => 'To many products'));
            // return new JsonResponse($manager->getAllProducts());
        }
        else{

            $cmId = $collectionmemberid;
            $allProducts = $manager->getSingleProducts(null, $cmId);
            $count = 0;
            $productArray = array();
            foreach ($allProducts as $key => $product) {
                $count ++;
                $this->formatProductInfo($productArray, $product) ;
            }
            
            $PostProcess = new ApiControllerProjectSpecific();
            $productArray = $PostProcess->productallDetailsPostProcessor($productArray);
            // $this->productallDetailsPostProcessor($productArray) ;
            
            if(!empty($productArray)) {
                return new JsonResponse($productArray);
            } else {
                return new JsonResponse(array('success' => false, 'message' => 'No products found'));
            }
        }
    }

    /**
     * Return a product list by season id 
     *
     * @Route("/season/list/{seasonid}", name="get_product_all_by_season")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function productallDetailsBySeasonAction(Request $request, $seasonid)
    {
        $manager = $this->get('app.product.manager');
        $mapper = $this->get('app.mapper.factory')->createVariantCodeMapper();

        $allProducts = $manager->getProductsBySeason($seasonid);
        $count = 0;
        foreach ($allProducts as $key => $product) {
			$count ++;
			$this->formatProductInfo($productArray, $product) ;
		}
		
        $PostProcess = new ApiControllerProjectSpecific();
        $productArray = $PostProcess->productallDetailsPostProcessor($productArray);
		// $this->productallDetailsPostProcessor($productArray) ;
		
        if(!empty($productArray)) {
            return new JsonResponse($productArray);
        } else {
            return new JsonResponse(array('success' => false, 'message' => 'No products found'));
        }
    }
	
    private function formatProductInfo(&$productArray, $product)
    {
        $manager = $this->get('app.product.manager');
        $mapper = $this->get('app.mapper.factory')->createVariantCodeMapper();

		$productArray[$product['ArticleNumber']]['name'][$product['langCode']] = $product['langName']==''? $product['ArticleName'] : $product['langName'];
		
		// description
		$productArray[$product['ArticleNumber']]['description'][$product['langCode']] = $product['langDesc'];
		
		//Categories
		$CategoryNames = $manager->getCategoryName($product['ArticleNumber']);
		foreach ($CategoryNames as $value) {
			$catName = $value['lang_name'] ==''?$value['Name']:$value['lang_name'];
            if(isset($productArray[$product['ArticleNumber']]['categories'])){
                if (!in_array($catName, $productArray[$product['ArticleNumber']]['categories'][$value['langCode']])) {
                    $productArray[$product['ArticleNumber']]['categories'][$value['langCode']][] = $catName ;
                    $productArray[$product['ArticleNumber']]['categoriesdesc'][$value['langCode']][] = $value['lang_description'] ;
                }
            }
		}
		
		// Webshop culture
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['webshop'][$product['langCode']] = $product['onWebshop'];

		//Product Image
		//$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['image'] = 'http://'. $_SERVER['HTTP_HOST'] .  '/legacynew/image/thumbnails/' . $product['ArticleNumber'].'_'.$product['ColorNumber'].'.jpg';
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['image'] = $product['ArticleNumber'].'_'.$product['ColorNumber'].'.jpg';

		//Brand Name
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['brand'] = $product['brand_name'];

		//Season Id
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['seasonId'] = $product['season_id'];

		 //Collection Id
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['collectionId'] = $product['collection_id'];

		//Collection Name
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['collectionName'] = $product['collectionName'];

		 //Collection Member Id
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['collectionMemberId'] = $product['CollectionMemberId'];

		//Store code
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['storeCode'] = $product['store_code'];

		//Crosssell products
		if($product['WebFitGroup'] != '' || $product['WebFitGroup'] != NULL){
			$crosssellProducts = $manager->getCrosssellProducts(null, $product['WebFitGroup'], $product['CollectionMemberId']);
			$crossselArray = array();
			foreach ($crosssellProducts as $crosssellProduct) {
//				$crossselArray[$crosssellProduct['articleId'] . '_' . $crosssellProduct['colorNumber']] = $crosssellProduct['articleId'] . '_' . $crosssellProduct['colorNumber'];
				$crossselArray[] = $crosssellProduct['articleId'] . '_' . $crosssellProduct['colorNumber'];
			}
			$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['relatedProducts'] = $crossselArray;
		}

		//Store partner
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['storePartner'] = $product['store_partner'];

		//Country of manufacture
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['countryOfManufacture'] = $product['country_of_manufacture'];

		//Color name
		$colorNames = $manager->getColorName($product['ean']);
		foreach ($colorNames as $value) {
			$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['colorName'][$value['langCode']] = $value['description'] ==''?$product['color_name']:$value['description'];
		}

		// Color RGB
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['ColorRGB'] = array(
							'valueRed' 		=> (int)$product['ValueRed'],
							'valueGreen'	=> (int)$product['ValueGreen'],
							'valueBlue'		=> (int)$product['ValueBlue']
						);

		//Gender
		$genderNames = $manager->getGenderName((int)$product['genderId']);
		foreach ($genderNames as $value) {
			$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['gender'][$value['langCode']] = $value['Name'] ==''?'None':$value['Name'];
		}

		// Size information 
		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['sizes'][$product['SizeName']] = array(
			'sku' => $product['ean'],
			'weight'  => (string)1,
		);

		// Prices
		$priceSizeArray = $manager->getPriceSize((int)$product['CollectionMemberId'], (int)$product['ArticleSizeId']);
		foreach ($priceSizeArray as $key => $priceSize) {
			$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['sizes'][$product['SizeName']][$priceSize['CurrencyName']] = array (
				'Price' => $priceSize['RetailPrice'] ,
				'Campaign' => ($product['RetailDiscount']>0) ? ($priceSize['RetailPrice']*(100-$product['RetailDiscount'])/100) : "0.00"
			) ;
		}

		// Find available quantity: "on stock" - "in order"
		$quantityArray = $manager->getProductQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);
		if (false === isset($quantityArray[0])) {
			$quantityArray[0]['quantity'] = 0;
		}
		if ($quantityArray[0]['quantity'] > 0) {

			$order = $manager->getOrderQuantity($product['ArticleId'], $product['ArticleColorId'], $product['ArticleSizeId']);

			if (true === isset($order[0]['quantity']) && $order[0]['quantity'] > 0) {
				$quantityArray[0]['quantity'] -= $order[0]['quantity'];
			}
		} 

		$productArray[$product['ArticleNumber']]['colors'][$product['ColorNumber']]['sizes'][$product['SizeName']]['QTY'] =  (string)$quantityArray[0]['quantity'];
	}
}
