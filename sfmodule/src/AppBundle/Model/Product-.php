<?php

namespace AppBundle\Model;

/**
 * Class Product
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Model
 */
class Product
{
    /**
     * Method return string query for getting product using in POT system.
     *
     * @return string
     */
    public function getWebProductsQuery($languages, $currencies, $webshop, $season = null)
    {
        $query = "SELECT `cm`.`id` AS `ID`, a.id AS id_product,
					concat(`a`.`Number`,'_',co.Number) AS `PRODUCTID`,
					(SELECT acg.id as ID FROM articlecategory ac, articlecategorygroup acg where ac.articlecategorygroupid=acg.id and Category=substring(a.number,2,3))  as `CATID`,
					if((isnull(`a`.`DeliveryComment`) or (`a`.`DeliveryComment` = _utf8'')),`a`.`Description`,`a`.`DeliveryComment`) AS `DESCRIPTION`,
					`cmp`.`RetailPrice` AS `PRICE`,
					0 AS `STOCK`,0 AS `FRONTPAGE`,0 AS `NEW`,

					ac.id as ArticleColorId, co.Description as ColorName, sl.Name as SexName, `cmp`.`CampaignPrice` AS `CAMPAIGN_PRICE`, size.id as SIZE, vc.VariantCode, cm.WebFrontpage as front_page,
					cm.WebFitGroup, cm.WebCampaign, size.Name AS SIZE_NAME, se.id AS SEASON_ID, co.id as COLOR_ID, co.Number AS COLOR_NUMBER, `a`.`Number` AS PRODUCT_NUMBER,
					cmp.CurrencyId AS CurrencyId, cur.Name as CurrencyName, sl.SexId AS SexId,
					artlang.Description as article_title_lang, artlang.WebDescription as article_desc_lang, l.Culture AS article_lang, b.Name as brand_name, bl.Name as brand_lang_name, b.id AS brand_id
					FROM `collection` `c`
                        JOIN `collectionmember` `cm` ON `c`.`id` = `cm`.`CollectionId`
                        JOIN `collectionmemberprice` `cmp` ON `cmp`.`CollectionMemberId` = `cm`.`id`
                        JOIN `article` `a` ON `cm`.`ArticleId` = `a`.`Id`
                        JOIN `articlecolor` `ac` ON ac.id = cm.articlecolorid
                        JOIN `articlesize` `size` ON size.ArticleId = a.Id
                        JOIN `sex` `s` ON ac.sexid = s.id
                        JOIN `sex_lang` `sl` ON sl.SexId = s.id
                        JOIN `color` `co` ON co.id = ac.colorid
                        JOIN variantcode vc ON (vc.ArticleId = a.id AND vc.ArticleColorId = ac.id AND vc.ArticleSizeId = size.id)
                        JOIN season se ON c.SeasonId = se.id
                        JOIN currency cur ON cur.id = cmp.CurrencyId
                        LEFT JOIN article_lang artlang ON artlang.ArticleId = a.id
                        LEFT JOIN language l ON artlang.LanguageId = l.id
                        LEFT JOIN brand b ON c.brandid = b.id
                        LEFT JOIN brand_lang bl ON b.id = bl.brandId
                        
				    WHERE `c`.`Active` = 1 
                        AND `cm`.`notonwebshop` = 0
                        AND se.WebshopId = $webshop
                        AND `cm`.`Active` = 1 
                        AND `cmp`.`Active` = 1 
                        AND `cmp`.`CurrencyId` IN ($currencies) 
						AND `cmp`.`RetailPrice` > 0
                        AND sl.LanguageId IN ($languages)
                        AND vc.active = 1
                        AND `cm`.`Cancel` = 0 ";

        if (null !== $season) {
            $query .= ' AND se.id = ' . $season;
        }

        $query .= ' ORDER BY a.Id ';

        return $query;
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getProductInventoryQuantityQuery($articleId, $articleColorId, $articleSizeId, $webshop, $stockIds)
    {
        $ids = $this->transformArrayToIds($stockIds, 'pickstockid|fromstockid');

        return "SELECT  cm.articleid, cm.articlecolorid, i.articlesizeid, sum(quantity) as quantity
                FROM (Season se, Collection co, collectionmember cm)
                INNER JOIN item i ON i.articleid=cm.articleid and  i.articlecolorid=cm.articlecolorid and i.active=1
                INNER JOIN container c ON i.containerid=c.id and c.stockid in ($ids) and c.active=1
                INNER JOIN articlesize az ON az.id=i.articlesizeid
                WHERE se.WebshopId= $webshop and se.done=0 and se.active=1
                    and cm.articleid = $articleId and cm.articlecolorid = $articleColorId
                    and i.articlesizeid = $articleSizeId
                    and co.seasonid=se.id and co.active=1
                    and cm.collectionid=co.id and cm.notonwebshop=0 and cm.active=1
                    GROUP BY cm.articleid, cm.articlecolorid, i.articlesizeid
                 ";
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getProductOrderQuantityQuery($articleId, $articleColorId, $articleSizeId)
    {
        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 left join collectionlot cl ON cl.Active=1 and cl.id=ol.LotId and ol.LotId>0
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       and (isnull(cl.id) or (cl.id>0 and cl.presale=0 and (select count(id) from requisitionline rl where rl.Active=1 and rl.Done=0 and rl.LotId=cl.id)>0) )
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid"  ;
/*
        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid
                 ";
*/
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getAvailableUrlShops($articleId, $articleColorId, $webshop)
    {
        return "select ol.articlecolorid, c.Name, c.webshop, c.web, co.Name as Country 
                from (season se, orderline ol, `order` o, company c, country co) where se.WebshopId=$webshop and o.seasonid=se.id and  ol.orderid=o.id and o.companyid=c.id
         and o.active=1 and ol.active=1 and ol.articleid=$articleId and ol.articlecolorid=$articleColorId and co.id=c.countryid
         and c.active=1 and c.ListHidden=0 and c.webshop=1 group by c.id, ol.articlecolorid";
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getVariantOrdersQuery(array $variants)
    {
        $vids = '';

        foreach ($variants as $id) {
            $vids .= $id . ', ';
        }

        $vids = substr($vids, 0, -2);

        $query = "SELECT vc.VariantCode, ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
        FROM variantcode vc 
        JOIN orderline ol ON ol.ArticleId = vc.ArticleId AND ol.ArticleColorId = vc.ArticleColorId 
        JOIN `order` o ON ol.orderId = o.id and o.active=1 and o.done=0
        JOIN orderquantity oq ON oq.OrderLineId = ol.id and oq.articlesizeid=vc.articlesizeid
        WHERE ol.active=1 and ol.done=0 and oq.active=1 and vc.VariantCode IN ($vids) GROUP BY vc.VariantCode; ";

        return $query;


        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid
                 ";
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getVariantsQuery(array $variants, $webshop, $stockIds, $season = null) {

        $ids = $this->transformArrayToIds($stockIds, 'pickstockid|fromstockid');

        $vids = '';

        foreach ($variants as $id) {
            $vids .= $id . ', ';
        }

        $vids = substr($vids, 0, -2);

        $query = "SELECT vc.VariantCode, cm.articlecolorid, i.articlesizeid, sum(i.quantity) as quantity, mc.id_magento as colorMagento, ms.id_magento as sexMagento, col.id as colorid, msi.id_magento as sizeMagento, mb.id_magento as brandMagento FROM variantcode vc
                JOIN collectionmember cm ON cm.ArticleId = vc.ArticleId AND cm.ArticleColorId = vc.ArticleColorId
                JOIN collection c ON cm.CollectionId = c.id
                JOIN season se ON se.id = c.SeasonId
                JOIN articlecolor ac ON ac.id = vc.ArticleColorId
                JOIN articlesize asize ON asize.id = vc.ArticleSizeId
                JOIN color col ON col.id = ac.ColorId
                LEFT JOIN magento_brand mb ON mb.id_pot = c.brandId
                LEFT JOIN magento_color mc ON mc.id_pot = col.id
                LEFT JOIN magento_sex ms ON ms.id_pot = ac.sexId
                LEFT JOIN magento_size msi ON msi.id_pot = asize.id
                LEFT JOIN item i ON i.articleId = vc.ArticleId and i.articlecolorid = vc.ArticleColorId and i.articlesizeid = vc.ArticleSizeId
                LEFT JOIN container co ON i.containerid = co.id and co.stockid in ($ids) and co.active = 1
                WHERE se.WebshopId = $webshop and se.done = 0 and se.active = 1 and c.active = 1 and cm.notonwebshop = 0 and cm.active = 1
                and vc.VariantCode IN ($vids) GROUP BY vc.VariantCode ";

        if ($season) {
            $query .= " and se.id = $season ";
        }

        $query .= ";"; //end query

        return $query;
    }

    /**
     * Method return string query for getting products by season using in POT system.
     *
     * @return string
     */
    public function getProductsBySeasonQuery($seasonid)
    {
        $query = "  SELECT 
                    article.Number as ArticleNumber,
                FROM article
                ";


        return $query;
    }

    private function transformArrayToIds(array $stocks, $fieldName)
    {
        $ids = '';
        $names = explode('|', $fieldName);

        foreach ($stocks as $stock) {
            $ids .= $stock[$names[0]] . ', ';

            if (isset($names[1])) {
                $ids .= $stock[$names[1]] . ', ';
            }
        }

        return substr($ids, 0, -2);
    }
}
