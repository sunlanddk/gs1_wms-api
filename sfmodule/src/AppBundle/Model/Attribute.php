<?php

namespace AppBundle\Model;

class Attribute
{

    public function mapSize($idPot, $idMagento)
    {
        return "INSERT INTO magento_size(id_pot, id_magento) VALUES($idPot, $idMagento); ";
    }

    public function mapColor($idPot, $idMagento)
    {
        return "INSERT INTO magento_color(id_pot, id_magento) VALUES($idPot, $idMagento); ";
    }

    public function mapSex($idPot, $idMagento)
    {
        return "INSERT INTO magento_sex(id_pot, id_magento) VALUES($idPot, $idMagento); ";
    }

    public function mapBrand($idPot, $idMagento)
    {
        return "INSERT INTO magento_brand(id_pot, id_magento) VALUES($idPot, $idMagento); ";
    }

    public function clearSizeMap()
    {
        return "TRUNCATE TABLE magento_size";
    }

    public function clearColorMap()
    {
        return "TRUNCATE TABLE magento_color";
    }

    public function clearSexMap()
    {
        return "TRUNCATE TABLE magento_sex";
    }

    public function clearBrandMap()
    {
        return "TRUNCATE TABLE magento_brand";
    }

    public function getColorsByIds($idsColor, $langs)
    {
        $ids = '';

        if (is_array($idsColor)) {
            foreach ($idsColor as $id) {
                $ids .= $id['color_id'] . ', ';
            }

            $ids = substr($ids, 0, -2);
        }

        return " SELECT c.id, c.Description, cl.LanguageId, l.Culture as lang, cg.ValueRed as RED, cg.ValueGreen AS GREEN, cg.ValueBlue as BLUE, cl.Description as langDescription
             FROM color c JOIN colorgroup cg ON c.ColorGroupId = cg.id 
             LEFT JOIN color_lang cl ON cl.ColorId = c.id 
             LEFT JOIN language l ON l.id = cl.LanguageId
             WHERE c.id IN ($ids); ";
    }

    public function getSex($langs)
    {
        return "SELECT s.id, s.Name, sl.Name as langName, l.Culture as lang FROM sex s
                LEFT JOIN sex_lang sl ON sl.SexId = s.id 
                LEFT JOIN language l ON sl.LanguageId = l.id
                ;";
    }

    public function getBrandsById($idsBrand, $langs)
    {
        $ids = '';

        if (is_array($idsBrand)) {
            foreach ($idsBrand as $id) {
                $ids .= $id . ', ';
            }

            $ids = substr($ids, 0, -2);
        }

        return "SELECT b.Name as brand_name, b.id as brand_id, bl.Name as brand_lang_name, l.Culture as lang FROM brand b 
                LEFT JOIN brand_lang bl ON bl.BrandId = b.id 
                LEFT JOIN language l ON bl.LanguageId = l.id
                WHERE b.id IN ($ids);
                ";
    }
}
