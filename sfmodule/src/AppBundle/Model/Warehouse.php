<?php

namespace AppBundle\Model;

/**
 * Class Warehouse
 *
 * @author Alex Roland <aro@turndigital.dk>
 * @package AppBundle\Model
 */
class Warehouse
{
    /**
     * Method return string query for getting product using in POT system.
     *
     * @return string
     */
    public function getNavQuery()
    {
        $query = "SELECT * FROM navigation";
        return $query;
    }
}
