<?php

namespace AppBundle\Model;

/**
 * Class Shoporama
 *
 * @author Alex Roland <alexander.roland.a@gmail.com>
 * @package AppBundle\Model
 */
class Shoporama
{
    /**
     * Method return string query for getting product using in POT system.
     *
     * @return string
     */
    public function getNavQuery()
    {
        $query = "SELECT * FROM navigation";
        return $query;
    }
}
