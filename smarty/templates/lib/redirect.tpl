{if isset($mark)}
   {literal}
      <script type='text/javascript'>
         $(document).ready(function() {
            $("#ajax_redirect").click();
         });
      </script>
   {/literal}

   <div id="ajax_redirect" {navigation_on_click_mark mark=$mark} style="display: none;" />
{else}

   {literal}
      <script type='text/javascript'>
         $(document).ready(function() {
            var nid = $("#ajax_redirect #nid").val();
            var defaultNID =  $("#ajax_redirect #defaultNID").val();
            var indexId = $("#ajax_redirect #indexId").val();
            appLoadLegacy(true, nid, indexId,
               (defaultNID != null) ? "DefaultNID=" + defaultNID : ""
            );
         });
      </script>
   {/literal}

   <div id="ajax_redirect">
      <input type="hidden" id="nid" value="{$nid}"/>
      <input type="hidden" id="defaultNID" value="{$defaultNID}"/>
      <input type="hidden" id="indexId" value="{$indexId}"/>
   </div>
{/if}
