<div class="pagging">
	{*link to first page*}
	<a href="#" class="paggingArrow" {if $current_page != 1}{prepare_paging_link offset=0}{/if}>
		<img src="layout/new/images/firstPage.png" alt="Passon logo" />
	</a>
	
	{*link to previous page*}
	<a href="#" class="paggingArrow" {if $current_page != 1} {prepare_paging_link offset=($current_page-2)*$lines_per_page} {/if}>
		<img src="layout/new/images/left.png" onmouseover="$(this).attr('src', 'layout/new/images/leftHover.png');"
													  onmouseout="$(this).attr('src', 'layout/new/images/left.png');" 
													  alt="Passon logo" />
	</a>
	
	<div>
		<span class="currentPage">Page {$current_page} of {$overall_pages}</span>
	</div>
	
	{*link to next page*}
	<a href="#" class="paggingArrow" {if $current_page != $overall_pages} {prepare_paging_link offset=$current_page*$lines_per_page} {/if}>
		<img src="layout/new/images/right.png" onmouseover="$(this).attr('src', 'layout/new/images/rightHover.png');"
													   onmouseout="$(this).attr('src', 'layout/new/images/right.png');" 
													   alt="Passon logo" />
	</a>
	
	{*link to last page*}
	<a href="#" class="paggingArrow" {if $current_page != $overall_pages} {prepare_paging_link offset=($overall_pages-1)*$lines_per_page} {/if}>
		<img src="layout/new/images/lastPage.png" alt="Passon logo" />
	</a>
</div>
