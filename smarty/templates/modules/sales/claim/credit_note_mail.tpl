
 <!DOCTYPE html>
  <html>
      <head>
         <meta http-equiv='Content-Type' content="text/html;charset=ISO-8859-1" />
         <style>
			table.item {
			    overflow:hidden;
			    width: 100%;
			    table-layout:fixed;
			}
			td.itemheader {
			    overflow:hidden; 
			    border-bottom: 1px solid #cdcabb;
			    background: #898989;
			    padding-left: 8px ;
			    padding-top: 2px ;
			    padding-bottom: 2px ;
			    margin-bottom:10px;
			    cursor: default;
			}
			
			td.super {
				background: #898989;
				border: none;
				border-left:5px solid #898989;
				border-right: 5px solid #898989;
				border-bottom: 1px solid #c2c2c2;
				font-size:1.1em;
				padding-left: 2px; !important
			}	
			td.itemspace {
			    white-space:pre;
			    height:9px;
			    width:80px;
			}
			td.itemfield {
			    overflow:hidden;
			    padding-top:2px;
			    padding-left:8px;
			}
			td.itemlabel {
			    overflow:hidden;
			    padding-top:2px;
			    padding-left:8px;
			    width:80px;
			}
			table.list {
			    overflow:hidden;
			    width: 100%;
			    table-layout:fixed;
				border:none;
			}
			td.listhead {
				border: none;
			    border-bottom: 1px solid #cdcabb;
			    background: #898989;
			    overflow:hidden;
			}
			p.listhead{
			    height:16px;
			    padding-top: 1px;
			    padding-bottom: 1px;
			    overflow:hidden;
			}	 
			.claimlist tr:nth-child(odd){
				background-color: #c2c2c2;
			}
         </style>
      </head>
      <body >

		<table class="item">
			<tbody>
				<tr>
					<td class="itemheader" style="width:80px">Field</td>
					<td class="itemheader">Content</td>
				</tr>
				<tr>
					<td class="itemspace"></td>
				</tr>
				<tr>
					<td class="itemlabel">Credit note</td>
					<td class="itemfield">{$credit_note.Id}</td>
				</tr>
				<tr>
					<td class="itemlabel">Description</td>
					<td class="itemfield">{$credit_note.Description}</td>
				</tr>
				<tr>
					<td class="itemlabel">Type</td>
					<td class="itemfield">Credit</td>
				</tr>
				<tr>
					<td class="itemlabel">Created</td>
					<td class="itemfield">{$credit_note.CreateDate}</td>
				</tr>
				<tr>
					<td class="itemspace"></td>
				</tr>
			</tbody>
		</table>

		<table class="list">
			<tbody>
				<tr>
					<td class="itemheader super">Lines</td>
				</tr>
			</tbody>
		</table>
		
		<table class="list claimlist">
			{assign var=total_quantity value=0}
			{assign var=total_price value=0}
			<tbody>
				<tr>
					<td class="listhead" style="width:30px">Pos</td>
					<td class="listhead" style="width:100px">Article</td>
					<td class="listhead" style="width:100px">Colour</td>
					<td class="listhead" style="width:100%">Description</td>
					<td class="listhead" style="width:100px; text-align: right;">Quantity</td>
					<td class="listhead" style="width:100px; text-align: right;">Price</td>
					<td class="listhead" style="width:100px; text-align: right;">Total</td>
				</tr>
		
				{foreach $credit_note_lines as $i => $credit_note_line}
				<tr>
					<td>{$i + 1}</td>
					<td>{$credit_note_line.Article}</td>
					<td>{$credit_note_line.Colour}</td>
					<td>{$credit_note_line.Description}</td>
					<td style="text-align: right;">{floor($credit_note_line.Quantity)} Pcs</td>
					<td style="text-align: right;">{number_format($credit_note_line.Price, 2, ',', ' ')} {$credit_note.Currency}</td>
					<td style="text-align: right;">{number_format($credit_note_line.Quantity * $credit_note_line.Price, 2, ',', ' ')} {$credit_note.Currency}</td>
		
					{assign var=total_quantity value=$total_quantity + $credit_note_line.Quantity}
					{assign var=total_price value=$total_price + $credit_note_line.Quantity*$credit_note_line.Price}
				</tr>
				{/foreach}
		
				<tr>
					<td class="list" colspan="4"><p class="list"></p></td>
					<td class="list" style="text-align: right;">{floor($total_quantity)} Pcs</td>
					<td class="list"><p class="list"></p></td>
					<td class="list" style="text-align: right;">{number_format($total_price, 2, ',', ' ')} {$credit_note.Currency}</td>
				</tr>
			</tbody>
		</table>
      </body>
</html>
