{assign var='linkMark' value=$itemLinkMark|default:'orderview'}

	<table class="tblData orderTable" nid="{$params.nid}">
		<thead>
			{foreach $headers as $k =>  $header}
      			{if $header@iteration eq abs($sort)}
      			   <th class="{$styles[$header]}" sortId='{$header@iteration}' sort="{if $sort >= 0}ASC{else}DESC{/if}">{$header}</th>
      			{else}
      			   <th class="{$styles[$header]}" sortId='{$header@iteration}' >{$header}</th>
      			{/if}
			{/foreach}
		</thead>
		<tbody>
			{foreach $listItems as $i => $item}
			<tr >
				{foreach $headers as $j => $header}
				{if $bgcolors[$header]==1}
				<td {navigation_on_click_link_by_mark mark=$linkMark id=$item.OrderId} class="{$styles[$header]}" bgcolor={$item.bgcolor}>{$item[$header]}</td>
				{else}
				<td {navigation_on_click_link_by_mark mark=$linkMark id=$item.OrderId} class="{$styles[$header]}" >{$item[$header]}</td>
				{/if}
				{/foreach}
			</tr>
			{/foreach}
		</tbody>
		
		{if isset($total)}
		<tfoot>
			<tr>
				<td>Total</td>
					{for $i=1 to $headers|@count - 1}
						<td class="right"> {$total[$i]} </td>
					{/for}
			</tr>
		</tfoot>
		{/if}
	</table>
