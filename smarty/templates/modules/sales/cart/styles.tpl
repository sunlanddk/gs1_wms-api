<div>

<div class="list">
<div class="mainlist card">

	<div class="container clearfix">
		{foreach $listItems as $i => $item}
		<div class="inner-item {if $item.inCart eq 1}in-cart{else}{if $item.soldOut}sold-out{/if}{/if}" id = "{$item.Id}_{$item.ColorId}_{$item.seasonId}"
			onclick="showArticle({$item.Id}, {$item.ColorId}, {$item.seasonId}, {$nid}, {$vnid})">
			<div>
				{assign var="url" value="{$imageStore}/thumbnails/{$item.Number}_{$item.ColorNumber}.jpg"}
				{if !file_exists($url)}
					{assign var="url" value="image/no_image.png"}
				{/if}
				<img class="item-img" src="{$url}">
			</div>
			<div class="label">{$item.Name}</div>
			<div class="label">{$item.Number} - {$item.seasonName}</div>
			<div class="label">{$item.colorName}</div>
			{if isset($item.WholesalePrice) && $item.WholesalePrice>=0 && $currency.id>0}
			<div class="item-price">
				<div class="price">{number_format($item.WholesalePrice, 2, ',', ' ')} {$currency.symbol}</div>
			</div>
			{/if}

		</div>
		{/foreach}

	</div>
</div>

</div>
</div>





