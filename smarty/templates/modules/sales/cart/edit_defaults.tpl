{literal}
   <script type='text/javascript'>
      function saveForm() {
         $("#alert").hide();
         var msg = [];
         if ($("#CompanyId").val() == "") {
            $("#alert font").html("You should select Customer Company first.");
            $("#alert").show();
            return;
         }

         $("[required]").each(function() {
            $(this).removeClass("ui-state-error");
            if ($(this).val() == "") {
               $(this).addClass("ui-state-error");
               msg.push($(this).attr("name"));
            }
         });

        if (msg == "") {
              if ($('#ready').length > 0 && $('#ready').is(':checked')) {
                $('#Body').val($('#BodyDraft').val());
              }
              $( "#dialog-modal" ).dialog({
               height: 500,
               width : 600,
               modal: true,
               buttons: {
                  'Create & Mail': function() {
                    save(true);
                    $( this ).dialog( "close" );
                  },
                  'Only Create': function() {
                     save(false);
                     $( this ).dialog( "close" );
                  },
                  'Cancel': function() {
                     $( this ).dialog( "close" );
                  }
                }
             });
//           } else {
//             save(false);
//           }
           return;
         } else {
            $("#alert font").html("Next fields: <b>" + msg.join() + "</b> are mandatory.");
            $("#alert").show();
            return;
         }
      }

      function save (withMail) {
         var data =  $("#defaults").serializeObject();

         if (withMail) {
            data['sendMail'] = 1;
            data['Email'] = $("#Email").val();
            data['Body'] = $("#Body").val();
            data['EmailUpdate'] = $("#EmailUpdate").is(':checked');
         }
         $.address.history(false);
         appLoadAjaxBody(
               $("#savenew").attr('nid'),
               null,
               null,
               "#alert",
               data,
               "POST"
         );
         $.address.history(true);
      }

      $(document).ready(function() {
   		$('table.saveform input.int').each(function() {
   			maskElement(this, isInteger);
   		});
   		$('table.saveform input.float').each(function() {
   			maskElement(this, isFloat);
   		});
   		$('table.saveform input.percent').each(function() {
   			maskElement(this, isPercentage);
   		});

   		$("#CompanyId").change(function() {
   		   appLoadLegacy(true, $("#editdef").attr('nid'), 0, "CompanyId="+$(this).val());
   		});

      $("#ConsolidatedId").change(function() {
         appLoadLegacy(true, $("#editdef").attr('nid'), 0, "ConsolidatedId="+$(this).val()+"&CompanyId="+$("#CompanyId").val());
      });

         $( "#currency-dlg" ).dialog({
            height: 150,
            width : 400,
            modal : true,
            autoOpen : false,
            buttons: {
               'Ok': function() {
                  $( this ).dialog( "close" );
               }
             }
          });

   		var compCurrId = parseInt($("#CompanyCurrencyId").val());
   		var currId = parseInt($("#currId").val());

   		if (!isNaN(compCurrId) && !isNaN(currId) && currId != compCurrId) {
   		   $( "#currency-dlg" ).dialog("open" );
   		}
      });
   </script>
{/literal}

<div class="ui-widget" style="display: none;" id="alert">
   <div class="ui-state-error ui-corner-all messages">
   <p>
      <span class="ui-icon ui-icon-alert"></span>
      <strong>Error:</strong>
      <font>&nbsp;</font>
   </p>
   </div>
</div>

<form class="saveform" id="defaults">
   <input type="hidden" id="editdef" nid="{$nid}" />
   <input type="hidden" id="savenew" nid="{navigation_mark mark='sales.ordercart.savenew'}" />
   <input type="hidden" name="Proposal"     value="{$Proposal}" />
   <input type="hidden" name="ToCompanyId"  value="{$record['ToCompanyId']}" />
   <input type="hidden" id="CompanyCurrencyId"    value="{$record['CurrencyId']}" />

   <table class="saveform">
      {if $user['Extern']}
         <input type="hidden" name="CompanyId" value="{$record['CompanyId']}" />
	     <input type="hidden" name="SalesUserId"  value="{$record['SalesUserId']}" />
         <tr>
            <td class="header" colspan="4">{$header}</td>
         </tr>
      {else}
         <tr>
            <td class="label">Company:</td>
            <td class="field">
               <select name="CompanyId" id="CompanyId" required>
                  {html_options options=$userCompanies selected=(int)$record['CompanyId']}
               </select>
            {if $user['SalesRef']}
   			   <input type="hidden" name="SalesUserId"  value="{$record['SalesUserId']}" />
            {else}
               <td class="label">Sales Ref:</td>
               <td class="field">
                  <select name="SalesUserId">
                     {html_options options=$sales_ref selected=(int)$record['SalesUserId']}
                  </select>
               </td>
   		    {/if}
         </tr>
      {/if}

      <tr>
         <td class="label">Reference:</td>
         <td class="field"><input name="Reference" value="{$record['Reference']}" /></td>
		 {if !($user['SalesRef'] or $user['Extern'])}
            <td class="label">Description:</td>
            <td class="field"><input name="Description" value="{$record['Description']}" /></td>
        {/if}
      </tr>
	 {if !($user['SalesRef'] or $user['Extern'])}
      <tr>
          <td class="label">Consolidate with:</td>
         <td class="field">
           <select id="ConsolidatedId" name="ConsolidatedId">
              {html_options options=$consolidatedorders selected=(int)$record['ConsolidatedId']}
            </select>
         </td>
      </tr>
     {/if}
      <tr>
         <td class="field">
            <input type="hidden" name="OrderTypeId"  value="{$record['OrderTypeId']}" />
         </td>
      </tr>
      <tr>
         <td class="label">Delivery at:</td>
         <td class="field"><input name="Address1" value="{$record['Address1']}"/></td>
		 {if !($user['SalesRef'] or $user['Extern'])}
            <td class="label">Currency:</td>
            <td class="field">
               <!--  input type="hidden" name="CurrencyId" value="{$record['CurrencyId']}" /-->
               <select name="CurrencyId"  id="currId" required>
                  {html_options options=$currencies selected=$currencyId}
               </select>
            </td>
         {else}
            <input type="hidden" name="CurrencyId" id="currId" value="{$currencyId}" />
         {/if}
      </tr>
      <tr>
         <td class="label">Adress:</td>
         <td class="field"><input name="Address2" value="{$record['Address2']}"/></td>
		 {if !($user['SalesRef'] or $user['Extern'])}
            <td class="label">Payment Term:</td>
            <td class="field">
               <select name="PaymentTermId" required>
                  {html_options options=$payment_terms selected=(int)$record['PaymentTermId']}
               </select>
            </td>
         {else}
           <input type="hidden" name="PaymentTermId"  value="{$record['PaymentTermId']}" />
         {/if}
      </tr>
      <tr>
         <td class="label">ZIP:</td>
         <td class="field"><input name="ZIP" value="{$record['ZIP']}"/></td>
		 {if !($user['SalesRef'] or $user['Extern'])}
            <td class="label">Delivery Terms:</td>
            <td class="field">
               <select name="DeliveryTermId" required>
                  {html_options options=$delivery_terms selected=(int)$record['DeliveryTermId']}
               </select>
            </td>
         {else}
            <input type="hidden" name="DeliveryTermId"  value="{$record['DeliveryTermId']}" />
         {/if}
      </tr>
      <tr>
         <td class="label">City:</td>
         <td class="field"><input name="City" value="{$record['City']}"/></td>
		 {if !($user['SalesRef'] or $user['Extern'])}
            <td class="label">Carrier:</td>
            <td class="field">
               <select name="CarrierId" required>
                  {html_options options=$carriers selected=(int)$record['CarrierId']}
               </select>
            </td>
         {else}
            <input type="hidden" name="CarrierId"  value="{$record['CarrierId']}" />
         {/if}
      </tr>
      <tr>
         <td class="label">Country:</td>
         <td class="field">
            <select name="CountryId">
               {html_options options=$countries selected=(int)$record['CountryId']}
            </select>
         </td>
          <td class="label">&nbsp;</td>
          <td class="field">&nbsp;</td>
		 </tr>
      <tr>
         {if $user['SalesRef'] or $user['Extern']}
			 <td class="label">Freight Price:</td>
			 <td class="labelleft">&nbsp;&nbsp;{$record['FreightPrice']}&nbsp;{$currencyName}<input name="Freight" value="{$record['FreightPrice']}" type="HIDDEN" class="float right"/></td>
            <td class="label" colspan="4">&nbsp;</td>
         {else}
			 <td class="label">Freight Price:</td>
			 <td class="field"><input name="Freight" value="{$record['FreightPrice']}" class="float right"/></td>
			 <!-- td class="label">Freight Q-ty:</td>
			 <td class="field"><input name="Freight" value="" class="int right"/></td-->
		 {/if}
      </tr>

	  {if !($user['SalesRef'] or $user['Extern'])}
         <tr>
           <td class="label">Discount:</td>
            <td class="field">
               <input name="Discount" value="{$record['Discount']}" class="percent right"/><span>%</span>
            </td>
          </tr>
     {/if}
      {if !($user['Extern']) and !$Proposal}
         <tr>
            <td class="label">Confirmed:</td>
            <td class="field">
               <input type=checkbox id='ready'  name="Ready" />
            </td>
         </tr>
      {/if}

      <tr>
         {if $user['Extern']}
            <td class="label" colspan="2">&nbsp;</td>
         {else}
            <td class="label">EAN Codes:</td>
            <td class="field">
               <input type=checkbox name="VariantCodes" />
            </td>
         {/if}
      </tr>

   	  {if !($user['SalesRef'] or $user['Extern'])}
         <tr>
            <td class="label">Document Header:</td>
            <td class="field" colspan="3">
               <textarea name="InvoiceHeader">{$record['InvoiceHeader']}</textarea>
            </td>
         </tr>
         <tr>
            <td class="label">Document Footer:</td>
            <td class="field" colspan="3">
               <textarea name="InvoiceFooter">{$record['InvoiceFooter']}</textarea>
            </td>
         </tr>
         <tr>
            <td class="label">Pick Instructions:</td>
            <td class="field" colspan="3">
               <textarea name="Instructions">{$record['Instructions']}</textarea>
            </td>
         </tr>
      {/if}

   </table>
</form>

<div id="dialog-modal" title="Send Confirmation To mails" style="display: none;">
  <p>Customer <b>{$record.Name}</b> is registered to use the email address <font color="red">{$record.Mail}</font>. If e-mail is incorrect please change it in the textbox below and finally confirm by clicking confirm button</p>
  <br><br><br>
  <form class="saveform">
     <table class="saveform">
        <tr>
           <td class="label left small">Email:</td>
           <td class="field">
              <input name="Email" id="Email" value="{$record.Mail}"/><br><br>
           </td>
        </tr>
        <tr>
           <td class="label  left small">
           </td>
          <td class="label  left large">
			<input type=checkbox id="EmailUpdate" name="EmailUpdate" value="1"> Update e-mail address in customer account.<br><br>
          </td>
        </tr>
        <tr>
           <td>Email body:</td>
           <td>
               <textarea name="Body" id="Body" rows="35" cols="90">{$record.Body}</textarea>
           </td>
        </tr>
     </table>
  </form>
  <div style="display:none;">
      <textarea name="BodyDraft" id="BodyDraft" rows="35" cols="90">{$record.BodyDraft}</textarea>
  </div>
</div>

<div id="currency-dlg" title="Warning" style="display: none;">
  <p>Customer <b>{$record.Name}</b> has default currency <br><b>{$record['CurrencyName']}</b></span>, but you current Cart currency is <br><span style="white-space:nowrap;"><b>{$currencyName}</b></span>

</div>