 <!DOCTYPE html>
  <html>
      <head>
         <meta http-equiv='Content-Type' content="text/html;charset=ISO-8859-1" />
         <style>
            .tblData{
            	font-family: verdana;
            	font-size: 11px;
            	border: 0;
            	border-collapse: collapse;
            	width : 100%;

            }
            .tblData th {
            	font-weight: bold;
            	background: #ebeaea;
            	border-bottom: 1px solid #8c8c8c;
            	padding: 10px 15px 10px 5px;
            	text-align: left;
            	cursor: pointer;
            }
            .tblData th:hover {
            	background: #C4C4C4;
            }

            .tblData tr{
                background:  #fff left bottom url('images/trBorder.png') repeat-x;
            }

            .tblData tr:nth-child(odd){
            	background: #f7f7f7 left bottom url('images/trBorder.png') repeat-x !important;
            }
            .tblData tr:hover td {
            	background: #ebeaea left bottom url('images/trBorder.png') repeat-x;
            }

            .tblData td {
                padding: 5px 3px;
                text-align: left;
            }
            .tblData th.right, .tblData td.right {
	            text-align: right !important;
	            padding-right: 10px !important;
            }
            .tblView th, .tblView th:hover  {
                background: #ebeaea !important;
                cursor: default;
            }
            .tblView td {
                vertical-align: middle;
            }
            .tblView tr.selected, .tblView tr.selected:hover td {
                background: rgba(137, 168, 43, 0.6) left bottom url('images/trBorder.png') repeat-x !important;
            }
         </style>
      </head>
      <body>
         {assign var=items_count value=$cart|@count}
		<div>
		</div>
         {if $items_count > 0}
             <div class="item-panel"></div>
             {foreach $cart as $seasonId => $season}
         	<h2 class="cartInfo">{$season.seasonName}</h2>
         	<table class="tblData tblView" id="view-cart">
         		<thead>
                    <th width="10%">&nbsp;</th>
                    <th width="25%">Style No</th>
                    <th width="25%">Color</th>
                    <th width="10%" class="right">Discount</th>
                    <th width="10%" class="right">Price</th>
                    <th width="10%" class="right">Q-ty</th>
                    <th width="10%" class="right">Subtotal</th>
         		</thead>
         		<tbody>
                    {foreach $season.items as $article}
                       <tr>
                          <td>
                             {assign var="url"  value="{$imagesPath}/thumbnails/{$article.Number}_{$article.ColorNumber}.jpg"}
                             {assign var="file" value="{$imageStore}\\thumbnails\\{$article.Number}_{$article.ColorNumber}.jpg"}
                             {if !file_exists($file)}
                                {assign var="url" value="{$base_url}/image/no_image.png"}
                             {/if}
                             <img  src="{$url}" height="80px" width="80px">
                          </td>
                          <td>{$article.Number}<br/>{$article.Desc}</td>
                          <td>{$article.ColorNumber}<br>{$article.ColorDesc}</td>
                          <td class="right">0 %</td>
                          <td class="right">{number_format($article.Price, 2, ',', ' ')} {$season.currency.Name}</td>
                          <td class="right">{$article.qty}</td>
                          <td class="right">{number_format($article.subTotal, 2, ',', ' ')} {$season.currency.Name}</td>
                       </tr>
                    {/foreach}
         		</tbody>
         		<tfoot>
         		   <tr>
         		      <th colspan="4">&nbsp;</th>
         		      <th  class="right">SubTotal</th>
         		      <th  class="right">{$season.qty}</th>
         		      <th  class="right">{number_format($season.subTotal, 2, ',', ' ')}  {$season.currency.Name}</th>
         		   </tr>
         		</tfoot>
         	</table>
         	{/foreach}
         {/if}
</body>
</html>