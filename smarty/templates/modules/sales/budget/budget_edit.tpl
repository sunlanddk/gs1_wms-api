{literal}
<script type='text/javascript' src='js/style.js'></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#budgetsum').each(function() {
			maskElement(this, isInteger);
		});
	});
</script>
{/literal}
<form method=POST name=appform>
	<input type=hidden name=nid />
	{if isset($recordId)}<input type=hidden name=recordid id=recordid value={$recordId} />{/if}
	{if isset($companyId)}<input type=hidden name=CompanyId id=CompanyId value={$companyId} />{/if}
	{if isset($userId)}<input type=hidden name=UserId id=UserId value={$userId} />{/if}
	<input type=hidden name=SeasonId id=SeasonId value={$seasonId} />
	<input type=hidden name=CurrencyId id=CurrencyId value={$currencyId} />
	<table class=item>
		<tr><td class=itemspace></td></tr>
		<tr>
			<td class=itemlabel>Budget</td>
			<td class=itemfield><input type="text" name="BudgetSum" id="BudgetSum" class="integer" value="{$budgetSum}" />{$curencyName}</td>
		</tr>
		<tr><td class=itemspace></td></tr>
	</table>
</form>