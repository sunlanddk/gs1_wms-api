{literal}
	<script type='text/javascript' src='js/budget.js'></script>
{/literal}

<table class="tblBudget">
	<thead>
		<th>Sales Responsible</th>
		{foreach $headers as $header}
			<th>{$header}</th>
		{/foreach}
	</thead>
	<tfoot>
		<td>Total</td>
		{foreach $headers as $i => $header}
			<td><span>Budget:</span>{number_format($total[$i]['Budget'], 2, ',', ' ')}{" "}{$currencyName}
			<br><span>Sold:</span>{number_format($total[$i]['Sold'], 2, ',', ' ')}{" "}{$currencyName}
			<br><span>Invoiced:</span>{number_format($total[$i]['Invoiced'], 2, ',', ' ')}{" "}{$currencyName}</td>
		{/foreach}
	</tfoot>
	<tbody>
		{foreach $users as $i => $item}
		<tr>
			<td>{$item['uname']}
			<br>{$item['aname']}</td>
			{foreach $headers as $j => $header}
  			   <td onclick="editBudget({$currencyId}, null, {$i}, {$j}, {$nid})">
  			   <span>Budget:</span> {if (isset($item[$j]['Budget']) && $item[$j]['Budget']>0)}{number_format($item[$j]['Budget'], 2, ',', ' ')}{" "}{$currencyName}{/if}
  			   <br><span>Sold:</span> {if isset($item[$j]['Sold'])}{number_format($item[$j]['Sold'], 2, ',', ' ')}{" "}{$currencyName}{/if}
  			   <br><span>Invoiced:</span> {if isset($item[$j]['Invoiced'])}{number_format($item[$j]['Invoiced'], 2, ',', ' ')}{" "}{$currencyName}{/if}</td>
			{/foreach}
		</tr>
		{/foreach}
	</tbody>
</table>