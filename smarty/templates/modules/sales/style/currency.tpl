{literal}
   <script type="text/javascript">
      $(document).ready(function() {
         var dlg = $( "div.tblCurrency" ).dialog({
            autoOpen: true,
            appendTo: ".main",
            height: 165,
            width: 350,
            modal: true,
            buttons: {
               Save : function() {
                  $.address.history(false);
                  var curName = $("#CurrencyId :selected").text();
                  appLoadAjaxBody(
                     $("#nid").val(),
                     null,
                     null,
                     null,
                     {
                        currencyId: $("#CurrencyId").val(),
                        companyId: $("#CompanyId").val()
                     },
                     null,
                     function () {
                        var results = [],
                            re      = /\(([^)]+)\)/gi,
                            text;

                        while(text = re.exec(curName)) {
                           results.push(text[1]);
                        }
                        $("#cartCurr").text(results[results.length - 1]);

                        showArticle($("#articleId").val(), $("#colorId").val(), $("#seasonId").val(), $("#nid").val(), $("#vnid").val());
                     }
                  );
                  $.address.history(true);

                  $( this ).dialog( "close" );
               }
            },
            close: function( event, ui ) {
           		$( "div.tblCurrency" ).remove();
            }
         });

         $(dlg).dialog('widget').position({
            my: "center",
            at: "center",
            of: ".subNavigation"
         });
         
         $("#CompanyId").change(function(){
         	var currencyId = $("#CompanyId option:selected").attr('currency');
         	if(currencyId)
         		$('#CurrencyId').val(currencyId);
         	else
         		$('#CurrencyId').val('');
         });
         
      });
   </script>
{/literal}
		<input type="hidden" id="articleId" value="{$articleId}" />
		<input type="hidden" id="colorId" value="{$colorId}" />
		<input type="hidden" id="seasonId" value="{$seasonId}" />
		<input type="hidden" id="nid" value="{$nid}" />
		<input type="hidden" id="vnid" value="{$vnid}" />
      	<div class="tblCurrency" title="Select Currency or Company">
      	   <table>
      	   <tbody>
                <tr>
                  <td>Currency:</td>
                  <td>
                     <select name="CurrencyId"  id ="CurrencyId" style="width: 95%;" required>
                        {html_options options=$currencies}
                     </select>
                  </td>
                </tr>
                <tr>
                  <td>Company:</td>
                  <td>
                     <select name="CompanyId" id="CompanyId" style="width: 95%;" required>
		               {foreach $userCompanies as $k =>  $company}
		               <option currency="{$company['currency']}" value="{$company['value']}" {if $company['value'] == $selectedCompany}selected{/if}>{$company['text']}</option>
		               {/foreach}
		            </select>
                  </td>
                </tr>
             </tbody>
      	   </table>
      	   <br/>
      	</div>